import React from 'react';


const NotFound = () => {
    return (
        <div className="error_bg">
        <div className="flex_p">
            <div><img src="/assets/images/404_img.svg" className="error_img_404" /></div>
            <div>
                <h2>Oops!</h2>
                <p>It looks like the link you have entered no longer exists or is incorrect or no data found. </p>
                <p>
                    {// You can search for what you’re looking for in the search bar above
                    // <br />or click the button below to go back to the previous page.
                    }
                </p>
                <div className="text-right ">
                    <a className="btn px-5 VEC_btn" href="/">Back To Previous Page</a>
                </div>
            </div>
        </div>
        </div>
    );
}


export default NotFound;
