import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { getInvoiceByType } from '../store/actions';

class FollowUpInvoices extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterVal: '',
      type: 3,
      search: ''
    }
  }

   onSubmitSearch = (e) => {
        e.preventDefault();
        this.setState({filtered: {search: this.state.search}});
    }

  fetchData = (e) => {
        this.props.getInvoiceByType({invoice_type: this.state.invoice_type, pageSize: e.pageSize, page: e.page, sorted: e.sorted, filtered:e.filtered });
    }

  toggleRow = (id) => {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 0
    });
  }

  toggleSelectAll = () => {

    let newSelected = {};

    if (this.state.selectAll === 0) {
      this.props.approvedInvoice.forEach(x => {
        newSelected[x.id] = true;
      });
    }
  }
  render() {
    const { invoiceListing } = this.props;
    const { plannerData } = this.props;
    const hdrColumn = [
      { Header: 'HCM ID', accessor: 'ocisid' },
      { Header: 'NDIS ID', accessor: 'ndisid', Cell: (props) => <div>{props.original.ndisid}</div> },
      { Header: 'Last Name', accessor: 'lastname', Cell: (props) => <div>{props.original.lastname}</div> },
      { Header: 'First Name', accessor: 'firstname', Cell: (props) => <div>{props.original.firstname}</div> },
      { Header: 'Invoice Number', accessor: 'invoice', Cell: (props) => <div>{props.original.invoice}</div> },
      { Header: 'Due Date', accessor: 'duedate', Cell: (props) => <div>{props.original.duedate}</div> },
      { Header: 'Invoice Total', accessor: 'total', Cell: (props) => <div>{props.original.total}</div> },
      { Header: 'Biller', accessor: 'biller', Cell: (props) => <div>{props.original.biller}</div> },
      {
        Header: 'Status',
        accessor: 'status',
        sortable: false,
        filterable: false,
        Cell: (props) =>
          <div className='stat_grp'>
            {
              (props.original.status === 'duplicate') ? <span className='status duplicate'>Duplicate</span>
                :
                (props.original.status === 'inQueue') ? <span className='status inQueue'>In Queue</span>
                  :
                  (props.original.status === 'followUp') ? <span className='status followUp'>Follow Up</span>
                    : ' '}

          </div>
      },
      {
        Header: '',
        accessor: 'id',
        sortable: false,
        filterable: false,
        width: 50,
        Cell: (props) =>
          <div>
            <Link to={'/followup/viewfollowup/' + props.value}>
              <i className='icon icon-view see_ic'></i>
            </Link>
          </div>
      }

    ]

    return (

      <AuthWrapper>

        <div className=" back_col_cmn-">
          <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
        </div>

        <div className="main_heading_cmn-">
          <h1>
            <span>Follow Up Invoice</span>
            <Link to={'/followup/viewfollowup/' + (typeof (plannerData) != 'undefined' ? plannerData : '')}>
              <button className="btn hdng_btn cmn-btn1">Run Planner</button>
            </Link>
          </h1>
        </div>

        <div className='row pd_t_15'>
          <div className='col-6'>
            <form onSubmit={this.onSubmitSearch}>
              <div className='cstm_select_fINacne'>
                <input type="text" placeholder="| Names, numbers, dates & status'" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
              </div>
            </form>
          </div>


        </div>


        <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
          <ReactTable
            data={invoiceListing}
            defaultPageSize={10}
            filtered={this.state.filtered}
            minRows={2}
            manual="true"
            pages={this.props.pages}
            loading = {this.props.loading}
            onFetchData={this.fetchData}
            className="-striped -highlight"
            previousText={<span className="icon icon-arrow-1-left previous"></span>}
            nextText={<span className="icon icon-arrow-1-right next"></span>}
            columns={hdrColumn}
            pageSizeOptions={[10, 20, 30, 40, 50]}
          />
        </div>
        {/* table comp ends */}

      </AuthWrapper>

    );
  }
}




const mapStateToProps = state => {
  return {
    invoiceListing: state.InvoiceReducer.invoiceListing,
    plannerData: state.InvoiceReducer.plannerData,
    pages: state.InvoiceReducer.plannerPages,
     loading: state.Reducer1.loading

  }
};
export default connect(mapStateToProps, { getInvoiceByType })(FollowUpInvoices);

// export default FollowUpInvoices;
