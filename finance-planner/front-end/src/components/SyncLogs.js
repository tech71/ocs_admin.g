import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import DatePicker from "react-datepicker";
import  {allsyncLogs,syncCsvDownload,processCsv,loader} from '../store/actions';
import { connect } from 'react-redux';
import { BASE_URL } from '../config.js';
import moment from 'moment';
class SyncLogs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            allsyncLogs:[],
            checkAll: true,
            selectAll:0,
            selected: {},
            approvedSelectedList:[],
            file_url:'',
            pageSize:'',
            page:'',
            sorted:'',
            filtered:'',
            fromDate:'',
            toDate:''

        }
    }

    // componentWillMount(){
    //   this.props.allsyncLogs().then(res => {
    //       this.setState({allsyncLogs:res.synclogs});
    //   });
    // }

    toggleRow = (id) => {
     const newSelected = Object.assign({}, this.state.selected);
     newSelected[id] = !this.state.selected[id];
     this.setState({
       selected: newSelected,
       selectAll: 0
     });
   }

   searchData = (key, value) => {
         if(key=='fromDate' || key=='toDate'){
           let state= this.state;
           state[key] = value;
           this.setState(state)
         }
         var srch_ary = { filterVal: this.state.filterVal,categoryVal: this.state.categoryVal,fromDate: this.state.fromDate,toDate: this.state.toDate };
         srch_ary[key] = value;
         this.setState(srch_ary);
         this.setState({filtered: srch_ary});
         //console.log(this.state.fromDate);
     }

   fetchData = (state, instance) => {
     // function for fetch data from database
     this.setState({ loading: true });
     this.props.allsyncLogs(
         state.pageSize,
         state.page,
         state.sorted,
         state.filtered
     ).then(res => {
         this.setState({
             allsyncLogs: res.synclogs,
             pages: res.synclogscount,
             loading: false
         });

     });
 }


   processCsv = (id) => {
     this.props.processCsv(id)
       .then(json => {
       if(json.status==true){
         this.props.allsyncLogs().then(res => {
             this.setState({allsyncLogs:res.synclogs,pages: res.synclogscount});
         });
       }
       this.props.loader(false);
     });
   }


   toggleSelectAll = () =>  {

     let newSelected = {};

     if (this.state.selectAll === 0) {
       this.state.allsyncLogs.forEach(x => {
         newSelected[x.id] = true;
       });
     }

     this.setState({
       selected: newSelected,
       selectAll: this.state.selectAll === 0 ? 1 : 0
     });
   }





   csvDownload = () => {
     let approvedSelectedList =[];
     let data = this.state.selected;
     this.state.allsyncLogs.forEach(function(k){
       if(data.hasOwnProperty(k.id) && data[k.id]){
         approvedSelectedList.push(k)
       }
     })
     var requestData = { approvedSelectedList: approvedSelectedList };
     this.props.syncCsvDownload(requestData)
     .then(json => {
         if (json.status == true) {
           window.location.href = BASE_URL + json.csv_url;
         }
     })
  }
    render() {
        var minDate = new Date();
        minDate.setFullYear(moment(this.state.fromDate).year(),moment(this.state.fromDate).month() ,moment(this.state.fromDate).date());

        const logsColumn = [
            {

              Header: (props) =>
              <label className='cstmChekie clrTpe2'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selectAll === 1}
                      ref={input => {
                          if (input) {
                              input.indeterminate = this.state.selectAll === 2;
                          }
                      }}
                      onChange={() => this.toggleSelectAll()}
                   />
                  <div className="chkie"></div>
              </label>,
              accessor: "",
              width: 70,
              sortable: false,
              filterable: false,
              Cell: (props) =>
              <label className='cstmChekie clrTpe3'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selected[props.original.id] === true}
                      onChange={() => this.toggleRow(props.original.id)}
                  />
                  <div className="chkie"></div>
              </label>,

          },
            { Header: 'Batch ID:', accessor: 'id' },
            { Header: 'Date of Upload:', accessor: 'date_of_upload', Cell: (props) =><div>{props.original.date_of_upload}</div> },
            { Header: 'Date of Completed:', accessor: 'date_of_complete', Cell: (props) =><div>{props.original.date_of_complete}</div> },
            { Header: 'Records Moved to followup:', accessor: 'no_of_records_to_followup', Cell: (props) =><div>{props.original.no_of_records_to_followup}</div> },
            { Header: 'Records Moved to Xero:', accessor: 'no_of_records_to_xero', Cell: (props) =><div>{props.original.no_of_records_to_xero}</div> },
            { Header: 'Total in CSV', accessor: 'total_record_in_csv', Cell: (props) =><div>{props.original.total_record_in_csv}</div> },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                          (props.original.status  == 2) ?
                          <div className='stat_grp'><span className='status duplicate'>Failed</span>
                          <span onClick={()=>this.processCsv(props.original.id)}>
                              Restart
                          </span></div>:
                          (props.original.status  == 3) ? <span className='status inQueue'>Processing</span>:
                          (props.original.status == 1) ? <span className='status paid'>Successful</span>:''
                        }

                    </div>
            },
        ]



        var options = [
            { value: '0', label: 'All' },
            { value: '1', label: 'Successful' },
            { value: '2', label: 'Failed' }
        ];


        return (

            <AuthWrapper>

                <div className=" back_col_cmn-">
                <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                  {// <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                  }
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Sync Logs</span>
                    </h1>
                </div>

                <div className='row pd_t_15'>
                    <div className='col-xl-5 col-lg-7 col-7 mr_b_15_min'>
                        <div className='cstm_select search_filter brdr_slct'>
                      {// <form  onSubmit={(e) => this.searchData( 'filterVal', this.state.filterVal )}>
                      }
                          <div className='cstm_select_fINacne'>
                          <input type="text" placeholder="| Sync Logs"  name="sync_search"
                          value={this.state.filterVal || ''}  onChange={(e) => this.searchData( 'filterVal', e.target.value )} />

                          <button  onClick={(e) => this.searchData( 'filterVal', e.target.value )} type="submit"><span className="icon icon-search1-ie"></span></button>
                          </div>
                        {  // </form>
                        }
                        </div>
                    </div>

                    <div className='col-xl-3 col-lg-5 col-5 mr_b_15_min'>

                        <div className='cstm_select  slct_cstm2 bg_grey'>

                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Status"
                                options={options}
                                onChange={(e) => this.searchData( 'categoryVal', e )}
                                value={this.state.categoryVal}

                            />

                        </div>

                    </div>

                    <div className='col-xl-2 col-lg-4 col-4'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>From</label>
                            <DatePicker
                                className="csForm_control"
                                selected={moment(this.state.fromDate).valueOf()}
                                value={this.state.fromDate}
                                onChange={(e) => this.searchData( 'fromDate', moment(e).format("YYYY-MM-DD") )}
                                dateFormat="DD/MM/YYYY"
                                name="dateofupload"
                                placeholderText={'DD/MM/YY'}
                                autoComplete={'off'}
                            />
                        </div>
                    </div>

                    <div className='col-xl-2 col-lg-4 col-4'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>To</label>
                            <DatePicker
                                className="csForm_control"
                                selected={moment(this.state.toDate).valueOf()}
                                value={this.state.toDate}
                                onChange={(e) => this.searchData( 'toDate', moment(e).format("YYYY-MM-DD") )}
                                dateFormat="DD/MM/YYYY"
                                name="dateofupload"
                                placeholderText={'DD/MM/YY'}
                                minDate={minDate}
                                autoComplete={'off'}
                                popperPlacement={"botom-start"}
                                // popperModifiers={{
                                //   flip: {
                                //     enabled: true
                                //   },
                                //   preventOverflow: {
                                //     enabled: true,
                                //     escapeWithReference: false
                                //   }
                                // }}
                            />
                        </div>
                    </div>

                </div>


                <div className="data_table_cmn dashboard_Table pending_invTable transFirst_tble text-center bor_top1 pd_tb_15" >
                {    // <ReactTable
                    //     data={this.state.allsyncLogs}
                    //     defaultPageSize={10}
                    //     className="-striped -highlight"
                    //     previousText={<span className="icon icon-arrow-1-left previous"></span>}
                    //     nextText={<span className="icon icon-arrow-1-right next"></span>}
                    //     columns={logsColumn}
                    //
                    //
                    // />
                  }

                  <ReactTable
                    //  ref={this.reactTable}
                      data={this.state.allsyncLogs}
                      onFetchData={this.fetchData}
                      onPageSizeChange={this.onPageSizeChange}
                      defaultPageSize={10}
                      pages={this.props.synclogscount}
                      manual="true"
                      filtered={this.state.filtered}
                      minRows={2}
                      loading={this.state.loading}
                      className="-striped -highlight"
                      previousText={<span className="icon icon-arrow-1-left previous"></span>}
                      nextText={<span className="icon icon-arrow-1-right next"></span>}
                      columns={logsColumn}
                      showPagination={true}
					  pageSizeOptions= {[10, 20, 30, 40, 50]}
                  />

                </div>
                {/* table comp ends */}

                <div className="text-right">
                     <button className="btn hdng_btn cmn-btn1" download href={BASE_URL+this.state.file_url} target="_blank" onClick={() => this.csvDownload()}>Export CSV &nbsp;   &nbsp;<i className="icon icon-download2-ie"></i></button>
                    </div>

            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
return {
  getsynclogs: state.InvoiceReducer.allsynclogsdata,
  csvDownload:state.InvoiceReducer.syncCsvDownload,
  synclogscount: state.InvoiceReducer.synclogscount
  }
};
export default connect(mapStateToProps,{allsyncLogs,syncCsvDownload,processCsv,loader})(SyncLogs);
