import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { BASE_URL } from '../config.js';
import { connect } from 'react-redux';
import { getInvoiceByType, approvedCsvDownload, loader } from '../store/actions';

class ApprovedInvoices extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterVal: '',
      checkAll: true,
      selectAll: 0,
      selected: {},
      approvedSelectedList: [],
      file_url: '',
      invoice_type: 1,
      search: ''
    }
  }

  toggleRow = (id) => {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 0
    });
  }

  toggleSelectAll = () => {

    let newSelected = {};
    if (this.state.selectAll === 0) {
      this.props.invoiceListing.forEach(x => {
        newSelected[x.id] = true;
      });
    }

    this.setState({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    });
  }


  csvDownload = () => {
    let approvedSelectedList = [];
    let data = this.state.selected;
    this.props.invoiceListing.forEach(function (k) {
        if (data.hasOwnProperty(k.id) && data[k.id]) {
            approvedSelectedList.push(k)
        }
    })
    this.setState(approvedSelectedList);
    this.props.loader(true);
    var requestData = { approvedSelectedList: approvedSelectedList };
    
    this.props.approvedCsvDownload(requestData)
      .then(json => {
        if (json.status == true) {
          window.location.href = BASE_URL + json.csv_url;
        }
      })
    this.props.loader(false);
  }

    onSubmitSearch = (e) => {
        e.preventDefault();
        this.setState({filtered: {search: this.state.search}});
    }
  
    fetchData = (e) => {
        this.props.getInvoiceByType({ invoice_type: this.state.invoice_type, pageSize: e.pageSize, page: e.page, sorted: e.sorted, filtered: e.filtered });
    }

  render() {
    const { invoiceListing } = this.props;
    const hdrColumn = [
      {
        Header: (props) =>
          <label className='cstmChekie clrTpe2'>
            <input
              type="checkbox"
              className=" "
              checked={this.state.selectAll === 1}
              ref={input => {
                if (input) {
                  input.indeterminate = this.state.selectAll === 2;
                }
              }}
              onChange={() => this.toggleSelectAll()}
            />
            <div className="chkie"></div>
          </label>,
        accessor: "",
        width: 70,
        sortable: false,
        filterable: false,
        Cell: (props) =>
          <label className='cstmChekie clrTpe3'>
            <input
              type="checkbox"
              className=" "
              checked={this.state.selected[props.original.id] === true}
              onChange={() => this.toggleRow(props.original.id)}
            />
            <div className="chkie"></div>
          </label>,

      },
      { Header: 'HCM ID', accessor: 'ocisid' },
      { Header: 'NDIS ID', accessor: 'ndisid' },
      {
        Header: 'Last Name',
        accessor: 'lastname',
        Cell: (props) => <div>{props.original.lastname}</div>
      },
      {
        Header: 'First Name',
        accessor: 'firstname',
        Cell: (props) => <div>{props.original.firstname}</div>
      },
      {
        Header: 'Invoice Number.',
        accessor: 'invoice',
        Cell: (props) => <div>{props.original.invoice}</div>
      },
      { Header: 'Due Date', accessor: 'duedate' },
      { Header: 'Invoice Total', accessor: 'total' },
      {
        Header: 'Biller',
        accessor: 'biller',
        Cell: (props) => <div>{props.original.biller}</div>
      },
      {
        Header: 'HCM Status',
        accessor: 'status',
        sortable: false,
        filterable: false,
        Cell: (props) =>
          <div className='stat_grp'>
            {(props.original.status === 'Approved') ? <span className='status paid'>Approved</span> : ' '}
          </div>
      },



    ]

    return (

      <AuthWrapper>
        <div className=" back_col_cmn-">
          <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
        </div>

        <div className="main_heading_cmn-">
          <h1>
            <span>Approved Invoices</span>
            {// <Link to='#'>
              //     <button className="btn hdng_btn cmn-btn1">Run Planner</button>
              // </Link>
            }
          </h1>
        </div>


        <div className='row pd_t_15'>
          <div className='col-6'>
            <form onSubmit={this.onSubmitSearch}>
              <div className='cstm_select_fINacne'>
                <input type="text" placeholder="| Names, numbers, dates & status'" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
              </div>
            </form>
          </div>
        </div>


        <div className="data_table_cmn dashboard_Table pending_invTable transFirst_tble text-center bor_top1 pd_tb_15" >
          <ReactTable
            loading = {this.props.loading}
            data={invoiceListing}
            manual="true"
            defaultPageSize={10}
            filtered={this.state.filtered}
            pages={this.props.pages}
            onFetchData={this.fetchData}
            minRows={2}
            className="-striped -highlight"
            previousText={<span className="icon icon-arrow-1-left previous"></span>}
            nextText={<span className="icon icon-arrow-1-right next"></span>}
            columns={hdrColumn}
            pageSizeOptions={[10, 20, 30, 40, 50]}
          />

        </div>
        <div className="text-right">
          <button className="btn hdng_btn cmn-btn1" download href={BASE_URL + this.state.file_url} target="_blank" onClick={() => this.csvDownload()}>Export CSV &nbsp;   &nbsp;<i className="icon icon-download2-ie"></i></button>
        </div>


        {/* table comp ends */}

      </AuthWrapper>

    );
  }
}




const mapStateToProps = state => {
  return {
    invoiceListing: state.InvoiceReducer.invoiceListing,
    csvDownload: state.InvoiceReducer.approvedCsvDownload,
    pages: state.InvoiceReducer.plannerPages,
     loading: state.Reducer1.loading
  }
};
export default connect(mapStateToProps, { getInvoiceByType, approvedCsvDownload, loader })(ApprovedInvoices);

// export default FollowUpInvoices;
