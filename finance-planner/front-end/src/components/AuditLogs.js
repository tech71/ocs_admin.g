import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import { BASE_URL } from '../config.js';
import  {list_audit_logs,auditCsvDownload} from '../store/actions';
import moment from 'moment';
import ReactHtmlParser from 'react-html-parser';
class AuditLogs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            categoryVal:'',
            fromDate:'',
            toDate:'',
            loading:false,
            startDate: null,auditLogs:[],checkAll: true,
            selectAll:0,
            selected: {},
            approvedSelectedList:[],
            file_url:'',
            auditLogs_count:''
        }
          this.reactTable = React.createRef();
    }
    fetchData = (state, instance) => {
      // function for fetch data from database
      this.setState({ loading: true });
      this.props.list_audit_logs(
          state.pageSize,
          state.page,
          state.sorted,
          state.filtered
      ).then(res => {         
          this.setState({
            auditLogs: res.data,
            auditLogs_count:res.count,
              pages: 1,
              loading: false
          });

      });
  }

  searchData = (key, value) => {
        if(key=='fromDate' || key=='toDate'){
          let state= this.state;
          state[key] = value;
          this.setState(state)
        }
        var srch_ary = { filterVal: this.state.filterVal,categoryVal: this.state.categoryVal,fromDate: this.state.fromDate,toDate: this.state.toDate };

        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({filtered: srch_ary});
        
    }
    toggleRow = (id) => {
     const newSelected = Object.assign({}, this.state.selected);
     newSelected[id] = !this.state.selected[id];
     this.setState({
       selected: newSelected,
       selectAll: 0
     });
   }

   toggleSelectAll = () =>  {

     let newSelected = {};

     if (this.state.selectAll === 0) {
       this.state.auditLogs.forEach(x => {
         newSelected[x.id] = true;
       });
     }

     this.setState({
       selected: newSelected,
       selectAll: this.state.selectAll === 0 ? 1 : 0
     });
   }


   csvDownload = () => {
     let approvedSelectedList =[];
     let data = this.state.selected;
     this.state.auditLogs.forEach(function(k){
       if(data.hasOwnProperty(k.id) && data[k.id]){
         approvedSelectedList.push(k)
       }
     })
     this.setState(approvedSelectedList);
     var requestData = { approvedSelectedList: approvedSelectedList };
     this.props.auditCsvDownload(requestData)
     .then(json => {
         if (json.status == true) {
           window.location.href = BASE_URL + json.csv_url;
         }
     })
  }
    render() {
          var minDate = new Date();
          minDate.setFullYear(moment(this.state.fromDate).year(),moment(this.state.fromDate).month() ,moment(this.state.fromDate).date());
        const hdrColumn = [
          {

              Header: (props) =>
              <label className='cstmChekie clrTpe2'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selectAll === 1}
                      ref={input => {
                          if (input) {
                              input.indeterminate = this.state.selectAll === 2;
                          }
                      }}
                      onChange={() => this.toggleSelectAll()}
                   />
                  <div className="chkie"></div>
              </label>,
              accessor: "",
              width: 70,
              sortable: false,
              filterable: false,
              Cell: (props) =>
              <label className='cstmChekie clrTpe3'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selected[props.original.id] === true}
                      onChange={() => this.toggleRow(props.original.id)}
                  />
                  <div className="chkie"></div>
              </label>,

          },
            { Header: 'Occurrence', accessor: 'title',  Cell: (props) => <div>{props.original.title}</div> },
            { Header: 'Date', accessor: 'date' },
            { Header: 'Time', accessor: 'time' },
            { Header: 'User', accessor: 'user' },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 55,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }


        ]



        var options = [
            { value: '', label: '--Select--' },
            { value: 'Pending', label: 'Pending' },
            { value: 'Paid', label: 'Paid' },
            { value: 'Duplicate', label: 'Duplicate' },
            { value: 'followUp', label: 'followUp' }
        ];


        return (

            <AuthWrapper>

                        <div className=" back_col_cmn-">
                            <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                            {// <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                            }
                        </div>

                        <div className="main_heading_cmn-">
                            <h1>
                                <span>Audit Logs</span>

                            </h1>
                        </div>

                        <div className='row pd_t_15'>

                            <div className='col-xl-6 col-lg-6 col-6 '>

                                <div className='cstm_select search_filter brdr_slct '>
                                    <div className="cstm_select_fINacne">
                                <input type="text"  placeholder="| Placeholder..." name="view_by_status" value={this.state.filterVal || ''}
                                 onChange={(e) => this.searchData( 'filterVal', e.target.value )}  />
                                  <button  type="submit"><span className="icon icon-search1-ie"></span></button>
                                  {
                                    // <Select name="view_by_status "
                                    //     required={true} simpleValue={true}
                                    //     searchable={true} Clearable={false}
                                    //     placeholder=""
                                    //     options={this.state.searchItems}
                                    //     onChange={(e) => this.setState('filterVal', e )}
                                    //     value={this.state.filterVal}
                                    //
                                    // />
}
                                </div>
                                </div>

                            </div>

                        {// <div className='col-xl-3 col-lg-5 col-5 mr_b_15_min'>
                        //
                        //     <div className='cstm_select  slct_cstm2 bg_grey'>
                        //
                        //         <Select name="view_by_status "
                        //             simpleValue={true}
                        //             searchable={false} Clearable={false}
                        //             placeholder="Status"
                        //             options={options}
                        //             onChange={(e) => this.searchData( 'categoryVal', e )}
                        //             value={this.state.categoryVal}
                        //
                        //         />
                        //
                        //     </div>
                        //
                        // </div>
                      }

                            <div className='col-xl-3 col-lg-3 col-3'>
                                <div className='d-flex align-items-center'>
                                    <label className='pd_r_10'>From</label>
                                    <DatePicker
                                        autoComplete={'off'}
                                        className="csForm_control"
                                        selected={moment(this.state.fromDate).valueOf()}
                                        value={this.state.fromDate}
                                        onChange={(e) => this.searchData( 'fromDate', moment(e).format("YYYY-MM-DD") )}
                                        dateFormat="DD/MM/YYYY"
                                        name="tempDueDate"
                                        placeholderText={'DD/MM/YY'}
                                    />
                                </div>
                            </div>

                            <div className='col-xl-3 col-lg-3 col-3'>
                                <div className='d-flex align-items-center'>
                                    <label className='pd_r_10'>To</label>
                                    <DatePicker
autoComplete={'off'}
                                        className="csForm_control"
                                        selected={moment(this.state.toDate).valueOf()}
                                        value={this.state.toDate}
                                        onChange={(e) => this.searchData( 'toDate', moment(e).format("YYYY-MM-DD") )}
                                        dateFormat="DD/MM/YYYY"
                                        name="tempDueDate"
                                        placeholderText={'DD/MM/YY'}
                                        minDate={minDate}
                                    />
                                </div>
                            </div>

                        </div>


                        <div className="data_table_cmn dashboard_Table pending_invTable transFirst_tble text-center bor_top1 pd_tb_15" >
{
//                         <ReactTable
//      columns={hdrColumn}
//    data={this.props.auditLogs}
//
//    loading={this.state.loading}
//    onFetchData={this.fetchData}
//    className="-striped -highlight"
//    previousText={<span className="icon icon-arrow-1-left previous"></span>}
//    nextText={<span className="icon icon-arrow-1-right next"></span>}
//
// />
}
                            <ReactTable
							                 pageSizeOptions= {[10, 20, 30, 40, 50]}
                                manual='true'
                                pages={this.state.auditLogs_count}
                                data={this.props.auditLogs}
                                onFetchData={this.fetchData}
                                defaultPageSize={10}
                                onPageSizeChange={this.onPageSizeChange}
                                filtered={this.state.filtered}
                                minRows={2}
                                loading={this.state.loading}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                columns={hdrColumn}
                                showPagination={true}
                                SubComponent={(ev) =><div className="subComp AuditSubC">
                                  {(ev.original.action<=4)?
                                    <div className='row col-12 compLi'>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>Participant:</b>{ev.original.firstname}</div>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>Invoice Number:</b>{ev.original.invoice_number}</div>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>Biller Name:</b>{ev.original.company_name}</div>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>Invoice Amount:</b>{ev.original.invoice_amount}</div>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>Due Date:</b>{ev.original.due_date}</div>
                                      <div className=' col-lg-3 col-md-6 col-12'><b>{ev.original.title}:</b>{ev.original.date}</div>
                                    </div>
                                    :
                                    <div  className='row col-xl-12'>{ReactHtmlParser(ev.original.Description)}</div>
                                  }
                                    </div>
                                }
                            />
                        </div>
                        <div className="text-right">
                             <button className="btn hdng_btn cmn-btn1" download href={BASE_URL+this.state.file_url} target="_blank" onClick={() => this.csvDownload()}>Export CSV &nbsp;   &nbsp;<i className="icon icon-download2-ie"></i></button>
                            </div>
                        {/* table comp ends */}


            </AuthWrapper>

        );
    }
}


const mapStateToProps = state => {

return {  auditLogs: state.InvoiceReducer.auditLogs ,
csvDownload:state.InvoiceReducer.auditCsvDownload }
};



export default connect(mapStateToProps,{list_audit_logs,auditCsvDownload})(AuditLogs);
