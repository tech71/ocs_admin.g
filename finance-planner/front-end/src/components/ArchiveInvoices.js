import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { getInvoiceByType } from '../store/actions';

class ArchiveInvoices extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterVal: '',
      invoice_type: 6,
      search: ''
    }
  }

  onSubmitSearch = (e) => {
    e.preventDefault();
    this.setState({ filtered: { search: this.state.search } });
  }

  fetchData = (e) => {
    this.props.getInvoiceByType({ invoice_type: this.state.invoice_type, pageSize: e.pageSize, page: e.page, sorted: e.sorted, filtered: e.filtered });
  }

  render() {
    const { invoiceListing } = this.props;
    const hdrColumn = [
      { Header: 'HCM ID', accessor: 'ocisid' },
      { Header: 'NDIS ID', accessor: 'ndisid' },
      { Header: 'Last Name', accessor: 'lastname' },
      { Header: 'First Name', accessor: 'firstname' },
      { Header: 'Invoice Number', accessor: 'invoice' },
      { Header: 'Due Date', accessor: 'duedate' },
      { Header: 'Invoice Total', accessor: 'total' },
      { Header: 'Biller', accessor: 'biller' },
      {
        Header: 'HCM Status',
        accessor: 'status',
        sortable: false,
        filterable: false,
        Cell: (props) =>
          <div className='stat_grp'>
            {
              (props.original.status === 'duplicate') ? <span className='status duplicate'>Duplicate</span>
                :
                (props.original.status === 'inQueue') ? <span className='status inQueue'>In Queue</span>
                  :
                  (props.original.status === 'followUp') ? <span className='status followUp'>Follow Up</span>
                    : (props.original.status === 'Paid') ? <span className='status paid'>Follow Up</span>
                      : (props.original.status === 'Archive') ? <span className='status archive'>Archived</span>
                        : ''}

          </div>
      },
      {
        Header: '',
        accessor: 'id',
        sortable: false,
        filterable: false,
        width: 50,
        Cell: (props) =>
          <div>
            <Link to={'/archiveinvoice/viewarchiveinvoice/' + props.value}>
              <i className='icon icon-view see_ic'></i>
            </Link>
          </div>
      }

    ]

    return (

      <AuthWrapper>

        <div className=" back_col_cmn-">
          <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
        </div>

        <div className="main_heading_cmn-">
          <h1>
            <span>Archived Invoices</span>
          </h1>
        </div>

        <div className='row pd_t_15'>
          <div className='col-6'>
            <form onSubmit={this.onSubmitSearch}>
              <div className='cstm_select_fINacne'>
                <input type="text" placeholder="| Names, numbers, dates & status'" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
              </div>
            </form>
          </div>
        </div>


        <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
          <ReactTable
            loading = {this.props.loading}
            data={invoiceListing}
            defaultPageSize={10}
            filtered={this.state.filtered}
            minRows={2}
            manual="true"
            pages={this.props.pages}
            onPageSizeChange={this.onPageSizeChange}
            onFetchData={this.fetchData}
            className="-striped -highlight"
            previousText={<span className="icon icon-arrow-1-left previous"></span>}
            nextText={<span className="icon icon-arrow-1-right next"></span>}
            columns={hdrColumn}
            pageSizeOptions={[10, 20, 30, 40, 50]}
          />
        </div>


      </AuthWrapper>

    );
  }
}




const mapStateToProps = state => {
  return {
    invoiceListing: state.InvoiceReducer.invoiceListing,
    pages: state.InvoiceReducer.plannerPages,
     loading: state.Reducer1.loading

  }
};
export default connect(mapStateToProps, { getInvoiceByType })(ArchiveInvoices);

// export default FollowUpInvoices;
