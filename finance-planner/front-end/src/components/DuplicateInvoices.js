import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { getInvoiceByType } from '../store/actions';

class DuplicateInvoices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            checkAll: true,
            invoice_type: 7,
            search: ''
        }
    }

    onSubmitSearch = (e) => {
        e.preventDefault();
        this.setState({ filtered: { search: this.state.search, d_invoice_type: this.state.d_invoice_type } });
    }

    onfilter = (e) => {
        this.setState({ type: e, d_invoice_type: e }, () => {
            var searchData = { search: this.state.search, d_invoice_type: this.state.d_invoice_type };
            this.setState({ filtered: searchData });
        });

    }

    fetchData = (e) => {
        this.props.getInvoiceByType({ invoice_type: this.state.invoice_type, pageSize: e.pageSize, page: e.page, sorted: e.sorted, filtered: e.filtered });
    }

    render() {
        const { invoiceListing } = this.props;
        const { plannerData } = this.props;
        const hdrColumn = [
            { Header: 'HCM ID', accessor: 'ocisid' },
            { Header: 'NDIS ID', accessor: 'ndisid' },
            { Header: 'Last Name', accessor: 'lastname', Cell: (props) => <div>{props.original.lastname}</div> },
            { Header: 'First Name', accessor: 'firstname', Cell: (props) => <div>{props.original.firstname}</div> },
            { Header: 'Invoice Number', accessor: 'invoice', Cell: (props) => <div>{props.original.invoice}</div> },
            { Header: 'Due Date', accessor: 'duedate' },
            { Header: 'Invoice Total', accessor: 'total' },
            { Header: 'Biller', accessor: 'biller', Cell: (props) => <div>{props.original.biller}</div> },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                            (props.original.status === 'Duplicate') ? <span className='status duplicate'>Duplicate</span>
                                :
                                (props.original.status === 'inQueue') ? <span className='status inQueue'>In Queue</span>
                                    :
                                    (props.original.status === 'followUp') ? <span className='status followUp'>Follow Up</span>
                                        :
                                        (props.original.status === 'PotentialDuplicate') ? <span className='status duplicate'>Potential Duplicate</span>
                                            : ' '}

                    </div>
            },
            {
                Header: '',
                accessor: 'id',
                sortable: false,
                filterable: false,
                width: 50,
                Cell: (props) =>
                    <div>
                        <Link to={'/duplicateinvoices/viewduplicateinvoice/' + props.value}>
                            <i className='icon icon-view see_ic'></i>
                        </Link>
                    </div>
            }

        ]



        var options = [
            { value: '2', label: 'Potential Duplicate' },
            { value: '5', label: 'Duplicate' }
        ];


        return (

            <AuthWrapper>
                <div className=" back_col_cmn-">
                    <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Duplicate Invoices</span>
                        <Link to={'/duplicateinvoices/viewduplicateinvoice/' + (typeof (plannerData) != 'undefined' ? plannerData : '')}>
                            <button className="btn hdng_btn cmn-btn1">Run Planner</button>
                        </Link>
                    </h1>
                </div>

                <div className='row pd_t_15'>
                    <div className='col-6'>
                        <form onSubmit={this.onSubmitSearch}>
                            <div className='cstm_select_fINacne'>
                                <input type="text" placeholder="| Names, numbers, dates & status'" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                                <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
                            </div>
                        </form>
                    </div>

                    <div className='col-3'>
                        <div className='cstm_select  slct_cstm2 bg_grey'>
                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Filter By Status"
                                options={options}
                                onChange={(e) => this.onfilter(e)}
                                value={this.state.filterVal}

                            />
                        </div>
                    </div>
                </div>


                <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
                    <ReactTable
                        loading = {this.props.loading}
                        data={invoiceListing}
                        defaultPageSize={10}
                        filtered={this.state.filtered}
                        manual="true"
                        pages={this.props.pages}
                        onFetchData={this.fetchData}
                        minRows={1}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                        pageSizeOptions={[10, 20, 30, 40, 50]}
                    />
                </div>
                {/* table comp ends */}

            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
    return {
        invoiceListing: state.InvoiceReducer.invoiceListing,
        plannerData: state.InvoiceReducer.plannerData,
        pages: state.InvoiceReducer.plannerPages,
        loading: state.Reducer1.loading
    }
};
export default connect(mapStateToProps, { getInvoiceByType })(DuplicateInvoices);

// export default DuplicateInvoices;
