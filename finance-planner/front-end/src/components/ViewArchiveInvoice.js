import React, { Component } from 'react';
import { Link,Redirect } from 'react-router-dom';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";

import PdfViewer from './utils/PdfViewer';
import jQuery from "jquery";
import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import NotifyPopUp from './utils/NotifyPopUp';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { archiveInvoiceDetails, pdfData, loader,invoice_status,invoice_exsits } from '../store/actions';
import Modal from './utils/Modal';
import {getOptionsParticipant,getOptionsCompany} from '../service/common.js';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';

class ViewArchiveInvoices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false,
            paidPopUp: false,
            followPopUp: false, archiveInvoiceDetails: [],
            redirect:false,
            invoiceModal:false,redirect_duplicate:false,biller_name:'',
            validate: {
                company_name: { error: false, message: '' }, invoice_number: { error: false, message: '' }, participant_firstname: { error: false, message: '' },
                invoice_amount: { error: false, message: '' }, due_date: { error: false, message: '' }, invoice_date: { error: false, message: '' }, reference_no_error: { error: false, message: '' },
                billpay_error: { error: false, message: '' }, ref_no_error: { error: false, message: '' }, bsb_error: { error: false, message: '' }, account_name_error: { error: false, message: '' },
                account_no_error: { error: false, message: '' }
            }
        }

    }

    componentWillMount() {
        this.props.loader(true);
        this.props.archiveInvoiceDetails(this.props.match.params.id)
            .then(json => {
                this.setState({ archiveInvoiceDetails: json.data,p_type: json.data.payment_method, validate: json.data.validate },()=>this.props.loader(false));
                this.props.pdfData(json.data);
            });
    }

    onInvoiceNoChange = (invoice_no) =>{
      this.setState({invoiceModal:true});
    }


    render() {
      let participantId = (typeof(this.props.CrmParticipantId)!='undefined')? this.props.CrmParticipantId:[{}];

        if(this.state.redirect){
        return(  <Redirect to='/archiveinvoices'/>);
        }

        const invoice_details = this.state.archiveInvoiceDetails;
        let service_count = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.service_count.message : '';
        let biller_code_reference_no_error = (typeof (invoice_details.validate) != 'undefined') ? ((typeof (this.props.biller_Status) != 'undefined') ? this.props.biller_Status : invoice_details.validate.biller_code_reference_no_check.error) : false;

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        const html = '<div>Example HTML string</div>';



        return (

            <AuthWrapper>


                <div className=" back_col_cmn-">
                    <Link to='/archiveinvoice'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Invoice #{invoice_details.invoice_number}</span>
                             <button className="btn error_btn cmn-btn1"  style={{backgroundColor:"red",border:'red'}}>Archived</button>
                        {// <Link to='/pendinginvoices/viewpaidinvoice'>
                        // </Link>
                        // <Link to='/pendinginvoices'>
                        //     <button className="btn hdng_btn cmn-btn2">Exit</button>
                        // </Link>
                      }
                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-12 col-12'>

                        <PdfViewer />
                        <div className='clearfix'></div>

                      {  // <div className='mr_t_15 pd_tb_15 d-flex justify-content-center'>
                        //     <span className=' flwUp_btn frm_bt23' onClick={() => {this.status_update('followUp')   }}>Follow Up</span>
                        //     <button className=' cmn-btn3 frm_bt23' onClick={() => { this.status_update('Paid')}}  disabled={(parseInt(invoice_details.funds)>=parseInt(invoice_details.invoice_amount))?false:true} >Pay Invoice <i className='icon icon-accept-approve2-ie'></i></button>
                        // </div>
                      }


                    </div>


                      <div className='col-xl-5 col-lg-12 col-12'>
                          <form method="POST" id="pending_invoive">
                          <div className='fieldArea'>

                              <div className='mini_profile__ d-flex'>

                                  <div className='pro_ic1'>
                                      <i className='icon icon-userm1-ie'></i>
                                  </div>

                                  <div className='prf_cntPart'>
                                      <h4 className='cmn_clr1'><strong>{invoice_details.Fullname}</strong></h4>
                                      <div>
                                          <h5 className='avl_fnd'>
                                              <strong>Avail Funds :</strong>
                                              <span>${invoice_details.funds}</span>
                                          </h5>
                                      </div>

                                      <div className='d-flex align-items-center'>
                                      {    // <select className='cstmSlctHt stats_slct' >
                                          //     <option selected={(invoice_details.status==1)?"selected":""}>Active</option>
                                          //     <option selected={(invoice_details.status==0)?"selected":""}>Inactive</option>
                                          // </select>
                                        }
                                          <div class="stat_grp"><span class="status val">{(invoice_details.status==1)?'Active':'Inactive'}</span></div>

                                          {// <label className='cstmChekie clrTpe2'>
                                          //     <input type="checkbox" className=" " />
                                          //     <div className="chkie"></div>
                                          // </label>
                                         }

                                      </div>


                                  </div>

                              </div>
                              {/* mini_profile__ ends */}

                              <div className='bor_tb  pd_tb_10' >
                                  <strong>Participant Details</strong>
                              </div>


                              <div className='row pd_tb_15'>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>HCM Mail Account</label>
                                          <div className='csaddOn_fld1__ '>
                                              <p> {invoice_details.participant_ocs_email_acc || ''} </p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>HCM ID</label>
                                          <div className='csaddOn_fld1__'>
                                            <p>{typeof(invoice_details.participant_ocs_id)!='undefined'?invoice_details.participant_ocs_id.value:''}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>First Name</label>
                                          <div className='csaddOn_fld1__'>
                                              <div className='cstm_select search_filter grnbrdr_slct'>
                                              <p>{typeof(invoice_details.participant_firstname)!='undefined'?invoice_details.participant_firstname.label:''}</p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Match System Data</label>
                                          <p>{this.state.validate.participant_firstname.error?'No':'Yes'}</p>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Last Name</label>
                                          <div className='csaddOn_fld1__'>
                                              <div className='cstm_select search_filter grnbrdr_slct'>
                                              <p>{typeof(invoice_details.participant_lastname)!='undefined'?invoice_details.participant_lastname.label:''}</p>
                                                  </div>
                                          </div>
                                      </div>
                                  </div>


                              </div>
                              {/* row ends */}

                              <div className='bor_tb  pd_tb_10' >
                                  <strong>Account Details</strong>
                              </div>

                              <div className='row pd_tb_15'>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Invoice Received Date</label>
                                          <div className='datewid_100'>
                                            <p>{invoice_details.invoice_date}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Due Date</label>
                                          <div className='datewid_100'>
                                            <p>{invoice_details.due_date}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Company Name</label>
                                          <div className='csaddOn_fld1__ '>
                                            <p>{invoice_details.company_name!=''?invoice_details.company_name:'N/A'}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Matches Xero Supplier</label>
                                            <p>{this.state.validate.company_name.error?'No':'Yes'}</p>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Invoice No.</label>
                                          <div className='csaddOn_fld1__ '>
                                              <p>{invoice_details.invoice_number!=''?invoice_details.invoice_number:'N/A'}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Known Biller</label>
                                              <p>{this.state.validate.invoice_number.error?'No':'Yes'}</p>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Invoice Amount</label>
                                          <div className='csaddOn_fld1__ '>
                                              <p>{invoice_details.invoice_amount}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Sufficient Available Balance</label>
                                              <p>{this.state.validate.invoice_amount.error?'No':'Yes'}</p>
                                      </div>
                                  </div>

                              </div>
                              {/* row ends */}


                              <div className='bor_tb_black cmn_clr1  pd_tb_10 mr_t_15' >
                                  <strong>Payment Method</strong>
                              </div>

                              <div className='row pd_tb_15' >


                                  <div className="col-lg-12 col-12">
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Please select one</label>
                                      </div>
                                  </div>
                                  <div className="col-lg-12 col-12">
                                      <div className="csform-group">
                                          <div className="custom_radio label_l bpay_lab">
                                              <input type="radio" readOnly name="radio-group" value={1} checked={(this.state.p_type == 1) ? true : false} />
                                              <label for="test1" ><strong>Bpay</strong></label>
                                          </div>
                                      </div>
                                  </div>

                                  <div className={(this.state.p_type == 1) ? 'col-lg-12 col-12 show' : 'col-lg-12 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Biller Name</label>
                                          <div className='csaddOn_fld1__ '>
                                              <p>{this.state.biller_name!=''?this.state.biller_name:'N/A'}</p>
                                          </div>
                                      </div>
                                  </div>

                                  <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Biller Code</label>
                                            <p>{invoice_details.biller_code!=''?invoice_details.biller_code:'N/A'}</p>
                                      </div>
                                  </div>

                                  <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Reference Number</label>
                                          <p>{invoice_details.reference_no!=''?invoice_details.reference_no:'N/A'}</p>
                                      </div>
                                  </div>
                                  <div className="col-lg-12 col-12">
                                      <div className="csform-group">
                                          <div className="custom_radio label_l bpay_lab">
                                              <input type="radio" readOnly name="radio-group" value={2} checked={(this.state.p_type == 2) ? true : false} />
                                              <label for="test1" ><strong>Aus Post</strong></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Billpay Code</label>
                                          <p>{(invoice_details.billpay_code)!=''?invoice_details.billpay_code:'N/A'}</p>

                                      </div>
                                  </div>

                                  <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Reference Number</label>
                                            <p>{invoice_details.ref_no!=''?invoice_details.ref_no:'N/A'}</p>

                                      </div>
                                  </div>
                                  <div className="col-lg-12 col-12">
                                      <div className="csform-group">
                                          <div className="custom_radio label_l bpay_lab">
                                              <input type="radio" readOnly name="radio-group" value={3} checked={(this.state.p_type == 3) ? true : false} />
                                              <label for="test1" ><strong>Bank</strong></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>BSB</label>
                                          <p>{invoice_details.bsb!=''?invoice_details.bsb:'N/A'}</p>

                                      </div>
                                  </div>
                                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Account Name</label>
                                          <p>{invoice_details.acc_name!=''?invoice_details.acc_name:'N/A'}</p>

                                      </div>
                                  </div>

                                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Account Number</label>
                                          <p>{invoice_details.acc_no!=''?invoice_details.acc_no:'N/A'}</p>

                                      </div>
                                  </div>

                              </div>




                                </div>

                          <div className={'processModal ' + (this.state.processModal ? 'show' : '')}>
                              <div className='modalDialog1'>
                                  <h4>Document Awaiting processing click to process now</h4>
                                  <button className='cmn-btn3 scn_btn'>Scan Now</button>
                              </div>
                          </div>



                      </form></div>

                    {/* col-6 ends */}

                </div>
                {/* row ends */}


            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
    return {
        invoice_details: state.InvoiceReducer.archiveInvoiceDetails,
        pdfData: state.InvoiceReducer.data,
          CrmParticipantId: state.InvoiceReducer.CrmParticipantId
        // InvoiceStatus:InvoiceStatus

    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    archiveInvoiceDetails, pdfData, loader,invoice_status,
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewArchiveInvoices);
