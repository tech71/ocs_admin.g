import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { getInvoiceByType } from '../store/actions';


class AllInvoices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            checkAll: true,
            invoicedetails: [],
            length: 0,
            type: 0,
            search: ''
        }

    }

    onSubmitSearch = (e) => {
        e.preventDefault();
        this.setState({ filtered: { search: this.state.search, a_invoice_type: this.state.filterVal}});
    }
    
    onfilter = (e) => {
        this.setState({type: e, filterVal: e}, () => {
            this.setState({ filtered: {search: this.state.search, a_invoice_type: this.state.filterVal}});
        });
    }

    fetchData = (e) => {
        this.props.getInvoiceByType({ invoice_type: this.state.invoice_type, pageSize: e.pageSize, page: e.page, sorted: e.sorted, filtered: e.filtered });
    }

    render() {
        const { invoiceListing } = this.props;

        const hdrColumn = [
            { Header: 'HCM ID', accessor: 'ocisid' },
            { Header: 'NDIS ID', accessor: 'ndisid' },
            {
                Header: 'Last Name',
                accessor: 'lastname',
                Cell: (props) => <div>{props.original.lastname}</div>
            },
            {
                Header: 'First Name',
                accessor: 'firstname',
                Cell: (props) => <div>{props.original.firstname}</div>
            },
            { Header: 'Invoice Number', accessor: 'invoice', Cell: (props) => <div>{props.original.invoice}</div> },
            { Header: 'Due Date', accessor: 'duedate' },
            { Header: 'Invoice Total', accessor: 'total' },
            {
                Header: 'Biller',
                accessor: 'biller',
                Cell: (props) => <div>{props.original.biller}</div>
            },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                            (props.original.status === 'Duplicate') ? <span className='status duplicate'>Duplicate</span>
                                :
                                (props.original.status === 'inQueue') ? <span className='status inQueue'>In Queue</span>
                                    :
                                    (props.original.status === 'followUp') ? <span className='status followUp'>Follow Up</span>
                                        :
                                        (props.original.status === 'PotentialDuplicate') ? <span className='status duplicate'>Potential Duplicate</span>
                                            : (props.original.status === 'Approved') ? <span className='status paid'>Approved</span> : ''}

                    </div>
            },
            {
                Header: '',
                accessor: 'ocisid',
                sortable: false,
                filterable: false,
                width: 50,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                            (props.original.status === 'Duplicate') ? <Link to={'/duplicateinvoices/viewduplicateinvoice/' + props.original.id}> <i className='icon icon-view see_ic'></i></Link>
                                :
                                (props.original.status === 'PotentialDuplicate') ? <Link to={'/duplicateinvoices/viewduplicateinvoice/' + props.original.id}> <i className='icon icon-view see_ic'></i></Link>
                                    :
                                    (props.original.status === 'inQueue') ? <Link to={'/pendinginvoices/viewpendinginvoice/' + props.original.id}><i className='icon icon-view see_ic'></i></Link>
                                        :
                                        (props.original.status === 'followUp') ? <Link to={'/followup/viewfollowup/' + props.original.id}><i className='icon icon-view see_ic'></i></Link>
                                            : (props.original.status === 'Approved') ? <Link to={'/approvedinvoices'}><i className='icon icon-view see_ic'></i></Link>
                                                : ' '
                        }

                    </div>
            }

        ]

        var options = [
            {value: 0, label: 'Filter By Status'},
            { value: '4', label: 'Pending' },
            { value: '1', label: 'Approved' },
            { value: '2', label: 'Potential Duplicate'},
            { value: '5', label: 'Duplicate' },
            { value: '3', label: 'Follow Up' }
        ];


        return (

            <AuthWrapper>

                <div className=" back_col_cmn-">
                    <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>All Invoice</span>
                    </h1>
                </div>

                <div className='row pd_t_15'>
                    <div className='col-6'>
                        <form onSubmit={this.onSubmitSearch}>
                            <div className='cstm_select_fINacne'>
                                <input type="text" placeholder="| Names, numbers, dates & status'" name="search" value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                                <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
                            </div>
                        </form>
                    </div>

                    <div className='col-3'>
                        <div className='cstm_select  slct_cstm2 bg_grey'>
                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Filter By Status"
                                options={options}
                                onChange={(e) => this.onfilter(e)}
                                value={this.state.filterVal}
                            />

                        </div>
                    </div>
                </div>
                
                <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
                    <ReactTable
                        loading = {this.props.loading}
                        data={invoiceListing}
                        manual="true"
                        filtered={this.state.filtered}
                        defaultPageSize={10}
                        minRows={2}
                        pages={this.props.pages}
                        className="-striped -highlight"
                        onFetchData={this.fetchData}
                        previousText={<span className="icon icon-arrow-1-left previous" ></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                        pageSizeOptions={[10, 20, 30, 40, 50]}
                    />
                </div>
                


            </AuthWrapper>

        );
    }

}
const mapStateToProps = state => {
    return {
        invoiceListing: state.InvoiceReducer.invoiceListing,
        pages: state.InvoiceReducer.plannerPages,
        loading: state.Reducer1.loading

    }
};
export default connect(mapStateToProps, { getInvoiceByType })(AllInvoices);
