import React, { Component } from 'react';
import ReactTable from "react-table";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { getAllDisputeNotes } from '../../store/actions';

class ListDisputeNotes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: [],
        }
    }

    componentWillMount() {
        this.props.getAllDisputeNotes(this.props.invoice_id);
    }

    editNotes = () => {
        
    }

    toggleSelectAll = () => {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.props.allDisputeNotes.forEach(x => {
                newSelected[x.notes_id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    render() {
      
        const hdrColumn = [
            {
                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input
                            type="checkbox"
                            className=" "
                            checked={this.state.selectAll === 1}
                            ref={input => {
                                if (input) {
                                    input.indeterminate = this.state.selectAll === 2;
                                }
                            }}
                            onChange={
                                () => this.toggleSelectAll()}
                        />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input
                            type="checkbox"
                            className=" "
                            checked={this.state.selected[props.original.notes_id] === true}
                            onChange={() => this.toggleRow(props.original.notes_id)}
                        />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Type', accessor: 'contact_method_name' },
            { Header: 'Due Date', accessor: 'raised' },
            { Header: 'User', accessor: 'contact_name' },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 40,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }

        ]


        return (
            <div className='disNotBox1__'>
                <div className='hdngDs'>Dispute Notes</div>
                <div className="data_table_cmn text-center dispute_table " >
                    <ReactTable
                        data={this.props.allDisputeNotes}
                        showPagination={false}
                        defaultPageSize={10}
                        minRows={2}
                        className="-striped -highlight"
                        previousText={< span className="icon icon-arrow-1-left previous" > </span>}
                        nextText={< span className="icon icon-arrow-1-right next" > </span>}
                        columns={hdrColumn}
                        SubComponent={(row) => <SubComponentDataMapper row={row.original} editNotes={this.props.editDisputeNotesFunction} />}
                    />
                </div>

            </div>

        );
    }
}




const mapStateToProps = state => {
    return {
        allDisputeNotes: state.InvoiceReducer.disputeNotes,
        editDisputeNotesFunction: state.InvoiceReducer.editDisputeNotesFunction,
    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    getAllDisputeNotes
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ListDisputeNotes);


class SubComponentDataMapper extends Component {

    render() {

    return (
        <div className='dispSubComp__'>
            <p>{this.props.row.notes}
                <i className='icon icon-view2-ie  vw_dips' style={{ marginRight: '3px' }} onClick={() => this.props.editNotes(this.props.row)}></i>
            </p>
        </div>
    );
}
}
