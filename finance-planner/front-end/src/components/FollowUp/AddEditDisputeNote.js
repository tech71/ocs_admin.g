import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import Select from 'react-select-plus';
import moment from 'moment';
import { addMonths } from 'date-fns';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {  getDisputeContactOption, setEditDisputeNotesFunction, createDisputeNotes, getAllDisputeNotes} from '../../store/actions';
import { handleChangeChkboxInput } from '../../service/common.js';
import jQuery from "jquery";



class AddEditDisputeNote extends Component {

  constructor(props) {
    super(props);
    this.state = {
      raised: '',
      notes: '',
      invoice_id: this.props.invoice_id,
    }
  }

  editDisputeNotes = (notesData) => {
      console.log(notesData);
    this.setState(notesData);
  }

  componentDidMount() {
    this.props.setEditDisputeNotesFunction(this.editDisputeNotes);
    this.props.getDisputeContactOption();
  }

  onHandleDispute = (e) => {
    e.preventDefault();

    var validator = jQuery("#dispute").validate({ignore: ".ignore"})
    if (jQuery("#dispute").valid()) {
      this.props.createDisputeNotes(this.state).then(res => {
        if (res.status) {
          this.setState({ reason: '', raised: '', contact_name: '', contact_method: '', notes: '', invoice_id: this.props.invoice_id, notes_id: '' });
          this.props.getAllDisputeNotes(this.props.invoice_id);
        }
      });
    }
  }

  selectContactMethod = (value) => {
    this.setState({ contact_method: value })
  }

  render() {

    let alldisputeErrors = this.props.disputeErrorMsg;

    return (
      <form id="dispute" method="POST">
        <div className='disputeDetl_box'>
          <b className="alert-success">{(this.props.disputeSuccessMsg) ? this.props.disputeSuccessMsg : ''}</b>
          <div className='bor_tb  pd_tb_10 mr_t_15' >
            <strong>Dispute Details   </strong>
          </div>

          <div className='row pd_tb_15'>

            <div className='col-lg-6 col-12'>
              <div className="csform-group">
                <label className='inpLabel1'>Reason for Dispute</label>
                <div className='csaddOn_fld1__'>
                  <input type="text" name="reason"
                    onChange={(evt) => handleChangeChkboxInput(this, evt)}
                    value={this.state.reason || ''}
                    className="csForm_control no_bor" maxLength="30" data-rule-required="true" data-rule-pattern="([a-z]$)" pattern="/^[a-zA-Z0-9, ';$]+$/" />
                </div>
              </div>
            </div>

            <div className='col-lg-6 col-12'>
              <div className="csform-group">
                <label className='inpLabel1'>Dispute Raised</label>
                <div className='csaddOn_fld1__'>
                  <DatePicker
                    className="csForm_control  no_bor"
                    selected={moment(this.state.raised).valueOf()}
                    onChange={(e) => this.setState({ raised: e })}
                    dateFormat="dd/MM/yyyy"
                    name="raised"
                    placeholderText={'DD/MM/YYYY'}
                    maxDate={addMonths(new Date(), 3)}
                    minDate={addMonths(new Date(), -3)}
                    autoComplete={'off'}
                    required={true}
                  />
                </div>
              </div>
            </div>

            <div className='col-lg-6 col-12'>
              <div className="csform-group">
                <label className='inpLabel1'>Contact Name at Provider</label>
                <div className='csaddOn_fld1__'>
                  <input type="text" name="contact_name"
                    value={this.state.contact_name || ''}
                    onChange={(evt) => handleChangeChkboxInput(this, evt)}
                    className="csForm_control no_bor" maxLength="30" data-rule-required="true" pattern="/^[a-zA-Z0-9, ';$]+$/" />
                </div>
              </div>
            </div>

            <div className='col-lg-6 col-12'>
              <div className="csform-group">
                <label className='inpLabel1'>Contact Method</label>
                <div className='csaddOn_fld1__'>
                  <Select name="view_by_status default_validation"
                    simpleValue={true}
                    required={true}
                    searchable={true} clearable={false}
                    placeholder="| Email, Phone, Post & Message"
                    options={this.props.disputeContactOption}
                    onChange={(e) => this.selectContactMethod(e)}
                    value={this.state.contact_method}
                   
                  />
                </div>
                <label id="-error" class="error" for="" style={{display: "none"}}>This field is required.</label>
              </div>
            </div>

            <div className='col-lg-12 col-12'>
              <div className="csform-group">
                <label className='inpLabel1'>Dispute Notes</label>
                <div className='csaddOn_fld1__'>
                  <textarea className='csForm_control no_bor' name="notes"
                    onChange={(evt) => handleChangeChkboxInput(this, evt)}
                    value={this.state.notes || ''}
                    maxLength="1000" data-rule-required="true" pattern="/^[a-zA-Z0-9,';@() &-:.?$]+$/" minLength="8" ></textarea>
                  (Maximum characters: 1000) Count: {(this.state.notes) ? this.state.notes.length : '0'}
                </div>
              </div>
            </div>

            <div className='col-lg-12 col-12 text-right'>
              <input type='button' className="btn cmn-btn2 dis_btns" onClick={this.onHandleDispute} value={(this.state.notes_id) ? 'Edit Note' : 'Add Note'} />
              <input type='button' className="btn cmn-btn1 dis_btns" onClick={this.props.resolveFollowUp} value='Resolve' />
            </div>
          </div>

        </div>
      </form>

    );
  }
}




const mapStateToProps = state => {
  return {
    invoice_details: state.InvoiceReducer.invoicedetails,
    disputeSuccessMsg: state.InvoiceReducer.disputeSuccess,
    disputeErrorMsg: state.InvoiceReducer.disputeError,
    CrmParticipantId: state.InvoiceReducer.CrmParticipantId,
    disputeContactOption: state.InvoiceReducer.disputeContactOption,
    editDisputeNoteData: state.InvoiceReducer.editDisputeNoteData
  }
};

const mapDispatchtoProps = (dispatch) => bindActionCreators({
  createDisputeNotes,
  getAllDisputeNotes,
  getDisputeContactOption,
  setEditDisputeNotesFunction,
}, dispatch)

export default connect(mapStateToProps, mapDispatchtoProps)(AddEditDisputeNote);

