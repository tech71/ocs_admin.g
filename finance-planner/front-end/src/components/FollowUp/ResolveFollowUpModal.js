import React, { Component } from 'react';
import Modal from '../utils/Modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

class ResolveFollowUpModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <Modal show={this.props.showModel} >
        <div className='modalDialog_mini cstmDialog' >
          <div className='mr_b_15 hdngModal'>
            <h5 className='hdngCont'><strong>Resolve Follow Up </strong></h5>
            <i className='icon icon-close2-ie closeIc' onClick={this.props.closeModel}></i>
          </div>

          <div className='text-right mr_t_60'>
            <button className='btn cmn-btn2 dis_btns2 ' onClick={() => this.props.status_update('Archieve')}>
              <span>
                Deny Invoice<i className='icon icon-close2-ie'></i>
              </span>
            </button>
            <button className='btn cmn-btn1 dis_btns2' onClick={() => this.props.status_update('Paid')}>
              <span>Pay <i className='icon icon-accept-approve1-ie' ></i>
              </span>
            </button>

          </div>
        </div>

      </Modal>
    );
  }
}




const mapStateToProps = state => {
  return {

  }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({

}, dispatch)

export default connect(mapStateToProps, mapDispatchtoProps)(ResolveFollowUpModal);
