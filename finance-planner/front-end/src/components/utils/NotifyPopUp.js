import React from 'react';


const NotifyPopUp = (props) => {
    return (
        <div className={'notifyPop' + ' ' + (props.show ? 'show' : null) + ' ' + (props.notifyColor)}>
            <div className='notiFyPopBox'>
                <div className='notiContD'>
                    <div className='contBody'>
                        <i className={'icon txt_clr catIcPop' + ' ' + props.iconLef}></i>
                        <div className='msg'>
                            {props.children}
                        </div>
                      {  // <button className='btn btn-primary btn_clr notiBtn'>{props.btnName}</button>
                      }
                    </div>
                    <i className='icon icon-close notiFyClose txt_clr' onClick={props.Close}></i>
                </div>
            </div>
        </div>
    );
}


export default NotifyPopUp;
