import React from 'react';

import { connect } from 'react-redux';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

const Loader = (props) => {

        let loader = props.loading;
        let viewStatus = props.viewStatus;
        if(loader && viewStatus){
            return (
                <div className='MainLoader'>
                    <BlockUi tag="div" blocking={props.loading}  />
                </div>
            );
        }
        else{
            return null;
        }
        
}

const mapStateToProps = state => {
    return {
        loading:state.Reducer1.loading
    }
};

export default connect(mapStateToProps, null)(Loader);