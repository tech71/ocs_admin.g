import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Sidebar extends Component {


    constructor(props) {
        super(props);
        this.state = {
           fixedSidebarState:false,
           drop1:false,
           drop2:false
        }
    }
    // componentDidMount() {
    //     window.addEventListener('scroll', this.handleScroll);
    // }

    // handleScroll = (e) => {
    //     const currentScrollTop = window.pageYOffset;
    //     if(currentScrollTop <=55){
    //         this.setState({fixedSidebarState:false})
    //     }
    //     else{this.setState({fixedSidebarState:true})}

    //   }

    render() {



        return (
            <aside className={'main_sidebar__ ' + (this.state.fixedSidebarState? 'fixed': '')}>
                <div className='sideHead1__'>
                    Invoice Management
                </div>
                <ul className='sideNavies__'>
                    <li>
                        <NavLink to={'/dashboard'}>Dashboard</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/pendinginvoices'}>Pending Invoices</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/approvedinvoices'}>Approved Invoices</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/allinvoices'}>All Invoices</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/duplicateinvoices'}>Duplicate Invoices</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/followup'}>Follow Up</NavLink>
                    </li>
                  {// <li>
                  //     <NavLink to={'/reporting'}>Reporting</NavLink>
                  // </li>
                }
                    <li>
                        <NavLink to={'/synclogs'}>Sync Logs</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/auditlogs'}>Audit Logs</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/archiveinvoice'}>Archived Invoices</NavLink>
                    </li>
                    <li>
                        <NavLink to={'/importCSV'}>Import CSV</NavLink>
                    </li>
{/*
                  <li className='dropdownMenu'>
                        <div className={'drpHdng' + ' ' + (this.state.drop1?'open':'')} onClick={()=> this.setState({drop1:!this.state.drop1})}>
                            <span><strong>Dropdown 1</strong></span>
                            <i className='icon icon-arrow-right'></i>
                        </div>
                        <ul>
                            <li><NavLink to={'#'}>Option 1 </NavLink></li>
                            <li><NavLink to={'#'}>Option 2 </NavLink></li>
                            <li><NavLink to={'#'}>Option 3 </NavLink></li>
                        </ul>
                    </li> */}
  {/*
                    <li className='dropdownMenu'>
                        <div className={'drpHdng' + ' ' + (this.state.drop2?'open':'')} onClick={()=> this.setState({drop2:!this.state.drop2})}>
                            <span><strong>Dropdown 2</strong></span>
                            <i className='icon icon-arrow-right'></i>
                        </div>
                        <ul>
                            <li><NavLink to={'#'}>Option 1 </NavLink></li>
                            <li><NavLink to={'#'}>Option 2 </NavLink></li>
                            <li><NavLink to={'#'}>Option 3 </NavLink></li>
                        </ul>
                    </li> */}

                </ul>

                {this.props.children}

            </aside>

        );
    }
}



export default Sidebar;
