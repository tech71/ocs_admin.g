import React from 'react';



const Modal = (props) => {
    return (
        <div className={'cstmModal ' + (props.show ? ' show' : ' ')}  >
            {props.children}
        </div>

    );
}


export default Modal;
