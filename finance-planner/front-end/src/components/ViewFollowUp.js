import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import DatePicker from "react-datepicker";

import Select from 'react-select-plus';
import NotifyPopUp from './utils/NotifyPopUp';
import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import Modal from './utils/Modal';
import AddCompany from './AddCompany';
import AddEditDisputeNote from './FollowUp/AddEditDisputeNote';
import ListDisputeNotes from './FollowUp/ListDisputeNotes';
import ResolveFollowUpModal from './FollowUp/ResolveFollowUpModal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader, invoice_status, invoice_exsits, createDisputeNotes, getAllDisputeNotes, getOptionsCrmParticipantId, biller_refernce_check, getNewDate } from '../store/actions';
import { getOptionsParticipant, getOptionsCompany } from '../service/common.js';
import PdfViewer from './utils/PdfViewer';
import jQuery from "jquery";
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';


class ViewFollowUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterVal: '', selected: '',
      startDate: null,
      processModal: false,
      invoicedetails: [], invoicedetails: [],
      disputeDetails: { reason: '', raised: '', provider: '', method: '', notes: '', invoice_id: this.props.match.params.id, notes_id: '' },
      countLength: '', p_type: 0,
      setup_company: false, setup_company: false, biller_name: '',
      validate: {
        company_name: { error: false, message: '' }, invoice_number: { error: false, message: '' }, participant_firstname: { error: false, message: '' },
        invoice_amount: { error: false, message: '' }, due_date: { error: false, message: '' }, invoice_date: { error: false, message: '' }, reference_no_error: { error: false, message: '' },
        billpay_error: { error: false, message: '' }, ref_no_error: { error: false, message: '' }, bsb_error: { error: false, message: '' }, account_name_error: { error: false, message: '' },
        account_no_error: { error: false, message: '' }, biller_code_reference_no_check: { error: false, message: '' }
      }
    }
  }


  componentWillMount() {
    this.props.loader(true);
    this.props.getOptionsCrmParticipantId();
    let invoicedetails = [];
    this.props.invoiceDetails(this.props.match.params.id)
      .then(json => {
        invoicedetails = json.data;
        invoicedetails['company_name'] = { label: json.data.company_name, value: json.data.company_name }
        this.setState({ invoicedetails: invoicedetails, biller_name: invoicedetails.biller_name, p_type: json.data.payment_method, validate: invoicedetails.validate }, () => this.props.loader(false));
        this.props.pdfData(json.data);

      });
    
  }
  
  reset_invoice = () => {
    var state = this.state.invoicedetails;
    state['invoice_number'] = '';
    this.setState(state)
  }

  selectChange = (selectedOption, fieldname) => {
    var state = this.state.invoicedetails;
    state[fieldname] = selectedOption;
    state[fieldname + '_error'] = false;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    today = dd + '/' + mm + '/' + yyyy;
    let previous_year = dd + '/' + mm + '/' + (yyyy - 1);
    var c_date = this.props.getNewDate(today);
    var p_date = this.props.getNewDate(previous_year);
    if (fieldname == 'invoice_date') {

      var s_date = this.props.getNewDate(state['invoice_date']);
      let state2 = this.state.invoicedetails.validate.invoice_date;
      if ((p_date <= s_date) && (s_date <= c_date)) {
        state2['error'] = false;
        state2['message'] = '';
      } else {
        state2['error'] = true;
        state2['message'] = 'Invalid Invoice Date,Invoice date is more than 1 year old OR invoice date is in the future';
      }
      this.setState(state2);
    }
    else if (fieldname == 'due_date') {
      var s_date = this.props.getNewDate(state['due_date']);
      let state2 = this.state.invoicedetails.validate.due_date;
      if ((c_date <= s_date)) {
        state2['error'] = false;
        state2['message'] = '';
      } else {
        state2['error'] = true;
        state2['message'] = 'Invalid Due Date, Invoice due date is in the past';
      }
      this.setState(state2);
    }
    else if (fieldname == 'biller_code') {
      let state3 = this.state.invoicedetails.validate.biller_code_reference_no_check;
      this.props.biller_refernce_check(state['biller_code'], state['reference_no']).
        then(json => {
          state3['error'] = json.status;
          state3['message'] = '';
          this.setState(state3);
          if (json.status) {
            this.setState({ biller_name: json.data })
          }
        })
      state[fieldname] = selectedOption;
    }
    else if (fieldname == 'invoice_amount') {
      const re = /^[0-9]*([.,][0-9]+)?$/;
      let state4 = this.state.validate.invoice_amount;
      if (selectedOption < 0) {
        state4['error'] = true;
        state4['message'] = 'Invoice Amount Should be greater than 0';
      } else if (!re.test(selectedOption)) {
        state4['error'] = true;
        state4['message'] = 'Invoice Amount Should be Number Only';
      }
      else if (this.state.invoicedetails.funds < this.state.invoicedetails.invoice_amount) {
        state4['error'] = true;
        state4['message'] = "Participant's balance " + this.state.invoicedetails.funds + " is not sufficient to clear the invoice amount " + this.state.invoicedetails.invoice_amount;
      }
      else {
        state4['error'] = false;
        state4['message'] = '';
      }
      this.setState(state4);
    }
    else {
      state[fieldname] = selectedOption;
    }
    this.setState(state);
  }

  custom_validation = () => {
    let status = true;
    let status2 = true;
    let status3 = true;
    let status4 = false;
    let p_type = this.state.p_type;
    let state1 = this.state.validate;
    state1['biller_code_reference_no_check']['message'] = ''; state1['reference_no_error']['message'] = ''; state1['billpay_error']['message'] = '';
    state1['ref_no_error']['message'] = ''; state1['bsb_error']['message'] = ''; state1['account_name_error']['message'] = '';
    state1['account_no_error']['message'] = '';
    switch (parseInt(p_type)) {
      case 1:
        let biller_code = typeof (this.state.invoicedetails.biller_code) != 'undefined' ? this.state.invoicedetails.biller_code : '';
        let ref = typeof (this.state.invoicedetails.reference_no) != 'undefined' ? this.state.invoicedetails.reference_no : '';
        // let ref = this.state.invoicedetails.reference_no;
        if (biller_code.length == 0 || biller_code.length > 15) {
          status = false;
          state1['biller_code_reference_no_check']['error'] = false;
          state1['biller_code_reference_no_check']['message'] = 'Invalid Biller Code';

        } else {
          state1['biller_code_reference_no_check']['error'] = true;
          state1['biller_code_reference_no_check']['message'] = '';
          status = true;
        }
        if (ref.length == 0 || ref.length > 15) {
          status2 = false;
          state1['reference_no_error']['error'] = false;
          state1['reference_no_error']['message'] = 'Reference Number should be in between 1 to 15';
        } else {
          state1['reference_no_error']['error'] = true;
          state1['reference_no_error']['message'] = '';
          status2 = true;
        }

        break;
      case 2:
        let billpay_code = typeof (this.state.invoicedetails.billpay_code) != 'undefined' ? this.state.invoicedetails.billpay_code : '';
        let refe = typeof (this.state.invoicedetails.ref_no) != 'undefined' ? this.state.invoicedetails.ref_no : '';
        if (billpay_code.length == 0 || billpay_code.length > 4) {
       
          status = false;
          state1['billpay_error']['error'] = false;
          state1['billpay_error']['message'] = "Billpay should be in between 1 to 4 ";
        } else {
          state1['billpay_error']['error'] = true;
          status = true;
          state1['billpay_error']['message'] = "";
        }
        if (refe.length == 0 || refe.length > 15) {
          state1['ref_no_error']['message'] = "Reference Number should be in between 1 to 15";
          status2 = false;
          state1['ref_no_error']['error'] = false;
        } else {
          state1['ref_no_error']['message'] = "";
          status2 = true;
          state1['ref_no_error']['error'] = true;
        }
        break;
      case 3:
        let bsb = this.state.invoicedetails.bsb;
        let acc_no = this.state.invoicedetails.acc_no;
        let acc_name = this.state.invoicedetails.acc_name;
        if (bsb.length == 0) {
          status = false;
          state1['bsb_error']['message'] = "BSB Code is required";
          state1['bsb_error']['error'] = false;
        } else {
          state1['bsb_error']['error'] = true;
          status = true;
          state1['bsb_error']['message'] = "";
        }
        if (acc_name.length == 0) {
          status2 = false;
          state1['account_name_error']['error'] = false;
          state1['account_name_error']['message'] = "Account Name is required";
        } else {
          state1['account_name_error']['error'] = true;
          status2 = true;
          state1['account_name_error']['message'] = "";
        }
        const re = /^\d{3}-?\d{3}$/;
        if (!re.test(acc_no)) {
          status3 = false;
          state1['account_no_error']['error'] = false;
          state1['account_no_error']['message'] = "The length is 6 digits without dash or 7 digits with dash and contains only numbers. ex:123-123 or 123123";
        } else {
          state1['account_no_error']['error'] = true;
          state1['account_no_error']['message'] = "";
          status3 = true;
        }
        break;
      default: status4 = false;
        break;
    }
    if (status && status2 && status3)
      status4 = true;
    this.setState(state1)
    return status4;
  }

  invoice_validate = (invoice_number) => {
    if (invoice_number != '' && invoice_number != 'undefined') {
      this.props.invoice_exsits(this.props.match.params.id, invoice_number)
        .then(json => {
          if (json.status) {
            return new Promise((resolve, reject) => {
              confirmAlert({
                customUI: ({ onClose }) => {

                  return (
                    <Modal show={true} >
                      <div className='modalDialog_mini cstmDialog'>
                        <div className="hdngModal  pl-2">
                          <h5 className='hdngCont'><strong>Confirmation </strong></h5>
                        </div>
                        <div className="mb-3  pl-2">An Invoice with the same invoice number already exist in the system. Do you want to check the invoice in the Duplicate section?</div>
                        <div className="confi_but_div">
                          <button className="btn cmn-btn1 dis_btns2" onClick={
                            () => {
                              this.props.update_invoice_status(this.props.match.params.id, invoice_number)
                                .then(json => {
                                  if (json.status) {
                                    this.setState({ redirect_duplicate: true })
                                    onClose();
                                  }
                                })

                            }}>Yes
                                          {/* <i className='icon icon-accept-approve1-ie'></i> */}
                          </button>
                          <button className="btn cmn-btn2 dis_btns2" onClick={
                            () => {
                              onClose();
                              resolve({ status: false }); this.reset_invoice();
                            }}> No
                                            {/* <i className='icon icon-close2-ie'></i> */}
                          </button>
                        </div>
                      </div>
                    </Modal>
                  )
                }
              })
            }
            );
          }
        })
    }
  }
  selectChanges = (selectedOption, fieldname) => {

    var state = this.state.invoicedetails;
    let state2 = this.state.validate.participant_firstname;
    let state3 = this.state.validate.company_name;
    state[fieldname] = selectedOption;
    if (fieldname != 'company_name') {
      state['participant_ocs_email_acc'] = selectedOption.email;
      state['participant_lastname'] = { "label": selectedOption.lastname, 'value': selectedOption.lastname };
      state['participant_firstname'] = { "label": selectedOption.firstname, 'value': selectedOption.firstname };
      state['participant_ocs_id'] = { "label": selectedOption.value, 'value': selectedOption.value };
      state2['message'] = '';
      state2['error'] = false;
    } else {
      state['company_name'] = { "label": selectedOption.value, 'value': selectedOption.value };
      state3['message'] = '';
      state3['error'] = false;
    }


    this.setState(state);
    this.setState(state2);
    this.setState(state3);

  }

  selectSearch = (selectedOption, fieldname) => {
    var state = this.state.invoicedetails;
    var state2 = this.state.validate.participant_firstname;
    if (selectedOption != null) {
      state[fieldname] = selectedOption.name;
      state['participant_ocs_email_acc'] = selectedOption.email;
      state['participant_lastname'] = { "label": selectedOption.lastname, 'value': selectedOption.lastname }
      state['participant_firstname'] = { "label": selectedOption.value, 'value': selectedOption.label };
      state['participant_ocs_id'] = { "label": selectedOption.label, 'value': selectedOption.label };
      state2['error'] = false;
      state2['message'] = '';

    }
    else {
      state[fieldname] = '';
      state['participant_ocs_email_acc'] = '';
      state['participant_lastname'] = '';
      state['participant_firstname'] = '';
      state['participant_ocs_id'] = '';
      state2['error'] = false;
      state2['message'] = '';
    }
    this.setState(state2);
    this.setState(state);

  }
  componentDidCatch(error) {
    //console.log(error);
  }

  handleClose = () => {
    this.setState({ setup_company: false })
  }
  status_update = (status) => {
    let validation = this.custom_validation();
    let state = this.state.invoicedetails;
    state['payment_method'] = this.state.p_type;
    this.setState({ invoicedetails: state })
    switch (status) {
      case 'Paid':
        if (validation) {
          this.props.invoice_status(status, this.props.match.params.id, state)
            .then(json => {
              if (json.status) {
                this.setState({ paidPopUp: true, resolveFollowModal: false }, () => this.props.loader(false));
                setTimeout(() => this.setState({ paidPopUp: false, redirect: true }), 3000);
              }
              else {
                this.setState({ invoicedetails: json.data, validate: json.data.validate, resolveFollowModal: false }, () => this.props.loader(false))
                this.props.pdfData(json.data);
              }
            })
        }

        break;
      case 'Archieve':
        this.props.invoice_status(status, this.props.match.params.id, state)
          .then(json => {
            if (json.status) {
              this.setState({ ArchievePopUp: true, resolveFollowModal: false }, () => this.props.loader(false));
              setTimeout(() => this.setState({ ArchievePopUp: false }), 3000);
            }
            else {
              this.props.loader(false);
            }
          })
        break;
    }


  }

  onHandleDispute = (e) => {
    e.preventDefault();
    let custom_validation = this.custom_dispute_validation();
    if (jQuery("#dispute").valid() && custom_validation) {
      this.props.createDisputeNotes(this.state.disputeDetails).then(res => {
        if (res.status) {
          this.setState({ disputeDetails: { reason: '', raised: '', provider: '', method: '', notes: '', invoice_id: this.props.match.params.id, notes_id: '' } });
          this.props.getAllDisputeNotes(this.props.match.params.id);
        }
      });
    }

  }

  handleShareholderNameChange = (stateKey, fieldtkey, fieldValue) => {
    var state = {};
    var tempField = {};

    if (fieldtkey == 'notes') {
      var fieldLength = fieldValue.length.toString();
      this.setState({ countLength: fieldLength });
      if (fieldLength >= 1000) {
        var str = fieldValue.substring(0, 1000);
        this.setState({ countLength: str.length.toString() });
        fieldValue = str;
      }
    }
    var List = this.state[stateKey];
    List[fieldtkey] = fieldValue
    state[stateKey] = List;
    this.setState(state);
  }

  selectContactMethod = (selectedOption, fieldname) => {
    var state = this.state.disputeDetails;
    state[fieldname] = { 'label': selectedOption, 'value': selectedOption };
    state[fieldname + '_error'] = false;
    this.setState(state);
  }

  selectDisputeRaised = (selectedOption, fieldname) => {
    var state = this.state.disputeDetails;
    state[fieldname] = selectedOption;
    state[fieldname + '_error'] = false;
    this.setState(state);
  }

  companySuccess = (name) => {
    let state = this.state.invoicedetails;
    let state2 = this.state.invoicedetails.validate.company_name;
    state2['error'] = false;
    state2['message'] = '';
    state['company_name'] = { label: name, value: name }
    this.setState(state);
    this.setState(state2);
  }

  handleClose = () => {
    this.setState({ setup_company: false })
  }

  render() {

    var validationErrors = [];
    Object.values(this.state.validate).forEach(function (value) {
      validationErrors.push(value.message)
    });


    let participantId = (typeof (this.props.CrmParticipantId) != 'undefined') ? this.props.CrmParticipantId : [{}];
    if (this.state.redirect) {
      return (<Redirect to='/followup' />);
    }


    const invoice_details = this.state.invoicedetails;
    let service_count = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.service_count.message : '';
    let biller_code_reference_no_error = (typeof (invoice_details.validate) != 'undefined') ? ((typeof (this.props.biller_Status) != 'undefined') ? this.props.biller_Status : invoice_details.validate.biller_code_reference_no_check.error) : false;
   
    return (

      <AuthWrapper>

        <div className=" back_col_cmn-">
          <Link to='/followup'><span className="icon icon-back1-ie"></span></Link>
        </div>

        <div className="main_heading_cmn-">
          <h1>
            <span>Follow Up Invoice: #{this.state.invoicedetails.invoice_number}</span>
            <button className="btn hdng_btn cmn-btn4">Follow Up</button>
          </h1>
        </div>

        <div className='row'>

          <div className='col-xl-7 col-lg-12 col-12'>
            <PdfViewer />
          </div>

          <div className='col-xl-5 col-lg-12 col-12'>
            <div className='fieldArea pd_tb_15'>
              <div className='mini_profile__ d-flex'>

                <div className='pro_ic1'>
                  <i className='icon icon-userm1-ie'></i>
                </div>

                <div className='prf_cntPart'>
                  <h4 className='cmn_clr1'><strong>{invoice_details.Fullname}</strong></h4>
                  <div>
                    <h5 className='avl_fnd'>
                      <strong>Avail Funds :</strong>
                      <span>${invoice_details.funds}</span>
                    </h5>
                  </div>

                  <div className='d-flex align-items-center'>
                    {    // <select  className='cstmSlctHt stats_slct' disabled>
                      //     <option  selected={(invoice_details.status==1)?"selected":""}>Active</option>
                      //     <option  selected={(invoice_details.status==0)?"selected":""}>Inactive</option>
                      // </select>
                    }

                    <div className="stat_grp"><span className="status val">{(invoice_details.status == 1) ? 'Active' : 'Inactive'}</span></div>

                  </div>
                </div>
              </div>


              {/* mini_profile__ ends */}
              <div className='bor_tb  pd_tb_10' >
                <strong>Participant Details</strong>
              </div>


              <div className='row pd_tb_15'>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>HCM Mail Account</label>
                    <div className='csaddOn_fld1__ '>
                      <input type="text" className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" readOnly value={invoice_details.participant_ocs_email_acc || ''} />
                      <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>HCM ID</label>
                    <div className='csaddOn_fld1__ cssaddOn_select___'>
                      <Select name="view_by_status "
                        required={true} simpleValue={false}
                        searchable={true} Clearable={false}
                        placeholder=""
                        options={participantId}
                        onChange={(e) => this.selectSearch(e, 'participant_ocs_id')}
                        value={invoice_details.participant_ocs_id}

                      />
                      <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>First Name</label>
                    <div className='csaddOn_fld1__'>
                      <div className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn2" : "cstm_select search_filter grnbrdr_slct"}>
                        <Select.Async
                          cache={false}
                          clearable={false}
                          name="participant_firstname" required={true}
                          value={invoice_details.participant_firstname}
                          loadOptions={getOptionsParticipant}
                          placeholder='Search'
                          onChange={(e) => this.selectChanges(e, 'participant_firstname')}
                        />
                        <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Match System Data</label>
                    <div className={(this.state.validate.participant_firstname.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                      <input type="text" readOnly className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.validate.participant_firstname.error) ? 'No' : 'Yes'} />
                      <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Last Name</label>
                    <div className='csaddOn_fld1__'>
                      <div className='cstm_select search_filter grnbrdr_slct'>
                        <Select.Async
                          cache={false}
                          clearable={false}
                          name="participant_lastname" required={true}
                          value={invoice_details.participant_lastname}
                          loadOptions={getOptionsParticipant}
                          placeholder='Search'
                          onChange={(e) => this.selectChanges(e, 'participant_lastname')}
                        />
                        <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className='bor_tb  pd_tb_10 mr_t_15' >
                <strong>Account Details</strong>
              </div>

              <div className='row pd_tb_15'>
                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Invoice Received Date</label>
                    <div className={(this.state.validate.invoice_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                      <DatePicker
                        className={(this.state.validate.invoice_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                        selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                        value={invoice_details.invoice_date}
                        onChange={(e) => this.selectChange(moment(e).format("DD/MM/YYYY"), 'invoice_date')}
                        dateFormat="DD/MM/YYYY"
                        name="tempDueDate"
                        placeholderText={'DD/MM/YY'}
                        autoComplete={'off'}
                      />
                      <span className="error pd_tb_15">{this.state.validate.invoice_date.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Due Date</label>
                    <div className={(this.state.validate.due_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                      <DatePicker
                        className={(this.state.validate.due_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                        selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                        value={invoice_details.due_date}
                        onChange={(e) => this.selectChange(moment(e).format("DD/MM/YYYY"), 'due_date')}
                        dateFormat="DD/MM/YYYY"
                        name="tempDueDate"
                        placeholderText={'DD/MM/YY'}
                        autoComplete={'off'}
                      />
                      <span className="error pd_tb_15">{this.state.validate.due_date.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Company Name</label>
                    <div className={(this.state.validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__  filled'}>
                      {//<input type="text" className="csForm_control no_bor" name="" value={invoice_details.company_name || ''} onChange={(e) =>this.selectChange(e.target.value,'company_name')}/>
                      }  <Select.Async
                        cache={false}
                        clearable={false}
                        name="company_name" required={true}
                        value={invoice_details.company_name}
                        loadOptions={getOptionsCompany}
                        placeholder='Search'
                        onChange={(e) => this.selectChanges(e, 'company_name')}
                      />
                      <span className="error pd_tb_15">{this.state.validate.company_name.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Matches Supplier</label>
                    <div className={(this.state.validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                      <input type="text" readOnly className={(this.state.validate.company_name.error) ? "csForm_control no_bor errorIn" : "csForm_control no_bor completed"} name="" value={(this.state.validate.company_name.error) ? 'No' : typeof (invoice_details.company_name) != 'undefined' ? invoice_details.company_name.label : 'Yes'} />
                      <em>{service_count}</em>
                      {//Company name doesn't match with allowed list,{this.state.validate.company_name.error?<a onClick={()=>this.setState({setup_company:true})}><b>Click here</b></a>:''} to add in the allowed list.
                      }
                      {this.state.validate.company_name.error ? <a onClick={() => this.setState({ setup_company: true })}>Set up Company</a> : ''}

                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Invoice Amount</label>
                    <div className='csaddOn_fld1__'>
                      <input type="text" className={(this.state.validate.invoice_amount.error) ? "csForm_control errorIn2" : "csForm_control filled"} name="" value={invoice_details.invoice_amount || ''} onChange={(e) => this.selectChange(e.target.value, 'invoice_amount')} />
                      <span className="error pd_tb_15">{this.state.validate.invoice_amount.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Amount Paid</label>
                    <div className='csaddOn_fld1__'>
                      <input type="text" className="csForm_control no_bor" name="" readOnly value={invoice_details.amount_paid || ''} />
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>Invoice Number</label>
                    <div className='csaddOn_fld1__'>
                      <input type="text" className={(this.state.validate.invoice_number.error) ? "csForm_control errorIn2" : "csForm_control filled"} name="" onChange={(e) => { this.selectChange(e.target.value, 'invoice_number'); this.invoice_validate(e.target.value) }} value={invoice_details.invoice_number || ''} />
                      <span className="error pd_tb_15">{this.state.validate.invoice_number.message}</span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-12'>
                  <div className="csform-group">
                    <label className='inpLabel1'>PO Number</label>
                    <div className='csaddOn_fld1__ notAvail'>
                      <input type="text" className="csForm_control " name="" value='NA' readOnly />
                    </div>
                  </div>
                </div>

                <div className='col-12'>
                  <div className='bor_tb  pd_tb_10' >
                    <strong>Payment Method</strong>
                  </div>
                  <div className="col-lg-12 col-12">
                    <div className="csform-group">
                      <label className='inpLabel1'>Please select one</label>
                    </div>
                  </div>
                  <div className="col-lg-12 col-12">
                    <div className="csform-group">
                      <div className="custom_radio label_l bpay_lab">
                        <input type="radio" name="radio-group" value={1} checked={(this.state.p_type == 1) ? true : false} />
                        <label for="test1" onClick={() => this.setState({ p_type: 1 })}><strong>Bpay</strong></label>
                      </div>
                    </div>
                  </div>

                  <div className={(this.state.p_type == 1) ? 'col-lg-12 col-12 show' : 'col-lg-12 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Biller Name</label>
                      <div className='csaddOn_fld1__ '>
                        <p>{this.state.biller_name}</p>
                      </div>
                    </div>
                  </div>

                  <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Biller Code</label>
                      <div className={(biller_code_reference_no_error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" className={(biller_code_reference_no_error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="biller_code" value={(invoice_details.biller_code) ? invoice_details.biller_code : ''} onChange={(e) => this.selectChange(e.target.value, 'biller_code')} data-rule-required="true" data-rule-maxlength="15" maxLength="15" />
                        <span className="error pd_tb_15">{this.state.validate.biller_code_reference_no_check.message}</span>
                      </div>
                    </div>
                  </div>

                  <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Reference Number</label>
                      <div className={(this.state.validate.reference_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" className={(this.state.validate.reference_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="reference_no" value={(invoice_details.reference_no) ? invoice_details.reference_no : ''} onChange={(e) => this.selectChange(e.target.value, 'reference_no')} data-rule-required="true" data-rule-maxlength="15" maxLength="15" />
                        <span className="error pd_tb_15">{this.state.validate.reference_no_error.message}</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-12">
                    <div className="csform-group">
                      <div className="custom_radio label_l bpay_lab">
                        <input type="radio" name="radio-group" value={2} checked={(this.state.p_type == 2) ? true : false} />
                        <label for="test1" onClick={() => this.setState({ p_type: 2 })}><strong>Aus Post</strong></label>
                      </div>
                    </div>
                  </div>
                  <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Billpay Code</label>
                     
                      <div className={(this.state.validate.billpay_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" maxLength="4" value={(invoice_details.billpay_code) ? invoice_details.billpay_code : ''} onChange={(e) => this.selectChange(e.target.value, 'billpay_code')} className={(this.state.validate.billpay_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                        <span className="error pd_tb_15">{this.state.validate.billpay_error.message}</span>
                      </div>
                    </div>
                  </div>

                  <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Reference Number</label>
                      <div className={(this.state.validate.ref_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" maxLength="15" value={(invoice_details.ref_no) ? invoice_details.ref_no : ''} onChange={(e) => this.selectChange(e.target.value, 'ref_no')} className={(this.state.validate.ref_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                        <span className="error pd_tb_15">{this.state.validate.ref_no_error.message}</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 col-12">
                    <div className="csform-group">
                      <div className="custom_radio label_l bpay_lab">
                        <input type="radio" name="radio-group" value={3} checked={(this.state.p_type == 3) ? true : false} />
                        <label for="test1" onClick={() => this.setState({ p_type: 3 })}><strong>Bank</strong></label>
                      </div>
                    </div>
                  </div>
                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>BSB</label>
                      <div className={(this.state.validate.bsb_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" value={(invoice_details.bsb) ? invoice_details.bsb : ''} onChange={(e) => this.selectChange(e.target.value, 'bsb')} className={(this.state.validate.bsb_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                        <span className="error pd_tb_15">{this.state.validate.bsb_error.message}</span>
                      </div>
                    </div>
                  </div>
                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Account Name</label>
                      <div className={(this.state.validate.account_name_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="text" value={(invoice_details.acc_name) ? invoice_details.acc_name : ''} onChange={(e) => this.selectChange(e.target.value, 'acc_name')} className={(this.state.validate.account_name_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                        <span className="error pd_tb_15">{this.state.validate.account_name_error.message}</span>
                      </div>
                    </div>
                  </div>

                  <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                    <div className="csform-group">
                      <label className='inpLabel1'>Account Number</label>
                      <div className={(this.state.validate.account_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                        <input type="number" value={(invoice_details.acc_no) ? invoice_details.acc_no : ''} onChange={(e) => this.selectChange(e.target.value, 'acc_no')} className={(this.state.validate.account_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                        <span className="error pd_tb_15">{this.state.validate.account_no_error.message}</span>
                      </div>
                    </div>
                  </div>
                </div>

              </div>


              {  //     <div className='col-lg-6 col-12'>
                //         <div className="csform-group">
                //             <label className='inpLabel1'>Biller Code</label>
                //             <div className={(biller_code_reference_no_error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                //                 <input type="text" className={(biller_code_reference_no_error)?"csForm_control no_bor completed":"csForm_control no_bor  errorIn2"} name="" value={(invoice_details.biller_code)?invoice_details.biller_code:''}  onChange={(e) =>this.selectChange(e.target.value,'biller_code')}/>
                //             </div>
                //         </div>
                //     </div>
                //
                //     <div className='col-lg-6 col-12'>
                //         <div className="csform-group">
                //             <label className='inpLabel1'>Reference Number</label>
                //             <div className={(biller_code_reference_no_error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                //                 <input type="text" className={(biller_code_reference_no_error)?"csForm_control no_bor completed":"csForm_control no_bor errorIn2"} name="reference_no" value={(invoice_details.reference_no)?invoice_details.reference_no:''}  onChange={(e) =>this.selectChange(e.target.value,'reference_no')} data-rule-required="true" data-rule-maxlength="15"  data-rule-minlength="15"/>
                //             </div>
                //         </div>
                //     </div>
                //
                // </div>
              }  {/* row ends */}



            </div>
          </div>
          {/* col-6 ends */}

        </div>
        {/* row ends */}


        <div className='row '>
          <div className='col-xl-7 col-lg-12 col-12'>
            <AddEditDisputeNote invoice_id={this.props.match.params.id} resolveFollowUp={() => this.setState({resolveFollowModal: true})} />
          </div>

          <div className='col-xl-5 col-lg-12 col-12'>
            <ListDisputeNotes invoice_id={this.props.match.params.id} />
          </div>
        </div>
        {/* row ends */}


        <ResolveFollowUpModal showModel={this.state.resolveFollowModal} closeModel={() => this.setState({ resolveFollowModal: false })} status_update={this.status_update} />
        
        <NotifyPopUp
          notifyColor='green'
          show={this.state.paidPopUp}
          Close={() => { this.setState({ paidPopUp: false, redirect: true }) }}
          iconLef={'icon-approved2-ie'}
        // btnName={"Undo"}
        >

          Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> marked as paid

                </NotifyPopUp>


        <NotifyPopUp
          notifyColor='red'
          show={this.state.ArchievePopUp}
          Close={() => { this.setState({ followPopUp: false, redirect: true }) }}
          iconLef={"icon-error-icons"}
        // btnName={"Undo"}
        >

          Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> Archived

                </NotifyPopUp>
        <AddCompany show={this.state.setup_company} handleClose={this.handleClose} companySuccess={this.companySuccess} />

      </AuthWrapper>

    );
  }
}




const mapStateToProps = state => {
  //   console.log(state.InvoiceReducer.newDate);
  return {
    invoice_details: state.InvoiceReducer.invoicedetails,
    pdfData: state.InvoiceReducer.data,
    disputeSuccessMsg: state.InvoiceReducer.disputeSuccess,
    disputeErrorMsg: state.InvoiceReducer.disputeError,
    allDisputeNotes: state.InvoiceReducer.disputeNotes,
    CrmParticipantId: state.InvoiceReducer.CrmParticipantId,
    biller_Status: state.InvoiceReducer.billerStatus,
    getnewdate: state.InvoiceReducer.getnewdate
  }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
  invoiceDetails,
  pdfData,
  loader, invoice_status,
  invoice_exsits,
  createDisputeNotes,
  getAllDisputeNotes,
  getOptionsCrmParticipantId,
  biller_refernce_check,
  getNewDate
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewFollowUp);
