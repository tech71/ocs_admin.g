import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import DatePicker from "react-datepicker";
import moment from 'moment';
import NotifyPopUp from './utils/NotifyPopUp';
import AuthWrapper from '../hoc/AuthWrapper';
import Select from 'react-select-plus';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader, invoice_status, biller_refernce_check,invoice_exsits, getNewDate, getOptionsCrmParticipantId } from '../store/actions';
import PdfViewer from './utils/PdfViewer';
import jQuery from "jquery";
import { getOptionsCompany, getOptionsParticipant } from '../service/common.js';
import Modal from './utils/Modal';
import AddCompany from './AddCompany';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';

class ViewDuplicateInvoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false, invoicedetails: [], duplicateinvoice: [],
            redirect: false,
            inQueueModal: false,
            confirmBox: false,
            p_type: 0,setup_company:false,duplicate_p_type: 0,biller_name:'N/A',duplicate_biller_name:'N/A',
            validate:{ company_name:{error:false,message:''},invoice_number:{error:false,message:''},participant_firstname:{error:false,message:''},
              invoice_amount:{error:false,message:''},due_date:{error:false,message:''},invoice_date:{error:false,message:''},reference_no_error:{error:false,message:''},
              billpay_error:{error:false,message:''},ref_no_error:{error:false,message:''},bsb_error:{error:false,message:''},account_name_error:{error:false,message:''},
              account_no_error:{error:false,message:''},biller_code_reference_no_check:{error:false,message:''}
            },
            duplicate_validate:{ company_name:{error:false,message:''},invoice_number:{error:false,message:''},participant_firstname:{error:false,message:''},
              invoice_amount:{error:false,message:''},due_date:{error:false,message:''},invoice_date:{error:false,message:''},reference_no_error:{error:false,message:''},
              billpay_error:{error:false,message:''},ref_no_error:{error:false,message:''},bsb_error:{error:false,message:''},account_name_error:{error:false,message:''},
              account_no_error:{error:false,message:''},biller_code_reference_no_check:{error:false,message:''}
             }

        }
    }
    componentWillMount() {
        this.props.loader(true);
        let invoicedetails = [];
        let duplicateinvoice = [];
        this.props.getOptionsCrmParticipantId();
        this.props.invoiceDetails(this.props.match.params.id)
            .then(json => {
                duplicateinvoice = json.data;
                duplicateinvoice['company_name'] = { label: json.data.company_name, value: json.data.company_name };
                invoicedetails = json.data.original_invoce;
                invoicedetails['company_name'] = { label: json.data.original_invoce.company_name, value: json.data.original_invoce.company_name };
                this.setState({p_type:invoicedetails.payment_method,biller_name:invoicedetails.biller_name,duplicate_biller_name:duplicateinvoice.biller_name,duplicate_p_type:duplicateinvoice.payment_method, invoicedetails: invoicedetails, duplicateinvoice: duplicateinvoice,validate:invoicedetails.validate,duplicate_validate:duplicateinvoice.validate }, () => this.props.loader(false));
                this.props.pdfData(duplicateinvoice);
            });

    }
    selectChange = (selectedOption, fieldname) => {
        var state = this.state.duplicateinvoice;
        state[fieldname] = selectedOption;
        state[fieldname + '_error'] = false;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        today = dd + '/' + mm + '/' + yyyy;
        let previous_year = dd + '/' + mm + '/' + (yyyy - 1);
        var c_date = this.props.getNewDate(today);
        var p_date = this.props.getNewDate(previous_year);
        if(fieldname == 'invoice_date'){

          var s_date =  this.props.getNewDate(state['invoice_date']);
          let state2 = this.state.duplicate_validate.invoice_date;
           if((p_date <= s_date) && (s_date <= c_date) ){
                state2['error']=false;
                state2['message']='';
           }   else {
           state2['error']=true;
           state2['message']='Invalid Invoice Date,Invoice date is more than 1 year old OR invoice date is in the future';
         }
         this.setState(state2);
       }
       else if(fieldname == 'due_date'){
          var s_date =  this.props.getNewDate(state['due_date']);
         let state2 = this.state.duplicate_validate.due_date;
          if( (c_date <= s_date) ){
               state2['error']=false;
               state2['message']='';
          }   else {
          state2['error']=true;
          state2['message']='Invalid Due Date, Invoice due date is in the past';
        }
        this.setState(state2);
      }
        else if(fieldname=='biller_code' ){
          let state3 = this.state.duplicate_validate.biller_code_reference_no_check;
            this.props.biller_refernce_check(state['biller_code'],state['reference_no']).
            then(json => {
               state3['error'] = json.status;
               state3['message']='';
               this.setState(state3);
               if(json.status){
                 this.setState({duplicate_biller_name:json.data})
               }
            })
            state[fieldname] = selectedOption;
         }
         else if(fieldname=='invoice_amount'){
             const re = /^[0-9]*([.,][0-9]+)?$/;
           let state4 = this.state.duplicate_validate.invoice_amount;
           if(selectedOption < 0){
             state4['error']=true;
             state4['message'] ='Invoice Amount Should be greater than 0';
           }else if(!re.test(selectedOption)) {
             state4['error']=true;
             state4['message'] ='Invoice Amount Should be Number Only';
           } else if(this.state.duplicate_validate.funds<this.state.duplicate_validate.invoice_amount){
              state4['error']=true;
              state4['message'] ="Participant's balance "+this.state.duplicate_validate.funds+" is not sufficient to clear the invoice amount "+this.state.duplicate_validate.invoice_amount;
            }
           else{
             state4['error']=false;
             state4['message'] ='';
           }
           this.setState(state4);
         }
         else{
             state[fieldname] = selectedOption;
         }
        this.setState({duplicateinvoice:state});
    }
    selectChanges = (selectedOption, fieldname) => {
        var state = this.state.duplicateinvoice;
        var state2 = this.state.duplicate_validate.company_name;
        var state3 = this.state.duplicate_validate.participant_firstname;
        if(fieldname == 'company_name'){
          state[fieldname] = selectedOption;
            state2['error'] = false;
            state2['message']='';
            // this.setState({duplicate_validate:state2})
        }else{
        state[fieldname] = selectedOption;
        state['participant_ocs_email_acc'] = selectedOption.email;
        state['participant_lastname']= {"label":selectedOption.lastname,'value':selectedOption.lastname};
        state['participant_firstname']= {"label":selectedOption.firstname,'value':selectedOption.firstname};
        state['participant_ocs_id'] = { "label": selectedOption.value, 'value': selectedOption.value };
        state3['error'] = false;
        state3['message']='';
        // this.setState({duplicate_validate:state3})
      }
      this.setState({duplicateinvoice:state});
    }
    status_update = (e,status) => {
      e.preventDefault();
      let validation = this.custom_validation();
      let state = this.state.duplicateinvoice;
      state['payment_method'] = this.state.duplicate_p_type;
      this.setState({duplicateinvoice:state})
      if ((status == 'followUp' || status == 'Paid') && validation) {
            this.props.invoice_status(status, this.props.match.params.id, state)
                .then(json => {
                    if (json.status) {
                        if (status == 'followUp') {
                            this.setState({ followPopUp: true }, () => this.props.loader(false));
                            setTimeout(() => this.setState({followPopUp:false,redirect: true}), 3000);
                        } else {
                            this.setState({ paidPopUp: true }, () => this.props.loader(false));
                            setTimeout(() => this.setState({paidPopUp:false,redirect: true}), 3000);
                        }
                    }
                    else {
                        this.setState({ duplicateinvoice: json.data,duplicate_validate:json.data.validate }, () => this.props.loader(false));
                        this.props.pdfData(json.data);
                    }
                })
        }
        else if(status == 'Duplicate'){
            this.setState({ confirmBox: true })

        }

    }
    markDuplicate = () => {
        this.props.invoice_status('Duplicate', this.props.match.params.id, this.state.duplicateinvoice)
            .then(json => {
                if (json.status) {
                    this.setState({ Duplicate: true },()=>this.props.loader(false));
                    this.setState({ confirmBox: false })
                }
                else {
                    this.setState({ duplicateinvoice: json.data }, () => this.props.loader(false));
                    this.props.pdfData(json.data);
                    this.setState({ confirmBox: false })
                }
            })
    }
    selectSearch = (selectedOption, fieldname) => {

        var state = this.state.duplicateinvoice;
        state[fieldname] = selectedOption.name;
        state['participant_ocs_email_acc'] = selectedOption.email;
        state['participant_lastname']= {"label":selectedOption.lastname,'value':selectedOption.lastname}
        state['participant_firstname'] = { "label": selectedOption.value, 'value': selectedOption.label };
        state['participant_ocs_id'] = { "label": selectedOption.label, 'value': selectedOption.value };
        state[fieldname + '_error'] = false;

        this.setState({duplicateinvoice:state});

    }
    reset_invoice = () =>{
      var state = this.state.duplicateinvoice;
      state['invoice_number'] = '';
      this.setState(state)
    }
    invoice_validate = (invoice_number)=>{
      if(invoice_number !='' && invoice_number !='undefined')
        {
          this.props.invoice_exsits(this.props.match.params.id,invoice_number)
          .then(json=>{
          if(json.status){
            return new Promise((resolve, reject) => {
              confirmAlert({
                customUI: ({ onClose }) => {

                  return (
                          <Modal show={true} >
                              <div className='modalDialog_mini cstmDialog'>
                                <div className="hdngModal  pl-2">
                                  <h5 className='hdngCont'><strong>Confirmation </strong></h5>
                                </div>
                                                <div className="mb-3  pl-2">An Invoice with the same invoice number already exist in the system. Do you want to check the invoice in the Duplicate section?</div>
                                <div className="confi_but_div">
                                  <button className="btn cmn-btn1 dis_btns2" onClick={
                                    () => {this.props.update_invoice_status(this.props.match.params.id,invoice_number)
                                    .then(json=>{
                                      if(json.status){
                                      this.setState({redirect_duplicate:true})
                                      onClose();
                                      }
                                    })

                                    }}>Yes
                                      {/* <i className='icon icon-accept-approve1-ie'></i> */}
                                                        </button>
                                  <button className="btn cmn-btn2 dis_btns2" onClick={
                                    () => {
                                      onClose();
                                      resolve({ status: false });this.reset_invoice();
                                    }}> No
                                        {/* <i className='icon icon-close2-ie'></i> */}
                                    </button>
                                </div>
                              </div>
                          </Modal>
                         )
                       }
                   })
                }
              );
            }
         })
       }
    }
    custom_validation = () =>{
      let status = true;
      let status2 = true;
      let status3 = true;
      let status4 = false;
      let p_type = this.state.duplicate_p_type;
      let state1 = this.state.duplicate_validate;
      state1['biller_code_reference_no_check']['message']='';state1['reference_no_error']['message']='';state1['billpay_error']['message']='';
      state1['ref_no_error']['message']='';state1['bsb_error']['message']='';state1['account_name_error']['message']='';
      state1['account_no_error']['message']='';
      switch(parseInt(p_type)){
        case 1:
            let biller_code = typeof(this.state.duplicateinvoice.biller_code)!='undefined'?this.state.duplicateinvoice.biller_code:'';
            let ref = typeof(this.state.duplicateinvoice.reference_no)!='undefined'?this.state.duplicateinvoice.reference_no:'';
            // let ref = this.state.duplicateinvoice.reference_no;
            if(biller_code.length==0 || biller_code.length>15){
              status = false;
              state1['biller_code_reference_no_check']['error'] = false;
              state1['biller_code_reference_no_check']['message']='Invalid Biller Code';

            }else{
              state1['biller_code_reference_no_check']['error'] = true;
                state1['biller_code_reference_no_check']['message']='';
              status = true;
            }
            if(ref.length==0 || ref.length>15){
              status2 = false;
              state1['reference_no_error']['error'] = false;
              state1['reference_no_error']['message']='Reference Number should be in between 1 to 15';
            }else{
              state1['reference_no_error']['error'] = true;
              state1['reference_no_error']['message']='';
              status2 = true;
            }

          break;
        case 2:
            let billpay_code = typeof(this.state.duplicateinvoice.billpay_code)!='undefined'?this.state.duplicateinvoice.billpay_code:'';
            let refe = typeof(this.state.duplicateinvoice.ref_no)!='undefined'?this.state.duplicateinvoice.ref_no:'';
            if(billpay_code.length==0 || billpay_code.length>4){
        
              status = false;
              state1['billpay_error']['error'] = false;
              state1['billpay_error']['message']= "Billpay should be in between 1 to 4 ";
            }else{
              state1['billpay_error']['error'] = true;
              status = true;
              state1['billpay_error']['message']="";
            }
            if(refe.length==0 || refe.length>15){
              state1['ref_no_error']['message']= "Reference Number should be in between 1 to 15";
              status2 = false;
              state1['ref_no_error']['error'] = false;
            }else{
              state1['ref_no_error']['message']="";
              status2 = true;
              state1['ref_no_error']['error'] = true;
            }
              break;
        case 3:
            let bsb = this.state.duplicateinvoice.bsb;
            let acc_no = this.state.duplicateinvoice.acc_no;
            let acc_name = this.state.duplicateinvoice.acc_name;
            if(bsb.length==0 ){
              status = false;
              state1['bsb_error']['message']="BSB Code is required";
              state1['bsb_error']['error'] = false;
            }else {
              state1['bsb_error']['error'] = true;
              status = true;
              state1['bsb_error']['message']="";
            }
            if(acc_name.length==0 ){
              status2 = false;
              state1['account_name_error']['error'] = false;
              state1['account_name_error']['message']="Account Name is required";
            }else {
              state1['account_name_error']['error'] = true;
              status2 = true;
              state1['account_name_error']['message']="";
            }
            const re = /^\d{3}-?\d{3}$/;
            if(!re.test(acc_no)){
              status3 = false;
              state1['account_no_error']['error'] = false;
              state1['account_no_error']['message']="The length is 6 digits without dash or 7 digits with dash and contains only numbers. ex:123-123 or 123123";
            }else {
              state1['account_no_error']['error'] = true;
              state1['account_no_error']['message']="";
              status3 = true;
            }
          break;
        default: status4 = false;
          break;
      }
      if(status && status2 && status3)
        status4 = true;
      this.setState(state1)
      return status4;
    }

    send_invoice = (e) => {
        e.preventDefault();
        if (jQuery("#invoice").valid()) {
            let state = this.state.duplicateinvoice;
            state['invoice_number'] = this.state.returntoinvoicenumber;
            this.props.invoice_status('inQueue', this.props.match.params.id, state);
            this.setState({ inQueue: true })
        }
    }
    companySuccess = (name) =>{
      let state = this.state.duplicateinvoice;
      let state2 = this.state.duplicate_validate.company_name;
      state2['error']=false;
      state2['message']='';
      state['company_name']={label:name,value:name}
      this.setState(state);
      this.setState(state2);
    }
    handleClose = () =>{
      this.setState({setup_company:false})
    }
    render() {

      var validationErrors = [];
      Object.values(this.state.duplicate_validate).forEach(function(value) {
          validationErrors.push(value.message )
      });

        let participantId = (typeof (this.props.CrmParticipantId) != 'undefined') ? this.props.CrmParticipantId : [{}];

        if (this.state.redirect) {
            return (<Redirect to='/duplicateinvoices' />);
        }
        let invoice_details = this.state.invoicedetails;
        let duplicateinvoice = this.state.duplicateinvoice;
        let service_count = (typeof (duplicateinvoice.validate) != 'undefined') ? duplicateinvoice.validate.service_count.message : '';
        let biller_code_reference_no_error = (typeof (invoice_details.validate) != 'undefined') ?  invoice_details.validate.biller_code_reference_no_check.error : false;
        let duplicate_biller_code_reference_no_error = (typeof (duplicateinvoice.duplicate_validate) != 'undefined') ? (typeof (this.props.biller_Status) != 'undefined') ? this.props.biller_Status : duplicateinvoice.duplicate_validate.biller_code_reference_no_check.error : false;

        return (


            <AuthWrapper>

                <Modal show={this.state.inQueueModal} >

                    <div className='modalDialog_mini cstmDialog' >
                        <form >
                            <div className='mr_b_15 hdngModal'>
                                <h5 className='hdngCont'><strong>Return to Due </strong></h5>
                                <i className='icon icon-close2-ie closeIc' onClick={() => this.setState({ inQueueModal: false })}></i>
                            </div>
                            <div>
                                <div className='csaddOn_fld1__ '>
                                    <label className='inpLabel1'>Invoice Number:</label>
                                    <input type="text" className="csForm_control filled" data-rule-required="true" name="returntoinvoicenumber" onChange={(e) => this.setState({ returntoinvoicenumber: e.target.value })} />
                                </div>
                            </div>
                            <div className='text-right mr_t_60'>
                                <button className='btn cmn-btn2 dis_btns2 ' onClick={() => this.setState({ inQueueModal: false })}>
                                    <span>
                                        Cancel
                                <i className='icon icon-close2-ie'></i>
                                    </span>
                                </button>
                                <button className='btn cmn-btn1 dis_btns2' onClick={(e) => this.send_invoice(e)}>
                                    <span>
                                        Send Invoice No
                                <i className='icon icon-accept-approve1-ie'></i>
                                    </span>
                                </button>

                            </div>
                        </form>
                    </div>

                </Modal>
                <Modal show={this.state.confirmBox} >

                    <div className='modalDialog_mini cstmDialog' >

                        <h3>Are you sure you want to mark it duplicate?  </h3>
                        <div className='text-right mr_t_60'>
                            <button className='btn cmn-btn2 dis_btns2 ' onClick={() => this.setState({ confirmBox: false })}>
                                <span>
                                    Cancel
                                <i className='icon icon-close2-ie'></i>
                                </span>
                            </button>
                            <button className='btn cmn-btn1 dis_btns2' onClick={(e) => this.markDuplicate(e)}>
                                <span>
                                    Confirm
                                <i className='icon icon-accept-approve1-ie'></i>
                                </span>
                            </button>

                        </div>
                    </div>

                </Modal>
                <div className=" back_col_cmn-">
                    <Link to='/duplicateinvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>{duplicateinvoice.duplicate_position}/{duplicateinvoice.duplicate_count} Duplicate Invoice</span>

                    </h1>
                </div>

                <div className='row pd_tb_15'>

                    <div className='col-xl-6 col-lg-12 col-12'>

                        <div className='bor_tb_grey pd_tb_10 d-flex align-items-center hdnfFlx23' >
                            <span><strong className='cmn_clr1 '>Invoice #{invoice_details.invoice_number}</strong></span>
                            <button className={invoice_details.invoice_status_class + ' pd_btns2'}>{invoice_details.invoice_status_text}</button>
                        </div>

                        <div className=''>
                            <PdfViewer contents={invoice_details} />
                            <div className='clearfix'></div>
                        </div>




                        <div className='fieldArea pd_tb_15 '>

                            <div className='mini_profile__ d-flex mt-5 mb-4'>

                                <div className='pro_ic1'>
                                    <i className='icon icon-userm1-ie'></i>
                                </div>

                                <div className='prf_cntPart'>
                                    <h4 className='cmn_clr1'><strong>{invoice_details.Fullname}</strong></h4>
                                    <div>
                                        <h5 className='avl_fnd'>
                                            <strong>Avail Funds :</strong>
                                            <span>{invoice_details.funds}</span>
                                        </h5>
                                    </div>

                                    <div disabled className='d-flex align-items-center'>
                                      {  // <select className='cstmSlctHt stats_slct' >
                                        //     <option >Active</option>
                                        //     <option >Inactive</option>
                                        // </select>
                                      }
                                      <div class="stat_grp"><span class="status val">{(invoice_details.status==1)?'Active':'Inactive'}</span></div>

                                        {
                                        // <label className='cstmChekie clrTpe2'>
                                        //     <input type="checkbox" className=" " />
                                        //     <div className="chkie"></div>
                                        // </label>
                                       }

                                    </div>



                                </div>

                            </div>





                            {/* mini_profile__ ends */}
                            <div className='bor_tb_black   pd_tb_10 mr_t_15' >
                                <strong>Participant Details</strong>
                            </div>

                            <div className='row pd_tb_15' >

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>HCM Mail Account</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" readOnly className="csForm_control completed" name="" value={invoice_details.participant_ocs_email_acc || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>HCM ID</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" readOnly className="csForm_control completed" name="" value={typeof (invoice_details.participant_ocs_id) != 'undefined' ? invoice_details.participant_ocs_id.label : ''} />
                                        </div>
                                    </div>
                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>First Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" readOnly className="csForm_control filled" name="" value={typeof (invoice_details.participant_firstname) != 'undefined' ? invoice_details.participant_firstname.label : ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Match System Data</label>
                                        <div className={(this.state.validate.participant_firstname.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Last Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" readOnly className="csForm_control filled" name="" value={invoice_details.participant_lastname || ''} />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}
                            <div className='bor_tb_black   pd_tb_10' >
                                <strong>Account Details</strong>
                            </div>

                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Received Date</label>
                                        <div className={(this.state.validate.invoice_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                            <DatePicker
                                                className={(this.state.validate.invoice_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                selected={moment(invoice_details.invoice_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.invoice_date}
                                                onChange={(e) => this.setState({ invoicedetails: [{ invoice_date: e }] })}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                readOnly
                                                placeholderText={'DD/MM/YY'}
                                                autoComplete={'off'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Due Date</label>
                                        <div className={(this.state.validate.due_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                            <DatePicker
                                                className={(this.state.validate.due_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                selected={moment(invoice_details.due_date, 'DD/MM/YYYY').valueOf()}
                                                value={invoice_details.due_date}
                                                onChange={(e) => this.setState({ invoicedetails: [{ due_date: e }] })}
                                                dateFormat="DD/MM/YYYY"
                                                name="tempDueDate"
                                                readOnly
                                                placeholderText={'DD/MM/YY'}
                                                autoComplete={'off'}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Company Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <input type="text" readOnly className={(this.state.validate.company_name.error) ?"csForm_control errorIn2" :"csForm_control filled"} name="" value={typeof (invoice_details.company_name) != 'undefined' ? invoice_details.company_name.value : ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Matches Supplier</label>
                                        <div className={(this.state.validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.company_name.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" />

                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice No.</label>
                                        <div className={(this.state.validate.invoice_number.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.invoice_number.error) ? 'csForm_control errorIn2' : 'csForm_control completed'} name="" value={invoice_details.invoice_number || ''}  />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Known Biller</label>
                                        <div className={(this.state.validate.invoice_number.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.invoice_number.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" />

                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Amount</label>
                                        <div className={(this.state.validate.invoice_amount.error) ?'csaddOn_fld1__ errorIc':'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.invoice_amount.error) ? 'csForm_control errorIc2' :"csForm_control filled"} name="" value={invoice_details.invoice_amount || ''} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Sufficient Available Balance</label>
                                        <div className={(this.state.validate.invoice_amount.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" readOnly className={(this.state.validate.invoice_amount.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}



                            <div className='bor_tb_black cmn_clr1  pd_tb_10 mr_t_15' >
                                <strong>Payment Method</strong>
                            </div>

                            <div className='row pd_tb_15' >


                            <div className="col-lg-12 col-12">
                                <div className="csform-group">
                                    <label className='inpLabel1'>Please select one</label>
                                </div>
                            </div>
                            <div className="col-lg-12 col-12">
                                <div className="csform-group">
                                    <div className="custom_radio label_l bpay_lab">
                                        <input type="radio" readOnly  name="radio-group"  value={1} checked={(this.state.p_type==1)?true:false} />
                                        <label for="test1" ><strong>Bpay</strong></label>
                                    </div>
                                </div>
                            </div>

                            <div className={(this.state.p_type==1)?'col-lg-12 col-12 show':'col-lg-12 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Biller Name</label>
                                    <div className='csaddOn_fld1__ '>
                                        <p>{this.state.biller_name}</p>
                                    </div>
                                </div>
                            </div>

                            <div className={(this.state.p_type==1)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Biller Code</label>
                                    <div className={(biller_code_reference_no_error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly className={(biller_code_reference_no_error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name="biller_code" value={(invoice_details.biller_code)?invoice_details.biller_code:''}  onChange={(e) =>this.selectChange(e.target.value,'biller_code')} data-rule-required="true" data-rule-maxlength="15"  maxLength="15"/>
                                    </div>
                                </div>
                            </div>

                            <div className={(this.state.p_type==1)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Reference Number</label>
                                    <div className={(this.state.validate.reference_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly className={(this.state.validate.reference_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name="reference_no" value={(invoice_details.reference_no)?invoice_details.reference_no:''}  onChange={(e) =>this.selectChange(e.target.value,'reference_no')} data-rule-required="true" data-rule-maxlength="15"  maxLength="15"/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-12">
                                <div className="csform-group">
                                    <div className="custom_radio label_l bpay_lab">
                                        <input type="radio" readOnly  name="radio-group"  value={2} checked={(this.state.p_type==2)?true:false} />
                                        <label for="test1" ><strong>Aus Post</strong></label>
                                    </div>
                                </div>
                            </div>
                            <div className={(this.state.p_type==2)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Billpay Code</label>
                                    {console.log(this.state.validate.billpay_error.error)}
                                    <div className={(this.state.validate.billpay_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly value={(invoice_details.billpay_code)?invoice_details.billpay_code:''}  onChange={(e) =>this.selectChange(e.target.value,'billpay_code')}  className={(this.state.validate.billpay_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                    </div>
                                </div>
                            </div>

                            <div className={(this.state.p_type==2)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Reference Number</label>
                                    <div className={(this.state.validate.ref_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly value={(invoice_details.ref_no)?invoice_details.ref_no:''}  onChange={(e) =>this.selectChange(e.target.value,'ref_no')}  className={(this.state.validate.ref_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12 col-12">
                                <div className="csform-group">
                                    <div className="custom_radio label_l bpay_lab">
                                        <input type="radio" readOnly  name="radio-group"  value={3} checked={(this.state.p_type==3)?true:false} />
                                        <label for="test1" ><strong>Bank</strong></label>
                                    </div>
                                </div>
                            </div>
                            <div className={(this.state.p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>BSB</label>
                                    <div className={(this.state.validate.bsb_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly value={(invoice_details.bsb)?invoice_details.bsb:''}  onChange={(e) =>this.selectChange(e.target.value,'bsb')}  className={(this.state.validate.bsb_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                    </div>
                                </div>
                            </div>
                            <div className={(this.state.p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Account Name</label>
                                    <div className={(this.state.validate.account_name_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="text" readOnly value={(invoice_details.acc_name)?invoice_details.acc_name:''}  onChange={(e) =>this.selectChange(e.target.value,'acc_name')}  className={(this.state.validate.account_name_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                    </div>
                                </div>
                            </div>

                            <div className={(this.state.p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                <div className="csform-group">
                                    <label className='inpLabel1'>Account Number</label>
                                    <div className={(this.state.validate.account_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                        <input type="number" readOnly value={(invoice_details.acc_no)?invoice_details.acc_no:''}  onChange={(e) =>this.selectChange(e.target.value,'acc_no')} className={(this.state.validate.account_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                    </div>
                                </div>
                            </div>

                            </div>
                            </div>
                            {/* fieldArea ends */}


                        </div>


                        <div className='col-xl-6 col-lg-12 col-12 bor_l_grey rg_brCl_12zz'>

                            <div className='bor_tb_grey pd_tb_10 d-flex align-items-center hdnfFlx23' >
                                <span><strong className='cmn_clr1 '>Invoice #{duplicateinvoice.invoice_number}</strong></span>
                                <button className={duplicateinvoice.invoice_status_class + ' pd_btns2'}>{duplicateinvoice.invoice_status_text}</button>
                            </div>

                            <div className=''>
                                <PdfViewer contents={duplicateinvoice} />
                                <div className='clearfix'></div>
                            </div>


                            <div className='fieldArea pd_tb_15'>
                              <form >

                                {/* <div className='dupliAssesment__'>

                            <div className='head_dup__'>
                                <div className='name_hd'>
                                    <h5>Duplicate Assessment Correct?</h5>
                                </div>
                                <div>
                                    {// <button className='btn cmn-btn6 ases_bts' onClick={() => { this.status_update('inQueue') }}>Return to Due</button>
                                    }<button className='btn cmn-btn7 ases_bts' onClick={() => { this.status_update('Duplicate') }}>Mark Duplicate</button>
                                    <button className='btn cmn-btn7 ases_bts' onClick={() => { this.status_update('followUp') }}>Follow Up</button>
                                    <button className='btn cmn-btn7 ases_bts' onClick={() => { this.status_update('Paid') }}>Approve</button>
                                </div>
                            </div>
                            <div className='assess_body__'>
                                <i className='icon icon-warning2-ie wrng_ico'></i>
                                <div className=''>
                                    <h6>Denotes Duplicated Data</h6>
                                    <div>Compare Invoices and Selection above Date</div>
                                </div>
                            </div>

                        </div> */}
                                {/* dupliAssesment__ ends */}


                                <div className='dupliAssesment__'>

                                    <div className='head_dup__'>
                                        <div className='name_hd text-center ptb_7'>
                                            <h5>HCM has indentified potential duplicate data on this invoice</h5>
                                        </div>

                                    </div>
                                    <div className='assess_body__'>
                                        <i className='icon icon-warning2-ie wrng_ico'></i>
                                        <div className=''>
                                            <h6>Denotes Duplicated Data</h6>
                                            <div>Compare Invoices and select action below</div>
                                        </div>
                                    </div>
                                    <div className="assess_foot__">
                                        <div>
                                            {// <button className='btn cmn-btn6 ases_bts' onClick={() => { this.status_update('inQueue') }}>Return to Due</button>
                                            }<button className='btn cmn-btn7 ases_bts' onClick={(e) => { this.status_update(e,'Duplicate') }}>Mark Duplicate</button>
                                            <button className='btn cmn-btn10 ases_bts' onClick={(e) => { this.status_update(e,'followUp') }}>Follow Up</button>
                                            <button className='btn cmn-btn11 ases_bts' onClick={(e) => { this.status_update(e,'Paid') }}>Approve</button>
                                        </div>
                                    </div>

                                </div>
                                {/* dupliAssesment__ ends */}


                        {  //       <div className='row '>
                          //
                          //     <div className="invoice_errors col-md-12">
                          //     <div>
                          //         <ul className="error_listie">
                          //             {
                          //                 validationErrors.map(function (item, i) {
                          //                     // console.log(item)
                          //                     if (item !== undefined && item !== '') {
                          //                         return (<li key={i}>{item}</li>)
                          //                     }
                          //                 })
                          //             }
                          //         </ul>
                        //     </div>
                        // </div>
                          //     </div>
}
                                <div className='bor_tb_black   pd_tb_10 mr_t_15' >
                                <strong>Participant  Details</strong>
                            </div>

                                <div className='row pd_tb_15' >

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>HCM Mail Account</label>
                                            <div className='csaddOn_fld1__ '>
                                                <input type="text" className={this.state.duplicate_validate.participant_firstname.error?"csForm_control errorIn2":"csForm_control completed"} name="" value={duplicateinvoice.participant_ocs_email_acc || ''} />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>HCM ID</label>
                                            <div className='csaddOn_fld1__ cssaddOn_select___'>
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={false}
                                                    searchable={true} Clearable={false}
                                                    placeholder=""
                                                    options={participantId}
                                                    onChange={(e) => this.selectSearch(e, 'participant_ocs_id')}
                                                    value={duplicateinvoice.participant_ocs_id}

                                                />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>


                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>First Name</label>
                                            <div className='csaddOn_fld1__  cssaddOn_select___'>
                                                <Select.Async
                                                    cache={false}
                                                    clearable={false}
                                                    name="participant_firstname" required={true}
                                                    value={duplicateinvoice.participant_firstname}
                                                    loadOptions={getOptionsParticipant}
                                                    placeholder='Search'
                                                    onChange={(e) => this.selectChanges(e, 'participant_firstname')}
                                                    className={(this.state.duplicate_validate.participant_firstname.error) ?"errorIn2":'filled'}
                                                />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Match System Data</label>
                                            <div className={(this.state.duplicate_validate.participant_firstname.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.duplicate_validate.participant_firstname.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.duplicate_validate.participant_firstname.error)?'No':'Yes'}/>
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Last Name</label>
                                            <div className='csaddOn_fld1__ '>
                                            <Select.Async
                                                  cache={false}
                                                  clearable={false}
                                                  name="participant_lastname" required={true}
                                                  value={duplicateinvoice.participant_lastname}
                                                  loadOptions={getOptionsParticipant}
                                                  placeholder='Search'
                                                  onChange={(e)=>this.selectChanges(e,'participant_lastname')}
                                                  className={(this.state.duplicate_validate.participant_firstname.error) ?"errorIn2":'filled'}
                                              />
                                              <span class="error pd_tb_15">{this.state.duplicate_validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {/* row ends */}
                                <div className='bor_tb_black  pd_tb_10' >
                                    <strong>Account Details</strong>
                                </div>

                                <div className='row pd_tb_15'>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice Received Date</label>
                                            <div className={(this.state.duplicate_validate.invoice_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                                <DatePicker
                                                    className={(this.state.duplicate_validate.invoice_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                    selected={moment(duplicateinvoice.invoice_date, 'DD/MM/YYYY').valueOf()}
                                                    value={duplicateinvoice.invoice_date}
                                                    onChange={(e) => this.selectChange(moment(e).format("DD/MM/YYYY"), 'invoice_date')}
                                                    dateFormat="DD/MM/YYYY"
                                                    name="tempDueDate"
                                                    placeholderText={'DD/MM/YY'}
                                                    autoComplete={'off'}
                                                />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.invoice_date.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Due Date</label>
                                            <div className={(this.state.duplicate_validate.due_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                                <DatePicker
                                                    className={(this.state.duplicate_validate.due_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                    selected={moment(duplicateinvoice.due_date, 'DD/MM/YYYY').valueOf()}
                                                    value={duplicateinvoice.due_date}
                                                    onChange={(e) => this.selectChange(moment(e).format("DD/MM/YYYY"), 'due_date')}
                                                    dateFormat="DD/MM/YYYY"
                                                    name="tempDueDate"
                                                    placeholderText={'DD/MM/YY'}
                                                    autoComplete={'off'}
                                                />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.due_date.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Company Name</label>
                                            <div className={this.state.duplicate_validate.company_name.error?'csaddOn_fld1__  errorIc':'csaddOn_fld1__  filled'}>
                                                <Select.Async
                                                    cache={false}
                                                    clearable={false}
                                                    name="company_name" required={true}
                                                    value={duplicateinvoice.company_name}
                                                    loadOptions={getOptionsCompany}
                                                    placeholder='Search'
                                                    onChange={(e) => this.selectChanges(e, 'company_name')}
                                                    className={"errorIn2"}
                                                />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.company_name.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Matches Supplier</label>
                                            <div className={(this.state.duplicate_validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.duplicate_validate.company_name.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.duplicate_validate.company_name.error) ?'No':typeof(duplicateinvoice.company_name)!='undefined'?duplicateinvoice.company_name.label:'Yes'} />
                                                <em>{service_count}</em>
                                                {//Company name doesn't match with allowed list,{this.state.duplicate_validate.company_name.error?<a onClick={()=>this.setState({setup_company:true})}><b>Click here</b></a>:''} to add in the allowed list.
                                                }
                                                  {this.state.duplicate_validate.company_name.error?<a onClick={()=>this.setState({setup_company:true})}>Set up Company</a>:''
                                                  }
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice No.</label>
                                            <div className={(this.state.duplicate_validate.invoice_number.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" className={(this.state.duplicate_validate.invoice_number.error) ? 'csForm_control errorIn2' : 'csForm_control completed'} name="" value={duplicateinvoice.invoice_number || ''} onChange={(e) => {this.selectChange(e.target.value, 'invoice_number');this.invoice_validate(e.target.value)}} />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.invoice_number.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Known Biller</label>
                                            <div className={(this.state.duplicate_validate.invoice_number.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.duplicate_validate.invoice_number.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.duplicate_validate.invoice_number.error) ?'No':'Yes'}/>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice Amount</label>
                                                  <div className={(this.state.duplicate_validate.invoice_amount.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" className={(this.state.duplicate_validate.invoice_amount.error) ? 'csForm_control errorIn2' : 'csForm_control completed'}  name="" value={duplicateinvoice.invoice_amount || ''} onChange={(e) => this.selectChange(e.target.value, 'invoice_amount')} />
                                                <span class="error pd_tb_15">{this.state.duplicate_validate.invoice_amount.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Sufficient Available Balance</label>
                                            <div className={(this.state.duplicate_validate.invoice_amount.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.duplicate_validate.invoice_amount.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.duplicate_validate.invoice_amount.error) ?'No':'Yes'} />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {/* row ends */}


                                <div className='bor_tb_black cmn_clr1  pd_tb_10 mr_t_15' >
                                    <strong>Payment Method</strong>
                                </div>

                                <div className='row pd_tb_15' >


                                <div className="col-lg-12 col-12">
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Please select one</label>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-12">
                                    <div className="csform-group">
                                        <div className="custom_radio label_l bpay_lab">
                                            <input type="radio"  name="radio-group"  value={1} checked={(this.state.duplicate_p_type==1)?true:false} />
                                            <label for="test1" onClick={()=>this.setState({duplicate_p_type:1})}><strong>Bpay</strong></label>
                                        </div>
                                    </div>
                                </div>

                                <div className={(this.state.duplicate_p_type==1)?'col-lg-12 col-12 show':'col-lg-12 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Biller Name</label>
                                        <div className='csaddOn_fld1__ '>
                                            <p>{this.state.duplicate_biller_name}</p>
                                        </div>
                                    </div>
                                </div>

                                <div className={(this.state.duplicate_p_type==1)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Biller Code</label>
                                        <div className={(duplicate_biller_code_reference_no_error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" className={(duplicate_biller_code_reference_no_error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name="biller_code" value={(duplicateinvoice.biller_code)?duplicateinvoice.biller_code:''}  onChange={(e) =>this.selectChange(e.target.value,'biller_code')} data-rule-required="true" data-rule-maxlength="15"  maxLength="15"/>
                                            <span class="error pd_tb_15">{this.state.duplicate_validate.biller_code_reference_no_check.message}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className={(this.state.duplicate_p_type==1)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reference Number</label>
                                        <div className={(this.state.duplicate_validate.reference_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" className={(this.state.duplicate_validate.reference_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name="reference_no" value={(duplicateinvoice.reference_no)?duplicateinvoice.reference_no:''}  onChange={(e) =>this.selectChange(e.target.value,'reference_no')} data-rule-required="true" data-rule-maxlength="15"  maxLength="15"/>
                                              <span class="error pd_tb_15">{this.state.duplicate_validate.reference_no_error.message}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-12">
                                    <div className="csform-group">
                                        <div className="custom_radio label_l bpay_lab">
                                            <input type="radio"  name="radio-group"  value={2} checked={(this.state.duplicate_p_type==2)?true:false} />
                                            <label for="test1" onClick={()=>this.setState({duplicate_p_type:2})}><strong>Aus Post</strong></label>
                                        </div>
                                    </div>
                                </div>
                                <div className={(this.state.duplicate_p_type==2)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Billpay Code</label>
                                        <div className={(this.state.duplicate_validate.billpay_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" maxLength="4" value={(duplicateinvoice.billpay_code)?duplicateinvoice.billpay_code:''}  onChange={(e) =>this.selectChange(e.target.value,'billpay_code')}  className={(this.state.duplicate_validate.billpay_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                            <span class="error pd_tb_15">{this.state.duplicate_validate.billpay_error.message}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className={(this.state.duplicate_p_type==2)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reference Number</label>
                                        <div className={(this.state.duplicate_validate.ref_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" maxLength="15" value={(duplicateinvoice.ref_no)?duplicateinvoice.ref_no:''}  onChange={(e) =>this.selectChange(e.target.value,'ref_no')}  className={(this.state.duplicate_validate.ref_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                            <span class="error pd_tb_15">{this.state.duplicate_validate.ref_no_error.message}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-12">
                                    <div className="csform-group">
                                        <div className="custom_radio label_l bpay_lab">
                                            <input type="radio"  name="radio-group"  value={3} checked={(this.state.duplicate_p_type==3)?true:false} />
                                            <label for="test1" onClick={()=>this.setState({duplicate_p_type:3})}><strong>Bank</strong></label>
                                        </div>
                                    </div>
                                </div>
                                <div className={(this.state.duplicate_p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>BSB</label>
                                        <div className={(this.state.duplicate_validate.bsb_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" value={(duplicateinvoice.bsb)?duplicateinvoice.bsb:''}  onChange={(e) =>this.selectChange(e.target.value,'bsb')}  className={(this.state.duplicate_validate.bsb_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                            <span class="error pd_tb_15">{this.state.duplicate_validate.bsb_error.message}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className={(this.state.duplicate_p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Account Name</label>
                                        <div className={(this.state.duplicate_validate.account_name_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" value={(duplicateinvoice.acc_name)?duplicateinvoice.acc_name:''}  onChange={(e) =>this.selectChange(e.target.value,'acc_name')}  className={(this.state.duplicate_validate.account_name_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                            <span class="error pd_tb_15">{this.state.duplicate_validate.account_name_error.message}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className={(this.state.duplicate_p_type==3)?'col-lg-6 col-12 show':'col-lg-6 col-12 hidden'}>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Account Number</label>
                                        <div className={(this.state.duplicate_validate.account_no_error.error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="number" value={(duplicateinvoice.acc_no)?duplicateinvoice.acc_no:''}  onChange={(e) =>this.selectChange(e.target.value,'acc_no')} className={(this.state.duplicate_validate.account_no_error.error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name=""  />
                                              <span class="error pd_tb_15">{this.state.duplicate_validate.account_no_error.message}</span>
                                        </div>
                                    </div>
                                </div>
                                </div>

                                </form>
                            </div>
                            {/* fieldArea ends */}

                        </div>
                    </div>
                    {/* col-6 ends */}

                    <NotifyPopUp
                        notifyColor='red'
                        show={this.state.Duplicate}
                        Close={() => { this.setState({ Duplicate: false, redirect: true }) }}
                        iconLef={'icon-approved2-ie'}
                    // btnName={"Undo"}
                    >

                        Invoice <span className='txt_clr'>{duplicateinvoice.invoice_number}</span> marked as Duplicate

               </NotifyPopUp>


                    <NotifyPopUp
                        notifyColor='yellow'
                        show={this.state.inQueue}
                        Close={() => { this.setState({ inQueue: false, redirect: true }) }}
                        iconLef={"icon-error-icons"}
                    // btnName={"Undo"}
                    >

                        Invoice <span className='txt_clr'>{duplicateinvoice.invoice_number}</span> moved to Pending

               </NotifyPopUp>
                    <NotifyPopUp
                        notifyColor='green'
                        show={this.state.paidPopUp}
                        Close={() => { this.setState({ paidPopUp: false, redirect: true }) }}
                        iconLef={'icon-approved2-ie'}
                    // btnName={"Undo"}
                    >

                        Invoice <span className='txt_clr'>{duplicateinvoice.invoice_number}</span> marked as paid

               </NotifyPopUp>


                    <NotifyPopUp
                        notifyColor='yellow'
                        show={this.state.followPopUp}
                        Close={() => { this.setState({ followPopUp: false, redirect: true }) }}
                        iconLef={"icon-error-icons"}
                    // btnName={"Undo"}
                    >

                        Invoice <span className='txt_clr'>{duplicateinvoice.invoice_number}</span> moved to follow up

               </NotifyPopUp>

               <AddCompany  show={this.state.setup_company} handleClose={this.handleClose} companySuccess={this.companySuccess}/>


            </AuthWrapper>

                );
            }
        }




const mapStateToProps = state => {
    return {
                    invoice_details: state.InvoiceReducer.invoicedetails,
                pdfData: state.InvoiceReducer.data,
                biller_Status: state.InvoiceReducer.billerStatus,
                getnewdate: state.InvoiceReducer.getnewdate,
                CrmParticipantId: state.InvoiceReducer.CrmParticipantId

            }
        };
const mapDispatchtoProps = (dispatch) => bindActionCreators({
                    invoiceDetails, pdfData,invoice_exsits, loader, invoice_status, biller_refernce_check, getNewDate, getOptionsCrmParticipantId
                }, dispatch)
                export default connect(mapStateToProps, mapDispatchtoProps)(ViewDuplicateInvoice);
