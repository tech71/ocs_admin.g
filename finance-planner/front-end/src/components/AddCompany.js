import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Modal from './utils/Modal';
import AuthWrapper from '../hoc/AuthWrapper';
import jQuery from "jquery";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, setupCompany } from '../store/actions';
class AddCompany extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: props.show,
            loading: false,
            biller_code: '',
            billpay_code: '',
        }
    }

    handleChange = (fieldname, e) => {
        var inputFields = this.state;
        this.setState({ error: '' });
        inputFields[fieldname] = e;
        this.setState(inputFields);
    }
    handleClose = () => {
        this.setState({ showModal: false })
    }
    submit = (e) => {
        e.preventDefault();
        var validator = jQuery("#set_company").validate({ ignore: [] });
        if (!this.state.loading && jQuery("#set_company").valid()) {
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            var str = '';

            this.setState({ loading: true }, () => {
                str = { 'biller_code': this.state.biller_code, 'billpay_code': this.state.billpay_code, 'bsb_number': this.state.bsb_number, 'bank_account_no': this.state.bank_account_no, 'company_name': this.state.company_name, 'bank_account_name': this.state.bank_account_name }
                this.props.setupCompany(str).then((res) => {
                    if (res.status) {
                        this.props.companySuccess(res.data);
                        this.props.handleClose();
                    }
                })

            });
        } else {
            validator.focusInvalid();
        }
    }

    render() {

        return (

            <Modal show={this.props.show} >
            {// <Modal show={this.props.show} >
}

                <div className='modalDialog_macro cstmDialog pb-5' >
                    <div className='mr_b_15 hdngModal text-center mt-3'>
                        <h5 className='hdngCont'><strong>Set Up Company </strong></h5>
                        <i className='icon icon-close2-ie closeIc' onClick={() => this.props.handleClose()}></i>
                    </div>

                    <div className="Modal_form_center">
                        <form id="set_company" method="post" autoComplete="off">



                            <div className="csform-group">
                                <label className='inpLabel1'>Biller Code</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="biller_code" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('biller_code', evt.target.value)}
                                        value={this.state.biller_code}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <div className="csform-group">
                                <label className='inpLabel1'>Billpay Code</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="billpay_code" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('billpay_code', evt.target.value)}
                                        value={this.state.billpay_code}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <div className="csform-group">
                                <label className='inpLabel1'>BSB Number</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="bsb_number" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('bsb_number', evt.target.value)}
                                        value={this.state.bsb_number}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <div className="csform-group">
                                <label className='inpLabel1'>Bank Account Number</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="bank_account_no" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('bank_account_no', evt.target.value)}
                                        value={this.state.bank_account_no}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <div className="csform-group">
                                <label className='inpLabel1'>Company Name</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="company_name" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('company_name', evt.target.value)}
                                        value={this.state.company_name}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <div className="csform-group">
                                <label className='inpLabel1'>Bank Account Name</label>
                                <div className='csaddOn_fld1__'>
                                    <input type="text" name="bank_account_name" data-rule-required="true"
                                        onChange={(evt) => this.handleChange('bank_account_name', evt.target.value)}
                                        value={this.state.bank_account_name}
                                        className="csForm_control " />
                                </div>
                                <p className="alert-danger"></p>
                            </div>

                            <button className='btn cmn-btn1  ' onClick={this.submit}>
                                <span>
                                    Submit
    </span>
                            </button>
                        </form>
                    </div>

                </div>

            </Modal>
        );
    }
}
const mapStateToProps = state => {
    //   console.log(state.InvoiceReducer.newDate);
    return {
        invoice_details: state.InvoiceReducer.invoicedetails,
    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, setupCompany
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(AddCompany);
