import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import ReactTable from "react-table";
import Select from 'react-select-plus';
import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import Modal from './utils/Modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader, manualUpload, participantDetailsById } from '../store/actions';
import { getOptionsParticipant, getOptionsCompany } from '../service/common.js';
import PdfViewer from './utils/PdfViewer';
import Dropzone from 'react-dropzone';
import _ from 'lodash';

const allowedFileTypes = ['image/png', 'image/jpg', 'image/jpeg', 'application/pdf'];
const UPLOAD_MAX_SIZE_IN_MB = 10; //1 Megabyte = 1048576 Bytes.
const UPLOAD_MAX_SIZE = (parseInt(1048576) * parseInt(UPLOAD_MAX_SIZE_IN_MB)); //1 Megabyte = 1048576 Bytes

class ManualUpload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            processModal: false,
            invoicedetails: [], invoicedetails: [],
            countLength: '',
            filesToBeSent: '',
            fileUpload: false,
            fileError: false,
            participantDetails: [],
            returned_invoice_id: '',


        }
    }


    componentWillMount() {
        let participantId = (typeof (this.props.match.params.id) != 'undefined') ? this.props.match.params.id : [{}];
        this.props.participantDetailsById(participantId)
            .then(json => {
                if (json.status) {
                    this.setState({ participantDetails: json.data });
                }
            });
    }

    selectChange = (selectedOption, fieldname) => {
        var state = this.state.participantDetails;
        state[fieldname] = selectedOption;
        state[fieldname + '_error'] = false;
        this.setState(state);
    }


      

    onDrop = (acceptedFiles, rejectedFiles) => {
      // console.log();
      // if(typeof(acceptedFiles)!='undefined' || acceptedFiles.length>0){
      //   var filesToBeSent = acceptedFiles[0];
      //   var fileSize = acceptedFiles[0].size / 1024 / 1024;
      //   if (fileSize > 10) { // 2M
      //       this.setState({ fileError: true });
      //       return false;
      //   }
      // }

        if (rejectedFiles.length > 0) {

            // console.log("rejectedFiles", rejectedFiles);
            let errormsg = false;
            if (rejectedFiles.length > 0 && !_.includes(allowedFileTypes, rejectedFiles[0].type)) {
                errormsg = "The filetype you are attempting to upload is not allowed";
            }

            else if (rejectedFiles.length > 0 && rejectedFiles[0].size > UPLOAD_MAX_SIZE) {
                errormsg = "The file you are attempting to upload is larger than the permitted size (" + UPLOAD_MAX_SIZE_IN_MB + " MB)";
            }

            else {
                errormsg = "The filetype you are attempting to upload is not allowed";
            }

            this.setState({ rejected_msg: errormsg,fileError: true });
        }
        else{
            var filesToBeSent = acceptedFiles[0];
            this.setState({ filesToBeSent: filesToBeSent });
            this.props.manualUpload(this.props.match.params.id, this.state.filesToBeSent, this.state.participantDetails.email).then(res => {
                if (res) {
                    this.setState({ fileUpload: true, returned_invoice_id: res, fileError: false });
                } else {
                    this.setState({ fileError: true });
                }
            });
        }
    }


    render() {

        let participantId = (typeof (this.props.CrmParticipantId) != 'undefined') ? this.props.CrmParticipantId : [{}];
        var options = [
            { label: 'Email Send', value: 'Email Send' },
            { label: 'Phone Call', value: 'Phone Call' },
            { label: 'Post Send', value: 'Post Send' },
            { label: 'Message Send', value: 'Message Send' }
        ];

        const invoice_details = this.state.invoicedetails;
        let company_name_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.company_name.error : false;
        let firstname_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.participant_firstname.error : false;
        let invoice_number_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_number.error : false;
        let invoice_amount_error = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.invoice_amount.error : false;




        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Type', accessor: 'type' },
            { Header: 'Due Date', accessor: 'date' },
            { Header: 'User', accessor: 'user' },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 40,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }


        ]

        let alldisputeNotesData = this.props.allDisputeNotes;
        let alldisputeErrors = this.props.disputeErrorMsg;

        const subComponentDataMapper = row => {
            let data = row.original;
            return (
                <div className='dispSubComp__'>
                    <p>{data.notes}</p>
                    <i className='icon icon-view2-ie vw_dips'></i>
                </div>
            );
        };




        return (

            <AuthWrapper>

                <div className=" back_col_cmn-">
                <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                  {// <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                  }
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Manual Upload</span>

                        <button onClick={() => this.setState({ resolveFollowModal: true })} className="btn cmn-btn2">Exit</button>

                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-12 col-12'>
                        <div className='Drag_and_Drop_wraper'>
                            {  // <div className='pdf_box mr_tb_15'>
                                // </div>
                            }
                            <PdfViewer />

                            <div className="Drag_and_Drop_div">

                                {/* Drag and re Try start  */}
                                <div>
                                    <div className="Drag_Drop">

                                        {(!this.state.fileUpload) ?
                                            <Dropzone
                                            autoDiscover={false}
                                                maxSize={UPLOAD_MAX_SIZE}
                                                accept={allowedFileTypes}
                                                onabort={() => console.log('file reading was aborted')}
                                                multiple={false}
                                                onDrop={(files, rejected) => this.onDrop(files, rejected)} >
                                                {({ getRootProps, getInputProps }) => (
                                                    <div className="custom-file-upload" {...getRootProps()}>
                                                        <input {...getInputProps()} onClick= {event => console.log(event)} />
                                                        <span className="ic-upload-img"></span>
                                                        <p>Drop file to begin upload.</p>
                                                        <h2><strong>Drag & Drop</strong></h2>
                                                        <p>or <strong>Browse</strong> for your files</p>

                                                    </div>

                                                )}
                                            </Dropzone>
                                            : ''}



                                        {(this.state.fileError) ?
                                            <div><label htmlFor="file-upload" className="custom-file-upload reTry">
                                                <span className="icon icon-warning2-ie"></span>
                                                <h2><strong>Oops!</strong></h2>
                                                <p>The file is too big</p>
                                                <p><strong>Max file size: 10MB</strong></p>
                                            </label>
                                                <div>
                                                    <h3 className="mb-0 text-center"><strong>Please Try again</strong></h3>
                                                </div></div>
                                            : ''}
                                    </div>


                                    <div className="content_drag_file mt-2">
                                        {(!this.state.fileUpload) ? <div>
                                            <p><b>Allowed formats:</b></p>
                                            <p>PDF,DOC,DOCX,RTF,HTML,HTM,ODT,JPEG,TIFF,PNG,GIF</p>
                                            <p className="mt-1"><b>Max file size: 10MB</b></p>
                                        </div>
                                            : ''}
                                        {/* <div>
                                        <h3 className="mb-0 text-center"><strong>Please Try again</strong></h3>
                                    </div>  */}
                                    </div>
                                </div>

                                {/* End div Drag and re Try   */}

                                {(this.state.fileUpload) ?
                                    <div className="Drag_done">
                                        <div>
                                            <i className="icon icon-accept-approve2-ie"></i>
                                            <h2><strong>Success!</strong></h2>
                                            <p>This invoice Has been uploaded successfully</p>
                                        </div>

                                        <div>
                                            <span>
                                                {//<Link to={'/pendinginvoices/viewpendinginvoice/'+this.state.returned_invoice_id} class="Drag_done-btn1">View Invoice</Link>
                                                }
                                                <Link to={'/dashboard/'} class="Drag_done-btn2">Back to Dashborad</Link>
                                            </span>
                                        </div>
                                    </div>
                                    : ''}



                            </div>


                        </div>
                    </div>

                    <div className='col-xl-5 col-lg-12 col-12'>

                        <div className='fieldArea pd_tb_15'>




                            <div className="PRF_div">
                                <div className="PRF_left">
                                    <span>
                                        <i className="icon icon-userm1-ie"></i>
                                    </span>
                                </div>
                                <div className="PRF_right">
                                    <div>
                                        <h3>{this.state.participantDetails.firstname} {this.state.participantDetails.lastname}</h3>
                                        {  // <h5><strong>Avail Funds:</strong> <span>$1562.36</span></h5>
                                            <div>
                                                <select className='cstmSlctHt stats_slct' >
                                                    <option selected={(invoice_details.status == 1) ? "selected" : ""}>Active</option>
                                                    <option selected={(invoice_details.status == 0) ? "selected" : ""}>Inactive</option>
                                                </select>

                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>

                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Account Details</strong>
                            </div>

                            <div className='row pd_tb_15'>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>HCM Mail Acc</label>
                                        <div className={(company_name_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(company_name_error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"} name="" value={(this.state.participantDetails.email)} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>HCM ID</label>
                                        <div className={(company_name_error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                            <input type="text" className={(company_name_error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"} name="" value={(this.state.participantDetails.ocs_id)} />
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>First Name</label>
                                        <div className='csaddOn_fld1__ cstm_select search_filter brdr_slct '>
                                            <input type="text" className="csForm_control no_bor" name="" value={(this.state.participantDetails.firstname)} onChange={(e) => this.selectChange(e.target.value, 'firstname')} />
                                            { //<Select.Async
                                                //     cache={false}
                                                //     clearable={false}
                                                //     name="company_name" required={true}
                                                //     value={this.state.participantDetails.firstname}
                                                //     loadOptions={getOptionsCompany}
                                                //     disabled={true}
                                                //     placeholder='Search'
                                                //     onChange={(e) => this.selectChanges(e, 'company_name')}
                                                // />
                                            }
                                        </div>
                                    </div>
                                </div>
                                {
                                    // <div className='col-lg-6 col-12'>
                                    //     <div className="csform-group">
                                    //         <label className='inpLabel1'>Match System Data</label>
                                    //         <div className='csaddOn_fld1__ notAvail'>
                                    //             <input type="text" className="csForm_control " name="" value='NA' readOnly />
                                    //         </div>
                                    //     </div>
                                    // </div>
                                }
                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Last Name</label>
                                        <div className='csaddOn_fld1__ cstm_select search_filter brdr_slct '>
                                            <input type="text" className="csForm_control no_bor" name="" value={(this.state.participantDetails.lastname)} onChange={(e) => this.selectChange(e.target.value, 'lastname')} />
                                            {//<input type="text" className="csForm_control no_bor" name="" value={invoice_details.company_name || ''} onChange={(e) =>this.selectChange(e.target.value,'company_name')}/>
                                                //   <Select.Async
                                                //     cache={false}
                                                //     clearable={false}
                                                //     disabled={true}
                                                //     name="company_name" required={true}
                                                //     value={this.state.participantDetails.lastname}
                                                //     loadOptions={getOptionsCompany}
                                                //     placeholder='Search'
                                                //     onChange={(e) => this.selectChanges(e, 'company_name')}
                                                // />
                                            }
                                        </div>
                                    </div>
                                </div>






                            </div>
                            {/* row ends

                            <div className='bor_tb  pd_tb_10 mr_t_15' >
                                <strong>Member Details</strong>
                            </div>

                            <div className='row pd_tb_15' >



                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Received Date</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>


                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Due Date</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Company Name</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Matches Xero Supplier</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Number</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Known Biller</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Invoice Ammount</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Sufficient Available Funds</label>
                                        <div class="csaddOn_fld1__"><input type="text" class="csForm_control no_bor" name="" value="" /></div>
                                    </div>
                                </div>


                            </div>
                            */}
                            {/* row ends

                            <div className="row">
                                <div className='col-12'>

                                    <div className='inpLabel1'><strong>Payment Type</strong></div>
                                    <ul className='py_typeUL'>
                                        <li className='active'>Bank</li>
                                        <li>B Pay</li>
                                        <li>AUS Post</li>
                                    </ul>

                                </div>
                            </div>
                            */}
                        </div>


                    </div>
                    {/* col-6 ends */}

                </div>
                {/* row ends */}


                {  // <div className="row">
                    //     <div className="col-lg-7">
                    //         <div className="Manual_bottom_btn">
                    //             <a className="fol_btn">Follow Up</a>
                    //             <a className="fol_btn pay_invoice">Pay Invoice</a>
                    //         </div>
                    //     </div>
                    // </div>
                }



                <Modal show={this.state.resolveFollowModal} >

                    <div className='modalDialog_mini cstmDialog' >
                        <div className='mr_b_15 hdngModal'>
                            <h5 className='hdngCont'><strong>Resolve Follow Up </strong></h5>
                            <i className='icon icon-close2-ie closeIc' onClick={() => this.setState({ resolveFollowModal: false })}></i>
                        </div>

                        <div className='text-right mr_t_60'>
                            <button className='btn cmn-btn2 dis_btns2 '>
                                <span>
                                    Deny Invoice
                                    <i className='icon icon-close2-ie'></i>
                                </span>
                            </button>
                            <button className='btn cmn-btn1 dis_btns2'>
                                <span>
                                    Pay
                                    <i className='icon icon-accept-approve1-ie'></i>
                                </span>
                            </button>

                        </div>
                    </div>

                </Modal>


            </AuthWrapper>

        );
    }
}

const mapStateToProps = state => {
    return {
        pdfData: state.InvoiceReducer.data,
        manualUpload: state.uploadReducer.upload_status,
        participantDetailsById: state.uploadReducer.participantdetailsbyid
    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, pdfData, loader, manualUpload, participantDetailsById
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ManualUpload);
