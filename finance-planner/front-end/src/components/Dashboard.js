import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Bar } from 'react-chartjs-2';
import Select from 'react-select-plus';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import * as CT from '../hoc/Color';
import { dashboardData, fetch_invoice, python_script, getGraphData } from '../store/actions';

class Dashboard extends Component {


    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            status: 1,
            measure: 1,
            type: '',
            title: 'Qty',
            search: ''
        };

    }

    componentWillMount() {
        // this.props.dashboardCountData();
        // this.props.get_members_summary();
        // this.props.get_processed_invoice();
        var parameter = { status: this.state.status, measure: this.state.measure, type: this.state.type, search: this.state.search };
        this.props.dashboardData(parameter);
    }

    onSubmitSearch = (e) => {
        e.preventDefault();
        var srch_ary = { status: this.state.status, measure: this.state.measure, type: this.state.type, search: this.state.search };

        this.props.dashboardData(srch_ary);
    }

    render() {
        const { dashboardCount, membersData, processedInvoice, processedInvoiceTill, graphData } = this.props;

        const Graphdata = {
            labels: typeof (graphData) != 'undefined' && typeof (graphData.graph_month) != 'undefined' ? graphData.graph_month.name : ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                data: typeof (graphData) != 'undefined' && typeof (graphData.graph_qty) != 'undefined' ? graphData.graph_qty : [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                backgroundColor: [CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1],

            }]

        };

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];
        // <span className='tb_vals1_'>
        const hdrColumn = [
            { Header: 'HCM ID', accessor: 'ocs_id' },
            {
                Header: "Participant's Name", accessor: 'member_name',
                Cell: (props) => (props.original.pending == 0 && props.original.follow_up == 0 && props.original.duplicate == 0 && props.original.paid == 0) ? <div>{props.value}</div> : <div><a href={"/allinvoices/detail/" + props.original.ocs_id} className="clr_bl">{props.value}</a></div>
            },
            { Header: 'Available Funds', accessor: 'funds' },
            {
                Header: 'Pending', accessor: 'pending',
                Cell: (props) => <Link to={{ pathname: "/allinvoices/detail/" + props.original.ocs_id, state: { status: 4 } }} ><span class="tb_vals1_">{props.value}</span></Link>
            },
            { Header: 'Follow Up', accessor: 'follow_up', Cell: (props) => <Link to={{ pathname: "/allinvoices/detail/" + props.original.ocs_id, state: { status: 3 } }} ><span class="tb_vals1_">{props.original.follow_up}</span></Link> },
            { Header: 'Duplicate', accessor: 'duplicate', Cell: (props) => <Link to={{ pathname: "/allinvoices/detail/" + props.original.ocs_id, state: { status: 5 } }} ><span class="tb_vals1_">{props.original.duplicate}</span></Link> },
            { Header: 'Paid', accessor: 'paid', Cell: (props) => <Link to={{ pathname: "/allinvoices/detail/" + props.original.ocs_id, state: { status: 1 } }} ><span class="tb_vals1_">{props.original.paid}</span></Link> },

        ]




        return (

            <AuthWrapper>
                {  // <div className=" back_col_cmn-">
                    //     <Link to={'#'}><span className="icon icon-back1-ie"></span></Link>
                    // </div>
                }
                <div className="main_heading_cmn-">
                    <h1>
                        <span>Dashboard</span>
                        <Link to='#'>
                            <button className="btn hdng_btn cmn-btn1" onClick={() => this.props.fetch_invoice()}>Fetch Invoice <i className='icon icon-add2-ie'></i></button>
                        </Link>
                        <Link to='#'>
                            <button className="btn hdng_btn cmn-btn1" onClick={() => this.props.python_script()}>Python Script <i className='icon icon-add2-ie'></i></button>
                        </Link>
                    </h1>
                </div>

                <div className='invStatus bor_bot1'>
                    Invoices Processed
                        </div>

                <div className='compoBoxDiv__ d-flex row'>

                    <div className='col-sm-12'>
                        <div className='compBox'>
                            <p className='grpMes cmn_clr1'><strong>{this.state.title}</strong></p>
                            <div className='grph_dv'>
                                <Bar data={Graphdata} height={350} legend={null}
                                    options={{
                                        maintainAspectRatio: false,
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    suggestedMin: 0,
                                                    suggestedMax: 100
                                                }
                                            }]
                                        }
                                    }
                                    }

                                />
                            </div>
                            <p className='grpMes'><strong>Month</strong></p>


                            <div className='row'>

                                <div className='col-lg-3 col-md-1'></div>
                                <div className='col-lg-6 col-md-8'>
                                    <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                        <span><strong>Select:</strong></span>
                                        {/* console.log(this.state.status) */}
                                        <button className={(this.state.status == 1) ? 'cmn-btn1 pd_bts active' : 'cmn-btn1 pd_bts'} onClick={() => this.setState({ status: 1 }, () => this.props.getGraphData({ 'status': 1, 'measure': this.state.measure, 'type': '' }))}>Paid</button>
                                        <button className={(this.state.status == 5) ? 'cmn-btn1 pd_bts active' : 'cmn-btn1 pd_bts'} onClick={() => this.setState({ status: 5 }, () => this.props.getGraphData({ 'status': 5, 'measure': this.state.measure, 'type': '' }))}>Duplicate</button>
                                        <button className={(this.state.status == 3) ? 'cmn-btn1 pd_bts active' : 'cmn-btn1 pd_bts'} onClick={() => this.setState({ status: 3, type: 'automated' }, () => this.props.getGraphData({ 'status': 1, 'measure': this.state.measure, 'type': this.state.type }))}>Automated</button>
                                        <button className={(this.state.status == 4) ? 'cmn-btn1 pd_bts active' : 'cmn-btn1 pd_bts'} onClick={() => this.setState({ status: 4, type: 'manual' }, () => this.props.getGraphData({ 'status': 1, 'measure': this.state.measure, 'type': this.state.type }))}>Manual</button>
                                    </div>
                                </div>
                                <div className='col-lg-3 col-md-3'>
                                    <ul className='grph_opts2'>{/* console.log(this.props) */}
                                        <li className={(this.state.measure == 1) ? 'active' : ''} onClick={() => this.setState({ measure: 1, title: 'Qty' }, () => this.props.getGraphData({ 'status': (this.state.status == 3 || this.state.status == 4) ? 1 : this.state.status, 'measure': 1, 'type': this.state.type }))}>Quantity</li>
                                        <li className={(this.state.measure == 2) ? 'active' : ''} onClick={() => this.setState({ measure: 2, title: 'Dollar' }, () => this.props.getGraphData({ 'status': (this.state.status == 3 || this.state.status == 4) ? 1 : this.state.status, 'measure': 2, 'type': this.state.type }))}>Dollar Value</li>
                                    </ul>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                {/* compoBoxDiv__ ends */}


                <div className='compoBoxDiv__ d-flex row justify-content-center'>

                    <div className='col-lg-4'>
                        <div className='compBox text-center colored'>
                            <p><strong>Invoices Processed last 24 hrs</strong></p>
                            <h1 className='amts'>{typeof (processedInvoice) != 'undefined' ? processedInvoice.count : 0}</h1>

                        </div>
                    </div>

                    <div className='col-lg-4'>
                        <div className='compBox text-center colored'>
                            <p><strong>Invoices Processed till date</strong></p>
                            <h1 className='amts'>{typeof (processedInvoiceTill) != 'undefined' ? processedInvoiceTill.count : 0}</h1>

                        </div>
                    </div>
                </div>


                <div className='compoBoxDiv__ d-flex row'>

                    <div className='col-lg-4'>
                        <div className='compBox text-center'>
                            <p><strong>Pending Invoices</strong></p>
                            <h1 className='amts'>{typeof (dashboardCount) != 'undefined' ? dashboardCount.Pending.count : 0}</h1>
                            <Link to={'/pendinginvoices'}>
                                <button className='cmn-btn1 vw_btns'>Start</button>
                            </Link>
                        </div>
                    </div>

                    <div className='col-lg-4'>
                        <div className='compBox text-center'>
                            <p><strong>Duplicate Invoices</strong></p>
                            <h1 className='amts'>{typeof (dashboardCount) != 'undefined' ? dashboardCount.Duplicate.count : 0}</h1>
                            <Link to={'/duplicateinvoices'}>
                                <button className='cmn-btn1 vw_btns'>Start</button>
                            </Link>
                        </div>
                    </div>

                    <div className='col-lg-4'>
                        <div className='compBox text-center'>
                            <p><strong>Invoices for follow up</strong></p>
                            <h1 className='amts'>{typeof (dashboardCount) != 'undefined' ? dashboardCount.Followup.count : 0}</h1>
                            <Link to={'/followup'}>
                                <button className='cmn-btn1 vw_btns'>Start</button>
                            </Link>
                        </div>
                    </div>


                </div>
                {/* compoBoxDiv__ ends */}

                <h1 className='hdng2'><strong>Summary of Invoices by Participants</strong></h1>

                <div className='row'>


                    <div className='col-7'>

                        <div className='cstm_select search_filter brdr_slct'>
                            {
                                // <Select
                                //     name="view_by_status "
                                //     required={true} simpleValue={true}
                                //     searchable={true} Clearable={false}
                                //     placeholder="| Search for Users, Dates and Categories"
                                //     options={options}
                                //     onChange={(e) => this.setState({ filterVal: e })}
                                //     value={this.state.filterVal}
                                // />
                            }
                            <form onSubmit={this.onSubmitSearch}>
                                <div className='cstm_select_fINacne'>
                                    <input type="text" placeholder="| Sync Logs" name="sync_search"
                                        value={this.state.search || ''} onChange={(e) => this.setState({ 'search': e.target.value })} />
                                    <button onClick={this.onSubmitSearch} type="submit"><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </form>

                        </div>

                    </div>




                </div>
                {/* row ends */}

                <div className="data_table_cmn dashboard_Table text-center">
                    <ReactTable
                        data={membersData}
                        defaultPageSize={10}
                        minRows={1}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                        pageSizeOptions={[10, 20, 30, 40, 50]}
                    />
                </div>
                {/* table comp ends */}

            </AuthWrapper>

        );
    }
}




const mapStateToProps = state => {
   
    return {
        dashboardCount: state.Reducer1.countData,
        membersData: state.Reducer1.summary,
        processedInvoice: state.Reducer1.processedInvoice,
        processedInvoiceTill: state.Reducer1.processedInvoiceTill,
        graphData: state.Reducer1.graphdata
    }
};
// const mapDispatchToProps = {
//   dashboardCountData,
//   get_members_summary,
//   get_processed_invoice,
//   get_graph_data
// }

export default connect(mapStateToProps, { dashboardData, fetch_invoice, python_script, getGraphData })(Dashboard);
// export default Dashboard;
