import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import DatePicker from "react-datepicker";
import  {importCSV, syncLogs,csvBatchesExport, processCsv,loader} from '../store/actions';
import { BASE_URL } from '../config.js';
import { connect } from 'react-redux';
class ImportCSV extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,
            selectedFile: null,
            filename:'',
            importedFile:false,
            fileError:false,
            c_batch:[],
            p_batch:[],
            success_msg:'',
            error_msg:'',
            checkAll: true,
            cselectAll:0,
            cselected: {},
            selectAll:0,
            selected: {},


        }
    }

    fileChangedHandler = (event) => {
      event.preventDefault();
      if(event.target.files[0].type=='application/vnd.ms-excel'){
        this.setState({ selectedFile: event.target.files[0], filename: event.target.files[0].name, importedFile:true, error_msg:''})
      } else {
        this.setState({importedFile:false,  error_msg:'Please Upload Csv File Only'})
      }
    }

    toggleRow = (id,tbl) => {
      if(tbl == 'prev'){
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
          selected: newSelected,
          selectAll: 0,
        });
      }else{
        const newcSelected = Object.assign({}, this.state.cselected);
        newcSelected[id] = !this.state.cselected[id];
        this.setState({
          cselected: newcSelected,
          cselectAll: 0
        });
      }

   }

   toggleSelectAll = (tbl) =>  {
     if(tbl=='prev'){
       let newSelected = {};
       if (this.state.selectAll === 0) {
         if(typeof(this.state.p_batch)!='undefined')
         this.state.p_batch.forEach(x => {
           newSelected[x.id] = true;
         });
       }
       this.setState({
         selected: newSelected,
         selectAll: this.state.selectAll === 0 ? 1 : 0,
       });
     }else{
       let newcSelected = {};
       if (this.state.cselectAll === 0) {
         if(typeof(this.state.c_batch)!='undefined')
         this.state.c_batch.forEach(x => {
           newcSelected[x.id] = true;
         });
       }
       this.setState({
         cselected: newcSelected,
         cselectAll: this.state.cselectAll === 0 ? 1 : 0
       });
     }

   }
   csvDownload = () => {
     let approvedSelectedList =[];
     let data = this.state.selected;
     let cdata = this.state.cselected;
    //  console.log(this.state.p_batch)
    //  console.log(this.state.c_batch)
     if(this.state.p_batch !=undefined)
     {

       this.state.p_batch.forEach(function(k){
         if(data.hasOwnProperty(k.id) && data[k.id]){
           approvedSelectedList.push(k)
         }
       })
     }
     if(this.state.c_batch !=undefined)
     {

       this.state.c_batch.forEach(function(k){
         if(cdata.hasOwnProperty(k.id) && cdata[k.id]){
           approvedSelectedList.push(k)
         }
       })
     }
     var requestData = { approvedSelectedList: approvedSelectedList };
     this.props.csvBatchesExport(requestData)
     .then(json => {
         if (json.status == true) {
           window.location.href = BASE_URL + json.csv_url;
         }
     })
  }
      componentWillMount(){
        this.props.syncLogs().then(res => {
            this.setState({c_batch:res.current_batch,p_batch:res.previous_batches});
        });
      }

      uploadImporCsvHandler = (e) => {
         e.preventDefault();
         //console.log(this.state.selectedFile);
         if(this.state.selectedFile!=null){
           this.props.importCSV(this.state.selectedFile).then(res => {
             if(res.status==true){
              this.setState({fileUpload:true,returned_invoice_id:res,fileError:false,success_msg:""});

            } else {
               this.setState({fileError:true});
            }
            this.props.syncLogs().then(res => {
                this.setState({c_batch:res.current_batch,p_batch:res.previous_batches});
            });
           });
         }
       }

      processCsv = (id) => {
        this.props.processCsv(id)
          .then(json => {
          if(json.status==true){
            this.props.syncLogs().then(res => {
                this.setState({c_batch:res.current_batch,p_batch:res.previous_batches});
            });
          }
          this.props.loader(false);
        });
      }


    render() {

        const cbatchColumn = [
          {Header: (props) =>
              <label className='cstmChekie clrTpe2'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.cselectAll === 1}
                      ref={input => {
                          if (input) {
                              input.indeterminate = this.state.cselectAll === 2;
                          }
                      }}
                      onChange={() => this.toggleSelectAll('curr')}
                   />
                  <div className="chkie"></div>
              </label>,
              accessor: "",
              width: 70,
              sortable: false,
              filterable: false,
              Cell: (props) =>
              <label className='cstmChekie clrTpe3'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.cselected[props.original.id] === true}
                      onChange={() => this.toggleRow(props.original.id,'curr')}
                  />
                  <div className="chkie"></div>
              </label>,

          },
            { Header: 'Batch Id', accessor: 'id' },
            { Header: 'Date of Upload', accessor: 'date_of_upload' },
            { Header: 'Date of Completed', accessor: 'date_of_complete' },
            { Header: 'Record Moved to Follow up', accessor: 'no_of_records_to_followup' },
            { Header: 'Records Moved to Xero', accessor: 'no_of_records_to_xero' },
            { Header: 'Total in Csv', accessor: 'total_record_in_csv' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                              (props.original.status  == 2) ? <div className='stat_grp'><span className='status duplicate'>Failed</span>
                             <span onClick={()=>this.processCsv(props.original.id)}>
                                 Restart
                             </span>

                              </div>:
                              (props.original.status  == 3) ? <span className='status inQueue'>Processing</span>:
                              (props.original.status == 1) ? <span className='status paid'>Successful</span>:''
                        }
                    </div>
            },
        ]
        const batchColumn = [
          {Header: (props) =>
              <label className='cstmChekie clrTpe2'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selectAll === 1}
                      ref={input => {
                          if (input) {
                              input.indeterminate = this.state.selectAll === 2;
                          }
                      }}
                      onChange={() => this.toggleSelectAll('prev')}
                   />
                  <div className="chkie"></div>
              </label>,
              accessor: "",
              width: 70,
              sortable: false,
              filterable: false,
              Cell: (props) =>
              <label className='cstmChekie clrTpe3'>
                  <input
                      type="checkbox"
                      className=" "
                      checked={this.state.selected[props.original.id] === true}
                      onChange={() => this.toggleRow(props.original.id,'prev')}
                  />
                  <div className="chkie"></div>
              </label>,

          },
            { Header: 'Batch Id', accessor: 'id' },
            { Header: 'Date of Upload', accessor: 'date_of_upload' },
            { Header: 'Date of Completed', accessor: 'date_of_complete' },
            { Header: 'Record Moved to Follow up', accessor: 'no_of_records_to_followup' },
            { Header: 'Records Moved to Xero', accessor: 'no_of_records_to_xero' },
            { Header: 'Total in Csv', accessor: 'total_record_in_csv' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                              (props.original.status  == 2) ? <span className='status duplicate'>Failed</span>:
                              (props.original.status  == 3) ? <span className='status inQueue'>Processing</span>:
                              (props.original.status == 1) ? <span className='status paid'>Successful</span>:''
                        }
                    </div>
            },
        ]

        const isValidCsv = this.state.importedFile;
        let proccedButton;

        if (isValidCsv) {
          proccedButton = <input type="submit"  className="browser_btn" style={{width:'200px'}} value={'Processed File'} name="content"   onClick={(e) => this.uploadImporCsvHandler(e)} />;
        }

       return (
            <AuthWrapper>
                <div className=" back_col_cmn-">
                <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                  {// <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                  }
                </div>
                <div className="main_heading_cmn-">
                    <h1>
                        <span>Import CSV</span>

                    </h1>
                </div>
                <form id="ndis_upload_csv"  autoComplete="off">
                <div className="row mt-5 d-flex  mb-5">
                    <div className="col-xl-5 col-lg-6 col-md-6  col-sm-12">
                        <div className="File-Drag-and-Drop_bOX">
                            <label>
                                <input type="file"  name="ndis_upload_csv" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="csv" />
                                <div className="Drop_bOX_content_1">
                                    {/* <i className="icon icon-upload-img"></i> */}
                                    <i className="icon ic-upload-img"></i>
                                    <span>
                                        <h3>Drag & Drop</h3>
                                        <div>or <span>Browser</span> for your files</div>
                                        <a className="browser_btn">Browser</a>
                                    </span>
                                </div>
                                {/* <div className="Drop_bOX_content_2">
                                    <i className="icon icon-document3-ie"></i>
                                    <span>
                                        <div>for your files</div>
                                    </span>
                                </div> */}
                            </label>
                        </div>
                    </div>
                    <div className="col-xl-7 col-lg-6 d-flex flex-column justify-content-between">
                        <div className="FileName_show_div">
                            <h6>File Title</h6>
                            <p>NDIS Upload CSV</p>
                        </div>
                        <div className="Errors_show_div">
                        {(this.state.error_msg!='')?
                          <p><span>{this.state.error_msg}</span><i className="icon icon-block-im"></i></p>:
                          ''
                          // <a className="browser_btn" style={{width:'200px'}}>Processed File</a>
                        }
                        </div>

                        <div className="Success_show_div">
                            {(this.state.importedFile)?
                            <p><span>Successfully Uploaded </span><i className="icon icon-accept-approve2-ie"></i></p>:
                            ' '
                            }

                            {(this.state.success_msg!='')?
                              <p><span>{this.state.success_msg}</span><i className="icon icon-accept-approve2-ie"></i></p>:
                              ''

                              // <a className="browser_btn" style={{width:'200px'}}>Processed File</a>
                            }
                            {proccedButton}


                        </div>
                    </div>

                </div>
                </form>

                <div className="Sub_compon_head">
                    <h3><strong>Current Batch:</strong></h3>
                </div>
                <div className="data_table_cmn dashboard_Table pending_invTable transFirst_tble text-center" >


                    <ReactTable
                        data={this.state.c_batch}
                        defaultPageSize={5}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={cbatchColumn}
                        showPagination={false}
                        pageSizeOptions= {[10, 20, 30, 40, 50]}
                    />

                </div>
                {/* table comp ends */}

                <div className="Sub_compon_head">
                    <h3><strong>Previous Batches:</strong></h3>
                </div>


                <div className="data_table_cmn dashboard_Table pending_invTable transFirst_tble text-center" >

                    <ReactTable
                        data={this.state.p_batch}
                        defaultPageSize={5}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={batchColumn}
                        showPagination={false}

                    />

                </div>
                {/* table comp ends */}

                <div className="text-right">
                     <button className="btn hdng_btn cmn-btn1"  onClick={() => this.csvDownload()}>Export CSV &nbsp;   &nbsp;<i className="icon icon-download2-ie"></i></button>
                </div>

            </AuthWrapper>

        );
    }
}


const mapStateToProps = state => {
return {
getsynclogs: state.InvoiceReducer.synclogsdata,
csvDownload:state.InvoiceReducer.batchCsvDownload

  }
};


export default connect(mapStateToProps,{importCSV,syncLogs,csvBatchesExport,processCsv,loader})(ImportCSV);
//export default ImportCSV;
