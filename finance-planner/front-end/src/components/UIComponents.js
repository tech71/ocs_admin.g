import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Bar } from 'react-chartjs-2';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import * as CT from '../hoc/Color';
import NotifyPopUp from './utils/NotifyPopUp';

class UIComponents extends Component {


    constructor(props) {
        super(props);
        this.state = {
            startDate: null,
            paidPopUp: false,
            followPopUp: false,
            archivePopUp: false,
            dupliPopUp: false
        };

    }


    htmlTocanvas = () => {
        // const {body} = document;
        const canvas = document.createElement('canvas');
        const canva = document.getElementById('canvas');
        const ctx = canva.getContext('2d');

        // canvas.height = 50;
        // canvas.width = 200;

        canva.height = 50;
        canva.width = 200;
        const tempImg = document.createElement('img')
        tempImg.addEventListener('load', onTempImageLoad)
        tempImg.src = 'data:image/svg+xml,' + encodeURIComponent('<svg xmlns="http://www.w3.org/2000/svg" width="200" height="100"><foreignObject width="100%" height="100%"><div xmlns="http://www.w3.org/1999/xhtml"><style>em{color:red;}</style><em>Bill Total</em> <b>$1247.52</b></div></foreignObject></svg>')

        const targetImg = document.createElement('img');
        // const imgDiv = document.getElementById('imgDiv')
        // imgDiv.appendChild(targetImg)

        function onTempImageLoad(e){
            ctx.drawImage(e.target, 0, 0)
            targetImg.src = canva.toDataURL()
        }
    }

    render() {


        const Recruitmentdata = {
            labels: ['July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
            datasets: [{
                data: ['50', '40', '55', '85', '75', '80', '85', '90', '80', '70', '50', '40', '0', '100'],
                backgroundColor: [CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1, CT.COLOR1],

            }]

        };

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        const hdrColumn = [
            {

                Header: '',
                accessor: "",
                width: 50,
                Cell: (props) =>
                    <label className='cstmChekie'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: "Alert Details", accessor: 'alert' },
            { Header: "Date", accessor: 'date' },
            { Header: "Category", accessor: 'category' },
            { Header: "User", accessor: 'user' },
            {

                Header: 'Action',
                Cell: (props) =>
                    <div className='tble_btn_grp'>
                        <button>View</button>
                        <button>Dismiss</button>
                    </div>


            },

        ]

        const dashboardData = [
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',},
            {alert: 'Invoice System over 7 years',date: '01/02/03',category: 'Reminder',user: 'NA',}
        ]


        const dispColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'Type', accessor: 'type' },
            { Header: 'Due Date', accessor: 'date' },
            { Header: 'User', accessor: 'user' },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 40,
                headerStyle: { border: "0px solid #fff" },
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="rec-table-icon">
                        {isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1"></i>
                            : <i className="icon icon-arrow-right icn_ar1"></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                },

            }


        ]

        


        return (

            <AuthWrapper>



                <div className='compoBoxDiv__ d-flex row'>

                    <div className='col-sm-12'>
                        <div className='compBox'>
                            <p className='grpMes cmn_clr1'><strong>Qty</strong></p>
                            <div className='grph_dv'>
                                <Bar data={Recruitmentdata} height={350} legend={null}
                                    options={{
                                        maintainAspectRatio: false
                                    }}
                                />
                            </div>
                            <p className='grpMes'><strong>Month</strong></p>


                            <div className='row'>

                                <div className='col-md-3'></div>
                                <div className='col-md-6'>
                                    <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                        <span><strong>Select:</strong></span>
                                        <button className='cmn-btn1 pd_bts active'>Paid</button>
                                        <button className='cmn-btn1 pd_bts'>Duplicate</button>
                                        <button className='cmn-btn1 pd_bts'>Automated</button>
                                    </div>
                                </div>
                                <div className='col-md-3'>
                                    <ul className='grph_opts2'>
                                        <li className='active'>Quantity</li>
                                        <li>Dollar Value</li>
                                    </ul>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                {/* compoBoxDiv__ ends */}


                <div className='compoBoxDiv__ d-flex row justify-content-center'>

                    <div className='col-lg-4'>
                        <div className='compBox text-center colored'>
                            <p><strong>Invoices Processed last 24 hrs</strong></p>
                            <h1 className='amts'>141</h1>

                        </div>
                    </div>


                    <div className='col-lg-4'>
                        <div className='compBox text-center'>
                            <p><strong>Pending Invoices</strong></p>
                            <h1 className='amts'>141</h1>
                            <Link to='#'>
                                <button className='cmn-btn1 vw_btns'>Start</button>
                            </Link>
                        </div>
                    </div>




                </div>
                {/* compoBoxDiv__ ends */}




                <div className='row'>

                    <div className='col-5'>

                        <div className='cstm_select search_filter brdr_slct'>

                            <Select name="view_by_status "
                                required={true} simpleValue={true}
                                searchable={true} Clearable={false}
                                placeholder="| Search for Users, Dates and Categories"
                                options={options}
                                onChange={(e) => this.setState({ filterVal: e })}
                                value={this.state.filterVal}

                            />

                        </div>

                    </div>

                    <div className='col-3'>

                        <div className='cstm_select  slct_cstm2'>

                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Category"
                                options={options}
                                onChange={(e) => this.setState({ categoryVal: e })}
                                value={this.state.categoryVal}

                            />

                        </div>

                    </div>

                    <div className='col-2'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>From</label>
                            <DatePicker

                                className="csForm_control"
                                selected={this.state.fromDate}
                                onChange={(e) => this.setState({ fromDate: e })}
                                dateFormat="DD/MM/YYYY"
                                name="tempDueDate"
                                placeholderText={'DD/MM/YY'}
                            />
                        </div>
                    </div>

                    <div className='col-2'>
                        <div className='d-flex align-items-center'>
                            <label className='pd_r_10'>To</label>
                            <DatePicker

                                className="csForm_control"
                                selected={this.state.toDate}
                                onChange={(e) => this.setState({ toDate: e })}
                                dateFormat="DD/MM/YYYY"
                                name="tempDueDate"
                                placeholderText={'DD/MM/YY'}
                            />
                        </div>
                    </div>

                </div>
                {/* row ends */}

                <div className="data_table_cmn dashboard_Table ">
                    <ReactTable
                        data={dashboardData}
                        defaultPageSize={dashboardData.length}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                    />
                </div>
                {/* table comp ends */}

                <div className='row'>
                    <div className='col-xl-12 col-lg-12 col-12 mr_tb_15'>

                        <div className='disNotBox1__'>
                            <div className='hdngDs'>Dispute Notes</div>
                            <div className="data_table_cmn text-center dispute_table " >
                                <ReactTable
                                    data={dashboardData}
                                    showPagination={false}
                                    defaultPageSize={dashboardData.length}
                                    className="-striped -highlight"
                                    previousText={<span className="icon icon-arrow-1-left previous"></span>}
                                    nextText={<span className="icon icon-arrow-1-right next"></span>}
                                    columns={dispColumn}
                                    SubComponent={(ev) =>
                                        <div className='dispSubComp__'>
                                            <p>lorem ipsum is a free text made for all free printing and typesetting works for f
                                                ree.orem ipsum is a free text made for all free printing and typesetting works for free
                                                orem ipsum is a free text made for all free printing and typesetting works for free
                                            </p>
                                            <i className='icon icon-view2-ie vw_dips'></i>
                                        </div>
                                    }
                                />
                            </div>
                            {/* table comp ends */}

                        </div>

                    </div>

                </div>

                <div className='bor_bot1 mr_tb_15'></div>

                <div className='row pd_tb_15'>


                    <div className='col-sm-12'><h2><strong>Input Fields</strong></h2></div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1'>Input A</label>
                            <div className='csaddOn_fld1__ '>
                                <input type="text" className="csForm_control filled" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1'>Input B</label>
                            <div className='csaddOn_fld1__ '>
                                <input type="text" className="csForm_control completed" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input C</label>
                            <div className='csaddOn_fld1__ checkedIc1'>
                                <input type="text" className="csForm_control completed" name="" />
                            </div>
                        </div>
                    </div>


                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input D</label>
                            <div className='csaddOn_fld1__ pendingIc'>
                                <input type="text" className="csForm_control pending" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input E</label>
                            <div className='csaddOn_fld1__ pendingIc2'>
                                <input type="text" className="csForm_control pending" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input F</label>
                            <div className='csaddOn_fld1__ errorIc'>
                                <input type="text" className="csForm_control errorIn" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input G</label>
                            <div className='csaddOn_fld1__ errorIc'>
                                <input type="text" className="csForm_control errorIn2" name="" />
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-4 col-12'>
                        <div className="csform-group">
                            <label className='inpLabel1 '>Input h</label>
                            <div className='csaddOn_fld1__ searchIc'>
                                <input type="text" className="csForm_control " name="" />
                            </div>
                        </div>
                    </div>


                </div>
                <div className='bor_bot1 mr_tb_15'></div>

                <div className='row'>

                    <div className='col-sm-12'><h2><strong>Notify Popup</strong></h2></div>

                    <div className='col-md-12'>


                        <button className='btn btn-success mr_r_15' onClick={() => { this.setState({ paidPopUp: true }) }}>Paid Notify PopUp</button>

                        <button className='btn btn-warning mr_r_15' onClick={() => { this.setState({ followPopUp: true }) }}>Follow Up Notify PopUp</button>

                        <button className='btn btn-warning mr_r_15' onClick={() => { this.setState({ dupliPopUp: true }) }}>Associate Duplicate Notify PopUp</button>

                        <button className='btn btn-danger mr_r_15' onClick={() => { this.setState({ archivePopUp: true }) }}>Archive Notify PopUp</button>

                    </div>
                </div>

                <div className='bor_bot1 mr_tb_15'></div>

                <div className='row'>

                    <div className='col-sm-12'>
                        <h2><strong>Button Group</strong></h2>
                        <button className='btn cmn-btn1 mr_r_15'>Button 1</button>
                        <button className='btn cmn-btn2 mr_r_15'>Button 2</button>
                        <button className='btn cmn-btn3 mr_r_15'>Button 3</button>
                        <button className='btn cmn-btn4 mr_r_15'>Button 4</button>
                        <button className='btn cmn-btn5 mr_r_15'>Button 5</button>
                        <button className='btn cmn-btn6 mr_r_15'>Button 6</button>
                        <button className='btn cmn-btn7 mr_r_15'>Button 7</button>
                        <button className='btn cmn-btn8 mr_r_15'>Button 8</button>

                    </div>

                </div>

                <div className='bor_bot1 mr_tb_15'></div>

                <div className='row'>

                    <div className='col-sm-12'><h2><strong>Checkbox Group</strong></h2></div>

                    <div className='col-sm-12'>

                        <label className='cstmChekie mr_r_15'>
                            <input type="checkbox" className=" " />
                            <div className="chkie"></div>
                        </label>

                        <label className='cstmChekie clrTpe2 mr_r_15'>
                            <input type="checkbox" className=" " />
                            <div className="chkie"></div>
                        </label>

                        <label className='cstmChekie clrTpe3 mr_r_15'>
                            <input type="checkbox" className=" " />
                            <div className="chkie"></div>
                        </label>

                    </div>

                </div>

                <div className='bor_bot1 mr_tb_15'></div>

                <div className='row'>

                    <div className='col-sm-12'>

                        <h2><strong>HTML TO IMAGE</strong></h2>
                                    
                        <div>
                            <canvas id='canvas' ></canvas>
                        </div>
                        <div id='imgDiv'></div>
                         <button className='btn cmn-btn2' onClick={()=>this.htmlTocanvas()}>convert</button>               

                    </div>
                
                </div>


                <NotifyPopUp 
                    notifyColor='green' 
                    show={this.state.paidPopUp} 
                    Close={() => { this.setState({ paidPopUp: false }) }}
                    iconLef={'icon-approved2-ie'}
                    btnName={"Undo"}
                >

                        Invoice <span className='txt_clr'>000 000 000</span> marked as paid

                </NotifyPopUp>


                <NotifyPopUp 
                    notifyColor='yellow' 
                    show={this.state.followPopUp} 
                    Close={() => { this.setState({ followPopUp: false }) }}
                    iconLef={'icon-error-icons'}
                    btnName={"Undo"}
                >
 
                        Invoice <span className='txt_clr'>000 000 000</span> moved to follow up
                </NotifyPopUp>

                <NotifyPopUp 
                    notifyColor='yellow' 
                    show={this.state.dupliPopUp} 
                    Close={() => { this.setState({ dupliPopUp: false }) }}
                    iconLef={'icon-error-icons'}
                    btnName={"View"}
                >
                   
                        <div><strong>This invoice has an associate duplicate</strong></div>
                        <h6 className='sml_msg'>Click here to view the archieved record</h6>


                </NotifyPopUp>

                <NotifyPopUp 
                    notifyColor='red' 
                    show={this.state.archivePopUp} 
                    Close={() => { this.setState({ archivePopUp: false }) }}
                    iconLef={'icon-error-icons'}
                    btnName={"Undo"}
                >
                        Invoice <span className='txt_clr'>000 000 000</span> has been archieved
                 
                </NotifyPopUp>

            </AuthWrapper>

        );
    }
}





export default UIComponents;
