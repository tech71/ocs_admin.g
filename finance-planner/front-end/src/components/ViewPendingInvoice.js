import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { addDays } from 'date-fns';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import PdfViewer from './utils/PdfViewer';
import jQuery from "jquery";
import moment from 'moment';
import AuthWrapper from '../hoc/AuthWrapper';
import NotifyPopUp from './utils/NotifyPopUp';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { invoiceDetails, pdfData, loader, invoice_status, invoice_exsits, update_invoice_status, biller_refernce_check, getNewDate, getOptionsCrmParticipantId } from '../store/actions';
import Modal from './utils/Modal';
import { getOptionsParticipant, getOptionsCompany } from '../service/common.js';
import { confirmAlert } from 'react-confirm-alert';
import AddCompany from './AddCompany';
import '../index.css';

class ViewPendingInvoices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            nodatafound: false,
            startDate: null,
            processModal: false,
            paidPopUp: false,
            followPopUp: false, invoicedetails: [],
            redirect: false, confirmExit: false,
            invoiceModal: false, redirect_duplicate: false, p_type: 0, setup_company: false, biller_name: '',
            validate: {
                company_name: { error: false, message: '' }, invoice_number: { error: false, message: '' }, participant_firstname: { error: false, message: '' },
                invoice_amount: { error: false, message: '' }, due_date: { error: false, message: '' }, invoice_date: { error: false, message: '' }, reference_no_error: { error: false, message: '' },
                biller_code_reference_no_check: { error: false, message: '' }, billpay_error: { error: false, message: '' }, ref_no_error: { error: false, message: '' }, bsb_error: { error: false, message: '' }, account_name_error: { error: false, message: '' },
                account_no_error: { error: false, message: '' }
            }
        }

    }

    componentWillMount() {
        this.props.loader(true);
        let invoicedetails = [];
        this.props.getOptionsCrmParticipantId();
        this.props.invoiceDetails(this.props.match.params.id)
            .then(json => {
                if (json.status) {
                    invoicedetails = json.data;
                    invoicedetails['company_name'] = { label: json.data.company_name, value: json.data.company_name }
                    this.setState({ invoicedetails: invoicedetails, biller_name: invoicedetails.biller_name, p_type: json.data.payment_method, validate: invoicedetails.validate }, () => this.props.loader(false));
                    this.props.pdfData(json.data);
                } else {
                    this.props.loader(false);
                    this.setState({ nodatafound: true });
                }
            });
    }

    onInvoiceNoChange = (invoice_no) => {
        this.setState({ invoiceModal: true });
    }
    selectChange = (selectedOption, fieldname) => {
        var state = this.state.invoicedetails;
        state[fieldname] = selectedOption;
        state[fieldname + '_error'] = false;
        

        if (fieldname == 'invoice_date') {
            let state2 = this.state.validate.invoice_date;
                state2['error'] = false;
                state2['message'] = '';
            this.setState(state2);
        }
        else if (fieldname == 'due_date') {
            let state2 = this.state.validate.due_date;
                state2['error'] = false;
                state2['message'] = '';
            this.setState(state2);
        }
        else if (fieldname == 'biller_code') {
            let state3 = this.state.validate.biller_code_reference_no_check;
            this.props.biller_refernce_check(state['biller_code'], state['reference_no']).
                then(json => {
                    state3['error'] = json.status;
                    state3['message'] = '';
                    this.setState(state3);
                    if (json.status) {
                        this.setState({ biller_name: json.data })
                    }
                })
            state[fieldname] = selectedOption;
        }
        else if (fieldname == 'invoice_amount') {
            const re = /^[0-9]*([.,][0-9]+)?$/;
            let state4 = this.state.validate.invoice_amount;
            if (selectedOption < 0) {
                state4['error'] = true;
                state4['message'] = 'Invoice Amount should be greater than 0';
            } else if (!re.test(selectedOption)) {
                state4['error'] = true;
                state4['message'] = 'Invoice Amount should be number only';
            }
            else if (parseFloat(this.state.invoicedetails.funds) < parseFloat(this.state.invoicedetails.invoice_amount)) {
                state4['error'] = true;
                state4['message'] = "Participant's balance " + this.state.invoicedetails.funds + " is not sufficient to clear the invoice amount " + this.state.invoicedetails.invoice_amount;
            }
            else {
                state4['error'] = false;
                state4['message'] = '';
            }
            this.setState(state4);
        }
        else if (fieldname == 'reference_no') {

            var status2 = true;
            let ref = state.reference_no;
            let state1 = this.state.validate;
            if (ref.length == 0 || ref.length > 15) {
                status2 = false;
                state1['reference_no_error']['error'] = false;
                state1['reference_no_error']['message'] = 'Reference Number should be in between 1 to 15';
            } else {
                state1['reference_no_error']['error'] = true;
                state1['reference_no_error']['message'] = '';
                status2 = true;
            }
        }
        else {
            state[fieldname] = selectedOption;
        }
        this.setState(state);
    }
    custom_validation = () => {
        let status = true;
        let status2 = true;
        let status3 = true;
        let status4 = false;
        let p_type = this.state.p_type;
        let state1 = this.state.validate;
        state1['biller_code_reference_no_check']['message'] = ''; state1['reference_no_error']['message'] = ''; state1['billpay_error']['message'] = '';
        state1['ref_no_error']['message'] = ''; state1['bsb_error']['message'] = ''; state1['account_name_error']['message'] = '';
        state1['account_no_error']['message'] = '';
        switch (parseInt(p_type)) {
            case 1:
                let biller_code = typeof (this.state.invoicedetails.biller_code) != 'undefined' ? this.state.invoicedetails.biller_code : '';
                let ref = typeof (this.state.invoicedetails.reference_no) != 'undefined' ? this.state.invoicedetails.reference_no : '';
                // let ref = this.state.invoicedetails.reference_no;
                if (biller_code.length == 0 || biller_code.length > 15) {
                    status = false;
                    state1['biller_code_reference_no_check']['error'] = false;
                    state1['biller_code_reference_no_check']['message'] = 'Invalid Biller Code';

                } else {
                    state1['biller_code_reference_no_check']['error'] = true;
                    state1['biller_code_reference_no_check']['message'] = '';
                    status = true;
                }
                if (ref.length == 0 || ref.length > 15) {
                    status2 = false;
                    state1['reference_no_error']['error'] = false;
                    state1['reference_no_error']['message'] = 'Reference Number should be in between 1 to 15';
                } else {
                    state1['reference_no_error']['error'] = true;
                    state1['reference_no_error']['message'] = '';
                    status2 = true;
                }

                break;
            case 2:
                let billpay_code = typeof (this.state.invoicedetails.billpay_code) != 'undefined' ? this.state.invoicedetails.billpay_code : '';
                let refe = typeof (this.state.invoicedetails.ref_no) != 'undefined' ? this.state.invoicedetails.ref_no : '';
                if (billpay_code.length == 0 || billpay_code.length > 4) {
                    status = false;
                    state1['billpay_error']['error'] = false;
                    state1['billpay_error']['message'] = "Billpay should be in between 1 to 4 ";
                } else {
                    state1['billpay_error']['error'] = true;
                    status = true;
                    state1['billpay_error']['message'] = "";
                }
                if (refe.length == 0 || refe.length > 15) {
                    state1['ref_no_error']['message'] = "Reference Number should be in between 1 to 15";
                    status2 = false;
                    state1['ref_no_error']['error'] = false;
                } else {
                    state1['ref_no_error']['message'] = "";
                    status2 = true;
                    state1['ref_no_error']['error'] = true;
                }
                break;
            case 3:
                let bsb = this.state.invoicedetails.bsb;
                let acc_no = this.state.invoicedetails.acc_no;
                let acc_name = this.state.invoicedetails.acc_name;
                if (bsb.length == 0) {
                    status = false;
                    state1['bsb_error']['message'] = "BSB Code is required";
                    state1['bsb_error']['error'] = false;
                } else {
                    state1['bsb_error']['error'] = true;
                    status = true;
                    state1['bsb_error']['message'] = "";
                }
                if (acc_name.length == 0) {
                    status2 = false;
                    state1['account_name_error']['error'] = false;
                    state1['account_name_error']['message'] = "Account Name is required";
                } else {
                    state1['account_name_error']['error'] = true;
                    status2 = true;
                    state1['account_name_error']['message'] = "";
                }
                const re = /^\d{3}-?\d{3}$/;
                if (!re.test(acc_no)) {
                    status3 = false;
                    state1['account_no_error']['error'] = false;
                    state1['account_no_error']['message'] = "The length is 6 digits without dash or 7 digits with dash and contains only numbers. ex:123-123 or 123123";
                } else {
                    state1['account_no_error']['error'] = true;
                    state1['account_no_error']['message'] = "";
                    status3 = true;
                }
                break;
            default: status4 = false;
                break;
        }
        if (status && status2 && status3)
            status4 = true;
        this.setState(state1)
        return status4;
    }
    selectChanges = (selectedOption, fieldname) => {

        var state = this.state.invoicedetails;
        let state2 = this.state.validate.participant_firstname;
        let state3 = this.state.validate.company_name;
        state[fieldname] = selectedOption;
        if (fieldname != 'company_name') {
            state['participant_ocs_email_acc'] = selectedOption.email;
            state['participant_lastname'] = { "label": selectedOption.lastname, 'value': selectedOption.lastname };
            state['participant_firstname'] = { "label": selectedOption.firstname, 'value': selectedOption.firstname };
            state['participant_ocs_id'] = { "label": selectedOption.value, 'value': selectedOption.value };
            state2['message'] = '';
            state2['error'] = false;
        } else {
            state['company_name'] = { "label": selectedOption.value, 'value': selectedOption.value };
            state3['message'] = '';
            state3['error'] = false;
        }


        this.setState(state);
        this.setState(state2);
        this.setState(state3);

    }

    selectSearch = (selectedOption, fieldname) => {
        var state = this.state.invoicedetails;
        var state2 = this.state.validate.participant_firstname;
        if (selectedOption != null) {
            state[fieldname] = selectedOption.name;
            state['participant_ocs_email_acc'] = selectedOption.email;
            state['participant_lastname'] = { "label": selectedOption.lastname, 'value': selectedOption.lastname }
            state['participant_firstname'] = { "label": selectedOption.value, 'value': selectedOption.label };
            state['participant_ocs_id'] = { "label": selectedOption.label, 'value': selectedOption.label };
            state2['error'] = false;
            state2['message'] = '';

        }
        else {
            state[fieldname] = '';
            state['participant_ocs_email_acc'] = '';
            state['participant_lastname'] = '';
            state['participant_firstname'] = '';
            state['participant_ocs_id'] = '';
            state2['error'] = false;
            state2['message'] = '';
        }
        this.setState(state2);
        this.setState(state);

    }

    reset_invoice = () => {
        var state = this.state.invoicedetails;
        state['invoice_number'] = '';
        this.setState(state)
    }
    
    invoice_validate = (invoice_number) => {
        if (invoice_number != '' && invoice_number != 'undefined') {
            this.props.invoice_exsits(this.props.match.params.id, invoice_number)
                .then(json => {
                    if (json.status) {
                        return new Promise((resolve, reject) => {
                            confirmAlert({
                                customUI: ({ onClose }) => {

                                    return (
                                        <Modal show={true} >
                                            <div className='modalDialog_mini cstmDialog'>
                                                <div className="hdngModal  pl-2">
                                                    <h5 className='hdngCont'><strong>Confirmation </strong></h5>
                                                </div>
                                                <div className="mb-3  pl-2">An Invoice with the same invoice number already exist in the system. Do you want to check the invoice in the Duplicate section?</div>
                                                <div className="confi_but_div">
                                                    <button className="btn cmn-btn1 dis_btns2" onClick={
                                                        () => {
                                                            this.props.update_invoice_status(this.props.match.params.id, invoice_number)
                                                                .then(json => {
                                                                    if (json.status) {
                                                                        this.setState({ redirect_duplicate: true })
                                                                        onClose();
                                                                    }
                                                                })

                                                        }}>Yes
                                      {/* <i className='icon icon-accept-approve1-ie'></i> */}
                                                    </button>
                                                    <button className="btn cmn-btn2 dis_btns2" onClick={
                                                        () => {
                                                            onClose();
                                                            resolve({ status: false }); this.reset_invoice();
                                                        }}> No
                                        {/* <i className='icon icon-close2-ie'></i> */}
                                                    </button>
                                                </div>
                                            </div>
                                        </Modal>
                                    )
                                }
                            })
                        }
                        );
                    }
                })
        }
    }
    status_update = (status) => {
        let validation = this.custom_validation();
        if (validation) {
            let state = this.state.invoicedetails;
            state['payment_method'] = this.state.p_type;
            this.setState({ invoicedetails: state })
            this.props.invoice_status(status, this.props.match.params.id, state)
                .then(json => {
                    if (json.status) {
                        switch (status) {
                            case 'followUp': this.setState({ followPopUp: true }, () => this.props.loader(false));
                                setTimeout(() => this.setState({ followPopUp: false, redirect: true }), 3000);
                                break;
                            case 'Paid': this.setState({ paidPopUp: true }, () => this.props.loader(false));
                                setTimeout(() => this.setState({ paidPopUp: false, redirect: true }), 3000);
                                break;
                        }
                    }
                    else {
                        this.setState({ invoicedetails: json.data, validate: json.data.validate }, () => this.props.loader(false))
                        this.props.pdfData(json.data);
                    }
                })
        }

    }
    componentDidCatch(error) {
        //console.log(error);
    }
    companySuccess = (name) => {
        let state = this.state.invoicedetails;
        let state2 = this.state.validate.company_name;
        state2['error'] = false;
        state2['message'] = '';
        state['company_name'] = { label: name, value: name }
        this.setState(state);
        this.setState(state2);
    }
    handleClose = () => {
        this.setState({ setup_company: false })
    }
    render() {

        if (this.state.nodatafound) {
            return (<Redirect to='/NotFound' />);
        }

        var validationErrors = [];
        Object.values(this.state.validate).forEach(function (value) {
            validationErrors.push(value.message)
        });

        let participantId = (typeof (this.props.CrmParticipantId) != 'undefined') ? this.props.CrmParticipantId : [{}];
        if (this.state.redirect) {
            return (<Redirect to='/pendinginvoices' />);
        }
        if (this.state.redirect_duplicate) {
            return (<Redirect to={'/duplicateinvoices/viewduplicateinvoice/' + this.props.match.params.id} />);
        }

        const invoice_details = this.state.invoicedetails;
        let service_count = (typeof (invoice_details.validate) != 'undefined') ? invoice_details.validate.service_count.message : '';
        let biller_code_reference_no_error = (typeof (invoice_details.validate) != 'undefined') ? ((typeof (this.props.biller_Status) != 'undefined') ? this.props.biller_Status : invoice_details.validate.biller_code_reference_no_check.error) : false;
       
        const html = '<div>Example HTML string</div>';

        // if(this.state.skip){
        //
        //   return(<Redirect to={'/pendinginvoices/viewpendinginvoice/'+invoice_details.next_id}/>);
        // }

        return (

            <AuthWrapper>


                <div className=" back_col_cmn-">
                    <Link to='/pendinginvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>{invoice_details.r_position}/{invoice_details.r_count} Pending Invoice</span>
                        {  // <Link to={'/pendinginvoices/viewpendinginvoice/'+invoice_details.next_id}>
                            // </Link>
                        }
                        <Link onClick={this.forceUpdate} to={'/pendinginvoices/viewpendinginvoice/' + invoice_details.r_next_id}>
                            <button className="btn hdng_btn cmn-btn1"  >Skip</button>
                        </Link>
                        <div onClick={() => { this.setState({ confirmExit: true }) }}>
                            <button className="btn hdng_btn cmn-btn2">Exit</button>
                        </div>
                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-12 col-12'>

                        <PdfViewer />
                        <div className='clearfix'></div>

                        <div className='mr_t_15 pd_tb_15 d-flex justify-content-center'>
                            <span className='btn flwUp_btn frm_bt23' onClick={() => { this.status_update('followUp') }}>Follow Up</span>
                            <button className='btn cmn-btn3 frm_bt23 ' onClick={() => { this.status_update('Paid') }} disabled={(parseInt(invoice_details.funds) >= (invoice_details.invoice_amount) && invoice_details.invoice_amount > 0) ? false : true} >Approve Invoice <i className='icon icon-accept-approve2-ie'></i></button>
                        </div>


                    </div>


                    <div className='col-xl-5 col-lg-12 col-12'>
                        <form method="POST" id="pending_invoive">
                            <div className='fieldArea'>

                                <div className='mini_profile__ d-flex'>

                                    <div className='pro_ic1'>
                                        <i className='icon icon-userm1-ie'></i>
                                    </div>

                                    <div className='prf_cntPart'>
                                        <h4 className='cmn_clr1'><strong>{invoice_details.Fullname}</strong></h4>
                                        <div>
                                            <h5 className='avl_fnd'>
                                                <strong>Avail Funds :</strong>
                                                <span>${invoice_details.funds}</span>
                                            </h5>
                                        </div>

                                        <div className='d-flex align-items-center'>
                                          
                                            <div className="stat_grp"><span className="status val">{(invoice_details.status == 1) ? 'Active' : 'Inactive'}</span></div>


                                        </div>


                                    </div>

                                </div>

                                {//  <div className='row '>
                                    // <div className="invoice_errors col-md-12">
                                    //        <div>
                                    //            <ul className="error_listie">
                                    //            {
                                    //            validationErrors.map(function (item, i) {
                                    //                // console.log(item)
                                    //                if (item !== undefined && item !== '') {
                                    //                    return (<li key={i}>{item}</li>)
                                    //                }
                                    //            })
                                    //        }
                                    //            </ul>
                                    //        </div>
                                    //       </div>
                                    //    </div>
                                }  {/* mini_profile__ ends */}

                                <div className='bor_tb  pd_tb_10' >
                                    <strong>Participant Details</strong>
                                </div>


                                <div className='row pd_tb_15'>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>HCM Mail Account</label>
                                            <div className='csaddOn_fld1__ '>
                                                <input type="text" className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn2" : "csForm_control completed"} name="" readOnly value={invoice_details.participant_ocs_email_acc || ''} />
                                                <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>HCM ID</label>
                                            <div className='csaddOn_fld1__ cssaddOn_select___'>
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={false}
                                                    searchable={true} Clearable={false}
                                                    placeholder=""
                                                    options={participantId}
                                                    onChange={(e) => this.selectSearch(e, 'participant_ocs_id')}
                                                    value={invoice_details.participant_ocs_id}

                                                />
                                                <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>First Name</label>
                                            <div className='csaddOn_fld1__'>
                                                <div className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn2" : "cstm_select search_filter grnbrdr_slct"}>
                                                    <Select.Async
                                                        cache={false}
                                                        clearable={false}
                                                        name="participant_firstname" required={true}
                                                        value={invoice_details.participant_firstname}
                                                        loadOptions={getOptionsParticipant}
                                                        placeholder='Search'
                                                        onChange={(e) => this.selectChanges(e, 'participant_firstname')}
                                                    />
                                                    <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>                                                  {  // <Select name="view_by_status "
                                                        //     required={true} simpleValue={true}
                                                        //     searchable={true} Clearable={false}
                                                        //     placeholder=""
                                                        //     onChange={(e) => this.setState({ filterVal: e })}
                                                        //     value={invoice_details.participant_firstname}
                                                        //     options={options}
                                                        //
                                                        // />
                                                    }</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Match System Data</label>
                                            <div className={(this.state.validate.participant_firstname.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.validate.participant_firstname.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.validate.participant_firstname.error) ? 'No' : 'Yes'} />
                                                <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Last Name</label>
                                            <div className='csaddOn_fld1__'>
                                                <div className='cstm_select search_filter grnbrdr_slct'>
                                                    {  // <input type="text" name="last_name" className="csForm_control completed" readOnly value={invoice_details.participant_lastname || ''} />
                                                    }{  // <Select name="view_by_status "
                                                        //     required={true} simpleValue={true}
                                                        //     searchable={true} Clearable={false}
                                                        //     placeholder=""
                                                        //     onChange={(e) => this.setState({ filterVal: e })}
                                                        //     value={invoice_details.participant_lastname}
                                                        //
                                                        // />
                                                    }
                                                    <Select.Async
                                                        cache={false}
                                                        clearable={false}
                                                        name="participant_lastname" required={true}
                                                        value={invoice_details.participant_lastname}
                                                        loadOptions={getOptionsParticipant}
                                                        placeholder='Search'
                                                        onChange={(e) => this.selectChanges(e, 'participant_lastname')}
                                                    />
                                                    <span className="error pd_tb_15">{this.state.validate.participant_firstname.message}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                {/* row ends */}

                                <div className='bor_tb  pd_tb_10' >
                                    <strong>Account Details</strong>
                                </div>

                                <div className='row pd_tb_15'>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice Received Date</label>
                                            <div className={(this.state.validate.invoice_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                                <DatePicker
                                                    className={(this.state.validate.invoice_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                    dateFormat="dd/MM/yyyy"
                                                    selected={moment(invoice_details.invoice_date).valueOf()}
                                                    onChange={(e) => this.selectChange(e, 'invoice_date')}
                                                    name="tempDueDate"
                                                    placeholderText={'DD/MM/YYYY'}
                                                    autoComplete={'off'}
                                                    minDate={moment().add(-1, 'years')}
                                                    maxDate={addDays(new Date(), +0)}
                                                />
                                                <span className="error pd_tb_15"></span>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Due Date</label>
                                            <div className={(this.state.validate.due_date.error) ? 'csaddOn_fld1__ errorIc datewid_100' : 'csaddOn_fld1__ checkedIc1 datewid_100'}>
                                                <DatePicker
                                                    className={(this.state.validate.due_date.error) ? "csForm_control no_bor errorIn2" : "csForm_control no_bor completed"}
                                                    selected={moment(invoice_details.due_date).valueOf()}
                                                    onChange={(e) => this.selectChange(e, 'due_date')}
                                                    dateFormat="dd/MM/yyyy"
                                                    name="tempDueDate"
                                                    placeholderText={'DD/MM/YY'}
                                                    autoComplete={'off'}
                                                    minDate={addDays(new Date(), 1)}
                                                    maxDate={addDays(new Date(), 365)}
                                                   
                                                />
                                                <span className="error pd_tb_15"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Company Name</label>
                                            <div className={(this.state.validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__  filled'}>
                                                <Select.Async
                                                    cache={false}
                                                    clearable={false}
                                                    name="company_name" required={true}
                                                    value={invoice_details.company_name}
                                                    loadOptions={getOptionsCompany}
                                                    placeholder='Search'
                                                    onChange={(e) => this.selectChanges(e, 'company_name')}
                                                />
                                                <span className="error pd_tb_15">{this.state.validate.company_name.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Matches Supplier</label>
                                            <div className={(this.state.validate.company_name.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.validate.company_name.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.validate.company_name.error) ? 'No' : typeof (invoice_details.company_name) != 'undefined' ? invoice_details.company_name.label : 'Yes'} />
                                                <em>{service_count}</em>
                                                {
                                                    // this.state.validate.company_name.error?
                                                    // return(<div><p>Company name doesnt match with allowed list,</p<a onClick={()=>this.setState({setup_company:true})}><b>Click here</b></a>  to add in the allowed list.</div>):''}
                                                }

                                                {this.state.validate.company_name.error ? <a onClick={() => this.setState({ setup_company: true })}>Set up Company</a> : ''
                                                }
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice No.</label>
                                            <div className='csaddOn_fld1__ '>
                                                <input type="text" className={(this.state.validate.invoice_number.error) ? "csForm_control errorIn2" : "csForm_control filled"} name="invoice_number" 
                                                 onChange={(e) => { this.selectChange(e.target.value, 'invoice_number'); this.invoice_validate(e.target.value) }} 
                                                 value={invoice_details.invoice_number || ''} />
                                                <span className="error pd_tb_15">{this.state.validate.invoice_number.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Known Biller</label>
                                            <div className={(this.state.validate.invoice_number.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.validate.invoice_number.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.validate.invoice_number.error) ? 'No' : 'Yes'} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Invoice Amount</label>
                                            <div className='csaddOn_fld1__ '>
                                                <input type="text" className={(this.state.validate.invoice_amount.error) ? "csForm_control errorIn2" : "csForm_control filled"} name="" onChange={(e) => this.selectChange(e.target.value, 'invoice_amount')} value={invoice_details.invoice_amount || ''} />
                                                <span className="error pd_tb_15">{this.state.validate.invoice_amount.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className='col-lg-6 col-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Sufficient Available Balance</label>
                                            <div className={(this.state.validate.invoice_amount.error) ? 'csaddOn_fld1__ errorIc' : 'csaddOn_fld1__ checkedIc1'}>
                                                <input type="text" readOnly className={(this.state.validate.invoice_amount.error) ? "csForm_control errorIn" : "csForm_control completed"} name="" value={(this.state.validate.invoice_amount.error) ? 'No' : 'Yes'} />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {/* row ends */}

                                <div className='bor_tb  pd_tb_10' >
                                    <strong>Payment Method</strong>
                                </div>

                                <div className='row'>

                                    {  // <div className='col-12'>
                                        //
                                        //     <div className='inpLabel1'><strong>Payment Type</strong></div>
                                        //     <ul className='py_typeUL'>
                                        //     <li className={(this.state.p_type==1)?'active':''} onClick={()=>this.setState({p_type:1})}>Bank</li>
                                        //     <li className={(this.state.p_type==2)?'active':''} onClick={()=>this.setState({p_type:2})}>B Pay</li>
                                        //     <li className={(this.state.p_type==3)?'active':''} onClick={()=>this.setState({p_type:3})}>AUS Post</li>
                                        //     </ul>
                                        //
                                        // </div>
                                    }


                                    <div className="col-lg-12 col-12">
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Please select one</label>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-12">
                                        <div className="csform-group">
                                            <div className="custom_radio label_l bpay_lab">
                                                <input type="radio" name="radio-group" value={1} readOnly={true} checked={(this.state.p_type == 1) ? true : false} />
                                                <label htmlFor="test1" onClick={() => this.setState({ p_type: 1 })}><strong>Bpay</strong></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.state.p_type == 1) ? 'col-lg-12 col-12 show' : 'col-lg-12 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Biller Name</label>
                                            <div className='csaddOn_fld1__ '>
                                                <p>{this.state.biller_name}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Biller Code</label>
                                            <div className={(biller_code_reference_no_error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" className={(biller_code_reference_no_error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="biller_code" value={(invoice_details.biller_code) ? invoice_details.biller_code : ''} onChange={(e) => this.selectChange(e.target.value, 'biller_code')} data-rule-required="true" data-rule-maxlength="15" maxLength="15" />
                                                <span className="error pd_tb_15">{this.state.validate.biller_code_reference_no_check.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.state.p_type == 1) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Reference Number</label>
                                            <div className={(this.state.validate.reference_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" className={(this.state.validate.reference_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="reference_no" value={(invoice_details.reference_no) ? invoice_details.reference_no : ''} onChange={(e) => this.selectChange(e.target.value, 'reference_no')} data-rule-required="true" data-rule-maxlength="15" maxLength="15" />
                                                <span className="error pd_tb_15">{this.state.validate.reference_no_error.message}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-12">
                                        <div className="csform-group">
                                            <div className="custom_radio label_l bpay_lab">
                                                <input type="radio" name="radio-group" value={2} checked={(this.state.p_type == 2) ? true : false} />
                                                <label for="test1" onClick={() => this.setState({ p_type: 2 })}><strong>Aus Post</strong></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Billpay Code</label>
                                            <div className={(this.state.validate.billpay_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" value={(invoice_details.billpay_code) ? invoice_details.billpay_code : ''} maxLength="4" onChange={(e) => this.selectChange(e.target.value, 'billpay_code')} className={(this.state.validate.billpay_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                                                <span className="error pd_tb_15">{this.state.validate.billpay_error.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.state.p_type == 2) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Reference Number</label>
                                            <div className={(this.state.validate.ref_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" value={(invoice_details.ref_no) ? invoice_details.ref_no : ''} maxLength="15" onChange={(e) => this.selectChange(e.target.value, 'ref_no')} className={(this.state.validate.ref_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                                                <span className="error pd_tb_15">{this.state.validate.ref_no_error.message}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-12 col-12">
                                        <div className="csform-group">
                                            <div className="custom_radio label_l bpay_lab">
                                                <input type="radio" name="radio-group" value={3} checked={(this.state.p_type == 3) ? true : false} />
                                                <label for="test1" onClick={() => this.setState({ p_type: 3 })}><strong>Bank</strong></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>BSB</label>
                                            <div className={(this.state.validate.bsb_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" value={(invoice_details.bsb) ? invoice_details.bsb : ''} onChange={(e) => this.selectChange(e.target.value, 'bsb')} className={(this.state.validate.bsb_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                                                <span className="error pd_tb_15">{this.state.validate.bsb_error.message}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Account Name</label>
                                            <div className={(this.state.validate.account_name_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" value={(invoice_details.acc_name) ? invoice_details.acc_name : ''} onChange={(e) => this.selectChange(e.target.value, 'acc_name')} className={(this.state.validate.account_name_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                                                <span className="error pd_tb_15">{this.state.validate.account_name_error.message}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className={(this.state.p_type == 3) ? 'col-lg-6 col-12 show' : 'col-lg-6 col-12 hidden'}>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Account Number</label>
                                            <div className={(this.state.validate.account_no_error.error) ? 'csaddOn_fld1__ checkedIc1 datewid_100' : 'csaddOn_fld1__  errorIc'}>
                                                <input type="text" value={(invoice_details.acc_no) ? invoice_details.acc_no : ''} onChange={(e) => this.selectChange(e.target.value, 'acc_no')} className={(this.state.validate.account_no_error.error) ? "csForm_control no_bor filled completed" : "csForm_control no_bor  filled errorIn2"} name="" />
                                                <span className="error pd_tb_15">{this.state.validate.account_no_error.message}</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                {/* row ends */}

                                {/*    <div className='row'>

                                  <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Biller Code</label>
                                        <div className={(biller_code_reference_no_error)?'csaddOn_fld1__ checkedIc1 datewid_100':'csaddOn_fld1__  errorIc'}>
                                            <input type="text" className={(biller_code_reference_no_error)?"csForm_control no_bor filled completed":"csForm_control no_bor  filled errorIn2"} name="biller_code" value={(invoice_details.biller_code)?invoice_details.biller_code:''}  onChange={(e) =>this.selectChange(e.target.value,'biller_code')} data-rule-required="true" data-rule-maxlength="15"  maxLength="15"/>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csform-group">
                                        <label className='inpLabel1'>Reference Number</label>
                                        <div className={'csaddOn_fld1__ '}>
                                            <input type="text" className={"csForm_control no_bor  "} name="reference_no" value={(invoice_details.reference_no)?invoice_details.reference_no:''}  onChange={(e) =>this.selectChange(e.target.value,'reference_no')} data-rule-required="true" data-rule-maxlength="15"  data-rule-minlength="15"/>
                                        </div>
                                    </div>
                                </div>



                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>PO Number</label>
                                          <div className='csaddOn_fld1__ notAvail'>
                                              <input type="text" className="csForm_control " name="" value='NA' onChange={(e) =>this.selectChange(e.target.value,'po_number')} />
                                          </div>
                                      </div>
                                  </div>


                              </div>
                               row ends

                              <div className='bor_tb  pd_tb_10 mr_t_15' >
                                  <strong>Member Details</strong>
                              </div>

                              <div className='row pd_tb_15' >

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>HCM Mail Account</label>
                                          <div className='csaddOn_fld1__ '>
                                              <input type="text" className="csForm_control completed" name="" />
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>HCM ID</label>
                                          <div className='csaddOn_fld1__ '>
                                              <input type="text" className="csForm_control completed" name="" />
                                          </div>
                                      </div>
                                  </div>


                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>First Name</label>
                                          <div className='csaddOn_fld1__ '>
                                              <input type="text" className="csForm_control filled" name="" />
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Match System Data</label>
                                          <div className='csaddOn_fld1__ checkedIc1'>
                                              <input type="text" className="csForm_control completed" name="" />
                                          </div>
                                      </div>
                                  </div>

                                  <div className='col-lg-6 col-12'>
                                      <div className="csform-group">
                                          <label className='inpLabel1'>Last Name</label>
                                          <div className='csaddOn_fld1__ '>
                                              <input type="text" className="csForm_control filled" name="" />
                                          </div>
                                      </div>
                                  </div>

                              </div>
                              {/* row ends */}

                            </div>

                            <div className={'processModal ' + (this.state.processModal ? 'show' : '')}>
                                <div className='modalDialog1'>
                                    <h4>Document Awaiting processing click to process now</h4>
                                    <button className='cmn-btn3 scn_btn'>Scan Now</button>
                                </div>
                            </div>



                        </form></div>

                    {/* col-6 ends */}

                </div>
                {/* row ends */}
                <Modal show={this.state.confirmExit} >

                    <div className='modalDialog_mini cstmDialog' >
                        <div className='mr_b_15 hdngModal'>
                            <h5 className='hdngCont'><strong>Are you sure you want to exit? </strong></h5>
                            <i className='icon icon-close2-ie closeIc' onClick={() => this.setState({ confirmExit: false })}></i>
                        </div>

                        <div className='text-right mr_t_60'>
                            <button className='btn cmn-btn2 dis_btns2 ' onClick={() => this.setState({ confirmExit: false })}>
                                <span>
                                    No
                                    <i className='icon icon-close2-ie'></i>
                                </span>
                            </button>
                            <button className='btn cmn-btn1 dis_btns2' onClick={() => this.setState({ redirect: true, confirmExit: false })}>
                                <span>
                                    Yes
                                    <i className='icon icon-accept-approve1-ie' ></i>
                                </span>
                            </button>

                        </div>
                    </div>

                </Modal>
                <NotifyPopUp
                    notifyColor='green'
                    show={this.state.paidPopUp}
                    Close={() => { this.setState({ paidPopUp: false, redirect: true }) }}
                    iconLef={'icon-approved2-ie'}
                // btnName={"Undo"}
                >

                    Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> marked as Approved

                </NotifyPopUp>


                <NotifyPopUp
                    notifyColor='yellow'
                    show={this.state.followPopUp}
                    Close={() => { this.setState({ followPopUp: false, redirect: true }) }}
                    iconLef={"icon-error-icons"}
                // btnName={"Undo"}
                >

                    Invoice <span className='txt_clr'>{invoice_details.invoice_number}</span> moved to follow up

                </NotifyPopUp>
                <AddCompany show={this.state.setup_company} handleClose={this.handleClose} companySuccess={this.companySuccess} />
            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
    return {
        invoice_details: state.InvoiceReducer.invoicedetails,
        pdfData: state.InvoiceReducer.data,
        CrmParticipantId: state.InvoiceReducer.CrmParticipantId
        // InvoiceStatus:InvoiceStatus

    }
};
const mapDispatchtoProps = (dispatch) => bindActionCreators({
    invoiceDetails, pdfData, loader, invoice_status, invoice_exsits, getNewDate, getOptionsCrmParticipantId, update_invoice_status, biller_refernce_check
}, dispatch)
export default connect(mapStateToProps, mapDispatchtoProps)(ViewPendingInvoices);
