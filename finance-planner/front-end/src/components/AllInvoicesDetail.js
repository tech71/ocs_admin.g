import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import { participantInvoice } from '../store/actions';
import Select from 'react-select-plus';

class AllInvoicesDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
        }

    }

    componentWillMount() {
      
        if (typeof (this.props.history.location.state) != "undefined") {
            this.props.participantInvoice(this.props.match.params.id, this.props.history.location.state.status);
        } else
            this.props.participantInvoice(this.props.match.params.id, 0);
    }


    onfilter = (e) => {
       
        this.setState({ filterVal: e });
        // var searchData = { e,this.state.search };
        this.props.participantInvoice(this.props.match.params.id, e);
    }


    render() {
        const { participantInvoiceDetails } = this.props;
        const { participantDetails } = this.props;
        const { plannerData } = this.props;
        const hdrColumn = [
            // {
            //
            //     Header: (props) =>
            //         <label className='cstmChekie clrTpe2'>
            //             <input type="checkbox" className=" " />
            //             <div className="chkie"></div>
            //         </label>,
            //     accessor: "id",
            //     width: 70,
            //     sortable: false,
            //     filterable: false,
            //     Cell: (props) =>
            //         <label className='cstmChekie clrTpe3'>
            //             <input type="checkbox" className=" " />
            //             <div className="chkie"></div>
            //         </label>,
            //
            // },
            { Header: 'Due Date', accessor: 'due_date' },
            { Header: 'Biller', accessor: 'biller' },
            { Header: 'Invoice Amount', accessor: 'invoice_amount' },
            { Header: 'Invoice Number', accessor: 'invoice_number' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                        {
                            (props.original.status === 'Duplicate') ? <Link to={'/duplicateinvoices/viewduplicateinvoice/' + props.original.id}> <span className='status duplicate'>Duplicate</span></Link>
                                :
                                (props.original.status === 'PotentialDuplicate') ? <Link to={'/duplicateinvoices/viewduplicateinvoice/' + props.original.id}> <span className='status duplicate'>Potential Duplicate</span></Link>
                                    :
                                    (props.original.status === 'inQueue') ? <Link to={'/pendinginvoices/viewpendinginvoice/' + props.original.id}><span className='status inQueue'>In Queue</span></Link>
                                        :
                                        (props.original.status === 'followUp') ? <Link to={'/followup/viewfollowup/' + props.original.id}><span className='status followUp'>Follow Up</span></Link>
                                            : (props.original.status === 'Approved') ? <Link to={'/approvedinvoices'}><span className='status paid'>Approved</span></Link>
                                                : ' '
                        }

                    </div>
            }

        ]

        var options = [
            { value: '2', label: 'Potential Duplicate' },
            { value: '5', label: 'Duplicate' },
            { value: '1', label: 'Approved' },
            { value: '3', label: 'Follow Up' },
            { value: '4', label: 'In Queue' }
        ];


        return (

            <AuthWrapper>

                <div className=" back_col_cmn-">
                    <Link to='/dashboard'><span className="icon icon-back1-ie"></span></Link>
                    {// <span onClick={() =>  window.history.back()} className="icon icon-back1-ie"></span>
                    }
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>All Invoices: {typeof (participantDetails) != 'undefined' ? participantDetails.full_name : 'N/A'}</span>
                        <Link to={'/pendinginvoices/viewpendinginvoice/' + (typeof (plannerData) != 'undefined' ? plannerData : '')}>
                            <button className="btn hdng_btn cmn-btn1">Run Planner</button>
                        </Link>
                    </h1>
                </div>

                <div className='mini_profile__ d-flex bor_bot1'>

                    <div className='pro_ic1'>
                        <i className='icon icon-userm1-ie'></i>
                    </div>

                    <div className='prf_cntPart flex-1'>
                        <h4 className='cmn_clr1'><strong>{typeof (participantDetails) != 'undefined' ? participantDetails.full_name : 'N/A'}</strong></h4>
                        <ul>
                            <li><strong>HCM ID:</strong>{typeof (participantDetails) != 'undefined' ? participantDetails.ocs_id : 'N/A'}</li>
                            <li><strong>Mail ID:</strong>{typeof (participantDetails) != 'undefined' ? participantDetails.participant_email : 'N/A'}</li>
                        </ul>
                        <div className='d-flex'>
                            <div>
                                {// <select  disabled className='cstmSlctHt stats_slct' >
                                    //     <option>Active</option>
                                    //     <option>Inactive</option>
                                    // </select>

                                    // <div class="stat_grp"><span class="status val">{(participantDetails.status==1)?'Active':'Inactive'}</span></div>
                                }
                            </div>
                            <div>
                                <h5 className='avl_fnd'>
                                    <strong>Avail Funds :</strong>
                                    <span>{typeof (participantDetails) != 'undefined' ? participantDetails.funds : 'N/A'}</span>
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div className="d-flex flex-1 align-self-end justify-content-end" >

                        <div className="cstm_select  slct_cstm2 bg_grey mr-3" style={{ width: '220px' }}>
                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Filter By HCM Status"
                                options={options}
                                onChange={(e) => this.onfilter(e)}
                                value={this.state.filterVal}

                            />
                        </div>
                        <Link to={'/manualupload/' + this.props.match.params.id}>
                            <button className="btn hdng_btn cmn-btn1 d-flex align-items-center manualBtn">
                                Manual Upload Invoice <i class="icon icon-add2-ie ml-1" style={{ height: '18px' }}></i></button>
                        </Link>
                    </div>

                    

                </div>








                <div className="data_table_cmn dashboard_Table text-center">
                    <ReactTable
                        data={participantInvoiceDetails}
                        defaultPageSize={10}
                        minRows={1}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                    />
                </div>
                {/* table comp ends */}

            </AuthWrapper>

        );
    }
}



const mapStateToProps = state => {
    return {
        participantInvoiceDetails: state.InvoiceReducer.participantInvoiceDetails,
        participantDetails: state.InvoiceReducer.participantDetails,
        plannerData: state.InvoiceReducer.plannerData
    }
};

const mapDispatchToProps = {
    participantInvoice
}
export default connect(mapStateToProps, mapDispatchToProps)(AllInvoicesDetail);

// export default AllInvoicesDetail;
