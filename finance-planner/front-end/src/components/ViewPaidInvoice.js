import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as dummy from '../hoc/DummyDatas';
import { Bar } from 'react-chartjs-2';
import ReactTable from "react-table";
import Select from 'react-select-plus';
import AuthWrapper from '../hoc/AuthWrapper';


class ViewPaidInvoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            filterVal: '',
            startDate: null,p_type:0
        }
    }


    render() {

        const hdrColumn = [
            {

                Header: (props) =>
                    <label className='cstmChekie clrTpe2'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,
                accessor: "",
                width: 70,
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <label className='cstmChekie clrTpe3'>
                        <input type="checkbox" className=" " />
                        <div className="chkie"></div>
                    </label>,

            },
            { Header: 'HCM ID', accessor: 'ocisid' },
            { Header: 'NDIS ID', accessor: 'ndisid' },
            { Header: 'Last Name', accessor: 'lastname' },
            { Header: 'First Name', accessor: 'firstname' },
            { Header: 'Invoice No.', accessor: 'invoice' },
            { Header: 'Due Date', accessor: 'duedate' },
            { Header: 'Invoice Total', accessor: 'total' },
            { Header: 'Biller', accessor: 'biller' },
            {
                Header: 'Status',
                accessor: 'status',
                sortable: false,
                filterable: false,
                Cell: (props) =>
                    <div className='stat_grp'>
                    {
                        (props.index % 2 === 0)?<span className='status duplicate'>Duplicate</span>
                    :
                        (props.index % 3 === 0)?<span className='status inQueue'>In Queue</span> : <span className='status followUp'>Follow Up</span>
                    }

                    </div>
            },
            {
                Header: '',
                accessor: '',
                sortable: false,
                filterable: false,
                width: 50,
                Cell: (props) =>
                    <div>
                        <Link to={'#'}>
                            <i className='icon icon-view see_ic'></i>
                        </Link>
                    </div>
            }

        ]

        const dashboardData = [
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            },
            {
                ocisid: '896574',
                ndisid: 'ND15874',
                lastname: 'Maccarthy',
                firstname: 'Rose',
                invoice: '5698741',
                duedate: '23/15/18',
                total: '$897457',
                biller: 'Telestra'
            }



        ]


        return (

            <AuthWrapper>



                <div className=" back_col_cmn-">
                    <Link to='/pendinginvoices'><span className="icon icon-back1-ie"></span></Link>
                </div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>Invoice #1235</span>

                        <Link to='#'>
                            <button className="btn hdng_btn cmn-btn3">Paid</button>
                        </Link>
                    </h1>
                </div>

                <div className='row'>

                    <div className='col-xl-7 col-lg-6 col-12'>

                    </div>

                    <div className='col-xl-5 col-lg-6 col-12'>

                        <div className='fieldArea pd_tb_15'>
                            <div className='bor_tb  pd_tb_10 mr_tb_15' >
                                <strong>Account Details</strong>
                            </div>

                            <div className='row pd_tb_15 '>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Invoice Received Date</label>
                                        <p className='ans1'>02/06/19</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Due Date</label>
                                        <p className='ans1'>02/06/19</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Company Name</label>
                                        <p className='ans1'>AGL PTY. Ltd</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Matched Xero Supplier</label>
                                        <p className='ans1'>AGL PTY. Ltd</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Account Number</label>
                                        <p className='ans1'>26858 898</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Known Biller</label>
                                        <p className='ans1'>Yes</p>
                                    </div>
                                </div>

                                <div className='col-12'>

                                    <div className='inpLabel1'><strong>Payment Type</strong></div>
                                    <ul className='py_typeUL'>
                                    <li className={(this.state.p_type==1)?'active':''} onClick={()=>this.setState({p_type:1})}>Bank</li>
                                    <li className={(this.state.p_type==2)?'active':''} onClick={()=>this.setState({p_type:2})}>B Pay</li>
                                    <li className={(this.state.p_type==3)?'active':''} onClick={()=>this.setState({p_type:3})}>AUS Post</li>
                                    </ul>

                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Biller Code</label>
                                        <p className='ans1'>26858 898</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Reference Number</label>
                                        <p className='ans1'>Yes</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group disable">
                                        <label className=''>PO Number</label>
                                        <p className='ans1'>NA</p>
                                    </div>
                                </div>

                            </div>
                            {/* row ends */}


                            <div className='bor_tb  pd_tb_10 ' >
                                <strong>Member Details</strong>
                            </div>

                            <div className='row pd_tb_15' >

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>HCM Mail Account</label>
                                        <p className='ans1'>ocs_159@ocs.com.au</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>HCM ID</label>
                                        <p className='ans1'>569874</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>First Name</label>
                                        <p className='ans1'>Sebastian</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Match System Data</label>
                                        <p className='ans1'>Yes</p>
                                    </div>
                                </div>

                                <div className='col-lg-6 col-12'>
                                    <div className="csdisp_group">
                                        <label className=''>Last Name</label>
                                        <p className='ans1'>sample</p>
                                    </div>
                                </div>



                            </div>
                            {/* row ends */}

                        </div>


                    </div>
                    {/* col-6 ends */}

                </div>
                {/* row ends */}



                <div className='invStatus bor_bot1'>
                    Invoices Processed
                        </div>

                <div className='compoBoxDiv__ d-flex row'>
                    <div className='col-sm-12'>
                        <div className='compBox'>
                            <p className='grpMes cmn_clr1'><strong>Qty</strong></p>
                            <div className='grph_dv'>
                                <Bar data={dummy.Recruitmentdata} height={350} legend={null}
                                    options={{
                                        maintainAspectRatio: false
                                    }}
                                />
                            </div>
                            <p className='grpMes'><strong>Month</strong></p>


                            <div className='row'>

                                <div className='col-md-3'></div>
                                <div className='col-md-6'>
                                    <div className='grph_optGrp d-flex align-items-center justify-content-center'>
                                        <span><strong>Select:</strong></span>
                                        <button className='cmn-btn1 pd_bts active'>Paid</button>
                                        <button className='cmn-btn1 pd_bts'>Duplicate</button>
                                        <button className='cmn-btn1 pd_bts'>Automated</button>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>

                <div className='bor_bot1'></div>

                <div className="main_heading_cmn-">
                    <h1>
                        <span>All Invoices: Tommy Ryan</span>
                        <Link to='#'>
                            <button className="btn hdng_btn cmn-btn1">Run Planner</button>
                        </Link>
                    </h1>
                </div>


                <div className='row pd_t_15'>
                    <div className='col-6'>

                        <div className='cstm_select search_filter brdr_slct'>

                            <Select name="view_by_status "
                                required={true} simpleValue={true}
                                searchable={true} Clearable={false}
                                placeholder="| Names, numbers, dates & status'"
                                options={dummy.options}
                                onChange={(e) => this.setState({ filterVal: e })}
                                value={this.state.filterVal}

                            />

                        </div>

                    </div>

                    <div className='col-3'>

                        <div className='cstm_select  slct_cstm2 bg_grey'>

                            <Select name="view_by_status "
                                simpleValue={true}
                                searchable={false} Clearable={false}
                                placeholder="Filter By Status"
                                options={dummy.options}
                                onChange={(e) => this.setState({ categoryVal: e })}
                                value={this.state.categoryVal}

                            />

                        </div>

                    </div>

                </div>


                <div className="data_table_cmn dashboard_Table pending_invTable text-center bor_top1 pd_tb_15" >
                    <ReactTable
                        data={dashboardData}
                        defaultPageSize={dashboardData.length}
                        className="-striped -highlight"
                        previousText={<span className="icon icon-arrow-1-left previous"></span>}
                        nextText={<span className="icon icon-arrow-1-right next"></span>}
                        columns={hdrColumn}
                    />
                </div>
                {/* table comp ends */}



            </AuthWrapper>

        );
    }
}





export default ViewPaidInvoice;
