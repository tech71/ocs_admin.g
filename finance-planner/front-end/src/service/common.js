import React, { Component } from 'react';
import jQuery from "jquery";
import moment from 'moment-timezone';
import { ROUTER_PATH, BASE_URL, LOGIN_DIFFERENCE } from '../config.js';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert'; // Import
import axios from 'axios';
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css



//check user login or not
        export function checkItsLoggedIn() {
            if (getLoginToken()) {
                window.location = ROUTER_PATH + 'Dashboard';
            }
        }

export function checkItsNotLoggedIn() {
    check_loginTime();
    if (!getLoginToken()) {
        window.location = ROUTER_PATH;
    }
}

export function getLoginTIme() {
    return localStorage.getItem("dateTime")
}

export function setLoginTIme(dateTime) {
    localStorage.setItem("dateTime", dateTime);
}

export function checkLoginWithReturnTrueFalse() {
    if (!getLoginToken()) {
        return false
    } else {
        return true
    }
}


export function setRemeber(data) {
   
    var d = new Date();
    var cookieDays = 7;
    d.setTime(d.getTime() + (cookieDays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    setCookie('adminUsername', data.username, cookieDays);
    setCookie('adminPassword', data.password, cookieDays);
    //  setCookie('adminRemember', data.remember, cookieDays);
}

export function getRemeber() {
    var username = getCookie('adminUsername');
    var password = getCookie('adminPassword');
    var data = {username: ((username != undefined) ? username : ''), password: ((password != undefined) ? password : '')}
    return data;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function check_loginTime() {
    var DATE_TIME = getLoginTIme();
    var server = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(server);
    const diffDuration = moment.duration(diff);

    if (diffDuration.days() > 0) {
        logout();
    } else if (diffDuration.hours() > 0) {
        logout();
    } else if (diffDuration.minutes() > LOGIN_DIFFERENCE) {
        logout();
    }
}

export function getLoginToken() {
    return localStorage.getItem("finance_membertoken")
}

export function setLoginToken(token) {
    localStorage.setItem("finance_membertoken", token);
}

export function getFullName() {
    return localStorage.getItem("user_name");
}

export function setFullName(full_name) {
    localStorage.setItem("user_name", full_name);
}

export function removeLogoutRequiredItem() {
    localStorage.removeItem('finance_membertoken');
    localStorage.removeItem('finance_memberid');
    localStorage.removeItem('dateTime');
    localStorage.removeItem('permission');
    localStorage.removeItem('user_name');
}


export function logout() {
    var ss = postData('finance_planner/Login/logout', getLoginToken(ROUTER_PATH));
    removeLogoutRequiredItem();
    if (typeof (ss) == 'object' && ss.hasOwnProperty('status')) {
        window.location = ROUTER_PATH
    } else {
        setTimeout(function () {
            window.location = ROUTER_PATH;
        }, 300);
    }
}

export function getFirstname() {
    return localStorage.getItem("firstname")
}

export function setFirstname(firstname) {
    localStorage.setItem("firstname", firstname);
}

export function getPercentage() {
    return localStorage.getItem("Percentage")
}

export function setPercentage(Percentage) {
    localStorage.setItem("Percentage", Percentage);
}

export function getPinToken() {
    return localStorage.getItem("finance_memberid")
}
export function getOCSIdToken() {
    return localStorage.getItem("finance_memberid")
}

export function setPinToken(token) {
    localStorage.setItem("finance_memberid", token);
}

export function destroyPinToken() {
    localStorage.removeItem('finance_memberid');
    window.location = 'admin/Login/dashboard';
}

export function getPermission() {
    return localStorage.getItem("permission")
}

export function setPermission(permission) {
    localStorage.setItem("permission", permission);
}

export function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

export function LogoutAccountOpenedAnotherLocation() {
    var msg = <span>This account is opened at another location, you are being logged off.</span>;
   
    return new Promise((resolve, reject) => {
        confirmAlert({
            customUI: ({ onClose }) => {
                removeLogoutRequiredItem();
                return (
                        <div className={'cstmModal show'}  >
                            <div className='modalDialog_mini cstmDialog'>
                                <div className="hdngModal  pl-2">
                                    <h5 className='hdngCont'><strong>System message </strong></h5>
                                </div>
                                <div className="mb-3  pl-2">{msg}</div>
                                <div className="confi_but_div">
                                    <a href="/" className="Confirm_btn_Conf"> <button className="btn cmn-btn1 dis_btns2">Ok</button></a>
                                </div>
                            </div>
                        </div>
                        // <div className='custom-ui'>
                        //     <div className="confi_header_div">
                        //         <h3>System message</h3>
                        //     </div>
                        //     <p>{msg}</p>
                        //     <div className="confi_but_div">
                        //         <a href="/" className="Confirm_btn_Conf"> Ok</a>
                        //     </div>
                        // </div>
                        )
            }
        })
    });
}

export function postData(url, data) {
  

    var request_data = {'token': getLoginToken(), 'data': data}

    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: JSON.stringify(request_data)
        }).then((response) => response.json())
                .then((responseJson) => {
                  
                    if (url == 'finance_planner/Login/logout') {
                        resolve(responseJson);
                        return true;
                    }

                    if (responseJson.another_location_opened) {
                        LogoutAccountOpenedAnotherLocation();
                    }

                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }

                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        setLoginTIme(moment())
                    }

                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export function IsValidJson() {
    return true;
}



export function handleDateChangeRaw(e) {
    e.preventDefault();
}

export function getTimeAgoMessage(DATE_TIME) {
    moment.tz.setDefault("Australia/Melbourne");
    var messageTime = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(messageTime);
    const diffDuration = moment.duration(diff);


    if (diffDuration.days() > 0) {
        return diffDuration.days() + ' Days.'
    } else if (diffDuration.hours() > 0) {
        return diffDuration.hours() + ' hr.'
    } else if (diffDuration.minutes() > 0) {
        return diffDuration.minutes() + ' Min.'
    } else {
        return 'Just now'
    }
}
export function postImageData(url, data) {
    data.append('id', getOCSIdToken());
    data.append('token', getLoginToken());

    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + url, data, {})
                .then((responseJson) => {

                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }

                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        //setLoginTIme(moment())
                        //  responseJson = responseJson.data;
                    }

                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export const downloadAttachment = (url) => {
    //    window.location.href = url;
    window.open(url, 'name');
}

export function handleShareholderNameChange(obj, stateName, index, fieldName, value) {
    var state = {};
    var tempField = {};
    var List = obj.state[stateName];
    List[index][fieldName] = value

    state[stateName] = List;
    obj.setState(state);
}
export function getOptionsParticipant(input) {
    if (!input) {
        return Promise.resolve({options: []});
    }
    return fetch(BASE_URL + 'finance_planner/invoice/get_participant_name?query=' + input)
            .then((response) => {
                return response.json();
            }).then((json) => {
        return {options: json};
    });
}
export function getOptionsCompany(input) {
    if (!input) {
        return Promise.resolve({options: []});
    }

    return postData('finance_planner/invoice/get_company_name', {search: input}).then((json) => {
        return {options: json};
    });
}

export function handleChange(Obj, e) {
    var state = {};
    state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
    Obj.setState(state);
}
/*used for checkbox,input field,*/
export function handleChangeChkboxInput(Obj, e) {
    if (e) {
        e.preventDefault();
    }

    if (e != undefined && e.target.pattern)
    {
    const re = eval(e.target.pattern);
    if (e.target.value != '' && !re.test(e.target.value)) {
        return;
    }
}


var state = {};
state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
Obj.setState(state, () => {
   
});
}