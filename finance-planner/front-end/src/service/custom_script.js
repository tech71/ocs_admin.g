import jQuery from "jquery";

var $ = jQuery;

jQuery.validator.addMethod("notequaltogroup", function (value, element, options) {
    var elems = jQuery(element).parents('form').find(options[0]);
    var valueToCompare = value;

    var matchesFound = 0;
    jQuery.each(elems, function () {
        var thisVal = jQuery(this).val();
        if (thisVal == valueToCompare) {
            matchesFound++;
        }
    });

    if (this.optional(element) || matchesFound <= 1) {
        //elems.removeClass('error');
        return true;
    } else {
        //elems.addClass('error');
    }
}, ("Please enter a unique email."));

jQuery.validator.addMethod("strongPassword", function (value, element) {
    if (value != '') {
        return  !(/^[a-z0-9\\-]+$/i.test(value));
    } else {
        return true;
    }
}, "Password must be contain alphanumeric, number and spacial character");


$.validator.addMethod("email", function (value, element) {
    if (value != '') {
        return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
    } else {
        return true;
    }
}, "Please enter valid email address");



$.validator.addMethod("phonenumber",
        function (value, element) {
            if (value != '') {
                //return /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{8,18}$/.test(value);
                return /^(?=.{8,18}$)(\(?\+?[0-9]{1,3}\)?)([ ]{0,1})?[0-9_\- \(\)]{8,18}$/.test(value);
            } else {
                return true;
            }

        },
        "Please enter valid phone number"
        );

$.validator.addMethod("postcodecheck",
        function (value, element) {
            if (value != '') {
                return /^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/.test(value);
            } else {
                return true;
            }

        },
        "Please enter valid 4 digit postcode number"
        );

$.validator.addMethod("valid_website",
        function (value, element) {
            if (value != '') {
                var regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
                var without_regex = new RegExp("^([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");

                if (regex.test(value) || without_regex.test(value)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }

        },
        "Please enter valid URL"
        );