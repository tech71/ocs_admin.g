import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { checkLoginWithReturnTrueFalse } from './service/common';

//module css imports
import 'react-select-plus/dist/react-select-plus.css';
import "react-datepicker/dist/react-datepicker.css";
import 'react-table/react-table.css';

// components imports
import Login from './components/webs/Login';
import Forgot_password from './components/webs/Forgot_password';
import Reset_password from './components/webs/Reset_password';
import Dashboard from './components/Dashboard';
import UIComponents from './components/UIComponents';
import AllInvoices from './components/AllInvoices';
import PendingInvoices from './components/PendingInvoices';
import AllInvoicesDetail from './components/AllInvoicesDetail';
import FollowUpInvoices from './components/FollowUpInvoices';
import DuplicateInvoices from './components/DuplicateInvoices';
import ArchiveInvoices from './components/ArchiveInvoices';
import ApprovedInvoices from './components/ApprovedInvoices';
import Reporting from './components/Reporting';
import SyncLogs from './components/SyncLogs';
import AuditLogs from './components/AuditLogs';
import ViewPendingInvoices from './components/ViewPendingInvoice';
import ViewPaidInvoice from './components/ViewPaidInvoice';
import ViewFollowUp from './components/ViewFollowUp';
import ViewDuplicateInvoice from './components/ViewDuplicateInvoice';
import ViewArchiveInvoice from './components/ViewArchiveInvoice';
import ManualUpload from './components/ManualUpload';
import AuthWrapper from './hoc/AuthWrapper';
import ImportCSV from './components/ImportCSV';
import NotFound from './components/NotFound';

import Loader from './components/utils/Loader';
import './service/custom_script.js';

// import custom css
import './App.css';
import './responsive.css';


class App extends Component {



  render() {

    let checkedLogin = checkLoginWithReturnTrueFalse();

    return (

      <React.Fragment>

      {this.props.loading?  <Loader />:null}



        <section className={'main_wrapper ' + (this.props.sidebarState === 'right' ? ' toggle_right' : (this.props.sidebarState === 'left') ? ' toggle_left' : '')} >


          <Router>

            <Switch>

              <Route exact path={'/'} render={() => checkedLogin ? <Redirect to="/dashboard" /> : <Login />} />
              <Route exact path={'/forgot_password'} render={() => checkedLogin ? <Redirect to="/dashboard" /> : <Forgot_password />} />
              <Route path={'/reset_password/:id/:token/:dateTime'} component={Reset_password}  />
              {/* {checkedLogin ?
                <AuthWrapper>
                <Switch> */}
                  <Route exact path={'/dashboard'} render={(props) => <Dashboard {...props} />} />
                  <Route exact path={'/ui'} render={(props) => <UIComponents {...props} />} />

                  <Route exact path={'/allinvoices'} render={(props) => <AllInvoices {...props} />} />
                  <Route exact path={'/allinvoices/detail/:id'} render={(props) => <AllInvoicesDetail {...props} />} />

                  <Route exact path={'/pendinginvoices'} render={(props) => <PendingInvoices {...props} />} />
                  <Route exact path={'/approvedinvoices'} render={(props) => <ApprovedInvoices {...props} />} />
                  {/* <Route exact path={'/pendinginvoices/viewpendinginvoice'} render={(props) => <ViewPendingInvoices {...props} />} /> */}
                  <Route path={'/pendinginvoices/viewpendinginvoice/:id'} render={(props) => <ViewPendingInvoices {...props}/>} />
                  <Route exact path={'/pendinginvoices/viewpaidinvoice'} render={(props) => <ViewPaidInvoice {...props} />} />


                  <Route exact path={'/duplicateinvoices'} render={(props) => <DuplicateInvoices {...props} />} />
                  <Route exact path={'/duplicateinvoices/viewduplicateinvoice/:id'} render={(props) => <ViewDuplicateInvoice {...props} />} />

                  <Route exact path={'/archiveinvoice'} render={(props) => <ArchiveInvoices {...props} />} />
                  <Route exact path={'/archiveinvoice/viewarchiveinvoice/:id'} render={(props) => <ViewArchiveInvoice {...props} />} />

                  <Route exact path={'/followup'} render={(props) => <FollowUpInvoices {...props} />} />
                  <Route exact path={'/followup/viewfollowup/:id'} render={(props) => <ViewFollowUp {...props} />} />

                  <Route exact path={'/reporting'} render={(props) => <Reporting {...props} />} />
                  <Route exact path={'/synclogs'} render={(props) => <SyncLogs {...props} />} />
                  <Route exact path={'/auditlogs'} render={(props) => <AuditLogs {...props} />} />

                  <Route exact path={'/manualupload/:id'} render={(props) => <ManualUpload {...props} />} />
                  <Route exact path={'/importCSV'} render={(props) => <ImportCSV {...props} />} />




                  {/*
                  </Switch>
                  </AuthWrapper>
                : null} */}

              <Route path={'*'}  exact={true} render={() => <NotFound />} />

            </Switch>

          </Router>





        </section>
      </React.Fragment>

    );
  }
}

const mapStateToProps = state => {
  return {
    sidebarState: state.Reducer1.sidebarState,
    loading:state.Reducer1.loading
    // loginState: state.loginState
  };
};

export default connect(mapStateToProps, null)(App);
