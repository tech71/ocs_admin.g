import * as actionType from '../actions';
import { func } from 'prop-types';
const initialState = {
    plannerPages: '',
    invoiceListing: [],
    archiveinvoicedetails: [],
    pythonScript: [],
    invoicedetails: [],
    participantInvoiceDetails: [],
    loading: false,
    error: null, InvoiceStatus: '',
    fetchInvoice: [],
    disputeNotes: [],
    disputeError: [], participnatId: [], auditLogs: [], newDate: [],
    getnewdate: '',
    csv_url: '',
    synclogsdata: [],
    allsynclogsdata: [],
    batchCsvDownload: [],
    approvedCsvDownload: [],
    syncCsvDownload: [],
    auditCsvDownload: [],
    plannerData: [],
    disputeContactOption: [],
    editDisputeNotesFunction: func
};
export default function invoiceReducer(state = initialState, action) {
    switch (action.type) {
        case actionType.INVOICE_STATUS:
            return {
                ...state,
                InvoiceStatus: action.InvoiceStatus
            }
        case actionType.FETCH_INVOICE:
            return {
                ...state,
                FetchInvoice: action.FetchInvoice
            }
        case actionType.PYTHON_SCRIPT:
            return {
                ...state,
                pythonScript: action.pythonScript
            }
        case actionType.AUDIT_LOGS:
            return {
                ...state,
                auditLogs: action.auditLogs
            }
        case actionType.PARTICIPANT_ID:
            return {
                ...state,
                CrmParticipantId: action.CrmParticipantId
            }

        case actionType.FETCH_INVOICE_LISTING:
            return {...state, loading: false, invoiceListing: action.invoices
            };

        case actionType.FETCH_PARTICIPANT_INVOICE:
            return {
                ...state,
                loading: false,
                participantInvoiceDetails: action.participant_invoice,
                participantDetails: action.participant_data
            };
        case actionType.INVOICE_DETAILS:
            return {
                ...state,
                loading: false,
                invoicedetails: action.invoicedetails
            };
        case actionType.PDF_DATA:
            return {
                ...state,
                pdfData: action.data
            }
        case actionType.DISPUTE_NOTE:
            return {
                ...state,
                disputeSuccess: action.disputeNoteSuccess,
                disputeError: action.disputeNoteError
            }
        case actionType.GET_DISPUTE_NOTES:
            return {
                ...state,
                disputeNotes: action.disputeAllNotes
            }
        case actionType.ARCHIVE_INVOICE_DETAILS:
            return {
                ...state,
                archiveinvoicedetails: action.archiveinvoicedetails
            }

        case actionType.APPROVED_CSV_DOWNLOAD:
            return {
                ...state,
                approvedCsvDownload: action.approvedCsvDownload
            }
        case actionType.SYNC_CSV_DOWNLOAD:
            return {
                ...state,
                syncCsvDownload: action.syncCsvDownload
            }
        case actionType.AUDIT_CSV_DOWNLOAD:
            return {
                ...state,
                auditCsvDownload: action.auditCsvDownload
            }
        case actionType.BATCH_CSV_DOWNLOAD:
            return {
                ...state,
                batchCsvDownload: action.batchCsvDownload
            }
        case actionType.FETCH_SYNC_LOGS:
            return {
                ...state,
                synclogsdata: action.synclogs
            }
        case actionType.FETCH_SYNC_LOGS:
            return {
                ...state,
                allsynclogsdata: action.allsynclogs
            }
        case actionType.FETCH_SYNC_LOGS_COUNT:
            return {
                ...state,
                synclogscount: action.synclogscount
            }
        case actionType.BILLER_STATUS:
            return {
                ...state,
                billerStatus: action.billerStatus
            }
        case actionType.GET_DATE:
            return {
                ...state,
                getnewdate: action.data
            }
        case actionType.PLANNER_DATA:
            return {
                ...state,
                plannerData: action.planner_data
            }
        case actionType.PLANNER_PAGES:
            return {
                ...state,
                plannerPages: action.planner_pages
            }
        case actionType.DISPUTE_CONTACT_LOAD_OPTION:
            return {
                ...state,
                disputeContactOption: action.data
            }
            case actionType.SET_EDIT_DISPUTE_NOTES_FUNCTION:
                return {
                    ...state,
                    editDisputeNotesFunction: action.cus_funcion
                }    
        default:
            return state;
}
}
