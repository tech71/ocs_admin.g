import * as actionType from '../actions';
const initialState = {
   upload_status:[],
   participantdetailsbyid:[]
};
export default function uploadReducer(state = initialState, action) {
  switch(action.type) {
    case actionType.MANUAL_UPLOAD_SUCCESS:
            return {
              ...state,
            upload_status:action.Status
    }
    case actionType.MANUAL_UPLOAD_ERROR:
            return {
              ...state,
            upload_status:action.Status
    }
    case actionType.PARTICIPANT_DETAILS_BY_ID:
            return {
              ...state,
            participantdetailsbyid:action.data
    }
    default:
      return state;
  }
}
