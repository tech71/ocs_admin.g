import { combineReducers } from 'redux';

import Reducer1 from './Reducer1';
import InvoiceReducer from './invoice.reducer';
import uploadReducer from './upload.reducer';
export default combineReducers({
    Reducer1,InvoiceReducer,uploadReducer
});
