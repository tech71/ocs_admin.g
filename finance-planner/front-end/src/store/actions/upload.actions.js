import {postImageData,postData} from '../../service/common.js';
import {loader} from './default.actions.js';
export const MANUAL_UPLOAD_SUCCESS = 'MANUAL_UPLOAD_SUCCESS';
export const MANUAL_UPLOAD_ERROR = 'MANUAL_UPLOAD_ERROR';
export const PARTICIPANT_DETAILS_BY_ID = 'PARTICIPANT_DETAILS_BY_ID';
export const IMPORT_CSV_UPLOAD = 'IMPORT_CSV_UPLOAD';
export const PROCESS_CSV= 'PROCESS_CSV';

export function manualUpload(id,file,participantEmail){
//console.log(file);
  return  dispatch=>{
      const formData = new FormData()
      formData.append('myFile', file)
      formData.append('participantId', id)
      formData.append('email', participantEmail)
      // dispatch(loader(true));
    return  postImageData('finance_planner/invoice/manual_upload',formData);
      // .then(res => res.json())
      // .then(json => {
      //   //console.log(json);
      //   if(json.status==true){
      //     dispatch(manual_upload_success(json.data));
      //   } else {
      //     dispatch(manual_upload_error(json.error));
      //   }
      //
      //   dispatch(loader(false));
      //   return json.data;
      // })
 };
 function manual_upload_success(data) { return { type: MANUAL_UPLOAD_SUCCESS, Status: data } }
 function manual_upload_error(error) { return { type: MANUAL_UPLOAD_ERROR, Status: error } }
}

export function participantDetailsById(id){
  let request_data = {'participantId':id};
  return  dispatch=>{
       // dispatch(loader(true));
    return  postData('finance_planner/invoice/get_participant_details',request_data)
    // .then(res => res.json())
      // .then(json => {
      //   dispatch(participantDetails(json.data));
      //   dispatch(loader(false));
      //   return json.data;
      // })
};

    function participantDetails(status) { return { type: PARTICIPANT_DETAILS_BY_ID,data: status } }
  }


  export function importCSV(file){
    const formData = new FormData()
    formData.append('myFile', file)
    formData.append('docsTitle', 'NDIS UPLOAD CSV')
    return  dispatch=>{
        dispatch(loader(true));
      return  postImageData('finance_planner/invoice/importCsv',formData)
        .then(json => {
          console.log(json);
          if(json.status==true){
            dispatch(import_csv(json.data));
          }
          dispatch(loader(false));
          return json;
        })
   };
   function import_csv(data) { return { type: IMPORT_CSV_UPLOAD, Status: data } }
  }

  export function processCsv(id){
    let request_data = {sync_id:id };
    return  dispatch=>{
       dispatch(loader(true));
      return  postData('finance_planner/invoice/processCsv',request_data)
      // .then(res => res.json())
        // .then(json => {
        //   dispatch(participantDetails(json.data));
        //   dispatch(loader(false));
        //   return json.data;
        // })
  };
   //function process_csv(data) { return { type: PROCESS_CSV, Status: data } }
  }
