import {postData} from '../../service/common.js';
import moment from 'moment-timezone';
export const SIDEBAROPEN = 'sidebarOpen';
export const SIDEBARCLOSE = 'closeSidebar';
export const LOGIN = 'logIn';
export const LOGOUT = 'logOut';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const COUNT_SUCCESS = 'COUNT_SUCCESS';
export const MEMBERS_SUMMARY = 'MEMBERS_SUMMARY';

export const LOADERSTATE = 'LOADERSTATE';
export const PROCESSED_INVOICE = 'PROCESSED_INVOICE';
export const PROCESSED_INVOICE_TILL = 'PROCESSED_INVOICE_TILL';
export const GRAPH_DATA = 'GRAPH_DATA';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export  function login(data) {
    return dispatch => {
        dispatch(request({data}));
        return postData('finance_planner/login/check_login', data).then((json) => {
            if (json.status) {
                localStorage.setItem("finance_membertoken", json.token);
                localStorage.setItem("finance_memberid", json.ocs_id);
                localStorage.setItem("dateTime", moment());
                dispatch(success(json.success));
                return json;
            } else {
                dispatch(failure(json.error));
            }

        })
                .catch(error => dispatch(failure(error.toString())));
    };
    function request(request) {
        return {type: LOGIN, request: request}
    }
    function success(success) {
        return {type: LOGIN_SUCCESS, success: success}
    }
    function failure(error) {
        return {type: LOGIN_FAILURE, error: error}
    }
}
export function forgot_password(data) {
    return dispatch => {
        //    dispatch(request({ data }));
        return postData('finance_planner/login/request_reset_password', data);

    };
    // function request(request) { return { type: FORGOT_PASSWORD,request: request } }
}
export function reset_password(data) {
    return dispatch => {
        return postData('finance_planner/login/reset_password', data);
    };

}
export function verify_token(data) {
    return dispatch => {
        return postData('finance_planner/login/verify_reset_password_token', data);
    };

}
export function dashboardData(requestData) {
    return dispatch => {
        dispatch(loader(true));
        return postData('finance_planner/dashboard/dashboard_data', requestData).then((json) => {
            if (json.status) {
              
                dispatch(graphdata(json.graph));
                dispatch(countData(json.count));
                dispatch(summary(json.summary));
                dispatch(processedInvoice(json.invoice));
                dispatch(processedInvoiceTill(json.till));
                dispatch(loader(false));
                return json.data;
            } else {
                dispatch(loader(false));
            }
        });
    };
    function graphdata(data) {
        return {type: GRAPH_DATA, graphdata: data}
    }
    function countData(countData) {
        return {type: COUNT_SUCCESS, countData: countData}
    }
    function summary(summary) {
        return {type: MEMBERS_SUMMARY, summary: summary}
    }
    function processedInvoice(data) {
        return {type: PROCESSED_INVOICE, processedInvoice: data}
    }
    function processedInvoiceTill(data) {
        return {type: PROCESSED_INVOICE_TILL, processedInvoiceTill: data}
    }
}


export function getGraphData(requestData) {
    return dispatch => {
        //  dispatch(loader(true));
        return postData('finance_planner/dashboard/get_graph_data', requestData).then((json) => {
            if (json.status) {
                dispatch(graphdata(json.graph));
                dispatch(loader(false));
                return json.data;
            } else {
                dispatch(loader(false));
            }
        });
    };
    function graphdata(data) {
        return {type: GRAPH_DATA, graphdata: data}
    }
}


export function loader(loadState, viewStatus = true) {
    return {
        type: LOADERSTATE,
        loadState: loadState,
        viewStatus: viewStatus,
    }
}
