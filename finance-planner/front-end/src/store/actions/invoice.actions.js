import {postData} from '../../service/common.js';
import {loader} from './default.actions.js';
//export const FETCH_ALL_INVOICE = 'FETCH_ALL_INVOICE';
export const FETCH_INVOICE_LISTING = 'FETCH_INVOICE_LISTING';
//export const FETCH_DUPLICATE_INVOICE = 'FETCH_DUPLICATE_INVOICE';
//export const FETCH_FOLLOW_UP_INVOICE = 'FETCH_FOLLOW_UP_INVOICE';
export const FETCH_PARTICIPANT_INVOICE = 'FETCH_PARTICIPANT_INVOICE';
export const INVOICE_DETAILS = 'INVOICE_DETAILS';
export const PDF_DATA = 'PDF_DATA';
export const INVOICE_STATUS = 'INVOICE_STATUS';
export const FETCH_INVOICE = 'FETCH_INVOICE';
export const DISPUTE_NOTE = "DISPUTE NOTE CREATE";
export const GET_DISPUTE_NOTES = "GET ALL DISPUTE NOTES";
export const PARTICIPANT_ID = 'PARTICIPANT_ID';
export const AUDIT_LOGS = 'AUDIT_LOGS';
export const INVOICE_RESULT = 'INVOICE_RESULT';
export const ARCHIVE_INVOICE_DETAILS = 'ARCHIVE_INVOICE_DETAILS';
//export const FETCH_ARCHIVE_INVOICE = 'FETCH_ARCHIVE_INVOICE';
//export const FETCH_APPROVED_INVOICES = 'FETCH_APPROVED_INVOICES';
export const BILLER_STATUS = 'BILLER_STATUS';
export const GET_DATE = 'GET_DATE';
export const PYTHON_SCRIPT = 'PYTHON_SCRIPT';
export const APPROVED_CSV_DOWNLOAD = 'APPROVED_CSV_DOWNLOAD';
export const BATCH_CSV_DOWNLOAD = 'BATCH_CSV_DOWNLOAD';
export const SYNC_CSV_DOWNLOAD = 'SYNC_CSV_DOWNLOAD';
export const AUDIT_CSV_DOWNLOAD = 'AUDIT_CSV_DOWNLOAD';
export const FETCH_SYNC_LOGS='FETCH_SYNC_LOGS';
export const FETCH_ALL_SYNC_LOGS='FETCH_ALL_SYNC_LOGS';
export const PLANNER_DATA='PLANNER_DATA';
export const PLANNER_PAGES='PLANNER_PAGES';
export const FETCH_SYNC_LOGS_COUNT='FETCH_SYNC_LOGS_COUNT';
export const DISPUTE_CONTACT_LOAD_OPTION='DISPUTE_CONTACT_LOAD_OPTION';
export const SET_EDIT_DISPUTE_NOTES_FUNCTION='SET_EDIT_DISPUTE_NOTES_FUNCTION';

export const pdfData = (data) => ({
    type: PDF_DATA,
    data: data
})

export function getNewDate(newDate) {
    return dispatch => {


        var dateString = newDate;
        var dataSplit = dateString.split('/');
        var dateConverted;

        if (dataSplit[2].split(" ").length > 1) {

            var hora = dataSplit[2].split(" ")[1].split(':');
            dataSplit[2] = dataSplit[2].split(" ")[0];
            dateConverted = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0], hora[0], hora[1]);

        } else {
            dateConverted = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0]);
        }
        dispatch(getNewDate1(dateConverted.getTime()));
        return dateConverted.getTime();
    };

    function getNewDate1(dateNew) {
        return {
            type: GET_DATE,
            data: dateNew
        }
    }
}
export function invoice_status(status, id, invoicedetails = {}) {
    let request_data = {
        status: status,
        id: id,
        invoicedetails: invoicedetails
    };
    return dispatch => {
        dispatch(loader(true));
      return  postData('finance_planner/invoice/status_change', request_data);
            // .then(res => res.json())
            // .then(json => {
            //     dispatch(fetchInvoiceStatus(json.data));
            //     dispatch(loader(false));
            //     return json.data;
            // })
    };

    function fetchInvoiceStatus(status) {
        return {
            type: INVOICE_STATUS,
            InvoiceStatus: status
        }
    }
}

export const setEditDisputeNotesFunction = (cus_funcion) => ({
    type: 'SET_EDIT_DISPUTE_NOTES_FUNCTION',
    cus_funcion
})

export function fetch_invoice(){
      let request_data = {};
      return  dispatch=>{
         dispatch(loader(true));
          postData('finance_planner/invoice/fetch_invoice',request_data)
        // .then(res => res.json())
          .then(json => {
            dispatch(fetchInvoice(json.data));
            dispatch(loader(false));
            return json.data;
          })
    };

    function fetchInvoice(status) { return { type: FETCH_INVOICE,FetchInvoice: status } }
  }
    export function python_script(){
      let request_data = {};
      return  dispatch=>{
         dispatch(loader(true));
          postData('finance_planner/invoice/python_script_run',request_data)
        // .then(res => res.json())
          .then(json => {
            dispatch(pythonScript(json.data));
            dispatch(loader(false));
            return json.data;
          })
    };

    function pythonScript(status) { return { type: PYTHON_SCRIPT,PythonScript: status } }
}


export function getInvoiceByType(request_data) {
    return dispatch => {
        dispatch(fetchInvoice([]));
        dispatch(page_count([]));
        dispatch(loader(true, false));
        postData('finance_planner/invoice/list_invoice', request_data)
            .then(json => {
              if(json.status){
                dispatch(fetchInvoice(json.data));
                dispatch(page_count(json.count));
                dispatch(planner_data(json.planner_id));
              }
                dispatch(loader(false));
                return json.data;
            })
            .catch(error => console.log(error));
    };

    function fetchInvoice(invoices) {
        return {
            type: FETCH_INVOICE_LISTING,
            invoices: invoices
        }
    }
    function planner_data(planner_data) {
        return {
            type: PLANNER_DATA,
            planner_data: planner_data
        }
    }
    function page_count(planner_count) {
        return {
            type: PLANNER_PAGES,
            planner_pages: planner_count
        }
    }

}

export function participantInvoice(id, status) {
    let request_data = {
        data: id,
        status: status
    };
    return dispatch => {
        dispatch(loader(true));
        postData('finance_planner/invoice/participant_invoices', request_data)
            // .then(res => res.json())
            .then(json => {
              if(json.status){
                dispatch(participant_invoice(json));
                dispatch(page_count(json.count));
                dispatch(planner_data(json.planner_id));
                dispatch(loader(false));
                return json.data;
              }
              dispatch(loader(false));
            })
            .catch(error => console.log(error));
    };

    function participant_invoice(participant_invoice) {
        return {
            type: FETCH_PARTICIPANT_INVOICE,
            participant_invoice: participant_invoice.data,
            participant_data: participant_invoice.participant
        }
    }
    function planner_data(planner_data) {
        return {
            type: PLANNER_DATA,
            planner_data: planner_data
        }
    }
    function page_count(planner_count) {
        return {
            type: PLANNER_PAGES,
            planner_pages: planner_count
        }
    }

}

export function invoiceDetails(id) {
    let request_data = {
        'id': id
    };
    return dispatch => {
        return postData('finance_planner/invoice/invoice_details', request_data)
        // .then(res => res.json())
        // .then(json => {
        //   dispatch(fetchInvoiceDetails(json.data));
        //   return json.data;
        // })
        // .catch(error => console.log(error));
    };

    function fetchInvoiceDetails(invoicedetails) {
        return {
            type: INVOICE_DETAILS,
            invoicedetails: invoicedetails
        }
    }
}
export function archiveInvoiceDetails(id) {
    let request_data = {
        'id': id
    };
    return dispatch => {
        return postData('finance_planner/invoice/invoice_details', request_data)
    };

    function fetchInvoiceDetails(invoicedetails) {
        return {
            type: ARCHIVE_INVOICE_DETAILS,
            archiveinvoicedetails: invoicedetails
        }
    }
}


// export csv approved invoices
export function approvedCsvDownload(data){
  let request_data = {   data }
  return dispatch => {
      dispatch(loader(true));
      return postData('finance_planner/invoice/csv_approved_export', request_data)
          // .then(res => res.json())
          .then(json => {
              if (json.status == true) {
                  dispatch(csvSuccess(json.data));
              }
              dispatch(loader(false));
              return json;
          })
          .catch(error => console.log(error));
  };
  function csvSuccess(data) {
      return {
          type: APPROVED_CSV_DOWNLOAD,
          approvedCsvDownload: data
      }
  }
}
export function csvBatchesExport(data){
  let request_data = {   data }
  return dispatch => {
      dispatch(loader(true));
      return postData('finance_planner/invoice/csv_batches_export', request_data)
          // .then(res => res.json())
          .then(json => {
              if (json.status == true) {
                  dispatch(csvSuccess(json.data));
              }
              dispatch(loader(false));
              return json;
          })
          .catch(error => console.log(error));
  };
  function csvSuccess(data) {
      return {
          type: BATCH_CSV_DOWNLOAD,
          batchCsvDownload: data
      }
  }
}
export function syncCsvDownload(data){
  let request_data = {   data }
  return dispatch => {
      dispatch(loader(true));
      return postData('finance_planner/invoice/csv_sync_export', request_data)
          // .then(res => res.json())
          .then(json => {
              if (json.status == true) {
                  dispatch(csvSuccess(json.data));
              }
              dispatch(loader(false));
              return json;
          })
          .catch(error => console.log(error));
  };
  function csvSuccess(data) {
      return {
          type: SYNC_CSV_DOWNLOAD,
          syncCsvDownload: data
      }
  }
}
export function auditCsvDownload(data){
  let request_data = {   data }
  return dispatch => {
      dispatch(loader(true));
      return postData('finance_planner/invoice/csv_audit_export', request_data)
          // .then(res => res.json())
          .then(json => {
              if (json.status == true) {
                  dispatch(csvSuccess(json.data));
              }
              dispatch(loader(false));
              return json;
          })
          .catch(error => console.log(error));
  };
  function csvSuccess(data) {
      return {
          type: AUDIT_CSV_DOWNLOAD,
          auditCsvDownload: data
      }
  }
}


// Handle HTTP errors since fetch won't.
function getLoginToken() {
    return localStorage.getItem("finance_membertoken");
}


export function createDisputeNotes(data) {
    return dispatch => {
        dispatch(loader(true));
        return postData('finance_planner/invoice/create_dispute_note', data)
            // .then(res => res.json())
            .then(json => {
                if (json.status == true) {
                    dispatch(disputeSuccess(json.data));
                } else {
                    dispatch(disputeFailure(json.error));
                }

                dispatch(loader(false));
                return json;
            })
            .catch(error => console.log(error));
    };

    function disputeSuccess(success) {
        return {
            type: DISPUTE_NOTE,
            disputeNoteSuccess: success
        }
    }

    function disputeFailure(error) {
        return {
            type: DISPUTE_NOTE,
            disputeNoteError: error
        }
    }
}

export function getDisputeContactOption(data) {
    return dispatch => {
        return postData('finance_planner/invoice/get_dispute_contact_option', data)
            .then(json => {
                if (json.status == true) {
                    dispatch(loadOptionSuccess(json.data));
                } else {
                    dispatch(loadOptionSuccess([]));
                }
                return json;
            })
            .catch(error => console.log(error));
    };

    function loadOptionSuccess(data) {
        return {
            type: DISPUTE_CONTACT_LOAD_OPTION,
            data
        }
    }
}

export function getAllDisputeNotes(id) {
    let request_data = {
        data: id
    }
    return dispatch => {
        // dispatch(loader(true));
        postData('finance_planner/invoice/list_dispute_note', request_data)
            // .then(res => res.json())
            .then(json => {
                dispatch(alldisputenotes(json.data));
                // dispatch(loader(false));
                return json.data;
            })
            .catch(error => console.log(error));
    };

    function alldisputenotes(allnotes) {
        return {
            type: GET_DISPUTE_NOTES,
            disputeAllNotes: allnotes
        }
    }
}

export function getOptionsCrmParticipantId() {
    let request_data = {};
    return dispatch => {
        return postData('finance_planner/invoice/get_participant_id', request_data)

            // .then(res => console.log(res))
            .then(res => {
                dispatch(fetchOptionsCrmParticipantId(res));
                return res;
            })
            .catch(error => console.log(error));
    };

    function fetchOptionsCrmParticipantId(CrmParticipantId) {
        return {
            type: PARTICIPANT_ID,
            CrmParticipantId: CrmParticipantId
        }
    }
}
export function list_audit_logs(pageSize, page, sorted, filtered) {
    return dispatch => {
        // dispatch(loader(true));

        var Request = JSON.stringify({
            pageSize: pageSize,
            page: page,
            sorted: sorted,
            filtered: filtered
        });
        return postData('finance_planner/invoice/list_audit_logs', Request)
            .then(res => {
                dispatch(auditLogs(res.data));
                return res;
            })
    };

    function auditLogs(auditLogs) {
        return {
            type: AUDIT_LOGS,
            auditLogs: auditLogs
        }
    }
}
export function biller_refernce_check(billerCode, billerReferenceNumber) {
    let request_data = {
        biller_code: billerCode,
        reference_no: billerReferenceNumber
    };
    return dispatch => {
        // dispatch(loader(true));

        return postData('finance_planner/invoice/check_biller_refernce_no', request_data)
            .then(res => {

                dispatch(billerRefernce(res.status));
                return res;
            })
    };

    function billerRefernce(billerStatus) {
        return {
            type: BILLER_STATUS,
            billerStatus: billerStatus
        }
    }
}
export function invoice_exsits(id, invoice_no) {
    let request_data = {
        id: id,
        invoice_no: invoice_no
    };
    return dispatch => {
        console.log(invoice_no)
        // dispatch(loader(true));
        return postData('finance_planner/invoice/invoice_already_exsits', request_data)
        // .then(res => res.json())
        // .then(json => {
        //   dispatch(fetchInvoiceResult(json.data));
        //   dispatch(loader(false));
        //   return json.data;
        // })
    };
}

export function update_invoice_status(id, invoice_no) {
    let request_data = {
        id: id,
        invoice_no: invoice_no
    };
    return dispatch => {
        // dispatch(loader(true));
        return postData('finance_planner/invoice/update_invoice_status', request_data)
        // .then(res => res.json())
        //   .then(json => {
        //     dispatch(fetchInvoiceResult(json.data));
        //     dispatch(loader(false));
        //     return json.data;
        //   })
    };

    function fetchInvoiceResult(status) {
        return {
            type: INVOICE_RESULT,
            InvoiceStatus: status
        }
    }
}
export function setupCompany(data){
  return dispatch => {
        return postData('finance_planner/invoice/set_up_company', data)

  }
}


export function syncLogs() {
    return dispatch => {
        dispatch(loader(true));
      return  postData('finance_planner/invoice/get_sync_logs')
            .then(json => {
                if(json.status==true){
                dispatch(get_sync_logs(json.data));
                dispatch(loader(false));
                return json.data;
              }

            })
    };
    function get_sync_logs(synclogs) {
        return {
            type: FETCH_SYNC_LOGS,
            synclogs: synclogs
        }
    }

}

export function allsyncLogs(pageSize, page, sorted, filtered) {
    return dispatch => {

      var Request = {
          pageSize: pageSize,
          page: page,
          sorted: sorted,
          filtered: filtered
      };

        dispatch(loader(true));
      return  postData('finance_planner/invoice/get_all_sync_logs',Request)
            .then(json => {
              if(json.status==true){
                dispatch(get_all_sync_logs(json.data));
                dispatch(page_count_sync_logs(json.count));
                dispatch(loader(false));
                return json.data;
              }

            })
    };
    function get_all_sync_logs(allsynclogs) {
        return {
            type: FETCH_ALL_SYNC_LOGS,
            allsynclogs: allsynclogs
        }
    }
    function page_count_sync_logs(synclogscount) {
        return {
            type: FETCH_SYNC_LOGS_COUNT,
            synclogscount: synclogscount
        }
    }
}
