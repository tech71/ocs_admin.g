<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function check_login($obj) {
        // using query check username
        $column = array('m.id', 'concat(m.firstname," ",m.lastname) as full_name', 'm.password', 'm.gender', 'm.status');

        // short_code = internal_staff mean only internal department user can login
        $where = array('m.username' => $obj->getUsername(), 'm.archive' => 0, 'd.short_code' => 'internal_staff');

        $this->db->select($column);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function check_pin($obj) {
        // using query check username
        $column = array('m.pin');

        // short_code = internal_staff mean only internal department user can login
        $where = array('m.id' => $obj->getAdminid(), 'm.archive' => 0, 'd.short_code' => 'internal_staff');

        $this->db->select($column);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function check_valid_email($email) {
        // using query check username
        $column = array('m.id', 'm.firstname', 'm.lastname', 'm.status');

        // short_code = internal_staff mean only internal department user can login
        $where = array('me.email' => $email, 'm.archive' => 0, 'm.status' => 1, 'd.short_code' => 'internal_staff');

        $this->db->select($column);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_email as me', 'me.memberId = m.id', 'inner');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function list_role_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'asc';
        }

        if (!empty($filter)) {
            $this->db->like('id', $filter);
            $this->db->or_like('name', $filter);
            $this->db->or_like('created', $filter);
        }

        $colowmn = array('id', 'name', 'status', 'created', 'archive');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'role');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where('archive', 0);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());



        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['id'] = $val->id;
                $row['name'] = $val->name;
                $row['created'] = date('d-m-y', strtotime($val->created));
                $row['action'] = '';
                $row['archive'] = $val->archive;
                $row['status'] = $val->status;
                $dataResult[] = $row;
            }
        }


        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);


        return $return;
    }

    //this method is used Recruitment side also to check duplicate email
    public function check_dublicate_email($email, $adminId) {
        $this->db->select(array('email'));
        $this->db->from('tbl_member_email');
        $this->db->where(array('email' => $email));

        if (!empty($adminId))
            $this->db->where('memberId !=', $adminId);

        $query = $this->db->get();
        return $query->result();
    }

    public function list_admins_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'm.id';
            $direction = 'desc';
        }
        $where = '';
        $sWhere = $where;

        $src_columns = array('username', 'firstname', 'lastname', 'mp.phone', 'me.email', 'gender');
        if (!empty($filter->search)) {
            $this->db->group_start();
            $search_value = $filter->search;

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $search_value);
                } else {
                    $this->db->or_like($column_search, $search_value);
                }
            }
            $this->db->group_end();
        }

        if (!empty($filter->search_by)) {
            if ($filter->search_by == 'archive_only') {
                $this->db->where('m.archive', 1);
            } elseif ($filter->search_by == 'inactive_only') {
                $this->db->where('status', 0);
            } elseif ($filter->search_by == 'active_only') {
                $this->db->where('status', 1);
                $this->db->where('m.archive', 0);
            }
        } else {
            $this->db->where('m.archive', 0);
            $this->db->where_in('status', [0, 1]);
        }

        $colowmn = array('m.id', 'username', 'firstname', 'lastname', 'status', 'gender', 'mp.phone', 'me.email', 'm.archive');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from('tbl_member as m');

        $this->db->join('tbl_member_email as me', 'me.memberId = m.id AND me.primary_email = 1', 'left');
        $this->db->join('tbl_member_phone as mp', 'mp.memberId = m.id AND mp.primary_phone = 1', 'left');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "internal_staff"', 'inner');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        if (!empty($sWhere))
            $this->db->where($sWhere, null, false);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

//        last_query();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);

        return $return;
    }

    public function get_admin_permission($adminId) {
        $this->db->select(array('ar.roleId'));
        $this->db->from('tbl_admin_role as ar');
        $this->db->where(array('ar.adminId' => $adminId));

        $role_query = $this->db->get();
        $rols_data = $role_query->result_array();


        if (!empty($rols_data) || in_array($adminId, $this->config->item('super_admins'))) {

            $this->db->select(array('p.permission'));
            $this->db->from("tbl_permission as p");
            $this->db->join("tbl_role_permission as rp", 'rp.permission = p.id', 'inner');

            if (!in_array($adminId, $this->config->item('super_admins'))) {
                $temp_roleIds = array_column((array) $rols_data, 'roleId');
                $this->db->where_in('rp.roleId', $temp_roleIds);
            }

            $permission_query = $this->db->get();
            return $permission_query->result();
        }
    }

    public function check_admin_permission($adminId, $pemission_key) {
        if (in_array($adminId, $this->config->item('super_admins')) && $pemission_key)
            return true;


        $this->db->select(array('ar.roleId', 'p.permission'));
        $this->db->from('tbl_admin_role as ar');

        $this->db->join('tbl_role as r', 'r.id = ar.roleId', 'inner');
        $this->db->join('tbl_role_permission as rp', 'r.id = rp.roleId', 'inner');
        $this->db->join('tbl_permission as p', 'rp.permission = p.id', 'inner');


        // check specific permission status like recruitment section and CRM section
        if ($pemission_key === 'access_recruitment' || $pemission_key === 'access_recruitment_admin') {

            // check from recruitment staff table status
            // if 1 = enable / 0- disable
            $this->db->join("tbl_recruitment_staff as rs", 'rs.adminId = ar.adminId AND rs.status = 1', 'inner');
        } elseif ($pemission_key === 'access_crm') {
            // check from CRM staff table status
            // if 1 = enable / 0- disable
            $this->db->join("tbl_crm_staff as cs", 'cs.admin_id = ar.adminId AND cs.status = 1', 'inner');
        }

        $this->db->where(array('p.permission' => $pemission_key));
        $this->db->where(array('ar.adminId' => $adminId));

        $permission_query = $this->db->get();


        $res = $permission_query->result();
//        last_query();
        return $res;
    }

    public function get_admin_based_roles($adminId, $all) {
        $tbl_admin_role = TBL_PREFIX . 'admin_role';
        $tbl_role = TBL_PREFIX . 'role';


        $join = ($all) ? 'left' : 'inner';

        $this->db->select(array($tbl_role . '.id', $tbl_role . '.name', $tbl_admin_role . '.adminId as access'));
        $this->db->from($tbl_role);

        if ($adminId == 1) {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId', $join);
        } else {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId AND ' . $tbl_admin_role . '.adminId = ' . $adminId, $join);
        }

        $role_query = $this->db->get();
        $rols_data = $role_query->result();
        if (!empty($rols_data)) {
            foreach ($rols_data as $val) {
                if ($val->access > 0) {
                    $val->access = true;
                }
            }
        }
        return $rols_data;
    }

    public function get_admin_details($adminId) {
        $this->db->select(array('m.id', 'm.username', 'm.firstname', 'm.lastname', 'm.position', 'm.department'));
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where(array('m.id' => $adminId, 'd.short_code' => 'internal_staff'));

        $query = $this->db->get();
        $result = (array) $query->row();

        return $result;
    }

    function get_admin_phone_number($adminId) {
        $tbl_admin_phone = TBL_PREFIX . 'member_phone';

        $this->db->select(array('phone as name', 'primary_phone'));
        $this->db->from($tbl_admin_phone);
        $this->db->where(array('memberId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

    function get_admin_email($adminId) {
        $tbl_admin_email = TBL_PREFIX . 'member_email';

        $this->db->select(array('email as name', 'primary_email'));
        $this->db->from($tbl_admin_email);
        $this->db->where(array('memberId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

    public function get_all_loges($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $moduleArr = array('admin' => 1, 'participant' => 2, 'member' => 3, 'schedule' => 4, 'fms' => 5, 'house' => 6, 'organisation' => 7, 'imail' => 8);


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                if ($sorted[0]->id == 'time') {
                    $orderBy = $sorted[0]->id;
                } else {
                    $orderBy = 'tbl_logs.' . $sorted[0]->id;
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'desc';
        }

        if (!empty($filter->search_box)) {
            $this->db->like('lg.created_by', date($filter->search_box));
            $this->db->or_like('lg.title', $filter->search_box);
        }

        if (!empty($filter->on_date)) {
            $this->db->where("Date(lg.created)", date('Y-m-d', strtotime($filter->on_date)));
        } else {
            if (!empty($filter->start_date)) {
                $this->db->where("Date(lg.created) >", date('Y-m-d', strtotime($filter->start_date)));
            }if (!empty($filter->end_date)) {
                $this->db->where("Date(lg.created) <", date('Y-m-d', strtotime($filter->end_date)));
            }
        }

        $colowmn = array('lg.id', 'lg.created_by', 'lg.title', "DATE_FORMAT(lg.created, '%d/%m/%Y') as created", "DATE_FORMAT(lg.created, '%h:%i %p') as time");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from('tbl_logs as lg');
        $this->db->join('tbl_module_title as mt', 'mt.id = lg.module', 'INNER');

        if (!empty($filter->module)) {
            $this->db->where('mt.key_name', $filter->module);
        }

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
// last_query();

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        $return = array('status' => true, 'count' => $dt_filtered_total, 'data' => $dataResult);

        return $return;
    }

    function get_alloted_to_admin_roles($adminId) {
        $this->db->select(array('ar.id', 'r.id as roleId'));
        $this->db->from('tbl_admin_role as ar');

        $this->db->join('tbl_role as r', 'ar.roleId = r.id', 'inner');
        $this->db->where('ar.adminId', $adminId);

        $role_query = $this->db->get();
        $rols_data = $role_query->result_array();

        return $rols_data;
    }

}
