<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_management_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function check_login($obj) {
        $super_admin_usernames = get_super_admins('username');

        // using query check username
        $column = array('m.id', 'concat(m.firstname," ",m.lastname) as full_name', 'm.password', 'm.gender', 'm.status');

        // short_code = internal_staff mean only internal department user can login
        // and check permission of plan management 
        $where = array('m.username' => $obj->getUsername(), 'm.archive' => 0);

        $this->db->select($column);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "internal_staff"', 'inner');

        if (!in_array($obj->getUsername(), $super_admin_usernames)) {
            $this->db->join('tbl_admin_role as ar', 'ar.adminId = m.id', 'inner');
            $this->db->join('tbl_role as r', 'r.id = ar.roleId AND role_key = "finance_planner"', 'inner');
        }
        
        $this->db->where($where);
        $query = $this->db->get();

        return $query->row();
    }

    function check_auth_token($obj) {
        $this->db->select(['updated', 'memberId', 'token', 'ip_address']);
        $this->db->from('tbl_member_login');
        $this->db->where(['token' => $obj->getToken()]);

        $query = $this->db->get();

        return $query->row();
    }

    public function check_valid_email($email) {
        // using query check username
        $column = array('m.id', 'm.firstname', 'm.lastname', 'm.status');

        // short_code = internal_staff mean only internal department user can login
        $where = array('me.email' => $email, 'm.archive' => 0, 'm.status' => 1, 'd.short_code' => 'internal_staff');

        $this->db->select($column);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_email as me', 'me.memberId = m.id', 'inner');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    //this method is used Recruitment side also to check duplicate email
    public function check_dublicate_email($email, $adminId) {
        $this->db->select(array('email'));
        $this->db->from('tbl_member_email');
        $this->db->where(array('email' => $email));

        if (!empty($adminId))
            $this->db->where('memberId !=', $adminId);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_admin_permission($adminId) {
        $this->db->select(array('ar.roleId'));
        $this->db->from('tbl_admin_role as ar');
        $this->db->where(array('ar.adminId' => $adminId));

        $role_query = $this->db->get();
        $rols_data = $role_query->result_array();


        if (!empty($rols_data) || in_array($adminId, $this->config->item('super_admins'))) {

            $this->db->select(array('p.permission'));
            $this->db->from("tbl_permission as p");
            $this->db->join("tbl_role_permission as rp", 'rp.permission = p.id', 'inner');

            if (!in_array($adminId, $this->config->item('super_admins'))) {
                $temp_roleIds = array_column((array) $rols_data, 'roleId');
                $this->db->where_in('rp.roleId', $temp_roleIds);
            }

            $permission_query = $this->db->get();
            return $permission_query->result();
        }
    }

    public function check_admin_permission($adminId, $pemission_key) {
        if (in_array($adminId, $this->config->item('super_admins')) && $pemission_key)
            return true;


        $this->db->select(array('ar.roleId', 'p.permission'));
        $this->db->from('tbl_admin_role as ar');

        $this->db->join('tbl_role as r', 'r.id = ar.roleId', 'inner');
        $this->db->join('tbl_role_permission as rp', 'r.id = rp.roleId', 'inner');
        $this->db->join('tbl_permission as p', 'rp.permission = p.id', 'inner');


        // check specific permission status like recruitment section and CRM section
        if ($pemission_key === 'access_recruitment' || $pemission_key === 'access_recruitment_admin') {

            // check from recruitment staff table status
            // if 1 = enable / 0- disable
            $this->db->join("tbl_recruitment_staff as rs", 'rs.adminId = ar.adminId AND rs.status = 1', 'inner');
        } elseif ($pemission_key === 'access_crm') {
            // check from CRM staff table status
            // if 1 = enable / 0- disable
            $this->db->join("tbl_crm_staff as cs", 'cs.admin_id = ar.adminId AND cs.status = 1', 'inner');
        }

        $this->db->where(array('p.permission' => $pemission_key));
        $this->db->where(array('ar.adminId' => $adminId));

        $permission_query = $this->db->get();


        $res = $permission_query->result();
//        last_query();
        return $res;
    }

    public function get_admin_based_roles($adminId, $all) {
        $tbl_admin_role = TBL_PREFIX . 'admin_role';
        $tbl_role = TBL_PREFIX . 'role';


        $join = ($all) ? 'left' : 'inner';

        $this->db->select(array($tbl_role . '.id', $tbl_role . '.name', $tbl_admin_role . '.adminId as access'));
        $this->db->from($tbl_role);

        if ($adminId == 1) {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId', $join);
        } else {
            $this->db->join($tbl_admin_role, $tbl_role . '.id = ' . $tbl_admin_role . '.roleId AND ' . $tbl_admin_role . '.adminId = ' . $adminId, $join);
        }

        $role_query = $this->db->get();
        $rols_data = $role_query->result();
        if (!empty($rols_data)) {
            foreach ($rols_data as $val) {
                if ($val->access > 0) {
                    $val->access = true;
                }
            }
        }
        return $rols_data;
    }

    public function get_admin_details($adminId) {
        $this->db->select(array('m.id', 'm.username', 'm.firstname', 'm.lastname', 'm.position', 'm.department'));
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');
        $this->db->where(array('m.id' => $adminId, 'd.short_code' => 'internal_staff'));

        $query = $this->db->get();
        $result = (array) $query->row();

        return $result;
    }

    function get_admin_phone_number($adminId) {
        $tbl_admin_phone = TBL_PREFIX . 'member_phone';

        $this->db->select(array('phone as name', 'primary_phone'));
        $this->db->from($tbl_admin_phone);
        $this->db->where(array('memberId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

    function get_admin_email($adminId) {
        $tbl_admin_email = TBL_PREFIX . 'member_email';

        $this->db->select(array('email as name', 'primary_email'));
        $this->db->from($tbl_admin_email);
        $this->db->where(array('memberId' => $adminId));
        $query = $this->db->get();
        return $result = (array) $query->result();
    }

}
