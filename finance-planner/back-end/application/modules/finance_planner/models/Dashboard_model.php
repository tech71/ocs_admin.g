<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct();
    }

    public function get_counts_bystatus($where){
      $table_name='plan_management';
      $this->db->select('count(*) as count');
      $this->db->where($where);
      $result = $this->db->get(TBL_PREFIX.$table_name);
      return $result->row();
    }

    public function get_graph_monthly($reqData) {
      $status = $reqData->status;
      $measure = $reqData->measure;
      $payment_type = $reqData->type;
      $months = array();
      $month_name = array();
      require_once APPPATH . 'Classes/finance_planner/MyDateTime.php';
      $getDates = new MyDateTime\MyDateTime();
      $res = $getDates->fiscalYear();
      $current = "created BETWEEN '" . $res['l_f']['start_date'] . "' AND '" . $res['l_f']['end_date'] . "'  ";
      if ($payment_type == 'manual') {
          $current = $current . " and payment_type = '0'";
      } else if ($payment_type == 'automated') {
          $current = $current . " and payment_type = '1'";
      }
      if ($measure == 1) {
          $sql = "select 
      COUNT(IF( $current , 1, NULL)) as total_count,
      MONTH(created) as m ,
      YEAR(created) as y
      from tbl_plan_management 
      where status='" . $status . "' and read_status=1 and load_status=1
      group by  MONTH(created)
      ";
      } else {
          $sql = "select 
        SUM(IF( $current , invoice_amount, NULL)) as total_count,
        MONTH(created) as m ,
        YEAR(created) as y
        from tbl_plan_management 
        where status='" . $status . "' and   read_status=1 and load_status=1
        group by  MONTH(created)
        ";
      }
      $result = $this->db->query($sql)->result();
      $data = array();
      $row = array();
      for ($i = 0;$i < 12;$i++) {
        //   $months['no'][$i] = date('m', strtotime("+$i month"));
          $months['no'][$i] = date("m", strtotime("+$i month", strtotime(date("F") . "1")) );
          $months['name'][$i] = date("F", mktime(0, 0, 0, $months['no'][$i], 10));
      }
      $totalCount = 0;
      foreach ($months['no'] as $key => $value) {
          $data[] = $this->getMonthsCount($result, $value);  
      }
 
      $row['graph_qty'] = $data;
      $row['graph_month'] = $months;
   
      return $row;
  }

   

    function getMonthsCount($all_status, $monthNumber) {
      $values = array();
      foreach ($all_status as $m => $status) {
          if ($monthNumber == $status->m) {
              $values = $status->total_count;
          }
      }
      return $values;
  }

    public function get_processed_invoice($reqData){
     $where = "updated > DATE_SUB(CURDATE(), INTERVAL 1 DAY) and load_status=1 and read_status=1";
     $table_name='plan_management';
     $this->db->select('count(*) as count');
     $this->db->where($where);
     $result = $this->db->get(TBL_PREFIX.$table_name);
     return $result->row();
   }

   public function get_processed_invoice_till($reqData){
    $where = "load_status=1 and read_status=1 and status!=4";
    $table_name='plan_management';
    $this->db->select('count(*) as count');
    $this->db->where($where);
    $result = $this->db->get(TBL_PREFIX.$table_name);
    return $result->row();
  }

    public function get_members_summary($reqData){
      $limit = '10';
      $orderBy = '';
      $direction = '';
      $tbl = TBL_PREFIX . 'plan_management';
      $tbl3 = TBL_PREFIX . 'participant';
       $this->db->select(array($tbl . '.id as id',$tbl.'.status','concat("$",'.$tbl3.".houseId) as funds",
      'concat(tbl_participant.firstname," ",tbl_participant.lastname) as member_name',
       $tbl3.'.id as ocs_id',"SUM(CASE WHEN ".$tbl.".status = 1 THEN 1 ELSE 0 END) AS paid,
      SUM(CASE WHEN ".$tbl.".status = 5 THEN 1 ELSE 0 END) AS duplicate,
      SUM(CASE WHEN ".$tbl.".status = 3 THEN 1 ELSE 0 END) AS follow_up,
      SUM(CASE WHEN ".$tbl.".status = 4 THEN 1 ELSE 0 END) AS pending"

      ));
      $src_columns = array($tbl3 . '.firstname',$tbl3 . '.lastname',$tbl3.'.id as participant_id',$tbl3.'.houseId as funds');

      if (isset($reqData->search)) {
          $search = $reqData->search;
      }
      if (!empty($search)) {
          $this->db->group_start();
          for ($i = 0;$i < count($src_columns);$i++) {
              $column_search = $src_columns[$i];
              if (strstr($column_search, "as") !== false) {
                  $serch_column = explode(" as ", $column_search);
                  if ($serch_column[0] != 'null') $this->db->or_like($serch_column[0], $search);
              } else if ($column_search != 'null') {
                  $this->db->or_like($column_search, $search);
              }
          }
          $this->db->group_end();
      }
      $this->db->join($tbl3, $tbl3.'.id = '.$tbl.'.participant_id', 'left');
      $this->db->from($tbl);
	  $this->db->where('read_status=1 and load_status=1');
      $this->db->order_by($orderBy, $direction);
      $this->db->group_by('ocs_id');
      // $this->db->limit($limit, ($page * $limit));

      $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
      $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

      if ($dt_filtered_total % $limit == 0) {
          $dt_filtered_total = ($dt_filtered_total / $limit);
      } else {
          $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
      }

      $dataResult = array();

      if (!empty($query->result())) {
          $dataResult = $query->result();
        }
    $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
    return $return;
  }
}
