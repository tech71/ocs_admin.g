<?php

//require_once APPPATH.'modules\xero\controllers\Xero_dashboard.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $params = array('company_id' => 1);
        $this->load->library('XeroContact', $params);
        $this->load->library('XeroInvoice', $params);
    }

    public function participant_invoices($reqData) {
        // var_dump($reqData);
        $limit = '10';
        $orderBy = '';
        $direction = '';
        $tbl = TBL_PREFIX . 'plan_management';
        $tbl3 = TBL_PREFIX . 'participant';
        $participant_id = $reqData->data;
        $status = $reqData->status;
        $planner = $this->get_planner_details('', '', true);
        $status_where = 'tbl_plan_management.read_status !=0 AND tbl_plan_management.load_status!=0';
        $where = array($tbl3 . '.id' => $participant_id);
        $swhere = ($status == 0) ? array($tbl . '.status!=' => 6) : (($status == 7) ? "( tbl_plan_management.status= 2 OR tbl_plan_management.status = 5)" : array($tbl . '.status' => $status));
        $this->db->select(array($tbl . '.id ', $tbl . '.invoice_number', $tbl . '.due_date', $tbl . '.participant_email', $tbl . '.account_number', $tbl . '.invoice_date', $tbl . '.company_name', $tbl . '.invoice_amount', $tbl . '.status', $tbl3 . '.firstname', $tbl3 . '.lastname', $tbl3 . '.id as ocs_id'));
        $this->db->join($tbl3, $tbl3 . '.id = ' . $tbl . '.participant_id', 'right');
        $this->db->from($tbl);
        $this->db->where($where);
        $this->db->where($swhere);
        $this->db->where($status_where);
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('id');
        // $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        // print_r($this->db->last_query());
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $dataResult = array();
        $this->db->select(array('tbl_participant_email.email as participant_email', $tbl3 . '.firstname', $tbl3 . '.lastname', 'concat(' . $tbl3 . '.firstname,' . $tbl3 . '.lastname) as full_name', $tbl3 . '.id as ocs_id', 'concat("$",' . $tbl3 . ".houseId) as funds"));
        $this->db->join('tbl_participant_email', 'tbl_participant_email' . '.participantId = ' . $tbl3 . '.id', 'left');
        $this->db->from($tbl3);

        $this->db->where($where);
        $p_query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $p_data = $p_query->row_array();
        // $p_data = $this->basic_model->get_row('participant', array($tbl3 . '.firstname', $tbl3 . '.lastname','concat('.$tbl3.'.firstname,'.$tbl3.'.lastname) as full_name', $tbl3 . '.id as ocs_id','concat("$",'.$tbl3.".houseId) as funds"), $where);
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $status = "";
                switch ($val->status) {
                    case 1:
                        $status = 'Approved';
                        break;
                    case 2:
                        $status = 'PotentialDuplicate';
                        break;
                    case 3:
                        $status = 'followUp';
                        break;
                    case 4:
                        $status = 'inQueue';
                        break;
                    case 5:
                        $status = 'Duplicate';
                        break;
                }
                $row['id'] = $val->id;
                $row['ocisid'] = $val->ocs_id;
                $row['ndisid'] = $val->id;
                $row['company_name'] = $val->company_name;
                $row['biller'] = $val->company_name;
                $row['lastname'] = $val->firstname;
                $row['firstname'] = $val->lastname;
                $row['full_name'] = $val->firstname . ' ' . $val->lastname;
                $row['due_date'] = $val->due_date;
                $row['duedate'] = $val->due_date;
                $row['invoice_amount'] = $val->invoice_amount;
                $row['total'] = $val->invoice_amount;
                $row['invoice_number'] = $val->invoice_number;
                $row['invoice'] = $val->invoice_number;
                $row['participant_email'] = $val->participant_email;
                $row['status'] = $status;
                $dataResult[] = $row;
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'participant' => $p_data, 'planner_id' => $planner['r_next_id'], 'status' => true);
        return $return;
    }

    public function invoice_list($reqData) {
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;

        $type = isset($reqData->invoice_type) ? $reqData->invoice_type : 0;

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $type == 4 ? 'due_date' : 'updated';
            $direction = $type == 4 ? 'asc' : 'desc';
        }

        $planner = $this->get_planner_details('', '', true);

        $search_colums = array('pm.invoice_number', 'pm.due_date', 'pm.due_date', 'pm.participant_email',
            'pm.account_number', 'pm.invoice_date', 'pm.company_name', 'pm.company_name', 'pm.invoice_amount', 'pm.invoice_amount',
            'p.firstname', 'p.lastname', 'p.id', 'p.ndis_num');

        if (!empty($filter->a_invoice_type)) {
            $type = $filter->a_invoice_type;
        }

        if (isset($filter->search)) {
            $search = $filter->search;

            $this->db->group_start();
            for ($i = 0; $i < count($search_colums); $i++) {
                $column_search = $search_colums[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $search);
                } else {
                    $this->db->or_like($column_search, $search);
                }
            }
            $this->db->group_end();
        }


        $select_column = array('p.ndis_num as ndisid', 'pm.id', 'pm.invoice_number as invoice', 'pm.due_date', 'pm.due_date as duedate', 'pm.participant_email',
            'pm.account_number', 'pm.invoice_date', 'pm.company_name', 'pm.company_name as biller', 'pm.invoice_amount as total', 'pm.invoice_amount',
            'p.firstname', 'p.lastname', 'p.id as ocisid');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        $this->db->select("(CASE
              WHEN pm.status=1
               THEN 'Approved'
               WHEN pm.status=2
                 THEN 'PotentialDuplicate'
               WHEN pm.status=3
                 THEN 'followUp'
               WHEN pm.status=4
                 THEN 'inQueue'
               WHEN pm.status=5
                 THEN 'Duplicate'
               WHEN pm.status=6
                 THEN 'Archive'
              END) as status");
        $this->db->join('tbl_participant as p', 'p.id = pm.participant_id', 'right');
        $this->db->from('tbl_plan_management as pm');


        $where = ($type == 0) ? array('pm.status!=' => 6) : (($type == 7) ? "( pm.status= 2 OR pm.status = 5)" : array('pm.status' => $type));
        $this->db->where($where);

        $this->db->where('pm.participant_id > 0 AND pm.read_status !=0 AND pm.load_status!=0', null, false);
        $this->db->order_by($orderBy, $direction);


        if (isset($reqData->pageSize)) {
            $this->db->limit($reqData->pageSize, ($reqData->page * $reqData->pageSize));
        }

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if (isset($reqData->pageSize)) {
            if ($dt_filtered_total % $reqData->pageSize == 0) {
                $dt_filtered_total = ($dt_filtered_total / $reqData->pageSize);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $reqData->pageSize)) + 1;
            }
        }

        $dataResult = $query->result();
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'status' => true, 'planner_id' => $planner['r_next_id']);
        return $return;
    }

    public function invoice_details($reqData, $adminId) {
        if (!empty($reqData)) {
            $invoiceId = $reqData;
            $tbl = TBL_PREFIX . 'plan_management';
            $tbl3 = TBL_PREFIX . 'participant';
            $Where = array($tbl . '.id' => $invoiceId);
            $this->db->select(array($tbl . '.reference_no', $tbl . '.biller_code', $tbl . '.account_name', $tbl . '.payment_method', $tbl . '.id as invoice_id', $tbl . '.invoice_number', $tbl . '.due_date', $tbl . '.participant_email', $tbl . '.status as invoice_status',
                $tbl . '.account_number', $tbl . '.invoice_date', $tbl . '.company_name', $tbl . '.invoice_amount', $tbl . '.html_url', $tbl . '.original_id',
                $tbl3 . '.firstname', $tbl3 . '.lastname', $tbl3 . '.id as ocs_id', $tbl3 . '.houseId as funds', $tbl3 . '.status'
            ));
            $this->db->join($tbl3, $tbl3 . '.id = ' . $tbl . '.participant_id', 'left');
            $this->db->from($tbl);
            $this->db->where($Where);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $dataResult = array();
            $row1 = array();
            $row = array();
            $original_data = array();
            $original_id = "";
            $original_id1 = "";
            if (!empty($query->result())) {
                $biller_name = '';
                $biller_status = $this->check_biller_refernce_no($query->row(), $adminId);
                if (isset($biller_status['data'])) {
                    $biller_name = $biller_status['data'];
                }
                $row['biller_name'] = $biller_name;
                $row1['validate'] = $this->pdftohtmlDataValidation($query->row(), $adminId);
                foreach ($query->result() as $k => $val) {
                    // select id from tbl_plan_management where id = (select min(id) from tbl_plan_management where id > 3 AND status = 4)


                    $planner = $this->get_planner_details($val->invoice_id, $val->due_date, false);
                    $duplicate_count = $this->get_planner_details($val->invoice_id, $val->due_date, false, 7);
                    $getStatusDetails = $this->get_status_details($val->invoice_status);
                    $row['duplicate_count'] = $duplicate_count['r_count'];
                    $row['duplicate_position'] = $duplicate_count['r_position'];
                    $row['invoice_status_class'] = $getStatusDetails['class'];
                    $row['invoice_status_text'] = $getStatusDetails['text'];
                    //if payment method is 1(bpay) -fileds --biller_code, reference_no
                    //if 2(aus post) -- billpaycode(biller_code), reference_no
                    //if 3 (bank) -- bsb(biller_code),account_name,account_no(reference_no)
                    switch ($val->payment_method) {
                        case '1':
                            $row['reference_no'] = $val->reference_no;
                            $row['biller_code'] = $val->biller_code;
                            break;
                        case '2':
                            $row['ref_no'] = $val->reference_no;
                            $row['billpay_code'] = $val->biller_code;
                            break;
                        case '3':
                            $row['acc_no'] = $val->reference_no;
                            $row['acc_name'] = $val->account_name;
                            $row['bsb'] = $val->biller_code;
                            break;
                    }
                    $row['amount_paid'] = 0;
                    $row['r_count'] = $planner['r_count'];
                    $row['r_next_id'] = $planner['r_next_id'];
                    $row['r_position'] = $planner['r_position'];
                    $row['id'] = $val->invoice_id;
                    $row['company_name'] = $val->company_name;
                    $row['invoice_date'] = ($val->invoice_date != '0000-00-00') ? date("d/m/Y", strtotime($val->invoice_date)) : '';
                    $row['date_diff'] = strtotime($val->invoice_date) - strtotime(date('Y-m-d'));
                    $row1['due_date'] = ($val->due_date != '0000-00-00') ? date("d/m/Y", strtotime($val->due_date)) : '';
                    $row['invoice_amount'] = $val->invoice_amount;
                    $row['invoice_number'] = $val->invoice_number;
                    $row['account_name'] = $val->account_name;
                    $row['payment_method'] = $val->payment_method;
                    $row['account_number'] = $val->account_number;
                    $row['status'] = $val->status;
                    $row['original_id'] = $val->original_id;
                    $original_id = $val->original_id;
                    $row['funds'] = $val->funds;
                    $row1['participant_firstname'] = array('value' => $val->ocs_id, 'label' => $val->firstname);
                    $row1['Fullname'] = $val->firstname . ' ' . $val->lastname;
                    $row1['participant_lastname'] = array('value' => $val->ocs_id, 'label' => $val->lastname);
                    $row1['participant_ocs_id'] = array('label' => $val->ocs_id, 'value' => $val->ocs_id);
                    $row1['participant_ocs_email_acc'] = $val->participant_email;
                    //xero amount paid
                    if ($val->invoice_status == 3) {
                        $fillterRequest = ['page' => 1];
                        $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
                        if ($inoviceDetails['status'] == true && $inoviceDetails['data']) {
                            $invoiceStatus = array();
                            $no_of_records_followup = 0;
                            // foreach($import_data_array as $dataarray){
                            $invoiceNumberExist = $this->multi_array_search(trim($val->invoice_number), $inoviceDetails['data']);
                            if ($invoiceNumberExist) {
                                $statusRequest = ['InvoiceID' => $invoiceNumberExist['InvoiceID']];
                                $invoiceStatus = $this->xeroinvoice->get_single_inovice($statusRequest);
                                // var_dump($invoiceStatus);
                                $row['amount_paid'] = $invoiceStatus['amount'];
                            }
                        }
                    }
                    $urls = explode(',', $val->html_url);
                    foreach ($urls as $url) {
                        // if(!empty($url))
                        $row1['file_contents'][] = $this->highlight($url, $row, $val->ocs_id);
                    }
                    $row['html_url'] = $val->html_url;
                    $ext = trim(pathinfo($val->html_url, PATHINFO_EXTENSION));
                    if ($ext == 'htm')
                        $ext = 'html';
                    if ($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg')
                        $ext = 'image';
                    $row['ext'] = $ext;
                    $dataResult = array_merge($row, $row1);
                    //$dataResult[] = $row1;
                }
                if (!empty($original_id)) {
                    $colums = array($tbl . '.reference_no', $tbl . '.account_name', $tbl . '.payment_method', $tbl . '.biller_code', $tbl . '.id as invoice_id', $tbl . '.invoice_number', $tbl . '.due_date', $tbl . '.participant_email', $tbl . '.status as invoice_status',
                        $tbl . '.account_number', $tbl . '.invoice_date', $tbl . '.company_name', $tbl . '.invoice_amount', $tbl . '.html_url', $tbl . '.original_id',
                        $tbl3 . '.firstname', $tbl3 . '.lastname', $tbl3 . '.id as ocs_id', $tbl3 . '.houseId as funds', $tbl3 . '.status');
                    $sWhere = array($tbl . '.id' => $original_id);
                    $this->db->select($colums);
                    $this->db->join($tbl3, $tbl3 . '.id = ' . $tbl . '.participant_id', 'left');
                    $this->db->from($tbl);
                    $this->db->where($sWhere);
                    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
                    $original_data = $query->row_array();
                    //duplicate values
                    $original_data['validate'] = $this->pdftohtmlDataValidation($query->row(), $adminId);
                    $original_biller_name = '';
                    $original_biller_status = $this->check_biller_refernce_no($query->row(), $adminId);
                    if (isset($original_biller_status['data'])) {
                        $original_biller_name = $original_biller_status['data'];
                    }
                    $original_data['biller_name'] = $original_biller_name;
                    $original_data['id'] = $original_data['invoice_id'];
                    // $original_data['company_name'] = $original_data['company_name'];
                    // $original_data['reference_no'] = $val->reference_no;
                    // $original_data['biller_code'] = $val->biller_code;
                    $row['invoice_date'] = ($val->invoice_date != '0000-00-00') ? date("d/m/Y", strtotime($val->invoice_date)) : '';
                    $row['date_diff'] = strtotime($val->invoice_date) - strtotime(date('Y-m-d'));
                    $row1['due_date'] = ($val->due_date != '0000-00-00') ? date("d/m/Y", strtotime($val->due_date)) : '';
                    // $original_data['invoice_amount'] = $original_data['invoice_amount'];
                    // $original_data['invoice_number'] = $original_data['invoice_number'];
                    // $original_data['account_number'] = $original_data['account_number'];
                    // $original_data['status'] = $original_data['status'];
                    //
                    $getDuplicateStatusDetails = $this->get_status_details($original_data['invoice_status']);
                    $original_data['invoice_status_class'] = $getDuplicateStatusDetails['class'];
                    $original_data['invoice_status_text'] = $getDuplicateStatusDetails['text'];
                    switch ($original_data['payment_method']) {
                        case '1':
                            $original_data['reference_no'] = $original_data['reference_no'];
                            $original_data['biller_code'] = $original_data['biller_code'];
                            break;
                        case '2':
                            $original_data['ref_no'] = $original_data['reference_no'];
                            $original_data['billpay_code'] = $original_data['biller_code'];
                            break;
                        case '3':
                            $original_data['acc_no'] = $original_data['reference_no'];
                            $original_data['acc_name'] = $original_data['account_name'];
                            $original_data['bsb'] = $original_data['biller_code'];
                            break;
                    }
                    // $original_data['funds'] = $original_data['funds'];
                    $original_data1['participant_firstname'] = array('value' => $original_data['ocs_id'], 'label' => $original_data['firstname']);
                    $original_data1['Fullname'] = $original_data['firstname'] . ' ' . $original_data['lastname'];
                    $original_data1['participant_lastname'] = $original_data['lastname'];
                    $original_data1['participant_ocs_id'] = array('value' => $original_data['ocs_id'], 'label' => $original_data['ocs_id']);
                    $original_data1['participant_ocs_email_acc'] = $original_data['participant_email'];
                    $urls = explode(',', $original_data['html_url']);
                    foreach ($urls as $url) {
                        $original_data1['file_contents'][] = $this->highlight($url, $original_data, $original_data['ocs_id']);
                    }
                    // $original_data['html_url'] = $original_data['html_url'];
                    $dataResult['original_invoce'] = array_merge($original_data, $original_data1);
                }
            }
            return $dataResult;
        }
    }

    public function get_status_details($invoice_status) {
        $data = array();
        switch ((int) ($invoice_status)) {
            case 1: $data['class'] = 'cmn-btn3';
                $data['text'] = 'Approved';
                break;
            case 2: $data['class'] = 'cmn-btn7';
                $data['text'] = 'Potential Duplicate';
                break;
            case 3: $data['class'] = 'cmn-btn4';
                $data['text'] = 'Follow Up';
                break;
            case 4: $data['class'] = 'cmn-btn8';
                $data['text'] = 'Pending';
                break;
            case 5: $data['class'] = 'cmn-btn7';
                $data['text'] = 'Duplicate';
                break;
            default: $data['class'] = 'cmn-btn7';
                $data['text'] = 'Archived';
                break;
        }
        return $data;
    }

    public function get_planner_details($id, $due_date, $list = false, $status = null) {
        $tbl = 'tbl_plan_management';
        $where = "load_status='1' AND read_status='1'";
        $this->db->SELECT('id,due_date');
        $this->db->from($tbl);
        $this->db->where($where);
        if ($status == 7) {
            $this->db->where("status = 2 OR status=5");
            $this->db->order_by('updated', 'asc');
        } else {
            $this->db->where("status = 4");
            $this->db->order_by('due_date', 'asc');
        }
        $result = $this->db->get()->result_array();
        $data = array();
        $data['r_count'] = count($result);
        $data['r_next_id'] = 0;
        $data['r_position'] = 0;
        foreach ($result as $key => $res) {
            if ($status == 7) {
                if ($res['id'] != $id) {
                    $data['r_next_id'] = $res['id'];
                    $data['r_position'] = $key + 1;
                    return $data;
                } else {
                    $data['r_next_id'] = $result[0]['id'];
                    $data['r_position'] = 1;
                }
            } else if ($res['id'] != $id && (strtotime($res['due_date']) > strtotime($due_date)) && !$list) {
                $data['r_next_id'] = $res['id'];
                $data['r_position'] = $key;
                return $data;
            } else {
                $data['r_next_id'] = $result[0]['id'];
                $data['r_position'] = 1;
            }
        }
        return $data;
        // $select_column = array($tbl.'.id','count('.$tbl.'.id) as r_count','min(id) as r_next_id',);
        // $this->db->select($select_column);
    }

    public function multi_array_search($search_for, $search_in) {
        foreach ($search_in as $element) {
            if (($element === $search_for)) {
                return true;
            } else if (is_array($element)) {
                $result = $this->multi_array_search($search_for, $element);
                if ($result == true) {
                    return $element;
                }
            }
        }
        return false;
    }

    public function highlight($url = '', $words = '', $p_id) {
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $contents = "";
        if (!empty($url) && $ext == 'html') {
            $content = file_get_contents(FCPATH . $url);
            $content = htmlspecialchars_decode($content);
            $html = str_get_html($content);
            if (!$html->find('img[draggable="false"]', 0)) {
                $content = preg_replace('/' . preg_quote('src="', '/') . '/', 'draggable="false" src="' . base_url() . PLAN_MANAGEMENT_HTMLFILES . $p_id . '/', $content);
            }
            // var_dump(gettype( $content));
            if (!empty($words)) {
                foreach ($words as $k => $v) {
                    // var_dump(gettype( $v);
                    if (gettype($v) == 'string' && (strlen($content) > 0 && strlen($v) > 0)) {
                        switch ($k) {
                            case 'company_name':
                                if (!$html->find('span[ref=company_name]', 0)) {
                                    $content = preg_replace('/' . preg_quote($v, '/') . '/', "<span ref='company_name' style='background:lightgreen'>$v</span>", $content);
                                }
                                break;
                            case 'invoice_date':
                                if (!$html->find('span[ref=invoice_date]', 0)) {
                                    $content = preg_replace('/' . preg_quote($v, '/') . '/', "<span ref='invoice_date' style='background:lightgreen'>$v</span>", $content);
                                }
                                break;
                            case 'due_date':
                                if (!$html->find('span[ref=due_date]', 0)) {
                                    $content = preg_replace('/' . preg_quote($v, '/') . '/', "<span ref='due_date' style='background:lightgreen'>$v</span>", $content);
                                }
                                break;
                            case 'invoice_amount':
                                if (!$html->find('span[ref=invoice_amount]', 0)) {
                                    $content = preg_replace('/' . preg_quote($v, '/') . '/', "<span ref='invoice_amount' style='background:lightgreen'>$v</span>", $content);
                                }
                                break;
                            case 'invoice_number':
                                if (!$html->find('span[ref=invoice_number]', 0)) {
                                    $content = preg_replace('/' . preg_quote($v, '/') . '/', "<span ref='invoice_number' style='background:lightgreen'>$v</span>", $content);
                                }
                                break;
                        }
                    }
                }
            }
            $contents = array();
            $contents['pages'][] = array('Page1');
            $contents['contents'][] = array($content);
            file_put_contents(FCPATH . $url, $content);
        }
        return $contents;
    }

    //onload validation
    public function pdftohtmlDataValidation($data, $adminId) {
        $response = array();
        $participant_details = $this->basic_model->get_row($table_name = 'participant', $columns = array('firstname, lastname, id'), $id_array = array('id' => $data->ocs_id));
        // Last Name
        if ($data->firstname != $participant_details->firstname && $data->lastname != $participant_details->lastname && $data->ocs_id != $participant_details->id) {
            $response['participant_firstname'] = array('error' => true, 'message' => "First name, Last name, HCM ID & Email doesn't belong to this participant");
        } else {
            $response['participant_firstname'] = array('error' => false);
        }
        //  Validate for company
        $companyId = 1;
        $page = !empty($page) && $page > 0 ? $page : 1;
        $order_by = ['field_name' => 'Name', 'direction' => 'ASC'];
        $cmp = false;
        // $contactDetails = $this->Basic_model->get_record_where('plan_management_vendor','company_name,biller_code,billpay_code');
        // if($data->company_name!=null){
        //   $cmp = in_array(trim($data->company_name), array_column($contactDetails, 'company_name'));
        $contactDetails = $this->xerocontact->get_contact_list($companyId, ['page' => $page, 'modifiedAfter' => new \DateTime(date('2019-07-17')), 'order_by' => $order_by]);
        if ($data->company_name != null && isset($contactDetails['data'])) {
            $cmp = in_array(trim($data->company_name), array_column($contactDetails['data'], 'Name'));
            if ($cmp == false) {
                $response['new_company'] = array('error' => true);
                $response['company_name'] = array('error' => true, 'message' => 'Company Name does Not Match');
            } else {
                $response['company_name'] = array('error' => false);
            }
        } else {
            $response['company_name'] = array('error' => true, 'message' => 'Company Name does Not Match');
        }
        // validate for company first time serive for participant
        $participant_email = $data->participant_email;
        $company_details = $this->Basic_model->get_record_where('plan_management', 'company_name', array('participant_email' => $participant_email));
        $count = 0;
        foreach ($company_details as $value) {
            if ((trim($data->company_name)) == trim($value->company_name)) {
                $count++;
            }
        }
        if ($count > 1 || $cmp == false) {
            $response['service_count'] = array('error' => false);
        } else {
            $response['service_count'] = array('error' => true, 'message' => "This is the first time $data->firstname has received an invoice from $data->company_name");
        }
        // Here need to check available fund once data available in funds table
        if ((float) $data->invoice_amount <= 0) {
            $response['invoice_amount'] = array('error' => true, 'message' => 'Invoice Amount should be greater than 0');
        } else if ($data->funds < $data->invoice_amount && $data->funds > 0) {
            $response['invoice_amount'] = array('error' => true, 'message' => "Participant's balance $data->funds is not sufficient to clear the invoice amount $data->invoice_amount");
        } else {
            $response['invoice_amount'] = array('error' => false);
        }
        //Xero invoice number validation
        $fillterRequest = ['page' => $page];
        $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
        // var_dump($inoviceDetails);
        if ($data->invoice_number != null && $inoviceDetails['status']) {
            $invoice_cmp = in_array(trim($data->invoice_number), array_column($inoviceDetails['data'], 'InvoiceNumber'));
            if ($invoice_cmp == false) {
                $response['invoice_number'] = array('error' => true, 'message' => 'Not Known Biller');
            } else {
                $response['invoice_number'] = array('error' => false);
            }
        } else {
            $response['invoice_number'] = array('error' => true, 'message' => 'Not Known Biller');
        }
        // Here need to check available fund once data available in funds table
        //biller validation
        $response['billpay_error'] = array('error' => false, 'message' => '');
        $response['ref_no_error'] = array('error' => false, 'message' => '');
        $response['bsb_error'] = array('error' => false, 'message' => '');
        $response['account_no_error'] = array('error' => false, 'message' => '');
        $response['account_name_error'] = array('error' => false, 'message' => '');
        $response['biller_code_reference_no_check'] = array('error' => false, 'message' => '');
        $response['reference_no_error'] = array('error' => false, 'message' => '');
        switch ($data->payment_method) {
            case '1':
                if ($data->biller_code != null) {
                    $biller_status = $this->check_biller_refernce_no($data, $adminId);
                    if (isset($biller_status['status'])) {
                        $response['biller_code_reference_no_check'] = array('error' => false, 'message' => '');
                    } else {
                        $response['biller_code_reference_no_check'] = array('error' => true, 'message' => 'Not Known Biller Code');
                    }
                } else {
                    $response['biller_code_reference_no_check'] = array('error' => true, 'message' => '');
                }
                if (strlen($data->reference_no) == 0 || strlen($data->reference_no) > 15) {
                    $response['reference_no_error'] = array('error' => false, 'message' => 'Reference Number should be in between 1 to 15');
                } else {
                    $response['reference_no_error'] = array('error' => true, 'message' => '');
                }
                break;
            case '2':
                if (strlen($data->biller_code) == 0 || strlen($data->biller_code) > 4) {
                    $response['billpay_error'] = array('error' => false, 'message' => 'Billpay should be in between 1 to 4');
                } else {
                    $response['billpay_error'] = array('error' => true, 'message' => '');
                }
                if (strlen($data->reference_no) == 0 || strlen($data->reference_no) > 15) {
                    $response['ref_no_error'] = array('error' => false, 'message' => 'Reference Number should be in between 1 to 15');
                } else {
                    $response['ref_no_error'] = array('error' => true, 'message' => '');
                }
                break;
            case '3':
                if (strlen($data->biller_code) == 0 || strlen($data->biller_code) > 4) {
                    $response['bsb_error'] = array('error' => false, 'message' => 'BSB Code is required');
                } else {
                    $response['bsb_error'] = array('error' => true, 'message' => '');
                }
                $check_acc_no = preg_match("/^\d{3}-?\d{3}$/", $data->reference_no);
                if (!$check_acc_no) {
                    $response['account_no_error'] = array('error' => false, 'message' => 'The length is 6 digits without dash or 7 digits with dash and contains only numbers. ex:123-123 or 123123');
                } else {
                    $response['account_no_error'] = array('error' => true, 'message' => '');
                }
                if (strlen($data->account_name) == 0) {
                    $response['account_name_error'] = array('error' => false, 'message' => 'Account Name is required');
                } else {
                    $response['account_name_error'] = array('error' => true, 'message' => '');
                }
                break;
        }
        //invoice date validation
        $today = date("Y-m-d");
        $previous = date("Y-m-d", strtotime("-1 year"));
        if ($data->invoice_date != '0000-00-00' && (strtotime($previous) <= strtotime($data->invoice_date)) && (strtotime($data->invoice_date) <= strtotime($today))) {
            $response['invoice_date'] = array('error' => false);
        } else {
            $response['invoice_date'] = array('error' => true, 'message' => 'Invalid Invoice Date,Invoice date is more than 1 year old OR invoice date is in the future');
        }
        //due date validation
        if ($data->due_date != '0000-00-00' && (strtotime($today) < strtotime($data->due_date))) {
            $response['due_date'] = array('error' => false);
        } else {
            $response['due_date'] = array('error' => true, 'message' => 'Invalid Due Date, Invoice due date is in the past');
        }
        return $response;
    }

    public function status_change($invoice_status, $id, $invoicedetails, $adminId = 1) {
        $tbl = 'plan_management';
        switch ($invoice_status) {
            case 'Paid':
                $status = 1;
                if (!empty($invoicedetails)) {
                    $res = $this->invoice_update($invoicedetails);
                    $funds = floatval($invoicedetails->funds) - floatval($invoicedetails->invoice_amount);
                    $this->basic_model->update_records('participant', array('houseId' => $funds), array('id' => $invoicedetails->participant_ocs_id->value));

                    $create_draft = $this->create_draft($invoicedetails);
                    $action = 1;
                    $title = 'Invoice moved to Paid';
                }
                break;
            case 'Duplicate':
                $status = 5;
                if (!empty($invoicedetails)) {
                    $res = $this->invoice_update($invoicedetails);
                    $action = 2;
                    $title = 'Invoice moved to Duplicate';
                }
                break;
            case 'followUp':
                $status = 3;
                if (!empty($invoicedetails))
                    $res = $this->invoice_update($invoicedetails);
                $title = 'Invoice moved to followUp';
                $action = 3;
                break;
            case 'inQueue':
                $status = 4;
                if (!empty($invoicedetails))
                    $res = $this->invoice_update($invoicedetails);
                $action = 4;
                $title = 'Invoice moved to inQueue';
                break;
            case 'Archieve': $status = 6;
                $action = 6;
                $title = 'Invoice moved to Archieve';
                break;
        }
        $this->logs_lib->setUserId($adminId);
        $this->logs_lib->setTitle($title);
        $this->logs_lib->setDescription($title);
        $this->logs_lib->setAction($action);
        $this->logs_lib->setPlanId($invoicedetails->id);
        $this->logs_lib->setParticipantId($invoicedetails->participant_ocs_id->value);
        $this->logs_lib->createLog();
        $data = array('status' => $status, 'updated' => date("Y-m-d H:i s"));
        $where = array('id' => $id);
        $response = $this->basic_model->update_records($tbl, $data, $where);
        return $response;
    }

    public function invoice_update($invoicedetails, $adminId = 11) {
        $tbl = 'plan_management';
        $invoice_date = empty($invoicedetails->invoice_date) ? '0000-00-00' : date("Y-m-d", strtotime(str_replace('/', '-', $invoicedetails->invoice_date)));
        $due_date = date("Y-m-d", strtotime(str_replace('/', '-', $invoicedetails->due_date)));
        $company_name = is_object($invoicedetails->company_name) ? $invoicedetails->company_name->value : $invoicedetails->company_name;
        $invoice_number = $invoicedetails->invoice_number;
        $invoice_amount = $invoicedetails->invoice_amount;
        $payment_method = $invoicedetails->payment_method;
        $account_name = '';
        switch ($payment_method) {
            case 1:$biller_code = $invoicedetails->biller_code;
                $reference_no = $invoicedetails->reference_no;
                break;
            case 2: $reference_no = $invoicedetails->ref_no;
                $biller_code = $invoicedetails->billpay_code;
                break;
            case 3:$biller_code = $invoicedetails->bsb;
                $reference_no = $invoicedetails->acc_no;
                $account_name = $invoicedetails->acc_name;
                break;
        }
        // $biller_code= $invoicedetails->biller_code;
        // $reference_no= $invoicedetails->reference_no;
        // $po_number= $invoicedetails->po_number;
        $id = $invoicedetails->id;
        $data2 = array('invoice_date' => $invoice_date, 'company_name' => $company_name, 'due_date' => $due_date, 'invoice_number' => $invoice_number, 'invoice_amount' => $invoice_amount, 'payment_method' => $payment_method, 'biller_code' => $biller_code,
            'reference_no' => $reference_no, 'account_name' => $account_name);
        $where = array('id' => $id);
        $swhere = array('id' => $invoicedetails->id);
        $this->db->select('*');
        $this->db->from('tbl_plan_management');
        $this->db->where($swhere);
        $data = $this->db->get()->row_array();
        $changed_data = '';
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'participant_email':
                    if (strcmp($value, $invoicedetails->participant_ocs_email_acc) != 0) {
                        $changed_data .= "Participant Changed <br/>";
                    }
                    break;
                case 'invoice_number':
                    if (strcmp($value, $invoicedetails->invoice_number) != 0) {
                        $changed_data .= "Invoice number Changed <br/>";
                    }
                    break;
                case 'invoice_date':
                    if (strcmp(date("d/m/Y", strtotime($value)), $invoice_date) != 0) {
                        $changed_data .= "Invoice Date Changed <br/>";
                    }
                    break;
                case 'company_name':
                    if (strcmp($value, $company_name) != 0) {
                        $changed_data .= "Company Name Changed <br/>";
                    }
                    break;
                case 'account_number':
                    if (strcmp($value, $invoicedetails->account_number) != 0) {
                        $changed_data .= "Account Number Changed <br/>";
                    }
                    break;
                case 'invoice_amount':
                    if (strcmp($value, $invoicedetails->invoice_amount) != 0) {
                        $changed_data .= "Invoice Amount Changed <br/>";
                    }
                    break;
                //   case 'biller_code':
                //  if(strcmp($value,$invoicedetails->biller_code)!= 0){
                //      $changed_data .= "Biller Code Changed <br/>";
                //  }
                //   break;
                //  case 'reference_no':
                //   if(strcmp($value,$invoicedetails->reference_no)!= 0){
                //       $changed_data .= "Reference Number Changed <br/>";
                //   }
                // break;
            }
        }
        if (!empty($changed_data)) {
            $this->logs_lib->setUserId($adminId);
            $this->logs_lib->setTitle('Invoice manual input');
            $this->logs_lib->setDescription($changed_data);
            $this->logs_lib->setAction(5);
            $this->logs_lib->setPlanId($invoicedetails->id);
            $this->logs_lib->setParticipantId($invoicedetails->participant_ocs_id->value);
            $this->logs_lib->createLog();
        }
        $res = $this->basic_model->update_records($tbl, $data2, $where);
        return $res;
    }

    public function add_dispute_note($reqData) {
        $disputeData = array(
            'reason' => $reqData->reason,
            'raised' => $reqData->raised,
            'contact_name' => $reqData->contact_name,
            'contact_method' => $reqData->contact_method,
            'notes' => $reqData->notes,
            'invoice_id' => $reqData->invoice_id
        );

        if (isset($reqData->notes_id) && !empty($reqData->notes_id)) {
            $where = array('id' => $reqData->notes_id);
            $result = $this->basic_model->update_records('plan_management_dispute_note', $disputeData, $where);
        } else {
            $result = $this->basic_model->insert_records('plan_management_dispute_note', $disputeData);
        }

        return true;
    }

    public function get_list_dispute_note($data) {
        $this->db->select(['dn.id as notes_id', 'dn.reason', 'dn.contact_name', 'dn.contact_method', 'dn.raised', 'dn.notes', 'dn.contact_method', 'cm.name as contact_method_name']);
        $this->db->from('tbl_plan_management_dispute_note as dn');
        $this->db->join('tbl_plan_dispute_contact_method as cm', 'cm.id = dn.contact_method', 'inner');
        $this->db->where(array('dn.invoice_id' => $data->data));
        $this->db->order_by('dn.id', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function get_participant_name($post_data) {
        $tbl = TBL_PREFIX . 'participant';
        $tbl2 = TBL_PREFIX . 'participant_email';
        // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);
        $this->db->select($tbl . ".firstname," . $tbl . ".lastname," . $tbl . ".id as ocs_id," . $tbl2 . ".email");
        $this->db->join($tbl2, $tbl2 . '.participantId = ' . $tbl . '.id', 'left');
        $this->db->or_where($tbl . ".firstname LIKE  '%$post_data%' ");
        $this->db->or_where($tbl . ".lastname LIKE  '%$post_data%' ");
        $this->db->where($tbl . '.archive =', 0);
        $this->db->where($tbl . '.status=', 1);
        $this->db->where($tbl2 . '.primary_email=', 1);
        $query = $this->db->get(TBL_PREFIX . 'participant');
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('value' => $val->ocs_id, 'label' => $val->firstname . ' ' . $val->lastname, 'email' => $val->email, 'lastname' => $val->lastname, 'firstname' => $val->firstname);
            }
        }
        return $participant_rows;
    }

    public function get_company_name($post_data) {
        // $tbl = TBL_PREFIX . 'plan_management_vendor';
        // $this->db->select($tbl.".company_name,".$tbl.".id as id,");
        // $this->db->or_where($tbl.".company_name LIKE  '%$post_data%' ");
        // $this->db->where($tbl.'.status=', 1);
        // $query = $this->db->get(TBL_PREFIX . 'plan_management_vendor');
        // $contactDetails = $query->result();
        $companyId = 1;
        $page = !empty($page) && $page > 0 ? $page : 1;
        $order_by = ['field_name' => 'Name', 'direction' => 'ASC'];
        $contactDetails = $this->xerocontact->get_contact_list($companyId, ['page' => $page, 'modifiedAfter' => new \DateTime(date('2019-07-17')), 'order_by' => $order_by]);
        $company_rows = array();
        if ($contactDetails['status']) {
            $cmp = array_column($contactDetails['data'], 'Name');
            if (!empty($cmp)) {
                foreach ($cmp as $val) {
                    $company_rows[] = array('value' => $val, 'label' => $val,);
                }
            }
        }
        return $company_rows;
    }

    public function get_participant_id() {
        $tbl = TBL_PREFIX . 'participant';
        $tbl2 = TBL_PREFIX . 'participant_email';
        // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);,'firstname' => $val->firstname
        $this->db->select($tbl . ".firstname," . $tbl . ".lastname," . $tbl . ".id as ocs_id," . $tbl2 . ".email");
        $this->db->join($tbl2, $tbl2 . '.participantId = ' . $tbl . '.id', 'left');
        $this->db->where($tbl . '.archive =', 0);
        $this->db->where($tbl . '.status=', 1);
        //$this->db->where($tbl2.'.primary_email=', 1);
        $query = $this->db->get(TBL_PREFIX . 'participant');
        $query->result();
        $participant_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $participant_rows[] = array('label' => $val->ocs_id, 'value' => $val->firstname, 'email' => $val->email, 'lastname' => $val->lastname);
            }
        }
        return $participant_rows;
    }

    public function list_audit_logs($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = array('created');
        $direction = 'Desc';

        if (!empty(request_handler())) {
            $tbl_1 = TBL_PREFIX . 'plan_management_audit_logs';
            $tbl_2 = TBL_PREFIX . 'member';
            $tbl_3 = TBL_PREFIX . 'plan_management';
            $tbl_4 = TBL_PREFIX . 'participant';
            $dt_query = $this->db->select(array($tbl_1 . '.title', $tbl_1 . '.id ', $tbl_1 . '.Description', $tbl_1 . '.created', "CONCAT(tbl_member.firstname,' ',tbl_member.lastname) AS user_name", $tbl_1 . '.user', $tbl_1 . '.participant_id', $tbl_1 . '.action',
                $tbl_3 . '.invoice_number', $tbl_3 . '.invoice_amount', $tbl_3 . '.company_name', $tbl_3 . '.due_date', $tbl_4 . '.firstname'));

            $this->db->from($tbl_1);
            $this->db->join('tbl_member', 'tbl_plan_management_audit_logs.user = tbl_member.id', 'inner');
            $this->db->join('tbl_plan_management', 'tbl_plan_management_audit_logs.plan_id = tbl_plan_management.id', 'inner');
            $this->db->join('tbl_participant', 'tbl_participant.id = tbl_plan_management.participant_id', 'inner');
            $this->db->where($tbl_2 . '.archive=', "0");

            //pr($sorted);
            if (!empty($sorted)) {
                if (!empty($sorted[0]->id)) {
                    if ($sorted[0]->id) {
                        $orderBy = $sorted[0]->id;
                        switch ($orderBy) {
                            case 'date':
                                $orderBy = $tbl_1 . '.created';
                                break;
                            case 'time':
                                $orderBy = $tbl_1 . '.created';
                                break;
                            case 'user':
                                $orderBy = $tbl_1 . '.user';
                                break;
                            default:
                                // code...
                                break;
                        }
                    } else {
                        $orderBy = 'tbl_member.' . $sorted[0]->id;
                    }
                    $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
                }
            } else {
                $orderBy = 'tbl_plan_management_audit_logs.id';
                $direction = 'desc';
            }
            if (!empty($filter)) {

                if (!empty($filter->filterVal)) {

                    $this->db->group_start();
                    $src_columns = array($tbl_1 . '.title', $tbl_1 . '.user', $tbl_1 . '.participant_id', $tbl_1 . '.action', $tbl_4 . '.firstname', $tbl_4 . '.lastname');

                    for ($i = 0; $i < count($src_columns); $i++) {
                        $column_search = $src_columns[$i];
                        if (strstr($column_search, "as") !== false) {
                            $serch_column = explode(" as ", $column_search);
                            $this->db->or_like($serch_column[0], $filter->filterVal);
                        } else {
                            $this->db->or_like($column_search, $filter->filterVal);
                        }
                    }
                    $this->db->group_end();
                }


                if (!empty($filter->fromDate) && !empty($filter->toDate)) {
                    $this->db->where('DATE(tbl_plan_management_audit_logs.created)>="' . $filter->fromDate . '"');
                    $this->db->where('DATE(tbl_plan_management_audit_logs.created)<="' . $filter->toDate . '"');
                } else if (!empty($filter->fromDate)) {
                    $this->db->where('DATE(tbl_plan_management_audit_logs.created)>="' . $filter->fromDate . '"');
                } else if (!empty($filter->toDate)) {
                    $this->db->where('DATE(tbl_plan_management_audit_logs.created)<="' . $filter->toDate . '"');
                }
            }
            $this->db->order_by($orderBy, $direction);
            $this->db->limit($limit, ($page * $limit));
            $query = $this->db->get();
            // echo $this->db->last_query();             

            $dt_filtered_total = $all_count = $this->db->query('SELECT count(*) as count from tbl_plan_management_audit_logs;')->row()->count;
            // var_dump($this->db->last_query());
            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }
            $x = $query->result_array();
            $site_ary = array();
            foreach ($x as $key => $value) {

                $temp['user_name'] = $value['user_name'];
                $temp['firstname'] = $value['firstname'];
                $temp['id'] = $value['id'];
                $temp['user'] = $value['user'];
                $temp['participant_id'] = $value['participant_id'];
                $temp['action'] = $value['action'];
                $temp['Description'] = $value['Description'];
                $temp['title'] = $value['title'];
                $temp['invoice_number'] = $value['invoice_number'];
                $temp['invoice_amount'] = $value['invoice_amount'];
                $temp['company_name'] = $value['company_name'];
                $temp['due_date'] = $value['due_date'];
                $temp['date'] = $value['created'] != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($value['created'])) : '';
                $temp['time'] = $value['created'] != '0000-00-00 00:00:00' ? date('h:i', strtotime($value['created'])) : '';
                $site_ary[] = $temp;
            }
            $return = array('data' => $site_ary, 'count' => $dt_filtered_total, 'status' => true, 'total' => $all_count);
            return $return;
        }
    }

    public function participant_details($data) {
        $tbl = TBL_PREFIX . 'participant';
        $tbl2 = TBL_PREFIX . 'participant_email';
        // $this->db->or_where("(MATCH (firstname) AGAINST ('$post_data *'))", NULL, FALSE);
        // $this->db->or_where("(MATCH (lastname) AGAINST ('$post_data *'))", NULL, FALSE);,'firstname' => $val->firstname
        $this->db->select($tbl . ".firstname," . $tbl . ".lastname," . $tbl . ".id as ocs_id," . $tbl2 . ".email");
        $this->db->join($tbl2, $tbl2 . '.participantId = ' . $tbl . '.id', 'left');
        $this->db->where($tbl . '.archive =', 0);
        $this->db->where($tbl . '.status=', 1);
        $this->db->where($tbl2 . '.primary_email=', 1);
        $this->db->where($tbl . '.id=', $data->participantId);
        $query = $this->db->get(TBL_PREFIX . 'participant');
        $result = $query->row_array();
        //$participant_rows['firstname'] =$result['firstname'];
        //var_dump($result);
        //var_dump($result); exit;
        //     $participant_rows = array();
        //     if (!empty($query->result())) {
        //         foreach ($query->result() as $val) {
        // var_dump($val); exit;
        //             $participant_rows[] = array('label' => $val->ocs_id,'value' => $val->firstname,'email'=>$val->email,'lastname' => $val->lastname,'oc_id'=>$val->id);
        //          }
        //     }
        return $result;
    }

    public function invoice_already_exsits($invoicedetails, $check = null) {
        $response = array();
        $tbl = 'plan_management';
        $id = $invoicedetails->id;
        if ($check == 'validation')
            $invoice_no = $invoicedetails->invoice_number;
        else
            $invoice_no = $invoicedetails->invoice_no;
        $colums = (TBL_PREFIX . $tbl . ".id");
        $where = array('invoice_number' => $invoice_no);
        $this->db->select($colums);
        $this->db->where($where);
        $this->db->where('id!=' . $id);
        $result = $this->db->get(TBL_PREFIX . $tbl);
        $result = $result->result();
        if ($check == 'validation') {
            return $result;
        }
        if (!empty($result)) {
            $response = array('status' => $result);
        } else {
            $response = array('status' => false);
        }
        return $response;
    }

    public function check_biller_refernce_no($invoicedetails, $adminId) {
        $response = array();
        $tbl = 'plan_management_biller';
        $biller_code = $invoicedetails->biller_code;
        $colums = (array(TBL_PREFIX . $tbl . ".id", TBL_PREFIX . $tbl . ".biller_short_name"));
        $this->db->select($colums);
        $this->db->where('biller_code', $biller_code);
        // $this->db->like('biller_reference_number',$biller_reference_number);
        $result = $this->db->get(TBL_PREFIX . $tbl);
        $result = $result->row_array();
        // var_dump($result);
        if (!empty($result) && $result != null) {
            $response = array('status' => true, 'data' => $result['biller_short_name']);
        } else {
            $response = $this->getBillerCodeFromUrl($biller_code, $adminId);
        }
        return $response;
    }

    function getBillerCodeFromUrl($biller_code, $adminId) {
        $biller_code_array = array();
        $response = array();
        //var_dump($biller_code);
        //  $lines = file("https://www.bpay.com.au/Business/Large-Business/Find-Biller-codes-or-Financial-institutions.aspx?find=".$biller_code);
        if (isset($biller_code) && $biller_code != 0) {
            $lines = get_content_from_url("https://www.bpay.com.au/BillerLookupResults?query=" . $biller_code);
            //$lines1 = str_word_count($lines,1);
            $lines = preg_split('/<[^>]*[^\/]>/i', $lines, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            // var_dump($lines);
            $bcode = trim(strip_tags($lines['152']));
            $valid_code = ($biller_code == $bcode);
            if (isset($bcode) && !empty($bcode) && $valid_code) {
                $biller_code_array = array(
                    'biller_code' => $bcode,
                    'biller_short_name' => $lines['156'],
                    'biller_long_name' => $lines['160'],
                    'biller_reference_number' => $lines['164'],
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                );
                $res = $this->Basic_model->insert_records('plan_management_biller', $biller_code_array);
            }
            if (!empty($res)) {
                $response = array('status' => true, 'data' => $biller_code_array['biller_short_name']);
            } else {
                $response = array('status' => false);
            }
        }
        return $response;
    }

    public function update_invoice_status($invoicedetails) {
        $response = array();
        $tbl = 'plan_management';
        $id = $invoicedetails->id;
        $invoice_no = $invoicedetails->invoice_no;
        $colums = (TBL_PREFIX . $tbl . ".id");
        $where = array('invoice_number' => $invoice_no);
        $this->db->select($colums);
        $this->db->where($where);
        $result = $this->db->get(TBL_PREFIX . $tbl);
        $result = $result->row_array();
        if (!empty($result)) {
            $data = array('original_id' => $result['id'], 'status' => 2);
            $where = array('id' => $id);
            $res = $this->basic_model->update_records($tbl, $data, $where);
            if (!empty($res)) {
                $response = array('status' => true);
            } else {
                $response = array('status' => false);
            }
        } else {
            $response = array('status' => false);
        }
        return $response;
    }

    public function set_up_company($reqData, $adminId) {
        $response = array();
        $tbl = "plan_management_vendor";
        $companyId = 1;
        $insData = ['Name' => $reqData->company_name, 'ContactStatus' => \XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ACTIVE,];
        $indoviceDetails = $this->xerocontact->create_contact($companyId, $insData);
        $contactId = $indoviceDetails['data'][0]['ContactID'];
        $data = array('biller_code' => $reqData->biller_code, 'billpay_code' => $reqData->billpay_code, 'bsb_number' => $reqData->bsb_number, 'bank_account_no' => $reqData->bank_account_no, 'company_name' => $reqData->company_name, 'contact_id' => $contactId, 'bank_account_name' => $reqData->bank_account_name);
        $res = $this->Basic_model->insert_records($tbl, $data);
        $desc = "New Biller $reqData->company_name Added.";
        $this->logs_lib->setUserId($adminId);
        $this->logs_lib->setTitle('Biller added');
        $this->logs_lib->setDescription($desc);
        $this->logs_lib->setAction(6);
        $this->logs_lib->setPlanId('');
        $this->logs_lib->setParticipantId('');
        $this->logs_lib->createLog();
        return $res;
    }

    public function create_draft($invoicedetails) {
        $data['Type'] = 'ACCREC';
        $data['ContactID'] = "58697449-85ef-46ae-83fc-6a9446f037fb";
        $data['LineItems'][] = array("Description" => "Consulting services as agreed (20% off standard rate)", "Quantity" => "10", "UnitAmount" => "100.00", "AccountCode" => "200", "DiscountRate" => "20");
        $data['Date'] = date('Y-m-d', strtotime($invoicedetails->invoice_date));
        $data['DueDate'] = date('Y-m-d', strtotime($invoicedetails->due_date));
        $data['InvoiceNumber'] = $invoicedetails->invoice_number;
        $data['Total'] = $invoicedetails->invoice_amount;
        // $companyId = 1;
        // $page = !empty($page) && $page>0 ? $page:1;
        // $order_by = ['field_name'=>'Name','direction'=>'ASC'];
        // $contactDetails = $this->xerocontact->get_contact_list($companyId,['page'=>$page,'modifiedAfter'=>new \DateTime(date('2019-07-17')),'order_by'=>$order_by]);
        //
            $result = $this->xeroinvoice->create_inovice("", $data);
        // var_dump($invoicedetails);
        $res = $this->Basic_model->update_records('plan_management', array('xero_invoice_id' => $result['data'][0]['InvoiceID']), array('id' => $invoicedetails->id));
        // var_dump($res['data'][0]['InvoiceID']);exit;
        return $res;
    }

    public function get_sync_logs() {
        $limit = 10;
        $tbl = TBL_PREFIX . 'plan_management_sync_logs';
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result_array();
        $dataResult = array();
        if (!empty($result)) {
            $row = array();
            foreach ($result as $val) {
                if (date('Y-m-d', strtotime($val['date_of_upload'])) == date('Y-m-d')) {
                    $dataResult['current_batch'][] = $val;
                } else {
                    $dataResult['previous_batches'][] = $val;
                }
                $dataResult['synclogs'][] = $val;
            }
        }
        $return = array('status' => true, 'data' => $dataResult);
        return $return;
    }

    public function get_all_sync_logs($reqData) {

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $limit = $reqData->pageSize;
            $page = $reqData->page;
            $sorted = $reqData->sorted;
            $filter = $reqData->filtered;

            $orderBy = array();
            $direction = '';

            $tbl_1 = TBL_PREFIX . 'plan_management_sync_logs';
            $src_columns = array($tbl_1 . '.id', $tbl_1 . '.status',
                $tbl_1 . '.date_of_upload',
                $tbl_1 . '.date_of_complete', $tbl_1 . '.no_of_records_to_followup', $tbl_1 . '.no_of_records_to_xero', $tbl_1 . '.total_record_in_csv');
            $dt_query = $this->db->select($src_columns);
            $this->db->from($tbl_1);

            if (!empty($sorted)) {
                if (!empty($sorted[0]->id)) {
                    if ($sorted[0]->id) {
                        $orderBy = $sorted[0]->id;
                    }
                    $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
                }
            }
            if (!empty($filter)) {
                if (!empty($filter->filterVal)) {
                    $this->db->group_start();

                    for ($i = 0; $i < count($src_columns); $i++) {
                        $column_search = $src_columns[$i];
                        if (strstr($column_search, "as") !== false) {
                            $serch_column = explode(" as ", $column_search);
                            $this->db->or_like($serch_column[0], $filter->filterVal);
                        } else {
                            $this->db->or_like($column_search, $filter->filterVal);
                        }
                    }
                    $this->db->group_end();
                }

                if (!empty($filter->categoryVal)) {
                    $this->db->where('tbl_plan_management_sync_logs.status', $filter->categoryVal);
                }

                if (!empty($filter->fromDate) && !empty($filter->toDate)) {
                    $this->db->where('DATE(tbl_plan_management_sync_logs.date_of_upload)>="' . $filter->fromDate . '"');
                    $this->db->where('DATE(tbl_plan_management_sync_logs.date_of_upload)<="' . $filter->toDate . '"');
                }

                if (!empty($filter->fromDate)) {
                    $this->db->where('DATE(tbl_plan_management_sync_logs.date_of_upload)>="' . $filter->fromDate . '"');
                }
                if (!empty($filter->toDate)) {
                    $this->db->where('DATE(tbl_plan_management_sync_logs.date_of_upload)<="' . $filter->toDate . '"');
                }
            }
            $this->db->order_by($orderBy, $direction);
            $this->db->limit($limit, ($page * $limit));
            $query = $this->db->get();
            $dt_filtered_total = $this->db->query('SELECT count(*) as count from tbl_plan_management_sync_logs;')->row()->count;

            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }

            $dataResult = array();
            if (!empty($query->result())) {
                $row = array();
                foreach ($query->result_array() as $val) {
                    $dataResult['synclogs'][] = $val;
                }
            }
            $return = array('count' => $dt_filtered_total, 'status' => true, 'data' => $dataResult);
            return $return;
        }
    }

    public function processed_invoice($invoiceNumber, $status, $syn_id, $no_of_records) {
        $syncData = array();
        switch ($status) {
            case '3': // FOLLOWUP
                $syncData['no_of_records_to_followup'] = $no_of_records;
                $syncData['status'] = 1; // 1- Successful , 2-Failed, 3-Processing

                break;
            case '1': // PAID or APPROVED
                $syncData['no_of_records_to_xero'] = $no_of_records;
                $syncData['status'] = 1; // 1- Successful , 2-Failed, 3-Processing

                break;
            case '2': // RESTRAT
                $syncData['no_of_records_to_followup'] = $no_of_records;
                $syncData['no_of_records_to_xero'] = 0;
                $syncData['status'] = 2; // 1- Successful , 2-Failed, 3-Processing
                $status = '3';
                break;
        }
        $syncData['date_of_complete'] = date('Y-m-d');
        $this->db->where('id', $syn_id);
        $this->db->update('tbl_plan_management_sync_logs', $syncData);
        $response = array('status' => true);
        $tbl = 'plan_management';
        $where = array('invoice_number' => $invoiceNumber);
        $data = array('status' => $status);
        $res = $this->basic_model->update_records($tbl, $data, $where);
        $result = $this->basic_model->get_row('plan_management', array('id'), $where);
        //  var_dump($result->id);
        $dispute_note_data = array();
        $dispute_note_data['invoice_id'] = $result->id;
        $dispute_note_data['reason'] = 'NDIS rejected';
        $dispute_note_data['raised'] = date('d/m/Y');
        $dispute_note_data['notes'] = 'NDIS rejected';
        $dispute_note_data['status'] = 1;
        $res1 = $this->basic_model->insert_records("plan_management_dispute_note", $dispute_note_data);
        return true;
    }

    public function checkInvoiceCsvExistinDb($inv_num) {
        $tbl = 'plan_management';
        $this->db->where('invoice_number', $inv_num);
        $result = $this->db->get(TBL_PREFIX . $tbl);
        //echo $this->db->last_query();
        $result = $result->result();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

}
