<table style="max-width:40%; min-width:280px; margin: 0px auto; border: 1px solid #cdcdcd; border-collapse:collapse; font-family:sans-serif;" cellpadding="0" cellspaceing="0">
    <tr>
        <td style="padding:15px 30px 0px; font-size:20px; font-weight:600; color:#443d3d;">Password reset information</td>
    </tr>
    <tr>
        <td style="padding:0px 30px 15px"><hr style="margin: 5px 0px; background: #00A63F; height: 2px; border: 0px; width: 127px;"></td>
    </tr>
    <tr>
        <td style="font-size:15px; font-weight:normal; padding:15px 30px; color:#443d3d;">Your password reset successfully.</td>
    </tr>

    <tr>
        <td style="font-size:15px; font-weight:normal; padding:15px 30px 10px; color:#443d3d;">
            If you didn't request a password reset or you feel you\'ve received this message in error, please call our 24/7 support team right away at 000 000 0000. If you take no action, don't worry â€” nobody will be able to change your password without access to this email. 
        </td>
    </tr>

    <tr>
        <td width="100%" style="margin-bottom: 30px; float: left; width: 100%;">
        </td>
    </tr>
</table>

<table style="width: 41%; margin: 0px auto; padding: 20px;">
    <tr style="margin-bottom: 20px; font-size: 12px; float: left;  width: 100%;">
        <td style="line-height: 20px; width: 100%; text-align: center; display: block; color: #909090;">&#169;2018-All Righ  Reserved <b>Healthcare Manager</b></td>
    </tr>
</table>
<style>
    a:hover, a:active{
        color:#fff !important;
        text-decoration:none !important;
    }
</style>
