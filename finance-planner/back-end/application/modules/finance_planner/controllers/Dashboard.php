<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Dashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Dashboard_model');
    }

    public function dashboard_data(){
      $reqData = request_handler();
      $reqData = $reqData->data;
      if (!empty($reqData)) {
      $types = array('Pending','Followup','Duplicate');
      $all = array();
      $where = array('load_status'=>1, 'read_status'=>1);
        foreach($types as $type){
          switch ($type) {
            case 'Pending':
              $where['status'] = '4';
              $all[$type]  = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            case 'Followup':
              $where['status'] = '3';
              $all[$type]  = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            case 'Duplicate':
              $where['status'] = '5';
              $all[$type] = $this->Dashboard_model->get_counts_bystatus($where);
              break;
            default:
              break;
          }
        }
        $data = $this->Dashboard_model->get_processed_invoice($reqData);
        $tilldata = $this->Dashboard_model->get_processed_invoice_till($reqData);
        $graph_data  = $this->Dashboard_model->get_graph_monthly($reqData);
        $summary = $this->Dashboard_model->get_members_summary($reqData);
        $response = array('status'=>true,'count'=>$all,'graph'=>$graph_data,'invoice'=>$data, 'till' => $tilldata, 'summary'=>$summary['data']);
    }
    else{
      $response = array('status'=>false,'data'=>[]);
    }
    echo json_encode($response);
  }


  public function get_graph_data(){
    $reqData = request_handler();
    $reqData = $reqData->data;
    if (!empty($reqData)) {
      $graph_data  = $this->Dashboard_model->get_graph_monthly($reqData);
      $response = array('status'=>true, 'graph'=>$graph_data);
   }
    else{
      $response = array('status'=>false,'data'=>[]);
    }
    echo json_encode($response);
  }

  }
