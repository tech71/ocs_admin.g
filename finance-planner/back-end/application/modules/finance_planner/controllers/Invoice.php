<?php

require_once FCPATH . 'pdf_to_html_con/vendor/autoload.php';

//use classRoles as adminRoles;
use Gufy\PdfToHtml\Config;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Invoice extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Invoice_model');
        $this->load->model('Basic_model');
        $this->load->helper(array('common'));
        if (!$this->session->userdata('username')) {
            // redirect('');
        }
    }

    function list_invoice() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->invoice_list($reqData);
            echo json_encode($response);
        }
    }

    public function list_audit_logs() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = json_decode($reqData);
            $response = $this->Invoice_model->list_audit_logs($reqData);
            echo json_encode($response);
        }
    }

    public function get_participant_name() {
        $post_data = $this->input->get('query');
        $rows = $this->Invoice_model->get_participant_name($post_data);
        echo json_encode($rows);
    }

    public function get_company_name() {
        $reqData = request_handler();

        if (!empty($reqData->data->search)) {
            $rows = $this->Invoice_model->get_company_name($reqData->data->search);
            echo json_encode($rows);
        }
    }

    public function get_participant_id() {
        $rows = $this->Invoice_model->get_participant_id();
        echo json_encode($rows);
    }

    public function get_participant_details() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $result = $this->Invoice_model->participant_details($reqData);
            if (!empty($result)) {
                $response = array('status' => true, 'data' => $result);
            } else {
                $response = array('status' => false, 'data' => []);
            }
            echo json_encode($response);
        }
    }

    function participant_invoices() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->participant_invoices($reqData);
            echo json_encode($response);
        }
    }

    // function
    function invoice_details() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $invoice_id = $reqData->id;
            // get about Invoice details
            $result = $this->Invoice_model->invoice_details($invoice_id, $adminId);
            if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    function status_change() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;

        if (!empty($reqData)) {
            $status = $reqData->status;
            $id = $reqData->id;
            $invoicedetails = $reqData->invoicedetails;
            $checkStatus = array('Archieve', 'Duplicate');
            if (!in_array($status, $checkStatus))
                $this->validation($invoicedetails);

            $result = $this->Invoice_model->status_change($status, $id, $invoicedetails, $adminId);
            if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found', 'data' => $invoicedetails));
            }
        }
    }

    function check_number_of_notes_count_not_more_than_10($invoiceId) {
        if (!empty($invoiceId)) {
            $where = array('invoice_id' => $invoiceId);
            $count = $this->basic_model->get_record_where('plan_management_dispute_note', 'count(*) as total_dispute', $where);

            if ($count[0]->total_dispute > 10) {
                $this->form_validation->set_message('check_number_of_notes_count_not_more_than_10', 'Sorry, More than 10 Notes Cannot be created');
                return false;
            } else {
                return true;
            }
        } else {
            $this->form_validation->set_message('check_number_of_notes_count_not_more_than_10', 'Invoice id is required');
            return false;
        }
    }

    function get_dispute_contact_option() {
        $reqData = request_handler();
        $res = $this->basic_model->get_record_where('plan_dispute_contact_method', ['id as value', 'name as label'], ['archive' => 0]);
        echo json_encode(['status' => true, 'data' => $res]);
    }

    function create_dispute_note() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'notes', 'label' => 'Dispute Notes', 'rules' => 'required|min_length[8]|max_length[1000]'),
                array('field' => 'contact_method', 'label' => 'Contact Method', 'rules' => 'required'),
                array('field' => 'contact_name', 'label' => 'Contact Name', 'rules' => 'required|max_length[30]'),
                array('field' => 'raised', 'label' => 'Dispute Raised', 'rules' => 'required'),
                array('field' => 'reason', 'label' => 'Dispute Reason', 'rules' => 'required|max_length[30]'),
                array('field' => 'invoice_id', 'label' => 'custom', 'rules' => 'callback_check_number_of_notes_count_not_more_than_10'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if (!$this->form_validation->run() == FALSE) {

                $this->Invoice_model->add_dispute_note($reqData);

                if (isset($reqData->notes_id) && !empty($reqData->notes_id)) {
                    $msg = "Dispute Note Edited Successful";
                    $action = 16;
                } else {
                    $msg = "Dispute Note Added Successful";
                    $action = 14;
                }

                $data = $this->Basic_model->get_row('plan_management', array('participant_id'), array('id' => $reqData->invoice_id));

                $this->logs_lib->setUserId($adminId);
                $this->logs_lib->setTitle($msg);
                $this->logs_lib->setDescription($msg);
                $this->logs_lib->setAction($action);
                $this->logs_lib->setPlanId("");
                $this->logs_lib->setParticipantId($data->participant_id);
                $this->logs_lib->createLog();

                echo json_encode(array('status' => true, 'data' => $msg));
            } else {
                $errors = $this->form_validation->error_array();
                echo json_encode(array('status' => false, 'data' => '', 'error' => $errors));
            }
        }
    }

    function list_dispute_note() {
        $reqData = request_handler();
        $reqData = $reqData->data;

        if (!empty($reqData)) {
            $result = $this->Invoice_model->get_list_dispute_note($reqData);
            if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    function manual_upload() {
        $reqData = request_handlerFile();
        $adminId = $reqData->adminId;
        // var_dump($adminId);exit;
        if (!empty($reqData)) {
            $result = $this->upload_docs($_FILES, $reqData->participantId, $reqData->email, $adminId);
            if (!empty($result)) {
                echo json_encode(array('status' => true, 'data' => $result));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function upload_docs($file, $participant_id, $email, $adminId) {
        $config['upload_path'] = FCPATH . PLAN_MANAGEMENT_ALLFILES;
        $config['input_name'] = 'myFile';
        $config['directory_name'] = $participant_id;
        $config['allowed_types'] = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf|html|htm';
        $url = PLAN_MANAGEMENT_ALLFILES . $participant_id;
        $path = FCPATH . PLAN_MANAGEMENT_ALLFILES . $participant_id;
        $this->make_path($path);
        $is_upload = do_upload($config);
        if (isset($is_upload['error'])) {
            echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
            $action = 9;
            $title = "Failure while uploading the invoice manually";
            $description = $is_upload['error'];
            exit();
        } else {
            $rows = '';
            $ext = pathinfo($is_upload['upload_data']['file_name'], PATHINFO_EXTENSION);
            $insert_ary = array('pdf_url' => $url . '/' . $is_upload['upload_data']['file_name'], 'participant_id' => $participant_id, 'created' => DATE_TIME, 'read_status' => 0, 'status' => 4, 'participant_email' => $email);
            if ($ext != 'pdf') {
                $insert_ary['html_url'] = $url . '/' . $is_upload['upload_data']['file_name'];
            }
            $rows = $this->Basic_model->insert_records('plan_management', $insert_ary);
            $insert_id = $this->db->insert_id();
            if (!empty($insert_id) && $ext == 'pdf')
                $this->convertHtml($insert_id, $is_upload['upload_data']['file_name']);
            // $convert = $this->convertHtml($rows);
            if ($rows) {
                return !empty($rows) ? $rows : false;
            }
            $action = 10;
            $title = "Invoice uploaded Manually successfully";
            $description = "Invoice uploaded Manually successfully";
        }
        $this->logs_lib->setUserId($adminId);
        $this->logs_lib->setTitle($title);
        $this->logs_lib->setDescription($description);
        $this->logs_lib->setAction($action = 10);
        $this->logs_lib->setPlanId("");
        $this->logs_lib->setParticipantId($participant_id);
        $this->logs_lib->createLog();
    }

    public function make_path($path) {
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        if (is_dir($dir)) {
            return true;
        } else {
            if ($this->make_path($dir)) {
                if (mkdir($dir)) {
                    chmod($dir, 0777);
                    return true;
                }
            }
        }
        return false;
    }

    function convertHtml($id, $file) {
        // echo "here";
        if (!empty($id)) {
            $getFile = $this->Basic_model->get_row($table_name = 'plan_management', $columns = array('pdf_url', 'participant_id'), $id_array = array('id' => $id));
            include FCPATH . 'pdf_to_html_con/vendor/autoload.php';
            $url = PLAN_MANAGEMENT_HTMLFILES . $getFile->participant_id;
            $path = FCPATH . $getFile->pdf_url;
            // Config::set('pdftohtml.bin', FCPATH.'pdf_to_html_con/poppler-0.51/bin/pdftohtml.exe');
            // Config::set('pdfinfo.bin', FCPATH.'pdf_to_html_con/poppler-0.51/bin/pdfinfo.exe');
            Config::set('pdftohtml.bin', '/usr/bin/pdftohtml');
            Config::set('pdfinfo.bin', '/usr/bin/pdfinfo');
            Config::set('pdftohtml.output', FCPATH . PLAN_MANAGEMENT_HTMLFILES . $getFile->participant_id);
            $pdf = new Gufy\PdfToHtml\Pdf($path);
            $total_pages = $pdf->getPages();
            $html = $pdf->html();
            $html_files = array();
            $filename = str_replace('.pdf', '', $file);
            for ($i = 1; $i <= $total_pages; $i++) {
                array_push($html_files, $url . '/' . $filename . '-' . $i . '.html');
            }
            $html_files = implode(",", $html_files);
            $data = array('html_url' => $html_files);
            $table = "plan_management";
            $result = $this->basic_model->update_records($table, $data, $id_array = array('id' => $id));
            return !empty($result) ? true : false;
        }
    }

    function duplicate_invoice() {
        $table = "plan_management";
        $data = array();
        $read_data = $this->basic_model->get_record_where($table, "company_name,invoice_number,due_date,invoice_amount,id", array('load_status' => 1));
        $unread_data = $this->basic_model->get_record_where($table, "company_name,invoice_number,due_date,invoice_amount,id", array('load_status' => 0));
        foreach ($unread_data as $unread) {
            if (!empty($read_data)) {
                foreach ($read_data as $read) {
                    if (($read->company_name == $unread->company_name) && ($read->due_date == $unread->due_date) && ($read->invoice_amount == $unread->invoice_amount) && ($read->invoice_number == $unread->invoice_number)) {
                        $data['status'] = '2';
                        $data['original_id'] = $read->id;
                        $this->db->where('id', $unread->id);
                        $this->db->update('tbl_plan_management', $data);
                    }
                }
            }
            $this->basic_model->update_records('plan_management', array('load_status' => 1), array('id' => $unread->id));
        }
    }

    function xero_validation() {
        $invoiceDetails = $this->xeroinvoice->get_invoice_list();
        if ($invoiceDetails) {
            $paid = '';
            if (!empty($invoiceDetails['data'])) {
                for ($i = 0; $i < count($invoiceDetails['data']); $i++) {
                    $paid_status = $invoiceDetails['data'][$i]['Status'];
                    if ($paid_status == 'PAID')
                        $paid = 1;
                    else
                        $paid = 0;
                    $update = array('payment_type' => $paid);
                    $table = "plan_management";
                    if (isset($invoiceDetails['data'][$i]['InvoiceNumber']))
                        $result = $this->basic_model->update_records($table, $update, $id_array = array('invoice_number' => $invoiceDetails['data'][$i]['InvoiceNumber']));
                }
            }
        }
    }

    function fetch_invoice() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;
        $tbl1 = TBL_PREFIX . 'participant';
        $tbl2 = TBL_PREFIX . 'participant_email';
        // $participant_id = 1;
        $sWhere = array($tbl1 . '.status' => 1);
        $this->db->select(array($tbl1 . '.firstname', $tbl1 . '.id', $tbl2 . '.email'));
        $this->db->join($tbl2, $tbl2 . '.participantId = ' . $tbl1 . '.id', 'left');
        $this->db->from($tbl1);
        $this->db->limit('1');
        $this->db->where($sWhere);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $participnat_data = $query->result();

        foreach ($participnat_data as $val) {
            $participant_id = $val->id;
            // var_dump($participnat_data);exit;
            $participant = $val->firstname;
            require_once APPPATH . 'Classes/finance_planner/attachment.php';
            $objAttachment = new classAttachment\Attachment();
            $max_emails = 3;
            $username = 'developertest60@gmail.com'; //$val->email
            $password = 'umealone';
            $attachments = $objAttachment->getAttachments($username, $password, $max_emails, $participant_id);

            foreach ($attachments as $values) {
                if (!empty($values['attachment'])) {
                    foreach ($values['attachment'] as $key => $value) {
                        $fileName = $value;
                        $ext = pathinfo($value, PATHINFO_EXTENSION);
                        $allFilePath = FCPATH . PLAN_MANAGEMENT_ALLFILES . $participant_id . '/' . $value;
                        $beforereadfile = PLAN_MANAGEMENT_ALLFILES . $participant_id . '/' . $value;
                        $afterreadfile = PLAN_MANAGEMENT_HTMLFILES . $participant_id . '/';
                        $savedConvertedFullPath = FCPATH . PLAN_MANAGEMENT_HTMLFILES . $participant_id . '/';
                        if (!is_dir($savedConvertedFullPath)) {
                            mkdir($savedConvertedFullPath, 0777, TRUE);
                        }


                        if ($ext == 'pdf') {
                            $html_files = $this->convert_to_pdf($fileName, $allFilePath, $afterreadfile, $savedConvertedFullPath);
                        } else {
                            $html_files = PLAN_MANAGEMENT_ALLFILES . $participant_id . '/' . $fileName;
                        }
                        $data = array('participant_email' => $val->email, 'participant_id' => $participant_id, 'pdf_url' => $beforereadfile, 'html_url' => $html_files);
                        $table = "plan_management";
                        $this->basic_model->insert_records($table, $data);
                    }
                }
            }
            $this->logs_lib->setUserId($adminId);
            $this->logs_lib->setTitle("Invoice sent by participant");
            $this->logs_lib->setDescription("Invoice sent by participant $participant");
            $this->logs_lib->setAction(11);
            $this->logs_lib->setPlanId("");
            $this->logs_lib->setParticipantId($participant_id);
            $this->logs_lib->createLog();
        }

        $tbl3 = 'email_last_fetch_time';
        $this->Basic_model->insert_records($tbl3, array('last_fetch' => date(date('Y-m-d H:i:s'))));
        echo json_encode(array('status' => true, 'data' => 'Successful'));
    }

    public function convert_to_pdf($givenfileName, $allFilesPath, $readfile, $savedConvertedPath) {
        # for windows
//        Config::set('pdftohtml.bin', 'pdf_to_html_con/poppler-0.51/bin/pdftohtml.exe');
//        Config::set('pdfinfo.bin', 'pdf_to_html_con/poppler-0.51/bin/pdfinfo.exe');
        #for linux
        Config::set('pdftohtml.bin', '/usr/bin/pdftohtml');
        Config::set('pdfinfo.bin', '/usr/bin/pdfinfo');

        Config::set('pdftohtml.output', $savedConvertedPath);

        $pdf = new Gufy\PdfToHtml\Pdf($allFilesPath);

        if (!empty($pdf->getInfo())) {
            $total_pages = $pdf->getPages();
            $html = $pdf->html();

            $filename = str_replace('.pdf', '.html', $givenfileName);
            $filename = $readfile . $filename;
            return $filename;
        }
        return false;
    }

    function invoice_already_exsits() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->invoice_already_exsits($reqData);
            echo json_encode($response);
        }
    }

    function update_invoice_status() {
        $reqData = request_handler();
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->update_invoice_status($reqData);
            echo json_encode($response);
        }
    }

    public function check_biller_refernce_no() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->check_biller_refernce_no($reqData, $adminId);
            echo json_encode($response);
        }
    }

    function python_script_run() {
        $command = "/usr/bin/python " . PLAN_PYTHON_SCRIPT . "data_updation_python.py 2>&1";

        $output = shell_exec($command);

        $this->duplicate_invoice();
        $this->xero_validation();
        echo json_encode($output);
    }

    public function csv_batches_export() {
        $request = request_handler();
        $adminId = $request->adminId;
        $csv_data = $request->data->data->approvedSelectedList;
        $this->load->library("excel");
        $object = new PHPExcel();
        if (!empty($csv_data)) {
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Date of Upload');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Date of Complete');
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Record Moved to Follow up');
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Records Moved to Xero');
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Total in Csv');
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Status');
            $var_row = 2;
            foreach ($csv_data as $one_row) {
                $object->getActiveSheet()->SetCellValue('A' . $var_row, $one_row->id);
                $object->getActiveSheet()->SetCellValue('B' . $var_row, $one_row->date_of_upload);
                $object->getActiveSheet()->SetCellValue('C' . $var_row, $one_row->date_of_complete);
                $object->getActiveSheet()->SetCellValue('D' . $var_row, $one_row->no_of_records_to_followup);
                $object->getActiveSheet()->SetCellValue('E' . $var_row, $one_row->no_of_records_to_xero);
                $object->getActiveSheet()->SetCellValue('F' . $var_row, $one_row->total_record_in_csv);
                $object->getActiveSheet()->SetCellValue('G' . $var_row, $one_row->status);
                $var_row++;
            }
            $object->setActiveSheetIndex()->getStyle('A1:D1')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'C0C0C0:'))));
            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');
            $filename = time() . '_previous_batch_export' . '.csv';
            $filePath = ARCHIEVE_DIR . '/' . $filename;
            $object_writer->save($filePath);
            echo json_encode(array('status' => true, 'csv_url' => $filePath));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'No record to export'));
            exit();
        }
    }

    public function csv_sync_export() {
        $request = request_handler();
        $adminId = $request->adminId;
        $csv_data = $request->data->data->approvedSelectedList;
        $this->load->library("excel");
        $object = new PHPExcel();
        if (!empty($csv_data)) {
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Batch ID');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Date of Upload');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Date of Complete');
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Records Moved to Follow up');
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Records Moved to Xero');
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Total in CSV');
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Status');
            $var_row = 2;
            foreach ($csv_data as $one_row) {
                $object->getActiveSheet()->SetCellValue('A' . $var_row, $one_row->id);
                $object->getActiveSheet()->SetCellValue('B' . $var_row, date('Y-m-d', strtotime($one_row->date_of_upload)));
                $object->getActiveSheet()->SetCellValue('C' . $var_row, date('Y-m-d', strtotime($one_row->date_of_complete)));
                $object->getActiveSheet()->SetCellValue('D' . $var_row, $one_row->no_of_records_to_followup);
                $object->getActiveSheet()->SetCellValue('E' . $var_row, $one_row->no_of_records_to_xero);
                $object->getActiveSheet()->SetCellValue('F' . $var_row, $one_row->total_record_in_csv);
                $object->getActiveSheet()->SetCellValue('G' . $var_row, $one_row->status);
                $var_row++;
            }
            $object->setActiveSheetIndex()->getStyle('A1:D1')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'C0C0C0:'))));
            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');
            $filename = time() . '_sync_log_export' . '.csv';
            $filePath = ARCHIEVE_DIR . '/' . $filename;
            $object_writer->save($filePath);
            echo json_encode(array('status' => true, 'csv_url' => $filePath));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'No record to export'));
            exit();
        }
    }

    public function csv_audit_export() {
        $request = request_handler();
        $adminId = $request->adminId;
        $csv_data = $request->data->data->approvedSelectedList;
        $this->load->library("excel");
        $object = new PHPExcel();
        if (!empty($csv_data)) {
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'ID');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Occurence');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Date');
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Time');
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'User');
            $var_row = 2;
            foreach ($csv_data as $one_row) {
                $object->getActiveSheet()->SetCellValue('A' . $var_row, $one_row->id);
                $object->getActiveSheet()->SetCellValue('B' . $var_row, $one_row->title);
                $object->getActiveSheet()->SetCellValue('C' . $var_row, $one_row->date);
                $object->getActiveSheet()->SetCellValue('D' . $var_row, $one_row->time);
                $object->getActiveSheet()->SetCellValue('E' . $var_row, $one_row->user);
                $var_row++;
            }
            $object->setActiveSheetIndex()->getStyle('A1:D1')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'C0C0C0:'))));
            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');
            $filename = time() . '_audit_log_export' . '.csv';
            $filePath = ARCHIEVE_DIR . '/' . $filename;
            $object_writer->save($filePath);
            echo json_encode(array('status' => true, 'csv_url' => $filePath));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'No record to export'));
            exit();
        }
    }

    public function csv_approved_export() {
        $request = request_handler();
        $adminId = $request->adminId;
        $csv_data = $request->data->data->approvedSelectedList;
        $this->load->library("excel");
        $object = new PHPExcel();
        if (!empty($csv_data)) {
            $object->setActiveSheetIndex(0);
            $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'HCM ID');
            $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'NDIS ID');
            $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Last Name');
            $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'First Name');
            $object->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Invoice Number');
            $object->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'Due Date');
            $object->getActiveSheet()->setCellValueByColumnAndRow(6, 1, 'Invoice Total');
            $object->getActiveSheet()->setCellValueByColumnAndRow(7, 1, 'Biller');
            $object->getActiveSheet()->setCellValueByColumnAndRow(8, 1, 'HCM Status');
            $var_row = 2;
            foreach ($csv_data as $one_row) {

                $object->getActiveSheet()->SetCellValue('A' . $var_row, $one_row->ocisid);
                $object->getActiveSheet()->SetCellValue('B' . $var_row, $one_row->ndisid);
                $object->getActiveSheet()->SetCellValue('C' . $var_row, ucfirst($one_row->lastname));
                $object->getActiveSheet()->SetCellValue('D' . $var_row, ucfirst($one_row->firstname));
                $object->getActiveSheet()->SetCellValue('E' . $var_row, $one_row->invoice);
                $object->getActiveSheet()->SetCellValue('F' . $var_row, $one_row->due_date);
                $object->getActiveSheet()->SetCellValue('G' . $var_row, $one_row->invoice_amount);
                $object->getActiveSheet()->SetCellValue('H' . $var_row, $one_row->biller);
                $object->getActiveSheet()->SetCellValue('I' . $var_row, $one_row->status);
                $var_row++;
            }
            $object->setActiveSheetIndex()->getStyle('A1:D1')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'C0C0C0:'))));
            $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');
            $filename = time() . '_approved_invoices_export' . '.csv';
            $filePath = ARCHIEVE_DIR . '/' . $filename;
            $object_writer->save($filePath);
            $this->logs_lib->setUserId($adminId);
            $this->logs_lib->setTitle('Invoices exported');
            $this->logs_lib->setDescription(count($csv_data) . " Invoices exported");
            $this->logs_lib->setAction(7);
            $this->logs_lib->setPlanId('');
            $this->logs_lib->setParticipantId('');
            $this->logs_lib->createLog();
            echo json_encode(array('status' => true, 'csv_url' => $filePath));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'No record to export'));
            exit();
        }
    }

    function callback_file_check() {
        $mime = get_mime_by_extension($_FILES['file']['name']);
        if (!empty($_FILES['myFile']['name']) && $mime == 'csv') {
            return true;
        } else {
            return false;
        }
    }

    public function importCsv() {
        $request = request_handlerFile();
        $adminId = $request->adminId;
        $import_data = (array) $request;
        $uploads_dir = IMPORT_CSV_DIR;
        $sourcePath = $_FILES['myFile']['tmp_name'];
        $newfile = rand(1, 100) . '.' . pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION); //any name sample.jpg
        $targetPath = $uploads_dir . $newfile;
        move_uploaded_file($sourcePath, $targetPath);
        $data = array();
        $file_path = $targetPath;
        $response = array();
        $import_data_array = array();
        if (($h = fopen("{$file_path}", "r")) !== FALSE) {
            $vals = null;
            fgetcsv($h);
            $i = 0;
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
                $import_data_array[] = $data;
                $i++;
            }
            fclose($h);
        }
        $this->logs_lib->setUserId($adminId);
        $this->logs_lib->setTitle('Invoices Imported');
        $this->logs_lib->setDescription(count($import_data_array) . " Invoices Imported at " . date('Y-m-d'));
        $this->logs_lib->setAction(4);
        $this->logs_lib->setPlanId('');
        $this->logs_lib->setParticipantId('');
        $this->logs_lib->createLog();
        $syncData = array('file_path' => $targetPath, 'total_record_in_csv' => count($import_data_array), 'date_of_upload' => date('Y-m-d'), 'status' => 3);
        $this->db->insert('tbl_plan_management_sync_logs', $syncData);
        $sync_id = $this->db->insert_id();
        $this->logs_lib->setUserId($adminId);
        $this->logs_lib->setTitle('A new entry in syc logs');
        $this->logs_lib->setDescription(" A new entry in syc logs at " . date('Y-m-d'));
        $this->logs_lib->setAction(13);
        $this->logs_lib->setPlanId('');
        $this->logs_lib->setParticipantId('');
        $this->logs_lib->createLog();
        $response = array('status' => true, 'data' => 'Successfully Imported Csv');
        // } else {
        //           $response  = array('status'=>false,'data'=>'Invalid file, please select only CSV file.');
        //
        //          }
        $this->csv_batch_process();
        echo json_encode($response);
        exit();
    }

    public function csv_batch_process() {
        $tbl = "tbl_plan_management_sync_logs";
        $result = $this->db->select('*')->order_by('id', "desc")->limit(1)->get($tbl)->row_array();
        $file_path = $result['file_path'];
        $sync_id = $result['id'];
        if (($h = fopen("{$file_path}", "r")) !== FALSE) {
            $vals = null;
            fgetcsv($h);
            $i = 0;
            $response = array();
            $import_data_array = array();
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
                $import_data_array[$i]['invoice_number'] = $data[3];
                $import_data_array[$i]['hcm_status'] = $data[6];
                $import_data_array[$i]['inovice_due_date'] = date('Y-m-d', strtotime($data[1]));
                $i++;
            }
            fclose($h);
        }
        $no_of_records_followup = 0;
        $no_of_records_to_xero = 0;
        $no_of_records = 0;
        try {
            $fillterRequest = ['page' => 1];
            $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
            // var_dump($inoviceDetails); exit;
            if ($inoviceDetails['status'] == true && $inoviceDetails['data']) {
                $invoiceStatus = array();
                foreach ($import_data_array as $dataarray) {
                    $checkInvoiceCsvExistinDb = $this->Invoice_model->checkInvoiceCsvExistinDb($dataarray['invoice_number']);
                    if ($checkInvoiceCsvExistinDb) {
                        $invoiceNumberExist = $this->multi_array_search(trim($dataarray['invoice_number']), $inoviceDetails['data']);
                        if ($invoiceNumberExist) {
                            $statusRequest = ['InvoiceID' => $invoiceNumberExist['InvoiceID']];
                            $invoiceStatus = $this->xeroinvoice->get_single_inovice($statusRequest);
                            if ($invoiceStatus['status'] == true) {
                                if ($invoiceStatus['data'] == 'PAID' && trim($dataarray['hcm_status']) == 'PAID') {
                                    $no_of_records_to_xero++;
                                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '1', $sync_id, $no_of_records_to_xero);
                                    $response = array('error' => false, 'status' => true);
                                } else {
                                    $no_of_records_followup++;
                                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '3', $sync_id, $no_of_records_followup);
                                    $response = array('error' => false, 'status' => true);
                                }
                            }
                        } else {
                            $no_of_records_followup++;
                            $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '3', $sync_id, $no_of_records_followup);
                            $response = array('error' => false, 'status' => true);
                        }
                    }
                }
            } else {
                // FAILED
                foreach ($import_data_array as $dataarray) {
                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '2', $sync_id, $no_of_records);
                }
                $response = array('error' => true, 'status' => false);
            }
        } catch (Exception $e) {
            //HNXQTZVMS0YK0QUBXY0ZI863IU1OOH - consumerKey
            foreach ($import_data_array as $dataarray) {
                $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '2', $sync_id, $no_of_records);
            }
            log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            // on error
        }
        echo json_encode($response);
        exit;
    }

    public function processCsv() {
        $request = request_handler();
        $tbl = "tbl_plan_management_sync_logs";
        if (!empty($request->data)) {
            $sync_id = $request->data->sync_id;
            $this->db->where('id', $sync_id);
        }
        $result = $this->db->select('*')->order_by('id', "desc")->limit(1)->get($tbl)->row_array();
        //echo $this->db->last_query(); exit;
        $file_path = $result['file_path'];
        $sync_id = $result['id'];
        if (($h = fopen("{$file_path}", "r")) !== FALSE) {
            $vals = null;
            fgetcsv($h);
            $i = 0;
            $response = array();
            $import_data_array = array();
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE) {
                $import_data_array[$i]['invoice_number'] = $data[3];
                $import_data_array[$i]['hcm_status'] = $data[6];
                $import_data_array[$i]['inovice_due_date'] = date('Y-m-d', strtotime($data[1]));
                $i++;
            }
            fclose($h);
        }
        $no_of_records_followup = 0;
        $no_of_records_to_xero = 0;
        $no_of_records = 0;
        try {
            $fillterRequest = ['page' => 1];
            $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
            // var_dump($inoviceDetails); exit;
            if ($inoviceDetails['status'] == true && $inoviceDetails['data']) {
                $invoiceStatus = array();
                foreach ($import_data_array as $dataarray) {
                    $checkInvoiceCsvExistinDb = $this->Invoice_model->checkInvoiceCsvExistinDb($dataarray['invoice_number']);
                    if ($checkInvoiceCsvExistinDb) {
                        $invoiceNumberExist = $this->multi_array_search(trim($dataarray['invoice_number']), $inoviceDetails['data']);
                        if ($invoiceNumberExist) {
                            $statusRequest = ['InvoiceID' => $invoiceNumberExist['InvoiceID']];
                            $invoiceStatus = $this->xeroinvoice->get_single_inovice($statusRequest);
                            if ($invoiceStatus['status'] == true) {
                                if ($invoiceStatus['data'] == 'PAID' && trim($dataarray['hcm_status']) == 'PAID') {
                                    $no_of_records_to_xero++;
                                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '1', $sync_id, $no_of_records_to_xero);
                                    $response = array('error' => false, 'status' => true);
                                } else {
                                    $no_of_records_followup++;
                                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '3', $sync_id, $no_of_records_followup);
                                    $response = array('error' => false, 'status' => true);
                                }
                            }
                        } else {
                            $no_of_records_followup++;
                            $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '3', $sync_id, $no_of_records_followup);
                            $response = array('error' => false, 'status' => true);
                        }
                    }
                }
            } else {
                // FAILED
                foreach ($import_data_array as $dataarray) {
                    $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '2', $sync_id, $no_of_records);
                }
                $response = array('error' => true, 'status' => false);
            }
        } catch (Exception $e) {
            //HNXQTZVMS0YK0QUBXY0ZI863IU1OOH - consumerKey
            foreach ($import_data_array as $dataarray) {
                $this->Invoice_model->processed_invoice($dataarray['invoice_number'], '2', $sync_id, $no_of_records);
            }
            log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
            // on error
        }
        echo json_encode($response);
        exit;
    }

    public function get_sync_logs() {
        $reqData = request_handler();
        if (!empty($reqData)) {
            $response = $this->Invoice_model->get_sync_logs();
            echo json_encode($response);
        }
    }

    public function get_all_sync_logs() {
        $reqData = request_handler();
        if (!empty($reqData)) {
            $response = $this->Invoice_model->get_all_sync_logs($reqData);
            echo json_encode($response);
        }
    }

    public function multi_array_search($search_for, $search_in) {
        foreach ($search_in as $element) {
            if (($element === $search_for)) {
                return true;
            } else if (is_array($element)) {
                $result = $this->multi_array_search($search_for, $element);
                if ($result == true) {
                    return $element;
                }
            }
        }
        return false;
    }

    public function set_up_company() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;
        if (!empty($reqData)) {
            $reqData = $reqData;
            $response = $this->Invoice_model->set_up_company($reqData, $adminId);
            if (!empty($response)) {
                echo json_encode(array('status' => true, 'data' => $reqData->company_name));
            } else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function validation($data) {
        $valid = true;
        $payment_valid = true;
        $response = array();
        $participant_details = $this->basic_model->get_row($table_name = 'participant', $columns = array('firstname, lastname, id'), $id_array = array('id' => $data->participant_firstname->value));
        // Last Name
        if ($data->participant_firstname->label != $participant_details->firstname && $data->participant_lastname != $participant_details->lastname && $data->id != $participant_details->id) {
            $response['participant_firstname'] = array('error' => true, 'message' => "First name, Last name, HCM ID & Email doesn't belong to this participant");
            $valid = false;
        } else {
            $response['participant_firstname'] = array('error' => false);
        }
        //  Validate for company
        $companyId = 1;
        $cmp = false;
        $page = !empty($page) && $page > 0 ? $page : 1;
        $order_by = ['field_name' => 'Name', 'direction' => 'ASC'];
        // $contactDetails = $this->Basic_model->get_record_where('plan_management_vendor','company_name,biller_code,billpay_code');
        // if($data->company_name!=null){
        //   $cmp = in_array(trim($data->company_name->value), array_column($contactDetails, 'company_name'));
        $contactDetails = $this->xerocontact->get_contact_list($companyId, ['page' => $page, 'modifiedAfter' => new \DateTime(date('2019-07-17')), 'order_by' => $order_by]);
        if ($data->company_name != null) {
            $cmp = in_array(trim($data->company_name->label), array_column($contactDetails['data'], 'Name'));
            if ($cmp == false) {
                $response['new_company'] = array('error' => true);
                $response['company_name'] = array('error' => false);
                $valid = false;
            } else {
                $response['company_name'] = array('error' => false);
            }
        } else {
            $response['company_name'] = array('error' => true, 'message' => 'Company Name does Not Match');
        }
        // validate for company first time serive for participant
        $participant_id = $data->participant_firstname->value;
        $company_details = $this->Basic_model->get_record_where('plan_management', 'company_name', array('participant_id' => $participant_id));
        $count = 0;
        foreach ($company_details as $value) {
            if ((trim($data->company_name->value)) == trim($value->company_name)) {
                $count++;
            }
        }
        if ($count > 1 || $cmp == false) {
            $response['service_count'] = array('error' => false);
        } else {
            $response['service_count'] = array('error' => true, 'message' => "This is the first time " . $data->participant_firstname->label . " has received an invoice from " . $data->company_name->value);
        }
        // Here need to check available fund once data available in funds table
        if ((float) $data->invoice_amount <= 0) {
            $response['invoice_amount'] = array('error' => true, 'message' => 'Invoice Amount should be greater than 0');
            $valid = false;
        } else if ($data->funds <= $data->invoice_amount && $data->funds > 0) {
            $response['invoice_amount'] = array('error' => true, 'message' => "Participant's balance $data->funds is not sufficient to clear the invoice amount $data->invoice_amount");
            $valid = false;
        } else {
            $response['invoice_amount'] = array('error' => false);
        }
        //Xero invoice number validation
        $fillterRequest = ['page' => $page];
        $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
        // var_dump($inoviceDetails);
        $duplicate_check = $this->Invoice_model->invoice_already_exsits($data, 'validation');
        if (!empty($duplicate_check)) {
            $response['invoice_number'] = array('error' => true, 'message' => 'Invoice Number already exists');
            $valid = false;
        } else {
            if ($data->invoice_number != null && $inoviceDetails['status']) {
                $invoice_cmp = in_array(trim($data->invoice_number), array_column($inoviceDetails['data'], 'InvoiceNumber'));
                if ($invoice_cmp == false) {
//                    $response['invoice_number'] = array('error' => true, 'message' => 'Not Known Biller');
//                    $valid = false;
                } else {
                    $response['invoice_number'] = array('error' => false);
                }
            } else {
//                $response['invoice_number'] = array('error' => true, 'message' => 'Not Known Biller');
            }
        }

        //biller validation
        $response['billpay_error'] = array('error' => false, 'message' => '');
        $response['ref_no_error'] = array('error' => false, 'message' => '');
        $response['bsb_error'] = array('error' => false, 'message' => '');
        $response['account_no_error'] = array('error' => false, 'message' => '');
        $response['account_name_error'] = array('error' => false, 'message' => '');
        $response['biller_code_reference_no_check'] = array('error' => false, 'message' => '');
        $response['reference_no_error'] = array('error' => false, 'message' => '');
        switch ($data->payment_method) {
            case '1':
                if ($data->biller_code != null) {
                    $biller_status = $this->check_biller_refernce($data);
                    $response['biller_code_reference_no_check'] = array('error' => $biller_status['status'], 'message' => 'Invalid Biller Code');
                    $payment_valid = false;
                } else {
                    $response['biller_code_reference_no_check'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                if (strlen($data->reference_no) == 0 || strlen($data->reference_no) > 15) {
                    $response['reference_no_error'] = array('error' => false, 'message' => 'Reference Number should be in between 1 to 15');
                    $payment_valid = false;
                } else {
                    $response['reference_no_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                break;
            case '2':
                if (strlen($data->billpay_code) == 0 || strlen($data->billpay_code) > 4) {
                    $response['billpay_error'] = array('error' => false, 'message' => 'Billpay should be in between 1 to 4');
                    $payment_valid = false;
                } else {
                    $response['billpay_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                if (strlen($data->ref_no) == 0 || strlen($data->ref_no) > 15) {
                    $response['ref_no_error'] = array('error' => false, 'message' => 'Reference Number should be in between 1 to 15');
                    $payment_valid = false;
                } else {
                    $response['ref_no_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                break;
            case '3':
                if (strlen($data->bsb) == 0 || strlen($data->bsb) > 4) {
                    $response['bsb_error'] = array('error' => false, 'message' => 'BSB Code is required');
                    $payment_valid = false;
                } else {
                    $response['bsb_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                $check_acc_no = preg_match("/^\d{3}-?\d{3}$/", $data->acc_no);
                if (!$check_acc_no) {
                    $response['account_no_error'] = array('error' => false, 'message' => 'The length is 6 digits without dash or 7 digits with dash and contains only numbers. ex:123-123 or 123123');
                    $payment_valid = false;
                } else {
                    $response['account_no_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                if (strlen($data->acc_name) == 0) {
                    $response['account_name_error'] = array('error' => false, 'message' => 'Account Name is required');
                    $payment_valid = false;
                } else {
                    $response['account_name_error'] = array('error' => true, 'message' => '');
                    $payment_valid = true;
                }
                break;
        }
        //invoice date validation
        $today = date("Y-m-d");
        $previous = date("Y-m-d", strtotime("-1 year"));
        $invoice_date = DateFormate($data->invoice_date, 'Y-m-d');
        if ($invoice_date != '0000-00-00' && (strtotime($previous) <= strtotime($invoice_date)) && (strtotime($invoice_date) <= strtotime($today))) {
            $response['invoice_date'] = array('error' => false);
        } else {
            $response['invoice_date'] = array('error' => true, 'message' => 'Invalid Invoice Date, Invoice date is more than 1 year old OR invoice date is in the future');
            $valid = false;
        }
        //due date validation
        $due = str_replace('/', '-', $data->due_date);
        if ($due != '0000-00-00' && (strtotime($today) < strtotime($due))) {
            $response['due_date'] = array('error' => false);
        } else {
            $response['due_date'] = array('error' => true, 'message' => 'Invalid Due Date, Invoice due date is in the past');
            $valid = false;
        }
        $res = (array) $data;
        $res['validate'] = $response;

        if ($valid && $payment_valid)
            return true;
        else {
            echo json_encode(array('status' => false, 'data' => $res));
            exit;
        }
    }

    public function check_biller_refernce($invoicedetails) {
        $response = array();
        $tbl = 'plan_management_biller';
        $biller_code = $invoicedetails->biller_code;
        $biller_reference_number = $invoicedetails->reference_no;
        $colums = (TBL_PREFIX . $tbl . ".id");
        $this->db->select($colums);
        $this->db->where('biller_code', $biller_code);
        $result = $this->db->get(TBL_PREFIX . $tbl);
        $result = $result->result();
        if (!empty($result)) {
            $response = array('status' => true);
        } else {
            $response = array('status' => false);
        }
        return $response;
    }

    public function get_xero_list() {
        $fillterRequest = ['page' => 1];
        $inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
        print ('<pre>');
        print_r($inoviceDetails);
        print ('</pre>');
    }

    function conv() {
        $givenfileName = 'compressed.tracemonkey-pldi-09.pdf';
        $allFilesPath = 'D:\xampp\htdocs\ocs_admin\finance-planner\back-end\assets/documents/allfiles/1/compressed.tracemonkey-pldi-09.pdf';
        $readfile = 'assets/documents/htmlfiles/1/';
        $savedConvertedPath = 'D:\xampp\htdocs\ocs_admin\finance-planner\back-end\assets/documents/htmlfiles/1/';

        $x = $this->convert_to_pdf($givenfileName, $allFilesPath, $readfile, $savedConvertedPath);

        print_r($x);
    }

}
