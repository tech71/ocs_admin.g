<?php

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

function pr($data, $die = 1) {
    print_r("<pre>");
    print_r($data);
    if ($die == 1) {
        die;
    }
}

function last_query($die = 0) {
    $ci = & get_instance();
    echo $ci->db->last_query();
    if ($die == 1) {
        die;
    }
}

function all_user_data() {
    $ci = & get_instance();
    echo '<pre>';
    print_r($ci->session->all_userdata());
    exit;
}

function logout_login_history($adminId) {
    $CI = & get_instance();
    $check_previous = $CI->basic_model->update_records('member_login_history', array('status' => 2), $where = array('memberId' => $adminId, 'status' => 1));
}

function request_handler($permission_key = false, $check_token = 1, $pin = false) {
    require_once APPPATH . 'Classes/finance_planner/AuthManagement.php';
    $adminAuth = new AuthManagement();

    $request_body = file_get_contents('php://input');
    $request_body = json_decode($request_body);


    if ($check_token && !empty($request_body)) {

        // here verify Domian request
        $ser_response = $adminAuth->verify_server_request();
        if ($ser_response['status'] === false) {
            echo json_encode($ser_response);
            exit();
        }

        // here check token 
        $adminAuth->setToken($request_body->token);
        $adminAuth->setIp_address(get_client_ip_server());
        $response = $adminAuth->verify_admin_token();

        if ($response['status'] === false) {
            echo json_encode($response);
            exit();
        }

        $adminAuth->update_token_time();

        $request_body->adminId = $adminAuth->getMemberId();
//        print_r($request_body);
        return $request_body;
    } elseif (!$check_token) {
        return $request_body->data;
    } else {
        echo json_encode(array('status' => false, 'token_status' => true, 'error' => system_msgs('verfiy_token_error')));
        exit();
    }
}

function get_content_from_url($url) {
    $resp = '';
    try {
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'YDT'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        //echo '<pre>';print_r($resp);
        if (FALSE === $resp)
            throw new Exception(curl_error($curl), curl_errno($curl));

        // ...process $resp now
    } catch (Exception $e) {
        $resp = trigger_error(sprintf('Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
    }
    return $resp;
}

function request_handlerFile($permission_key = false, $check_token = 1, $pin = false) {
    require_once APPPATH . 'Classes/finance_planner/AuthManagement.php';
    $adminAuth = new AuthManagement();

    $CI = & get_instance();
    $request_data = (object) $CI->input->post();

    if ($check_token && !empty($request_data)) {

        $ser_response = $adminAuth->verify_server_request();
        if ($ser_response['status'] === false) {
            echo json_encode($ser_response);
            exit();
        }

        // here check token 
        $adminAuth->setToken($request_data['token']);
        $adminAuth->setIp_address(get_client_ip_server());
        $response = $adminAuth->verify_admin_token();

        if ($response['status'] === false) {
            echo json_encode($response);
            exit();
        }

        $adminAuth->update_token_time();

        return $request_data;
    } elseif (!$check_token) {
        return $request_body->data;
    } else {
        echo json_encode(array('status' => false, 'token_status' => true, 'error' => system_msgs('verfiy_token_error')));
        exit();
    }
}

function do_upload($config_ary) {
    $CI = & get_instance();
    $response = array();
    if (!empty($config_ary)) {
        $directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

        $config['upload_path'] = $directory_path;
        $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
        $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '';
        $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
        $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';

        create_directory($directory_path);

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload($config_ary['input_name'])) {
            $response = array('error' => $CI->upload->display_errors());
        } else {
            $response = array('upload_data' => $CI->upload->data());
        }
    }
    return $response;
}

function do_muliple_upload($config_ary) {
    $CI = & get_instance();
    $CI->load->library('upload');
    $response = array();

    if (!empty($config_ary)) {
        $directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

        $config['upload_path'] = $directory_path;

        $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
        $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '';
        $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
        $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';

        create_directory($directory_path);

        $input_name = $config_ary['input_name'];

        $files = $_FILES;
        $cpt = count($_FILES[$input_name]['name']);

        for ($i = 0; $i < $cpt; $i++) {
            $_FILES[$input_name]['name'] = $files[$input_name]['name'][$i];
            $_FILES[$input_name]['type'] = $files[$input_name]['type'][$i];
            $_FILES[$input_name]['tmp_name'] = $files[$input_name]['tmp_name'][$i];
            $_FILES[$input_name]['error'] = $files[$input_name]['error'][$i];
            $_FILES[$input_name]['size'] = $files[$input_name]['size'][$i];

            $CI->upload->initialize($config);

            if (!$CI->upload->do_upload($input_name)) {
                $response[] = array('error' => $CI->upload->display_errors());
            } else {
                $response[] = array('upload_data' => $CI->upload->data());
            }
        }

        return $response;
    }
}

function create_directory($directoryName) {
    if (!is_dir($directoryName)) {
        mkdir($directoryName, 0755);
        fopen($directoryName . "/index.html", "w");
    }
}

function get_client_ip_server() {
    $ipaddress = '';
    if (array_key_exists('HTTP_CLIENT_IP', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (array_key_exists('HTTP_X_FORWARDED', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } elseif (array_key_exists('HTTP_FORWARDED_FOR', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } elseif (array_key_exists('HTTP_FORWARDED', @$_SERVER)) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } elseif (array_key_exists('REMOTE_ADDR', @$_SERVER)) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = 'UNKNOWN';
    }
    $ipaddresses = explode(',', $ipaddress);
    $ipaddress = isset($ipaddresses[0]) ? $ipaddresses[0] : 0;
    return $ipaddress;
}

function add_hour_minute($times) {
    // pr($times);
    error_reporting(0);
    $minutes = 0;
    foreach ($times as $time) {
        list($hour, $minute) = explode(':', $time);
        $minutes += $hour * 60;
        $minutes += $minute;
    }
    $hours = floor($minutes / 60);
    $minutes -= $hours * 60;
    return sprintf('%02d:%02d', $hours, $minutes);
}

function is_json($string) {
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function dateRangeBetweenDate($start_date, $end_date) {
    $first = $start_date;
    $last = $end_date;

    $dates = array();
    $step = '+1 day';
    $format = 'Y-m-d';
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $date1 = date($format, $current);
        $dates[$date1] = date('D', $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

function dayDifferenceBetweenDate($fromDate, $toDate) {
    $now = strtotime($toDate); //current date
    $your_date = strtotime($fromDate);
    $datediff = $now - $your_date;
    return round($datediff / (60 * 60 * 24));
}

function getStateById($stateId) {
    $ci = & get_instance();
    $result = $ci->basic_model->get_row('state', array('name'), $where = array('id' => $stateId));

    if (!empty($result)) {
        return $result->name;
    } else {
        return false;
    }
}

function getLatLong($address) {
    $ci = & get_instance();
    if (!empty($address)) {
        $formattedAddr = str_replace(' ', '+', $address);

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=' . GOOGLE_MAP_KEY;

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);

        if (empty($buffer)) {
            return false;
        } else {
            $output = json_decode($buffer);
            $data = array();

            if (!empty($output->results)) {
                $data['lat'] = $output->results[0]->geometry->location->lat;
                $data['long'] = $output->results[0]->geometry->location->lng;

                if (!empty($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}

function dateRangeBetweenDateWithWeek($start_date, $end_date, $totalWeek) {
    $first = $start_date;
    $last = $end_date;

    $dates = array();
    $step = '+1 day';
    $format = 'Y-m-d';
    $current = strtotime($first);
    $last = strtotime($last);
    $weekNumber = 0;
    $cnt = count($totalWeek);

    $weekCount = 1;
    while ($current <= $last) {
        $date1 = date($format, $current);
        if (($weekNumber) == $cnt) {
            $weekNumber = 0;
        }

        $dates[$totalWeek[$weekNumber]][$weekCount][$date1] = date('D', $current);

        if (date('D', $current) == 'Sun') {
            $weekNumber++;
            $weekCount++;
        }

        $current = strtotime($step, $current);
    }
    return $dates;
}

function numberToDay($key = false) {
    $myDayAry = array(1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun');
    if ($key)
        return $myDayAry[$key];
    else
        return $myDayAry;
}

function DateFormate($date, $formate) {
    return date($formate, strtotime($date));
}

function strTime($time) {
    return DateTime::createFromFormat('H:i', date('H:i', strtotime($time)));
}

function strTimes($time) {
    return (date('H:i', strtotime($time)));
}

function timeZoneDevidation($date, $timeZone, $formate = "Y-m-d H:i:s") {
    return date($formate, strtotime($date) + ($timeZone * 60));
}

function concatDateTime($date, $time) {
    $newData = date('Y-m-d', strtotime($date));
    $newTime = date('H:i:s', strtotime($time));

    $dateTime = date('Y-m-d H:i:s', strtotime("$newData . $newTime"));

    return $dateTime;
}

function setting_length($x, $length) {
    if (strlen($x) <= $length) {
        return $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        return $y;
    }
}

function get_super_admins($defualt = 'id') {
    $CI = & get_instance();

    if ($defualt == 'id') {
        $res = $CI->basic_model->get_record_where('member', ['id'], 'username IN("admin","hcm.admin")');

        $admins_ids = [];
        if (!empty($res)) {
            $admins_ids = array_column(obj_to_arr($res), 'id');
        }
        return $admins_ids;
    } else {
        return $usernames = ['admin', 'hcm.admin'];
    }
}
