<?php

function emailHeader() {
    $obj = &get_instance();
    return $obj->load->view('finance_planner/header_mail_template', null, true);
}

function emailFooter() {
    $obj = &get_instance();
    return $obj->load->view('finance_planner/footer_mail_template', null, true);
}

function send_mail_smtp($to_email, $subject, $body, $cc_email_address = null, $file_attach = null) {
    $obj = &get_instance();
    $msg = $body;
    $from_email = 'developer@yourdevelopmentteam.com.au';
    $obj->load->library('email');
    $config['protocol'] = "smtp";
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    //$config['smtp_host'] = 'tls://smtp.gmail.com';
    $config['smtp_port'] = 465;  //25
    $config['smtp_user'] = 'developer@yourdevelopmentteam.com.au';
    $config['smtp_pass'] = 'NPRBpXEtUf';
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";
    $config['priority'] = "1";
    $obj->email->initialize($config);
    $obj->email->from($from_email, APPLICATION_NAME);
    $obj->email->to($to_email);
    $obj->email->subject($subject);
    $obj->email->message($msg);
    $obj->email->bcc(ADMIN_EMAIL);
    if ($file_attach != null) {
        $obj->email->attach($file_attach);
    }
    $obj->email->send();
    $output = $obj->email->print_debugger();

    return true;
}

function send_mail_php_mailer($to_email, $subject, $body, $cc_email_address = null, $file_attach = null) {
    //mail send using ci library
    $obj = &get_instance();
    $obj->load->library('email');
    $obj->email->set_mailtype('html');
    $obj->email->from(FROM_EMAIL, APPLICATION_NAME);
    $obj->email->to($to_email);
    $obj->email->subject($subject);
    $obj->email->message($body);
    $obj->email->bcc(ADMIN_EMAIL);
    if ($file_attach != null) {
        $obj->email->attach($file_attach);
    }
    $return = @$obj->email->send();

    return true;
}

function send_mail($to_email, $subject, $body, $cc_email_address = null, $file_attach = null) {
    if (MAIL_METHOD === "PHP_MAILER") {
        send_mail_php_mailer($to_email, $subject, $body, $cc_email_address, $file_attach);
    } elseif (MAIL_METHOD === "SMTP") {
        send_mail_smtp($to_email, $subject, $body, $cc_email_address, $file_attach);
    } else {
        send_mail_php_mailer($to_email, $subject, $body, $cc_email_address, $file_attach);
    }
}

function forgot_password_mail($userdata, $cc_email_address = null) {
    $obj = & get_instance();
    $subject = 'HCM Marketing: Reset your password';
    $userdata['username'] = $userdata['firstname'] . ' ' . $userdata['lastname'];

    $msg = $obj->load->view('finance_planner/forgot_password_mail_template', $userdata, true);
    $msg = emailHeader() . $msg . emailFooter();

    $output = send_mail($userdata['email'], $subject, $msg);

    return $output;
}

function password_reset_success_mail($userdata, $cc_email_address = null) {
    $obj = & get_instance();
    $subject = 'HCM Marketing: Password reset successfully';


    $msg = $obj->load->view('finance_planner/password_reset_successfully_mail_template', $userdata, true);
    $msg = emailHeader() . $msg . emailFooter();

    $output = send_mail($userdata['email'], $subject, $msg);
    return $output;
}
