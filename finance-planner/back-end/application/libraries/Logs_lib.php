<?php

class Logs_lib {

    protected $CI;
    protected $userId;
    protected $title;
    protected $action;
    protected $plan_id;
    protected $participant_id;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
    }

    function setUserID($userId) {
        $this->userId = $userId;
    }

    function getUserID() {
        return $this->userId;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function getAction() {
        return $this->action;
    }

    function setPlanId($plan_id) {
        $this->plan_id = $plan_id;
    }

    function getPlanId() {
        return $this->plan_id;
    }

    function setParticipantId($participant_id) {
        $this->participant_id = $participant_id;
    }

    function getParticipantId() {
        return $this->participant_id;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function getTitle() {
        return $this->title;
    }
    function setDescription($description) {
        $this->description = $description;
    }

    function getDescription() {
        return $this->description;
    }

    function createLog() {

        $data = array(
            'user' => $this->userId,
            'title' => $this->title,
            'Description' => $this->description,
            'plan_id' => $this->plan_id,
            'participant_id' => $this->participant_id,
            'action' => $this->action,
            'created' => date('Y-m-d h:i:sa'),
        );

        $this->CI->db->insert(TBL_PREFIX . 'plan_management_audit_logs', $data);
    }

}
