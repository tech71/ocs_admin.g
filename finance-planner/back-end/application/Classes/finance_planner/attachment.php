<?php

use Gufy\PdfToHtml\Config;

namespace classAttachment;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Attachment {

    public function __construct() {
        $this->CI = & get_instance();
        //$this->CI->load->model('admin/Admin_model');
    }

    public function getAttachments($username, $password, $max_emails, $participant_id) {
        error_reporting(0);

        $arr_email = array();
        //$last_email_date = "2018-08-28";
        $last_email_date = date('Y-m-d');
        // $last_fetch = $this->CI->Basic_model->get_record_where_orderby('email_last_fetch_time', array('last_fetch'),'','id','DESC');
        // if(!empty($last_fetch)){
        //   $last_email_date = $last_fetch[0]->last_fetch;
        // }
        $dateemail = date('d M Y', strtotime($last_email_date));
        $dateemailcheck = date('m/d/Y H:i:s', strtotime($last_email_date));
        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $inbox = imap_open($hostname, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());
        $emails = imap_search($inbox, 'SINCE "28 Aug 2019"');
        if ($emails) {
            $count = 1;
            $cnt = 1;
            $arrcnt = 1;
            sort($emails);
            $arr_email = array();
            foreach ($emails as $email_number) {
                $arr_att = array();

                $namefrom = '';
                $dateemail = '';
                $emailid = '';
                $subject = '';

                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 1.2);
                foreach ($overview as $details) {
                    $namefrom = $details->from;
                    $dateemail = $details->date;
                    $emailid = $details->uid;
                    $pdf_date = date('m/d/Y H:i:s', strtotime($details->date));
                    $subject = $details->subject;
                    $mass = $message;
                }
                if ($dateemailcheck < $pdf_date) {

                    $arr_email[$arrcnt]['name'] = $namefrom;
                    $arr_email[$arrcnt]['date'] = $dateemail;
                    $arr_email[$arrcnt]['mailid'] = $emailid;
                    $arr_email[$arrcnt]['subject'] = $subject;


                    $structure = imap_fetchstructure($inbox, $email_number);
                    $attachments = array();
                    if (isset($structure->parts) && count($structure->parts)) {
                        for ($i = 0; $i < count($structure->parts); $i++) {
                            $attachments[$i] = array(
                                'is_attachment' => false,
                                'filename' => '',
                                'name' => '',
                                'attachment' => ''
                            );
                            if ($structure->parts[$i]->ifdparameters) {
                                foreach ($structure->parts[$i]->dparameters as $object) {
                                    if (strtolower($object->attribute) == 'filename') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['filename'] = $object->value;
                                    }
                                }
                            }
                            if ($structure->parts[$i]->ifparameters) {
                                foreach ($structure->parts[$i]->parameters as $object) {
                                    if (strtolower($object->attribute) == 'name') {
                                        $attachments[$i]['is_attachment'] = true;
                                        $attachments[$i]['name'] = $object->value;
                                    }
                                }
                            }

                            if ($attachments[$i]['is_attachment']) {
                                $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);


                                if ($structure->parts[$i]->encoding == 3) {
                                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                                } elseif ($structure->parts[$i]->encoding == 4) {
                                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                                }
                            }
                        }
                    }

                    foreach ($attachments as $attachment) {
                        if ($attachment['is_attachment'] == 1) {
                            $filename = $attachment['name'];


                            $file_name[] = $email_number . "-" . $attachment['name'];

                            if (empty($filename))
                                $filename = $attachment['filename'];

                            if (empty($filename))
                                $filename = time() . ".dat";

                            $filename = strtolower($filename);
                            $savedPath = FCPATH . PLAN_MANAGEMENT_ALLFILES . $participant_id . "/";
                            if (!is_dir($savedPath)) {
                                mkdir($savedPath, 0777, TRUE);
                            }
                            $fp = fopen(FCPATH . PLAN_MANAGEMENT_ALLFILES . $participant_id . "/" . $filename, "w+");
                            fwrite($fp, $attachment['attachment']);
                            fclose($fp);

                            $filename1 = $filename;
                            $arr_att[] = $filename1;
                            $fname[] = $filename1;
                        }
                    }
                    $arr_email[$arrcnt]['attachment'] = $arr_att;
                    $arrcnt++;
                    if ($count++ >= $max_emails)
                        break;
                }
                $cnt++;
            }
            return $arr_email;
        }
    }

}
