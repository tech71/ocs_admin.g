<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdminLogin
 *
 * @author user
 */
require_once APPPATH . 'Classes/finance_planner/JWT.php';

class AuthManagement {

    public function __construct() {
        
    }

    private $memberId;
    private $ip_address;
    private $token;
    private $pin;
    private $created;
    private $updated;
    private $details;
    private $login_time;
    private $last_access;
    private $status;
    private $username;
    private $password;

    function setMemberId($memberId) {
        $this->memberId = $memberId;
    }

    function getMemberId() {
        return $this->memberId;
    }

    function setIp_address($ip_address) {
        $this->ip_address = $ip_address;
    }

    function getIp_address() {
        return $this->ip_address;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function getToken() {
        return $this->token;
    }

    function setPin($pin) {
        $this->pin = $pin;
    }

    function getPin() {
        return $this->pin;
    }

    function setCreated($created) {
        $this->created = $created;
    }

    function getCreated() {
        return $this->created;
    }

    function setUpdated($updated) {
        $this->updated = $updated;
    }

    function getUpdated() {
        return $this->updated;
    }

    function setDetails($details) {
        $this->details = $details;
    }

    function getDetails() {
        return $this->details;
    }

    function setLogin_time($login_time) {
        $this->login_time = $login_time;
    }

    function getLogin_time() {
        return $this->login_time;
    }

    function setLast_access($last_access) {
        $this->last_access = $last_access;
    }

    function getLast_access() {
        return $this->last_access;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getStatus() {
        return $this->status;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function getUsername() {
        return $this->username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function getPassword() {
        return $this->password;
    }

    function check_login() {
        $CI = & get_instance();
        $Obj_JWT = new \JWT();

        $CI->load->model('Auth_management_model');
        $result = $CI->Auth_management_model->check_login($this);

        if (!empty($result)) {

            // check password using PASSWORD_BCRYPT method
            if (password_verify($this->getPassword(), $result->password)) {

                // check user active or not 
                if ($result->status) {
                    //set token
                    $this->setMemberId($result->id);

                    $token = array(DATE_TIME . $result->id);
                    $JWT_Token = $Obj_JWT->encode($token, JWT_SECRET_KEY);

                    $this->setToken($JWT_Token);
                    $this->set_admin_login();

                    $response = array('token' => $JWT_Token, 'fullname' => $result->full_name, 'status' => true, 'success' => system_msgs('success_login'));
                } else {
                    $response = array('status' => false, 'error' => system_msgs('account_not_active'));
                }
            } else {
                $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
            }
        } else {
            $response = array('status' => false, 'error' => system_msgs('wrong_username_password'));
        }

        return $response;
    }

    public function set_admin_login() {
        $CI = & get_instance();

        $columns = array('updated', 'memberId', 'token');
        $where = array('memberId' => $this->memberId);
        $response = $CI->basic_model->get_row('member_login', $columns, $where);

        if (!empty($response)) {

            $columns = array('updated' => DATE_TIME, 'pin' => '', 'token' => $this->token, 'ip_address' => $this->ip_address);
            $where = array('memberId' => $this->memberId);
            $CI->basic_model->update_records('member_login', $columns, $where);
        } else {

            $data = array(
                'token' => $this->token,
                'memberId' => $this->memberId,
                'created' => DATE_TIME,
                'updated' => DATE_TIME,
                'pin' => '',
                'ip_address' => $this->ip_address,
            );

            $CI->basic_model->insert_records('member_login', $data, FALSE);
        }
    }

    function add_login_history() {
        $CI = & get_instance();

        // check status of preivous login user
        $column = array('status' => 2);
        $where = array('memberId' => $this->memberId, 'status' => 1);
        $CI->basic_model->update_records('member_login_history', $column, $where);

        $data = array(
            'ip_address' => $this->ip_address,
            'details' => $this->details,
            'memberId' => $this->memberId,
            'status' => 1,
            'login_time' => DATE_TIME,
            'last_access' => DATE_TIME,
            'token' => $this->token
        );

        $CI->basic_model->insert_records('member_login_history', $data, $multiple = false);
    }

    function verify_admin_token() {
        $CI = & get_instance();

        $CI->load->model('Auth_management_model');
        $response = $CI->Auth_management_model->check_auth_token($this);

        if (!empty($response)) {
            $this->setMemberId($response->memberId);
            $diff = strtotime(DATE_TIME) - strtotime($response->updated);

            // here check login time is not greater than 30 min.
            if ($diff > $CI->config->item('jwt_token_time')) {
                return ['status' => false, 'token_status' => true, 'error' => system_msgs('verfiy_token_error')];
            }

            // here chack the current user ip address same as login time
            if ($response->ip_address != $this->ip_address) {
                return array('status' => false, 'ip_address_status' => true, 'error' => system_msgs('ip_address_error'));
            }


            return ['status' => true, 'memberId' => $this->memberId];
        } else {
            $response = $this->check_another_location_opened();

            if ($response) {
                return ['status' => false, 'another_location_opened' => true, 'error' => 'This account is opened at another location, you are being logged off.'];
            } else {
                return ['status' => false, 'token_status' => true, 'error' => system_msgs('verfiy_token_error')];
            }
        }
    }

    function update_token_time() {
        $CI = & get_instance();

        $columns = array('updated' => DATE_TIME);
        $where_al = array('token' => $this->token);
        $CI->basic_model->update_records('member_login', $columns, $where_al);

        return true;
    }

    function update_login_history($adminId) {
        $CI = & get_instance();

        $column = array('last_access' => DATE_TIME);
        $where = array('memberId' => $this->memberId, 'status' => 1);
        $CI->basic_model->update_records('member_login_history', $column, $where);

        return true;
    }

    function check_another_location_opened() {
        $CI = & get_instance();

        $where_h = array('token' => $this->token, 'ip_address' => $this->ip_address);
        $columns = array('last_access');
        $response = $CI->basic_model->get_row('member_login_history', $columns, $where_h);

        if (!empty($response)) {
            $diff = strtotime(DATE_TIME) - strtotime($response->last_access);

            if ($diff < $CI->config->item('jwt_token_time')) {
                return true;
            }
        }

        return false;
    }

    function verify_server_request() {
        $CI = & get_instance();
        $request_server = (!empty($_SERVER['HTTP_ORIGIN'])) ? $_SERVER['HTTP_ORIGIN'] : '';

        $servers = $CI->config->item('request_accept_server');

        if (in_array($request_server, $servers)) {
            return array('status' => true);
        } else {
            return array('status' => false, 'server_status' => true, 'error' => system_msgs('server_error'));
        }
    }

}
