<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");

use thiagoalessio\TesseractOCR\TesseractOCR;

include 'vendor/autoload.php';

if (!empty($_POST['data'])) {
    define('UPLOAD_DIR', 'temp_img/');

    $img = $_POST['data'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = UPLOAD_DIR . uniqid() . '.png';

   
    $success = file_put_contents($file, $data);

    if ($success) {
        $obj = new TesseractOCR($file);
        $obj->executable('C:/Program Files/Tesseract-OCR/tesseract.exe');
        $response = $obj->run();

        $res = ['status' => true, 'data' => $response];
    } else {
        $res = ['status' => false, 'data' => 'Sorry unable to get text'];
    }

    echo json_encode($res);
}





