#!/usr/bin/env python3.6
import mysql.connector
import urllib,sys
import PyPDF2
import requests
import io
from io import StringIO
#from flask import Flask,render_template,url_for,request
# import pickle.pickle
import pandas as pd
import joblib
from joblib import load
import random
import imutils
import cv2
import numpy
import logging

# saved_model = open ('/var/www/html/fpapi.dev.healthcaremgr.net/public_html/python/finalized_model.sav', 'rb')
loaded_model = joblib.load(open("D:/xampp/htdocs/ocs_admin/finance-planner/back-end/python/finalized_model.sav","rb"))
import mysql.connector
from dateutil.parser import parse
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="ocs",
  port="3306"
  )
mycursor = mydb.cursor()
mycursor1 = mydb.cursor()
mycursor.execute("SELECT CONCAT('http://localhost/ocs_admin/finance-planner/back-end/',pdf_url),id  FROM tbl_plan_management WHERE read_status = 0")

myresult = mycursor.fetchall()
global t
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
file_handler = logging.FileHandler('D:/xampp/htdocs/ocs_admin/finance-planner/back-end/python/error_log_file.log')
formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
def pdf_url_reader1():
  y=[]
  try:
      for i in myresult:
        url = i[0]
        extension = url[-3:]
        if extension == 'pdf':

          response = requests.get(url)
          my_raw_data = response.content

          try:
              pdf_content = io.BytesIO(my_raw_data)
              pdf_reader = PyPDF2.PdfFileReader(pdf_content)

              if pdf_reader.isEncrypted:


                  logger.info('encrypted')
                  pdf_reader.decrypt("")
                  x = (pdf_reader.getPage(0).extractText())
                  result = test(x)
                  y.append(result)
                  db_update(result,i)
                  logger.info('INPUT: %s', x)
              else:

                  x = (pdf_reader.getPage(0).extractText())
                  logger.info('INPUT: %s', x)
              result = test(x)
              y.append(result)
              db_update(result,i)

          except Exception as e:
              logger.error('error:%s', e)
        else :

          img = imutils.url_to_image(url)
          img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
          cv2.imwrite('D:/xampp/htdocs/ocs_admin/finance-planner/back-end/python/invoiceimage.png',img)

          try:
            from PIL import Image
          except ImportError:
            import Image
          import pytesseract
          #pytesseract.pytesseract.tesseract_cmd = r"C:/Program Files/Tesseract-OCR/tesseract.exe"
          x = (pytesseract.image_to_string(Image.open('D:/xampp/htdocs/ocs_admin/finance-planner/back-end/python/invoiceimage.png')))
          logger.info('INPUT: %s', x)
          result = test(x)
          y.append(result)
          db_update(result,i)

  except IOError:

      logger.error("Failed to open url")
      sys.exit()
  return y
def test(t):
  # loaded_model = pickle.dump(pickle.load("/var/www/html/fpapi.dev.healthcaremgr.net/public_html/python/finalized_model.sav"), protocol=2)
  loaded_model = joblib.load(open("D:/xampp/htdocs/ocs_admin/finance-planner/back-end/python/finalized_model.sav","rb"))

  tokens = [(t.split()) ]

  prds = loaded_model.predict(tokens)

  d = []
  for i,j in zip(prds[0],tokens[0]):
      d.append((i,j))
      df = pd.DataFrame(d, columns=('named entity', 'output'))

      x = random.choices(d[:],k=10)
      op = dict(x)

      list1=[]
      list1=(list(op.values()))
  return list1

def db_update(po,i):
  mycursor2 = mydb.cursor()
  invoice_number = po[0]
  logger.info('Invoice no: %s', invoice_number)
  def is_date(string, fuzzy=False):

      try:
          parse(string, fuzzy=fuzzy)
          return True

      except ValueError:
            return False
  h=is_date(po[1])
  mycursor.execute("SET SQL_MODE = ALLOW_INVALID_DATES")
  logger.info('Invoice date: %s', po[1])
  if (h=='false'):
    invoice_date=po[1]
  else:
    invoice_date='00-00-0000'
  y = is_date(po[2])
  logger.info('Due date: %s', po[2])
  if (y=='false'):
    due_date=po[2]
  else:
     due_date='00-00-0000'



  company_name = po[3]
  logger.info('Company Name: %s', po[3])

  try:
    q1 = ("UPDATE tbl_plan_management SET invoice_number = %s,invoice_date = %s,due_date = %s,company_name = %s WHERE id = %s")

    v1 = (invoice_number,invoice_date,due_date,company_name,i[1])


    mycursor1.execute(q1,v1)


  except Exception as e:


                logger.info('error: %s', e)

  sql = "UPDATE tbl_plan_management SET read_status = %s WHERE id = %s"
  val = ("1", i[1])

  mycursor2.execute(sql, val)
  mydb.commit()
  logging.info('invoice status:read')

  return(po)
pdf_url_reader1()
