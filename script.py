image: "node:10.15.0"
pipelines: 
  branches: 
    master: 
      - 
        step:
          deployment: test
          name: "Install and Testing"
          caches:
            - node
            - npmadmin
            - npmfinance
            - npmmarketing
            - npmorganisation
          script: 
            - "apt-get update -y"
            - "apt-get install -y zip"
            - "cd admin/front-end"
            - "npm install"
            - CI=false
            - "npm run build"
            - "zip -r /tmp/artifact.zip *"
            - cd ..
            - cd ..
            - "cd finance-planner/front-end"
            - "npm install"
            - CI=false
            - "npm run build"
            - "zip -r /tmp/artifact.zip *"
            - cd ..
            - cd ..
            - "cd marketing/front-end"
            - "npm install"
            - CI=false
            - "npm run build"
            - "zip -r /tmp/artifact.zip *"
            - cd ..
            - cd ..
            - "cd organisation/front-end"
            - "npm install"
            - CI=false
            - "npm run build"
            - "zip -r /tmp/artifact.zip *"
          trigger: automatic
          artifacts:
            - admin/front-end/build/**
            - finance-planner/front-end/build/**
            - marketing/front-end/build/**
            - organisation/front-end/build/**
      - 
        step: 
          image: "python:3.5.7"
          name: test
          caches:
            - pip
          script: 
            - "apt-get update -y"
            - "pip install boto3==1.9.197"
            - "apt-get install -y zip"
            - "zip -r /tmp/artifact.zip *"
            - "python codedeploy_deploy.py"

definitions:
  caches:
    npmadmin: admin/front-end/build
    npmfinance: finance-planner/front-end/build
    npmmarketing: marketing/front-end/build
    npmorganisation: organisation/front-end/build