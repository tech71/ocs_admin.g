# Installation

## Admin Portal

### Backend

`vagrant up`

This will install Apache, PHP 7.2, MySQL, xdebug, composer and npm. It will also change Apache's configuration to point to the correct directory for the backend code, enable .htaccess overrides and hide the .git directory from the webserver. MySQL has default credentials root:root.

`mysql -u root -p < ocs.sql`

This will import the `ocs.sql` file into MySQL, assuming `ocs.sql` exists. Currently no schema has been committed to the repository, so it is recommended that you get the latest dump from our staging website.

`cd /var/www/html/admin/back-end && composer install`

This will install any PHP dependencies the backend needs.

#### Migrations(Currently we are not use this migration)

We use CodeIgniter's migration system along with a custom tool that makes it easier to create and run migrations.

**Creating a New Migration**

`php index.php Migrations new {name}`

**Running Migrations**

`php index.php Migrations run`

**Rolling Back/Forward Migrations**

`php index.php Migrations version {timestamp}`

**Resetting Migrations**

`php index.php Migrations reset`


**Fixing Migration Version**

`php index.php Migrations fix_version`

Useful for deleting migrations during testing and needing the latest version reset. *Should not be used in production, as migrations should never be deleted.*

### Frontend

`cd admin/front-end && npm-install`

This will install all node.js dependencies the frontend needs.

Ensure your `admin/front-end/src/config.js` `BASE_URL` points to the correct place (usually `localhost:8080`).

`npm run start`

This will build and serve a dev environment of our React code at `localhost:3000`.

### For new laravel migration

Make sure composer globally install on your server.
First command run "composer update" inside directory (application\migrations\migration-db) 
and environment variable setup in server, use same variable in database config file(application\migrations\migration-db\config\database.php)


###For creating migration file
php nomad make:migration create_{file_name}_table --create={DB_table_name}
##To run our migration
php nomad migrate
###For creating seed file:
php nomad make:seed  {ClassName in Pascal case}Seeder
###To run seed file (--class is required in below syntax):
php nomad db:seed --class={ClassName}Seeder