<?php

namespace FmsLocationClass;

/*
 * Filename: Fms.php
 * Desc: Fms of Members, end and start time of shift
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: Fms
 * Desc: Class Has 5 Arrays, variables ans setter and getter methods of shifts
 * Created: 25-10-2018
 */

class FmsLocation {

    public $CI;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('Shift_roster_model');
    }
	
    private $id;
    private $caseId;
	private $address;
    private $suburb;
    private $postal;
    private $state;
    private $status;
    private $lat;
    private $long;
    private $categoryId;
   

    public function setId($id){ $this->id = $id;}
    public function getId() {  return $this->id;}

    public function setCaseId($caseId){$this->caseId = $caseId;}
    public function getCaseId() { return $this->caseId;}

    public function setAddress($address){ $this->address = $address;  }
    public function getAddress() {     return $this->address;   }

   public function setSuburb($suburb) {  $this->suburb = $suburb; }
   public function getSuburb(){ return $this->suburb;}

   public function setPostal($postal){$this->postal = $postal; }
    public function getPostal() { return $this->postal; }

    public function setState($state) {$this->state = $state;}
    public function getState() {return $this->state; }

    public function setStatus($status) { $this->status = $status; }
    public function getStatus() { return $this->status; }

	public function setCategoryId($categoryId) { $this->categoryId = $categoryId; }
    public function getCategoryId() { return $this->categoryId; }
	
	public function setLat($lat) { $this->lat = $lat; }
    public function getLat() { return $this->lat; }
	
	public function setLong($long) { $this->long = $long; }
    public function getLong() { return $this->long; }
	  

}
