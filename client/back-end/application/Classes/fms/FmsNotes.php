<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace FmsNotesClass;

/**
 * Description of FmsNotes
 *
 * @author Corner stone solution
 */
class FmsNotes {

    private $noteid;
    private $caseId;
    private $title;
    private $description;
    private $created_by;
    private $created_type;
    public $CI;

    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('Shift_roster_model');
    }

    function setNoteId($noteid) {
        $this->noteid = $noteid;
    }

    function getNoteId() {
        return $this->noteid;
    }

    function setCaseId($caseId) {
        $this->caseId = $caseId;
    }

    function getCaseId() {
        return $this->caseId;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function getTitle() {
        return $this->title;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function getDescription() {
        return $this->description;
    }

    function setCreated_by($created_by) {
        $this->created_by = $created_by;
    }

    function getCreated_by() {
        return $this->created_by;
    }

    function setCreated_type($created_type) {
        $this->created_type = $created_type;
    }

    function getCreated_type() {
        return $this->created_type;
    }

    function createNote() {		
		$CI = & get_instance();
        $CI->load->model('Fms_model');
       return $CI->Fms_model->createNote($this);        
    }

}
