<?php

namespace classParticipantAddress;

/*
 * Filename: ParticipantAddress.php
 * Desc: Address details of Participant
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: ParticipantAddress
 * Desc:  Getter and Setter Methods for getting participant address.
 * Variables for participant address class, which are used in getter ands setter methods
 * Created: 02-08-2018
 */

class ParticipantAddress {

    /**
     * @var participantdocsid
     * @access private
     * @vartype: int
     */
    private $participantaddressid;

    /**
     * @var participantid
     * @access private
     * @vartype: int
     */
    private $participantid;

    /**
     * @var street
     * @access private
     * @vartype: varchar
     */
    private $street;

    /**
     * @var city
     * @access private
     * @vartype: varchar
     */
    private $city;

    /**
     * @var postal
     * @access private
     * @vartype: varchar
     */
    private $postal;

    /**
     * @var state
     * @access private
     * @vartype: tinyint
     */
    private $state;

    /**
     * @var site_category
     * @access private
     * @vartype: tinyint
     */
    private $site_category;

    /**
     * @var participantid
     * @access private
     * @vartype: tinyint
     */
    private $primary_address;
    private $lat;
    private $long;

    /**
     * @function getParticipantaddressid
     * @access public
     * @returns $participantaddressid integer
     * Getet Participant Address Id
     */
    public function getParticipantaddressid() {
        return $this->participantaddressid;
    }

    /**
     * @function setParticipantaddressid
     * @access public
     * @param $participantaddressid integer 
     * Set Participant Address Id
     */
    public function setParticipantaddressid($participantaddressid) {
        $this->participantaddressid = $participantaddressid;
    }

    /**
     * @function getParticipantdocsid
     * @access public
     * @returns $participantdocsid integer
     * Get participant Id
     */
    public function getParticipantid() {
        return $this->participantid;
    }

    /**
     * @function setParticipantid
     * @access public
     * @param $participantid integer 
     * Set Participant Id
     */
    public function setParticipantid($participantid) {
        $this->participantid = $participantid;
    }

    /**
     * @function getStreet
     * @access public
     * @returns $street integer
     * Get Street
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * @function setStreet
     * @access public
     * @param $street varchar 
     * Set Street
     */
    public function setStreet($street) {
        $this->street = $street;
    }

    /**
     * @function getStreet
     * @access public
     * @returns $city varchar
     * Get City
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * @function setCity
     * @access public
     * @param $city varchar 
     * Set Street
     */
    public function setCity($city) {
        $this->city = $city;
    }

    /**
     * @function getPostal
     * @access public
     * @returns $postal varchar
     * Get Postal
     */
    public function getPostal() {
        return $this->postal;
    }

    /**
     * @function setPostal
     * @access public
     * @param $postal varchar 
     * Set Postal
     */
    public function setPostal($postal) {
        $this->postal = $postal;
    }

    /**
     * @function getState
     * @access public
     * @returns $state tinyint
     * Get State
     */
    public function getState() {
        return $this->state;
    }

    /**
     * @function setState
     * @access public
     * @param $postal tinyint 
     * Set State
     */
    public function setState($state) {
        $this->state = $state;
    }

    /**
     * @function getSiteCategory
     * @access public
     * @returns $site_category tinyint
     * Get Site Category
     */
    public function getSiteCategory() {
        return $this->site_category;
    }

    /**
     * @function setSiteCategory
     * @access public
     * @param $postal tinyint 
     * Set Site Category
     */
    public function setSiteCategory($siteCategory) {
        $this->site_category = $siteCategory;
    }

    /**
     * @function getPrimaryAddress
     * @access public
     * @returns $primary_address tinyint
     * Get Primary Address
     */
    public function getPrimaryAddress() {
        return $this->primary_address;
    }

    /**
     * @function setPrimaryAddress
     * @access public
     * @param $primary_address tinyint 
     * Set Primary Address
     */
    public function setPrimaryAddress($primaryAddress) {
        $this->primary_address = $primaryAddress;
    }

    public function getLat() {
        return $this->lat;
    }

    public function setLat($lat) {
        $this->lat = $lat;
    }

    public function getLong() {
        return $this->long;
    }

    public function setLong($lat) {
        $this->long = $lat;
    }

    function setLatLongWithGenrate() {
        $CI = & get_instance();
        $result = $CI->basic_model->get_row('state', array('name'), $where = array('id' => $this->state));

        $address = $this->street . ' ' . $this->city . ' ' . $result->name;
        $latLong = getLatLong($address);

        if (!empty($latLong)) {
            $this->setLat($latLong['lat']);
            $this->setLong($latLong['long']);
        }
    }

    public function deletePreviousAddress() {
        $CI = & get_instance();
        $CI->basic_model->delete_records('participant_address', $where = array('participantId' => $this->participantid));
    }

    function createAddress() {
        $CI = & get_instance();
        $address = array('street' => $this->street, 'city' => $this->city, 'postal' => $this->postal, 'state' => $this->state, 'site_category' => $this->site_category, 'primary_address' => $this->primary_address, 'lat' => $this->lat, 'long' => $this->long, 'participantId' => $this->participantid);

        return $CI->basic_model->insert_records('participant_address', $address, $multiple = FALSE);
    }

}
