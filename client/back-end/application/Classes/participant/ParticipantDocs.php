<?php

namespace ParticipantDocClass;
/*
 * Filename: ParticipantDocs.php
 * Desc: Docs details of participant like filename, type etc.
 * @author YDT <yourdevelopmentteam.com.au>
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Class: ParticipantDocs
 * Desc: This Class is for maintaining docs details of participant.
 * Created: 02-08-2018 
*/

class ParticipantDocs
 {
    /**
     * @var participantdocsid
     * @access private
     * @vartype: int
     */
    private $participantdocsid;

    /**
     * @var participantid
     * @access private
     * @vartype: int
     */
    private $participantid;

    /**
     * @var type
     * @access private
     * @vartype: tinyint
     */
    private $type;

    /**
     * @var title
     * @access private
     * @vartype: varchar
     */
    private $title;
	
    /**
     * @var filename
     * @access private
     * @vartype: varchar
     */
    private $filename;

    /**
     * @var created
     * @access private
     * @vartype: varchar
     */
    private $created;
	
	
    /**
     * @function getParticipantdocsid
	 * @access public
	 * @returns $oc_departments int
	 * Get Participant Docs Id
	 */
    public function getParticipantdocsid() {
        return $this->participantdocsid;
    }

    /**
	 * @function setParticipantdocsid
	 * @access public
	 * @param $participantdocsid tinyint 
	 * Set Participant Docs Id
	 */
    public function setParticipantdocsid($participantdocsid) {
        $this->participantdocsid = $participantdocsid;
    }

    /**
     * @function getParticipantid
	 * @access public
	 * @returns $participantid int
	 * Get Participant Id
	 */
    public function getParticipantid() {
        return $this->participantid;
    }

    /**
	 * @function setParticipantid
	 * @access public
	 * @param $participantid int 
	 * Set Participant Id
	 */
    public function setParticipantid($participantid) {
        $this->participantid = $participantid;
    }

    /**
     * @function getType
	 * @access public
	 * @returns $type tinyint
	 * Get Type
	 */
    public function getType() {
        return $this->type;
    }

    /**
	 * @function setType
	 * @access public
	 * @param $type tinyint 
	 * Set Type
	 */
    public function setType($type) {
        $this->type = $type;
    }
	
    /**
     * @function getTitle
	 * @access public
	 * @returns $title varchar
	 * Get Title
	 */
    public function getTitle() {
        return $this->title;
    }

    /**
	 * @function setTitle
	 * @access public
	 * @param $title varchar
	 * Set Title
	 */
    public function setTitle($title) {
        $this->title = $title;
    }
	
    /**
     * @function getFilename
	 * @access public
	 * @returns $filename varchar
	 * Get Filename
	 */
    public function getFilename() {
        return $this->filename;
    }

    /**
	 * @function setFilename
	 * @access public
	 * @param $filename varchar
	 * Set Filename
	 */
    public function setFilename($filename) {
        $this->filename = $filename;
    }

    /**
     * @function getCreated
	 * @access public
	 * @returns $created varchar
	 * Get Created
	 */
    public function getCreated() {
        return $this->created;
    }

    /**
	 * @function setCreated
	 * @access public
	 * @param $created varchar
	 * Set Created
	 */
    public function setCreated($created) {
        $this->created = $created;
    }
    
    public function UpdateProfileImage(){
		$CI = & get_instance();
        $CI->load->model('Participant_model');
        $getPassword = $CI->Participant_model->participant_update_profile_image($this);
	}
}
