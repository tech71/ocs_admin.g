<?php

/*
 * Filename: ParticipantKin.php
 * Desc: Participant Relation Details 
 * @author YDT <yourdevelopmentteam.com.au>
 */

namespace ParticipantKinBookerClass;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: Member
 * Desc: Variables and Methods for Participant Relation Details
 * Created: 02-08-2018
 */

class ParticipantKinBooker {

    /**
     * @var participantkinid
     * @access private
     * @vartype: integer
     */
    private $participantbookerid;

    /**
     * @var participantid
     * @access private
     * @vartype: integer
     */
    private $participantid;

    /**
     * @var firstname
     * @access private
     * @vartype: varchar
     */
    private $firstname;

    /**
     * @var lastname
     * @access private
     * @vartype: varchar
     */
    private $lastname;

    /**
     * @var relation
     * @access private
     * @vartype: varchar
     */
    private $relation;

    /**
     * @var phone
     * @access private
     * @vartype: varchar
     */
    private $phone;

    /**
     * @var email
     * @access private
     * @vartype: varchar
     */
    private $email;
    private $primary_kin;

    /**
     * @function getParticipantkinid
     * @access public
     * @returns $participantkinid integer
     * Get Participant Kin Id
     */
    public function getParticipantbookerid() {
        return $this->participantbookerid;
    }

    /**
     * @function setParticipantkinid
     * @access public
     * @param $street integer 
     * Set Participant Kin Id
     */
    public function setParticipantbookerid($participantbookerid) {
        $this->participantbookerid = $participantbookerid;
    }

    /**
     * @function getParticipantid
     * @access public
     * @returns $getParticipantid integer
     * Get Participant Id
     */
    public function getParticipantid() {
        return $this->participantid;
    }

    /**
     * @function setParticipantid
     * @access public
     * @param $participantid integer 
     * Set Participant Id
     */
    public function setParticipantid($participantid) {
        $this->participantid = $participantid;
    }

    /**
     * @function getFirstname
     * @access public
     * @returns $firstname varchar
     * Get Firstname
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * @function setFirstname
     * @access public
     * @param $firstname integer 
     * Set Firstname
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    /**
     * @function getLastname
     * @access public
     * @returns $firstname varchar
     * Get Lastname
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * @function setLastname
     * @access public
     * @param $lastname varchar 
     * Set Lastname
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    /**
     * @function getRelation
     * @access public
     * @returns $relation varchar
     * Get Relation
     */
    public function getRelation() {
        return $this->relation;
    }

    /**
     * @function setRelation
     * @access public
     * @param $relation varchar 
     * Set Relation
     */
    public function setRelation($relation) {
        $this->relation = $relation;
    }

    /**
     * @function getPhone
     * @access public
     * @returns $phone varchar
     * Get Phone
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * @function setPhone
     * @access public
     * @param $phone varchar 
     * Set Phone
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * @function getEmail
     * @access public
     * @returns $email varchar
     * Get Email
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @function setEmail
     * @access public
     * @param $email varchar 
     * Set Email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    public function setPrimaryKin() {
        return $this->primary_kin;
    }

    /**
     * @function setEmail
     * @access public
     * @param $email varchar 
     * Set Email
     */
    public function getPrimaryKin($type) {
        $this->primary_kin = $type;
    }

    public function deletePreviousBookers() {
        $CI = & get_instance();
        $CI->basic_model->delete_records('participant_booking_list', $where = array('participantId' => $this->participantid));
    }
    
    public function createBookerList(){
         $CI = & get_instance();
         $bookerList = array('firstname' => $this->firstname, 'lastname' => $this->lastname, 'phone' => $this->phone, 'email' => $this->email, 'relation' => $this->relation, 'participantId' => $this->participantid);
         return $CI->basic_model->insert_records('participant_booking_list', $bookerList, $multiple = FALSE);
    }

}
