<?php

use classRoles as adminRoles;

defined('BASEPATH') or exit('No direct script access allowed');
global $requestClient;

//class Master extends MX_Controller
class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'logs_lib'));
        $this->load->model('Participant_model');
        $this->load->helper(array('comman', 'auth'));
        $clientResponse = client_auth_status();
        if (!$clientResponse['status']) {
            echo json_encode($clientResponse);
            exit;
            //redirect('/participant/Auth/login');
        }
    }

    // ======================================== Select methods =========================================
    // Here to get participant info
    public function participant_info($ClientToken = null) {
        $client_id = get_client_id();
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objparticipant = new ParticipantClass\Participant();
        $objparticipant->setParticipantid($client_id);
        $response = $objparticipant->getParticipantInfo();
        $arrResponse = array('status' => true, 'data' => $response);
        echo json_encode($arrResponse);
    }

    public function get_participant_firstname($ClientToken = null) {
        $client_id = get_client_id();
        $response = $this->basic_model->get_row('participant', array('firstname'), $where = array('id' => $client_id));
        $response->profile_percent = get_profile_complete('PARTICIPANT', $client_id);
        $arrResponse = array('status' => true, 'data' => $response);
        echo json_encode($arrResponse);
    }

    public function get_participant_profile_image($ClientToken = null) {
        $client_id = get_client_id();
        $response = $this->basic_model->get_row('participant', array('profile_image', 'gender'), $where = array('id' => $client_id));

        $checkfile = PARTICIPANT_UPLOAD_PATH . $client_id . '/' . $response->profile_image;
        $arrResponse = array();
        if (file_exists($checkfile)) {
            $path = PARTICIPANT_PROFILE_PATH . $client_id . '/';
            $arrResponse = array('status' => true, 'data' => $path . $response->profile_image);
        } else {
            $path = '';
            if ($response->gender == 1) {
                $path = PARTICIPANT_PROFILE_PATH . '/boy_client_portal.svg';
            } else {
                $path = PARTICIPANT_PROFILE_PATH . '/girls_client_portal.svg';
            }
            $arrResponse = array('status' => true, 'data' => $path);
        }
        echo json_encode($arrResponse);
    }

    public function participant_preferences($ClientToken = null) {
        $client_id = get_client_id();
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objparticipant = new ParticipantClass\Participant();
        $objparticipant->setParticipantid($client_id);
        $response = $objparticipant->getParticipantPreferences();
        $arrResponse = array('status' => true, 'data' => $response);
        echo json_encode($arrResponse);
    }

    public function get_state() {
        $reqData = request_handler();
        $response = $this->basic_model->get_record_where('state', $column = array('name', 'id'), $where = array('archive' => 0));
        foreach ($response as $val) {
            $state[] = array('label' => $val->name, 'value' => $val->id);
        }
        echo json_encode(array('status' => true, 'data' => $state));
    }

    function get_suburb() {
        $suburb = $this->input->get('query');
        $state = $this->input->get('state');
        $rows = $this->Participant_model->get_suburb($suburb, $state);
        echo json_encode($rows);
    }

    function get_shift_requirement_old() {
        $response = $this->basic_model->get_record_where('shift_requirement', $column = array('name', 'id'), $where = array('archive' => 0));
        foreach ($response as $val) {
            $state['shift_data'][] = array('label' => $val->name, 'value' => $val->id);
        }
        $response_org = $this->basic_model->get_record_where('shift_org_requirement', $column = array('name', 'id'), $where = array('archive' => 0));
        foreach ($response_org as $val) {
            $state['org_shift_data'][] = array('label' => $val->name, 'value' => $val->id);
        }
        echo json_encode(array('status' => true, 'data' => $state));
    }

    function get_shift_requirement() {
        $client_id = get_client_id();

        require_once APPPATH . 'Classes/participant/ParticipantShift.php';
        $objparticipantShitf = new ShiftClass\Shift();
        $objparticipantShitf->setParticipantid($client_id);
        $response = $objparticipantShitf->getShiftRequirement();
        echo json_encode(array('status' => true, 'data' => $response));
    }

    public function get_member_name() {
        $post_data = $this->input->get('query');
        $rows = $this->Participant_model->get_member_name($post_data);
        echo json_encode($rows);
    }

    // ======================================== Select methods =========================================
    // ======================================== Update methods =========================================

    function update_participant_kin_details() {
        // Done Server side validation
        $reqData = request_handler();
        $client_id = get_client_id();
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objparticipant = new ParticipantClass\Participant();

        $response_valid = $this->validate_kindetails($reqData);

        if ($response_valid['status']) {
            $kinRequestDetails = $reqData->kinDetails;
            if (!empty($kinRequestDetails)) {
                $objparticipant->setParticipantid($client_id);
                $objparticipant->setKinDetails($kinRequestDetails);
                $objparticipant->UpdateParticipantKinDetails();
                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false, 'error' => 'No kinInfo found'));
            }
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_participant_bookinginfo() {
        $reqData = request_handler();
        $client_id = get_client_id();
        $participant_data = array();
        // fill kin data 
        require_once APPPATH . 'Classes/participant/ParticipantKinBooker.php';
        require_once APPPATH . 'Classes/participant/Participant.php';
        $objparticipant = new ParticipantClass\Participant();
        $objparticipant->setParticipantid($client_id);
        $kinDetails = array();
        $kinRequestDetails = $reqData->kinBookerInfo;
        $response_valid = $this->validate_BookerInfodetails($reqData);
        if ($response_valid['status']) {

            // $objparticipantKin->deletePreviousBookers();
            if (!empty($reqData->kinBookerInfo)) {
                foreach ($kinRequestDetails as $kinInfo) {
                    $objparticipantKin = new ParticipantKinBookerClass\ParticipantKinBooker();
                    $objparticipantKin->setParticipantid($client_id);
                    $objparticipantKin->setFirstname($kinInfo->firstname);
                    $objparticipantKin->setLastname($kinInfo->lastname);
                    $objparticipantKin->setRelation($kinInfo->relation);
                    $objparticipantKin->setPhone($kinInfo->phone);
                    $objparticipantKin->setEmail($kinInfo->email);

                    if (!empty($kinInfo->kinbookingId)) {
                        $objparticipantKin->setParticipantbookerid($kinInfo->kinbookingId);
                    }
                    //$objparticipantKin->createBookerList();
                    $kinDetails[] = $objparticipantKin;
                }
                $objparticipant->setKinBookerDetails($kinDetails);
                $objparticipant->UpdateParticipantKinBookerDetails();

                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => true, 'error' => 'No kinInfo found'));
            }
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_participant_info() {
        $reqData = request_handler();
        $client_id = get_client_id();
        $participant_data = array();
        $participant_data['firstname'] = $reqData->profile->firstname;
        $participant_data['lastname'] = $reqData->profile->lastname;
        $participant_data['middlename'] = $reqData->profile->middlename;
        $participant_data['Preferredndis'] = $reqData->profile->NDISNO;
        $participant_data['Preferredcontacttype'] = $reqData->profile->PreferredContact;
        $participant_data['PreferredName'] = $reqData->profile->preferredname;
        $participant_data['Dob'] = $reqData->profile->dob;
        $participant_data['Preferredcrn'] = $reqData->profile->CRN;
        $participant_data['Preferredmedicare'] = $reqData->profile->medicare;
        $participant_data['gender'] = $reqData->profile->gender;
        $participant_data['email'] = $reqData->emails;
        $participant_data['phone'] = $reqData->phones;
        $response_valid = $this->validate_participant_data($participant_data);
        if ($response_valid['status']) {
            // fill kin data 
            require_once APPPATH . 'Classes/participant/ParticipantKin.php';
            $kinDetails = array();
            $kinRequestDetails = $reqData->kinInfo;
            foreach ($kinRequestDetails as $kinInfo) {

                $objparticipantKin = new ParticipantKinClass\ParticipantKin();
                $objparticipantKin->setParticipantkinid($kinInfo->kinId);
                $objparticipantKin->setParticipantid($client_id);
                $objparticipantKin->setFirstname($kinInfo->firstname);
                $objparticipantKin->setLastname($kinInfo->lastname);
                $objparticipantKin->setRelation($kinInfo->relation);
                $objparticipantKin->setPhone($kinInfo->phone);
                $objparticipantKin->setEmail($kinInfo->email);
                $objparticipantKin->setPrimaryKin($kinInfo->primary_kin);
                $kinDetails[] = $objparticipantKin;
            }
            // fill address data 
            require_once APPPATH . 'Classes/participant/ParticipantAddress.php';
            $kinAddressDetails = array();
            for ($i = 1; $i < 6; $i++) {
                $objparticipantAdd = new classParticipantAddress\ParticipantAddress();
                $objparticipantAdd->setParticipantaddressid('0');
                $objparticipantAdd->setParticipantid($client_id);
                $objparticipantAdd->setStreet('Street ' . $i);
                $objparticipantAdd->setCity('City ' . $i);
                $objparticipantAdd->setPostal('Postel' . $i);
                $objparticipantAdd->setState('12' . $i);
                $objparticipantAdd->setSiteCategory($i);
                $objparticipantAdd->setPrimaryAddress(2);
                $kinAddressDetails[] = $objparticipantAdd;
            }
            // create participant class object
            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            $objparticipant->setFirstname($participant_data['firstname']);
            $objparticipant->setMiddlename($participant_data['middlename']);
            $objparticipant->setLastname($participant_data['lastname']);
            $objparticipant->setDob($participant_data['Dob']);
            $objparticipant->setCrnNum($participant_data['Preferredcrn']);
            $objparticipant->setNdisNum($participant_data['Preferredndis']);
            $objparticipant->setMedicareNum($participant_data['Preferredmedicare']);
            $objparticipant->setContactType($participant_data['Preferredcontacttype']);
            $objparticipant->setGender($participant_data['gender']);
            $objparticipant->setPreferredname($participant_data['PreferredName']);
            $objparticipant->setParticipantPhone($participant_data['phone']);
            $objparticipant->setParticipantEmail($participant_data['email']);
            $objparticipant->setParticipantAddresse($kinAddressDetails);
            $objparticipant->setKinDetails($kinDetails);
            $reponse = $objparticipant->UpdateParticipant();
            echo json_encode(array('status' => true, 'data' => $reponse));
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_participant_email_and_phone() {
        $reqData = request_handler();
        $client_id = get_client_id();

        $response_valid = $this->validate_Phonedetails($reqData);
        if ($response_valid['status']) {

            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            $objparticipant->setParticipantPhone($reqData->phones);
            $objparticipant->setParticipantEmail($reqData->emails);
            //$objparticipant->UpdateParticipantPhone();
            $objparticipant->UpdateParticipantPhoneEmail();
            $arrResponse = array('status' => true);
            echo json_encode($arrResponse);
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_participant_profile() {
        // Done server side Validation
        $reqData = request_handler();
        $client_id = get_client_id();
        /* start */
        $participant_data = array();
        $participant_data['firstname'] = $reqData->firstname;
        $participant_data['lastname'] = $reqData->lastname;
        $participant_data['middlename'] = $reqData->middlename;
        $participant_data['Preferredndis'] = $reqData->NDISNO;
        $participant_data['Preferredcontacttype'] = $reqData->PreferredContact;
        $participant_data['PreferredName'] = $reqData->preferredname;
        $participant_data['Dob'] = $reqData->dob;
        $participant_data['Preferredcrn'] = $reqData->CRN;
        $participant_data['Preferredmedicare'] = $reqData->medicare;
        $participant_data['gender'] = $reqData->gender;
        $response_valid = $this->validate_participant_data($participant_data);
        if ($response_valid['status']) {

            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            $objparticipant->setFirstname($reqData->firstname);
            $objparticipant->setMiddlename($reqData->middlename);
            $objparticipant->setLastname($reqData->lastname);
            $objparticipant->setDob(date('Y-m-d', strtotime($reqData->dob)));
            $objparticipant->setCrnNum($reqData->CRN);
            $objparticipant->setNdisNum($reqData->NDISNO);
            $objparticipant->setMedicareNum($reqData->medicare);
            $objparticipant->setContactType($reqData->PreferredContact);
            $objparticipant->setGender($reqData->gender);
            $objparticipant->setPreferredname($reqData->preferredname);
            $reponse = $objparticipant->UpdateParticipantProfile();
            echo json_encode(array('status' => true, 'data' => $reponse));
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_care_requirements() {
        $reqData = request_handler();
        $client_id = get_client_id();
        $arrRequireAssistance = array();
        $arrSupportRequire = array();
        $arrOCService = array();
        $arrMobilityService = array();

        $arrOCService = $reqData->OcServices;
        $arrSupportRequire = $reqData->Support_Required;
        $arrRequireAssistance = $reqData->Assistance_Services;
//        $arrMobilityService = $reqData->mobility_services;


        $participant_data = array();
        $participant_data['care_requirment_id'] = $reqData->ParticipantCare->care_id;
        $participant_data['language_interpreter'] = $reqData->ParticipantCare->language_interpreter;
        $participant_data['hearing_interpreter'] = $reqData->ParticipantCare->hearing_interpreter;
        $participant_data['preferred_language'] = $reqData->ParticipantCare->preferred_language;

        $response_valid = $this->validate_participant_Caredata($reqData);
        if ($response_valid['status']) {
            require_once APPPATH . 'Classes/participant/ParticipantCareRequirement.php';
            $objparticipantCare = new classParticipantCareRequirement\ParticipantCareRequirement();
            $objparticipantCare->setParticipantcarerequirementid($participant_data['care_requirment_id']);
            $objparticipantCare->setParticipantid($client_id);
            $objparticipantCare->setPreferredLanguage($participant_data['preferred_language']);
            $objparticipantCare->setLinguisticInterpreter($participant_data['language_interpreter']);
            $objparticipantCare->setHearingInterpreter($participant_data['hearing_interpreter']);
            // Set array property
            $objparticipantCare->setRequireAssistanceOther($arrRequireAssistance);
            $objparticipantCare->setSupportRequireOther($arrSupportRequire);
            $objparticipantCare->setOCServices($arrOCService);
//            $objparticipantCare->setMobilityAssistance($arrMobilityService);
            $response = $objparticipantCare->UpdateCareRequirementInfo();
            echo json_encode(array('status' => true));
        } else {
            echo json_encode($response_valid);
        }
    }

    function update_participant_activity() {
        $reqData = request_handler();
        $client_id = get_client_id();
        if (!empty($reqData)) {
            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            // $objparticipant->setParticipantPlace($places);
            $objparticipant->setParticipantActivity($reqData);
            $response = $objparticipant->UpdateParticipantPreferencesInfo();
            $array = array("status" => true);
            echo json_encode($array);
        } else {
            echo json_encode(array('status' => false, 'error' => 'No Activity info'));
        }
    }

    function update_participant_places() {
        $reqData = request_handler();
        $client_id = get_client_id();
        if (!empty($reqData)) {
            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            $objparticipant->setParticipantPlace($reqData);

            $response = $objparticipant->UpdateParticipantPreferencesPlacesInfo();
            echo json_encode(array("status" => true));
        } else {
            echo json_encode(array('status' => false, 'error' => 'No places info'));
        }
    }

    function participant_change_password() {
        $reqData = request_handler();
        $client_id = get_client_id();
        $ArrRequest = array();
        $ArrRequest['newpassword'] = $reqData->newpassword;
        $ArrRequest['confirm_password'] = $reqData->confirmpassword;
        $ArrRequest['password'] = $reqData->password;
        $response = array();
        $responsevalidate = $this->validate_password($ArrRequest);
        if ($responsevalidate['status']) {
            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);
            $objparticipant->setClientPassword($ArrRequest['password']);
            $checkPasswordResponse = $objparticipant->CheckPassword();
            if ($checkPasswordResponse) {
                if ($ArrRequest['password'] == $ArrRequest['newpassword']) {
                    $response = json_encode(array('status' => false, 'error' => system_msgs('new_password_not_same_as_old_password')));
                } else {
                    $password_encrpt = password_encrpt($ArrRequest['newpassword']);
                    $objparticipant->setClientPassword($password_encrpt);
                    $responseChangePassword = $objparticipant->ChangePassword();
                    if ($responseChangePassword) {
                        $response = json_encode(array('status' => true, 'success' => system_msgs('password_reset_successfully')));
                    } else {
                        $response = json_encode(array('status' => true, 'success' => system_msgs('something_went_wrong')));
                    }
                }
            } else {
                $response = json_encode(array('status' => false, 'error' => system_msgs('password_not_match')));
            }
        } else {
            $response = json_encode($responsevalidate);
        }
        echo $response;
    }

    public function update_participant_address() {
        $reqData = request_handler();
        $client_id = get_client_id();

        $response_valid = $this->validate_Addressdetails($reqData);
        if ($response_valid['status']) {

            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();
            $objparticipant->setParticipantid($client_id);

            // fill address data        
            require_once APPPATH . 'Classes/participant/ParticipantAddress.php';
            $AddressDetails = array();
            if (!empty($reqData->address)) {
                foreach ($reqData->address as $key => $val) {
                    $objparticipantAdd = new classParticipantAddress\ParticipantAddress();
                    $objparticipantAdd->setParticipantid($client_id);
                    $objparticipantAdd->setStreet($val->street);
                    $objparticipantAdd->setCity($val->city->value);
                    $objparticipantAdd->setPostal($val->postal);
                    $objparticipantAdd->setState($val->state);
                    $objparticipantAdd->setSiteCategory($val->site_category);
                    $objparticipantAdd->setLatLongWithGenrate();
                    if ($key == 0) {
                        $objparticipantAdd->setPrimaryAddress(1);
                    } else {
                        $objparticipantAdd->setPrimaryAddress(2);
                    }
                    $AddressDetails[] = $objparticipantAdd;
                    // $objparticipantAdd->createAddress();
                }
                $objparticipant->setParticipantAddresse($AddressDetails);
                $objparticipant->UpdateParticipantAddress();

                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false, 'error' => 'No Addresses found'));
            }
        } else {
            echo json_encode($response_valid);
        }
    }

    // ======================================== Update methods =========================================
    // ======================================== Delete methods =========================================
    public function remove_participant_account() {
        $reqData = request_handler();
        $client_id = get_client_id();
        if ($reqData->RemoveReason) {
            require_once APPPATH . 'Classes/participant/ParticipantRemoveAccount.php';
            $objparticipantRemove = new ParticipantRemoveClass\ParticipantRemoveAccount();

            //$objparticipantRemove->setParticipantremoveaccountid($participant_goal_data['title']);
            $objparticipantRemove->setParticipantid($client_id);
            $objparticipantRemove->setReason($reqData->RemoveReason);
            $objparticipantRemove->setContact($reqData->iscontact);
            $response = $objparticipantRemove->participant_RemoveAccount();

            // archive upcoming shift when participant no more
            $objparticipantRemove->archiveUpcomingShift();

            if ($response) {
                echo json_encode(array('status' => true));
            } else {
                echo json_encode(array('status' => false));
            }
        } else {
            echo json_encode(array('status' => false, 'error' => 'No Reason found'));
        }
    }

    // ======================================== Delete methods =========================================
    // ======================================== Validation methods =========================================
    // Here to validate participant information data
    function validate_participant_Caredata($participant_caredata) {

        $participant_caredata = (array) $participant_caredata;
        try {
            $validation_rules = array(
                array('field' => 'AssistanceServices', 'label' => 'Language Interpreter', 'rules' => 'callback_check_assistance_services[' . json_encode($participant_caredata['Assistance_Services']) . ']'),
                array('field' => 'ParticipantCare[]', 'label' => 'Participant Care', 'rules' => 'callback_check_participantcare'),
                array('field' => 'Oc_Services', 'label' => 'OC Services', 'rules' => 'callback_check_ocServices[' . json_encode($participant_caredata['OcServices']) . ']'),
                array('field' => 'SupportRequired', 'label' => 'Support Required', 'rules' => 'callback_check_support_required[' . json_encode($participant_caredata['Support_Required']) . ']'),
//                array('field' => 'mobilityservices', 'label' => 'Mobility services', 'rules' => 'callback_check_mobility_services[' . json_encode($participant_caredata['mobility_services']) . ']')
            );
            $this->form_validation->set_data($participant_caredata);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function validate_kindetails($participantKinData) {
        $participantKinData = (array) $participantKinData;
        try {
            $validation_rules = array(
                array('field' => 'kinDetails[]', 'label' => 'kinDetails', 'rules' => 'callback_check_kin_details')
            );
            $this->form_validation->set_data($participantKinData);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    public function check_kin_details($kin_detailObject) {
        $this->load->helper('email');
        $kin_details = (array) $kin_detailObject;

        if (!empty($kin_details)) {

            $phone_number = trim($kin_details['phone']);
            $checkNumeric = $this->numeric_dash($phone_number);
            $phone_length = strlen($phone_number);

            if (empty($kin_details['relation'])) {
                $this->form_validation->set_message('check_kin_details', 'kin relation can not be empty');
                return false;
            } elseif (empty($phone_number)) {
                $this->form_validation->set_message('check_kin_details', 'kin phone can not be empty');
                return false;
            } elseif (empty($kin_details['email'])) {
                $this->form_validation->set_message('check_kin_details', 'kin email can not be empty');
                return false;
            } elseif (empty($kin_details['firstname'])) {
                $this->form_validation->set_message('check_kin_details', 'kin firname can not be empty');
                return false;
            } elseif (empty($kin_details['lastname'])) {
                $this->form_validation->set_message('check_kin_details', 'kin lastname can not be empty');
                return false;
            } elseif (!filter_var($kin_details['email'], FILTER_VALIDATE_EMAIL)) {
                $this->form_validation->set_message('check_kin_details', 'kin email not valid');
                return false;
            } elseif ($phone_length > 20) {
                $this->form_validation->set_message('check_kin_details', 'Phone number not greater then 20 digit');
                return false;
            } elseif (!$checkNumeric) {
                $this->form_validation->set_message('check_kin_details', 'Not a valid phone number');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_kin_details', 'Kin details empty');
            return false;
        }
        return true;
    }

    function validate_BookerInfodetails($participantBooker) {
        $participantBooker = (array) $participantBooker;
        try {
            $validation_rules = array(
                array('field' => 'kinBookerInfo[]', 'label' => 'Address', 'rules' => 'callback_check_booker_details')
            );
            $this->form_validation->set_data($participantBooker);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    public function check_booker_details($booker_detailObject) {
        $booker_details = (array) $booker_detailObject;
        if (!empty($booker_details)) {

            $phone_number = trim($booker_details['phone']);
            $checkNumeric = $this->numeric_dash($phone_number);
            $phone_length = strlen($phone_number);

            if (empty($booker_details['relation'])) {
                $this->form_validation->set_message('check_booker_details', 'Booker relation can not be empty');
                return false;
            } elseif (empty($booker_details['phone'])) {
                $this->form_validation->set_message('check_booker_details', 'Booker phone can not be empty');
                return false;
            } elseif (empty($booker_details['email'])) {
                $this->form_validation->set_message('check_booker_details', 'Booker email can not be empty');
                return false;
            } elseif (empty($booker_details['firstname'])) {
                $this->form_validation->set_message('check_booker_details', 'Booker first name can not be empty');
                return false;
            } elseif (empty($booker_details['lastname'])) {
                $this->form_validation->set_message('check_booker_details', 'Booker last name can not be empty');
                return false;
            } elseif (!filter_var($booker_details['email'], FILTER_VALIDATE_EMAIL)) {
                $this->form_validation->set_message('check_booker_details', 'Email not valid');
                return false;
            } elseif ($phone_length > 20) {
                $this->form_validation->set_message('check_booker_details', 'Phone number not greater then 20 digit');
                return false;
            } elseif (!$checkNumeric) {
                $this->form_validation->set_message('check_booker_details', 'Not a valid phone number');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_booker_details', 'Booker details empty');
            return false;
        }
        return true;
    }

    function validate_Addressdetails($participantAddress) {
        try {
            $validation_rules = array(
                array('field' => 'address[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address')
            );
            $this->form_validation->set_data((array) $participantAddress);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_participant_address($address) {
        if (!empty($address)) {
            if (empty($address->street)) {
                $this->form_validation->set_message('check_participant_address', 'participant primary address can not be empty');
                return false;
            }
            if (empty($address->city->value)) {
                $this->form_validation->set_message('check_participant_address', 'participant address city can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_participant_address', 'participant state can not be empty');
                return false;
            } elseif (empty($address->postal)) {
                $this->form_validation->set_message('check_participant_address', 'participant pincode can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participant_address', 'participant address can not empty');
            return false;
        }
        return true;
    }

    function validate_password($pass) {
        try {
            $validation_rules = array(
                array('field' => 'password', 'label' => 'Password', 'rules' => 'required'),
                array('field' => 'newpassword', 'label' => 'New Password', 'rules' => 'required'),
                array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'required|matches[newpassword]'),
            );
            $this->form_validation->set_data($pass);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode('<br>', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function validate_Phonedetails($participantphones) {
        try {
            $validation_rules = array(
                array('field' => 'phones[]', 'label' => 'Phone', 'rules' => 'callback_check_participant_number'),
                array('field' => 'emails[]', 'label' => 'Email', 'rules' => 'callback_check_participant_email_address')
            );
            $this->form_validation->set_data((array) $participantphones);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_participant_number($phone_numbers) {
        if (!empty($phone_numbers)) {

            $phone_number = trim($phone_numbers->phone);
            $phone_length = strlen($phone_number);
            if (empty($phone_number)) {
                $this->form_validation->set_message('check_participant_number', 'participant phone number can not be empty');
                return false;
            }
            if ($phone_length > 16) {
                $this->form_validation->set_message('check_participant_number', 'Phone number not greater then 20 digit');
                return false;
            }
//            $checkNumeric = $this->numeric_dash($phone_number);
//            if (!$checkNumeric) {
//                $this->form_validation->set_message('check_participant_number', 'Not a valid phone number');
//                return false;
//            }
        } else {
            $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
            return false;
        }
        return true;
    }

    function numeric_dash($num) {
        return (!preg_match("/^([0-9-\s])+$/D", $num)) ? FALSE : TRUE;
    }

    function check_participant_email_address($email_address) {
        $this->load->helper('email');
        if (!empty($email_address)) {
            if (empty($email_address->email)) {
                $this->form_validation->set_message('check_participant_email_address', 'participant email address can not be empty');
                return false;
            } elseif (!filter_var($email_address->email, FILTER_VALIDATE_EMAIL)) {
                $this->form_validation->set_message('check_participant_email_address', 'participant email not valid');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participant_email_address', 'email address can not be empty');
            return false;
        }
        return true;
    }

    function validate_participant_data($participant_data) {
        try {
            $validation_rules = array(
                array('field' => 'firstname', 'label' => 'Firs name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last name', 'rules' => 'required'),
                array('field' => 'Preferredcontacttype', 'label' => 'Preferred contact type', 'rules' => 'required'),
                //array('field' => 'Dob', 'label' => 'Dob', 'rules' => 'required'),
                //array('field' => 'Preferredndis', 'label' => 'Preferred ndis', 'rules' => 'required'),
                //array('field' => 'Preferredmedicare', 'label' => 'Medicare No', 'rules' => 'required'),
                //array('field' => 'Preferredcrn', 'label' => 'CRN No', 'rules' => 'required')
            );
            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_assistance_services($assistanceservices_optinal, $assistance_services_main) {

        $Assistance_services = json_decode($assistance_services_main);
        if (!empty($Assistance_services)) {
            $checked = false;
            foreach ($Assistance_services as $AssistanceServices) {
                if ($AssistanceServices->service_id == 1) {
                    $checked = true;
                }
            }
            if ($checked) {
                
            } else {
                $this->form_validation->set_message('check_assistance_services', 'Select at least one assistance service');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_assistance_services', 'participant assistance service can not empty');
            return false;
        }
        return true;
    }

    function check_ocServices($OCServicesoptional, $OCServicesMain) {
        $OCServices = json_decode($OCServicesMain);
        if (!empty($OCServices)) {
            $checked = false;
            foreach ($OCServices as $Services) {
                if ($Services->service_id == 1) {
                    $checked = true;
                }
            }
            if ($checked) {
                
            } else {
                $this->form_validation->set_message('check_ocServices', 'Select at least one OC Service');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_ocServices', 'participant OC Services can not empty');
            return false;
        }
        return true;
    }

    function check_participantcare($ParticipantCare) {
        $ParticipantCare = (array) $ParticipantCare;
        //print_r($ParticipantCare);			
        if (!empty($ParticipantCare)) {
            if (empty($ParticipantCare['care_id'])) {
                $this->form_validation->set_message('check_participantcare', 'participant care id can not be empty');
                return false;
            }
            if (empty($ParticipantCare['hearing_interpreter'])) {
                $this->form_validation->set_message('check_participantcare', 'participant hearing interpreter can not be empty');
                return false;
            }
            if (empty($ParticipantCare['language_interpreter'])) {
                $this->form_validation->set_message('check_participantcare', 'participant language interpreter can not be empty');
                return false;
            }
            if (empty($ParticipantCare['preferred_language'])) {
                $this->form_validation->set_message('check_participantcare', 'participant preferred language can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participantcare', 'participant care can not empty');
            return false;
        }
        return true;
    }

    function check_support_required($Supportoptional, $support_requiredMain) {

        $support_required = json_decode($support_requiredMain);
        if (!empty($support_required)) {
            $checked = false;
            foreach ($support_required as $supportRequired) {
                if ($supportRequired->service_id == 1) {
                    $checked = true;
                }
            }
            if ($checked) {
                
            } else {
                $this->form_validation->set_message('check_support_required', 'Select at least one support required');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_support_required', 'participant support required can not empty');
            return false;
        }
        return true;
    }

    function check_mobility_services($mobilityservicesoptional, $mobility_services_main) {

        $mobility_services = json_decode($mobility_services_main);
        if (!empty($mobility_services)) {
            $checked = false;
            foreach ($mobility_services as $mobilityServices) {
                if ($mobilityServices->service_id == 1) {
                    $checked = true;
                }
            }
            if ($checked) {
                
            } else {
                $this->form_validation->set_message('check_mobility_services', 'Select at least one mobility services required');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_mobility_services', 'participant mobility services can not empty');
            return false;
        }
        return true;
    }

    // ======================================== Validation methods =========================================
}
