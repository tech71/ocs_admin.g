<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shift_roster_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getPreviousRoster($objRoster) {

        $tbl_participant_roster = TBL_PREFIX . 'participant_roster';
        $this->db->select(array('title', 'is_default', 'start_date', 'end_date', 'status', 'id'));

        $this->db->from($tbl_participant_roster);
        $where = array('participantId' => $objRoster->getParticipantid());

        $this->db->where($where);
        $this->db->where_in('status', array(1, 2, 3, 4));
        $result = $this->db->get()->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                if ($val->end_date == '0000-00-00 00:00:00') {
                    $val->end_date = '';
                }

                $val->status = (($val->status == 2) ? 'Pending' : (($val->status == 4) ? 'Denied' : 'Active'));
            }
        }

        return $result;
    }

    public function get_previous_shifts($participantID, $shiftIds) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';


        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".status");
        $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

        $this->db->select($select_column);
        $this->db->from($tbl_shift_participant);
        $this->db->join($tbl_shift, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');

        $this->db->where_in('shift_date', $shiftIds);
        $this->db->where_in($tbl_shift . '.status', array(1, 2, 3, 4, 5, 6, 7));
        $this->db->where('participantId', $participantID);




        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $response = $query->result_array();

        if (!empty($response)) {
            foreach ($response as $key => $val) {
                $response[$key]['shift_date'] = $val;
                $response[$key]['shift_date'] = date('d-m-Y', strtotime($val['shift_date']));

                $response[$key]['start_time'] = date('h:i a', strtotime($val['start_time']));
                $response[$key]['end_time'] = date('h:i a', strtotime($val['end_time']));
                $response[$key]['duration'] = ($val['duration']);
            }
        }

        return $response;
    }

    public function participant_upcoming_shifts($reqData, $participantId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $prev_date = date('Y-m-d');
        $default_date = $reqData->date;
        $month = date('m', strtotime($default_date));
        $year = date('Y', strtotime($default_date));

        $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".status");
        $dt_query = $this->db->select($select_column);
        $this->db->from($tbl_shift);
        $this->db->where(array($tbl_shift_participant . '.participantId' => $participantId));
        $this->db->where(array("MONTH(" . $tbl_shift . ".shift_date)" => $month, "YEAR(" . $tbl_shift . ".shift_date)" => $year,));
        $this->db->where_in($tbl_shift . '.status', array(1, 2, 5, 7));
        $this->db->where($tbl_shift . '.start_time >', $prev_date);
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participant . '.status = 1', 'left');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $cntUnconfirmed = 0;
        $cntConfirmed = 0;
        $cntCancelled = 0;
        $cntUnfilled = 0;
        $dataResult = $query->result();
        $shiftResponse = array();
        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {

                $val->title = '';
                $val->description = $val->shift_date;
                $val->start = $val->shift_date;
                $val->end = $val->shift_date;

                if ($val->status == 1) {
                    $cntUnfilled++;
                } elseif ($val->status == 2) {
                    $cntUnconfirmed++;
                } elseif ($val->status == 5) {
                    $cntCancelled++;
                } else {
                    $cntConfirmed++;
                }

                $shiftResponse[$val->id] = $val;
            }
        }

        $result = array('shifts' => $dataResult, 'shiftdetails' => $shiftResponse, 'unfilled' => $cntUnfilled, 'unconfirmed' => $cntUnconfirmed, 'confirmed' => $cntConfirmed, 'cancelled' => $cntCancelled);

        return $result;
    }

    function get_participant_shift_related_information($participantId) {
        $tbl_participant = TBL_PREFIX . 'participant';
        $tbl_participant_address = TBL_PREFIX . 'participant_address';
        $tbl_participant_email = TBL_PREFIX . 'participant_email';
        $tbl_participant_phone = TBL_PREFIX . 'participant_phone';


        $this->db->select(array($tbl_participant_address . '.street as address', $tbl_participant_address . '.city as suburb', $tbl_participant_address . '.postal', $tbl_participant_address . '.state', $tbl_participant_address . '.lat', $tbl_participant_address . '.long'));

        $this->db->select(array($tbl_participant . '.firstname', $tbl_participant . '.lastname', $tbl_participant . '.prefer_contact'));

        $this->db->select(array($tbl_participant_email . '.email', $tbl_participant_phone . '.phone'));

        $this->db->from($tbl_participant);
        $this->db->join($tbl_participant_address, $tbl_participant_address . '.participantId = ' . $tbl_participant . '.id', 'inner');
        $this->db->join($tbl_participant_email, $tbl_participant_email . '.participantId = ' . $tbl_participant . '.id AND ' . $tbl_participant_email . '.primary_email = 1', 'inner');
        $this->db->join($tbl_participant_phone, $tbl_participant_phone . '.participantId = ' . $tbl_participant . '.id AND ' . $tbl_participant_phone . '.primary_phone = 1', 'inner');


        $this->db->where($tbl_participant . '.id', $participantId);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());


        $response = $query->row();

        $response->requirement = $this->basic_model->get_record_where('participant_assistance', array('assistanceId'), $where = array('participantId' => $participantId, 'type' => 'assistance'));


        return $response;
    }

    function check_shift_date_time_already_exist($objShift, $participantId) {
        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';

        $this->db->select(array($tbl_shift . ".id"));
        $this->db->from($tbl_shift);

        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id', 'inner');

        $where = "participantId = " . $participantId . " AND shift_date = '" . $objShift->getShiftDate() . "' AND
 (('" . $objShift->getStartTime() . "' BETWEEN (start_time) AND (end_time))  or ('" . $objShift->getEndTime() . "' BETWEEN (start_time) AND (end_time)) OR
((start_time) BETWEEN '" . $objShift->getStartTime() . "' AND '" . $objShift->getEndTime() . "')  or ((end_time) BETWEEN '" . $objShift->getStartTime() . "' AND '" . $objShift->getEndTime() . "')) ";

//        if ($rosObj->getRosterId() > 0) {
//            $this->db->where('id !=', $rosObj->getRosterId());
//        }
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    function check_roster_start_end_date_already_exist($roster) {
        $roster = (object) $roster;
        $tbl_participant_roster = TBL_PREFIX . 'participant_roster';

        $this->db->select(array($tbl_participant_roster . ".id"));

        $this->db->from($tbl_participant_roster);

        $where = "is_default = 1 AND participantId = " . $roster->participantId . " AND 
(('" . $roster->start_date . "' BETWEEN date(start_date) AND date(end_date))  or ('" . $roster->end_date . "' BETWEEN date(start_date) AND date(end_date)) OR
(date(start_date) BETWEEN '" . $roster->start_date . "' AND '" . $roster->end_date . "')  or (date(end_date) BETWEEN '" . $roster->start_date . "' AND '" . $roster->end_date . "')) ";
        $this->db->where_in('status', array(1, 2));
        $this->db->where('is_default', 1);

//        if($roster->participantId > 0){
//            $this->db->where('id !=', $roster->participantId);
//        }
        $this->db->where($where);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->row();
    }

    function get_completed_shift_by_date($obj, $participantId) {


        $this->db->select(array("s.id as value", "concat(DATE_FORMAT(s.start_time, '%h:%i %p'), ' - ', DATE_FORMAT(s.end_time, '%h:%i %p')) as label"));
        $this->db->from('tbl_shift as s');

        $this->db->join('tbl_shift_participant as sp', 'sp.shiftId = s.id', 'inner');

        $this->db->where(array('s.status' => 6, 'sp.participantId' => $participantId, 'sp.status' => 1, 'date(s.start_time)' => $obj->getShiftDate()));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        
//        last_query();

        return $query->result_array();
    }

    function get_ongoing_feedback($obj) {
        $tbl_fms_case = TBL_PREFIX . 'fms_case';
        $this->db->select(array($tbl_fms_case . '.created', $tbl_fms_case . '.id', $tbl_fms_case . '.event_date'));
        $this->db->from($tbl_fms_case);
        $this->db->where(array($tbl_fms_case . '.initiated_by' => $obj->getInitiated_by(), $tbl_fms_case . '.initiated_type' => $obj->getInitiated_type(), 'status' => 0));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
        $array = array();
        foreach ($result as $key => $value) {
            $value->created = (strtotime(date('Y-m-d H:i:s')) - strtotime($value->created)) * 1000;
        }
        return $result;
    }

    function get_ongoing_feedback_old($obj) {
        $tbl_fms_case = TBL_PREFIX . 'fms_case';


        $this->db->select(array($tbl_fms_case . '.created', $tbl_fms_case . '.id'));
        $this->db->from($tbl_fms_case);



        $this->db->where(array($tbl_fms_case . '.initiated_by' => $obj->getInitiated_by(), $tbl_fms_case . '.initiated_type' => $obj->getInitiated_type(), 'status' => 2));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->result_array();
    }

    function get_MemberByShiftId($obj) {

        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_member = TBL_PREFIX . 'member';
        $this->db->select(array($tbl_shift_member . '.memberId AS value', "CONCAT($tbl_member.firstname,' ',$tbl_member.lastname) AS label"));
        $this->db->from($tbl_shift_member);
        $this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'inner');
        $this->db->where(array($tbl_shift_member . '.shiftId' => $obj->getShiftId()));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result_array();
    }

    function insert_fms_locations($objFMSLocation) {
        $tableFmsLocation = TBL_PREFIX . 'fms_case_location';
        $bulk_insert_location = array();

        foreach ($objFMSLocation as $FmsLocation) {
            $arrLocation = array();

            /* $objFMSLocation->setCaseId($caseId);
              $objFMSLocation->setAddress($location->address);
              $objFMSLocation->setSuburb($location->suburb->value);
              $objFMSLocation->setPostal($location->postal);
              $objFMSLocation->setState($location->state);
              $objFMSLocation->setCategoryId(0);
             */

            $arrLocation['caseId'] = $FmsLocation->getCaseId();
            $arrLocation['address'] = $FmsLocation->getAddress();
            $arrLocation['suburb'] = $FmsLocation->getSuburb();
            $arrLocation['state'] = $FmsLocation->getState();
            $arrLocation['postal'] = $FmsLocation->getPostal();
            $arrLocation['categoryId'] = $FmsLocation->getCategoryId();
            $bulk_insert_location[] = $arrLocation;
        }
        $this->bulk_record_opration($tableFmsLocation, 'insert', null, null, $bulk_insert_location);
    }

    public function get_ShiftRequirement($objParticipant) {

        $tbl_shift_requirment = TBL_PREFIX . 'shift_requirement';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select(array('name', 'id'));
        $this->db->from($tbl_shift_requirment);
        $this->db->where(array('archive' => 0));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->result();
        $state = array();
        foreach ($response as $val) {
            $state['shift_data'][] = array('label' => $val->name, 'value' => $val->id);
        }

        $this->db->select(array('shiftId'));
        $this->db->from($tbl_shift_participant);
        $this->db->where(array('participantId' => $objParticipant->getParticipantid()));
        $query_shift = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response_shift = $query_shift->result();

        if (count($response_shift) > 0) {

            $this->db->select(array($tbl_shift_member . '.memberId AS value', "CONCAT($tbl_member.firstname,' ',$tbl_member.lastname) AS label"));
            $this->db->distinct();
            $this->db->from($tbl_shift_member);
            $this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'inner');
            $this->db->join($tbl_shift_participant, $tbl_shift_member . '.shiftId = ' . $tbl_shift_participant . '.shiftId', 'inner');
            $this->db->where(array($tbl_member . '.archive' => 0));
            $this->db->where(array($tbl_member . '.status' => 1));
            $this->db->where(array($tbl_shift_participant . '.participantId' => $objParticipant->getParticipantid()));
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $response_shift_member = $query->result();
            $state['shift_member'] = $response_shift_member;
        } else {

            $this->db->select(array($tbl_member . '.id AS value', "CONCAT($tbl_member.firstname,' ',$tbl_member.lastname) AS label"));
            $this->db->distinct();
            $this->db->from($tbl_member);
            $this->db->where(array($tbl_member . '.archive' => 0));
            $this->db->where(array($tbl_member . '.status' => 1));

            //	$this->db->join($tbl_member, $tbl_shift_member . '.memberId = ' . $tbl_member . '.id', 'inner');			
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $response_shift_member = $query->result();
            $state['shift_member'] = $response_shift_member;
        }
        return $state;
    }

    public function get_ShiftDetailByShiftId($objShift) {

        $ParticipantId = $objShift->getParticipantid();
        $shiftId = $objShift->getShiftId();

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';
        $tbl_shift_member = TBL_PREFIX . 'shift_member';
        $tbl_shift_location = TBL_PREFIX . 'shift_location';
        $tbl_state = 'tbl_state';
        $tbl_member = TBL_PREFIX . 'member';

        $arr_result = array();

        // Shift Data
        $this->db->select(array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".status"));
        $this->db->from($tbl_shift);
        $where = array($tbl_shift . '.id' => $shiftId);
        $this->db->where($where);
        $query = $this->db->get();
        $arr_result['shift'] = $query->row();

        // Shift member data

        $this->db->select(array($tbl_shift_location . '.address', $tbl_shift_location . '.suburb', $tbl_shift_location . '.postal', $tbl_state . '.name as state'));
        $this->db->from($tbl_shift_location);
        $where = array($tbl_shift_location . '.shiftId' => $shiftId);
        $this->db->where($where);
        $this->db->join($tbl_state, $tbl_state . '.id = ' . $tbl_shift_location . '.state', 'inner');
        $query = $this->db->get();
        $arr_result['shift_location'] = $query->result();

        // Shift Member

        $this->db->select(array($tbl_shift_member . '.memberId', $tbl_shift_member . '.status', "concat(tbl_member.firstname, ' ', tbl_member.lastname ) as name"));
        $this->db->from($tbl_shift_member);
        $where = array($tbl_shift_member . '.shiftId' => $shiftId);
        $this->db->where($where);
        $this->db->join($tbl_member, $tbl_member . '.id = ' . $tbl_shift_member . '.memberId', 'inner');
        $query = $this->db->get();
        $arr_result['shift_member'] = $query->result();

        //$this->db->join($tbl_shift_member, $tbl_shift_member . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        //$this->db->join($tbl_shift_location, $tbl_shift_location . '.shiftId = ' . $tbl_shift . '.id', 'inner');
        //$where=array($tbl_shift . '.id' => $shiftId);
        // $this->db->where($where); 
        // $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //$arr_result=array();
        //$arr_result['data']=$query->row();
        return $arr_result;
    }

    function get_feedback_category_list() {
        $tbl_fms_case_category = TBL_PREFIX . 'fms_case_all_category';
        $this->db->select(array($tbl_fms_case_category . '.id as value', $tbl_fms_case_category . '.name as label'));
        $this->db->from($tbl_fms_case_category);
        $this->db->where(array($tbl_fms_case_category . '.archive' => 0));
        $this->db->order_by("name", "asc");
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    // =================== Private Methods ===========================
    private function bulk_record_opration($table, $type, $wherekey, $wherevalue, $memberData) {
        if ($type == 'insert') {
            $insert_query = $this->db->insert_batch($table, $memberData);
            $this->db->last_query();
            if ($this->db->affected_rows() > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            $this->db->where($wherekey, $wherevalue);
            $this->db->update($table, $memberData);
            $this->db->last_query();
        }
    }

}
