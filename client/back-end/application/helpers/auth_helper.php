<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include APPPATH . 'helpers/jwt_helper.php';

if (!function_exists('password_encrpt')) {

    function password_encrpt($string) {
        $encry_password = password_hash($string, PASSWORD_BCRYPT);
        return $encry_password;
    }

}
if (!function_exists('password_validate')) {

    function password_validate($pass_string, $hash_key) {
        //echo $pass_string.'-'.$hash_key;
        $response = false;

        //echo '-('.password_hash($pass_string, PASSWORD_BCRYPT).')-';

        if (password_verify($pass_string, $hash_key)) {
            $response = true;
        } else {
            $response = false;
        }
        return $response;
    }

}
if (!function_exists('generate_token')) {
    function generate_token($token_value) {
        $Obj_JWT = new \JWT();
        $JWT_Token = $Obj_JWT->encode($token_value, 'secret_server_key');
        return $JWT_Token;
    }

}

if (!function_exists('auth_login_status')) {

    function auth_login_status($login_detail) {
        $login_status = false;
        $CI = & get_instance();
        $CI->load->model('api_model');
        //$login_attempt=3;		 
        return $CI->api_model->member_login_status($login_detail);
    }

}
if (!function_exists('client_auth_status')) {

    function client_auth_status() {

        $request_body = file_get_contents('php://input');
        $request_body = json_decode($request_body);
        $client_token = $request_body->request_data->token;
        $client_id = $request_body->request_data->id;

        $CI = & get_instance();
        $CI->load->model('Participant_model');
        $response = array();
        $loginStatus = false;
        
        if (empty($client_id) || empty($client_token)) {
            return $response = array('status' => false, 'error' => system_msgs('token_mismatch'));
        } else {
            require_once APPPATH . 'Classes/participant/ParticipantLogin.php';
            $objparticipant = new ParticipantLoginClass\ParticipantLogin();
            $currunt_date = date("Y-m-d H:i:s");
            
            $objparticipant->setLoginUpdate($currunt_date);
            - $objparticipant->setParticipantToken($client_token);
            $objparticipant->setParticipantid($client_id);
            
            $response = $CI->Participant_model->participant_login_status($objparticipant);

            if ($response['type']) {
                $response = array('status' => true, 'error' => system_msgs('success_login'));
            } else {
                $response = array('status' => false, 'token_status' => true, 'error' => system_msgs('invalid_request'));
            }
        }
        return $response;
    }

}

if (!function_exists('client_auth_status_by_token')){

    function client_auth_status_by_token($client_token,$client_id) {      
        $CI = & get_instance();
        $CI->load->model('Participant_model');
        $response = array();
        $loginStatus = false;
        
        if (empty($client_id) || empty($client_token)) {
            return $response = array('status' => false, 'error' => system_msgs('token_mismatch'));
        } else {
            require_once APPPATH . 'Classes/participant/ParticipantLogin.php';
            $objparticipant = new ParticipantLoginClass\ParticipantLogin();
            $currunt_date = date("Y-m-d H:i:s");
            
            $objparticipant->setLoginUpdate($currunt_date);
            - $objparticipant->setParticipantToken($client_token);
            $objparticipant->setParticipantid($client_id);
            
            $response = $CI->Participant_model->participant_login_status($objparticipant);

            if ($response['type']) {
                $response = array('status' => true, 'error' => system_msgs('success_login'));
            } else {
                $response = array('status' => false, 'token_status' => true, 'error' => system_msgs('invalid_request'));
            }
        }
        return $response;
    }
}

if (!function_exists('isValidJson')) {

// YDT Methods
    function isValidJson($data = NULL) {
        if (!empty($data)) {
            @json_decode($data);
            return (json_last_error() === JSON_ERROR_NONE);
        }
        return false;
    }

}