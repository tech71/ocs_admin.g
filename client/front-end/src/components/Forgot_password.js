import React, { Component } from 'react';
import BlockUi from 'react-block-ui';

import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../config.js';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import  'react-block-ui/style.css';

class Forgot_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            error: '',
            loading: false,
            success: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({username: e.target.value});
        this.setState({error: ''});
        this.setState({success: ''});
    }

    onSubmit(e) {
        e.preventDefault();
        jQuery("#forget-password").validate();
        if (!this.state.loading && jQuery("#forget-password").valid()) {
            this.setState({loading: true})
            jQuery.ajax({
                url: BASE_URL + 'participant/auth/client_forget_password',
                type: 'POST',
                dataType: 'JSON',
                data: {username: this.state.username},
                success: function (data) {
                    if (data.status) {
                        this.setState({error: ''});
                        this.setState({success: data.success});
                    } else {
                        this.setState({error: data.error});
                    }
                    this.setState({loading: false});
                }.bind(this),
                error: function (xhr) {
                    this.setState({loading: false});
                }.bind(this)
            });
        }
    }

    componentDidMount() {

    }

    render() {
        return (
                <div>
    <BlockUi tag="div" blocking={this.state.loading}>

        <section>
            <div className="container gradient_color">
               <div className="row justify-content-md-center justify-content-sm-center">
            <div className="col-sm-11  col-md-7  col-lg-5">
                         <div className="logo text-center"><img className="img-fluid"  src={'/assets/images/logo_oncall.svg'} / ></div>
                        <div className="limiter">
                            <div className="login_1">
                                <div className="Smiley">
				<h1><span>Forgot Password&nbsp;<i className="icon icon-smail-big"></i></span></h1>
                                </div>
                                
                                <form id="forget-password" className="login100-form" >
                                        <div className="px-lg-4 px-md-3 px-sm-4 px-3 px-xl-5 pt-5">
                                        <div className="User d-flex">
                                           <span>
                                               {/* <img src="/assets/images/client/man.png" className="img-fluid align-self-center" alt="IMG"/> */}
                                               <i className="icon icon-userm1-ie logie_ic"></i>
                                            </span>
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" type="text" name="username" placeholder="Your username here" onChange={this.handleChange} value={this.state.username} data-rule-required="true" />
                                        </div>
                                        <div className="login_but">
                                            <button onClick={this.onSubmit} className="but_login orange">
                                                Request
                                            </button>
                                        </div>

                                        <div className="success_login">{this.state.success}</div>
                                        <div className="error_login">{this.state.error}</div>
                                    </div>
					<h5 className="col-md-12 text-center P_30_T text-center for_text"><Link to={ROUTER_PATH} >Login</Link></h5>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

            <footer className="text-center footer_1">
           <span>&#169;2018-All Right  Reserved <b>Healthcare Manager</b></span>
       </footer>
    </BlockUi>
</div>
                );
    }
}
export default Forgot_password;