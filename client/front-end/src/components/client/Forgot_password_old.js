import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import jQuery from "jquery";

import { ROUTER_PATH, BASE_URL } from '../../config.js';

class Forgot_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            error: '',
            loading: false,
            success: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({email: e.target.value});
        this.setState({error: ''});
        this.setState({success: ''});
    }

    onSubmit(e) {
        e.preventDefault();
        jQuery("#forget-password").validate();
        if (!this.state.loading && jQuery("#forget-password").valid()) {
            jQuery.ajax({
                url: BASE_URL + 'admin/Login/request_reset_password',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    email: this.state.email,
                },
                beforeSend: function () {
                    this.setState({loading: true});
                }.bind(this),
                success: function (data) {
                    if (data.status) {
                        this.setState({error: ''});
                        this.setState({success: data.success});
                    } else {
                        this.setState({error: data.error});
                    }
                    this.setState({loading: false});
                }.bind(this),
                error: function (xhr) {
                    this.setState({loading: false});
                }.bind(this)
            });
        }
    }

    componentDidMount() {

    }

    render() {
        return (
                <div>
                    <BlockUi tag="div" blocking={this.state.loading}>
        <link rel="stylesheet" type="text/css" href="assets/css/user_admin.css"/>
        <section>
            <div className="container">
                <div className="row">
			<div className="col-md-4 col-md-offset-4">
                        
                        <div className="logo text-center"><a href="#"><img className="img-fluid" width="70px" src="assets/images/user_admin/ocs_logo.svg"/></a></div>

                        <div className="limiter">
                            <div className="login_1">
                                <div className="Smiley">
				<h1>
									<span>Frogot Password&nbsp;<i className="icon icon-smail-big"></i></span> 
								</h1>
                                </div>
                                
                                <form id="forget-password" className="login100-form" >
                                        <div className="col-md-12">
                                        <div className="User d-flex">
                                           <span><img src="assets/images/user_admin/user_img.png" className="img-fluid align-self-center" alt="IMG"/></span>
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" type="email" name="Username" placeholder="Your Email Id here" onChange={this.handleChange} value={this.state.email} data-rule-required="true" />
                                        </div>
                                        <div className="login_but">
                                            <button onClick={this.onSubmit} className="but_login orange">
                                                Request
                                            </button>
                                        </div>

                                        <div className="success_login">{this.state.success}</div>
                                        <div className="error_login">{this.state.error}</div>
                                    </div>
					<h5 className="col-md-12 text-center P_30_T text-center for_text"><Link to={ROUTER_PATH} >Login</Link></h5>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

       <footer className="text-center">
                        <div className="container">
                            <div className="row">
                                <h6>© 2018 All Rights Reserved <span>Healthcare Manager</span></h6>
                            </div>
                        </div>
                    </footer>
    </BlockUi>
                </div>
                );
    }
}
export default Forgot_password;