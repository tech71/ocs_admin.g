import React, { Component } from 'react';
import jQuery from "jquery";
import {Panel, Button, ProgressBar, Modal} from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import {postData, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, getPercentage} from '../../service/common';
import '../../service/jquery.validate.js';

const WeekData = {Mon :{is_active: false}, 
                  Tue: {is_active: false}, 
                  Wed: {is_active: false}, 
                  Thu: {is_active: false}, 
                  Fri: {is_active: false}, 
                  Sat: {is_active: false}, 
                  Sun: {is_active: false} }
              
class RosterRequest extends Component {
    constructor(props) {
        super(props);

        this.state = {rosterList : [WeekData],
            start_time: '',
            end_time: '',
            booked_by: 2,
            modal_collapse_shift: false,
            collapseSelect: 2,
            collapse_shift: [],
            rosterId : false,
            update_disabled: true
        }
    }
    
    componentDidMount() {
        this.setState({modal_par_info: true, update_disabled: false, rosterList : [this.reInitializeObject(WeekData)]});
    }

    addRoster = (index, day) => {
        if(!this.state.update_disabled){
            var List = this.state.rosterList
            if(List[index][day]['is_active']){
                List[index][day]['is_active'] = false
            }else{
                this.setState({modal_show: true},  () => {
                    this.setState({start_time: null, end_time: null, index: index, day: day})

                })
            }

            this.setState({rosterList:List })
        }
    }
    
    saveDateTime = (index, day) => {
        jQuery('#saving_time').validate();
        
        if(jQuery('#saving_time').valid()){
            var List = this.state.rosterList
            List[index][day]['is_active'] = true
            List[index][day]['start_time'] = this.state.start_time
            List[index][day]['end_time'] = this.state.end_time
            this.setState({rosterList:List })
            this.closeModal();
        }
    }
    
    handleAddShareholder = (e, stateName, object_array) => {
        var state = {};
        var temp = object_array
        var list = this.state[stateName];

        state[stateName] = list.concat(this.reInitializeObject(WeekData));
        this.setState(state);
    }
    
    closeModal = () => {
         this.setState({modal_show: false})
    }
    
    reInitializeObject = (object_array) => {
        var state = {}
        Object.keys(object_array).forEach(function (key) {
            state[key] = {is_active: false};
        });
        return state;
    }
    
    selectChange = ( value, fieldName) => {
         var state = {};
         state[fieldName] = value;
         this.setState(state)
        
    }
    
    saveAndFinish = () => {
        this.setState({modal_show_type: false, modal_start_end_date: true})
        if(!this.state.rosterId){
            this.setState({start_date: '', end_time: ''})
        }
    }
    
    checkValidDay = () => {
         var status = false;
          var List = this.state.rosterList
          List.map((val, index) => {
              Object.keys(val).forEach(function(key, index) {
                    if(val[key].is_active){
                        status = true;
                    }
              })
          })
          
        toast.dismiss();  
        if(status){
            this.setState({modal_show_type: true})
        }else{
            toast.error('Please select at least one day', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }
    
    check_shift_collapse = () => {
         jQuery('#saving_roster').validate();
         toast.dismiss(); 
         this.setState({loading: true});
         if(jQuery('#saving_roster').valid()){
              postData('participant/Shift_Roster/check_collapse_shifts', this.state).then ((result) => {
                  if(result.status){
                        this.setState({collapse_shift: result.data},  () => {
                            if(result.count > 0 && this.state.is_default == 2){
                                this.setState({modal_collapse_shift: true})
                                this.setState({loading: false});
                            }else{
                                this.saveRoster();
                            }
                            this.setState({modal_start_end_date: false});
                        })
                  }else{
                      this.setState({loading: false});
                      toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                  }
             });
         }
    }
    
    checkboxResolveCollapse = (id, value, selectAll, old, oldIndex) => {
        var List = this.state.collapse_shift;
        
        // if want to select all
        if(selectAll){
            List.map((shift, id) => {
                List[id]['status'] = value;
                
               var NewOld = (value == 2)? false: true
               if(List[id]['old']){
                List[id]['old'].map((temp, tId) => {
                    List[id]['old'][tId]['active'] = NewOld;
                }) 
               }
            })
            this.setState({collapseSelect: value})
        }else if(old == 'old'){
            if(List[id]['old'][oldIndex]['active']){
                
                List[id]['old'][oldIndex]['active'] = false;
                var totl = false;
                
                List[id]['old'].map((temp, tId) => {
                    if(List[id]['old'][tId]['active'])
                        totl = true
                }) 
                
                if(!totl)
                    List[id]['status'] = 2;
                
                
            }else{
                 List[id]['status'] = false;
                 List[id]['old'][oldIndex]['active'] = true;
            }

            this.setState({collapseSelect: false})
        }else{
            List[id]['old'].map((temp, tId) => {
                List[id]['old'][tId]['active'] = false;
            }) 
            List[id]['status'] = value;
        }
        
        this.setState({collapse_shift: List});
    }
    
    saveRoster = (validate) => {
            this.setState({loading: true})
              postData('participant/Shift_Roster/create_roster', this.state).then ((result) => {
                  if(result.status){
                        toast.success('Roster create successfully', {
                           position: toast.POSITION.TOP_CENTER,
                           hideProgressBar: true
                       });
                       
                         setTimeout(() => this.setState({is_redirect: true, loading: false}), 2000);
                      
                      this.setState({modal_collapse_shift: false})
                  }else{
                      toast.error('Please select at least one day', {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                         this.setState({loading: false})
                  }
             });
       
    }

    render() {
        return (
                <div>
                    <Header title={'Request Roster'} />	
                    {(this.state.is_redirect)?  <Redirect to='/dashboard'  />: ''}
                   <section>
    <div className="container">
        <div className="row mt-3 mb-4">
            <div className="col-lg-12">				
               <div className="line"></div>   		
            	<div className="labels mt-2" align="right">
            		
            	</div>				
            </div>
             <div className="col-md-12 back-arrow"><Link to="/shift_roster"><i className="icon icon-back-arrow"></i></Link></div>
                        
	</div>
        <div className="row">

            <div className="col-lg-12">

                <div className="row px-3">
                    <span className="cal-main-div justify-content-md-center w-100">
                        <div className="col-lg-1 align-self-center text-center">
                            <i></i>
                        </div>
                        <div className="col-lg-10">
                            <h2 className="my-4 text-center">New Roster</h2>
                        </div>
                        <div className="col-lg-1 align-self-center text-center">
                            <i></i>
                        </div>

                        <div className="col-lg-11">
                            <div className="calendar-porta">
                            
                                    {this.state.rosterList.map((weekList, index) => (
                                            <div className="week-div">
                                          <SingleDay day="Mon" addRoster={this.addRoster} index={index} weekList={weekList['Mon']} />               
                                          <SingleDay day="Tue" addRoster={this.addRoster} index={index} weekList={weekList['Tue']} />               
                                          <SingleDay day="Wed" addRoster={this.addRoster} index={index} weekList={weekList['Wed']} />               
                                          <SingleDay day="Thu" addRoster={this.addRoster} index={index} weekList={weekList['Thu']} />               
                                          <SingleDay day="Fri" addRoster={this.addRoster} index={index} weekList={weekList['Fri']} />               
                                          <SingleDay day="Sat" addRoster={this.addRoster} index={index} weekList={weekList['Sat']} />               
                                          <SingleDay day="Sun" addRoster={this.addRoster} index={index} weekList={weekList['Sun']} />     


                                        {(this.state.update_disabled)?"": (this.state.rosterList.length == (index+1) && (this.state.rosterList.length != 4)) > 0 ? <button className="add_i_icon" onClick={(e) => this.handleAddShareholder(e, 'rosterList', weekList)}>
                                                    <i  className="icon icon-add-icons" ></i>
                                                </button> : <button className="button_unadd" onClick={(e) => handleRemoveShareholder(this, e, index, 'rosterList')}> 
                                                    <i  className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button>}              
                                    </div>
                                    ))}
                               
                            </div>
                        </div>
                    </span>
                </div>

            </div>
            
            <OverlapShifts modal_show={this.state.modal_collapse_shift} collapse_shift={this.state.collapse_shift} checkboxResolveCollapse={this.checkboxResolveCollapse} 
                    collapseSelect={this.state.collapseSelect} saveRoster={this.saveRoster} selectChange={this.selectChange} loading={this.state.loading} />
            
             <StartEndTimeModal modal_show={this.state.modal_show} selectChange={this.selectChange} start_time={this.state.start_time} 
                    end_time={this.state.end_time} index={this.state.index} day={this.state.day} saveDateTime={this.saveDateTime}  closeModal={this.closeModal} loading={this.state.loading}/>
                    
           
            
            <SavingChanges update_disabled={this.state.update_disabled} modal_show={this.state.modal_show_type} saving_type={this.state.is_default} selectChange={this.selectChange} saveAndFinish={this.saveAndFinish} loading={this.state.loading} />
                    
            <StartAndEndDateOfRoster  modal_show={this.state.modal_start_end_date} is_default={this.state.is_default} 
                    selectChange={this.selectChange}  start_date={this.state.start_date} title={this.state.title}
                    end_date={this.state.end_date} check_shift_collapse={this.check_shift_collapse} />    
                    
            <div className="col-lg-12">
                <div className="row mt-3 mb-5 justify-content-between">
                    <div className="col-lg-3"><a onClick={() => this.setState({rosterList : [this.reInitializeObject(WeekData)]})} className="archived_but text-center">Start Again</a></div>
                    <div className="col-lg-3"><a  onClick={() => this.checkValidDay( )} className="submit_button" >Submit</a></div>
                </div>
            </div>

            <PreviousRequest />
        </div>
    </div>
</section>    
                     <p className="empty_message w-100 text-center">if Your have any concerns or need any help regarding a
shift/roster you booked, call us on: <b><a href="tel:03 9896 2468"  data-rel="external">03 9896 2468</a></b></p>
                    <Footer />
                </div>
                );
    }
}

export default RosterRequest;

class SingleDay extends React.Component {
  render() {
      return <div onClick={() => this.props.addRoster(this.props.index, this.props.day)} className="c_style">
                <span className="justify-content-between"><div>{this.props.day}</div></span>
                <span className={((this.props.weekList.is_active)? 'outside_funding_number odd': '')}></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
  }
}

class StartEndTimeModal extends React.Component {
  render() {
      return <Modal
               className="Modal fade size_add roster_z"
               show={this.props.modal_show}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
              <div className="dis_cell">
                <div className="text text-left">
                <h4 className="py-3 my-0 text-center heading_modal">Select Start and End time of this day
                    <a onClick={this.props.closeModal} className="close_i pull-right mt-1"><img src="/assets/images/client/cross_icons.svg"/></a></h4>
                </div>
                <div className="my-0 mb-3"><div className="line"></div></div>
                <h5 className="P_20_T text-center mb-5">Please set a start and an end time for this day.</h5>

                <div className="row P_15_T justify-content-center">
                    <div className="col-md-8">
                        <form id="saving_time">
                        <div className="row">
                            <div className="col-md-6">
                                <label>Start:</label>
                                <span className="input_gradient">
                                    <DatePicker autoComplete={'off'} required={true} className="text-center input_pass"  maxTime={(this.props.end_time)? moment(this.props.end_time) : moment().hours(23).minutes(59)}  
                                        minTime={moment().hours(0).minutes(0)}
                                        selected={this.props.start_time ? moment(this.props.start_time) : null} name="start_time" onChange={(e) => this.props.selectChange(e, 'start_time')} 
                                        showTimeSelect showTimeSelectOnly scrollableTimeDropdown timeIntervals={15} dateFormat="LT"   />
                                        
                                </span>
                            </div>
                            <div className="col-md-6">
                                <label>End:</label>
                                <span className="input_gradient">
                                    <DatePicker autoComplete={'off'} required={true} className="text-center input_pass"   selected={this.props.end_time? moment(this.props.end_time): null} 
                                name="end_time"  minTime={(this.props.start_time)? moment(this.props.start_time) : moment().hours(0).minutes(0)} 
                                maxTime={moment().hours(23).minutes(59)}   onChange={(e) => this.props.selectChange(e, 'end_time')} 
                                showTimeSelect showTimeSelectOnly scrollableTimeDropdown timeIntervals={15} dateFormat="LT"   />
                                </span>
                            </div>
                        </div>
                        </form>
                    </div>

                  
                </div>

                <div className="row justify-content-center">
                    <div className="col-md-6 P_15_T mt-3 mb-4">
                        <button  className="submit_button w-100" onClick={() => this.props.saveDateTime(this.props.index, this.props.day)}>Save & Continue</button>
                    </div>
                </div>
            </div>
            </Modal.Body>
        </Modal>;
  }
}

class SavingChanges extends React.Component {
  render() {
      return <Modal
               className="Modal fade size_add roster_z"
               show={this.props.modal_show}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
              <div className="dis_cell">
                <div className="text text-left">
                   <h4 className="py-3 my-0 text-center heading_modal">Saving new Roster
                    <a onClick={() => this.props.selectChange(false, 'modal_show_type')} className="close_i pull-right mt-1">
                    <img src="/assets/images/client/cross_icons.svg"/></a>
                    </h4>
                      <div className="my-0 mb-3"><div className="line"></div></div>
                </div>
                <h5 className="P_20_T">Would you like to set this as the DEFAULT roster for this Participant?</h5>
                <div className="row P_15_T">
                    <div className="col-md-5">
                        <Select clearable={false} className="custom_select my-select" simpleValue={true}  searchable={false} value={this.props.saving_type} onChange={(e) => this.props.selectChange(e, 'is_default')} 
                            options={[{label: 'No', value: 1},{label: 'Yes', value: 2}]} placeholder="Please Select" />
                    </div>
                    <div className="col-md-2">
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-7"></div>
                    <div className="col-md-5 P_15_T">
                        <button  onClick={this.props.saveAndFinish} className="submit_button w-100">Finish & Save</button>
                    </div>
                </div>

            </div>
            </Modal.Body>
        </Modal>;
  }
}

class StartAndEndDateOfRoster extends React.Component {
  render() {
      return <Modal
               className="Modal fade size_add roster_z"
               show={this.props.modal_show}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
              <div className="dis_cell">
                <div className="text text-left">
                <h4 className="py-3 my-0 text-center heading_modal">Saving New Roster
                    <a onClick={() => this.props.selectChange(false, 'modal_start_end_date')} className="close_i pull-right mt-1">
                    <img src="/assets/images/client/cross_icons.svg"/></a>
                </h4>
                  <div className="my-0 mb-3"><div className="line"></div></div>
                </div>
                <h5 className="P_20_T">Please set a start date for this roster.</h5>

                <div className="row P_15_T">
                    <div className="col-md-12">
                        <form id="saving_roster">
                        <div className="row">
                            {(this.props.is_default == 1)? 
                            <div className="col-md-6">
                                <label>Title</label>
                                <span className="input_gradient">
                                    <input className="input_pass" required={1} name="start_date" value={this.props.title} onChange={(e) => this.props.selectChange(e.target.value, 'title')}  />
                                </span>
                            </div>:''}
                            <div className="col-md-3">
                                <label>Start:</label>
                                <span className="input_gradient">
                                    <DatePicker autoComplete={'off'} required={true} dateFormat="DD/MM/YYYY" utcOffset={1} maxDate={(this.props.end_date)? moment(this.props.end_date) : null}  
                                    minDate={moment()} className="text-center input_pass"  
                                        selected={this.props.start_date ? moment(this.props.start_date) : null} name="start_date" 
                                        onChange={(e) => this.props.selectChange(e, 'start_date')} 
                                         />
                                </span>
                            </div>
                            {(this.props.is_default == 1)? 
                            <div className="col-md-3">
                                <label>End:</label>
                                <span className="input_gradient">
                                    <DatePicker autoComplete={'off'} className="input_pass" required={true} dateFormat="DD/MM/YYYY" className="text-center input_pass"  utcOffset={1} 
                                    selected={this.props.end_date? moment(this.props.end_date): null} 
                                name="end_date"  minDate={(this.props.start_date)? moment(this.props.start_date) : moment()} 
                                  onChange={(e) => this.props.selectChange(e, 'end_date')} 
                                />
                                </span>
                            </div>:''}
                        </div>
                        </form>
                    </div>

                    <div className="col-md-2">
                    </div>
                </div>

                <div className="row mt-2">
                    <div className="col-md-7"></div>
                    <div className="col-md-5 P_15_T">
                        <button className="submit_button w-100" onClick={() => this.props.check_shift_collapse(true)}>Save & Continue</button>
                    </div>
                </div>
            </div>
            </Modal.Body>
        </Modal>;
  }
}

class PreviousRequest extends React.Component {
  constructor(props) {
        super(props);

        this.state = {
            previousRoster:[]
        }
    } 
    
    componentDidMount() {
       this.getPreviousRoster();
    }  
    
    getPreviousRoster = () => {
        postData('participant/Shift_Roster/get_previous_rosters', {}).then((result) => {           
            if(result.status) {
                var details = result.data;
               
                this.setState({previousRoster: details});
            } 
            this.setState({loading: false});
        });
    }
  render() {
      return (
            <div className="col-lg-12">
            <div className="col-lg-12 mb-4">
                <div className="line"></div>
                <h1 className="user_name my-2" align="center">Previous Requests</h1>
                <div className="line"></div>
            </div>

            <div className="col-lg-12">
                <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                    {this.state.previousRoster.length?
                    this.state.previousRoster.map((roster, id) => (
                    <div className="panel panel-default">
                        <div className="panel-heading" role="tab" id="headingOne">
                            <h4 className="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                    {(roster.is_default == 0)? "Defualt Roster": roster.title}
                                    <small className="pull-right Rost_A1"> Status: <span>{roster.status}</span></small>        
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div className="panel-body">
                                <div className="row">
                                    <div className="col-lg-10">
                                        
                                    </div>
                                    <div className="col-lg-2 align-self-end">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    )):<div className="no_record py-2">No record available</div>}

                </div>
            </div>
            </div>
        );
    }
}

class OverlapShifts extends React.Component {
    
    render() {
      return <Modal
               className="Modal fade size_add big_modal_roster"
               show={this.props.modal_show}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
              
              <div className="dis_cell collaspe_roster">
		<div className="text text-left"><h4 className="py-3 my-0 text-center heading_modal">Saving New Roster
                    <a onClick={() => this.props.selectChange(false ,'modal_collapse_shift')} className="close_i pull-right mt-1">
                     <img src="/assets/images/client/cross_icons.svg"/></a>
               </h4>
               <div className="my-0"><div className="line"></div></div>
                </div>
                <h4 className="P_20_T">Please select the shifts to keep or discard from this Participants current <br/>DEFAULT roster. </h4>
                <small><i>*Please note, any shifts you discard will automatically be cancelled. </i></small>

              
              <div className="flex P_15_T mt-5">
                    <div className="col border_dotted_right px-0">
                        <div className="flex"><h3 className="w-100 text-center py-4 by-1 my-0 color">Previous Shifts:</h3></div>
                       </div>

                    <div className="col  px-0">
                        <div className="flex"><h3 className="w-100 text-center py-4 by-1 my-0 color">New Shifts:</h3></div>
                    </div>
              </div>
              
              <div className="flex">
                    <div className="col border_dotted_right px-0">
                        <div className="flex make_header  pr-3 bb-1">
                            <div className="col-1 py-3 px-3 color font_w_4 br-1">
                                <span>
                                    <input type="checkbox" className="checkbox_flex" name="" checked={this.props.collapseSelect == 1? true: false} onChange={() => this.props.checkboxResolveCollapse('', 1, true)} />
                                    <label htmlFor="zw"><span onClick={() => this.props.checkboxResolveCollapse('', 1, true)} className="mx-0"></span></label>
                                </span>
                            </div>
                            <div className="col-3 py-3 px-3 color font_w_4 br-1">Date:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">Start:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">Duration:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">End:</div>
                            <div className="col-2 py-3 px-3 color font_w_4">Status</div>
                        </div>
                    </div>

                    <div className="col px-0">
                      <div className="flex make_header  pl-3 bb-1">
                            <div className="col-1 py-3 px-3 color font_w_4 br-1">
                                <span>
                                    <input type="checkbox" className="checkbox_flex" name="" checked={this.props.collapseSelect == 2? true: false} onChange={() => this.props.checkboxResolveCollapse('', 2, true)} />
                                    <label htmlFor="z"><span onClick={() => this.props.checkboxResolveCollapse('', 2, true)} className="mx-0"></span></label>
                                </span>
                            </div>
                            <div className="col-3 py-3 px-3 color font_w_4 br-1">Date:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">Start:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">Duration:</div>
                            <div className="col-2 py-3 px-3 color font_w_4 br-1">End:</div>
                            <div className="col-2 py-3 px-3 color font_w_4">Status</div>
                        </div>

                        </div>
                </div>
                
                {this.props.collapse_shift.map((shift, id) => (
                (shift.is_collapse)?         
                <div key={id+1} className="flex">
                
                    
                    <div className="col pdd-b border_dotted_right align-self-center px-0">
                    {shift.old.map((oldShift, index) => (
                      <div className="flex make_up radi_1 mt-3 mr-3">
                            <div className="col-1 py-2 px-3 br-1">
                                <span>
                                    <input type="checkbox" className="checkbox_flex"  name="" checked={(oldShift.active)? true : false} onChange={() => this.props.checkboxResolveCollapse(id, 1, false, 'old', index)} />
                                    <label for="a"><span className="mx-0" onClick={() => this.props.checkboxResolveCollapse(id, 1, false, 'old', index)} ></span></label>
                                </span>
                            </div>
                            <div className="col-3 py-2 px-3 br-1">{oldShift.shift_date}</div>
                            <div className="col-2 py-2 px-3 br-1">{oldShift.start_time}</div>
                            <div className="col-2 py-2 px-3 br-1">{oldShift.duration}</div>
                            <div className="col-2 py-2 px-3 br-1">{oldShift.end_time}</div>
                            <div className="col-2 py-2 px-3">{oldShift.status}</div>
                        </div>
                    ))}  
                    </div>
                    
            
                    <div className="col pdd-b align-self-center px-0">
                       <div className="flex make_up radi_1 mt-3 ml-3">
                            <div className="col-1 py-2 px-3 br-1">
                                <span>
                                    <input type="checkbox" className="checkbox_flex" checked={(shift.status == 2)? true: false } name="" onChange={() => this.props.checkboxResolveCollapse(id, 2)} />
                                    <label htmlFor="g"><span onClick={() => this.props.checkboxResolveCollapse(id, 2)}  className="mx-0"></span></label>
                                </span>
                            </div>
                            <div className="col-3 py-2 px-3 br-1">{shift.new.shift_date}</div>
                            <div className="col-2 py-2 px-3 br-1">{shift.new.start_time}</div>
                            <div className="col-2 py-2 px-3 br-1">{shift.new.duration}</div>
                            <div className="col-2 py-2 px-3 br-1">{shift.new.end_time}</div>
                            <div className="col-2 py-2 px-3">{shift.new.status}</div>
                        </div>
                       
                    </div>
                </div>: ''
                ))}

                <div className="row justify-content-center">
                    <div className="col-md-3 P_15_T mt-3">
                        {this.props.loading?<span>Please wait...</span>: ''}
                        <button className="submit_button w-100" disabled={this.props.loading} onClick={this.props.saveRoster}>Save & Continue</button>
                    </div>
                </div>

                </div>				
					
             
            </Modal.Body>
        </Modal>;
  }
}