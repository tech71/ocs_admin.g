import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import jQuery from "jquery";
class Reset_password extends Component {
constructor(props) {
super(props);
        this.state = {
                password : '',
                confirm_password: '',
                loading: false,
                success: ''
        }

this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
}

handleChange(e) {
        var state = {};
        this.setState({error: ''}); 
        state[e.target.name] = (e.target.value);
        this.setState(state);
}

onSubmit(e){
e.preventDefault();
        jQuery("#reset-password").validate();
        if (!this.state.loading && jQuery("#reset-password").valid()) {
            jQuery.ajax({
            url: process.env.BASE_URL+'admin/Login/reset_password',
            type: 'POST',
            dataType: 'JSON',
            data: {
                password : this.state.password,
                id: this.props.data.id
            },
            beforeSend: function () {
                this.setState({loading: true});
            }.bind(this),
            success: function (data) {
                if (data.status) {
                    this.setState({error: ''});
                    this.setState({success: data.success});
                } else{
                     this.setState({error: data.error});
                }
                this.setState({loading: false});
                }.bind(this),
            error: function (xhr) {
                this.setState({loading: false});
            }.bind(this)
        });
    }
}

componentDidMount() {
}

render() {
return (
<div>
    <BlockUi tag="div" blocking={this.state.loading}>
        <link rel="stylesheet" type="text/css" href={process.env.BASE_URL+'assets/css/bootstrap/css/bootstrap_4.css'}/>
        <link rel="stylesheet" type="text/css" href={process.env.BASE_URL+"assets/css/user_admin.css"}/>
        <section>
            <div className="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
                        
                        <div className="logo text-center"><a href="#"><img className="img-fluid" width="70px" src={process.env.BASE_URL+ 'assets/images/user_admin/ocs_logo.svg'}/></a></div>

                        <div className="limiter">
                            <div className="login_1">
                                <div className="Smiley">
                                    <h1>
                                    	<span>Reset Password&nbsp;<i class="icon icon-smail-big"></i></span> 
                                    </h1>
                                </div>
                                
                                <form id="reset-password" className="login100-form" >
                                        <div class="col-md-12">
					<div class="User d-flex">
                                           <span><img src={process.env.BASE_URL+ 'assets/images/user_admin/user_img.png'} className="img-fluid align-self-center" alt="IMG"/></span>
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" id="password" type="password" name="password" placeholder="New password" onChange={this.handleChange} value={this.state.password} data-rule-required="true" />
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" type="password" name="confirm_password" placeholder="Confirm password" onChange={this.handleChange} value={this.state.confirm_password} data-rule-required="true" data-rule-equalTo="#password" data-msg-equalTo="Please enter same password" />
                                        </div>
                                        <div className="login_but">
                                            <button onClick={this.onSubmit} className="but_login orange">
                                                Reset password
                                            </button>
                                        </div>

                                        <div className="success_login">{this.state.success}</div>
                                           <div className="error_login">{this.state.error}</div>
                                    </div>
					<h5 class="col-md-12 text-center P_30_T text-center for_text"><a>Login</a></h5>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer className="text-center">
                        <div className="container">
                            <div className="row">
                                <h6>© 2018 All Rights Reserved <span>Healthcare Manager</span></h6>
                            </div>
                        </div>
                    </footer>
    </BlockUi>
</div>
                );
    }
}
export default Reset_password;