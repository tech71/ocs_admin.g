import validator from 'validator';


import React, { Component } from 'react';
import BlockUi from 'react-block-ui';

import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../config.js';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import axios from 'axios';


import 'react-block-ui/style.css';

const required = (value) => {
    if (!value.toString().trim().length) {
        return 'This field is required';
    }
};


class login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            gender: 1,
            remember: '',
            loading: false,
            error: '',
            success: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleChange(e) {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    validateAll() {
        this.form.validateAll();
    }

    onSubmit = (e) => {
        e.preventDefault();


        // if (!this.state.loading) {

        //     let data = {
        //         username: this.state.username,
        //         password: this.state.password,
        //         remember: this.state.remember
        //     };

        //     const config = {

        //         headers: {
        //             'content-type': 'multipart/form-data'
        //         }
        //     }

        //     axios.post(BASE_URL + 'Participant/Auth/client_login', data, config)
        //     .then(response => {
        //         console.log(response)
        //     });
        // }


        if (!this.state.loading) {
            jQuery.ajax({
                url: BASE_URL + 'Participant/Auth/client_login',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    username: this.state.username,
                    password: this.state.password,
                    remember: this.state.remember
                },
                beforeSend: function () {
                    this.setState({loading: true});
                }.bind(this),
                success: function (data) {

                    this.setState({loading: false});
                    if (data.status) {
                        this.setState({success: data.success});
                        localStorage.setItem("ocs_token603560", data.token);
                        window.location = ROUTER_PATH + 'admin/participant';

                    } else {
                        this.setState({error: data.error});
                    }
                }.bind(this),
                error: function (xhr) {					
                    this.setState({loading: false});
                }.bind(this)
            });
        }
    }

    componentDidMount() {
        this.setState({ gender: 1, username: '', password: '' });
    }

    render() {
        return (
            <div>
                <BlockUi tag="div" blocking={this.state.loading}>
                    <link rel="stylesheet" type="text/css" href="assets/css/user_admin.css" />

                    <section>
                        <div className="container">
                            <div className="row ">
                                <div className="col-md-4 col-md-offset-4">
                                    <div className="logo text-center">
                                        <a href="#">
                                            <img className="img-fluid" width="70px" src={'/assets/images/user_admin/ocs_logo.svg'} />
                                            <i className="icon icon-userm1-ie"></i>
                                        </a>
                                    </div>

                                    <div className="limiter">
                                        <div className="login_1">
                                            <div className="Smiley">
                                                <h1><span>Welcome Back  &nbsp;<i className="icon icon-smail-big"></i></span>
                                                    Please Login Below
													</h1>
                                            </div>

                                            <Form id="login" className="login100-form" ref={c => {
                                                this.form = c
                                            }} onSubmit={this.onSubmit}>

                                                <div className="col-md-12">
                                                    <div className="User d-flex">
                                                        <span><img src={'assets/images/user_admin/' + ((this.state.gender == 1) ? 'user_img.png' : 'she.png')} className="img-fluid align-self-center" alt="IMG" /></span>
                                                    </div>


                                                    <div className="input_2">
                                                        <Input className="input_3" type="text" name="username" placeholder="Username" value={this.state.username || ''} onChange={this.handleChange} validations={[required]} />
                                                    </div>


                                                    <div className="input_2">
                                                        <Input className="input_3 required" type="password" name="password" placeholder="Password" value={this.state.password || ''} onChange={this.handleChange} validations={[required]} />
                                                    </div>

                                                    <div className="login_but">
                                                        <button className="but_login orange" onClick={this.validateAll.bind(this)} >
                                                            Submit
                                                            </button>
                                                    </div>

                                                    <div className="success_login">{this.state.success}</div>
                                                    <div className="error_login">{this.state.error}</div>
                                                </div>

                                                <p className="col-md-12 text-center P_30_T">
                                                    <span className="b_check_2">
                                                        <input type="checkbox" className="checkbox2" id="c1" name="remember" value={this.state.remember || ''} onChange={this.handleChange} value="1" />
                                                        <label htmlFor="c1" ><span></span >Remember me</label>
                                                    </span>
                                                </p>
                                                <h5 className="col-md-12 text-center P_30_T text-center for_text"><Link to={ROUTER_PATH + 'forgot_password'}>Forgot Password?</Link></h5>
                                            </Form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <footer className="text-center">
                        <div className="container">
                            <div className="row">
                                <h6>© 2018 All Right Reserved <span>HCM</span></h6>
                            </div>
                        </div>
                    </footer>
                </BlockUi>
            </div>

        );
    }
}

export default login
