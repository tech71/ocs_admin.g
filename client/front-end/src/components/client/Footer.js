import React, { Component } from 'react';

class Footer extends Component {
	constructor(props) {
		super(props);
	}
	render() {

		var date = new Date();
		var year = date.getFullYear();

		return (
			<div>
				<footer className="text-center footer">

					<div className="container">

						<div className="row">

							<div className="col-lg-12">

								<ul className="footer_nav">

									<li><a>About My HCM</a></li>

									<li><a>Terms & Conditions</a></li>

									<li className="footer_1 hidden-md-down"><span>&#x24B8;{year}-All Right Reserved <b>Healthcare Manager</b></span></li>

									<li><a>Privacy Policy</a></li>

									<li><a>Staff Policies</a></li>

									<li className="footer_1 hidden-lg-up"><span>&#x24B8;2018-All Right Reserved <b>Healthcare Manager</b></span></li>

								</ul>

							</div>

						</div>

					</div>

				</footer>

			</div>
		);
	}
}

export default Footer
