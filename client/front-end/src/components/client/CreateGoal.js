import React, { Component } from 'react';
import jQuery from "jquery";
import {Panel, Button, ProgressBar, Modal} from 'react-bootstrap';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import {postData, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, getPercentage} from '../../service/common';
import '../../service/jquery.validate.js';

class CreateGoal extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }
    
    selectChange = (key, value) => {
        var state = {}
       /* if(key == 'shift_date'){
             this.setState({shiftLoading: true})
             postData('participant/Shift_Roster/get_shift_list_for_feedback', {shift_date: value}).then((result) => {
                if (result.status) {
                     this.setState({loadedShiftOption: result.data})
                } else {
                    this.setState({error: result.error});
                }
                this.setState({shiftLoading: false})
            });
        }*/
        state[key] = value;
        this.setState(state);
    }

    submitFeedback = ()=>{
        jQuery('#submit_feedback').validate({ignore: []});
        if(jQuery('#submit_feedback').valid()){			
            postData('participant/Goals/add_participant_goal', this.state).then((result) => {               
				if (result.status) {
                     toast.success('Participant Goal submit successfully', {
                           position: toast.POSITION.TOP_CENTER,
                           hideProgressBar: true
                     });    
                     //this.setState({shift_date: '', shiftId: '', feedback_message: ''})
                } else {
                    this.setState({error: result.error});
                }
            });
        }
    }
    
    componentDidMount() {

    }

    render() {
        return (
                <div>
                    <Header title={'Provide Feedback'} />

                    <section>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 mt-2"><div className="line"></div></div>
							<div className="col-md-12 back-arrow mt-2"><Link to="/goals"><i className="icon icon-back-arrow"></i></Link></div>
                        </div>
                        <form id="submit_feedback">
                        <div className="row mt-5">                           
						   <div className="col-lg-6 mb-3">
							<label>Goal</label>
                                <span className="input_2">
									<input className="input_pass" placeholder="Your Goal" type="text" name="goal" data-rule-required="true" onChange={(e) => this.selectChange('title', e.target.value)}  value={this.state.title} />
								</span>
                            </div>
						   
                            <div className="col-lg-2 mb-4" align="left">
                                <label>Start Date</label>
                                <span className="input_gradient">
                                <DatePicker  autoComplete={'off'} required={true} dateFormat="DD/MM/YYYY" placeholderText="00/00/0000"
                                     className="text-center input_pass"  
                                        selected={this.state.start_date ? moment(this.state.start_date) : null} name="start_date" 
                                        onChange={(e) => this.selectChange('start_date', e)} 
                                         />
                                </span>
                            </div>
							
							
							<div className="col-lg-2 mb-4" align="left">
                                
								<label>End Date</label>
                                <span className="input_gradient">
                                <DatePicker autoComplete={'off'} required={true} data-rule-email="true" dateFormat="DD/MM/YYYY" maxDate={moment()}  placeholderText="00/00/0000"
                                     className="text-center input_pass"  
                                        selected={this.state.end_date ? moment(this.state.end_date) : null} name="end_date" 
                                        onChange={(e) => this.selectChange('end_date', e)} 
                                         />
                                </span>
                            </div>
                            
                        </div>
                        <div className="row my-3 justify-content-end">
                          <div className="col-lg-3 col-sm-5">
                                <a onClick={this.submitFeedback} className="submit_button w-100">Submit</a>
                            </div>
                        </div>
                          </form>
                    </div>
                  
                </section>   
                
                    <Footer />
                </div>
                );
    }
}

export default CreateGoal;