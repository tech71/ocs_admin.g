import React, { Component } from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { checkItsNotLoggedIn, postData, getFirstname, setFirstname, setPercentage } from '../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
import Newsticker from 'react-newsticker';
import { connect } from 'react-redux'

class Dashboard extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.state = {
            firstname: '',
            notification: [],
            updates: [],
            message_count: 0,
        }
    }

    componentDidMount() {
        postData('participant/dashboard/get_participant_firstname', {}).then((result) => {
            if (result.status) {
                this.setState(result.data);
                setFirstname(result.data.firstname);
                setPercentage(result.data.profile_percent);
            }
            this.setState({ loading: false });
        });



        postData('participant/Dashboard/get_participant_profile_image', {}).then((result) => {
            if (result.status) {
                var details = result.data;
                this.setState({ imageUrl: details });
            }
        });
    }

    onError() {
        this.setState({
            imageUrl: "/assets/images/client/smail.svg"
        })
    }

    render() {
        return (
            <div>
                <Header title={'Hi ' + getFirstname()} smiley={true} />

                <div className="container">
                    <div className="row">

                        {this.props.ImailNotificationData?
                            <div className="col-md-12 dashborad_notification">
                                <Link to={"inbox/message/" + this.props.ImailNotificationData.id} >
                                    <p className="notification_text">{this.props.ImailNotificationData.title}  — {this.props.ImailNotificationData.content}
                                    </p>
                                </Link>
                            </div>
                            :
                            null
                        }


                        {
                            this.props.NotificationData.length > 0 ?
                                <div className="col-sm-12 newStick12" >
                                    <Newsticker news={this.props.NotificationData} />
                                </div>
                                : null
                        }

                    </div>
                    <div className="row your second justify-content-center">
                        <div className="col-lg-3 col-sm-6">
                            <Link to="Profile">
                                <h4 align="center">Your Proﬁle</h4>
                                <div className="User">
                                    <span className='profile_sp'><img onError={this.onError.bind(this)} src={this.state.imageUrl} className="img-fluid" alt="  " /></span>
                                </div>
                                <p align="center">Update My Proﬁle</p>
                            </Link>
                        </div>

                        <div className="col-lg-3 col-sm-6">
                            <Link to="shift_roster">
                                <h4 align="center">Shifts & Rosters</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-calendar icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center">View Upcoming & Book</p>
                            </Link>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <Link to="/spending_tracker">
                                <h4 align="center">Spending Tracker</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-graph icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center"><a >Track My Plan Spending</a></p>
                            </Link>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <Link to="/goals">
                                <h4 align="center">Goal Tracker</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-star icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center">Track My Plan Goals</p>
                            </Link>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            {(this.props.mail_count > 0) ?
                                <span className='notifyP'>{this.props.mail_count}</span>
                                : ''}
                            <Link to="/inbox/all">
                                <h4 align="center">Messages & Notiﬁcations</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-share icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center">View & Send Mail</p>
                            </Link>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <Link to={'/feedback'}>
                                <h4 align="center">Provide  Feedback</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-message icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center">Provide & Track Feedback</p>
                            </Link>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <Link to="settings">
                                <h4 align="center">Your Settings</h4>
                                <div className="User">
                                    <span className='dsh_spns'>
                                        <i className='icon icon-setting icn_dashLinks'></i>
                                    </span>
                                </div>
                                <p align="center">Manage My Settings</p>
                            </Link>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    NotificationData: state.NotificationReducer.NotificationData,
    ImailNotificationData: state.NotificationReducer.ImailNotificationData,
    mail_count: state.NotificationReducer.mail_count,
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);