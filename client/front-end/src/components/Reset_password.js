import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import jQuery from "jquery";
import { BASE_URL } from '../config.js';
import { LOGIN_SVG, OCS_LOGO} from '../service/OcsConstant.js';

class Reset_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            confirm_password: '',
            loading: false,
            success: ''
        }
    }

    handleChange = (e) => {
        var state = {};
        this.setState({error: ''});
        state[e.target.name] = (e.target.value);
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        jQuery("#reset-password").validate();
        if (!this.state.loading && jQuery("#reset-password").valid()) {
            jQuery.ajax({
                url: BASE_URL + 'participant/Auth/reset_password',
                type: 'POST',
                dataType: 'JSON',
                data: {password: this.state.password, auth: this.props.match.params},
                success: function (data) {
                    if (data.status) {
                        this.setState({error: ''});
                        this.setState({success: data.success});
                    } else {
                        this.setState({error: data.error});
                    }
                    this.setState({loading: false});
                }.bind(this),
                error: function (xhr) {
                    this.setState({loading: false});
                }.bind(this)
            });
        }
    }

    componentDidMount() {
        this.setState({validateMassage: <span><h2 className="text-center">Please wait..</h2><p className="text-center">we are validating your request</p></span>})
        jQuery.ajax({
            url: BASE_URL + 'participant/Auth/verify_reset_password_token',
            type: 'POST',
            dataType: 'JSON',
            data: this.props.match.params,
            success: function (data) {
                if (data.status) {
                    this.setState({validating: true});
                    this.setState({error: ''});
                    this.setState({success: data.success});
                } else {
                    this.setState({validateMassage: <h2 className="text-center">{data.error}</h2>});
                }
            }.bind(this),
            error: function (xhr) {
                this.setState({loading: false});
            }.bind(this)
        });
    }

    render() {
        return (
                <div>
                    <BlockUi tag="div" blocking={this.state.loading}>
        <section>
            <div className="container gradient_color">
		  <div className="row justify-content-md-center justify-content-sm-center">
            <div className="col-sm-11  col-md-7  col-lg-5">
                   
                       <div className="logo text-center"><img className="img-fluid"  src={OCS_LOGO} /></div>

                        <div className="limiter">
                            <div className="login_1">
                                <div className="Smiley">
                                    <h1>
                                    	<span>Reset Password&nbsp;<i className="icon icon-smail-big"></i></span> 
                                    </h1>
                                </div>
                                  {(this.state.validating) ?
                                <form id="reset-password" className="login100-form" >
                                      <div className="px-lg-4 px-md-3 px-sm-4 px-3 px-xl-5 pt-5">
					<div className="User d-flex">
                                           <span><img src={'/assets/images/client/man.png'} className="img-fluid align-self-center" alt="IMG"/></span>
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" id="password" type="password" name="password" placeholder="New password" onChange={this.handleChange} value={this.state.password} data-rule-required="true" data-rule-minlength="6" autoComplete="new-password"/>
                                        </div>
                                        <div className="input_2">
                                            <input className="input_3" type="password" name="confirm_password" placeholder="Confirm password" onChange={this.handleChange} value={this.state.confirm_password} data-rule-required="true" data-rule-equalto="#password" autoComplete="new-password" data-msg-equalto="Please enter same password" />
                                        </div>
                                        <div className="login_but">
                                            <button onClick={this.onSubmit} className="but_login orange">
                                                Reset password
                                            </button>
                                        </div>

                                       <div className="success_login">{this.state.success}</div>
                                       <div className="error_login">{this.state.error}</div>
                                    </div>
					
                                </form>
								  : <div>{this.state.validateMassage} </div>}
								  <h5 className="col-md-12 text-center P_30_T text-center for_text"><Link to={'/'}>Login</Link></h5>
                                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<footer className="text-center footer_1">
    <span>&#169;2018-All Righ  Reserved <b>Healthcare Manager</b></span>
</footer> 
    </BlockUi>
                </div>
                );
    }
}
export default Reset_password;