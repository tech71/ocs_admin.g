import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';


class PageNotFound extends Component {
    constructor(props) {
        super(props);
        this.state = {          
            pagenotfound:true,
        };
    }

    render() {
        return (
            <div>
                  
            <div className="error_bg">
                <div className="center_box">
                    <h1>4<span>0</span>4</h1>
                     <p>The page you requested was not found.</p>
                    <div className="pt-4">
                        <Link to={'/dashboard/'}>
                        <button className='btn bk_home_btn'><i className='icon icon-arrow-l'></i> back to home</button>
                        </Link>
                    </div>
                </div>
            </div>
        
        </div>     
                );
    }
}
export default PageNotFound
