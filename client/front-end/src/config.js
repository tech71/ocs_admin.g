export const ROUTER_PATH = '/';
export const LOGIN_DIFFERENCE = 60 // 60 minits;

export const BASE_URL = process.env.REACT_APP_OCS_CLIENT_API_HOST;//'http://localhost/OCS_repository/client/back-end/';
export const WS_URL = process.env.REACT_APP_OCS_CLIENT_WS_URL;//'ws://localhost:9000';


//export const BASE_URL = 'https://clientapi.dev.healthcaremgr.net/';
//export const WS_URL = 'wss://adminapi.dev.healthcaremgr.net/wss';