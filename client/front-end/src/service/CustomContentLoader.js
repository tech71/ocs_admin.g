import React, { Component } from 'react';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { TextBlock, MediaBlock, TextRow, RectShape, RoundShape } from 'react-placeholder/lib/placeholders';

export const customProfile = (
      <div className=''>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1 mt-3">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '62%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1 mt-3">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '32%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '40%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '30%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '42%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '35%', 'borderRadius': '25px', display: 'inline-block' }}> <RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '20%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '35%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '40%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
                  <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '40%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
                  <div className="col-lg-9 col-md-9 f py-1"><ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '65%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder></div></div>

            <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
                  <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '38%', height: 20, 'borderRadius': 25, display: 'inline-block' }} /></div>
                  <div className="col-lg-9 col-md-9 f py-1"><ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder></div></div>

            <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
                  <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '52%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
            </div>
            <div className="col-lg-9 col-md-9 f py-1">
                  <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '32%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
            </div>
            </div>

                  <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '46%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '60%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

                  <div className="row f_color_size"><div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '32%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '58%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>

            <div className="row f_color_size">
                  <div className="col-lg-3 col-md-3 f py-1">
                        <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '50%', height: 20, 'borderRadius': 25, display: 'inline-block' }} />
                  </div>
                  <div className="col-lg-9 col-md-9 f py-1">
                        <ReactPlaceholder ready={false} type='textRow' showLoadingAnimation={true} color='#CDCDCD' style={{ 'marginTop': '0em', width: '32%', 'borderRadius': '25px', display: 'inline-block' }} ><RectShape defaultValue={''} /></ReactPlaceholder>
                  </div>
            </div>
      </div>
);

export function custNumberLine(number) {
      var temp = []
      for (var i = 1; i < number; i++) {
            temp[i] = i;
      }
      return temp.map((name, id) => (<ReactPlaceholder ready={false} key={id + 1} showLoadingAnimation={true} style={{ "borderRadius": "25px" }} type='textRow'><RectShape defaultValue={''} /></ReactPlaceholder>))
};

export const CustomProfileImage = (
      <table width="100%" className="user_profile">
            <tbody>
                  <tr>
                        <td className="width_130">
                              <div className="">
                                    <ReactPlaceholder ready={false} showLoadingAnimation={true} type='round' ready={false} color='#E0E0E0' style={{ width: 100, height: 100 }}><RectShape defaultValue={''} /></ReactPlaceholder>
                              </div>
                        </td>
                        <td>
                              <ul>
                                    <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '30%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                                    <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '40%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                                    <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '48%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
                                    <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: '42%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />

                              </ul>
                        </td>
                  </tr>
            </tbody>
      </table>
);

export function customHeading(width, align) {
      if (align == 'center') {
            return <h1 className="color"><RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ 'margin': '0px auto', width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} /></h1>
      } else {
            return <h1 className="color"><RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} /></h1>
      }
}


export function customNavigation(number, width, align) {
      return <aside className="col-lg-2 col-md-3">
            <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
            <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
            <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
            <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
            <RectShape showLoadingAnimation={true} color='#CDCDCD' style={{ width: width + '%', height: 20, 'borderRadius': 25, 'marginBottom': '5px' }} />
      </aside>
};