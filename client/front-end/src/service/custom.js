import jQuery from "jquery";
import  "./bootstrap.min.js";

var $ = jQuery;

var bsMajorVer = 0;
var bsMinorVer = 0;
$.extend(true, $.validator, {
    prototype: {
        showErrors: function () {
            var _this = this;
            var bsVersion = $.fn.tooltip.Constructor.VERSION;

            // Try to determine Bootstrap major and minor versions
            if (bsVersion) {
                bsVersion = bsVersion.split('.');
                bsMajorVer = parseInt(bsVersion[0]);
                bsMinorVer = parseInt(bsVersion[1]);
            }

            $.each(this.errorList, function (index, value) {
                //If Bootstrap 3.3 or greater
                if (bsMajorVer === 3 && bsMinorVer >= 3 && $(value.element != null)) {
                    var $currentElement = $(value.element);
                    if ($currentElement.data('bs.tooltip') !== undefined) {
                        $currentElement.data('bs.tooltip').options.title = value.message;
                    } else {
                        $currentElement.tooltip(_this.applyTooltipOptions(value.element, value.message));
                    }

                    $(value.element).removeClass(_this.settings.validClass)
                            .addClass(_this.settings.errorClass)
                            .tooltip('show');
                } else {
                    $(value.element).removeClass(_this.settings.validClass)
                            .addClass(_this.settings.errorClass)
                            .tooltip(bsMajorVer === 4 ? 'dispose' : 'hide')
                            .tooltip(_this.applyTooltipOptions(value.element, value.message))
                            .tooltip('show');
                }

                if (_this.settings.highlight) {
                    // this.settings.highlight.call(_this, value.element, this.settings.errorClass, _this.settings.validClass);
                }
            });

            $.each(_this.validElements(), function (index, value) {

                if ($(value) != null && $(value) != undefined) {


                    $(value).removeClass(_this.settings.errorClass)
                            .addClass(_this.settings.validClass)
                            .tooltip(bsMajorVer === 4 ? 'dispose' : 'hide');

                }

                if (_this.settings.unhighlight) {
                  //  this.settings.unhighlight.call(_this, value, this.settings.errorClass, _this.settings.validClass);
                }
            });
        },

        applyTooltipOptions: function (element, message) {
            var defaults;

            if (bsMajorVer === 4) {
                defaults = $.fn.tooltip.Constructor.Default;
            } else if (bsMajorVer === 3) {
                defaults = $.fn.tooltip.Constructor.DEFAULTS;
            } else {
                // Assuming BS version 2
                defaults = $.fn.tooltip.defaults;
            }

            var options = {
                // Using Twitter Bootstrap Defaults if no settings are given 
                animation: $(element).data('animation') || defaults.animation,
                html: $(element).data('html') || defaults.html,
                placement: $(element).data('placement') || defaults.placement,
                selector: $(element).data('selector') || defaults.selector,
                title: $(element).attr('title') || message,
                trigger: $.trim('manual ' + ($(element).data('trigger') || '')),
                delay: $(element).data('delay') || defaults.delay,
                container: $(element).data('container') || defaults.container,
            };

            if (this.settings.tooltip_options && this.settings.tooltip_options[element.name]) {
                $.extend(options, this.settings.tooltip_options[element.name]);
            }
            //jshint ignore:start 
            if (this.settings.tooltip_options && this.settings.tooltip_options['_all_']) {
                $.extend(options, this.settings.tooltip_options['_all_']);
            }
            // jshint ignore:end 
            return options;
        },
    },
});

jQuery.validator.addMethod("notequaltogroup", function (value, element, options) {
    var elems = jQuery(element).parents('form').find(options[0]);
    var valueToCompare = value;

    var matchesFound = 0;
    jQuery.each(elems, function () {
        var thisVal = jQuery(this).val();
        if (thisVal == valueToCompare) {
            matchesFound++;
        }
    });

    if (this.optional(element) || matchesFound <= 1) {
        //elems.removeClass('error');
        return true;
    } else {
        //elems.addClass('error');
    }
}, ("Please enter a unique email."));

$.validator.addMethod("email",
        function (value, element) {
            if (value != '') {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            } else {
                return true;
            }

        },
        "Please enter valid email address"
        );

$.validator.addMethod("phonenumber",
        function (value, element) {
            if (value != '') {
                //return /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{8,18}$/.test(value);
                return /^(?=.{8,18}$)(\(?\+?[0-9]{1,3}\)?)([ ]{0,1})?[0-9_\- \(\)]{8,18}$/.test(value);
            } else {
                return true;
            }

        },
        "Please enter valid phone number"
        );