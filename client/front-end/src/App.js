import React, { Component } from 'react';
import './App.css';
import { postData, checkLoginWithReturnTrueFalse } from './service/common.js';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL } from './config.js';

import Login from './components/Login';
import Forgot_password from './components/Forgot_password';
import Reset_password from './components/Reset_password';
import Dashboard from './components/client/Dashboard';
import Settings from './components/client/Settings';
import Profile from './components/client/Profile';
import Preferences from './components/client/Preferences';
import RosterRequest from './components/client/RosterRequest';
import ShiftAndRoster from './components/client/ShiftAndRoster';
import ShiftCalander from './components/client/ShiftCalander';
import CreateShift from './components/client/CreateShift';
import MessageAndNotification from './components/client/MessageAndNotification';
import MessageReplay from './components/client/MessageReply';
import ProvideFeedback from './components/client/ProvideFeedback';
import FeedbackCaseDetail from './components/client/FeedbackCaseDetail';
import CreateGoal from './components/client/CreateGoal';
import Goals from './components/client/Goals';
import GoalsHistory from './components/client/GoalsHistory';
import PageNotFound from './components/PageNotFound';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment-timezone';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import {getNotification} from './components/client/actions/NotificationAction.js';
import { connect } from 'react-redux'
import WebsocketUserConnectionHandleData from './components/client/WebsocketUserConnectionHandleData.js';
import SpendingTracker from './components/client/SpendingTracker';
import ScrollToTop from 'react-router-scroll-top';



moment.tz.setDefault('Australia/Melbourne');


class App extends Component {
    constructor(props) {
        super(props);
        moment.tz.setDefault("Australia/Melbourne");
    }
    
    componentDidMount() {
        if(checkLoginWithReturnTrueFalse()){
            this.props.getNotification();
        }
    }

    render() {
        return (
            <div>
                <Router>
                    <div>
                        <ScrollToTop>
                        <Switch>
                            <Route exact path={ROUTER_PATH} render={() => <Login auth={this.props} />} />
                            <Route exact path={ROUTER_PATH + 'Forgot_password'} render={() => <Forgot_password auth={this.props} />} />
                            <Route exact path={ROUTER_PATH + 'Reset_password/:id/:token/:dateTime'} component={Reset_password} />
                            <Route exact path={ROUTER_PATH + 'dashboard'} component={Dashboard} />
                            <Route exact path={ROUTER_PATH + 'profile'} component={Profile} />
                            <Route exact path={ROUTER_PATH + 'settings'} component={Settings} />
                            <Route exact path={ROUTER_PATH + 'preferences'} component={Preferences} />
                            <Route exact path={ROUTER_PATH + 'roster_request'} component={RosterRequest} />
                            <Route exact path={ROUTER_PATH + 'shift_roster'} component={ShiftAndRoster} />
                            <Route exact path={ROUTER_PATH + 'shift_calander'} component={ShiftCalander} />
                            <Route exact path={ROUTER_PATH + 'create_shift'} component={CreateShift} />
                            <Route exact path={ROUTER_PATH + 'inbox/:type(all|archive|favourite)'} component={MessageAndNotification} />
                            <Route exact path={ROUTER_PATH + 'inbox/message/:id'} component={MessageReplay} />
                            <Route exact path={ROUTER_PATH + 'feedback'} component={ProvideFeedback} />
			    <Route exact path={ROUTER_PATH + 'feedback/case/:id'} component={FeedbackCaseDetail} />							
                            <Route exact path={ROUTER_PATH + 'goals'} component={Goals} />
                            <Route exact path={ROUTER_PATH + 'goals_history'} component={GoalsHistory} />
                            <Route exact path={ROUTER_PATH + 'goal/create'} component={CreateGoal} />
                            <Route exact path={ROUTER_PATH + 'spending_tracker'} component={SpendingTracker} />
                            
                            <Route exact path={'/*'} component={PageNotFound} />
                        </Switch>
                        </ScrollToTop>
                    </div>
                </Router>
                <ToastContainer />
                {(checkLoginWithReturnTrueFalse())? <WebsocketUserConnectionHandleData /> : ''}
            </div>
        );
    }
}


const mapStateToProps = state => ({
  
})

const mapDispatchtoProps = (dispach) => {
    return {
        getNotification : () => dispach(getNotification())
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(App);
