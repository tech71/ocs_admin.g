<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/KeyPayCommonAtuh.php';

use DocuSign\eSign\Model\Date;
use KeyPayCommonAtuh\KeyPayCommonAtuh;
class KeyPayFetch extends KeyPayCommonAtuh {

    function __construct($config) {
       // Call the KeyPayCommonAtuh constructor
       parent::__construct();

        if(isset($config['company_id'])){

            $this->setCompanyId($config['company_id']);
        }else{
            exit('company_id field is required.');
        }
    }

    public function get_payroll_txt(){
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'msg'=>$keyPayData['error']];
        }
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];
        $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',['fromDate'=>'2019-07-01','toDate'=>'2020-06-30','groupBy'=>'DefaultLocation']);
        pr([$keyPayResponse]);

    }
    /* $params = ['fromDate'=>'2019-07-01','toDate'=>'2020-06-30','groupBy'=>'DefaultLocation'] */
    public function get_payroll_gross_to_net_tax($params=array()){
        
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'error'=>$keyPayData['error']];
        }
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];

        $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',$params);
        $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
        if(empty($keyPayResponseData)){
            return ['status'=>false,'error'=>"give date range data not found."];
        }
        if(isset($keyPayResponseData['message'])){
            return ['status'=>false,'error'=>$keyPayResponseData['message']];
        }
        $totalWagesData = array_sum(array_column($keyPayResponseData,'taxableEarnings'));
        $totalExpenses = array_sum(array_column($keyPayResponseData,'totalExpenses'));
        $totalSuper = array_sum(array_column($keyPayResponseData,'sgc')) + array_sum(array_column($keyPayResponseData,'employerContribution'));
        return ['status'=>true,'data'=>[
            'totalWages'=>$totalWagesData,
            'totalExpenses'=>$totalExpenses,
            'totalSuper'=>$totalSuper
        ]];
       

    }

    private function get_interval_month_wise($fromDate,$toDate){
        $startDate = new DateTime($fromDate);
        $endDate = new DateTime($toDate);
        $dateInterval = new DateInterval('P1M');
        $datePeriod   = new DatePeriod($startDate, $dateInterval, $endDate);
        $monthData = [];
            foreach ($datePeriod as $date) {
              $temp= ['from_date'=>$date->format('Y-m-01'),'to_date'=>$date->format('Y-m-t')];
              $monthData[]=$temp;
            }
        return  $monthData;
    }
    public function get_finacial_year_data(int $numberofPastYear){
        $numberofPastYear= !empty($numberofPastYear)? $numberofPastYear-1:$numberofPastYear;
        $currentYear = (int) date('m')<6 ? date('Y')-1:date('Y');
        $fromYear = $currentYear - $numberofPastYear;
        $number = range($fromYear,$currentYear,1);
        return $number;
    }
    private function get_finacial_year_between_from_to_year(int $numberofPastYear){
        $number = $this->get_finacial_year_data($numberofPastYear);
        $financeYear =[];
        asort($number);
        foreach($number as $row){
            $temp = ['from_date'=> $row.'-07-01','to_date'=> ($row+1).'-06-30'];
            $financeYear[] = $temp;
        }
        return $financeYear;
    }
    public function get_current_finacial_year_data(){
           return $this->get_finacial_year_between_from_to_year(0);
    }
    
    public function get_payroll_gross_to_net_tax_by_last_three_year(){
        $currentDateTime = DATE_TIME;
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'error'=>$keyPayData['error']];
        }
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];
        $getMonthsData = [];
        $insertData = [];
        $financeYearData = $this->get_finacial_year_between_from_to_year(3);
        if(!empty($financeYearData)){
            foreach($financeYearData as $rowYear){
               $temp= $this->get_interval_month_wise($rowYear['from_date'],$rowYear['to_date']);
               $getMonthsData = array_merge($getMonthsData, $temp);
            }
        }


        if(!empty($getMonthsData)){
            $insertData =[];
            foreach($getMonthsData as $rowKey=>$rowMonth){
                if($rowKey%4===0){
                    sleep(1);
                }
                $params=['fromDate'=>$rowMonth['from_date'],'toDate'=>$rowMonth['to_date'],'groupBy'=>'DefaultLocation'];
                $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',$params);
                $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
                if(empty($keyPayResponseData)){
                    continue;
                }
                if(isset($keyPayResponseData['message'])){
                    continue;
                }
                if(is_string($keyPayResponseData) && !empty($keyPayResponseData)){
                    sleep(1);
                    $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',$params);
                    $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
                    if(empty($keyPayResponseData)){
                        continue;
                    }
                    if(isset($keyPayResponseData['message'])){
                        continue;
                    }
                }
                $totalWagesData = array_sum(array_column($keyPayResponseData,'taxableEarnings'));
                $totalExpenses = array_sum(array_column($keyPayResponseData,'totalExpenses'));
                $totalGrossPlusSuper = array_sum(array_column($keyPayResponseData,'totalGrossPlusSuper'));
                $totalSuper = array_sum(array_column($keyPayResponseData,'sgc')) + array_sum(array_column($keyPayResponseData,'employerContribution'));
                $insertData[]=[
                    'month_date'=>$rowMonth['from_date'],
                    'total_wages' =>$totalWagesData,
                    'total_expenses' =>$totalExpenses,
                    'total_super' =>$totalSuper,
                    'total_gross'=> $totalGrossPlusSuper,
                    'archive'=>0,
                    'created'=>$currentDateTime
                ];
            }
            if(!empty($insertData)){
                $this->CI->Basic_model->update_records('finance_payroll_keypay_graph_data',['archive'=>1],['archive'=>0]);
                $this->CI->Basic_model->insert_update_batch('insert','finance_payroll_keypay_graph_data',$insertData);
            }
        }

        return ['status'=>true];
    }

    public function get_total_payroll_current_finacial_year_data(){
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'msg'=>$keyPayData['error']];
        }
        $yearData = $this->get_finacial_year_between_from_to_year(0);
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];
        $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',['fromDate'=>$yearData[0]['from_date'],'toDate'=>$yearData[0]['to_date'],'groupBy'=>'DefaultLocation']);
        $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
        $totalGrossPlusSuper = 0;
        if(empty($keyPayResponseData)){
            $totalGrossPlusSuper = 0;
        }else if(isset($keyPayResponseData['message'])){
            $totalGrossPlusSuper = 0;
        }else if(!empty($keyPayResponseData) && is_array($keyPayResponseData)){
            $totalGrossPlusSuper = array_sum(array_column($keyPayResponseData,'totalGrossPlusSuper'));
        }
        return ['status'=>true,'total_data'=>$totalGrossPlusSuper];
    }

    public function get_total_payroll_current_finacial_year_to_current_date_data(){
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'msg'=>$keyPayData['error']];
        }
        $yearData = $this->get_finacial_year_between_from_to_year(0);
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];
        $keyPayResponse = $keyPayObject->get($urlData.'report/grosstonet',['fromDate'=>$yearData[0]['from_date'],'toDate'=>DATE_CURRENT,'groupBy'=>'DefaultLocation']);
        $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
        $totalGrossPlusSuper = 0;
        if(empty($keyPayResponseData)){
            $totalGrossPlusSuper = 0;
        }else if(isset($keyPayResponseData['message'])){
            $totalGrossPlusSuper = 0;
        }else if(!empty($keyPayResponseData) && is_array($keyPayResponseData)){
            $totalGrossPlusSuper = array_sum(array_column($keyPayResponseData,'totalGrossPlusSuper'));
        }
        return ['status'=>true,'total_data'=>$totalGrossPlusSuper];
    }
    public function get_total_payrun_out($viewType='week'){
        
        $keyPayData = $this->keyPayAuth();
        if(!isset($keyPayData['status'])){
            return ['status'=>$keyPayData['status'],'msg'=>$keyPayData['error']];
        }
        $viewType = !empty($viewType) ? strtolower($viewType):'';
        $viewType = in_array($viewType,['week','month','year']) ? $viewType : 'week';
        /* switch($viewType){
            case 'month':
            $fromDate = date('Y-m-01', strtotime('-2 month'));
            $toDate = DATE_CURRENT;
            break;
            case 'year':
            $fromDate = date('Y-01-01', strtotime('-2 year'));
            $toDate = DATE_CURRENT;
            break;
            case 'week': 
            $fromDate = date(DB_DATE_FORMAT,strtotime("this monday - 4 week"));
            $toDate = DATE_CURRENT;
            break;
        } */
        $viewTypeData = get_shfit_payroll_filter($viewType);
        $fromDate = $viewTypeData['fromDate'];
        $toDate = $viewTypeData['toDate'];
        $urlData = $keyPayData['url_with_id'];
        $keyPayObject = $keyPayData['keyPayObject'];
        $keyPayResponse = $keyPayObject->get($urlData.'report/payrunactivity',['fromDate'=>$fromDate,'toDate'=> $toDate]);
        $keyPayResponseData = $this->parserObjToArr($keyPayResponse);
        $totalGrossEarnings = 0;
        $totalSuperContributions = 0;
        if(empty($keyPayResponseData)){
            $totalGrossEarnings = 0;
            $totalSuperContributions = 0;
        }else if(isset($keyPayResponseData['message'])){
            $totalGrossEarnings = 0;
            $totalSuperContributions = 0;
        }else if(!empty($keyPayResponseData) && is_array($keyPayResponseData)){
            $totalGrossEarnings = array_sum(array_column($keyPayResponseData,'grossEarnings'));
            $totalSuperContributions = array_sum(array_column($keyPayResponseData,'superContributions'));
        }
        return ['status'=>true,'total_data'=>$totalGrossEarnings+$totalSuperContributions];
    }
}

