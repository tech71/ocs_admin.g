<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DocuSignEnvelope extends DocuSignAuthConfig { 
	public function __construct()
    {
        require_once APPPATH.'third_party/docusign/vendor/autoload.php';
		require_once APPPATH.'libraries/DocuSignAuthConfig.php';
    }
	
	/*
	Method CreateEnvelope (this method send single document for create envelop)
	
	Request 
	Array([userdetails] => Array ( [name] => Developer Test [email_subject] => Please sign the test document  [email] => developer@yourdevelopmentteam.com.au )
    [document] => Array([doc_id] => 1  [doc_name] => Dev Test  [doc_path] => D:\xampp\htdocs\ocs_admin\admin\back-end\uploads/docs/SignTest122.pdf , [web_hook_url]='https://adminapi.dev.healthcaremgr.net/test_mail.php' )
	[position] => Array ([position_x] => 100 [position_y] => 100 [document_id] => 1 [page_number] => 1 [recipient_id] => 1))
	
	Response 
	Array( [status] => 1 [msg] => Envelope created successfully
    [response] => Array([envelopeId] => e65c7b47-b240-4283-be37-a64b943d437c [status] => sent [statusDateTime] => 2019-09-12T09:59:02.5338440Z [uri] => /envelopes/e65c7b47-b240-4283-be37-a64b943d437c )) */



	public function CreateEnvelope($envlopeDetails){
			$validate_response=$this->validateCreateEnvelope($envlopeDetails);
			if(!$validate_response['status']){
				return $validate_response;
				exit();
			}		
			
			try{
				$userDetails=$envlopeDetails['userdetails'];
				$userDocument=$envlopeDetails['document'];
				$DocumentSignPosition=$envlopeDetails['position'];

				$config=$this->getConfig();			
				$apiClient = new DocuSign\eSign\ApiClient($config);			
            	$authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);            
            	$options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
            	$loginInformation = $authenticationApi->login($options);			
				$loginAccount = $loginInformation->getLoginAccounts()[0];
            	$accountId = $loginAccount->getAccountId();

				if($this->validateAccountId($accountId)){
					return ['status' => false, 'msg' => 'Something went wrong in config details.'];
					exit();
				}
			
				$event_notification=$this->SetEventNotificationDetails($userDocument['web_hook_url']);		

				$document = new \DocuSign\eSign\Model\Document();
				$document->setDocumentId($userDocument['doc_id']);
				$document->setName($userDocument['doc_name']);
				$document->setDocumentBase64(base64_encode(file_get_contents($userDocument['doc_path'])));


				// Here set sign position 
				$signPosition=$this->SignPosition($DocumentSignPosition);
				$tabs = new DocuSign\eSign\Model\Tabs();
				$tabs->setSignHereTabs($signPosition);
			
				// Here details of signer users
				$signerDetails=$this->SignerDetails($tabs,$userDetails);

				$recipients = new \DocuSign\eSign\Model\Recipients();
				$recipients->setSigners($signerDetails);
				//$recipients->setCarbonCopies([$carbon_copy]);

				$envelope_definition = new \DocuSign\eSign\Model\EnvelopeDefinition();
				// We want to use the most friendly email subject line.
				// The regexp below removes the suffix from the file name. 
				$envelope_definition->setEmailSubject($userDetails['email_subject']);
				$envelope_definition->setDocuments([$document]);
				$envelope_definition->setRecipients($recipients);
				$envelope_definition->setEventNotification($event_notification);
				$envelope_definition->setStatus("sent");
				// Send the envelope:
				$envelopesApi = new \DocuSign\eSign\Api\EnvelopesApi($apiClient);
				$envelope_summary = $envelopesApi->createEnvelope($accountId, $envelope_definition, null);
				if ( !isset($envelope_summary) || $envelope_summary->getEnvelopeId() == null ) {
					return ['status' => false, 'msg' => 'Error calling DocuSign'];
				}else{
					return ['status' => true, 'msg' => 'Envelope created successfully','response'=>json_decode((string)$envelope_summary, true)];
				}			
			}catch (DocuSign\eSign\ApiException $e) {
				$error_code = $e->getResponseBody()->errorCode;
				$error_message = $e->getResponseBody()->message;			
				return ['status' => false, 'msg' => $error_message,'error_code' => $error_code];
				exit();
			}
	}
	
	/*
		Method GetEnvelopeInformation (This method get envelope information using envelope id)
		Request 
		$envelopeId (set parameter envelope id only)

		Response 
		Array([status] => 1 [msg] => successfully
    	[response] => Array([allowMarkup] => false [allowReassign] => true [attachmentsUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/attachments
            [autoNavigation] => true [certificateUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/documents/certificate
            [completedDateTime] => 2019-09-12T06:02:06.0830000Z [createdDateTime] => 2019-09-12T05:58:55.0800000Z
            [customFieldsUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/custom_fields
            [deliveredDateTime] => 2019-09-12T06:01:58.6930000Z [documentsCombinedUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/documents/combined
            [documentsUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/documents [emailSubject] => Please sign the test document
            [enableWetSign] => true [envelopeId] => ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0 [envelopeIdStamping] => true
            [envelopeUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0 [initialSentDateTime] => 2019-09-12T05:58:55.7370000Z
            [is21CFRPart11] => false [isSignatureProviderEnvelope] => false [lastModifiedDateTime] => 2019-09-12T05:58:55.0970000Z
            [notificationUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/notification [purgeState] => unpurged
            [recipientsUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/recipients [sentDateTime] => 2019-09-12T05:58:55.7370000Z
            [status] => completed [statusChangedDateTime] => 2019-09-12T06:02:06.0830000Z [templatesUri] => /envelopes/ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0/templates))
	*/

	public function GetEnvelopeInformation($envelopeId) {

		if(empty($envelopeId)){
			return ['status' => false, 'msg' => 'please provide envelope Id!'];
			exit();
		}


		try {
			$config=$this->getConfig();			
			$apiClient = new DocuSign\eSign\ApiClient($config);			
			$authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);            
			$options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
			$loginInformation = $authenticationApi->login($options);			
			$loginAccount = $loginInformation->getLoginAccounts()[0];
			$accountId = $loginAccount->getAccountId();
			if($this->validateAccountId($accountId)){
				return ['status' => false, 'msg' => 'Something went wrong in config details.'];
				exit();
			}

			$envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);
			$options = new \DocuSign\eSign\Api\EnvelopesApi\GetEnvelopeOptions();			
			$envelope = $envelopeApi->getEnvelope($accountId,$envelopeId);
			$results = json_decode((string)$envelope, true);
			if(!empty($results)) {
				return ['status' => true, 'msg' => 'successfully','response' => $results];
				exit();	
			}else{
				return ['status' => false, 'msg' => 'something went wrong'];
				exit();	
			}
			
		} catch (\DocuSign\eSign\ApiException $e) {
			$error_code = $e->getResponseBody()->errorCode;
			$error_message = $e->getResponseBody()->message;			
			return ['status' => false, 'msg' => $error_message,'error_code' => $error_code];
			exit();
		}
	}

	/*
		Method DownloadEnvelopeDocuments (This method download envelope document using envelope id, filepath and filename)
		Request 
		Array([envelopeid] => ac1706e1-3b1e-4b98-a51a-fdc5147fc0c0 [filepath] => D:\xampp\htdocs\ocs_admin\admin\back-end\uploads/docs/  [filename] => mytest123.pdf)

		Response 
		Array([status] => 1 [msg] => successfully)
	*/

	public function DownloadEnvelopeDocuments($envlopDetails)
    {
		
		try {
			$filepath=$envlopDetails['filepath'].$envlopDetails['filename'];
			$document_id=1;
			$config=$this->getConfig();			
			$apiClient = new DocuSign\eSign\ApiClient($config);			
			$authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);            
			$options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
			$loginInformation = $authenticationApi->login($options);			
			$loginAccount = $loginInformation->getLoginAccounts()[0];
			$accountId = $loginAccount->getAccountId();
			if($this->validateAccountId($accountId)){
				return ['status' => false, 'msg' => 'Something went wrong in config details.'];
				exit();
			}
			$envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);
					
			$results = $envelopeApi->getDocument($accountId, $document_id, $envlopDetails['envelopeid']);							
			if(!empty($results->getPathname())){
							
				if(copy($results->getPathname(), $filepath)){
					return ['status' => true, 'msg' => 'successfully'];
					exit();	
				}else{
					return ['status' => false, 'msg' => 'file not exist'];
					exit();	
				}		
				
			}else{
				return ['status' => false, 'msg' => 'something went wrong'];
				exit();	
			}
			
		} catch (\DocuSign\eSign\ApiException $e) {
			$error_code = $e->getResponseBody()->errorCode;
			$error_message = $e->getResponseBody()->message;			
			return ['status' => false, 'msg' => $error_message,'error_code' => $error_code];
			exit();
		}


	}
	
	public function setDocumentDetails(){
		$document = new \DocuSign\eSign\Model\Document();
		$document->setDocumentId("1");
		$document->setName('Test Doc');
		$document->setDocumentBase64(base64_encode(file_get_contents($documentFileName)));
		return [$document];
	}

	public function SetEventNotificationDetails($web_hook_url){
		
		$envelope_events = [
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("delivered"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("completed"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("declined"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("voided"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent"),
			(new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent")
		];

		$recipient_events = [
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Sent"),
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Delivered"),
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Completed"),
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Declined"),
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("AuthenticationFailed"),
			(new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("AutoResponded")
		];
		
		$event_notification = new \DocuSign\eSign\Model\EventNotification();
		$event_notification->setUrl($web_hook_url);
		$event_notification->setLoggingEnabled("true");
		$event_notification->setRequireAcknowledgment("true");
		$event_notification->setUseSoapInterface("false");
		$event_notification->setIncludeCertificateWithSoap("false");
		$event_notification->setSignMessageWithX509Cert("false");
		$event_notification->setIncludeDocuments("true");
		$event_notification->setIncludeEnvelopeVoidReason("true");
		$event_notification->setIncludeTimeZone("true");
		$event_notification->setIncludeSenderAccountAsCustomField("true");
		$event_notification->setIncludeDocumentFields("true");
		$event_notification->setIncludeCertificateOfCompletion("true");
		$event_notification->setEnvelopeEvents($envelope_events);
		$event_notification->setRecipientEvents($recipient_events);
		return $event_notification;

	}
	public function SignerDetails($tabs,$signUser){
		$signer = new \DocuSign\eSign\Model\Signer();
		//$signerDetails['name']='Developer Test';
		//$signerDetails['email_subject']='Please sign the test document';
		//$signerDetails['email']='developer@yourdevelopmentteam.com.au';

		$signer->setEmail($signUser['email']);
		$signer->setName($signUser['name']);
		$signer->setRecipientId("1");
		$signer->setRoutingOrder("1");
		$signer->setTabs($tabs);
		return [$signer];
	}
	public function SignPosition($SignPosition=array()){
		// Create a |SignHere| tab somewhere on the document for the recipient to sign		
		$signHere = new \DocuSign\eSign\Model\SignHere();
		$signHere->setXPosition($SignPosition['position_x']);
		$signHere->setYPosition($SignPosition['position_y']);
		$signHere->setDocumentId($SignPosition['document_id']);
		$signHere->setPageNumber($SignPosition['page_number']);
		$signHere->setRecipientId($SignPosition['recipient_id']);
		return [$signHere];
	}
	/*
	Method validateAccountId
	this method check account id is empty 
	*/

	public function validateAccountId($accountid){
		return $response=isset($accountid)? false:true;		
	}

	/*
	Method validateCreateEnvelope
	this method validate create envelope data is valid or not	
	*/
	public function validateCreateEnvelope($envlopeDetails){
		if(empty($envlopeDetails)){
			return ['status' => false, 'msg' =>'Given Array is empty'];
			exit();
		}
		
		//validate Signer details

		if(!isset($envlopeDetails['userdetails']['name']) || empty($envlopeDetails['userdetails']['name'])){
			return ['status' => false, 'msg' =>'Please provide signer details name !'];
			exit();
		}
		if(!isset($envlopeDetails['userdetails']['email_subject']) || empty($envlopeDetails['userdetails']['email_subject'])){
			return ['status' => false, 'msg' =>'Please provide signer email subject !'];
			exit();
		}
		if(!isset($envlopeDetails['userdetails']['email']) || empty($envlopeDetails['userdetails']['email'])){
			return ['status' => false, 'msg' =>'Please provide signer email !'];
			exit();
		}

		
		// Validate position details

		if(!isset($envlopeDetails['position']['position_x']) || empty($envlopeDetails['position']['position_x'])){
			return ['status' => false, 'msg' =>'Please provide envelope X position !'];
			exit();
		}
		if(!isset($envlopeDetails['position']['position_y']) || empty($envlopeDetails['position']['position_y'])){
			return ['status' => false, 'msg' =>'Please provide envelope Y position !'];
			exit();
		}
		if(!isset($envlopeDetails['position']['document_id']) || empty($envlopeDetails['position']['document_id'])){
			return ['status' => false, 'msg' =>'Please provide envelope document id !'];
			exit();
		}
		if(!isset($envlopeDetails['position']['page_number']) || empty($envlopeDetails['position']['page_number'])){
			return ['status' => false, 'msg' =>'Please provide envelope document page number !'];
			exit();
		}
		if(!isset($envlopeDetails['position']['recipient_id']) || empty($envlopeDetails['position']['recipient_id'])){
			return ['status' => false, 'msg' =>'Please provide envelope recipient id !'];
			exit();
		}

		// Validate document details

		if(!isset($envlopeDetails['document']['doc_id']) || empty($envlopeDetails['document']['doc_id'])){
			return ['status' => false, 'msg' =>'Please provide document id !'];
			exit();
		}
		if(!isset($envlopeDetails['document']['doc_name']) || empty($envlopeDetails['document']['doc_name'])){
			return ['status' => false, 'msg' =>'Please provide document name !'];
			exit();
		}
		if(!isset($envlopeDetails['document']['web_hook_url']) || empty($envlopeDetails['document']['web_hook_url'])){
			return ['status' => false, 'msg' =>'Please provide web hook url !'];
			exit();
		}
		

		if(!isset($envlopeDetails['document']['doc_path']) || empty($envlopeDetails['document']['doc_path'])){
			return ['status' => false, 'msg' =>'Please provide document path !'];
			exit();
		}else{
			$absPath=realpath($envlopeDetails['document']['doc_path']);
			if($absPath === false) {
				return ['status' => false, 'msg' =>'Please provide valid document path !'];
				exit();
			}
		}
		return ['status' => true];
		exit();
	}
	
	public function ResendEnvelope($envlopeDetails){
		$validate_response=$this->validateResendEnvelope($envlopeDetails);
		if(!$validate_response['status']){
			return $validate_response;
			exit();
		}		
		
		try{
			
			$userDetails=$envlopeDetails['userdetails'];
			$envelope_id=$envlopeDetails['envelopeId'];
			//$userDocument=$envlopeDetails['document'];
			$DocumentSignPosition=$envlopeDetails['position'];

			$config=$this->getConfig();			
			$apiClient = new DocuSign\eSign\ApiClient($config);			
			$authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);            
			$options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();
			$loginInformation = $authenticationApi->login($options);			
			$loginAccount = $loginInformation->getLoginAccounts()[0];
			$accountId = $loginAccount->getAccountId();

			if($this->validateAccountId($accountId)){
				return ['status' => false, 'msg' => 'Something went wrong in config details.'];
				exit();
			}
		
			
			// Here set sign position 
			$signPosition=$this->SignPosition($DocumentSignPosition);
			$tabs = new DocuSign\eSign\Model\Tabs();
			$tabs->setSignHereTabs($signPosition);
		
			// Here details of signer users
			$signerDetails=$this->SignerDetails($tabs,$userDetails);

			$recipients = new \DocuSign\eSign\Model\Recipients();
			$recipients->setSigners($signerDetails);
			
			$envelopesApi = new \DocuSign\eSign\Api\EnvelopesApi($apiClient);
						
			$updateRecipientsOptions = new \DocuSign\eSign\Api\EnvelopesApi\UpdateRecipientsOptions();				
			$updateRecipientsOptions->setResendEnvelope('true');
			$envelope_summary = $envelopesApi->updateRecipients($accountId,$envelope_id,$recipients,$updateRecipientsOptions); 
			
			$error_details=$envelope_summary->getRecipientUpdateResults();
						
			if (isset($error_details[0]['error_details']['error_code']) || $error_details[0]['error_details']['error_code'] == 'SUCCESS' ) {
				return ['status' => true, 'msg' => 'Resend Envelope successfully','response'=>json_decode((string)$envelope_summary, true)];
				exit();
			}else{
				return ['status' => false, 'msg' => 'Error calling DocuSign'];
				exit();
			}			
		}catch (DocuSign\eSign\ApiException $e) {
			$error_code = $e->getResponseBody()->errorCode;
			$error_message = $e->getResponseBody()->message;			
			return ['status' => false, 'msg' => $error_message,'error_code' => $error_code];
			exit();
		}
}

public function validateResendEnvelope($envlopeDetails){
	if(empty($envlopeDetails)){
		return ['status' => false, 'msg' =>'Given Array is empty'];
		exit();
	}
	
	//validate Signer details

	if(!isset($envlopeDetails['userdetails']['name']) || empty($envlopeDetails['userdetails']['name'])){
		return ['status' => false, 'msg' =>'Please provide signer details name !'];
		exit();
	}
	//if(!isset($envlopeDetails['userdetails']['email_subject']) || empty($envlopeDetails['userdetails']['email_subject'])){
	//	return ['status' => false, 'msg' =>'Please provide signer email subject !'];
	//	exit();
	//}
	if(!isset($envlopeDetails['userdetails']['email']) || empty($envlopeDetails['userdetails']['email'])){
		return ['status' => false, 'msg' =>'Please provide signer email !'];
		exit();
	}

	
	// Validate position details

	if(!isset($envlopeDetails['position']['position_x']) || empty($envlopeDetails['position']['position_x'])){
		return ['status' => false, 'msg' =>'Please provide envelope X position !'];
		exit();
	}
	if(!isset($envlopeDetails['position']['position_y']) || empty($envlopeDetails['position']['position_y'])){
		return ['status' => false, 'msg' =>'Please provide envelope Y position !'];
		exit();
	}
	if(!isset($envlopeDetails['position']['document_id']) || empty($envlopeDetails['position']['document_id'])){
		return ['status' => false, 'msg' =>'Please provide envelope document id !'];
		exit();
	}
	if(!isset($envlopeDetails['position']['page_number']) || empty($envlopeDetails['position']['page_number'])){
		return ['status' => false, 'msg' =>'Please provide envelope document page number !'];
		exit();
	}
	if(!isset($envlopeDetails['position']['recipient_id']) || empty($envlopeDetails['position']['recipient_id'])){
		return ['status' => false, 'msg' =>'Please provide envelope recipient id !'];
		exit();
	}

	// Validate document details
	
	if(!isset($envlopeDetails['envelopeId']) || empty($envlopeDetails['envelopeId'])){
		return ['status' => false, 'msg' =>'Please provide envelope id !'];
		exit();
	}
	
	return ['status' => true];
	exit();
}


}
?>