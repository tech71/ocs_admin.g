<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function calculate_gst($amount) {
    $gst = 0;
    if ($amount > 0) {
        $gst = ($amount * INVOICE_GST) / 100;
        #$gst = round($gst);
    }
    return $gst;
}
