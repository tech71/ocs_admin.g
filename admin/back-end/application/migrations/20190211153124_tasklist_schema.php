<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Tasklist_schema extends CI_Migration
{
    public function up()
    {
        // $this->dbforge->add_field('`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
        // $this->dbforge->add_field('`key` VARCHAR(32) NOT NULL');
        // $this->dbforge->add_field('`display_name` VARCHAR(64) NOT NULL');
        // $this->dbforge->add_field('`description` VARCHAR(256) NOT NULL');
        // $this->dbforge->create_table('task_status');

        // $this->dbforge->add_field('`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY');
        // $this->dbforge->add_field('`created_by` INT(5) NOT NULL');
        // $this->dbforge->add_field('`subject` VARCHAR(64) NOT NULL');
        // $this->dbforge->add_field('`description` VARCHAR(256) NOT NULL');
        // $this->dbforge->add_field('`assigned_to_id` INT(11) UNSIGNED NOT NULL');
        // $this->dbforge->add_field('`assigned_to_type` VARCHAR(32) NOT NULL');
        // $this->dbforge->add_field('`status_id` INT(11) UNSIGNED NOT NULL');
        // $this->dbforge->add_field('`cancel_reason` VARCHAR(64)');
        // $this->dbforge->add_field('`created_at` INT(11) UNSIGNED');
        // $this->dbforge->add_field('FOREIGN KEY (`created_by`) REFERENCES `tbl_admin` (`id`)');
        // $this->dbforge->add_field('FOREIGN KEY (`status_id`) REFERENCES `task_status` (`id`)');
        // $this->dbforge->create_table('tasks');
    }

    public function down()
    {
        // $this->dbforge->drop_table('tasks');
        // $this->dbforge->drop_table('task_status');
    }
}
