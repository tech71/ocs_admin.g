<?php

namespace ClassApplicantDocs;

/*
 * Filename: applicantDocs.php
 * Desc: Docs details of applicant like filename, type etc.
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: ClassApplicantDocs
 * Desc: This Class is for maintaining docs details of applicant.
 * Created: 02-08-2018 
 */

class ApplicantDocs {

    /**
     * @var applicantdocsid
     * @access private
     * @vartype: int
     */
    private $applicantdocsid;

    /**
     * @var applicantId
     * @access private
     * @vartype: int
     */
    private $applicantId;

    /**
     * @var stageId
     * @access private
     * @vartype: int
     */
    private $stageId;
    /**
     * @var stageId
     * @access private
     * @vartype: int
     */
    private $isMainStage;

    /**
     * @var title
     * @access private
     * @vartype: varchar
     */
    private $title;

    /**
     * @var filename
     * @access private
     * @vartype: varchar
     */
    private $filename;

    /**
     * @var created
     * @access private
     * @vartype: varchar
     */
    private $created;
    private $createdBy;
    private $archive;

     

    /**
     * @var categoryType
     * @access private
     * @vartype: int
     */
    private $categoryType;

    /**
     * @function getApplicantdocsid
     * @access public
     * @returns $oc_departments int
     * Get applicant Docs Id
     */
    public function getApplicantdocsid() {
        return $this->applicantdocsid;
    }

    /**
     * @function setApplicantdocsid
     * @access public
     * @param $applicantdocsid tinyint 
     * Set applicant Docs Id
     */
    public function setApplicantdocsid($applicantdocsid) {
        $this->applicantdocsid = $applicantdocsid;
    }

    /**
     * @function getApplicantid
     * @access public
     * @returns $applicantid int
     * Get applicant Id
     */
    public function getApplicantId() {
        return $this->applicantId;
    }

    /**
     * @function setApplicantid
     * @access public
     * @param $applicantid int 
     * Set applicant Id
     */
    public function setApplicantId($applicantId) {
        $this->applicantId = $applicantId;
    }

    /**
     * @function getStage
     * @access public
     * @returns $stageId tinyint
     * Get Type
     */
    public function getStage() {
        return $this->stageId;
    }

    /**
     * @function setStage
     * @access public
     * @param $stageId tinyint 
     * Set Type
     */
    public function setStage($stageId) {
        $this->stageId = $stageId;
    }
    /**
     * @function getIsMainStage
     * @access public
     * @returns $isMainStage tinyint
     * Get Type
     */
    public function getIsMainStage() {
        return (int)$this->isMainStage;
    }

    /**
     * @function setIsMainStage
     * @access public
     * @param $isMainStage tinyint 
     * Set Type
     */
    public function setIsMainStage($isMainStage) {
        $this->isMainStage = (int) $isMainStage;
    }

    /**
     * @function getTitle
     * @access public
     * @returns $title varchar
     * Get Title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @function setTitle
     * @access public
     * @param $title varchar
     * Set Title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @function getFilename
     * @access public
     * @returns $filename varchar
     * Get Filename
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * @function setFilename
     * @access public
     * @param $filename varchar
     * Set Filename
     */
    public function setFilename($filename) {
        $this->filename = $filename;
    }

    /**
     * @function getCreated
     * @access public
     * @returns $created varchar
     * Get Created
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @function setCreated
     * @access public
     * @param $created varchar
     * Set Created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    function setArchive($archive) {
        $this->archive = $archive;
    }

    function getArchive() {
        return $this->archive;
    }

    /**
     * @function getCategoryType
     * @access public
     * @returns $categoryType tinyint
     * Get Type
     */
    public function getCategoryType() {
        return (int) $this->categoryType;
    }

    /**
     * @function setCategoryType
     * @access public
     * @param $categoryType tinyint 
     * Set Type
     */
    public function setCategoryType($categoryType) {
        $this->categoryType = (int) $categoryType;
    }

     /**
     * @function getCreated
     * @access public
     * @returns $created varchar
     * Get Created
     */
    public function getCreatedBy() {
        return $this->createdBy;
    }

    /**
     * @function setCreated
     * @access public
     * @param $created varchar
     * Set Created
     */
    public function setCreatedBy($created_by) {
        $this->createdBy = (int)$created_by;
    }

    function checkDublicateDocs() {
        $ci = & get_instance();
        $where = array('attachment' => $this->filename,
            'applicant_id' => $this->applicantId,
            'archive' => $this->archive,
        );

        $result = $ci->basic_model->get_row('recruitment_applicant_stage_attachment', array('attachment'), $where);
        if (!empty($result)) {
            $return = array('status' => false, 'warn' => 'Warning: Document with this name already exist, we are changing its name.');
        } else {
            $return = array('status' => true);
        }
        return $return;
    }

    function createFileData() {
        $ci = & get_instance();
        
        $insert_ary = array('attachment' => $this->filename,
            'applicant_id' => $this->applicantId,
            'attachment_title' => $this->title,
            'stage' => $this->stageId,
            'created' => $this->created,
            'created_by' => $this->createdBy,
            'archive' => $this->archive,
            'doc_category' => $this->categoryType,
            'is_main_stage_label' => $this->isMainStage,
        );
        return $rows = $ci->basic_model->insert_records('recruitment_applicant_stage_attachment', $insert_ary, $multiple = FALSE);
    }

}
