<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//include APPPATH . 'Classes/admin/jwt_helper.php';

//class Master extends MX_Controller
class Recruitment_device extends MX_Controller {

    use formCustomValidation;

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Recruitment_device_model');
        $this->load->model('Basic_model');
        $this->load->helper('i_pad');
        $this->form_validation->CI = & $this;
      
        if('ipad_valid_login'== $this->uri->segment(3)){

        }else{
            $reqData=api_request_handler();
            auth_login_status($reqData);
        }
      
    }

    public function update_applicant_info(){		
       $reqData=(array) api_request_handler();      
       $response = $this->validate_applicant_data($reqData);     
       if($response['status']){
            $response_data=$this->Recruitment_device_model->update_applicant_profile_info($reqData);
            if($response_data){
                echo json_encode(["status"=>true,"success"=>"Updated successfully"]);
                exit();
            }else{
                echo json_encode(["status"=>false,"error"=>system_msgs('something_went_wrong')]);
                exit();
            }
       }else{
            echo json_encode($response);
            exit();
       }
    }
    public function get_state_list(){
       $state_list=$this->Recruitment_device_model->state_list();
       $response = array('status' => true, 'data' => $state_list);
       echo json_encode($response);
       exit();
    }
    public function ipad_valid_login(){

        $reqData=(array) api_request_handler();      
        $response = $this->validate_device_data($reqData); 
    
        if($response['status']){
            $response_data=$this->Recruitment_device_model->device_validate_login($reqData);
            if(!empty($response_data)){
                $question_list=[];
                $question_status=$this->Recruitment_device_model->check_question_assigned_to_applicant($response_data);
                if(!$question_status){
                    $question_list=$this->Recruitment_device_model->get_question_assigned_applicant($response_data);
                }               
                $applicant_email=$reqData['email'];               
                $token = generate_token($applicant_email.DATE_TIME);
                $response_data['token']=$token;
                                
                $result=$this->Recruitment_device_model->insert_token_details($response_data);
                if($result){
                    $applicant_data=$this->Recruitment_device_model->get_applicant_info($response_data['applicant_id']);
                    $response_data['applicant_info']=$applicant_data;
                    echo json_encode(["status"=>true,"success"=>"login successfully",'data' =>$response_data]);
                    exit();
                }else{
                    echo json_encode(["status"=>false,"error"=> system_msgs('something_went_wrong')]);
                    exit();
                }                
            }else{
                echo json_encode(["status"=>false,"error"=>"login failed"]);
                exit();
            }
        } else{
            echo json_encode($response);
            exit();
        }
    }
    
    public function get_question_list(){
        $reqData=(array) api_request_handler();      
        $response = $this->validate_task_data($reqData); 
    
        if($response['status']){
            $taskId=$reqData['task_id'];
            $applicant_id=$reqData['applicant_id']; 
            $interview_type=$reqData['interview_type_id'];
            $available_timing=$this->Recruitment_device_model->check_exam_start_remaining_timing_available($applicant_id,$taskId);
            if($available_timing==0){
               echo json_encode(["status"=>false,"error"=> system_msgs('remaining_interview_start_time')]);
               exit();
            }
            $response_data=$this->Recruitment_device_model->device_question_list($reqData);
            if(!empty($response_data['status'])){
                echo json_encode(["status"=>true,"success"=>"get record successfully",'data' =>$response_data['data']]);
                exit();
            }else{
                echo json_encode($response_data);
                exit();
            } 
        } else{
            echo json_encode($response);
            exit();
        }
    }
    public function submit_answers_list(){

        $reqData=(array) api_request_handler();      
        // $response = $this->validate_submit_answers_data($reqData); 
    
        if(!empty($reqData['questions'])){
            $response_data=$this->Recruitment_device_model->submit_answer_list($reqData);
            if(!empty($response_data['status'])){
                echo json_encode($response_data);
                exit();
            }else{
                echo json_encode($response_data);
                exit();
            } 
        } else{
            echo json_encode(["status"=>false,"error"=>"submit empty question"]);
            exit();
        }
    }

    public function validate_applicant_data($applicant_data){
      try {
           // $this->form_validation->set_message('greater_than', 'This %s is not exist ');
            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'Applicant id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'applicant_email', 'label' => 'Applicant email', 'rules' => 'required|valid_email|callback_check_email_already_exist_to_another_applicant['.$applicant_data['applicant_id'].']'),
                array('field' => 'applicant_phone', 'label' => 'Applicant phone', 'rules' => 'required|min_length[8]|max_length[18]|callback_check_phone_already_exist_to_another_applicant['.$applicant_data['applicant_id'].']'),
                array('field' => 'applicant_address', 'label' => 'Applicant id', 'rules' => 'callback_check_recruitment_applicant_address[applicant_address]'),            
            );
        //|callback_phone_number_check[applicant_phone,reuired,Applicant contact info should be enter valid phone number.]
            $this->form_validation->set_data($applicant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();                
                //$key=array_key_first($errors);
                $key=current(array_keys($errors));
                $msg=$errors[$key];                
                $return = array('status' => false, 'error' =>$msg ); //implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }
    
    public function validate_device_data($device_data){
        try {
            $validation_rules = array(
                array('field' => 'email', 'label' => 'Applicant email', 'rules' => 'required|valid_email'),
                array('field' => 'device_pin', 'label' => 'Applicant device pin', 'rules' => 'required|min_length[8]|max_length[8]'),
            );
        //|callback_phone_number_check[applicant_phone,reuired,Applicant contact info should be enter valid phone number.]
            $this->form_validation->set_data($device_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return; 
     }

    public function validate_task_data($task_data){
        try {
            $validation_rules = array(
                array('field' => 'task_id', 'label' => 'Task id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'applicant_id', 'label' => 'Applicant id', 'rules' => 'required|greater_than[0]'),
            );
            //|callback_phone_number_check[applicant_phone,reuired,Applicant contact info should be enter valid phone number.]
            $this->form_validation->set_data($task_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }
    
    public function get_cab_day_task_applicant_specific_details(){
            $reqData=(array) api_request_handler();   
            echo json_encode($response_data=$this->Recruitment_device_model->get_cab_day_task_applicant_specific_details($reqData));
            exit();
        
    }
    
    public function log_out(){
        $reqData=(array) api_request_handler();
        $applicant_id=addslashes($reqData['applicant_id']);
        $response_data=$this->Recruitment_device_model->remove_token($applicant_id);
        echo json_encode(array('status' => true,"success"=>"logout successfully"));
    }

    public function view_draft_contract(){
        $reqData=(array) api_request_handler();
        
        $response = $this->validate_draft_contract_data($reqData);     
        if($response['status']){
            $task_applicant_id=addslashes($reqData['task_applicant_id']);
            $applicant_id=addslashes($reqData['applicant_id']);
            $interview_type=addslashes($reqData['interview_type_id']);

            $response_data=$this->Recruitment_device_model->get_draft_contract_data($task_applicant_id);      
                if(!empty($response_data)){
                        if(!empty($response_data['contract_unsigned_file'])){                
                            $contract_path='';
                            if($interview_type==1){ $filePath=GROUP_INTERVIEW_CONTRACT_PATH; 
                                $contract_path=base_url('mediaShowProfile/rg/'.urlencode(base64_encode($applicant_id)).'/'.urlencode(base64_encode($response_data['contract_unsigned_file'])));
                            }
                            if($interview_type==2){$filePath=CABDAY_INTERVIEW_CONTRACT_PATH;
                                $contract_path=base_url('mediaShowProfile/rc/'.urlencode(base64_encode($applicant_id)).'/'.urlencode(base64_encode($response_data['contract_unsigned_file'])));
                            }
                            $fileFCPath=FCPATH.$filePath.$response_data['contract_unsigned_file'];
                            if(file_exists($fileFCPath)){                 
                                //$response_data['filepath']= base_url().$filePath.$response_data['contract_unsigned_file'];
                                $response_data['filepath']=$contract_path;
                                echo json_encode(array('status' => true , 'success'=>"get record successfully" ,'data'=>$response_data));
                                exit();
                            }else{
                                echo json_encode(array('status' => false, 'error' => "file not found"));
                                exit();
                            }

                        }else{
                            echo json_encode(array('status' => false, 'error' => "file name not not exist"));
                            exit();
                        }
                }else{
                    echo json_encode(array('status' => false, 'error' => "draft contract data not exist"));
                    exit();
                }
        }
        echo json_encode($response);
        exit();
    }

    private function validate_draft_contract_data($task_data){
        try {
            $validation_rules = array(
                array('field' => 'task_applicant_id', 'label' => 'Task applicant id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'applicant_id', 'label' => 'Applicant id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'interview_type_id', 'label' => 'interview type', 'rules' => 'required|greater_than[0]'),
            );
            //|callback_phone_number_check[applicant_phone,reuired,Applicant contact info should be enter valid phone number.]
            $this->form_validation->set_data($task_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    public function send_draft_contract(){
        $reqData=(array) api_request_handler();
        $response = $this->validate_send_draft_contract_data($reqData);     
        if($response['status']){
            $contract_file_id=addslashes($reqData['contract_file_id']);
            $applicant_id=addslashes($reqData['applicant_id']);
            $interview_type=addslashes($reqData['interview_type_id']);
            $contract_email=addslashes($reqData['contract_send_mail']);
            $contract_response=$this->Recruitment_device_model->get_contract_file_by_id($contract_file_id); 
            $contract_applicant=$this->Recruitment_device_model->get_applicant_details($applicant_id);
            if(!empty($contract_response)){
                if(!empty($contract_response['contract_unsigned_file'])){ 
                    $interview='';
                    
                    if($interview_type==1){ $filePath=GROUP_INTERVIEW_CONTRACT_PATH; $interview='Group Interview';  }
                    if($interview_type==2){$filePath=CABDAY_INTERVIEW_CONTRACT_PATH; $interview='Cab Dat Interview'; }
                    
                    $fileFCPath=FCPATH.$filePath.$contract_response['contract_unsigned_file'];
                        if(file_exists($fileFCPath)){  
                            $contract_applicant['filepath']=$fileFCPath;
                            $contract_applicant['email']=$contract_email;
                            $contract_applicant['intreview']=$contract_email;                        
                            send_IPAD_draft_contract_email($contract_applicant);
                            echo json_encode(array('status' => true , 'success'=>"draft contract send successfully"));
                            exit();
                        }else{
                            echo json_encode(array('status' => false, 'error' => "file not found"));
                            exit();
                        }
                }else{
                    echo json_encode(array('status' => false, 'error' => "file name not not exist"));
                    exit();
                }
            }else{
                echo json_encode(array('status' => false, 'error' => "draft contract data not exist"));
                exit();
            } 
        }else{
            echo json_encode($response);
            exit();
        }
       
    }
    private function validate_send_draft_contract_data($task_data){
        try {
            $validation_rules = array(
                array('field' => 'contract_file_id', 'label' => 'contract file id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'applicant_id', 'label' => 'Applicant id', 'rules' => 'required|greater_than[0]'),
                array('field' => 'interview_type_id', 'label' => 'interview type', 'rules' => 'required|greater_than[0]'),
                array('field' => 'contract_send_mail', 'label' => 'contract mail', 'rules' => 'required|trim|valid_email'),
            );            
            $this->form_validation->set_data($task_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }
    public function view_recruitment_presentation(){
        $reqData=(array) api_request_handler();
        $response = $this->validate_recruitment_presentation($reqData);     
        if($response['status']){                      
            $interview_type=addslashes($reqData['interview_type_id']);
            $response_data=$this->Recruitment_device_model->get_recruitment_presentation($interview_type);   
            //echo json_encode($response_data);
            
            $presentaionFCPath=FCPATH.IPAD_DEVICE_PRESENTATION_PATH.$response_data['file_name'];
            if(file_exists($presentaionFCPath)){               
                $presentation_path=base_url('mediaShowProfile/rp/'.urlencode(base64_encode($response_data['file_name'])));
                $response_data['file_path']=$presentation_path;                
                echo json_encode(array('status' => true , 'success'=>"get record successfully" ,'data'=>$response_data));
                exit();
            }else{
                echo json_encode(array('status' => false, 'error' => "file not found"));
                exit();
            }
        }else{
            echo json_encode($response);
            exit();
        }   
        
    }
    private function validate_recruitment_presentation($task_data){
        try {
            $this->form_validation->set_message('less_than', 'This %s is not exist ');
            $this->form_validation->set_message('greater_than', 'This %s is not exist ');
            $validation_rules = array(                
                array('field' => 'interview_type_id', 'label' => 'interview type', 'rules' => 'required|greater_than[0]|less_than[3]'),                
            );            
            $this->form_validation->set_data($task_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {        
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

}
