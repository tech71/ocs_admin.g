<?php

require_once APPPATH . 'Classes/admin/permission.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_device_model extends CI_Model {
    
    public function update_applicant_profile_info($participant){ 
        $this->db->trans_begin();
        $phone_response=$this->update_applicant_phone($participant['applicant_id'],$participant['applicant_phone']);
       // $this->update_applicant_email($participant['applicant_id'],$participant['applicant_email']);
        $res_address= $this->update_applicant_address($participant['applicant_id'],$participant['applicant_address']);        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update_applicant_phone($applicant_id,$applicant_phone){       
        $this->basic_model->update_records('recruitment_applicant_phone',array('phone' => $applicant_phone), array('applicant_id' => $applicant_id));       
    }
    public function update_applicant_email($applicant_id,$applicant_email){      
        $this->basic_model->update_records('recruitment_applicant_email',array('email' => $applicant_email), array('applicant_id' => $applicant_id));        
    }
    public function update_applicant_address($applicant_id,$applicant_address){
       
       $this->db->select(array("id"));
       $this->db->from("tbl_recruitment_applicant_address");
       $this->db->where("applicant_id='".$applicant_id."'");        
       $res= $this->db->get();
       $res_array=$res->row_array();           
       if(!empty($res_array)){
            $this->basic_model->update_records('recruitment_applicant_address',$applicant_address, array('applicant_id' => $applicant_id));
        }else{
            $applicant_address->applicant_id=$applicant_id;
            $applicant_address->created=DATE_TIME;
            $applicant_address->updated=DATE_TIME;
            $applicant_address->primary_address=1;        
            return $result=$this->db->insert('tbl_recruitment_applicant_address',$applicant_address);
        }       
    }

    function state_list() {
        $where_array['archive'] = '0';
        $columns = array('id','name');
        $this->db->select($columns);
        $this->db->where($where_array);
        $this->db->from('tbl_state');        
        $res = $this->db->get();
        return $res->result_array();
    }

    public function device_validate_login($applicant_data){
         $applicant_email=$this->db->escape($applicant_data['email']);
         $applicant_device_pin=$this->db->escape($applicant_data['device_pin']); 
         //echo $this->input->post('device_pin');
        
        $tbl_rec_app_group_cab_int_detail=TBL_PREFIX.'recruitment_applicant_group_or_cab_interview_detail as interview';       
        $tbl_rec_task=TBL_PREFIX.'recruitment_task as task';
        $tbl_rec_task_app=TBL_PREFIX.'recruitment_task_applicant as rta';
        $tbl_rec_app=TBL_PREFIX.'recruitment_applicant as ra';
        $tbl_rec_app_email=TBL_PREFIX.'recruitment_applicant_email as rae';
        $tbl_rec_interview_type=TBL_PREFIX.'recruitment_interview_type as rit';
        
        $coulumns_rec_task=array('task.id as task_id','task.task_name','rta.id as task_applicant_id','rta.applicant_id','interview.interview_type as interview_type_id','rit.name as interview_type','interview.device_pin');          
        $query = $this->db->select($coulumns_rec_task);        
        $this->db->from($tbl_rec_task);
        $this->db->join($tbl_rec_task_app,'rta.taskId = task.id  and task.status=1', 'inner');
        $this->db->join($tbl_rec_app_group_cab_int_detail, 'interview.recruitment_task_applicant_id = rta.id and interview.archive=0 and interview.mark_as_no_show=0', 'inner');
        $this->db->join($tbl_rec_app, 'ra.id = rta.applicant_id and ra.archive=0', 'inner');
        $this->db->join($tbl_rec_app_email,'rae.applicant_id = ra.id and rae.archive=0 and rae.primary_email=1', 'inner');
        $this->db->join($tbl_rec_interview_type,'rit.id = interview.interview_type', 'inner');

        $this->db->where("interview.device_pin=".$applicant_device_pin."");
        $this->db->where("rae.email=".$applicant_email."");
        
        $res = $this->db->get();       
        $res_array=$res->row_array();
        $rows = $res->num_rows();  
        if($rows>0){
            if($res_array['device_pin']==$applicant_data['device_pin']){
                unset($res_array['device_pin']);
                return $res_array;
            }       
        }
        return [];        
    }
    
        public function get_applicant_info($applicant_id){
            $applicant_id=addslashes($applicant_id);
            $applicant_info=[];

            $sql="SELECT email AS applicant_email, 
            (SELECT phone FROM tbl_recruitment_applicant_phone WHERE applicant_id=$applicant_id  AND archive=0 AND primary_phone=1) as applicant_phone , 
             (SELECT GROUP_CONCAT(`firstname`,' ',`lastname` ) applicant_name FROM tbl_recruitment_applicant WHERE id=$applicant_id
             AND archive=0 ) as applicant_name FROM `tbl_recruitment_applicant_email` WHERE 
             tbl_recruitment_applicant_email.applicant_id=$applicant_id AND primary_email=1 AND archive=0";
                        
            $exe_query = $this->db->query($sql);
            $rows = $exe_query->num_rows();        
            if($rows > 0){
                $records = $exe_query->row_array();  
                $applicant_info=$records;                         
                
                $coulumns_address=array('street','city','postal','state');          
                $query = $this->db->select($coulumns_address);        
                $this->db->from('tbl_recruitment_applicant_address');            
                $this->db->where("applicant_id='".$applicant_id."' and primary_address=1");            
                $res = $this->db->get();        
                $response= $res->row_array(); 
                $applicant_info['applicant_address']=$response;                
            }
            return $applicant_info;           
        }

           
    // Get question list and save to table tbl_recruitment_additional_questions_for_applicant
    public function check_question_assigned_to_applicant($applicant_data){
            
            $this->db->select(array("question_id"));
            $this->db->from("tbl_recruitment_additional_questions_for_applicant");
            $this->db->where("recruitment_task_applicant_id='".$applicant_data['task_applicant_id']."' AND archive=0");        
            $res= $this->db->get();
            $res_array=$res->row_array();           
            if(!empty($res_array)){
                return true;
            }else{
                return false;
            }
    }
    public function get_question_assigned_applicant($applicant_data){
        $applicant_id=$applicant_data['applicant_id']; 
        $applicant_stage=$this->get_applicant_currunt_stage($applicant_id);
       
        // if stage 3 then traning category is Group Interview there id is 1 get all question        
        if($applicant_stage=='group_interview'){
            $training_category=1;        
            return $this->device_question_list_by_stage($applicant_data,$training_category);
        }

        // if stage 6 then traning category is Cab Day Interview there id is 2 and max question is 5
        if($applicant_stage=='cab_day'){
            $training_category=2;
            $limit=5;
           return $this->device_question_list_by_stage($applicant_data,$training_category,$limit);            
        }
        return ['status' => false, 'error' => "No records found"];
    }

    public function device_question_list_by_stage($applicant_data,$training_category,$limit=0){
        $taskId=addslashes($applicant_data['task_id']);
        $applicant_id=addslashes($applicant_data['applicant_id']); 
        $interview_type=addslashes($applicant_data['interview_type_id']);
        
        $data = $this->basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'),$id_array = array('taskId'=>$taskId,'applicant_id'=>$applicant_id));
        
        $recruitment_task_applicant_id = $data->id;
        if($recruitment_task_applicant_id>0){
                    
        $limit_query=($limit>0)? 'LIMIT '. $limit:'';

        $all_ques_sql = "SELECT raqfa.id,raqfa.created_by,raqfa.created, GROUP_CONCAT(raqa.answer SEPARATOR  '@#_BREAKER_#@') as answer_correct,
        GROUP_CONCAT(raqa.id SEPARATOR  '@#_BREAKER_#@') as answer_option_id, 
        GROUP_CONCAT(raqa.question_option SEPARATOR  '@#_BREAKER_#@') as answer_option, 
        GROUP_CONCAT(raqa.serial SEPARATOR  '@#_BREAKER_#@') as que_serial,raqfa.question, raqfa.question_type, raqfa.question_topic as 'question_topic' ,
        qtopic.topic
        FROM `tbl_recruitment_additional_questions` as raqfa
        INNER JOIN `tbl_recruitment_additional_questions_answer` as `raqa` ON `raqa`.`question` = `raqfa`.`id` 
        INNER JOIN `tbl_recruitment_question_topic` as `qtopic` ON  `qtopic`.`id`=`raqfa`.`question_topic`
        WHERE NOT EXISTS (SELECT nass.question_id
                    FROM tbl_recruitment_applicant_not_assign_question as nass
                    WHERE  nass.question_id = raqfa.id AND nass.archive = 0 and nass.recruitment_task_applicant_id=$recruitment_task_applicant_id) AND raqfa.archive = 0 AND raqfa.status = 1 AND raqfa.training_category = '".$training_category."'  AND raqa.archive = 0
        group by raqfa.id ORDER BY RAND()".$limit_query;
        
        $exe_query = $this->db->query($all_ques_sql);
        $rows = $exe_query->num_rows();        
        if($rows > 0){
                $records = $exe_query->result_array();        
                $main_aray = [];

                foreach ($records as $key => $value)
                {   
                   /* $temp['id']=$value['id'];
                    $temp['question'] = $value['question'];            
                    $temp['question_type'] = $value['question_type'];                    
                    //if($value['question_type']!=4){                        
                        $answer_option_id = explode('@#_BREAKER_#@', $value['answer_option_id']);
                        $answer_option = explode('@#_BREAKER_#@', $value['answer_option']);                       
                        $answer_option_combine=array_combine($answer_option_id,$answer_option); 
                        $arrAnswer=[];
                        foreach($answer_option_combine as $oKey=>$oValue){
                            $optAnswer=[];
                            $optAnswer['id']=$oKey;
                            $optAnswer['option']=$oValue;
                            $optAnswer['answered']=false;
                            $arrAnswer[]=$optAnswer;
                        }                        
                        $temp['answer_option']=$arrAnswer;
                   // }
                    $main_aray[$value['question_topic']]['id'] = $value['question_topic'];
                    $main_aray[$value['question_topic']]['name'] = $value['topic'];
                    $main_aray[$value['question_topic']]['question_ary'][] = $temp; */
                    $this->copy_additional_questions($value['id'],$recruitment_task_applicant_id);
                }
                return ['status' => true]; //, 'data' => array("questions"=>$main_aray)];    
        }else{
            return ['status' => false, 'error' => "No records found"];
        } 
    }else{
        return ['status' => false, 'error' => "Recruitment task applicant id not found"];
    }
    }



    public function device_question_list($applicant_data){
        $applicant_id=$applicant_data['applicant_id']; 
        $applicant_stage=$this->get_applicant_currunt_stage($applicant_id);
   
        // if stage 3 then traning category is Group Interview there id is 1 get all question        
        if($applicant_stage=='group_interview'){
            $training_category=1;        
            return $this->get_device_question_list_by_stage($applicant_data,$training_category);
        }

        // if stage 6 then traning category is Cab Day Interview there id is 2 and max question is 5
        if($applicant_stage=='cab_day'){
            $training_category=2;
            $limit=5;
            return $this->get_device_question_list_by_stage($applicant_data,$training_category,$limit);            
        }

        return ['status' => false, 'error' => "No records found"];
}

public function get_device_question_list_by_stage($applicant_data,$training_category,$limit=0){
    
    $taskId=addslashes($applicant_data['task_id']);
    $applicant_id=addslashes($applicant_data['applicant_id']); 
    $interview_type=addslashes($applicant_data['interview_type_id']);
    
    $data = $this->basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'),$id_array = array('taskId'=>$taskId,'applicant_id'=>$applicant_id));
    
    
    if(!empty($data->id) && ($data->id>0)){
        $recruitment_task_applicant_id = $data->id;

        $question_data = $this->basic_model->get_row('recruitment_additional_questions_for_applicant', array("GROUP_CONCAT(question_id) as question_id"),array('recruitment_task_applicant_id'=>$recruitment_task_applicant_id,'archive'=>0));
      
        if(!empty($question_data->question_id)){
            $question_id=$question_data->question_id;
            
            $limit_query=($limit>0)? 'LIMIT '. $limit:'';
            $all_ques_sql = "SELECT raqfa.id,raqfa.created_by,raqfa.created, GROUP_CONCAT(DISTINCT raqa.answer SEPARATOR  '@#_BREAKER_#@') as answer_correct,
            GROUP_CONCAT(DISTINCT raqa.id SEPARATOR  '@#_BREAKER_#@') as answer_option_id, 
            GROUP_CONCAT(DISTINCT raqa.question_option SEPARATOR  '@#_BREAKER_#@') as answer_option, 
            GROUP_CONCAT(DISTINCT  raqa.serial SEPARATOR  '@#_BREAKER_#@') as que_serial,raqfa.question, raqfa.question_type, raqfa.question_topic as 'question_topic' ,
            qtopic.topic
            FROM `tbl_recruitment_additional_questions` as raqfa
            INNER JOIN `tbl_recruitment_additional_questions_answer` as `raqa` ON `raqa`.`question` = `raqfa`.`id` 
            INNER JOIN `tbl_recruitment_question_topic` as `qtopic` ON  `qtopic`.`id`=`raqfa`.`question_topic`
            INNER JOIN `tbl_recruitment_additional_questions_for_applicant` as `aqa` ON  `aqa`.`question_id`=`raqfa`.`id`
            WHERE raqfa.archive = 0 AND raqfa.status = 1 AND raqfa.training_category = '".$training_category."' AND raqfa.id IN($question_id)  AND raqa.archive = 0
            group by raqfa.id ORDER BY RAND()".$limit_query;
                
            $exe_query = $this->db->query($all_ques_sql);
            $rows = $exe_query->num_rows();        
            if($rows > 0){
                    $records = $exe_query->result_array();        
                    $main_aray = [];
                    foreach ($records as $key => $value)
                    {   
                        $temp['id']=$value['id'];
                        $temp['question'] = $value['question'];            
                        $temp['question_type'] = $value['question_type'];                    
                        //if($value['question_type']!=4){                        
                            $answer_option_id = explode('@#_BREAKER_#@', $value['answer_option_id']);
                            $answer_option = explode('@#_BREAKER_#@', $value['answer_option']);            
                            $answer_option_combine=array_combine($answer_option_id,$answer_option); 
                            $arrAnswer=[];
                            foreach($answer_option_combine as $oKey=>$oValue){
                                $optAnswer=[];
                                $optAnswer['id']=$oKey;
                                $optAnswer['option']=$oValue;
                                $optAnswer['answered']=false;
                                $arrAnswer[]=$optAnswer;
                            }                        
                            $temp['answer_option']=$arrAnswer;
                    // }
                        $main_aray[$value['question_topic']]['id'] = $value['question_topic'];
                        $main_aray[$value['question_topic']]['name'] = $value['topic'];
                        $main_aray[$value['question_topic']]['question_ary'][] = $temp;
                        //tbl_recruitment_additional_questions_for_applicant
                    //  $this->copy_additional_questions($value['id'],$recruitment_task_applicant_id);
                    }
                    return ['status' => true, 'data' => array("questions"=>$main_aray)];    
            }else{
                return ['status' => false, 'error' => "No records found"];
            }         
        }else{
            return ['status' => false, 'error' => "No records found or questions not assigned"];
        }
}else{
    return ['status' => false, 'error' => "Recruitment task applicant id not found"];
}
} 





    public function submit_answer_list($answer_data){
        
       $recruitment_task_applicant_id=$this->get_applicant_details_by_token($answer_data);
       if($recruitment_task_applicant_id==0){
            return ['status' => false, 'error' => "recruitment task applicant id is empty"];
       }
       
        if(isset($answer_data['questions'])){
           // UPDATE `tbl_recruitment_additional_questions_for_applicant` SET `is_answer_correct`=0
         $get_status_arr=$this->get_applicant_quiz_submit_status($recruitment_task_applicant_id);
        
        if($get_status_arr['status']){

            if($get_status_arr['data']==0){

            foreach($answer_data['questions'] as $key=>$value){
                $question_topic_id=$value->id;
                $question_topic=$value->name;
                            
                foreach($value->question_ary as $qkey=>$qvalue){
                    $question_id=$qvalue->id;
                    $question=$qvalue->question;
                    $question_type=$qvalue->question_type;
                    $answered=$qvalue->answer_option;                
                    if($question_type==4){                    
                        $answer_list=$answered[0]->option;
                        $answer_status=0;
                    }else{
                        $isanswer=$this->check_submit_answer_is_correct($question_id,$answered);
                        $answer_status=($isanswer['status'])?1:2;
                       // $answer_list=implode(',',$isanswer['answer_val']); 
                        $answer_list=implode(',', (array)$isanswer['answer_opt']); 
                                           
                    }
                    $where_columns = ["question_id"=> $question_id,"recruitment_task_applicant_id"=>$recruitment_task_applicant_id];
                    $data = ["answer"=>$answer_list,"is_answer_correct"=>$answer_status,"updated"=>DATE_TIME];                   
                    $this->db->update('tbl_recruitment_additional_questions_for_applicant',$data, $where_columns);                
                }
                
            }
            $this->update_applicant_quiz_submit_status($recruitment_task_applicant_id);
            return ['status' => true, "success"=>"record submited successfully"];
        }else{
            return ['status' => false, 'error' => "This applicant allready submitted there question list"];    
        }
        }else{
            return ['status' => false, 'error' => "error in applicant quiz submit status"];    
        }       
        }else{
            return ['status' => false, 'error' => "questions list not available submited successfully"];    
        }
        return ['status' => true, 'error' => "answer submited successfully"];
    }
    private function check_submit_answer_is_correct($question_id,$answer_data){
        
        $answer_ids=[];
        $answer_options=[];    
        foreach($answer_data as $key=>$value){
            if($value->answered){
                $answer_ids[]= $value->id;
                $answer_options[]= $value->option;
            }           
        }        
        if(empty($answer_ids)){
            return ["status"=>false,"answer_val"=>$answer_ids,"answer_opt"=>$answer_options];
        }
        sort($answer_ids);
        $this->db->select(array("GROUP_CONCAT(id) as id"));
        $this->db->from("tbl_recruitment_additional_questions_answer");
        $this->db->where("question='".$question_id."'");
        $this->db->where("answer='1'");
        $this->db->where("archive='0'");
        $this->db->order_by("id", "asc");
        $res= $this->db->get();
        $res_array=$res->row_array();
        $ids=explode(",",$res_array['id']);       
        $answer_status=check_array_equal($ids,$answer_ids);        
        return ["status"=>$answer_status,"answer_val"=>$answer_ids,"answer_opt"=>$answer_options];
    }
    

    public function get_applicant_currunt_stage($applicant_id){
        $this->db->select(array("rts.key as current_stage_key"));
        $this->db->from("tbl_recruitment_applicant as req_app");
        $this->db->join('tbl_recruitment_stage as rs','rs.id=req_app.current_stage AND rs.archive=0','inner');
        $this->db->join('tbl_recruitment_task_stage as rts','rts.stage_label_id=rs.stage_label_id AND rs.archive=0','inner');
        //$this->db->join('tbl_recruitment_stage_label as rsl','rsl.stage_number=rs.stage_label_id AND rsl.archive=0','inner');
        //tbl_recruitment_stage

        //tbl_recruitment_stage_label

        $this->db->where("req_app.id='".$applicant_id."'");
        $res= $this->db->get();
        $res_array=$res->row_array();        
        if(!empty($res_array)){
            return $res_array['current_stage_key'];
        } 
        return 0;
    }
    public function insert_token_details($token_data){
        $this->remove_token($token_data['applicant_id']);
        $data = ["applicant_id"=>$token_data['applicant_id'],"task_applicant_id"=> $token_data['task_applicant_id'] ,"login_token"=>$token_data['token'] ,
        "interviewtype"=> $token_data['interview_type_id'],"archive"=>0,"created"=>DATE_TIME,"updated"=>DATE_TIME];
        return $result=$this->db->insert('tbl_recruitment_applicant_interview_login',$data);
    }
    public function remove_token($applicant_id){
        $this->db->update('tbl_recruitment_applicant_interview_login',["archive"=>1], array('applicant_id' => $applicant_id));
    }    
    public function verify_ipad_token($token_data) {      
         $applicant_id=$this->db->escape($token_data['applicant_id']);
         $token=$this->db->escape($token_data['token']);
        $this->db->select(array("id"));
        $this->db->from("tbl_recruitment_applicant_interview_login");
        $this->db->where("applicant_id=".$applicant_id."");
        $this->db->where("login_token=".$token."");
        $this->db->where("archive='0'");
        $res= $this->db->get();
        $res_array=$res->row_array();
        if(!empty($res_array)){
            return $res_array['id'];
        }
        return 0;
    }
    private function get_applicant_details_by_token($token_data){
        $this->db->select(array("task_applicant_id"));
        $this->db->from("tbl_recruitment_applicant_interview_login");
        $this->db->where("applicant_id='".$token_data['applicant_id']."'");
        $this->db->where("login_token='".$token_data['token']."'");
        $this->db->where("archive='0'");
        $res= $this->db->get();
        $res_array=$res->row_array();
        if(!empty($res_array)){
            return $res_array['task_applicant_id'];
        }
        return 0;
    }
    private function copy_additional_questions($question_id,$recruitment_task_applicant_id){
        $data = ["question_id"=> $question_id,"recruitment_task_applicant_id"=>$recruitment_task_applicant_id , "is_answer_correct"=>0,"archive"=>0,"updated"=>DATE_TIME];
        $result=$this->db->insert('tbl_recruitment_additional_questions_for_applicant',$data);
        $this->update_applicant_quiz_assined_status($recruitment_task_applicant_id);
    } 
    private function get_applicant_quiz_submit_status($rec_task_app_id){
        $this->db->select(array("quiz_submit_status"));
        $this->db->from("tbl_recruitment_applicant_group_or_cab_interview_detail");
        $this->db->where("recruitment_task_applicant_id='".$rec_task_app_id."'");        
        $res= $this->db->get();
        $res_array=$res->row_array();
        if(!empty($res_array)){
            return ["status"=>true,"data" => $res_array['quiz_submit_status']];
        }else{
            return ["status"=>false];
        }
    }
    private function update_applicant_quiz_submit_status($recruitment_task_applicant_id){
        $where_app_id_columns = ["recruitment_task_applicant_id"=>$recruitment_task_applicant_id];
        $data = ["quiz_submit_status"=>1,"updated"=>DATE_TIME];                   
        $this->db->update('tbl_recruitment_applicant_group_or_cab_interview_detail',$data, $where_app_id_columns);
    }
    public function get_cab_day_task_applicant_specific_details($applicant_data){
        $applicant_id=$applicant_data['applicant_id']; 
        $this->db->select(['radc.id as applicant_doc_id','radc.is_approved','rjrd.id as doc_type_id','rjrd.title as doc_type']);
        $this->db->select([            
            'CASE WHEN radc.is_approved=2 THEN (SELECT id FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_id'
    ],false);
        /*
         * $this->db->select([
            'CASE WHEN radc.is_approved=2 THEN 1 WHEN radc.is_approved=0 THEN 0 ELSE 2 END as outstanding_doc',
            'CASE WHEN radc.is_approved=2 THEN (SELECT attachment FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_data',
            'CASE WHEN radc.is_approved=2 THEN (SELECT id FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_id'
        ],false); */
        $this->db->from('tbl_recruitment_applicant_doc_category as radc');
        $this->db->join('tbl_recruitment_job_requirement_docs as rjrd','radc.recruitment_doc_id=rjrd.id AND radc.archive=rjrd.archive AND radc.archive=0','inner');
        $this->db->where('radc.is_approved',2);
        $this->db->where('radc.applicant_id',$applicant_id);
        $this->db->having('attachment_id is null or attachment_id=0');
        $query = $this->db->get();
        $res = $query->result();       
        return ['status'=>true,'data'=>['documentInfo'=>$res]];
    }
    public function check_cab_day_task_applicant($applicant_data){
        $applicant_id=$applicant_data['applicant_id']; 
        $this->db->select(['rjrd.id as doc_type_id']);
        $this->db->select([            
            'CASE WHEN radc.is_approved=2 THEN (SELECT id FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_id'
    ],false);
        /*
         * $this->db->select([
            'CASE WHEN radc.is_approved=2 THEN 1 WHEN radc.is_approved=0 THEN 0 ELSE 2 END as outstanding_doc',
            'CASE WHEN radc.is_approved=2 THEN (SELECT attachment FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_data',
            'CASE WHEN radc.is_approved=2 THEN (SELECT id FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_id'
        ],false); */
        $this->db->from('tbl_recruitment_applicant_doc_category as radc');
        $this->db->join('tbl_recruitment_job_requirement_docs as rjrd','radc.recruitment_doc_id=rjrd.id AND radc.archive=rjrd.archive AND radc.archive=0','inner');
        $this->db->where('radc.is_approved',2);
        $this->db->where('radc.applicant_id',$applicant_id);
        $this->db->having('attachment_id is null or attachment_id=0');
        $query = $this->db->get();
        $res = $query->result();       
        return ['status'=>true,'data'=>['documentInfo'=>$res]];
    }
    
    public function check_exam_start_remaining_timing_available($applicant_id,$task_id){
        $currunt_datatime=DATE_TIME; // '2019-09-26 19:30:44';
        $this->db->select(['id']);
        $this->db->from('tbl_recruitment_task');        
        $this->db->where('tbl_recruitment_task.id',$task_id);
        $this->db->where("('".$currunt_datatime."'BETWEEN start_datetime AND end_datetime)");         
        $res= $this->db->get();
        $res_array=$res->row_array();
        if(!empty($res_array)){
            return $res_array['id'];
        }
        return 0;        
    }
    private function update_applicant_quiz_assined_status($recruitment_task_applicant_id){
        $where_app_id_columns = ["recruitment_task_applicant_id"=>$recruitment_task_applicant_id];
        $data = ["allot_question"=>1,"updated"=>DATE_TIME];                   
        $this->db->update('tbl_recruitment_applicant_group_or_cab_interview_detail',$data, $where_app_id_columns);
    }
   
    function get_draft_contract_data($task_applicant_id){
        $this->db->select(array("contra.unsigned_file as contract_unsigned_file","contra.id as contract_file_id"));
        $this->db->from("tbl_recruitment_applicant_contract as contra");
        $this->db->where("contra.task_applicant_id='".$task_applicant_id."'");
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as idetail', 'idetail.recruitment_task_applicant_id = contra.task_applicant_id');
        $this->db->join('tbl_recruitment_task_applicant as taskapp', 'taskapp.id = idetail.recruitment_task_applicant_id');
        $this->db->where("idetail.quiz_submit_status=1");
        $this->db->where("taskapp.status=1");
        $this->db->where("taskapp.archive=0");
        $this->db->where("contra.signed_status=0");
        $this->db->where("contra.archive=0");
        $res= $this->db->get();      
        return $res_array=$res->row_array();        
    }


    function get_applicant_details($applicant_id){      
        $this->db->select(array("firstname","lastname"));
        $this->db->from("tbl_recruitment_applicant");
        $this->db->where("archive=0");
        $this->db->where("id='".$applicant_id."'");
        $res= $this->db->get();
        return $res_array=$res->row_array();
    }
    function get_contract_file_by_id($contract_file_id){
        $this->db->select(array("contra.unsigned_file as contract_unsigned_file"));
        $this->db->from("tbl_recruitment_applicant_contract as contra");
        $this->db->where("contra.id='".$contract_file_id."'");
        $this->db->where("contra.signed_status=0");
        $this->db->where("contra.archive=0");
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as idetail', 'idetail.recruitment_task_applicant_id = contra.task_applicant_id');
        $this->db->join('tbl_recruitment_task_applicant as taskapp', 'taskapp.id = idetail.recruitment_task_applicant_id');
        $this->db->where("idetail.quiz_submit_status=1");
        $this->db->where("taskapp.status=1");
        $this->db->where("taskapp.archive=0");
        $res= $this->db->get();
        return $res_array=$res->row_array();
    }
    function get_recruitment_presentation($interview_type){
        $this->db->select(array("file_name"));
        $this->db->from("tbl_recruitment_applicant_presentation");
        $this->db->where("interview_type='".$interview_type."'");      
        $this->db->where("archive=0");
        $res= $this->db->get();
        return $res_array=$res->row_array();
    }
}
