<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_question_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function create_Questions($objQuestion) {
        $tbl_question = TBL_PREFIX . 'recruitment_additional_questions';

        $arrQuestions = array();
        $arrQuestions['question'] = $objQuestion->getQuestion();
        $arrQuestions['status'] = $objQuestion->getStatus();
        $arrQuestions['created_by'] = $objQuestion->getCreated_by();
        $arrQuestions['question_topic'] = $objQuestion->getQuestionTopic();
        $arrQuestions['question_type'] = $objQuestion->getQuestion_Type();
        $arrQuestions['training_category'] = $objQuestion->getTrainingCategory();
        $arrQuestions['updated'] = $objQuestion->getCreated();
        
        if ($objQuestion->getId() > 0) {
            $this->db->where('id', $objQuestion->getId());
            $insert_query = $this->db->update($tbl_question, $arrQuestions);
            //echo $this->db->last_query();
            $this->update_answer($objQuestion->getId(), $objQuestion->getAnswer());
        } else {
            $arrQuestions['created'] = $objQuestion->getCreated();
            $insert_query = $this->db->insert($tbl_question, $arrQuestions);
            $questionid = $this->db->insert_id();
            $this->insert_answer($questionid, $objQuestion->getAnswer());
        }
        return $insert_query;
    }

    private function insert_answer($question, $arrAnswer) {
        $tbl_question_answer = TBL_PREFIX . 'recruitment_additional_questions_answer';

        foreach ($arrAnswer as $key => $value) {
            $arAnswer = array();
            $arAnswer['question'] = $question;
            $arAnswer['question_option'] = $value->value;
            $arAnswer['serial'] = $value->lebel;
            $arAnswer['answer'] = $value->checked;
            $this->db->insert($tbl_question_answer, $arAnswer);
            //last_query();
        }
    }

    private function update_answer($question, $arrAnswer) {
        $tbl_question_answer = TBL_PREFIX . 'recruitment_additional_questions_answer';
        $this->db->where('question', $question);
        $this->db->update('tbl_recruitment_additional_questions_answer', array('archive' => 1));

        foreach ($arrAnswer as $key => $value) {
            $arAnswer = array();
            $arAnswer['question'] = $question;
            $arAnswer['question_option'] = $value->value;
            $arAnswer['serial'] = $value->lebel;
            $arAnswer['answer'] = $value->checked;
            $this->db->insert($tbl_question_answer, $arAnswer);
        }
    }

    public function delete_Questions($objQuestion) {
        $tbl_question = TBL_PREFIX . 'recruitment_additional_questions';
        $this->db->where('id', $objQuestion->getId());
        $this->db->update($tbl_question, array('archive' => 1));
        return $this->db->affected_rows();
    }

    public function get_questions_list($reqData) {

        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        
        $tbl_question = TBL_PREFIX . 'recruitment_additional_questions';
        $tbl_question_topic = TBL_PREFIX . 'recruitment_question_topic';
        $tbl_member = TBL_PREFIX . 'member';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = (isset($sorted[0]->id) && $sorted[0]->id == 'view_id' ? 'id' : (($sorted[0]->id != 'view_id') ? $sorted[0]->id : 'N/A'));
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_question . '.id';
            $direction = 'DESC';
        }


        if (!empty($filter)) {
            if (isset($filter->filterBy) && $filter->filterBy != 'all') {
                $this->db->where($tbl_question . ".status", $filter->filterBy);
            }

            if (isset($filter->srch_box) && $filter->srch_box != '') {
                $this->db->group_start();
                
                if (isset($filter->interviewType) && $filter->interviewType != '' && $filter->interviewType == 'group_question_list') 
                    $srch_val =str_replace("GI-Q","",$filter->srch_box);
                else
                    $srch_val = str_replace("CAB-Q","",$filter->srch_box);

                $src_columns = array($tbl_question . ".id", $tbl_question . ".question", $tbl_question_topic . ".topic", "CONCAT(firstname,' ',middlename,' ',lastname) as created_by");

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0], $srch_val);
                    } else {
                        $this->db->or_like($column_search, $srch_val);
                    }
                }
                $this->db->group_end();
            }

            if (isset($filter->interviewType) && $filter->interviewType != '') {
                if($filter->interviewType == 'group_question_list')
                    $this->db->where($tbl_question . '.training_category', 1);
                else
                    $this->db->where($tbl_question . '.training_category', 2);
            }
        }

       
        $select_column = array($tbl_question . ".id", $tbl_question . ".question", $tbl_question . ".status", $tbl_question . '.created', $tbl_question . '.training_category', $tbl_question . '.question_type', $tbl_question . '.question_topic', $tbl_question . ".created_by", $tbl_question . ".updated", $tbl_question_topic . ".topic", "CONCAT(firstname,' ',middlename,' ',lastname) as created_by");
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        $this->db->from($tbl_question);
        $this->db->where($tbl_question . '.archive', '0');
        $this->db->join($tbl_question_topic, $tbl_question . '.question_topic =' . $tbl_question_topic . '.id', 'inner');
        $this->db->join($tbl_member, $tbl_question . '.created_by =' . $tbl_member . '.id', 'inner');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       # last_query();
        $dataResult = $query->result();

        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        if (!empty($dataResult)) {

            $question_id = array_column(obj_to_arr($dataResult), 'id');
            $question_id = empty($question_id)?[0]:$question_id;
            $question_data = $this->get_answer_details($question_id);

            foreach ($dataResult as $data) {
                if (isset($filter->interviewType) && $filter->interviewType != '' && $filter->interviewType == 'group_question_list') 
                    $data->view_id = 'GI-Q' . $data->id;
                else
                    $data->view_id = 'CAB-Q' . $data->id;
                

                $data->created = $data->created != '0000-00-00 00:00:00' ? date('d/m/y - h:ia', strtotime($data->created)) : '';
                $data->updated = $data->updated != '0000-00-00 00:00:00' ? date('d/m/y', strtotime($data->updated)) : '';
                $data->status = (int) $data->status;
                $data->answers = isset($question_data[$data->id])  ? $question_data[$data->id]:[];;
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_question_detail($reqData) {
        if (!empty($reqData)) {
            $questionId = $reqData->questionId;
            $tbl_job = TBL_PREFIX . 'recruitment_additional_questions';

            $question_data = "SELECT  raq.id as question_id,raq.question,raq.status as question_status,raq.question_type as answer_type,raq.question_topic,raq.training_category as question_category FROM `tbl_recruitment_additional_questions` as raq
            WHERE raq.archive = '0' AND raq.id = '" . $questionId . "' ";
            $que_data_ex = $this->db->query($question_data);
            $ques_data_ary = $que_data_ex->row_array();

            if (empty($ques_data_ary) || is_null($ques_data_ary)) {
                $return = array('status' => false);
                return $return;
            }

            $answer_data = "SELECT  raqa.answer,raqa.question_option,raqa.serial FROM `tbl_recruitment_additional_questions_answer` as raqa
            WHERE  raqa.question  = '" . $questionId . "' AND raqa.archive = 0";
            $answer_data_ex = $this->db->query($answer_data);
            $answer_ary = $answer_data_ex->result_array();
            $answer_data_ary = isset($answer_ary) && !empty($answer_ary) ? $answer_ary : array();
            $answers_ary = array();
            if (!empty($answer_data_ary)) {
                foreach ($answer_data_ary as $ans) {
                    $answers_ary[] = array('checked' => ($ans['answer'] == 1) ? true : false, 'value' => $ans['question_option'], 'lebel' => $ans['serial']);
                }
            }
            $ques_data_ary['answers'] = $answers_ary;
            $return = array('status' => true, 'data' => $ques_data_ary);
            return $return;
        }
    }

    public function get_applican_count($questionid)
    {
        if($questionid!='')
        {
            $this->db->select("id");
            $this->db->from('tbl_recruitment_additional_questions_for_applicant');
            $this->db->where('question_id', $questionid);
            $this->db->where('archive', 0);
            $query = $this->db->get();
            return $query->num_rows();
        }
    }


    public function get_answer_details($qid)
    {   
       $tbl_question_answer = TBL_PREFIX . 'recruitment_additional_questions_answer';
       $select_answer_column = array($tbl_question_answer . ".id as answer_id", $tbl_question_answer . ".answer as checked", $tbl_question_answer . ".question_option as value", $tbl_question_answer . ".serial as lebel",$tbl_question_answer . ".question as q_id");
       $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_answer_column)), false);
       $this->db->from($tbl_question_answer);
       $this->db->where('archive', 0);
       $this->db->where_in('question', $qid);

       $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       $res = $query->result_array();
       
       $complete_ary=[];
       if(!empty($res))
       {
        $temp_hold=[];
        foreach ($res as $key => $value) 
        {
            $q_id=$value['q_id'];
            if(!in_array($q_id, $temp_hold))
            {                
                $temp_hold[] = $q_id;
            }
            $temp['q_id'] = $q_id;

            $temp['answer_id'] = $value['answer_id'];
            $temp['checked'] = $value['checked'];
            $temp['value'] = $value['value'];
            $temp['lebel'] = $value['lebel'];
            $complete_ary[$q_id][] = $temp;
        }
    }
    return $complete_ary;
}

}
