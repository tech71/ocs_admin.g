<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_cab_day_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_cab_day_interview_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $src_columns = array();

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'rt.id';
            $direction = 'DESC';
        }

        if (!empty($filter->srch_box)) {
            $this->db->group_start();
            $src_columns = array("rt.id", "rt.task_name");

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $filter->srch_box);
                } else {
                    $this->db->or_like($column_search, $filter->srch_box);
                }
            }
            $this->db->group_end();
        }


        if(!empty($filter->filter_by)){

            if($filter->filter_by== 1){
                $this->db->where('rt.start_datetime>', DATE_TIME);
                $this->db->where('rt.commit_status',0);
            }else if($filter->filter_by== 2){
                $this->db->where('rt.commit_status',0);
                $this->db->where('rt.start_datetime<=' ,DATE_TIME);
            }else if($filter->filter_by== 3){
                $this->db->where('rt.commit_status',1);
            }
        }

        $select_column = array("rt.id", "rt.task_name", "rt.start_datetime", "rt.end_datetime", "rt.status", "rt.commit_status", "rl.name as location");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        //$this->db->select("(select count('id') from tbl_recruitment_task_applicant as rta Inner join tbl_recruitment_applicant as ra on ra.id = rta.applicant_id where rta.taskId = rt.id AND rta.status = 1 AND rta.archive=0 AND ra.status=1 AND ra.flagged_status=0) as applicant_cnt");

        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_location as rl', 'rl.id = rt.training_location AND rl.archive = 0', 'INNER');
        $this->db->where("rt.task_stage", '6');
        $this->db->where_in("rt.status", ['1','2']);

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //last_query(1);
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $status = 'in progress';
                if(strtotime($val->start_datetime) > strtotime(DATE_TIME) && $val->commit_status==0){
                    $status = 'pending';
                }else if($val->commit_status==1){
                    $status = 'completed';
                }
                $val->task_overall_status = $status;
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_cab_day_task_applicant_details($taskId) {
        $tableMember = TBL_PREFIX.'member';
        $tableApplicant = TBL_PREFIX.'recruitment_applicant';
        $tableTaskApplicant = TBL_PREFIX.'recruitment_task_applicant';
        $tableGroupCabDay = TBL_PREFIX.'recruitment_applicant_group_or_cab_interview_detail';
        $tableApplicantEmail = TBL_PREFIX.'recruitment_applicant_email';
        $this->db->select([
            //'Regexp_Replace(concat(ra.firstname," ",ra.middlename," ",ra.lastname),"( ){2,}", " ") as applicant_name',
            'REPLACE(concat(COALESCE(ra.firstname,"")," ",COALESCE(ra.middlename," ")," ",COALESCE(ra.lastname," ")),"  "," ")  as applicant_name',
            /* "DATE_FORMAT(ra.dob,'%d/%m/%Y') as dob" ,*/ 'ra.status as applicant_status', 'ra.flagged_status', 'ra.id as applicant_id',
            'ragc.device_pin',
            'ragc.mark_as_no_show',
            'ragc.id as details_id',            
            'ragc.app_orientation_status as app_orientation',            
            'ragc.app_login_status as app_login',            
            'ragc.recruitment_task_applicant_id as applicant_task_id'            
            ]);
        $this->db->select(['CASE WHEN ragc.quiz_status_overseen_by>0 THEN (SELECT concat(m.firstname," ",m.middlename," ",m.lastname) as overseen_by From '.$tableMember.' as m where m.id=ragc.quiz_status_overseen_by) ELSE "N/A" END as overseen_by']);
        $this->db->select([
            'CASE WHEN ragc.quiz_status=1 THEN "successful" WHEN ragc.quiz_status=2 THEN "unsuccessful" ELSE "pending" END as quiz_result',
            'CASE WHEN ragc.deviceId>0 THEN "1" ELSE "0" END as device_allocated',
            'CASE WHEN quiz_status=0 THEN (SELECT CASE WHEN COUNT(sub_raqa.id)>0 THEN 1 ELSE 0 END FROM tbl_recruitment_additional_questions_for_applicant as sub_raqa WHERE sub_raqa.recruitment_task_applicant_id=ragc.recruitment_task_applicant_id AND sub_raqa.archive=ragc.archive) ELSE 1 END as quiz_allocated',
            'CASE WHEN ragc.document_status=1 THEN "successful" WHEN ragc.document_status=2 THEN "unsuccessful" ELSE "pending" END as document_result',
            'CASE WHEN ragc.contract_status=1 THEN "in progress" ELSE "pending" END as contract_result',
            'CASE WHEN ragc.contract_status=1 THEN (SELECT CASE WHEN signed_status=0 THEN "in progress" WHEN signed_status=1 THEN "successful" WHEN signed_status=2 THEN "unsuccessful" ELSE "pending" END as contract_status_result FROM tbl_recruitment_applicant_contract where task_applicant_id=ragc.recruitment_task_applicant_id and archive=ragc.archive LIMIT 1) ELSE "pending" END as contract_result_other',
            'CASE WHEN ragc.applicant_status=1 THEN "successful" WHEN ragc.applicant_status=2 THEN "unsuccessful" ELSE "pending" END as applicant_status',
            'CASE WHEN ragc.quiz_status=1 && ragc.document_status=1 && ragc.contract_status=1 && app_login_status=1 && app_orientation_status=1 THEN "successful" ELSE "pending" END as app_on_boarding_result',
            'CASE WHEN ra.id>0 THEN (SELECT rae.email FROM '.$tableApplicantEmail.' as rae where rae.applicant_id=ra.id and primary_email=1 and archive=0 LIMIT 1) ELSE "N/A" END as applicant_email',
            "(SELECT CONCAT(COUNT(CASE WHEN sub_raqfa.is_answer_correct=1 THEN 1 ELSE null END),'/',COUNT(DISTINCT sub_raqfa.id)) FROM tbl_recruitment_additional_questions_for_applicant as sub_raqfa 
            INNER JOIN tbl_recruitment_applicant_group_or_cab_interview_detail as sub_ragcid ON  sub_ragcid.recruitment_task_applicant_id = sub_raqfa.recruitment_task_applicant_id AND sub_raqfa.archive=sub_ragcid.archive
            INNER JOIN tbl_recruitment_interview_type as sub_riy ON  sub_riy.id = sub_ragcid.interview_type and sub_riy.key_type='cab_day'
            INNER JOIN `tbl_recruitment_additional_questions` as `sub_raq` ON `sub_raq`.`id` = `sub_raqfa`.`question_id`  AND `sub_raq`.`training_category`= sub_riy.id
            where sub_raqfa.recruitment_task_applicant_id=rta.id and sub_raqfa.archive=0) as quiz_marked"
        ],false);

        $this->db->from($tableTaskApplicant.' as rta');
        $this->db->join($tableApplicant.' as ra', 'ra.id = rta.applicant_id AND ra.duplicated_status=0', 'inner');
        $this->db->join($tableGroupCabDay.' as ragc', 'ragc.recruitment_task_applicant_id = rta.id and ragc.archive=rta.archive', 'inner');
        $this->db->where('rta.taskId', $taskId);
        $this->db->where('rta.status', 1);
        $this->db->where('rta.archive', 0);
        $query = $this->db->get();
        $res = $query->result();
        return $res;
    }
    
    public function get_cab_day_task_applicant_specific_details($applicantId){
        $this->db->select(['radc.id','radc.is_approved','rjrd.title']);
        $this->db->select([
            'CASE WHEN radc.is_approved=2 THEN 1 WHEN radc.is_approved=0 THEN 0 ELSE 2 END as outstanding_doc',
            'CASE WHEN radc.is_approved=2 THEN (SELECT attachment FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_data',
            'CASE WHEN radc.is_approved=2 THEN (SELECT id FROM tbl_recruitment_applicant_stage_attachment WHERE applicant_id=radc.applicant_id AND archive=0 AND uploaded_by_applicant=1 AND doc_category=radc.recruitment_doc_id AND document_status=0  ORDER BY id DESC LIMIT 1) ELSE 0 END as attachment_id'
    ],false);
    $this->db->from('tbl_recruitment_applicant_doc_category as radc');
    $this->db->join('tbl_recruitment_job_requirement_docs as rjrd','radc.recruitment_doc_id=rjrd.id AND radc.archive=rjrd.archive AND radc.archive=0','inner');
    //$this->db->where('radc.is_required',1);
    $this->db->where('radc.applicant_id',$applicantId);
    $query = $this->db->get();
        $res = $query->result();
        return ['status'=>true,'data'=>['documentInfo'=>$res]];
    }
    
    public function check_pending_review_doc_or_outstaning_document_left($applicantId){
        $data = $this->get_cab_day_task_applicant_specific_details($applicantId);
        $data =$data['data']['documentInfo'];
        
        $dataReview = array_filter($data,function($val){
            if($val->is_approved == 2 && $val->outstanding_doc==1 && !empty($val->attachment_data) && !empty($val->attachment_id)){
                return true;
            }
            return false;
        });
        $dataOutStanding = array_filter($data,function($val){
            if($val->is_approved == 2 && $val->outstanding_doc==1 && empty($val->attachment_id)){
                return true;
            }
            return false;
        });
        
        if(!empty($dataReview) ||!empty($dataOutStanding)){
            return true;
        }else{
            return false;
        }
        
    }

    public function get_applicant_details_by_envelope_id($envId){
        $res= [];
        $this->db->select(['rac.task_applicant_id', 'rta.applicant_id', 'rac.id as contract_id','ra.status as applicant_status']);
        $this->db->select([
            'CASE WHEN rta.taskId>0 THEN (SELECT recruiterId FROM tbl_recruitment_task_recruiter_assign WHERE taskId=rta.taskId and primary_recruiter=1 and archive=0 limit 1 ) ELSE 0 END as recruiterId',
            '(SELECT id FROM tbl_recruitment_stage where stage_key="employment_contract " and archive=0 limit 1) as stageId'
            ]);
        $this->db->from(TBL_PREFIX.'recruitment_applicant_contract as rac');
        $this->db->join(TBL_PREFIX.'recruitment_task_applicant as rta  ', 'rta.id=rac.task_applicant_id and rta.archive=rac.archive', 'inner');
        $this->db->join(TBL_PREFIX.'recruitment_applicant as ra', 'ra.id=rta.applicant_id and ra.archive=rta.archive and ra.archive=0 and ra.status in (1,3)', 'inner');
        $this->db->where('rac.envelope_id',$envId);
        $query = $this->db->get();
        if($query->num_rows()>0){
            $res = $query->row();
        }
        return $res;
    }
    public function get_sub_query_interViewType(){
        /* $this->db->from('tbl_recruitment_interview_type as sub_rit');
        $this->db->select(['sub_rit.id']);
        $this->db->where('sub_rit.key_type','cab_day');
        $this->db->where('sub_rit.archive',0);
        $this->db->limit(1);
        $subQuery =$this->db->get_compiled_select(); */
        $subQuery ="SELECT `sub_rit`.`id` FROM `tbl_recruitment_interview_type` as `sub_rit` WHERE `sub_rit`.`key_type` = 'cab_day' AND `sub_rit`.`archive` = 0 LIMIT 1";
        return $subQuery;
    }

    public function commit_cabday_interview($reqData) {
        $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';
        if ($taskId == '') {
            echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
            exit();
        }

        $this->db->select('rt.id');
        $this->db->from("tbl_recruitment_task as rt");
        $this->db->join("tbl_recruitment_task_stage as rts","rts.id=rt.task_stage and rts.key='cab_day' and rts.archive=0 and rt.commit_status=0 and rt.status=1 and rt.id='".$taskId."'");
        $query= $this->db->get();
        if($query->num_rows()<=0){
            echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
            exit();
        }
        
        $subQuery = $this->get_sub_query_interViewType();

   

        $this->db->select(['rta.id as recruitment_task_applicant_id', 'ragid.quiz_status', 'ragid.applicant_status', 'ragid.contract_status', 
        'ragid.device_pin',
        'ragid.app_orientation_status',
        'ragid.app_login_status',
        'ragid.document_status',
        'CASE WHEN ragid.contract_status=1 then (SELECT signed_status FROM tbl_recruitment_applicant_contract where task_applicant_id=rta.id AND archive=0 order by id desc limit 1 ) ELSE "0" END as contract_final_status',
        'ragid.device_pin',
        'ragid.id as applicant_cab_id'
        ]);
        $this->db->from('tbl_recruitment_task_applicant as rta');
        $this->db->join('tbl_recruitment_applicant as ra', 'ra.id = rta.applicant_id', 'inner');
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as ragid', 'ragid.recruitment_task_applicant_id = rta.id AND ragid.interview_type =('.$subQuery.')  AND ragid.archive=0', 'inner');
        $this->db->where('rta.taskId', $taskId);
        $this->db->where('rta.status', 1);
        $this->db->where('rta.archive', 0);
        $query = $this->db->get();
        $applicant_ques = $query->result_array();

        $is_update = false;
        if (!empty($applicant_ques)) {
            $update_arr = [];
            foreach ($applicant_ques as $key => $value) {
                $temp =[];
                $temp['id'] = $value['applicant_cab_id'];

                if ($value['quiz_status'] == 1 && $value['document_status'] == 1 && $value['contract_status'] == 1 && $value['contract_final_status'] == 1 /*  && $value['app_login_status'] == 1 && $value['app_orientation_status'] == 1 */){
                    $temp['applicant_status'] = 1;
                }else{
                    $temp['applicant_status'] = 2;  
                }
                $update_arr[] = $temp;
            }

            if (!empty($update_arr)) {
                $this->basic_model->insert_update_batch('update', 'recruitment_applicant_group_or_cab_interview_detail', $update_arr, 'id');
                $this->basic_model->update_records('recruitment_task', array('commit_status' => 1, 'action_at' => DATE_TIME, 'status' => 2), array('id' => $taskId));
                $is_update = true;
            }
        }

        if ($is_update) {
            echo json_encode(array('status' => true, 'msg' => 'Updated successfully.'));
            exit();
        } else {
            echo json_encode(array('status' => false, 'msg' => 'Error in update.'));
            exit();
        }
    }

}
