<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_applicant_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    } 

    public function get_requirement_applicants($reqData, $adminId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $its_recruiter_admin = check_its_recruiter_admin($adminId);


        $src_columns = array('ra.id', 'ra.appId', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as FullName');

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'ra.id';
            $direction = 'DESC';
        }

        if (!empty($filter->filter_val) && in_array($filter->filter_val, [1, 2, 3])) {
            if ($filter->filter_val == 2) {
                
            } else {
                $this->db->where('ra.flagged_status', 0);
            }
            $this->db->where('ra.status', $filter->filter_val);
        }

        if (!empty($filter->job_pos) && $filter->job_pos > 0) {
            $this->db->where('raaaa.position_applied', $filter->job_pos);
        }

        if (!$its_recruiter_admin || !empty($filter->rec)) {
            $this->db->where('recruiter', $adminId);
        }

        if (!empty($filter->jobId)) {
            $this->db->where('ra.jobId', $filter->jobId);
        }

        if (!empty($filter->search)) {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }


        $select_column = array('ra.id', 'ra.date_applide', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as FullName', 'ra.status', 'ra.updated',
            'ra_em.email', 'ra_ph.phone',
            'ra.appId', 'ra.jobId', "(concat(rtb.stage_number,' - ',rtb.title)) as stage", "(concat(rs.stage,' - ',rs.title)) as sub_stage");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->select('(select count(ras.id) from tbl_recruitment_applicant_stage as ras where applicant_id = ra.id AND status IN (3,4)) as progress_count');
        $this->db->select('(select concat(m.firstname," ",m.lastname) as recruiter_name from tbl_member as m inner join tbl_department as d on d.id = m.department AND d.short_code = "internal_staff" where m.id = ra.recruiter) as recruiter_name');
        
        $this->db->select('(select group_concat(distinct title SEPARATOR ", ") from tbl_recruitment_applicant_applied_application as sub_applied_app INNER JOIN tbl_recruitment_job_position as sub_rjp ON sub_rjp.id = sub_applied_app.position_applied  where sub_applied_app.applicant_id = ra.id)  as job_position');

        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_email as ra_em', 'ra_em.applicant_id = ra.id AND ra_em.primary_email = 1', 'inner');
        $this->db->join('tbl_recruitment_applicant_phone as ra_ph', 'ra_ph.applicant_id = ra.id AND ra_ph.primary_phone = 1', 'inner');
        
        $this->db->join('tbl_recruitment_applicant_applied_application as raaaa', 'raaaa.applicant_id = ra.id', 'inner');
        $this->db->join('tbl_recruitment_job_position as rjp', 'raaaa.position_applied = rjp.id', 'inner');

        $this->db->join('tbl_recruitment_stage as rs', 'rs.id = ra.current_stage', 'inner');
        $this->db->join('tbl_recruitment_stage_label as rtb', 'rtb.id = rs.stage_label_id', 'inner');



        $this->db->where('ra.archive', 0);

        $this->db->where(['ra.duplicated_status' => 0]);
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('ra.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query();   
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $result = $query->result();

        $total_stage = $this->basic_model->get_row('recruitment_stage', ['count(id) as total', '']);

        $stage_count = (!empty($total_stage)) ? $total_stage->total : 14;

        if (!empty($result)) {

            foreach ($result as $val) {

                $val->progress_count = (int) (($val->progress_count / $stage_count) * 100);
                $val->questions = $this->get_applicant_job_question_answer($val->id);
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

    function get_applicant_job_question_answer($applicant_id) {
        $this->db->select(['rasa.questionId', 'rasa.questionId', 'rjq.question', 'rjpd.channel as channelId', 'rasa.answer_status', 'rasa.answer']);

        $this->db->from('tbl_recruitment_applicant_seek_answer as rasa');
        $this->db->join('tbl_recruitment_job_question as rjq', 'rjq.id = rasa.questionId', 'inner');
        $this->db->join('tbl_recruitment_job_published_detail as rjpd', 'rjpd.jobId = rasa.jobId', 'inner');
//        $this->db->join('tbl_recruitment_channel as rc', 'rc.id = rjpd.channel', 'inner');

        $this->db->where('rasa.applicantId', $applicant_id);
        $query = $this->db->get();
        $res = $query->result();

        $quest = array('website' => [], 'seek' => []);

        if (!empty($res)) {
            foreach ($res as $value) {
                if ($value->channelId == 1) {
                    $quest['seek'][] = (array) $value;
                } else {
                    $quest['website'][] = (array) $value;
                }
            }
        }

        return $quest;
    }

    function check_already_flag_applicant($applicant_id) {
        $this->db->select('applicant_id');
        $this->db->from('tbl_recruitment_flag_applicant');

        $this->db->where(['applicant_id' => $applicant_id, 'archive' => 0]);
        $this->db->where_in('flag_status', [1, 2]);

        $query = $this->db->get();
        return $query->result();
    }

    function flag_applicant($reqData, $adminId) {
        $flage_data = array(
            'applicant_id' => $reqData->applicant_id,
            'reason_title' => $reqData->reason_title,
            'reason_note' => $reqData->reason_note,
            'flaged_request_by' => $adminId,
            'flaged_approve_by' => 0,
            'flag_status' => 1,
            'created' => DATE_TIME,
            'updated' => DATE_TIME,
            'archive' => 0,
        );

        $this->basic_model->insert_records('recruitment_flag_applicant', $flage_data, false);

        $this->basic_model->update_records('recruitment_applicant', ['flagged_status' => 1], ['id' => $reqData->applicant_id]);

        return true;
    }

    public function get_requirement_flaged_applicants($reqData, $adminId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $its_recruiter_admin = check_its_recruiter_admin($adminId);


        $src_columns = array('ra.id', 'ra.appId', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as FullName');

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'ra.id';
            $direction = 'DESC';
        }

        if (!empty($filter->filter_val)) {
            if ($filter->filter_val == 'all') {
                $this->db->where_in('ra.flagged_status', [1, 2, 3]);
            } elseif ($filter->filter_val == 'pending') {
                $this->db->where('ra.flagged_status', 1);
            } elseif ($filter->filter_val == 'flagged') {
                $this->db->where('ra.flagged_status', 2);
            } elseif ($filter->filter_val == 'new') {
                $this->db->where('ra.flagged_status', 3);
            }
        }

        if (!$its_recruiter_admin) {
            $this->db->where('recruiter', $adminId);
        }

        if (!empty($filter->search)) {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }


        $select_column = array('ra.id', 'ra.date_applide', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as FullName', 'ra.status', 'ra.updated', 'ra_em.email', 'ra_ph.phone', 'ra.appId', 'rfa.reason_note', 'rfa.reason_title', 'ra.flagged_status');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->select('(select concat(firstname," ",lastname) as recruiter_name from tbl_member inner join tbl_department ON  tbl_member.department=tbl_department.id and tbl_department.short_code!="external_staff" and tbl_member.archive=tbl_department.archive AND tbl_member.archive=0 where tbl_member.id = recruiter) as recruiter_name');

        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_email as ra_em', 'ra_em.applicant_id = ra.id AND ra_em.primary_email = 1', 'inner');
        $this->db->join('tbl_recruitment_applicant_phone as ra_ph', 'ra_ph.applicant_id = ra.id AND ra_ph.primary_phone = 1', 'inner');
        $this->db->join('tbl_recruitment_flag_applicant as rfa', 'rfa.applicant_id = ra.id AND rfa.flag_status IN (1,2)', 'inner');

        $this->db->where('ra.archive', 0);

        $this->db->where(['ra.duplicated_status' => 0]);
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('ra.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//     last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $x = $this->get_application_job_application_title($val->id);
                $val->job_position = $x['job_position'];
                $val->job_application = $x['job_application'];
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

    function get_application_job_application_title($applicant_id) {
        $this->db->select(['rjp.title', 'raaa.id']);

        $this->db->from('tbl_recruitment_applicant_applied_application as raaa');
        $this->db->join('tbl_recruitment_job_position as rjp', 'raaa.position_applied = rjp.id', 'inner');
        $this->db->where('raaa.applicant_id', $applicant_id);
        $this->db->where('raaa.archive', 0);
        $query = $this->db->get();

        $res = $query->result_array();

        $return = ['job_position' => '', 'job_application' => $res];
        if (!empty($res)) {
            $return['job_position'] = implode(', ', array_column($res, 'title'));
        }

        return $return;
    }

    function get_job_postion_by_create_job() {
        $this->db->select(['rjp.title as label', 'rjp.id as value']);

        $this->db->from('tbl_recruitment_applicant_applied_application as raaa');
        $this->db->join('tbl_recruitment_job_position as rjp', 'raaa.position_applied = rjp.id', 'inner');
        $this->db->group_by('rjp.id');

        $query = $this->db->get();

        $res = $query->result();

        return $res;
    }

    function dont_flag_applicant($reqData, $adminId) {
        // update in applicant table request denied
        $update_data = array('flagged_status' => 0);
        $where_f = array('id' => $reqData->applicant_id);
        $this->basic_model->update_records('recruitment_applicant', $update_data, $where_f);

        // update in flage applicant table
        $update_f_d = array('flag_status' => 3, 'flaged_approve_by' => $adminId, 'updated' => DATE_TIME);
        $this->basic_model->update_records('recruitment_flag_applicant', $update_f_d, ['applicant_id' => $reqData->applicant_id]);

        return true;
    }

    function flag_applicant_approve($reqData, $adminId) {
        // update in applicant table request approve
        $update_data = array('flagged_status' => 2, 'status' => 2);
        $where_f = array('id' => $reqData->applicant_id);
        $this->basic_model->update_records('recruitment_applicant', $update_data, $where_f);

        // update in flage applicant table
        $update_f_d = array('flag_status' => 2, 'flaged_approve_by' => $adminId, 'updated' => DATE_TIME);
        $this->basic_model->update_records('recruitment_flag_applicant', $update_f_d, ['applicant_id' => $reqData->applicant_id]);
        $this->basic_model->update_records('recruitment_applicant_stage_attachment',['member_move_archive'=>2,'archive'=>1],['applicant_id'=>$reqData->applicant_id]);

        return true;
    }

    public function get_recruitment_duplicate_applicants($reqData, $adminId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $its_recruiter_admin = check_its_recruiter_admin($adminId);

        // create sub query first
        $this->db->select("group_concat(title SEPARATOR '@__@@__@') as job_position", false);
        $this->db->from('tbl_recruitment_applicant_applied_application s_raaa');
        $this->db->join("tbl_recruitment_applicant s_ra", "s_ra.id=s_raaa.applicant_id", "inner");
        $this->db->join("tbl_recruitment_job_position s_rjp", "s_rjp.id=s_raaa.position_applied", "inner");
        $this->db->where("s_ra.id=ra.duplicatedId", null, false);
        $sub_query = $this->db->get_compiled_select();

        $src_columns = array('ra.id', 'ra.appId', 'REPLACE(concat(COALESCE(ra.firstname,"")," ",COALESCE(ra.middlename," ")," ",COALESCE(ra.lastname," ")),"  "," ") as FullName', 'ra.appID', 'ra.date_applide', 'rjp.title', 'rd.name', "DATE_FORMAT(`ra`.`date_applide`, '%d/%m/%Y')");

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'ra.id';
            $direction = 'DESC';
        }

        if (!empty($filter->filter_by) && in_array($filter->filter_by, [1, 2, 3])) {
            $this->db->where('rads.status', $filter->filter_by);
        }

        if (!$its_recruiter_admin) {
            $this->db->where('recruiter', $adminId);
        }

        if (!empty($filter->srch_box)) {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                /* if (strstr($column_search, "date_format(") !== false) {
                  $this->db->or_like($column_search, $filter->srch_box,false);
                  }else */ if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->srch_box);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->srch_box);
                }
            }
            $this->db->group_end();
        }


        $select_column = array('ra.id', 'ra.date_applide', 'concat_ws(" ",ra.firstname,ra.middlename,ra.lastname) as FullName', 'ra.status', 'ra.created',
            'rae.email', 'rap.phone', "group_concat(rjp.title) as job_position", "group_concat(rd.name) as recruitment_area", "COALESCE(rads.relevant_note,'') as relevantNotes",
            'ra.appId', 'ra.duplicatedId as currentApplicant');

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->select("case when rads.status=1 then 'Pending'
        when rads.status=2 then 'Accepted'
        when rads.status=3 then 'Rejected'
        else 'Pending' end as duplicate_application_status", false);
        $this->db->select('(' . $sub_query . ') as current_job_possition', false);
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_duplicate_status as rads', 'ra.id=rads.applicant_id and ra.archive=0 and rads.archive=ra.archive and ra.duplicated_status=1', 'inner');
        $this->db->join('tbl_recruitment_applicant_applied_application as raaa', 'raaa.applicant_id =ra.id and ra.archive=0 and raaa.archive=ra.archive', 'inner');
        $this->db->join('tbl_recruitment_job_position as rjp', 'rjp.id=raaa.position_applied', 'inner');
        $this->db->join('tbl_recruitment_department as rd', 'rd.id=raaa.recruitment_area', 'inner');
        $this->db->join('tbl_recruitment_applicant_email as rae', 'rae.applicant_id = ra.id and rae.archive=0 and rae.primary_email=1', 'inner');
        $this->db->join('tbl_recruitment_applicant_phone as rap', 'rap.applicant_id = ra.id and rap.archive=0 and rap.primary_phone=1', 'inner');
        $this->db->where('ra.archive', 0);
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('ra.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query(1);
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $result = $query->result();

        //count is use for number of pages
        $return = array('status' => true, 'count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

    public function update_duplicate_application_status($reqData, $statusType = []) {
        $response = [];
        $appId = isset($reqData->id) ? $reqData->id : '';
        $status = isset($reqData->status) ? $reqData->status : '';
        $response = ['status' => false, 'error' => 'Something went wrong.'];

        if (!empty($appId) && $appId > 0 && in_array($status, array_keys($statusType))) {

            $dataUpdate = [];
            if ($status == 'reject') {
                $dataUpdate = ['status' => 3, 'accept_sub_status' => 0, 'action_taken_date' => DATE_TIME];
            } elseif ($status == 'accept_addnem') {
                $dataUpdate = ['status' => 2, 'accept_sub_status' => 1, 'action_taken_date' => DATE_TIME];
            } elseif ($status == 'accept_editexisting') {
                $dataUpdate = ['status' => 2, 'accept_sub_status' => 2, 'action_taken_date' => DATE_TIME];
            }

            $dd = !empty($dataUpdate) ? $this->basic_model->update_records('recruitment_applicant_duplicate_status', $dataUpdate, array('status' => 1, 'archive' => 0, 'applicant_id' => $appId)) : false;

            if ($dd) {
                if ($status == 'accept_addnem') {
                    $this->add_applied_application_to_current_application($appId);
                }
                $response = ['status' => true];
            } else {
                $response = ['status' => false, 'error' => 'Something went wrong.'];
            }
        }
        return $response;
    }

    public function update_duplicate_application_relevant_note($reqData) {
        $response = ['status' => false, 'error' => 'Something went wrong.'];
        $appId = isset($reqData->id) ? $reqData->id : '';
        $notes = isset($reqData->relevant_note) ? $reqData->relevant_note : '';

        if (!empty($appId) && $appId > 0 && !empty($notes)) {

            $dataUpdate = ['relevant_note' => $notes];
            $dd = !empty($dataUpdate) ? $this->basic_model->update_records('recruitment_applicant_duplicate_status', $dataUpdate, array('archive' => 0, 'applicant_id' => $appId)) : false;

            if ($dd) {
                $response = ['status' => true];
            } else {
                $response = ['status' => false, 'error' => 'Something went wrong.'];
            }
        }
        return $response;
    }

    private function add_applied_application_to_current_application($applicantId) {
        $duplicatApplicantDetails = $this->basic_model->get_row('recruitment_applicant', ['duplicatedId'], ['id' => $applicantId, 'duplicated_status' => 1]);
        if ($duplicatApplicantDetails) {
            $currentApplicantId = $duplicatApplicantDetails->duplicatedId;
            $result = $this->basic_model->get_result('recruitment_applicant_applied_application', ['applicant_id' => $applicantId, 'archive' => 0], ['position_applied', 'recruitment_area', 'employement_type', 'channelId', 'status'], [], 1);
            if ($result) {
                if (!empty($result) && is_array($result)) {
                    $insData = array_map(function($val) use($currentApplicantId, $applicantId) {
                        return $val + ['applicant_id' => $currentApplicantId, 'from_applicant_id' => $applicantId, 'from_status' => 1];
                    }, $result);

                    $this->basic_model->insert_update_batch('insert', 'recruitment_applicant_applied_application', $insData);
                }
            }
        }
    }

    function get_applicant_info($applicantId, $adminId) {
        $its_recruiter_admin = check_its_recruiter_admin($adminId);


        $this->db->select("Case when ra.recruiter>0 THEN (select concat(a.firstname,' ',a.lastname) as assign_recruiter from tbl_member as a  INNER JOIN tbl_department as d ON d.id = a.department AND d.short_code = 'internal_staff' where ra.recruiter = a.id) ELSE '' END as assign_recruiter");
        $this->db->select(["concat(ra.firstname,' ',ra.lastname) as  fullname", "ra.flagged_status", "ra.jobId", "ra.appId", "ra.id", "ra.status", "ra.lastname", "ra.firstname", "ra.current_stage", "ra.duplicated_status", "ra.pin"]);
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->where('ra.id', $applicantId);
        if (!$its_recruiter_admin) {
            $this->db->where('ra.recruiter', $adminId);
        }
        $query = $this->db->get();


        $res = $query->row_array();
        if (!empty($res)) {
            $res['applicant_status'] = ($res['status'] == 1) ? 'In-Progress' : (($res['status'] == 2) ? "Rejected" : "Hired");
        }
        return $res;
    }

    function get_applicant_phone($applicant_id) {
        $where = array('applicant_id' => $applicant_id, 'archive' => 0);
        $column = array('id', 'phone', 'primary_phone');

        return $this->basic_model->get_record_where('recruitment_applicant_phone', $column, $where);
    }

    function get_applicant_email($applicant_id) {
        $where = array('applicant_id' => $applicant_id, 'archive' => 0);
        $column = array('id', 'email', 'primary_email');

        return $this->basic_model->get_record_where('recruitment_applicant_email', $column, $where);
    }

    function get_applicant_reference($applicant_id) {
        $where = array('applicant_id' => $applicant_id, 'archive' => 0);
        $column = array('id', 'name', 'email', 'phone', 'status', 'relevant_note');

        return $this->basic_model->get_record_where('recruitment_applicant_reference', $column, $where);
    }

    function get_applicant_address($applicant_id) {
        $where = array('applicant_id' => $applicant_id, 'archive' => 0);
        $column = array('raa.id', 'raa.street', 'raa.city', 'raa.postal', 's.name as stateName', 'raa.state');

        $this->db->select($column);
        $this->db->from('tbl_recruitment_applicant_address as raa');
        $this->db->join('tbl_state as s', 's.id = raa.state', 'inner');

        $this->db->where($where);
        $query = $this->db->get();

        return $query->result();
    }

    function get_applicant_job_application($applicant_id) {
        $this->db->select(['rjp.title as position_applied', 'raaa.position_applied as position_applied_id', 'raaa.id', 'rd.name as recruitment_area', 'raaa.recruitment_area as recruitment_area_id',
            'raaa.employement_type as employement_type_id', 'rjet.title as employement_type', 'raaa.channelId as channel_id', 'rc.channel_name as channel']);

        $this->db->from('tbl_recruitment_applicant_applied_application as raaa');
        $this->db->join('tbl_recruitment_job_position as rjp', 'raaa.position_applied = rjp.id', 'inner');
        $this->db->join('tbl_recruitment_department as rd', 'raaa.recruitment_area = rd.id', 'inner');
        $this->db->join('tbl_recruitment_job_employment_type as rjet', 'raaa.employement_type = rjet.id', 'inner');
        $this->db->join('tbl_recruitment_channel as rc', 'raaa.channelId = rc.id', 'inner');

        $this->db->where('raaa.applicant_id', $applicant_id);
        $this->db->where('raaa.archive', 0);
        $query = $this->db->get();

        $res = $query->result();

        return $res;
    }

    function get_applicant_stage_details($applicant_id) {
        $main_stage = $this->basic_model->get_record_where('recruitment_stage_label', ['id', 'title', 'stage_number'], ['archive' => 0]);

        $this->db->select(['rs.title', 'rs.stage', 'ras.action_by', 'ras.action_at', 'rs.stage_label_id', 'rs.id', 'rs.stage_key']);
        $this->db->select("CASE
            WHEN ras.status is NULL THEN (1)
            ELSE ras.status
            END as stage_status", false);

        $this->db->select("CASE
            WHEN ras.action_by > 0 THEN (select concat(firstname,' ',lastname) as fullname from tbl_member where id = ras.action_by)
            ELSE ''
            END as action_username", false);

        $this->db->from('tbl_recruitment_stage as rs');
        $this->db->join('tbl_recruitment_applicant_stage as ras', 'ras.stageId = rs.id AND ras.archive = 0 AND ras.applicant_id = ' . $applicant_id, 'LEFT');
        $this->db->where('rs.archive', 0);
        $this->db->order_by('rs.stage_order', 'asc');
        $this->db->group_by('rs.id');

        $query = $this->db->get();
        $stage_details = $query->result();

        $mapping_coponent = [
            'review_answer' => 'ReviewAnswerStage',
            'phone_interview' => 'PhoneInterviewStage',
            'group_schedule_interview' => 'ScheduleInterviewStage',
            'group_applicant_responses' => 'ApplicantResponseGroupStage',
            'group_interview_result' => 'GroupInterviewResultStage',
            'document_checklist' => 'DocumentCheckListStage',
            'position_and_award_level' => 'PositionAndAwardLevelsStage',
            'review_references' => 'ReferenceChecksStage',
            'schedule_cab_day' => 'ScheduleCabDayStage',
            'cab_applicant_responses' => 'ApplicantResponseCabDayStage',
            'cab_day_result' => 'CabDayResultStage',
            'employment_contract' => 'EmploymentContractStage',
            'member_app_onboarding' => 'MemberAppOnbordingStage',
            'recruitment_complete' => 'RecruitmentCompletedStage',
        ];

        $sub_stages = [];
        $main_stage_status = [];
        if (!empty($stage_details)) {
            foreach ($stage_details as $val) {

                $val->component_name = $mapping_coponent[$val->stage_key];

                if ($val->stage_status)
                    $main_stage_status[$val->stage_label_id][$val->stage] = array('stage_status' => $val->stage_status, 'action_username' => $val->action_username, 'action_at' => $val->action_at);


                $sub_stages[$val->stage_label_id][] = (array) $val;
            }
        }

        if (!empty($main_stage)) {
            foreach ($main_stage as $key => $val) {
                $x = $main_stage_status[$val->stage_number];
                $x = $this->get_main_stage_status($x);

                $val->sub_stage = $sub_stages[$val->stage_number];
                $main_stage[$key] = array_merge((array) $val, (array) $x);
            }
        }

        return $main_stage;
    }

    function get_main_stage_status($data) {
        if (!empty($data)) {

            $unsuccess = false;
            $success = false;
            $inprogress = false;

            foreach ($data as $val) {
                $val = (object) $val;

                if ($val->stage_status == 4) { // complete
                    $return = $val;
                    $unsuccess = true;
                } elseif ($val->stage_status == 2 && (!$unsuccess)) { // in progress
                    $return = $val;
                    $inprogress = true;
                } elseif ($val->stage_status == 3 && (!$unsuccess && !$success)) { // complete
                    $return = $val;
                    $success = true;
                } elseif ($val->stage_status == 1 && (!$unsuccess && !$success && !$inprogress)) { // pending
                    $return = $val;
                    $inprogress = true;
                }
            }
        }

        return $return;
    }

    function get_applicant_progress($applicantId) {
        $this->db->select(['count(ras.id) as done_count']);
        $this->db->from('tbl_recruitment_applicant_stage as ras');
        $this->db->where('applicant_id', $applicantId);
        $this->db->where_in('status', [3, 4]);

        $query = $this->db->get();
        $x = $query->row();
//        last_query();
        if (!empty($x)) {
            $percent = (int) (($x->done_count / 14) * 100);
        } else {
            $percent = 0;
        }

        return $percent;
    }

    function get_nex_stage_recruitment($current_stage) {
        $this->db->select(['rs.id', 'rs.stage']);
        $this->db->from('tbl_recruitment_stage as rs');
        $this->db->where('id >', $current_stage);
        $this->db->where('archive', 0);
        $this->db->order_by('stage_order', 'asc');

        $query = $this->db->get();
        $x = $query->row();

        return $x;
    }

    function update_applicant_stage_status($reqData, $adminId) {
        $stage_status = ($reqData->status == 3) ? 3 : 4;

        // update current stage status according to status
        $stage_data = array('status' => $stage_status, 'action_by' => $adminId, 'action_at' => DATE_TIME);
        $stage_where = array('applicant_id' => $reqData->applicant_id, 'stageId' => $reqData->stageId);
        $this->basic_model->update_records('recruitment_applicant_stage', $stage_data, $stage_where);

        if ($stage_status == 4) {
            // mark as rejected applicant
            $app_where = array('id' => $reqData->applicant_id);
            $this->basic_model->update_records('recruitment_applicant', ['status' => 2], $app_where);
            $this->basic_model->update_records('recruitment_applicant_stage_attachment',['member_move_archive'=>2,'archive'=>1],['applicant_id'=>$reqData->applicant_id]);
        } else {
            // get next stage id
            $next_stage_details = $this->get_nex_stage_recruitment($reqData->stageId);

            // update current stage
            $stage_where = array('id' => $reqData->applicant_id,);
            $this->basic_model->update_records('recruitment_applicant', ['current_stage' => $next_stage_details->id], $stage_where);

            // check stage data already exist
            $stage_res = $this->basic_model->get_row('recruitment_applicant_stage', ['id'], ['applicant_id' => $reqData->applicant_id, 'archive' => 0, 'stageId' => $next_stage_details->id]);
            if (empty($stage_res)) {
                // create next stage data and mark as pending
                $next_data = array('applicant_id' => $reqData->applicant_id, 'status' => 2, 'created' => DATE_TIME, 'archive' => 0, 'stageId' => $next_stage_details->id);
                $this->basic_model->insert_records('recruitment_applicant_stage', $next_data, false);
            }
        }

        return true;
    }

    function get_applicant_phone_interview_classification($applicant_id) {
        $res = $this->basic_model->get_row('recruitment_applicant_phone_interview_classification', ['id', 'classfication'], ['applicant_id' => $applicant_id]);
        if (!empty($res)) {
            return $res->classfication;
        } else {
            return false;
        }
    }

    function get_applicant_group_or_cab_interview_details($applicant_id, $task_stage) {
        $this->db->select(["rt.start_datetime", "rl.name as training_location", "rt.status as task_status", "rta.email_status", "rta.invitation_accepted_at", "rta.invitation_send_at", "rta.invitation_cancel_at", "rta.status as applicant_email_status", "rt.id as taskId", "inter_det.applicant_status as applicant_result", "inter_det.quiz_status", "rta.id as task_applicant_id",
            "inter_det.mark_as_no_show", "inter_det.marked_date", "inter_det.app_orientation_status", "inter_det.app_login_status"]);

        $this->db->select("(select concat_ws(' ',m.firstname, lastname) from tbl_recruitment_task_recruiter_assign as rtra INNER JOIN tbl_member as m on m.id = rtra.recruiterId where rtra.taskId = rt.id AND rtra.archive = 0 and primary_recruiter = 1) 
        as recruiter_in_charge", false);

        $this->db->select('CASE
        when rta.status = 2 THEN rta.invitation_cancel_at 
        when inter_det.mark_as_no_show = 1 THEN inter_det.marked_date 
        else rta.created 
        END as manual_order', false);

        $this->db->from("tbl_recruitment_task as rt");
        $this->db->join('tbl_recruitment_task_applicant as rta', 'rt.id = rta.taskId AND rta.archive = 0 and applicant_id = ' . $applicant_id, 'inner');
        $this->db->join('tbl_recruitment_location as rl', 'rl.id = rt.training_location', 'inner');

        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as inter_det', 'inter_det.recruitment_task_applicant_id = rta.id ', 'INNER');
        $this->db->order_by('manual_order', 'desc');
        $this->db->where_in('rt.status', [1, 2]);
        $this->db->where("rt.task_stage", $task_stage);

        $query = $this->db->get();
        $result = $query->result();

        $current_interview = [];
        $history_interview = [];
        if (!empty($result)) {
            foreach ($result as $val) {
                $val->invitation_send_at = ($val->invitation_send_at == '0000-00-00 00:00:00') ? '' : $val->invitation_send_at;
                $val->invitation_accepted_at = ($val->invitation_accepted_at == '0000-00-00 00:00:00') ? '' : $val->invitation_accepted_at;

                if ($val->applicant_email_status == 2 || $val->mark_as_no_show == 1) {
                    $history_interview[] = $val;
                } else {
                    $current_interview = $val;
                }
            }

            if (empty($current_interview)) {
                $history_interview = obj_to_arr($history_interview);
                $current_interview = array_shift($history_interview);
            }

            if ($task_stage == 6) {
                $current_interview = obj_to_arr($current_interview);
                $x = $this->get_applicant_contract_details($current_interview['task_applicant_id']);
                $current_interview = array_merge($x, $current_interview);
            }
        }

        return ['history_interview' => $history_interview, 'current_interview' => $current_interview];
    }

    function get_applicant_contract_details($task_applicant_id) {
        $this->db->select(['signed_status', 'send_date', 'signed_date', 'id as contractId']);
        $this->db->from('tbl_recruitment_applicant_contract');
        $this->db->where(['archive' => 0, 'task_applicant_id' => $task_applicant_id]);

        $query = $this->db->get();
        $x = $query->row();

        if (!empty($x)) {
            $x->signed_date = ($x->signed_date == '0000-00-00 00:00:00') ? '' : $x->signed_date;
        } else {
            $x = [];
        }

        return (array) $x;
    }

    function update_applied_application_of_application($reqData) {
        if (!empty($reqData->applications)) {
            foreach ($reqData->applications as $val) {
                if (!empty($val->remove)) {
                    $this->basic_model->update_records('recruitment_applicant_applied_application', ['archive' => 1], ['id' => $val->id, 'applicant_id' => $reqData->applicant_id]);
                }

                if (!empty($val->id) && $val->id > 0) {
                    $update_app = array(
                        'position_applied' => $val->position_applied_id,
                        'recruitment_area' => $val->recruitment_area_id,
                        'employement_type' => $val->employement_type_id,
                        'channelId' => $val->channel_id,
                    );

                    $this->basic_model->update_records('recruitment_applicant_applied_application', $update_app, ['id' => $val->id, 'applicant_id' => $reqData->applicant_id]);
                } else {
                    $applications[] = array(
                        'applicant_id' => $reqData->applicant_id,
                        'position_applied' => $val->position_applied_id,
                        'recruitment_area' => $val->recruitment_area_id,
                        'employement_type' => $val->employement_type_id,
                        'channelId' => $val->channel_id,
                        'created' => DATE_TIME,
                        'archive' => 0,
                        'from_applicant_id' => 0,
                        'from_status' => 0,
                    );
                }
            }

            if (!empty($applications)) {
                $this->basic_model->insert_records('recruitment_applicant_applied_application', $applications, $multiple = true);
            }

            return true;
        }
    }

    function get_applicant_last_update($applicant_id) {
        $this->db->select(['lg.specific_title', 'lg.created']);
        $this->db->from('tbl_logs as lg');
        $this->db->join('tbl_module_title as mt', 'mt.id = lg.sub_module AND mt.key_name = "recruitment_applicant"', 'inner');
        $this->db->order_by('lg.created', 'desc');
        $this->db->where('lg.created_type', 1);
        $this->db->where('lg.userId', $applicant_id);


        $query = $this->db->get();
        $x = $query->row();
        if (!empty($x))
            return $x;
        else
            return [];
    }

    function get_applicant_logs($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'lg.created';
            $direction = 'desc';
        }

        $column = ['lg.specific_title', 'lg.created', "concat(firstname,' ',lastname) as recruiter"];
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $column)), false);
        $this->db->from('tbl_logs as lg');
        $this->db->join('tbl_module_title as mt', 'mt.id = lg.sub_module AND mt.key_name = "recruitment_applicant"', 'inner');
        $this->db->join('tbl_member as m', 'm.id = lg.created_by', 'inner');
        $this->db->order_by('lg.created', 'desc');
        $this->db->where('lg.created_type', 1);
        $this->db->where('lg.userId', $reqData->applicant_id);

        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $result = $query->result();

        $return = array('count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

    function request_for_pay_scal_approval($reqData, $adminId) {

        $pay_scal = array(
            'applicant_id' => $reqData->applicant_id,
            'requested_by' => $adminId,
            'approved_by' => '',
            'status' => 0, // set pending status
            'archived' => 0,
            'requested_at' => DATE_TIME,
            'relevant_notes' => '',
        );

        $pay_scal_id = $this->basic_model->insert_records('recruitment_applicant_pay_point_approval', $pay_scal, $multiple = FALSE);

        if ($reqData->pay_scale_approval) {
            foreach ($reqData->pay_scale_approval as $val) {
                $pay_scale_appoval_data[] = [
                    'work_area' => $val->work_area,
                    'pay_point' => $val->pay_point,
                    'pay_level' => $val->pay_level,
                    'applicant_id' => $reqData->applicant_id,
                    'pay_point_approval_id' => $pay_scal_id,
                    'is_approved' => 0
                ];
            }

            $this->basic_model->insert_records('recruitment_applicant_pay_point_options', $pay_scale_appoval_data, true);
            $this->basic_model->insert_records('recruitment_applicant_pay_point_options_before_approval', $pay_scale_appoval_data, true);
        }

        return true;
    }

    function get_applicant_pay_scale_approval_data($applicant_id) {
        $this->db->select(['ppa.id as pay_point_approval_id', 'ppa.status as pay_scale_approval_status', 'ppa.requested_at', 'ppa.approved_at']);
        $this->db->from('tbl_recruitment_applicant_pay_point_approval as ppa');
        $this->db->where('ppa.archived', 0);
        $this->db->where('ppa.applicant_id', $applicant_id);

        $query = $this->db->get();
        $pay_scale_data = $query->row();

        $pay_scale_approval = [];
        if (!empty($pay_scale_data)) {
            if ($pay_scale_data->pay_scale_approval_status == 1) {
                $pay_scale_approval = $this->basic_model->get_record_where('recruitment_applicant_pay_point_options', ['work_area', 'pay_point', 'pay_level'], ['pay_point_approval_id' => $pay_scale_data->pay_point_approval_id]);
            } else {
                $pay_scale_approval = $this->basic_model->get_record_where('recruitment_applicant_pay_point_options_before_approval', ['work_area', 'pay_point', 'pay_level'], ['pay_point_approval_id' => $pay_scale_data->pay_point_approval_id]);
            }
        } else {
            $pay_scale_data = [];
        }

        return ['pay_scale_details' => $pay_scale_data, 'pay_scale_approval' => $pay_scale_approval];
    }

    function update_applicant_reference_status_note($reqData) {
        $update_data = array(
            'relevant_note' => !empty($reqData->relevant_note) ? $reqData->relevant_note : '',
            'status' => ($reqData->status == 1) ? 1 : 2
        );

        $where = array('applicant_id' => $reqData->applicant_id, 'id' => $reqData->reference_id);

        $this->basic_model->update_records('recruitment_applicant_reference', $update_data, $where);

        return true;
    }

    function update_applicant_details($reqData) {
        // check any duplicant applicant exist with acceprt status
        $duplicated_response = $this->check_any_duplicate_applicant($reqData->id, 2);
        $applicantIds = [];

        if (!empty($duplicated_response)) {
            $duplicated_response = obj_to_arr($duplicated_response);

            $applicantIds = array_column($duplicated_response, 'id');
        }

        $applicantIds[] = $reqData->id;

        // update applicant details
        $this->update_applicant_info($reqData, $applicantIds);

        $this->update_applicant_emails($reqData, $applicantIds);

        $this->update_applicant_address($reqData, $reqData->id);

        $this->update_applicant_references($reqData, $reqData->id);

        $this->update_applicant_phone_number($reqData, $applicantIds);

        return true;
    }

    function update_applicant_info($reqData, $applicantIds) {
        // update applicant details
        $applicant_det = ['firstname' => $reqData->firstname, 'lastname' => $reqData->lastname];

        $this->db->where_in('id', $applicantIds);
        $this->db->where('archive', 0);
        $this->db->update(TBL_PREFIX . 'recruitment_applicant', $applicant_det);
    }

    function update_applicant_emails($reqData, $applicantIds) {
        if (!empty($reqData->emails)) {

            foreach ($reqData->emails as $val) {
                $this->db->where_in('applicant_id', $applicantIds);
//                $this->db->where('id', $val->id);
                $this->db->update(TBL_PREFIX . 'recruitment_applicant_email', ['email' => $val->email]);
            }
        }
    }

    function update_applicant_address($reqData, $applicantId) {
        if (!empty($reqData->addresses)) {
            foreach ($reqData->addresses as $key => $val) {
                $insert_add = [];
                $primary_add = ($key == 0) ? 1 : 2;
                $address = ['street' => $val->street, 'city' => $val->city, 'postal' => $val->postal, 'state' => $val->state, 'primary_address' => $primary_add, 'applicant_id' => $applicantId];

                if (!empty($val->id)) {
                    $this->basic_model->update_records('recruitment_applicant_address', $address, ['id' => $val->id, 'applicant_id' => $applicantId]);
                } else {
                    $address['created'] = DATE_TIME;
                    $insert_add[] = $address;
                }
            }

            if (!empty($insert_add)) {
                $this->basic_model->insert_records('recruitment_applicant_address', $insert_add, true);
            }
        }
    }

    function update_applicant_references($reqData, $applicantId) {
        if (!empty($reqData->references)) {
            $insert_ref = [];

            foreach ($reqData->references as $val) {
                $reference = ['name' => $val->name, 'email' => $val->email, 'phone' => $val->phone, 'updated' => DATE_TIME, 'applicant_id' => $applicantId];

                if (!empty($val->its_delete)) {
                    $this->basic_model->update_records('recruitment_applicant_reference', ['archive' => 1], ['id' => $val->id, 'applicant_id' => $applicantId]);
                } elseif (!empty($val->id)) {
                    $this->basic_model->update_records('recruitment_applicant_reference', $reference, ['id' => $val->id, 'applicant_id' => $applicantId]);
                } else {
                    $reference['created'] = DATE_TIME;
                    $insert_ref[] = $reference;
                }
            }

            if (!empty($insert_ref)) {
                $this->basic_model->insert_records('recruitment_applicant_reference', $insert_ref, true);
            }
        }
    }

    function update_applicant_phone_number($reqData, $applicantIds) {
        if (!empty($reqData->phones)) {
            foreach ($reqData->phones as $val) {

                $this->db->where_in('applicant_id', $applicantIds);
//                $this->db->where('id', $val->id);
                $this->db->update(TBL_PREFIX . 'recruitment_applicant_phone', ['phone' => $val->phone]);
            }
        }
    }

    public function get_all_attachments_by_applicant_id($applicantId = 0) {
        $data = [];
        $applicantId = (int) $applicantId;
        $this->db->select(['rasa.applicant_id', 'rasa.id', 'rasa.attachment_title', 'rasa.attachment', 'rasa.stage as stage_level', 'rasa.created', 'rasa.archive']);
        $this->db->select(['(select title from tbl_recruitment_job_requirement_docs as rjrd where rasa.doc_category=rjrd.id) category_title',
            '(case when is_main_stage_label=0 then (select stage From tbl_recruitment_stage as rs where rasa.stage=rs.id) else (select stage_number From tbl_recruitment_stage_label as rsl where rasa.stage=rsl.id) END) as current_stage_level'], false);
        $this->db->from("tbl_recruitment_applicant_stage_attachment as rasa");
        $this->db->where("rasa.applicant_id", $applicantId);
        $this->db->order_by('rasa.archive_at', 'DESC');
        $this->db->order_by('rasa.created', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }

        if (!empty($data)) {
            foreach ($data as $val) {
                $val->url = base_url() . 'mediaShow/r/' . $val->id . '/' . $val->attachment;
            }
        }

        return $data;
    }

    public function get_attachment_details_by_ids($ids = [], $applicantId = 0) {
        $response = [];
        $idsData = !empty($ids) ? $ids : 0;
        $idsData = !empty($ids) && is_array($ids) ? $ids : [$idsData];
        $this->db->select('attachment as filename');
        $this->db->where_in('id', $idsData);
        $this->db->where('applicant_id', $applicantId);
        //$this->db->where('archive', 0);
        $query = $this->db->get('tbl_recruitment_applicant_stage_attachment');

        if ($query->num_rows() > 0) {
            $response = $query->result();
        }
        return $response;
    }

    public function archived_attachment_details_by_ids($ids = [], $applicantId = 0) {
        $response = [];
        $idsData = !empty($ids) ? $ids : 0;
        $idsData = !empty($ids) && is_array($ids) ? $ids : [$idsData];
        $this->db->where_in('id', $idsData);
        $this->db->where('applicant_id', $applicantId);
        $this->db->where('archive', 0);
        $this->db->update('tbl_recruitment_applicant_stage_attachment', ['archive' => 1, 'archive_at' => DATE_TIME]);
        return $this->db->affected_rows();
    }

    public function check_any_duplicate_applicant($applicantId, $status) {
        $this->db->select('ra.id');
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_duplicate_status as rads', 'rads.applicant_id = ra.id AND rads.archive = 0 AND rads.status = ' . $status, 'INNER');
        $this->db->where('ra.duplicatedId', $applicantId);
        $this->db->where('ra.duplicated_status', 1);

        $this->db->group_by('ra.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->result();
    }

    public function check_email_already_exist_to_another_applicant($applicantId, $email) {
        $this->db->select('ra.id');
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_email as rae', 'rae.applicant_id = ra.id  AND rae.archive = 0', 'inner');
        $this->db->where('ra.id !=', $applicantId);
        $this->db->where('rae.email', $email);
        $this->db->where('ra.archive', 0);
        $this->db->where('ra.duplicated_status', 0);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->result();
    }

    public function check_phone_already_exist_to_another_applicant($applicantId, $phone) {
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('+', '', $phone);
//       print_r($phone);
        $this->db->select('ra.id');
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_phone as rap', 'rap.applicant_id = ra.id AND rap.archive = 0', 'inner');
        $this->db->where('ra.id !=', $applicantId);
        $this->db->where("REPLACE(REPLACE(rap.phone,' ',''), '+','') =", $phone);
        $this->db->where('ra.archive', 0);
        $this->db->where('ra.duplicated_status', 0);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $res = $query->row();

        return $query->row();
    }

    public function get_applicant_mandatory_doucment_list($applicantId) {
        $this->db->select(['rjrd.id as recruitment_doc_id', 'rjrd.title']);
        $this->db->select("(select CONCAT(a.is_required,'|_@BREACKER@_|',a.is_approved,'|_@BREACKER@_|',id,'|_@BREACKER@_|',applicant_id) from tbl_recruitment_applicant_doc_category as a where archive = 0 AND a.recruitment_doc_id = rjrd.id and a.applicant_id = " . $applicantId . ") as applicant_doc");
        $this->db->from('tbl_recruitment_job_requirement_docs as rjrd');
        $this->db->where('rjrd.archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $res = $query->result();

        if (!empty($res)) {
            foreach ($res as $val) {
                $val->applicant_doc;
                $x = explode('|_@BREACKER@_|', $val->applicant_doc);


                $val->is_required = ($x[0] == 0) ? '0' : '1';
                $val->is_approved = isset($x[1]) ? $x[1] : '';
                $val->applicant_doc_id = isset($x[2]) ? $x[2] : '';
                $val->assined = isset($x[3]) ? $x[3] : '';

                unset($val->applicant_doc);
            }
        }


        return $res;
    }

    public function update_mandatory_optional_docs_for_applicant($reqData) {
        $insert_data = [];

        if ($reqData->applicant_document_cat) {
            foreach ($reqData->applicant_document_cat as $val) {

                if (!empty($val->applicant_doc_id) && empty($val->assined)) {
                    $update_data = ['archive' => 1, 'updated' => DATE_TIME];

                    $this->basic_model->update_records('recruitment_applicant_doc_category', $update_data, ['id' => $val->applicant_doc_id]);
                } elseif (!empty($val->applicant_doc_id) && !empty($val->assined)) {

                    $update_data = ['is_required' => (($val->is_required === '1') ? 1 : 0), 'updated' => DATE_TIME];

                    $this->basic_model->update_records('recruitment_applicant_doc_category', $update_data, ['id' => $val->applicant_doc_id]);
                } elseif (!empty($val->assined)) {
                    $insert_data[] = [
                        'applicant_id' => $reqData->applicant_id,
                        'recruitment_doc_id' => $val->recruitment_doc_id,
                        'is_required' => $val->is_required,
                        'is_approved' => 0,
                        'created' => DATE_TIME,
                        'updated' => DATE_TIME,
                        'archive' => 0
                    ];
                }
            }

            if (!empty($insert_data)) {
                $this->basic_model->insert_records('recruitment_applicant_doc_category', $insert_data, True);
            }
        }


        return true;
    }

    public function save_application_stage_note($reqData) {
        $response = ['status' => false, 'error' => 'Something went wrong.'];
        $appId = isset($reqData->applicantId) ? $reqData->applicantId : '';
        $notes = isset($reqData->note) ? $reqData->note : '';

        if (!empty($appId) && $appId > 0 && !empty($notes)) {

            $dataIns = [
                'applicant_id' => $appId,
                'notes' => $notes,
                'created' => DATE_TIME,
                'stage' => $reqData->current_stage,
                'is_main_stage_label' => (int) $reqData->is_main_stage,
                'recruiterId' => (int) $reqData->recruiterId,
            ];
            $dd = $this->basic_model->insert_records('recruitment_applicant_stage_notes', $dataIns);

            if ($dd) {
                $response = ['status' => true];
            } else {
                $response = ['status' => false, 'error' => 'Something went wrong.'];
            }
        }
        return $response;
    }

    public function get_attachment_stage_notes_by_applicant_id($applicantId = 0) {
        $data = [];
        $applicantId = (int) $applicantId;
        $this->db->select(['rasn.applicant_id', 'rasn.id', 'rasn.notes', 'rasn.is_main_stage_label', 'date_format(rasn.created,"%d/%m/%Y") as created_date']);
        $this->db->select([
            '(case when recruiterId>0 then (select concat_ws(" ",firstname,middlename,lastname) From tbl_member as m  WHERE m.id=rasn.recruiterId) else ("") END) as created_by',
            '(case when is_main_stage_label=0 then (select stage From tbl_recruitment_stage as rs where rasn.stage=rs.id) else (select stage_number From tbl_recruitment_stage_label as rsl where rasn.stage=rsl.id) END) as current_stage_level'], false);
        $this->db->from("tbl_recruitment_applicant_stage_notes as rasn");
        $this->db->where("rasn.applicant_id", $applicantId);
        $this->db->order_by('rasn.created', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result();
        }
        return $data;
    }

    function check_applicant_onboarding_eligibility($task_applicant_id,$type='onboarding') {
        $typeData = $type == 'onboarding' ? 'interview_det.app_login_status' :'interview_det.app_orientation_status';
    
        $this->db->select([$typeData.' as status_onboarding']);
        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.taskId = rt.id AND rta.archive = 0 AND rta.status IN (0,1) AND rta.id = ' . $task_applicant_id, 'inner');
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as interview_det', 'rta.id = interview_det.recruitment_task_applicant_id AND interview_det.archive = 0', 'inner');

        $this->db->where('interview_det.recruitment_task_applicant_id', $task_applicant_id);
        $this->db->where('rt.status', 1);

        $this->db->group_by('rt.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row('status_onboarding');

        return $result;
    }

    function add_applicant_orientation($task_applicant_id,$status) {
        $where = ['recruitment_task_applicant_id' => $task_applicant_id];
        $data = ['app_orientation_status' => $status];

        $this->basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', $data, $where);
    }

    function add_applicant_onboarding($task_applicant_id,$status) {
        $where = ['recruitment_task_applicant_id' => $task_applicant_id];
        $data = ['app_login_status' =>$status];

        $this->basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', $data, $where);
    }
    function resend_docusign_enevlope($applicantData){
            
            $task_applicant_id=$applicantData->task_applicant_id;
            $applicant_id=$applicantData->applicant_id;
            
            $applicant_info=$this->get_applicant_contract_data($applicant_id);
            
            $this->db->select(['appContract.envelope_id']);
            $this->db->from('tbl_recruitment_applicant_contract as appContract');
            $this->db->where('appContract.task_applicant_id', $task_applicant_id);
            $this->db->where('appContract.signed_status', 0);
            $this->db->where('appContract.archive=0');
            // order by id desc
            $this->db->limit(1);
        
            $query_task_info = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            if($query_task_info->num_rows() > 0)
            {   
                $info = $query_task_info->row_array();
                $applicant_info['envelope_id']=$info['envelope_id'];  
            }
            return $applicant_info;      
    }
   

    public function get_applicant_contract_data($applicant_id=0){ 
        $this->db->select(array("concat(ra.firstname,' ',ra.middlename,' ',ra.lastname) as applicant_name"));
        $this->db->select(['(SELECT email FROM tbl_recruitment_applicant_email where primary_email=1 and archive=0 AND applicant_id=ra.id) as email',false]);
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->where(array('ra.id'=>$applicant_id));
        $this->db->where(array('ra.archive='=>0));
        $query_task_info = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $info = $query_task_info->row_array();       
        return $info; 
    }

}
