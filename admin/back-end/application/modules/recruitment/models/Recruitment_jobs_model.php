<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_jobs_model extends CI_Model {

	public function __construct() {
        // Call the CI_Model constructor
		parent::__construct();
	}

	public function create_Jobs_delete($objJobs) {
		$tbl_recruitment_jobs= TBL_PREFIX.'recruitment_jobs';

		$arrJobs = array();
		$arrJobs['jobname']=$objJobs->getJobName();
		$arrJobs['status']=$objJobs->getStatus();							
		$arrJobs['job_position']=$objJobs->getJobPosition();
		$arrJobs['phone']=$objJobs->getPhone();
		$arrJobs['email']=$objJobs->getEmail();			
		$arrJobs['weblink']=$objJobs->getWebLink();
		$arrJobs['job_description']=$objJobs->getJobDescription();				
		$arrJobs['job_start_date']=$objJobs->getJobStartDate();
		$arrJobs['job_end_date']=$objJobs->getJobEndDate();	

		if($objJobs->getId()>0){
			$this->db->where('id', $objJobs->getId());
			$insert_query =$this->db->update($tbl_recruitment_jobs,$arrJobs);
			$this->db->last_query();
			
		}else{
			$arrJobs['created'] = $objJobs->getCreated();
			$insert_query = $this->db->insert($tbl_recruitment_jobs, $arrJobs);
			$questionid=$this->db->insert_id();		
			//$this->insert_answer($questionid,$objQuestion->getAnswer());
		} 
		return $insert_query;		
	}

	private function insert_answer($question,$arrAnswer){
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		foreach($arrAnswer as $key=>$value){
			$arAnswer = array();				
			$arAnswer['question'] = $question;
			$arAnswer['question_option'] = $value->value;
			$arAnswer['serial'] = $value->lebel;			
			$arAnswer['answer'] = $value->checked;
			$this->db->insert($tbl_question_answer, $arAnswer);
		}		
	}

	private function update_answer($question,$arrAnswer){
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		foreach($arrAnswer as $key=>$value){
			$arAnswer = array();				
			$arAnswer['question'] = $question;
			$arAnswer['question_option'] = $value->value;
			$arAnswer['serial'] = $value->lebel;			
			$arAnswer['answer'] = $value->checked;			
			$this->db->where('id', $value->answer_id);
			$this->db->update($tbl_question_answer,$arAnswer);	
		}
	}

	public function delete_Questions($objQuestion){
		$tbl_question= TBL_PREFIX.'recruitment_additional_questions'; 
		$this->db->where('id', $objQuestion->getId());
		$this->db->update($tbl_question,array('archive'=>1));
		return $this->db->affected_rows();
	}

	private function remove_answer(){
		
	}
	
	public function question_list(){
		$tbl_question= TBL_PREFIX.'recruitment_additional_questions';
		$tbl_question_topic= TBL_PREFIX.'recruitment_question_topic';
		$tbl_question_answer= TBL_PREFIX.'recruitment_additional_questions_answer';
		
		$select_column = array($tbl_question . ".id", $tbl_question . ".question", $tbl_question . ".status", $tbl_question . '.created', $tbl_question . '.training_category',$tbl_question . '.question_type',$tbl_question . '.question_topic',$tbl_question . ".created_by" ,$tbl_question . ".updated" ,$tbl_question_topic . ".topic");
		
		$dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
		$this->db->from($tbl_question);
		$this->db->where('archive', '0');
		$this->db->join($tbl_question_topic,$tbl_question.'.question_topic =' . $tbl_question_topic . '.id', 'inner');		
		$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
		$dataResult = $query->result();
		$dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
		
		foreach($dataResult as $data){				

			$select_answer_column = array($tbl_question_answer . ".id as answer_id",$tbl_question_answer . ".answer as checked",$tbl_question_answer . ".question_option as value", $tbl_question_answer . ".serial as lebel");	
			$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_answer_column)), false);
			$this->db->from($tbl_question_answer);
			$this->db->where('question',$data->id);		
			$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
			$dataAnsResult = $query->result_array();
			$data->answers=$dataAnsResult;				
		}				
		$return = array('count' => $dt_filtered_total, 'data' => $dataResult);
		return $return;		
	}

	public function save_job($post_data) 
	{
		if (!empty($post_data)) 
		{
			$save_as = 0;
			$job_operation = $post_data['job_operation'];
			
			if(isset($post_data['save_as_draft']) && $post_data['save_as_draft'] == 1 && $post_data['post_job'] == '' ){
				$save_as = 0;
			}
			else if(isset($post_data['post_job']) && $post_data['post_job'] == 1 && $post_data['save_as_draft'] == '' ){
				
				if(strtotime(DateFormate($post_data['from_date'])) == strtotime(date('Y-m-d')))
					$save_as = 3;
				if(strtotime(DateFormate($post_data['from_date'])) > strtotime(date('Y-m-d')))
					$save_as = 5;
			}

			if(isset($post_data['is_recurring']) && $post_data['is_recurring'] ==1)
				$to_date = '';			
			else
				$to_date = DateFormate($post_data['to_date']);	
			
			$organisation_data = array('title' => '',
				'type' => $post_data['type'],
				'category' => $post_data['category'],
				'sub_category' => $post_data['sub_category'],
				'position' => $post_data['position'],
				'employment_type' => $post_data['employment_type'],
				'salary_range' => $post_data['salary_range'],
				'is_salary_publish' => isset($post_data['is_salary_publish']) ? $post_data['is_salary_publish'] : '0',
				'template' => $post_data['activeTemplate'],
				'created'=>DATE_TIME,
				'from_date' => isset($post_data['from_date'])?DateFormate($post_data['from_date']):'',
				'to_date' => $to_date,
				'is_recurring' => isset($post_data['is_recurring'])?$post_data['is_recurring']:0,
			);
			
			if($job_operation == 'E'){
				$jobId = $post_data['job_id'];
				if(isset($post_data['job_status']) && ($post_data['job_status']!=3 && $post_data['job_status']!=5)){
					$organisation_data['job_status'] = $save_as;
				}else{
					$save_as = $post_data['job_status'];
				}
				$this->Basic_model->update_records('recruitment_job', $organisation_data,array('id'=>$jobId));
			}
			else{
				$organisation_data['job_status'] = $save_as;
				$jobId = $this->Basic_model->insert_records('recruitment_job', $organisation_data);
			}

			$address = [];
			if(isset($post_data['complete_address']) && !empty($post_data['complete_address']))
			{
				$address = get_address_from_google_response($post_data['complete_address']);
			}
			
			$organisation_adr = array('jobId' => $jobId,
				'street' => isset($address['street_no'])?$address['street_no']:''.' '.isset($address['street'])?$address['street']:'',
				'city' => isset($address['city'])?$address['city']:'',
				'state' => isset($address['state_id'])?$address['state_id']:'',
				'postal' => isset($address['postal_code'])?$address['postal_code']:'',
				'lat' => isset($address['lat'])?$address['lat']:'',
				'long' => isset($address['long'])?$address['long']:'',
				'complete_address' => isset($address['full_address'])?$address['full_address']:'',
				'phone' => $post_data['phone'],
				'email' => $post_data['email'],
				'website' => $post_data['website'],
				'created'=>DATE_TIME,
				'google_response'=>json_encode($post_data['complete_address'])
			);
			
			if (!empty($organisation_adr)){
				if($job_operation == 'E'){
					$this->Basic_model->update_records('recruitment_job_location', $organisation_adr,array('jobId'=>$jobId,'id'=>$post_data['job_location_id']));
				}
				else{
					$this->Basic_model->insert_records('recruitment_job_location', $organisation_adr);
				}
			}

			if (!empty($post_data['publish_to'])) 
			{
				$job_published = array();

				if($job_operation == 'E'){
					$this->Basic_model->update_records('recruitment_job_published_detail', array('archive'=>1),array('jobId'=>$jobId));
					$this->Basic_model->update_records('recruitment_job_question', array('archive'=>1),array('jobId'=>$jobId));
				}

				foreach ($post_data['publish_to'] as $key => $val) 
				{
					$job_published = array('jobId' => $jobId,
						'channel' => isset($val['drp_dwn'])?$val['drp_dwn']['value']:'',
						'created'=>DATE_TIME,
					);
					$published_detail_id = $this->Basic_model->insert_records('recruitment_job_published_detail', $job_published);
					
					if(!empty($val['question']))
					{
						foreach ($val['question'] as $que)
						{
							$ar = array('question'=>$que['question'],'created'=>DATE_TIME,'jobId' => $jobId,'published_detail_id'=>$published_detail_id);
							$this->Basic_model->insert_records('recruitment_job_question', $ar);
						}
					}
				}
			}

			if(!empty($post_data['all_documents']))
			{
				if($job_operation == 'E'){
					$this->Basic_model->update_records('recruitment_job_posted_docs', array('archive'=>1),array('jobId'=>$jobId));
				}
				$ar = [];
				foreach ($post_data['all_documents'] as $docs)
				{
					if(isset($docs['clickable']) && $docs['clickable']){
						$ar[] = array('requirement_docs_id'=>$docs['value'],'created'=>DATE_TIME,'jobId' => $jobId,'is_required'=>isset($docs['mandatory']) && $docs['mandatory'] ==true ?1:0);
					}
				}
				if(!empty($ar))
					$this->Basic_model->insert_records('recruitment_job_posted_docs', $ar,true);
			}

			$template_detail = array('jobId' => $jobId,
				'template' => $post_data['activeTemplate'],
				'layout_text' => $post_data['job_content'],
				'created'=>DATE_TIME
			);
			
			if($job_operation == 'E'){
				$this->Basic_model->update_records('recruitment_selected_job_template', array('archive'=>1),array('jobId'=>$jobId));
			}
			$this->Basic_model->insert_records('recruitment_selected_job_template', $template_detail);

			$msg = '';
			#pr($organisation_adr);
			//echo $save_as.'<br/>';
			//var_dump($save_as);
			$save_as = (int)$save_as;
			//echo '->>>';
			 $job_status_str = ($save_as === 0)?'Draft':(($save_as === 5)?'Scheduled':(($save_as === 3)?'Live':''));
			
			
			if($job_operation == 'E')
				$msg = 'Job updated as '.$job_status_str.' successfully.';
			else
				$msg = 'Job saved as '.$job_status_str.' successfully.';

			return array('jobId' => $jobId,'msg'=>$msg);
		}
	}

	public function get_all_jobs($reqData)
	{
		$limit = $reqData->pageSize;
		$page = $reqData->page;
		$sorted = $reqData->sorted;
		$filter = $reqData->filtered;
		$orderBy = '';
		$direction = '';
		$tbl_job = TBL_PREFIX . 'recruitment_job';

		if (!empty($sorted)) {
			if (!empty($sorted[0]->id)) {
				$orderBy = $sorted[0]->id;
				$direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
			}
		} else {
			$orderBy = $tbl_job . '.id';
			$direction = 'DESC';
		}

		$this->db->where($tbl_job . ".job_status".' =', $reqData->filtered->filter_by);	
		$this->db->where($tbl_job . ".archive".' !=', 1);  //deleted job

		if (!empty($reqData->filtered->srch_value)) {
			$this->db->group_start();
			$src_columns = array("CONCAT(tbl_recruitment_applicant.firstname,' ',tbl_recruitment_applicant.middlename,' ',tbl_recruitment_applicant.lastname)", "CONCAT(tbl_recruitment_applicant.firstname,' ',tbl_recruitment_applicant.lastname)", "tbl_recruitment_job_position.title as position","tbl_recruitment_job_category.name as job_category","tbl_recruitment_job_employment_type.title as employment_type");

			for ($i = 0; $i < count($src_columns); $i++) {
				$column_search = $src_columns[$i];
				if (strstr($column_search, "as") !== false) {
					$serch_column = explode(" as ", $column_search);
					$this->db->or_like($serch_column[0], $filter->srch_value);
				} else {
					$this->db->or_like($column_search, $filter->srch_value);
				}
			}
			$this->db->group_end();
		}

		$select_column = array($tbl_job . ".id",$tbl_job . ".created", $tbl_job . ".job_status", "tbl_recruitment_job_position.title as position","tbl_recruitment_job_category.name as job_category","(select count('id') from tbl_recruitment_applicant where `tbl_recruitment_applicant`.`jobId` = `tbl_recruitment_job`.`id` AND `tbl_recruitment_applicant`.`archive` = 0 ) as applicant_cnt","tbl_recruitment_job_employment_type.title as employment_type");

		$this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
		$this->db->from($tbl_job);

		$this->db->join('tbl_recruitment_job_position', 'tbl_recruitment_job_position.id = ' . $tbl_job . '.position AND tbl_recruitment_job_position.archive = 0', 'inner');
		$this->db->join('tbl_recruitment_job_category', 'tbl_recruitment_job_category.id = ' . $tbl_job . '.category AND tbl_recruitment_job_category.archive = 0', 'inner');
		$this->db->join('tbl_recruitment_job_employment_type', 'tbl_recruitment_job_employment_type.id = ' . $tbl_job . '.employment_type AND tbl_recruitment_job_employment_type.archive = 0', 'inner');
        #$this->db->join('tbl_recruitment_job_published_detail', 'tbl_recruitment_job_published_detail.jobId = ' . $tbl_job . '.id AND tbl_recruitment_job_published_detail.archive = 0', 'inner');
		$this->db->join('tbl_recruitment_applicant', 'tbl_recruitment_applicant.jobId = ' . $tbl_job . '.id AND tbl_recruitment_applicant.archive = 0 AND tbl_recruitment_applicant.status = 1', 'left');

		$this->db->group_by($tbl_job . ".id");
		$this->db->order_by($orderBy, $direction);
		$this->db->limit($limit, ($page * $limit));

		$query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       ## last_query(); 
		$dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
		if ($dt_filtered_total % $limit == 0) {
			$dt_filtered_total = ($dt_filtered_total / $limit);
		} else {
			$dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
		}

		$dataResult = $query->result();

		$status = false;
		if (!empty($dataResult)) {
			$status = true;
			foreach ($dataResult as $val) {
				$val->job_status = (isset($val->job_status) && $val->job_status == 0 ?'Draft':(($val->job_status == 5)?'Scheduled':(($val->job_status == 2)?'Closed':(($val->job_status == 3)?'Live':'Canceled'))));
				$val->website_url = 'https://www.web_1.com';
				$val->seek_url = 'https://www.seek_1.com';
				$val->position = $val->position;
				$val->created = date('d/m/y',strtotime($val->created));
			}
		}
		$return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count,'status'=>true);
		return $return;
	}


	public function get_job_detail($reqData)
	{
		if(!empty($reqData))
		{
			$job_id = $reqData->jobId;
			$tbl_job = TBL_PREFIX . 'recruitment_job'; 

			$job_data = "SELECT  rj.is_recurring,DATE_FORMAT(rj.from_date,'%Y/%m/%d') as from_date,DATE_FORMAT(rj.to_date,'%Y/%m/%d') as to_date,rj.type,rj.category,rj.sub_category,rj.position,rj.employment_type,rj.salary_range,rj.is_salary_publish,jl.google_response,jl.complete_address as job_location,jl.phone,jl.email,jl.website,jl.id as job_location_id,sjt.layout_text,sjt.template as activeTemplate,rj.job_status FROM `tbl_recruitment_job` as rj
			LEFT JOIN `tbl_recruitment_job_location` as jl ON jl.jobId = rj.id AND jl.archive = '0'
			LEFT JOIN `tbl_recruitment_selected_job_template` as sjt ON sjt.jobId = rj.id AND sjt.archive = '0'
			WHERE rj.archive = '0' AND rj.id = '".$job_id."' AND rj.job_status!=4 ";
			$job_data_ex = $this->db->query($job_data);

			$job_data_ary = $job_data_ex->row_array();

			if(empty($job_data_ary) || is_null($job_data_ary))
			{
				$return = array('status' => false);
				return $return;
			}


			$publish_data = "SELECT  jpd.channel as drp_dwn_val,rc.channel_name,rc.id as channel_id FROM `tbl_recruitment_job_published_detail` as jpd
			INNER JOIN `tbl_recruitment_channel` as rc ON jpd.channel = rc.id AND rc.archive = '0'
			WHERE  jpd.archive = '0'  AND jpd.jobId  = '".$job_id."'";
			$publish_data_ex = $this->db->query($publish_data);
			$publish_ary = $publish_data_ex->result_array();
			$publish_data_ary =  isset($publish_ary) && !empty($publish_ary)?$publish_ary:array();
			if(!empty($publish_data_ary))
			{
				$jobs_chaneel_question = $this->get_job_question($job_id);
				if(!empty($jobs_chaneel_question))
				{
					$publish_data_ary= array_map(function ($val) use ($jobs_chaneel_question,$publish_data_ary) {
						$val['question']= array_key_exists($val['channel_id'], $jobs_chaneel_question)?$jobs_chaneel_question[$val['channel_id']]:array();
						$val['drp_dwn']= array('label'=>$val['channel_name'],'value'=>$val['channel_id']);
						return $val;
					}, $publish_data_ary);
				}
				else
				{
					$publish_data_ary= array_map(function ($val) use ($publish_data_ary) {
						$val['drp_dwn']= array('label'=>$val['channel_name'],'value'=>$val['channel_id']);
						return $val;
					}, $publish_data_ary);
				}
			}

			$response = $this->basic_model->get_record_where('recruitment_job_employment_type', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
			$emp_type_rows = isset($response) && !empty($response) ? $response:array();

			$responsePosition = $this->basic_model->get_record_where('recruitment_job_position', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
			$pos_rows  = isset($responsePosition) && !empty($responsePosition) ? $responsePosition:array();

			$responseCategory = $this->basic_model->get_record_where('recruitment_job_category', $column = array('parent_id', 'id as value','name as label'), $where = array('archive' => '0'));
			$data_category = isset($responseCategory) && !empty($responseCategory) ? $responseCategory:array();

			$responseSubCategory = $this->basic_model->get_record_where('recruitment_job_category', $column = array('parent_id', 'id as value','name as label'), $where = array('archive' => '0','parent_id'=>$job_data_ary['category']));
			$data_sub_category = isset($responseSubCategory) && !empty($responseSubCategory) ? $responseSubCategory:array();

			$responseSalaryRange = $this->basic_model->get_record_where('recruitment_job_salary_range', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
			$data_salaryRange = isset($responseSalaryRange) && !empty($responseSalaryRange) ? $responseSalaryRange:array();

			$responseJobType = $this->basic_model->get_record_where('recruitment_job_type', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
			$job_type_rows = isset($responseJobType) && !empty($responseJobType) ? $responseJobType:array();

			$all_docs = $this->basic_model->get_record_where('recruitment_job_requirement_docs', $column = array('title', 'id'), $where = array('archive' => '0'));
			
			$documnets = "SELECT  tbl_recruitment_job_posted_docs.requirement_docs_id ,tbl_recruitment_job_posted_docs.is_required FROM `tbl_recruitment_job_posted_docs`
			WHERE jobId = '".$job_id."' AND tbl_recruitment_job_posted_docs.archive=0 ";
			$wsql = $this->db->query($documnets);
			$rows = $wsql->result_array();
			if(!empty($rows))
			{
				$rows = pos_index_change_array_data($rows, 'requirement_docs_id');
			}

			if (!empty($all_docs)) 
			{ 
				foreach ($all_docs as $val) 
				{
					$val->label = $val->title;
					$val->value = $val->id;
					if(array_key_exists($val->id, $rows))
					{
						$val->clickable = true;
						if($rows[$val->id]['is_required'] == 1)
						{
							$val->optional = false;
							$val->mandatory = true;
						}
						else
						{
							$val->optional = true;
							$val->mandatory = false;
						}
					}
					else
					{
						$val->selected = false;
						$val->optional = false;
						$val->mandatory = false;
					}
				}
			}
			
			$main_aray = array();
			$main_aray['all_documents'] = $all_docs;
			$main_aray['job_employment_type'] = $emp_type_rows;
			$main_aray['job_position'] = $pos_rows;
			$main_aray['job_sub_category'] = $data_sub_category;
			$main_aray['job_category'] = $data_category;
			$main_aray['job_type'] = $job_type_rows;
			$main_aray['publish_to'] = $publish_data_ary;
			$main_aray['job_salary_range'] = $data_salaryRange;

			$main_aray['check_documents'] = true;
			$main_aray['mode'] = 'view';
			$main_aray['from_date'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['from_date']!=''?$job_data_ary['from_date']:'';
			$main_aray['is_recurring'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['is_recurring']!=''?$job_data_ary['is_recurring']:false;
			$main_aray['to_date'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['to_date']!=''?$job_data_ary['to_date']:'';
			$main_aray['job_status'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['job_status']!=''?$job_data_ary['job_status']:'';

			$main_aray['is_salary_publish'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['is_salary_publish']== 1?true:false;
			$main_aray['category'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['category']!=''?$job_data_ary['category']:'';
			$main_aray['sub_category'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['sub_category']!=''?$job_data_ary['sub_category']:'';
			$main_aray['position'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['position']!=''?$job_data_ary['position']:'';
			$main_aray['employment_type'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['employment_type']!=''?$job_data_ary['employment_type']:'';
			$main_aray['salary_range'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['salary_range']!=''?$job_data_ary['salary_range']:'';
			$main_aray['complete_address'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['job_location']!=''?array('formatted_address'=>$job_data_ary['job_location']):'';
			$main_aray['phone'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['phone']!=''?$job_data_ary['phone']:'';
			$main_aray['email'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['email']!=''?$job_data_ary['email']:'';
			$main_aray['website'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['website']!=''?$job_data_ary['website']:'';
			$main_aray['job_content'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['layout_text']!=''?$job_data_ary['layout_text']:'';
			$main_aray['type'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['type']!=''?$job_data_ary['type']:'';
			$main_aray['activeTemplate'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['activeTemplate']!=''?$job_data_ary['activeTemplate']:'';
			$main_aray['google_response'] = isset($job_data_ary) && !empty($job_data_ary) && $job_data_ary['google_response']!=''?json_decode($job_data_ary['google_response']):'';
			$main_aray['job_location_id'] = $job_data_ary['job_location_id'];
			$temp['parentState'] = $main_aray;
			$return = array('status' => true, 'data' => $temp);
			return $return;
		}
	}

	public function get_job_question($job_id)
	{
		$sql = "SELECT rjq.id,rjq.question,rjpd.channel as value FROM tbl_recruitment_job_question as rjq 
		inner join tbl_recruitment_job_published_detail as rjpd on rjpd.jobId = rjq.jobId AND rjpd.id=rjq.published_detail_id and rjpd.archive=0
		WHERE rjq.jobId='".$job_id."' AND rjq.archive=0";
		$sql_ex = $this->db->query($sql);
		$output = $sql_ex->result_array();
		$outpt_ary = isset($output) && !empty($output)?$output:array(); 
		$temp_hold = array();
		$sub_ary = array();
		$main_aray = array();
		if(!empty($output))
		{
			foreach ($output as $key => $ot)
			{
				$index = $ot['value'];
				if(!in_array($ot['value'], $temp_hold))
				{
					$temp_hold[] = $index;
				}
				$sub_ary['question'] = $ot['question'];
				$sub_ary['value'] = $index;
				$sub_ary['btn_txt'] = 'Edit';
				$sub_ary['editable_class'] = '';
				$sub_ary['id'] = '';
				$sub_ary['question_edit'] = false;
				$main_aray[$index][] = $sub_ary;
			}
		}
		#pr($main_aray);
		return $main_aray;
	}

	public function save_job_required_documents($post_data) 
	{
		if (!empty($post_data)) 
		{		
			$template_detail = array('title' => $post_data['document_name'],
				'created'=>DATE_TIME
			);
			$id = $this->Basic_model->insert_records('recruitment_job_requirement_docs', $template_detail);
			if($id)
				return array('status' => true,'id'=>$id,'data'=>array('label' => $post_data['document_name'], 'value' => $id,'optional'=>false,'mandatory'=>true,'clickable'=>false,'selected'=>false));
			else
				return array('status' => false);
		}
	}

	public function update_job_status($post_data) 
	{
		if (!empty($post_data)) 
		{	
			$update_ary = array();

			if($post_data->status == 1)
				$update_ary['archive'] = 1;
			else
				$update_ary['job_status'] = $post_data->status;

			$id = $this->Basic_model->update_records('recruitment_job', $update_ary,array('id'=>$post_data->job_id));
			if($id)
				return array('status' => true);
			else
				return array('status' => false);
		}
	}
}