<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_task_action extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_recruiter_listing_for_create_task($reqData) {
        $search = $reqData->search;

        if (!empty($reqData->assigned_user)) {
            $already_assigned = json_decode(json_encode($reqData->assigned_user), true);
            $already_assigned_ids = array_column($already_assigned, 'value');

            if (!empty($already_assigned_ids)) {
                $this->db->where_not_in('a.id', $already_assigned_ids);
            }
        }

        $this->db->select(array('concat(a.firstname," ",a.lastname) as label', 'a.id as value', '"2" as primary_recruiter'), false);
        $this->db->from('tbl_member as a');
        $this->db->join('tbl_department as d', 'd.id = a.department AND d.short_code = "internal_staff"', 'inner');
        $this->db->join('tbl_recruitment_staff as rs', 'rs.adminId = a.id and rs.archive=a.archive', 'inner');

        $this->db->like('concat(a.firstname," ",a.lastname)', $search);
        $this->db->where('a.archive', 0);
        $this->db->where('a.status', 1);
        $this->db->where('rs.status', 1);
        $this->db->where('rs.approval_permission', 1);
        $this->db->where('rs.its_recruitment_admin', 0);

        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }

    function get_selected_recruiter($reqData) {
        $search = $reqData->selected_recruiter;
        $search = !empty($search) ? $search : [0];
        $search = !empty($search) && is_array($search) ? $search : [$search];

        $this->db->where_in('a.id', $search);
        $this->db->select(array('concat(a.firstname," ",a.lastname) as label', 'a.id as value', '"2" as primary_recruiter'), false);
        $this->db->from('tbl_member as a');
        $this->db->join('tbl_department as d', 'd.id = a.department AND d.short_code = "internal_staff"', 'inner');
        $this->db->join('tbl_recruitment_staff as rs', 'rs.adminId = a.id and rs.archive=a.archive', 'inner');
        $this->db->where('a.archive', 0);
        $this->db->where('a.status', 1);
        $this->db->where('rs.status', 1);
        $this->db->where('rs.approval_permission', 1);
        $this->db->where('rs.its_recruitment_admin', 0);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = ['status' => true, 'recruiter_data' => $query->result()];
        } else {
            $result = ['status' => true, 'recruiter_data' => []];
        }

        return $result;
    }

    function get_applicant_option_for_create_task($reqData, $adminId, $extra_paramter = []) {
        // here applicant_ids is optional parameter for check applicant stages (server side validation)

        $search = $reqData->search;
        $serchById = isset($reqData->type) && $reqData->type == 'id' ? true : false;
        $stage_label_id = 0;

        $stage_det = $this->basic_model->get_row('recruitment_task_stage', ['stage_label_id'], ['id' => $reqData->task_stage]);
        if (!empty($stage_det)) {
            $stage_label_id = $stage_det->stage_label_id;
        }

        $its_recruiter_admin = check_its_recruiter_admin($adminId);

        if (!empty($extra_paramter['applicant_ids'])) {
            $this->db->where_in('ra.id', $extra_paramter['applicant_ids']);
        }

        if (!empty($reqData->applicant_list)) {
            $already_assigned = json_decode(json_encode($reqData->applicant_list), true);
            $already_assigned_ids = array_column($already_assigned, 'value');

            if (!empty($already_assigned_ids)) {
                $this->db->where_not_in('ra.id', $already_assigned_ids);
            }
        }

        // if its not recruiter admin then add check only those applicant come who assigned him/her
        if (!$its_recruiter_admin) {
            $this->db->where('ra.recruiter', $adminId);
        }

        $this->db->select(array('concat(ra.firstname," ",ra.lastname) as label', 'ra.id as value', 'ra.id as applicant_id', 'rae.email', 'rap.phone'));
        $this->db->from('tbl_recruitment_applicant as ra');
        $this->db->join('tbl_recruitment_applicant_email as rae', 'rae.applicant_id = ra.id AND primary_email = 1', 'Inner');
        $this->db->join('tbl_recruitment_applicant_phone as rap', 'rap.applicant_id = ra.id AND primary_phone = 1', 'Inner');

        if ($serchById) {
            $this->db->where('ra.id', $search);
            $this->db->limit(1);
        } else {
            $this->db->like('concat(ra.firstname," ",ra.lastname)', $search);
        }
        if ($stage_label_id > 0) {
            $this->db->where("current_stage IN (SELECT id FROM tbl_recruitment_stage as rs WHERE archive = 0 AND stage_label_id = " . $stage_label_id . ")", null, false);
        }
        $this->db->where('ra.archive', 0);
        $this->db->where('ra.status', 1);
        $this->db->where('ra.flagged_status', 0);
        $this->db->where('ra.duplicated_status', 0);

        if ($stage_label_id > 0) {
            $this->db->where("not EXISTS (
            select s_rta.applicant_id,
            (case when s_rta.status = 1 AND (s_rsl.stage_number = 3 or s_rsl.stage_number = 6) 
                then (select mark_as_no_show from tbl_recruitment_applicant_group_or_cab_interview_detail where recruitment_task_applicant_id = s_rta.id AND archive = 0)
                else 0 end) as mark_as_no_show
            from tbl_recruitment_task_applicant as s_rta 
            inner join tbl_recruitment_task s_rt on s_rt.id = s_rta.taskId AND s_rt.status = 1
            inner join tbl_recruitment_stage_label s_rsl on s_rsl.stage_number = s_rt.task_stage AND s_rsl.id = " . $stage_label_id . " 
            where ra.id = s_rta.applicant_id AND s_rta.archive = 0 AND s_rta.status IN (1,0) having mark_as_no_show = 0
            )", null, false);
        }

        $this->db->group_by('ra.id');

        $query = $this->db->get();
        $result = $query->result();
//        last_query();
        return $result;
    }

    function create_task($reqData, $created_by) {

        $date = DateFormate($reqData->task_date, 'Y-m-d');
        $start_time = DateFormate($reqData->start_time, 'H:i:s');
        $end_time = DateFormate($reqData->end_time, 'H:i:s');

        $start_datetime = DateFormate($date . $start_time, 'Y-m-d H:i:s');
        $end_datetime = DateFormate($date . $end_time, 'Y-m-d H:i:s');

        $task_data = array(
            'task_name' => $reqData->task_name,
            'task_stage' => $reqData->task_stage,
            'created_by' => $created_by,
            'start_datetime' => $start_datetime,
            'end_datetime' => $end_datetime,
            'training_location' => $reqData->training_location,
            'mail_status' => 0,
            'relevant_task_note' => $reqData->relevant_task_note,
            'max_applicant' => $reqData->max_applicant,
            'created' => DATE_TIME,
            'action_at' => DATE_TIME,
            'status' => 1,
            'task_piority' => 1,
        );

        $taskId = $this->basic_model->insert_records('recruitment_task', $task_data, FALSE);


        $this->add_update_task_applicant($reqData->applicant_list, $taskId, $reqData->task_stage);

        $this->add_update_task_recruiter($reqData->assigned_user, $taskId);
    }

    function get_recruitment_task_list($reqData, $adminId) {
        $its_recruiterment_admin = check_its_recruiter_admin($adminId);

        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'rt.start_datetime';
            $direction = 'asc';
        }

        if (!empty($filter->filter_val)) {
            if ($filter->filter_val === 'in_progress') {
                $this->db->where('rt.status', 1);
            } elseif ($filter->filter_val === 'completed') {
                $this->db->where('rt.status', 2);
            } elseif ($filter->filter_val === 'archive') {
                $this->db->where('rt.status', 4);
            } elseif ($filter->filter_val === 'cancelled') {
                $this->db->where('rt.status', 3);
            }
        }
        //$extraCond = " 1=1";
        $extraCondSub = " ";
        //$extraCond = "rtra.primary_recruiter=1 ";
        if (isset($filter->recruiterId) && !empty($filter->recruiterId)) {

            //$extraCond = "rtra.recruiterId='" . $filter->recruiterId . "'";
            $this->db->where('rtra.recruiterId', $filter->recruiterId);
            $extraCondSub .= " AND sub_rtra.recruiterId='" . $filter->recruiterId . "' ";
            if (isset($filter->actionTypeSatge) && !empty($filter->actionTypeSatge)) {
                $stageTypeFilter = strtolower($filter->actionTypeSatge);
                if ($stageTypeFilter == 'group') {
                    $this->db->where('rt.task_stage', 3);
                } else if ($stageTypeFilter == 'cab') {
                    $this->db->where('rt.task_stage', 6);
                } else if ($stageTypeFilter == 'other') {
                    $this->db->where_not_in('rt.task_stage', [3, 6]);
                } else {
                    $this->db->where('rt.task_stage', 0);
                }
            }
        }

        $src_columns = array('rt.task_name', 'spell_condition');


        if (!$its_recruiterment_admin) {
            $this->db->where('if(rt.task_stage IN (3,6), true, rtra.recruiterId = "' . $adminId . '")');
            // $this->db->where('if(rt.task_stage IN (3,7), true, rt.created_by = "' . $adminId . '")');;
        }

        if (!empty($filter->search)) {
            $this->db->group_start();
            $search_value = addslashes($filter->search);

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $search_value);
                } else if ($column_search == 'spell_condition') {

                    $this->db->or_where("case when rtra.primary_recruiter>0 then rt.id in (select sub_rtra.taskId from tbl_recruitment_task_recruiter_assign as sub_rtra 
                    inner join tbl_member as sub_a on sub_a.id = sub_rtra.recruiterId where sub_rtra.taskId = rt.id AND sub_rtra.primary_recruiter = 1 AND sub_rtra.archive=0 and `sub_a`.`firstname` LIKE '%" . $search_value . "%' ESCAPE '!' OR `sub_a`.`lastname` LIKE '%" . $search_value . "%' ESCAPE '!' ) ELSE '' END", null, false);
                } else {
                    $this->db->or_like($column_search, $search_value);
                }
            }
            $this->db->group_end();
        }



        $colowmn = array('rt.id', 'rt.task_name', 'rt.task_stage', 'rt.start_datetime', 'rt.status', "concat(rt.task_stage,' - ',rts.name) as stage", "rt.max_applicant");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->select("CASE
            WHEN rt.task_stage='3' OR rt.task_stage='6' THEN (SELECT COUNT(IF(status= 0, 1, NULL)) 'pending' FROM tbl_recruitment_task_applicant where taskId = rt.id AND archive = 0)
            ELSE NULL
            END as pending");
        $this->db->select("CASE
            WHEN rt.task_stage='3' OR rt.task_stage='6' THEN (SELECT COUNT(IF(status= 1, 1, NULL)) 'accepted' FROM tbl_recruitment_task_applicant where taskId = rt.id AND archive = 0)
            ELSE NULL
            END as accepted");


        $this->db->select("CASE WHEN rtra.primary_recruiter>0 THEN (select concat_ws(' ',sub_a.firstname, sub_a.lastname) 
        from tbl_recruitment_task_recruiter_assign as sub_rtra
        inner join tbl_member as sub_a on sub_a.id = sub_rtra.recruiterId 
        where sub_rtra.taskId = rt.id AND sub_rtra.primary_recruiter = 1 and sub_rtra.archive=0) else '' end as primary_recruiter");


        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_stage as rts', 'rts.id = rt.task_stage', 'inner');
        $this->db->join('(SELECT sub_rtra.* FROM  `tbl_recruitment_task_recruiter_assign` as sub_rtra  where `sub_rtra`.`archive`=0 ' . $extraCondSub . ') as rtra', 'rtra.taskId = rt.id', 'left');


        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->group_by('rt.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        //last_query(1);

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->slot_applicable = false;

                if ($val->task_stage == 3 || $val->task_stage == 6) {
                    $val->filled = $val->pending + $val->accepted;
                    $val->slot_applicable = true;
                }
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'status' => true);

        return $return;
    }

    function get_recruitment_task_list_calendar($reqData, $adminId) {
        $its_recruiter_admin = check_its_recruiter_admin($adminId);
        $str = array('1', '2');

        if (!$its_recruiter_admin) {
            $this->db->join('tbl_recruitment_task_recruiter_assign as rtra', "rtra.taskId = rt.id AND primary_recruiter = 1 AND recruiterId = " . $adminId, "INNER");
        }

        $colowmn = array('date(rt.start_datetime) as start_date', 'date(rt.end_datetime) as end_date', 'COUNT(IF( date(rt.start_datetime) >= date(NOW()) && rt.status= 1, 1, NULL)) as dueCount', '
            COUNT(IF(date(rt.start_datetime) <= date(NOW()) && rt.status= 2, 1, NULL)) as CompleteCount', 'rt.task_name', 'rt.status');
        $this->db->select($colowmn);
        $this->db->from('tbl_recruitment_task as rt');

        $this->db->where_in('rt.status', $str);
        $this->db->where("DATE_FORMAT(rt.start_datetime,'%m')", DateFormate($reqData->date, 'm'));
        $this->db->group_by('date(rt.start_datetime)');
        $query = $this->db->get();
//        last_query();
        $rows = $query->result();

        return ['status' => true, 'data' => $rows];
    }

    function get_task_details($reqData, $adminId) {
        $this->db->select(['rt.task_name', 'rt.start_datetime', 'rt.end_datetime', 'rl.name as training_location', 'rt.relevant_task_note', 'rt.max_applicant', 'rts.name as stage', 'rt.status', 'rt.task_stage']);
        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_stage as rts', 'rts.id = rt.task_stage', 'inner');
        $this->db->join('tbl_recruitment_location as rl', 'rl.id = rt.training_location', 'inner');
        $this->db->where('rt.id', $reqData->taskId);

        $query = $this->db->get();
        $res = $query->row();

        if (!empty($res)) {
            $res->edit_mode = true;
            if (in_array($res->status, [2, 3, 4]) || (strtotime(DATE_TIME) > strtotime($res->start_datetime))) {
                $res->edit_mode = false;
            }
            $res->assigned_user = $this->get_attached_recruit_with_task($reqData->taskId, $adminId);
            $res->applicant_list = $this->get_attached_applicant_with_task($reqData->taskId);
        }
        return $res;
    }

    function get_attached_recruit_with_task($taskId, $adminId) {
        $this->db->select(['rtra.id', 'rtra.recruiterId as value', 'rtra.primary_recruiter', 'concat(a.firstname," ",a.lastname) as label']);
        $this->db->from('tbl_recruitment_task_recruiter_assign as  rtra');
        $this->db->join('tbl_member as  a', 'a.id = rtra.recruiterId', 'INNER');
        $this->db->join('tbl_department as d', 'd.id = a.department AND d.short_code = "internal_staff"', 'inner');
        $this->db->where('rtra.taskId', $taskId);
        $this->db->where('rtra.archive', 0);

        $query = $this->db->get();
        $result = $query->result();

        $its_recruiter_admin = check_its_recruiter_admin($adminId);

        if (!empty($result) && !$its_recruiter_admin) {
            foreach ($result as $val) {
                if ($val->primary_recruiter == 1) {
                    $val->non_removal_primary = true;
                }
            }
        }
        return $query->result();
    }

    function get_attached_applicant_with_task($taskId) {
        $this->db->select(['rta.id', 'rta.status', 'rta.applicant_id as value', 'concat(ra.firstname," ",ra.lastname) as label', 'rta.applicant_id', 'ra_email.email', 'ra_phone.phone']);
        $this->db->from('tbl_recruitment_task_applicant as  rta');
        $this->db->join('tbl_recruitment_applicant as  ra', 'ra.id = rta.applicant_id', 'INNER');
        $this->db->join('tbl_recruitment_applicant_email as  ra_email', 'ra.id = ra_email.applicant_id AND primary_email = 1', 'INNER');
        $this->db->join('tbl_recruitment_applicant_phone as  ra_phone', 'ra.id = ra_email.applicant_id  AND primary_phone = 1', 'INNER');
        $this->db->where('rta.taskId', $taskId);
        $this->db->where('rta.archive', 0);

        $this->db->group_by('ra.id');
        $query = $this->db->get();
        return $query->result();
    }

    function verify_edit_task_applicant($old_applicant, $reqData) {
        $this->db->select(['rta.id', 'rta.applicant_id']);
        $this->db->from('tbl_recruitment_task_applicant as  rta');
        $this->db->join('tbl_recruitment_applicant as  ra', 'ra.id = rta.applicant_id', 'INNER');

        $this->db->where('rta.taskId', $reqData->taskId);
        $this->db->where_in('rta.applicant_id', $old_applicant);

        $this->db->group_by('ra.id');
        $query = $this->db->get();
        return $query->result();
    }

    function update_task($reqData, $adminId) {

        $this->add_update_task_applicant($reqData->applicant_list, $reqData->taskId, $reqData->task_stage);

        $this->add_update_task_recruiter($reqData->assigned_user, $reqData->taskId);
    }

    function add_update_task_applicant($applicant_list, $taskId, $task_stage) {
        require_once APPPATH . 'Classes/recruitment/ApplicantCommunicationLog.php';

        if (!empty($applicant_list)) {

            $attach_applicant = [];
            $email_log_data = [];

            $applicantIds_for_details = array_column(obj_to_arr($applicant_list), 'value');
            $applicantDetails = $this->get_applicant_name_appid_email($applicantIds_for_details);

            $task_date = '';
            if ($task_stage == 3 || $task_stage == 6) {
                $res = $this->basic_model->get_row('recruitment_task', ['start_datetime'], ['id' => $taskId]);
                $task_date = $res->start_datetime;
            }

            foreach ($applicant_list as $val) {
                if (!empty($val->id) && !empty($val->removed)) {
                    $this->basic_model->update_records('recruitment_task_applicant', ['archive' => 1], ['id' => $val->id]);
                } elseif (empty($val->id)) {

                    $status = 0;
                    $token = '';

                    // only send mail for group interview and cab day interview
                    if ($task_stage == 3 || $task_stage == 6) {
                        $status = 1;
                        $device_pin = random_genrate_password(8);

                        $applicantIds_for_create_interview_details[] = $val->value;
                        $applicantDetails[$val->value]['device_pin'] = $device_pin;

                        $mail_details = $applicantDetails[$val->value];
                        $mail_details['task_date_time'] = $task_date;

                        $token = $this->send_task_mail_to_applicant($mail_details);

                        $logObj = new ApplicantCommunicationLog();
                        $log_title = ($task_stage == 3) ? $logObj->getLogTitle('group_interview_invitation') : $logObj->getLogTitle('cab_day_interview_invitation');

                        // only for log purpose
                        $email_log_data[] = [
                            'communication_text' => json_encode(['data' => $mail_details, 'mail_template_version' => 1]),
                            'applicant_id' => $val->value,
                            'log_type' => 2,
                            'created' => DATE_TIME,
                            'title' => $log_title
                        ];
                    }


                    $attach_applicant[] = array(
                        'taskId' => $taskId,
                        'applicant_id' => $val->value,
                        'applicant_message' => '',
                        'email_status' => $status,
                        'status' => 0,
                        'token_email' => $token,
                        'invitation_send_at' => DATE_TIME,
                        'created' => DATE_TIME
                    );
                }
            }


            if (!empty($attach_applicant)) {
                $this->basic_model->insert_records('recruitment_task_applicant', $attach_applicant, true);

                if ($task_stage == 3 || $task_stage == 6) {
                    // get all the recruitment task applicant id and applicant for create interview details
                    $recruitment_task_applicant_ids = $this->get_recruitment_task_applicant_ids($taskId, $applicantIds_for_create_interview_details);

                    // create interview details
                    $this->create_interview_details_for_applicant($recruitment_task_applicant_ids, $applicantDetails, $task_stage);

                    $logObj = new ApplicantCommunicationLog();
                    // insert mutiple log data
                    $logObj->createMuitipleCommunicationLog($email_log_data);
                }
            }
        }
    }

    function create_interview_details_for_applicant($recruitment_task_applicant_ids, $applicant_details, $task_stage) {
        $interview_type = $task_stage == 3 ? 1 : 2;

        if (!empty($recruitment_task_applicant_ids)) {
            foreach ($recruitment_task_applicant_ids as $val) {
                $interview_details[] = array(
                    'deviceId' => 0,
                    'device_pin' => $applicant_details[$val->applicant_id]['device_pin'],
                    'interview_type' => $interview_type,
                    'recruitment_task_applicant_id' => $val->recruitment_task_applicant_id,
                    'quiz_status' => 0,
                    'applicant_status' => 0,
                    'applicant_status' => 0,
                    'contract_status' => 0,
                    'archive' => 0,
                    'created' => DATE_TIME,
                    'updated' => DATE_TIME,
                );
            }
        }

        if (!empty($interview_details)) {
            $this->basic_model->insert_records('recruitment_applicant_group_or_cab_interview_detail', $interview_details, $multiple = TRUE);
        }
    }

    function get_recruitment_task_applicant_ids($taskId, $applicantIds) {
        $this->db->select(['id as recruitment_task_applicant_id', 'applicant_id']);
        $this->db->from('tbl_recruitment_task_applicant');
        $this->db->where_in('applicant_id', $applicantIds);
        $this->db->where('taskId', $taskId);

        $query = $this->db->get();
        return $response = $query->result();
    }

    function send_task_mail_to_applicant($appcnt_data) {
        $token = encrypt_decrypt('encrypt', rand(1111, 9999999));
        $appcnt_data['url'] = $this->config->item('server_url') . 'task_confirmation/' . encrypt_decrypt('encrypt', json_encode(['date_time' => DATE_TIME, 'token' => $token]));

        $status = send_invitation_mail_to_applicant_for_task($appcnt_data);

        return $token;
    }

    function add_update_task_recruiter($assign_recruiter, $taskId) {
        if (!empty($assign_recruiter)) {
            $pre_task_recruiter = $this->basic_model->get_record_where('recruitment_task_recruiter_assign', ['id'], $where = ['taskId' => $taskId]);
            $pre_task_recruiter = array_column(obj_to_arr($pre_task_recruiter), 'id');

            $assign_user = array();
            foreach ($assign_recruiter as $val) {
                $prim_sec = (!empty($val->primary_recruiter) && $val->primary_recruiter === '1') ? 1 : 2;

                if (!empty($val->id)) {
                    $key = array_search($val->id, $pre_task_recruiter);

                    $this->basic_model->update_records('recruitment_task_recruiter_assign', ['primary_recruiter' => $prim_sec], ['id' => $val->id]);
                    unset($pre_task_recruiter[$key]);
                } else {

                    $assign_user[] = array(
                        'recruiterId' => $val->value,
                        'taskId' => $taskId,
                        'primary_recruiter ' => $prim_sec,
                        'created' => DATE_TIME,
                        'archive' => 0,
                    );
                }
            }

            if (!empty($assign_user)) {
                $this->basic_model->insert_records('recruitment_task_recruiter_assign', $assign_user, true);
            }

            if (!empty($pre_task_recruiter)) {
                foreach ($pre_task_recruiter as $id) {
                    $this->basic_model->update_records('recruitment_task_recruiter_assign', ['archive' => 1], ['id' => $id]);
                }
            }
        }
    }

    function verify_task_confirmation_token($reqData) {
        $decrypt_data = encrypt_decrypt('decrypt', $reqData->token);
        $return = [];

        if (is_json($decrypt_data)) {
            $x = json_decode($decrypt_data);

            $where_ch = array('token_email' => $x->token, 'archive' => 0);
            $this->db->select(['rta.id', 'taskId', 'applicant_id', 'rta.status', 'rt.start_datetime']);
            $this->db->from('tbl_recruitment_task as rt');
            $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.taskId = rt.id AND rta.status = 0');
            $this->db->where(['rta.token_email' => $x->token, 'rta.archive' => 0]);
            $this->db->where('rt.status', 1);

            $query = $this->db->get();
            $response = $query->row();

            if (!empty($response)) {

                if ($response->status == 0 && strtotime($response->start_datetime) > strtotime(DATE_TIME)) {

                    if ($reqData->action === 'a') {
                        // first mark as token invitation acceprt and blank the token email field
                        $this->basic_model->update_records('recruitment_task_applicant', ['status' => 1, 'token_email' => '', 'invitation_accepted_at' => DATE_TIME], $where_ch);
                    } elseif ($reqData->action === 'c') {

                        $this->basic_model->update_records('recruitment_task_applicant', ['status' => 2, 'token_email' => '', 'invitation_cancel_at' => DATE_TIME], $where_ch);
                    }

                    $return = array('status' => true);
                } else {
                    $return = array('status' => false, 'error' => 'Link has expired');
                }
            } else {
                $return = array('status' => false, 'error' => 'Invalid Request');
            }
        } else {
            $return = array('status' => false, 'error' => 'Invalid Request');
        }

        return $return;
    }

    function get_applicant_name_appid_email($applicantIds) {
        $this->db->select(["CONCAT_WS(' ',firstname,lastname) as fullname", "ra.appId", "rae.email", "ra.id"]);
        $this->db->from("tbl_recruitment_applicant as ra");
        $this->db->join("tbl_recruitment_applicant_email as rae", "rae.applicant_id = ra.id AND rae.archive = 0 AND rae.primary_email = 1", "INNER");
        $this->db->where_in("ra.id", $applicantIds);

        $query = $this->db->get();
        $result = $query->result_array();

        return pos_index_change_array_data($result, 'id');
    }

    function resend_task_mail_to_applicant($reqData) {
        require_once APPPATH . 'Classes/recruitment/ApplicantCommunicationLog.php';

        $this->db->select(["CONCAT_WS(' ',firstname,lastname) as fullname", "ra.appId", "rae.email", "rt.start_datetime as task_date_time", "interview_det.device_pin", "rt.task_stage"]);
        $this->db->from("tbl_recruitment_task as rt");

        $this->db->join("tbl_recruitment_task_applicant as rta", "rta.taskId = rt.id AND rta.archive = 0", "INNER");
        $this->db->join("tbl_recruitment_applicant_group_or_cab_interview_detail as interview_det", "interview_det.recruitment_task_applicant_id = rta.id AND interview_det.archive = 0", "INNER");
        $this->db->join("tbl_recruitment_applicant as ra", "ra.id = rta.applicant_id AND rta.archive = 0", "INNER");
        $this->db->join("tbl_recruitment_applicant_email as rae", "rae.applicant_id = ra.id AND rae.archive = 0 AND rae.primary_email = 1", "INNER");
        $this->db->where("rta.applicant_id", $reqData->applicant_id);
        $this->db->where("rt.id", $reqData->taskId);
        $this->db->where("rt.status", 1);


        $query = $this->db->get();
        $result = $query->row_array();

        $token = $this->send_task_mail_to_applicant($result);

        // create communication log
        $logObj = new ApplicantCommunicationLog();
        $log_title = ($result['task_stage'] == 3) ? $logObj->getLogTitle('group_interview_invitation') : $logObj->getLogTitle('cab_day_interview_invitation');
        $logObj->setTitle($log_title);
        $logObj->setCommunication_text(json_encode(['data' => $result, 'mail_template_version' => $logObj->getMail_template_version()]));
        $logObj->setApplicant_id($reqData->applicant_id);
        $logObj->setLog_type(2);

        $logObj->createCommunicationLog();



        $where_u = array('taskId' => $reqData->taskId, 'applicant_id' => $reqData->applicant_id);
        $this->basic_model->update_records('recruitment_task_applicant', ['token_email' => $token], $where_u);

        return true;
    }

    function get_available_group_or_cab_interview_for_applicant($task_stage, $extra_param = []) {
        $colowmn = array('rt.id as taskId', 'rt.task_name', 'rl.name as training_location', 'rt.start_datetime', 'rt.status', "rt.max_applicant");

        $this->db->select($colowmn);
        $this->db->select("(select concat_ws(' ',m.firstname, lastname) from tbl_recruitment_task_recruiter_assign as rtra
                INNER JOIN tbl_member as m on m.id = rtra.recruiterId 
                INNER JOIN tbl_recruitment_staff as staff on m.id = staff.adminId AND staff.status = 1 
                where rtra.taskId = rt.id AND rtra.archive = 0 and primary_recruiter = 1 ) as primary_recruiter", false);

        $this->db->select("(SELECT COUNT(IF(status = 0, 1, NULL)) 'pending' FROM tbl_recruitment_task_applicant where taskId = rt.id AND archive = 0) as pending");
        $this->db->select("(SELECT COUNT(IF(status = 1, 1, NULL)) 'accepted' FROM tbl_recruitment_task_applicant where taskId = rt.id AND archive = 0) as accepted");
        $this->db->select("(SELECT (rt.max_applicant - COUNT(id)) 'accepted' FROM tbl_recruitment_task_applicant where taskId = rt.id AND archive = 0 AND status IN (0,1)) as available");

        $this->db->from('tbl_recruitment_task as rt');
        $this->db->from('tbl_recruitment_location as rl', 'rl.id = rt.training_location', 'INNER');
        $this->db->where('rt.task_stage', $task_stage);
        $this->db->where('rt.status', 1);
        $this->db->where("rt.start_datetime >=", DATE_TIME);

        if (!empty($extra_param['applicant_id'])) {
            $this->db->where('NOT EXISTS (SELECT sb_rta.taskId FROM tbl_recruitment_task_applicant as sb_rta WHERE sb_rta.taskId = rt.id AND sb_rta.archive = 0 AND sb_rta.applicant_id = "' . $extra_param['applicant_id'] . '")');
        }

        if (!empty($extra_param['taskId'])) {
            $this->db->where('rt.id', $extra_param['taskId']);
        }

        $this->db->order_by('rt.start_datetime', 'asc');
        $this->db->limit(10);
        $this->db->having('available > 0');
        $this->db->group_by('rt.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
//        last_query();
        return $result;
    }

    function check_applicant_already_exist_in_interview($applicant_id, $task_stage) {
        $this->db->select(["rt.id", "rta.status as approve_deny_status", "interview_det.applicant_status as task_applicant_status", "interview_det.quiz_status as quiz_status", "interview_det.app_orientation_status", "interview_det.app_login_status"]);
        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.taskId = rt.id AND rta.archive = 0 AND rta.status IN (0,1) AND rta.applicant_id = ' . $applicant_id, 'inner');
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as interview_det', 'rta.id = interview_det.recruitment_task_applicant_id AND interview_det.archive = 0 AND interview_det.mark_as_no_show = 0', 'inner');

        $this->db->where('rt.task_stage', $task_stage);
        $this->db->where_in('rt.status', [1, 2]);

        $this->db->group_by('rt.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row();
   
        return $result;
    }

    function check_applicant_exist_in_any_inprogress_task() {
        
    }

    function check_applicant_docusign_signed($applicant_id, $task_stage) {
        $this->db->select(["rt.id", "rac.signed_status"]);
        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.taskId = rt.id AND rta.archive = 0 AND rta.status IN (0,1) AND rta.applicant_id = ' . $applicant_id, 'inner');
        $this->db->join('tbl_recruitment_applicant_contract as rac', 'rac.task_applicant_id = rta.id AND rac.archive = 0', 'inner');

        $this->db->where('rt.task_stage', $task_stage);
        $this->db->where('rt.status', 1);

        $this->db->group_by('rt.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row();

        return $result;
    }

    function add_applicant_in_available_interview($reqData) {
        // this function handle multiple applicant so we are send applicant id as multiple
        $applicant_list = [];

        $applicant_list[] = (object) ['value' => $reqData->applicant_id];
        $this->add_update_task_applicant($applicant_list, $reqData->taskId, $reqData->task_stage);

        return true;
    }

    function check_task_eligibility_for_archive($taskId, $adminId) {
        $its_recruiter_admin = check_its_recruiter_admin($adminId);

        $this->db->select(['rt.created_by', 'rt.start_datetime', 'rt.commit_status', 'rts.key']);
        $this->db->from('tbl_recruitment_task as rt');
        $this->db->join('tbl_recruitment_task_stage as rts', 'rts.id = rt.task_stage AND rts.archive = 0', 'inner');

        $this->db->where('rt.id', $taskId);
        $this->db->where('rt.status', 1);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $response = $query->row();

        
        if (!empty($response)) {
            if (!$its_recruiter_admin && $response->created_by != $adminId) {

                $return = ['status' => false, 'error' => 'Sorry you do not have permission to complete this task.'];
            } elseif (strtotime($response->start_datetime) < strtotime(DATE_TIME) && ($response->key == 'group_interview' || $response->key == 'cab_day')) {

                $return = ['status' => false, 'error' => 'Task has started, It can not be archived.'];
            } else {
                $return = ['status' => true];
            }
        } else {
            $return = ['status' => false, 'error' => 'Task not found as in-progress'];
        }

        return $return;
    }

    public function check_applicant_task_allocated_check_assign_requiter($adminId=0,$taskIdOrCabdayDetailId=0,$type='taskId'){
        $this->db->select('1',false);
        $this->db->from('tbl_recruitment_applicant_group_or_cab_interview_detail sub_ragocid');
        $this->db->join('tbl_recruitment_task_applicant sub_rta',"sub_rta.id=sub_ragocid.recruitment_task_applicant_id AND sub_ragocid.archive=sub_rta.archive ","inner");
        $this->db->join('tbl_recruitment_task_recruiter_assign sub_rtra',"sub_rtra.taskId=sub_rta.taskId AND sub_rtra.archive=sub_ragocid.archive","inner");
        $this->db->join('tbl_recruitment_staff sub_rs',"sub_rs.adminId=sub_rtra.recruiterId and sub_rs.status=1 AND sub_rs.archive=sub_ragocid.archive","inner");
        if($type=='taskId'){
            $this->db->where("sub_rta.taskId",$taskIdOrCabdayDetailId);
            $this->db->group_by("sub_rta.taskId");
        }elseif($type=='detailId'){
            $this->db->where("sub_ragocid.id",$taskIdOrCabdayDetailId);
            $this->db->group_by("sub_ragocid.id");
        }elseif($type=='taskApplicantId'){
            $this->db->where("sub_rta.id",$taskIdOrCabdayDetailId);
            $this->db->group_by("sub_rta.id");
        }
        $this->db->limit(1);
       
        $this->db->where("sub_rtra.recruiterId =rs.adminId and sub_ragocid.archive=rs.archive",null,false);
        $sub_query = $this->db->get_compiled_select();
        $this->db->select([
            "CASE WHEN its_recruitment_admin=1 THEN '1' WHEN its_recruitment_admin=0 THEN COALESCE( (".$sub_query."),0) ELSE 0 END as permission_status"
        ]);
        $this->db->from("tbl_recruitment_staff rs");
        $this->db->where("rs.adminId",$adminId);
        $this->db->where("rs.archive",0);
        $query = $this->db->get();
        $status = $query->num_rows()>0 ? $query->row()->permission_status : 0;
        return $status;
    }

}
