<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_group_interview_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_group_interview_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_rec_task = TBL_PREFIX . 'recruitment_task';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_rec_task . '.id';
            $direction = 'DESC';
        }

        if (isset($filter->filter_by) && $filter->filter_by != '') {
            if($filter->filter_by == 1){
                $this->db->where($tbl_rec_task . ".commit_status", $filter->filter_by);
            }
            elseif($filter->filter_by == 2){
                $this->db->where($tbl_rec_task . ".start_datetime>" ,   "NOW()",false );
            }
            elseif($filter->filter_by == 3){
                $this->db->where($tbl_rec_task . ".commit_status!=",1 );
                $this->db->where($tbl_rec_task . ".start_datetime<" ,   "NOW()",false );
            }
        }

        if (!empty($filter->srch_box)) {
            $this->db->group_start();
            $src_columns = array($tbl_rec_task . ".id", $tbl_rec_task . ".task_name");

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $filter->srch_box);
                } else {
                    $this->db->or_like($column_search, $filter->srch_box);
                }
            }
            $this->db->group_end();
        }


        $select_column = array($tbl_rec_task . ".id", $tbl_rec_task . ".task_name", $tbl_rec_task . ".start_datetime", $tbl_rec_task . ".end_datetime", $tbl_rec_task . ".commit_status", "rl.name as training_location", "(select count('id') from tbl_recruitment_task_applicant as rta Inner join tbl_recruitment_applicant as ra on ra.id = rta.applicant_id where rta.taskId = tbl_recruitment_task.id AND rta.status = 1 AND rta.archive=0  ) as applicant_cnt");

        #AND ra.status=1
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_rec_task);
        $this->db->join('tbl_recruitment_location as rl', 'rl.id =  ' . $tbl_rec_task . '.training_location', 'INNER');

        $this->db->where($tbl_rec_task . ".task_stage=", '3');
        $this->db->where_in($tbl_rec_task . ".status", array(1, 2));
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        $data = [];
        #pr($dataResult);
        if (!empty($dataResult)) {
            $task_ids = array_column(obj_to_arr($dataResult), 'id');
            $task_ids = empty($task_ids) ? [0] : $task_ids;
            $applicant_data = $this->get_interview_applicant_list($task_ids);

            foreach ($dataResult as $val) {
                $val->datetime_end = $val->end_datetime;
                $val->datetime_start = date('d/m/Y', strtotime($val->start_datetime));
                $current_date = DATE_TIME;
                $status = 2; #'In-Progress';
                if (strtotime($current_date) < strtotime($val->start_datetime))
                    $status = 1;#'Pending';

                if ($val->commit_status == 1)
                    $status = 3;#'Completed';

                $val->task_status = $status;
                $val->start_datetime = date('H:i', strtotime($val->start_datetime));
                $val->end_datetime = date('H:i', strtotime($val->end_datetime));

                $val->aplicantDetail = isset($applicant_data[$val->id]) ? $applicant_data[$val->id] : [];
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    #$interViewId/groupInterViewId/taskId tbl_recruitment_applicant_group_or_cab_interview_detail
    public function get_interview_applicant_list($interViewId) {
        $this->db->select(['rta.taskId', 'concat(ra.firstname," ",ra.middlename," ",ra.lastname) as applicant_name', 'rjp.title as job_position', 'rjt.title as recruitment_area', 'rapic.classfication', "DATE_FORMAT(ra.dob,'%d/%m/%Y') as dob", 'ragid.applicant_status', 'ragid.device_pin', 'ra.flagged_status', 'ra.id as applicant_id', 'ragid.quiz_status', 'ragid.contract_status','ragid.app_login_status', 'rta.id as recruitment_task_applicant_id', 'rd.device_number as device_number_main', 'ragid.mark_as_no_show', 'count(raqfa.is_answer_correct) as total_question', 'COUNT(IF(raqfa.is_answer_correct=1,1, NULL)) correct_count', 'COUNT(IF(raqfa.is_answer_correct=0,1, NULL)) not_checked_count', 'ragid.deviceId as device_id','ragid.allot_question', "COALESCE( (select rae.email from tbl_recruitment_applicant_email as rae where rae.applicant_id = ra.id and rae.primary_email = 1 AND rae.archive = 0),'N/A') as applicant_primary_email"]);

        $this->db->from('tbl_recruitment_task_applicant as rta');
        $this->db->join('tbl_recruitment_applicant as ra', 'ra.id = rta.applicant_id', 'inner');
        $this->db->join('tbl_recruitment_job as rj', 'rj.id = ra.jobId', 'inner');
        $this->db->join('tbl_recruitment_job_position as rjp', 'rjp.id = rj.position', 'inner');
        $this->db->join('tbl_recruitment_applicant_phone_interview_classification as rapic', 'rapic.applicant_id = rta.applicant_id AND rapic.archive = 0', 'inner');
        $this->db->join('tbl_recruitment_job_type as rjt', 'rjt.id = rj.type', 'inner');
        $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as ragid', 'ragid.recruitment_task_applicant_id = rta.id', 'inner');
        $this->db->join('tbl_recruitment_interview_type as rit', 'rit.id = ragid.interview_type AND rit.key_type="group_interview" AND rit.archive=0', 'inner');
        $this->db->join('tbl_recruitment_device as rd', 'rd.id = ragid.deviceId', 'left');
        $this->db->join('tbl_recruitment_additional_questions_for_applicant as raqfa', 'raqfa.recruitment_task_applicant_id = ragid.recruitment_task_applicant_id', 'left');
        $this->db->where_in('rta.taskId', $interViewId);
        $this->db->where('rta.status', 1);
        $this->db->where('rta.archive', 0);
        $this->db->group_by('rta.id');
        $query = $this->db->get();
        #last_query();
        $res = $query->result_array();
        $complete_ary = [];
        if (!empty($res)) {
            $temp_hold = [];
            foreach ($res as $key => $value) {
                $task_id = $value['taskId'];
                if (!in_array($task_id, $temp_hold)) {
                    $temp_hold[] = $task_id;
                }
                $temp['taskId'] = $task_id;
                $temp['applicant_name'] = $value['applicant_name'];
                $temp['job_position'] = $value['job_position'];
                $temp['recruitment_area'] = $value['recruitment_area'];
                $temp['classfication'] = $value['classfication'];
                $temp['dob'] = $value['dob'];
                $temp['applicant_status'] = $value['applicant_status'];
                $temp['flagged_status'] = $value['flagged_status'];
                $temp['applicant_id'] = $value['applicant_id'];
                $temp['quiz_status'] = $value['quiz_status'];
                $temp['contract_status'] = $value['contract_status'];
                $temp['recruitment_task_applicant_id'] = $value['recruitment_task_applicant_id'];
                $temp['device_number_main'] = $value['device_number_main'];
                $temp['mark_as_no_show'] = $value['mark_as_no_show'];
                $temp['total_question'] = $value['total_question'];
                $temp['correct_count'] = $value['correct_count'];
                $temp['not_checked_count'] = $value['not_checked_count'];
                $temp['device_id'] = $value['device_id'];
                $temp['device_pin'] = $value['device_pin'];
                $temp['app_login_status'] = $value['app_login_status'];
                $temp['applicant_primary_email'] = $value['applicant_primary_email'];
                $temp['allot_question'] = $value['allot_question'];
                $complete_ary[$task_id][] = $temp;
            }
        }
        return $complete_ary;
    }

    public function get_applicant_selected_question($reqData) {
        $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
        $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';

        $select_column = array('raq.question', 'qt.topic', 'raq.id', '"1" as selected');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_recruitment_applicant_not_assign_question as ragcq');
        /* $this->db->where("ragcq.applicant_id=", $applicant_id);
        $this->db->where("ragcq.task_id=", $taskId); */
        $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.id = ragcq.recruitment_task_applicant_id AND rta.taskId=' . $taskId . ' AND rta.applicant_id=' . $applicant_id, 'inner');
        $this->db->join('tbl_recruitment_interview_type as rit', 'rit.id = ragcq.interview_type AND rit.key_type="group_interview" AND rit.archive=0', 'inner');
        $this->db->where("ragcq.archive=", 0);
        $this->db->join('tbl_recruitment_additional_questions as raq', 'raq.id = ragcq.question_id AND raq.archive = 0 AND raq.status = 1 AND raq.training_category = 1', 'inner');
        $this->db->join('tbl_recruitment_question_topic as qt', 'qt.id = raq.question_topic', 'inner');
        $query_app = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $applicant_ques = $query_app->result();
        return $applicant_ques;
    }

    public function get_applicant_question($reqData) {
        $questionTopic = isset($reqData->data->defaultFiltered[0]->questionTopic) ? $reqData->data->defaultFiltered[0]->questionTopic : '';
        $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
        $task_id = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';

        if ($task_id != '' && $applicant_id != '') {
            $this->load->library('userName');
            $applicant_name = $this->username->getName('applicant', $applicant_id);

            $tbl_additional_question = TBL_PREFIX . 'recruitment_additional_questions';
            $select_column = array($tbl_additional_question . ".id", $tbl_additional_question . ".question", 'qt.topic');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from($tbl_additional_question);
            $this->db->where($tbl_additional_question . ".status=", '1');
            $this->db->where($tbl_additional_question . ".archive=", '0');
            $this->db->where($tbl_additional_question . ".training_category=", '1');
            if (!empty($questionTopic))
                $this->db->where_in($tbl_additional_question . ".question_topic", $questionTopic);

            $this->db->join('tbl_recruitment_question_topic as qt', 'qt.id = ' . $tbl_additional_question . '.question_topic', 'inner');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $all_question = $query->result();

            $select_column = array('ragcq.question_id');
            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from('tbl_recruitment_applicant_not_assign_question as ragcq');
            $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.id = ragcq.recruitment_task_applicant_id AND rta.taskId=' . $task_id . ' AND rta.applicant_id=' . $applicant_id, 'inner');
            $this->db->join('tbl_recruitment_interview_type as rit', 'rit.id = ragcq.interview_type AND rit.key_type="group_interview" AND rit.archive=0', 'inner');
            $this->db->where("ragcq.archive=", 0);
            #$this->db->where("ragcq.interview_type=", 1);
            $query_app = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            //last_query();
            $applicant_ques = $query_app->result_array();

            if (!empty($applicant_ques)) {
                $applicant_ques = pos_index_change_array_data($applicant_ques, 'question_id');
            }

            if (!empty($all_question)) {
                foreach ($all_question as $val) {
                    if (array_key_exists($val->id, $applicant_ques)) {
                        $val->selected = true;
                    } else {
                        $val->selected = false;
                    }
                }
            }

            
            return array('status' => true, 'data' => $all_question, 'applicant_name' => $applicant_name);
        } else {
            return array('status' => false);
        }
    }

    /* Additional question */

    public function commit_n_save_ques($reqData) {

        $tbl_additional_question = TBL_PREFIX . 'recruitment_additional_questions';
        $select_column = array($tbl_additional_question . ".id", $tbl_additional_question . ".question", 'qt.topic');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_additional_question);
        $this->db->where($tbl_additional_question . ".status=", '1');
        $this->db->where($tbl_additional_question . ".archive=", '0');
        $this->db->where($tbl_additional_question . ".training_category=", '1');
        if (!empty($questionTopic))
            $this->db->where_in($tbl_additional_question . ".question_topic", $questionTopic);

        $this->db->join('tbl_recruitment_question_topic as qt', 'qt.id = ' . $tbl_additional_question . '.question_topic', 'inner');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $all_question = $query->result();
        $all_question_count =  !empty($all_question)?count($all_question):0;
        #pr($reqData);
        if (!empty($reqData->data)) {
            $applicantId = $reqData->data->applicantId;
            $taskId = $reqData->data->taskId;

            $selected_que_obj = isset($reqData->data->selected_que) && !empty($reqData->data->selected_que) ? $reqData->data->selected_que : [];
            $ids_ary = [];
            $i = 0;
            if (!empty($selected_que_obj)) {
                foreach ($selected_que_obj as $key => $value) {
                    if (isset($value->selected)) {
                        $ids_ary[] = $value->id;
                        $i++;
                    }
                }
            } else {
                return array('status' => false, 'msg' => 'Please select atleast one question for Add/Remove.');
            }

            if($all_question_count == $i)
                return array('status' => false, 'msg' => 'You can\'t remove all Questions.');

            if (!empty($ids_ary)) {
                $insert_ary = [];

                $data = $this->load->Basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'), $id_array = array('taskId' => $taskId, 'applicant_id' => $applicantId));
                $recruitment_task_applicant_id = !empty($data) ? $data->id : '';

                foreach ($ids_ary as $id) {
                    $temp_ary['question_id'] = $id;
                    $temp_ary['created'] = DATE_TIME;
                    $temp_ary['interview_type'] = 1;
                    $temp_ary['recruitment_task_applicant_id'] = $recruitment_task_applicant_id;
                    $insert_ary[] = $temp_ary;
                }

                if (!empty($insert_ary)) {
                    $this->Basic_model->update_records('recruitment_applicant_not_assign_question', array('archive' => 1), array('recruitment_task_applicant_id' => $recruitment_task_applicant_id));
                    $this->Basic_model->insert_records('recruitment_applicant_not_assign_question', $insert_ary, true);
                    return array('status' => true, 'msg' => 'Question list is successfully updated for Recruiter.');
                }
            } else {
                return array('status' => false, 'msg' => 'Please select atleast one question for Add/Remove.');
            }
        }
    }

    /* Applicant and group interview detail */

    public function get_applicant_detail($reqData) {
        $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
        $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';
        $type = isset($reqData->data->type) && $reqData->data->type != '' ? $reqData->data->type : '';
        if ($applicant_id != '' && $taskId != '') {
            $tbl_applicant = TBL_PREFIX . 'recruitment_applicant';
            $select_column = array($tbl_applicant . ".id", $tbl_applicant . ".appId", 'concat(firstname," ",middlename," ",lastname) as applicant_name', $tbl_applicant . ".status", "interview_detail.applicant_status as task_status", "interview_detail.quiz_status", "interview_detail.mark_as_no_show", $tbl_applicant . ".flagged_status", "interview_detail.quiz_status_overseen_by");

            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->select("case when interview_detail.quiz_status_overseen_by!=0 THEN 
                (SELECT concat(m.firstname,' ',m.middlename,' ',m.lastname) as recruiter_name FROM tbl_member as m where m.id=interview_detail.quiz_status_overseen_by) ELSE '' END as quiz_status_overseen_by", false);

            $this->db->from($tbl_applicant);
            $this->db->join('tbl_recruitment_task_applicant as tapp', 'tapp.applicant_id = tbl_recruitment_applicant.id AND tapp.archive=tbl_recruitment_applicant.archive AND tapp.taskId=' . $taskId, 'inner');
            $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as interview_detail', 'interview_detail.recruitment_task_applicant_id = tapp.id AND tapp.archive=interview_detail.archive', 'inner');
            $this->db->where($tbl_applicant . ".id=", $applicant_id);
            $this->db->where($tbl_applicant . ".archive=", 0);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $basic_info = $query->row_array();

            $this->db->select("phone");
            $this->db->from('tbl_recruitment_applicant_phone');
            $this->db->where(array('applicant_id' => $applicant_id, 'archive' => 0, 'primary_phone' => 1));
            $query_phone = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $applicant_ph = $query_phone->row_array();

            $this->db->select("email");
            $this->db->from('tbl_recruitment_applicant_email');
            $this->db->where(array('applicant_id' => $applicant_id, 'archive' => 0, 'primary_email' => 1));
            $query_email = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $applicant_email = $query_email->row_array();

            $this->db->select(array("street", "city", "postal", "state", "tbl_state.name", 'concat(street," ",city," ",name," ",postal) as full_address'));
            $this->db->from('tbl_recruitment_applicant_address');
            $this->db->where(array('applicant_id' => $applicant_id, 'primary_address' => 1));
            $this->db->join('tbl_state', 'tbl_state.id = tbl_recruitment_applicant_address.state', 'inner');
            $query_address = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            
            $applicant_address = $query_address->row_array();

            $this->db->select(['concat(sub_m.firstname," ",sub_m.middlename," ",sub_m.lastname)'], false);
            $this->db->from('tbl_recruitment_task_recruiter_assign sub_rtra');
            $this->db->join('tbl_member sub_m', 'sub_m.id=sub_rtra.recruiterId AND sub_rtra.primary_recruiter=1 AND sub_m.archive=sub_rtra.archive', 'inner');
            $this->db->where('sub_rtra.taskId=rt.id');
            $this->db->where('sub_rtra.archive', 0);
            $this->db->limit(1);
            $sub_query_recuiter_name = $this->db->get_compiled_select();

            $this->db->select(array("DATE_FORMAT(rt.start_datetime,'%d/%m/%y') as date", "CONCAT(
                MOD(HOUR(TIMEDIFF(rt.start_datetime, rt.end_datetime)), 24), 'h:',MINUTE(TIMEDIFF(rt.start_datetime, rt.end_datetime)), 'min') as duration"));
            $this->db->select([
                '(' . $sub_query_recuiter_name . ') as recruiter_name',
                '(SELECT name from tbl_recruitment_location as sub_rl where sub_rl.id=rt.training_location AND sub_rl.archive = 0 limit 1) as location'
            ], false);
            $this->db->from('tbl_recruitment_task as rt');
            $this->db->join('tbl_recruitment_task_applicant as rta', 'rta.taskId = rt.id AND rta.archive=0 AND rta.taskId=' . $taskId . ' AND rta.applicant_id=' . $applicant_id, 'inner');
            $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as ragcid', 'ragcid.recruitment_task_applicant_id = rta.id AND rta.archive=ragcid.archive', 'inner');
            $this->db->join('tbl_recruitment_interview_type as riy', "riy.id = ragcid.interview_type and key_type='" . $type . "'", 'inner');
            $this->db->where(array('rt.id' => $taskId));
            $this->db->where(array('rt.status !=' => 4));

            $query_task_info = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $task_info = $query_task_info->row_array();
            if (empty($task_info) || empty($basic_info)) {
                return (['status' => false]);
            }

            $assign_recruiter = $this->Basic_model->get_result('recruitment_task_recruiter_assign', array('taskId'=>$taskId,'archive'=>0), $columns = array('recruiterId'), '', true);
            $assign_recruiter = array_column($assign_recruiter, 'recruiterId');

            $permission = false;
            if(in_array($reqData->adminId, $assign_recruiter))
            {
             $permission = true;
         }

         return (['basic_info' => $basic_info, 'phone' => $applicant_ph, 'email' => $applicant_email, 'address' => $applicant_address, 'status' => true, 'task_info' => $task_info,'permission'=>$permission]);
     } else {
        return (['status' => false]);
    }
}

public function get_question_details($reqData) {
    $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
    $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';
    $type = isset($reqData->data->type) && $reqData->data->type != '' ? $reqData->data->type : 'none';

    if ($applicant_id == '' && $taskId == '') {
        echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
        exit();
    }

    $sql = "SELECT SQL_CALC_FOUND_ROWS concat(m.firstname,' ',m.middlename,' ',m.lastname) as created_by,DATE_FORMAT(raq.created,'%d/%m/%y - %h:%i %p') as created_date,rqt.topic,raq.question,  raq.question_type, raq.question_topic, raq.training_category,raqfa.answer as applicant_answer,raqfa.is_answer_correct,raq.id as question_id, case when raq.question_type!=4 THEN 
    (SELECT concat_ws('#__BREAKER__#',GROUP_CONCAT(raqa.answer),GROUP_CONCAT(raqa.question_option), GROUP_CONCAT(raqa.serial)) as data FROM tbl_recruitment_additional_questions_answer as raqa where raqa.question=raqfa.question_id AND raqa.archive=0) ELSE (SELECT question_option as data FROM tbl_recruitment_additional_questions_answer as raqa where raqa.question=raqfa.question_id AND raqa.archive=0) END as job_details 
    FROM `tbl_recruitment_additional_questions_for_applicant` as `raqfa` 
    INNER JOIN `tbl_recruitment_task_applicant` as `rta` ON `rta`.`id` = `raqfa`.`recruitment_task_applicant_id` AND rta.archive=raqfa.archive AND rta.applicant_id= ? AND rta.taskId= ? 
    INNER JOIN tbl_recruitment_applicant_group_or_cab_interview_detail as ragcid ON  ragcid.recruitment_task_applicant_id = rta.id AND raqfa.archive=ragcid.archive
    INNER JOIN tbl_recruitment_interview_type as riy ON  riy.id = ragcid.interview_type and riy.key_type= ? AND `riy`.archive= `raqfa`.archive
    INNER JOIN `tbl_recruitment_additional_questions` as `raq` ON `raq`.`id` = `raqfa`.`question_id`  AND `raq`.`training_category`= riy.id AND `raq`.archive= `raqfa`.archive
    INNER JOIN `tbl_recruitment_question_topic` as `rqt` ON `rqt`.`id` = `raq`.`question_topic` 
    INNER JOIN `tbl_member` as `m` ON `m`.`id` = `raq`.`created_by` 
    WHERE `raqfa`.`archive` = '0'";

    $exe_query = $this->db->query($sql, array($applicant_id, $taskId, $type));

    $rows = $exe_query->num_rows();

    if ($rows > 0) {
        $records = $exe_query->result_array();
        $wrong_ans = 0;
        $right_ans = 0;
        $graph_ary = array(array('', 'Pass', 'Fail'));
        if (!empty($records)) {
            $select_param_ary = [];
            $main_aray = [];

            foreach ($records as $key => $value) {
                if (!isset($graph_ary[$value['topic']])) {
                    $graph_ary[$value['topic']] = array($value['topic'], 0, 0);
                }

                if (!in_array($value['question_topic'], $select_param_ary)) {
                    $select_param_ary[] = $value['question_topic'];

                    if ($value['is_answer_correct'] == 1) {
                        $right_ans = $right_ans + 1;
                        $graph_ary[$value['topic']]['1'] = ((int) $graph_ary[$value['topic']]['1'] + 1);
                    } else if ($value['is_answer_correct'] == 2) {
                        $wrong_ans = $wrong_ans + 1;
                        $graph_ary[$value['topic']]['2'] = ((int) $graph_ary[$value['topic']]['2'] + 1);
                    }
                } else {
                    if ($value['is_answer_correct'] == 1) {
                        $right_ans = $right_ans + 1;
                        $graph_ary[$value['topic']]['1'] = ((int) $graph_ary[$value['topic']]['1'] + 1);
                    } else if ($value['is_answer_correct'] == 2) {
                        $wrong_ans = $wrong_ans + 1;
                        $graph_ary[$value['topic']]['2'] = ((int) $graph_ary[$value['topic']]['2'] + 1);
                    }
                }

                $temp['question'] = $value['question'];
                $temp['question_type'] = $value['question_type'];
                $temp['question_topic'] = $value['question_topic'];
                $temp['training_category'] = $value['training_category'];
                $temp['question_id'] = $value['question_id'];


                $temp_ary = [];

                if ($value['question_type'] != 4) {
                    $temp_detail = explode('#__BREAKER__#', $value['job_details']);
                    $temp_ary['is_answer'] = !empty($temp_detail[0]) ? explode(',', $temp_detail[0]) : [];
                    $temp_ary['option_value'] = !empty($temp_detail[1]) ? explode(',', $temp_detail[1]) : [];
                    $temp_ary['serial'] = !empty($temp_detail[2]) ? explode(',', $temp_detail[2]) : [];
                    $applicant_answer = !empty($value['applicant_answer']) ? explode(',', $value['applicant_answer']) : [];
                    $applicant_answer = !empty($applicant_answer) ? array_filter($applicant_answer) : [];                   
                    $index =  array_search($value['applicant_answer'],$temp_ary['option_value']);
                    
                    $temp['applicant_answer'] = $applicant_answer;
                    $temp['applicant_answer_'] = array('key'=>$temp_ary['serial'][$index],'val'=>$value['applicant_answer']);
                } else {
                    $temp_ary['answer_key'] = $value['job_details'];
                    $temp['applicant_answer'] = $value['applicant_answer'];
                }

                $temp['is_correct'] = $value['is_answer_correct'];
                $temp['job_details'] = $temp_ary;
                $temp['created_by'] = $value['created_by'] . ' (' . $value['created_date'] . ')';
                $main_aray[$value['question_topic']]['id'] = $value['question_topic'];
                $main_aray[$value['question_topic']]['name'] = $value['topic'];
                $main_aray[$value['question_topic']]['complete_ary'][] = $temp;
            }
                #pr($main_aray);
            if (Count($graph_ary) == 1) {
                $graph_ary[] = array('', 0, 0);
            }
            echo json_encode(array('status' => true, 'data' => $main_aray, 'right_ans' => $right_ans, 'wrong_ans' => $wrong_ans, 'total_row' => count($records), 'graph_ary' => array_values($graph_ary)));
            exit();
        }
    } else {
        echo json_encode(array('status' => false));
        exit();
    }
}

public function update_answer($reqData) {
    $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
    $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';
    $type = isset($reqData->data->type) && $reqData->data->type != '' ? $reqData->data->type : 'none';

    if ($applicant_id == '' && $taskId == '' && $type == 'none') {
        echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
        exit();
    }

    $questionId = isset($reqData->data->questionId) && $reqData->data->questionId != '' ? $reqData->data->questionId : '-1';
    $answer_val = isset($reqData->data->answer_val) && $reqData->data->answer_val != '' ? $reqData->data->answer_val : '';

    $this->db->select(["rta.id"]);
    $this->db->from('tbl_recruitment_additional_questions_for_applicant raqfa');
    $this->db->join('tbl_recruitment_task_applicant rta', 'rta.id = raqfa.recruitment_task_applicant_id AND rta.archive=raqfa.archive AND rta.applicant_id=\'' . $applicant_id . '\' AND rta.taskId=\'' . $taskId . '\'', 'inner');
    $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail ragcid', 'ragcid.recruitment_task_applicant_id = rta.id AND raqfa.archive=ragcid.archive', 'inner');
    $this->db->join('tbl_recruitment_interview_type riy', 'riy.id = ragcid.interview_type and riy.key_type=\'' . $type . '\' AND riy.archive= raqfa.archive', 'inner');
    $this->db->where('raqfa.question_id', $questionId);
    $this->db->where('raqfa.archive', 0);
    $query = $this->db->get();

        //$data = $this->load->Basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'), $id_array = array('taskId'=>$taskId,'applicant_id'=>$applicant_id));
    $data = $query->num_rows() > 0 ? $query->row() : [];

    if (!empty($data)) {
        $recruitment_task_applicant_id = $data->id;
        $is_update = $this->load->Basic_model->update_records('recruitment_additional_questions_for_applicant', array('is_answer_correct' => $answer_val), array('recruitment_task_applicant_id' => $recruitment_task_applicant_id, 'question_id' => $questionId));
        if ($is_update) {
            echo json_encode(array('status' => true, 'msg' => 'Updated successfully.'));
            exit();
        } else {
            echo json_encode(array('status' => false, 'msg' => 'Error in update.'));
            exit();
        }
    } else {
        echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
        exit();
    }
}

public function mark_applicant_quiz_status($reqData) {
    $applicant_id = isset($reqData->data->applicantId) && $reqData->data->applicantId != '' ? $reqData->data->applicantId : '';
    $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';

    if ($applicant_id == '' && $taskId == '') {
        return array('status' => false, 'msg' => 'Invalid request.');
    }

    $quiz_status = isset($reqData->data->quiz_status) && $reqData->data->quiz_status != '' ? $reqData->data->quiz_status : 0;

    $data = $this->load->Basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'), $id_array = array('taskId' => $taskId, 'applicant_id' => $applicant_id));

    if (!empty($data)) {
        $recruitment_task_applicant_id = $data->id;
        
        $is_update = $this->load->Basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', array('quiz_status' => $quiz_status, 'quiz_status_overseen_by' => $reqData->adminId), array('recruitment_task_applicant_id' => $recruitment_task_applicant_id));

        if ($is_update) {
            return array('status' => true, 'msg' => 'Updated successfully.', 'recruitment_task_applicant_id' => $recruitment_task_applicant_id,'quiz_status' =>$quiz_status);
        } else {
            return array('status' => false, 'msg' => 'Error in update.');
        }
    } else {
        return array('status' => false, 'msg' => 'Invalid request.');
    }
}

public function get_draft_contract_data($applicant_id=0)
#public function get_draft_contract_data()
{ 
    $this->db->select(array("concat(ra.firstname,' ',ra.middlename,' ',ra.lastname) as applicant_name","(select concat(street,'@@__BREAKER__@@',city,' ',name,' ',postal) from tbl_recruitment_applicant_address as addr inner join  tbl_state on tbl_state.id=addr.state AND tbl_state.archive=0 AND addr.applicant_id='".$applicant_id."' AND addr.primary_address=1) as street_address"));
    $this->db->select(['(SELECT email FROM tbl_recruitment_applicant_email where primary_email=1 and archive=0 AND applicant_id=ra.id) as email',false]);
    $this->db->from('tbl_recruitment_applicant as ra');
    $this->db->where(array('ra.id'=>$applicant_id));
    $this->db->where(array('ra.archive='=>0));
    $query_task_info = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $info = $query_task_info->row_array();
    $info['street_address_other']='';
    if(!empty($info)){
        $address = $info['street_address'];
        $address = explode('@@__BREAKER__@@',$address);
        $info['street_address'] = $address[0];
        $info['street_address_other'] = isset($address[1])? $address[1]:'';
    }
    return $info; 
}

public function mark_no_show($reqData) {
    $recruitment_task_applicant_id = isset($reqData->data->recruitment_task_applicant_id) && $reqData->data->recruitment_task_applicant_id != '' ? $reqData->data->recruitment_task_applicant_id : '';
    if ($recruitment_task_applicant_id == '') {
        echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
        exit();
    }

    $data = $this->load->Basic_model->get_row($table_name = 'recruitment_applicant_group_or_cab_interview_detail', $columns = array('allot_question'), $id_array = array('recruitment_task_applicant_id' => $recruitment_task_applicant_id));

    $allot_question = !empty($data) ? $data->allot_question : '';
    if($allot_question == 1)
    {
       echo json_encode(array('status' => false, 'msg' => 'Quiz is allocated to applicant, "No Show" is not allowed.'));
       exit();
    }

   $is_update = $this->load->Basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', array('mark_as_no_show' => 1, 'applicant_status' => 2, 'marked_date' => DATE_TIME), array('recruitment_task_applicant_id' => $recruitment_task_applicant_id));

   if ($is_update) {
    echo json_encode(array('status' => true, 'msg' => 'Updated successfully.'));
    exit();
} else {
    echo json_encode(array('status' => false, 'msg' => 'Error in update.'));
    exit();
}
}

public function commit_group_interview($reqData) {
    $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';

    if ($taskId == '') {
        echo json_encode(array('status' => false, 'msg' => 'Invalid request.'));
        exit();
    }

    $this->db->select(['rta.id as recruitment_task_applicant_id', 'ragid.quiz_status', 'ragid.applicant_status', 'ragid.contract_status', 'ragid.device_pin', 'ragid.device_pin']);
    $this->db->from('tbl_recruitment_task_applicant as rta');
    $this->db->join('tbl_recruitment_applicant as ra', 'ra.id = rta.applicant_id', 'inner');
    $this->db->join('tbl_recruitment_applicant_group_or_cab_interview_detail as ragid', 'ragid.recruitment_task_applicant_id = rta.id AND ragid.archive=0', 'inner');
    $this->db->join('tbl_recruitment_interview_type rit', 'rit.id = ragid.interview_type and rit.key_type="group_interview" AND rit.archive= 0', 'inner');
    $this->db->where('rta.taskId', $taskId);
    $this->db->where('rta.status', 1);
    $this->db->where('rta.archive', 0);
    $query = $this->db->get();
        #last_query();
    $applicant_ques = $query->result_array();

    $is_update = false;
    if (!empty($applicant_ques)) {
        $update_arr = [];
        foreach ($applicant_ques as $key => $value) {
            $temp=[];
            $temp['recruitment_task_applicant_id'] = $value['recruitment_task_applicant_id'];

            if ($value['quiz_status'] != 1 && $value['contract_status'] != 1)
                $temp['applicant_status'] = 2;
            else if ($value['quiz_status'] == 1 && $value['contract_status'] == 1)
                $temp['applicant_status'] = 1;
            else
                $temp['applicant_status'] = 2;
            
            $update_arr[] = $temp;
        }

        if (!empty($update_arr)) {
            $this->Basic_model->insert_update_batch($action = 'update', $table_name = 'recruitment_applicant_group_or_cab_interview_detail', $update_arr, 'recruitment_task_applicant_id');
            ##@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/uploads/dev_log/controllers/response.txt', $this->db->last_query().PHP_EOL , FILE_APPEND | LOCK_EX);
            #pr($update_arr);die;
            $this->Basic_model->update_records('recruitment_task', array('commit_status' => 1, 'action_at' => DATE_TIME, 'status' => 2), array('id' => $taskId));
            $is_update = true;
        }
    }

    if ($is_update) {
        echo json_encode(array('status' => true, 'msg' => 'Updated successfully.'));
        exit();
    } else {
        echo json_encode(array('status' => false, 'msg' => 'Error in update.'));
        exit();
    }
}

public function generate_draft_contract($applicantId,$recruitment_task_applicant_id,$extraParms=['type'=>'group_interview','file_name'=>''])
{
    $contractALlowedFor = ['group_interview'=>['file_path'=>GROUP_INTERVIEW_CONTRACT_PATH],'cabday_interview'=>['file_path'=>CABDAY_INTERVIEW_CONTRACT_PATH]];
    #$recruitment_task_applicant_id = 8;
    $file_name = isset($extraParms['file_name'])? $extraParms['file_name']:'';
    $type = isset($extraParms['type'])? $extraParms['type']:'group_interview';
    $file_name = !empty($file_name) ? $file_name:$recruitment_task_applicant_id.'.pdf';
    error_reporting(0);
    $file ='';
    $fileHeader ='';
    $fileFooter ='';
    $info = $this->get_draft_contract_data($applicantId);
    $data['complete_data'] = isset($info) && !empty($info)?$info:[];
    if($type=='group_interview'){
        $file = $this->load->view('group_interview_contract',$data,true);
    }elseif($type=='cabday_interview'){
        $data['type']='header';
        $fileHeader =$this->load->view('cabday_interview_contract',$data,true);
        $data['type']='content';
        $file = $this->load->view('cabday_interview_contract',$data,true);
        $data['type']='footer';
        $fileFooter = $this->load->view('cabday_interview_contract',$data,true);
    }
    
    $this->load->library('m_pdf');
    $pdf = $this->m_pdf->load();
    $pdf->setAutoTopMargin='pad';
    if(!empty($fileHeader))
        $pdf->SetHTMLHeader($fileHeader);

    if(!empty($fileFooter))
        $pdf->SetHTMLFooter($fileFooter);

    $pdf->WriteHTML($file);
    $fileDir = isset($contractALlowedFor[$type]['file_path']) ? $contractALlowedFor[$type]['file_path']:GROUP_INTERVIEW_CONTRACT_PATH;
    $pdfFilePath =  $fileDir. $file_name;
    $xx = $pdf->Output($pdfFilePath, 'F');
    $contract_generate = false;
    if(file_exists($pdfFilePath))
    {
        $contract_generate = true;
        $ins_data = array('task_applicant_id'=>$recruitment_task_applicant_id,'unsigned_file'=>$file_name,'send_date'=>DATE_TIME,'created'=>DATE_TIME);

        $ids = $this->basic_model->insert_records('recruitment_applicant_contract', $ins_data);
        if($type=='cabday_interview'){
            $data = [
                'userdetails' => ['name' => (isset($info['applicant_name'])? $info['applicant_name']:'Applicant name'), 'email_subject' => 'cabday interview contract document','email' => $info['email']],
                'document' => ['doc_id' => 1 , 'doc_name' => $recruitment_task_applicant_id.'_signed_copy',  'doc_path' => $pdfFilePath , 'web_hook_url'=>base_url(DS_STAGING_WEBHOOK_URL_CABDAY)],
                'position' => ['position_x' => 110, 'position_y' => 260, 'document_id' => 1, 'page_number' => 26, 'recipient_id' => 1]
            ];

            $this->load->library('DocuSignEnvelope');
            $statusDocuSign = $this->docusignenvelope->CreateEnvelope($data);
            
            if(isset($statusDocuSign['status']) && $statusDocuSign['status']){
                $envId = $statusDocuSign['response']['envelopeId'];
                $this->basic_model->update_records('recruitment_applicant_contract', array('envelope_id'=>$envId),array('id'=>$ids,'archive'=>0));
            }else{
                $contract_generate = false;
            }
        }
        if($contract_generate)
            $this->basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', array('contract_status'=>1),array('recruitment_task_applicant_id'=>$recruitment_task_applicant_id,'archive'=>0));
    }    
    return $contract_generate;
}

}

