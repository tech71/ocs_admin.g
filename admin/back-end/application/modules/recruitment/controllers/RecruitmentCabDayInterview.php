<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RecruitmentCabDayInterview extends MX_Controller {
    use formCustomValidation;
    use callByPassStageProcess;
    function __construct() {
        parent::__construct();
        $this->load->model(['Recruitment_cab_day_model','Recruitment_group_interview_model']);
        $this->load->library('form_validation');
        $this->load->library('UserName');
        $this->form_validation->CI = & $this;
    }

    public function get_cab_day_interview_list() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
           
            $result = $this->Recruitment_cab_day_model->get_cab_day_interview_list($reqData->data);
            echo json_encode($result);
        }
    }
    
    public function get_cab_day_task_applicant_details() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
           
            $result = $this->Recruitment_cab_day_model->get_cab_day_task_applicant_details($reqData->data->taskId);
            echo json_encode(['status' => true, 'data' => $result]);
        }
    }
    public function get_cab_day_task_applicant_specific_details() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
           
            $result = $this->Recruitment_cab_day_model->get_cab_day_task_applicant_specific_details($reqData->data->applicant_id);
            echo json_encode($result);
            exit;
        }
    }

    public function update_document_status() {
        $request = request_handler('access_recruitment');
        $this->loges->setCreatedBy($request->adminId);
        $reqData = $request->data;

        if (!empty($reqData)) {
            $reqData->applicant_rule = json_encode(['type_of_checks' => ['cab_day_interview_permission_edit'],
                'applicant_id' => $reqData->applicant_id,
                'extra_params' => ['type'=>'detailId','taskIdOrCabdayDetailId' =>  $reqData->detailId,'adminId'=>$request->adminId]]);
            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'detailId', 'label' => 'detail Id', 'rules' => 'required'),
                array('field' => 'action', 'label' => 'action', 'rules' => 'required'),
                array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks')
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if(isset($reqData->action) && $reqData->action!=1 && $reqData->action!=2){
                    echo json_encode(['status'=>false,'error'=>'invalid request.']);
                    exit;
                }
                $statusData = $this->Recruitment_cab_day_model->check_pending_review_doc_or_outstaning_document_left($reqData->applicant_id);
                $action = $reqData->action == 2 ? 2 : 1;
                if($statusData && $action==1){
                    echo json_encode(['status'=>false,'error'=>'Review or outstanding Document list left so document status not updated.']);
                    exit;
                }
                $action = $reqData->action == 2 ? 2 : 1;
                $update_data = ['document_status' => $action];
                $this->db->trans_start();

                $res =$this->basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', $update_data, ['id' => $reqData->detailId,'document_status'=>0]);
                //last_query(1);
                $resDocusign=true;
                if($res){
                    if($action==1){
                        $resData =$this->basic_model->get_row('recruitment_applicant_group_or_cab_interview_detail', array('recruitment_task_applicant_id'), array('id'=>$reqData->detailId));
                        $resDocusign = $this->Recruitment_group_interview_model->generate_draft_contract($reqData->applicant_id,$resData->recruitment_task_applicant_id,['type'=>'cabday_interview','file_name'=>$resData->recruitment_task_applicant_id.'_unsigned.pdf']);
                    }
                    // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $request->adminId);


                $txt = "document status updated is " . (($action == 1) ? "approved" : "pending");
                $this->loges->setTitle($applicantName . ' - ' . 'Applicant ' . $txt . 'document');
                $this->loges->setSpecific_title($txt . ' updated by ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();
                if(!$resDocusign){
                    $this->db->trans_rollback();
                    $response = ['status' =>false,'error'=>'Genrate Document error occuer please try again.'];
                }else{
                    $response = ['status' => true];
                }

                }else{
                    $this->db->trans_rollback();
                    $response = ['status' =>false,'error'=>'invalid request.'];
                }
                $this->db->trans_commit();
                
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    public function callback_cabday_docusign(){
		$data = file_get_contents('php://input');

        file_put_contents (FCPATH.'testresponse.txt', $data);
		$xml = simplexml_load_string ($data, "SimpleXMLElement", LIBXML_PARSEHUGE);
		if(!$xml){
            echo "null content response";
			exit();
		}
		if(empty($xml->EnvelopeStatus->EnvelopeID)){
            echo "Envelope Id not exist";
			exit();
		}
		if(empty($xml->EnvelopeStatus->TimeGenerated)){
			echo "Generated time not exist";
			exit();
        }

        $envelope_id = (string)$xml->EnvelopeStatus->EnvelopeID;
        $time_generated = (string)$xml->EnvelopeStatus->TimeGenerated;
        $envelope_dir = FCPATH.CABDAY_INTERVIEW_CONTRACT_PATH;
        if(! is_dir($envelope_dir)) {mkdir ($envelope_dir, 0755);}
        $filename = $envelope_dir . "docuSignresponse" . str_replace (':' , '_' , $time_generated) . ".xml";
		$ok = file_put_contents ($filename, $data);
		if ($ok === false) {
			// Here to handel if file not generated
			echo "Xml file content not available";
			exit();
		}


        $response = $this->Recruitment_cab_day_model->get_applicant_details_by_envelope_id($envelope_id);
        $data_test=[
            'query'=>$this->db->last_query(),
            'envelope_id'=>$envelope_id,
            'response'=>$response,
            'EnvelopeStatus'=>(string)$xml->EnvelopeStatus->Status
        ];
        file_put_contents (FCPATH.'testresponse_qury.txt', print_r($data_test,true),FILE_APPEND | LOCK_EX);
        if(!empty($response)){
            $contractId = $response->contract_id;
            $taskApplicantId = $response->task_applicant_id;
            $applicantId = $response->applicant_id;
            $applicantStatus = $response->applicant_status;
            $stageId = $response->stageId;
            $recruiterId = is_null($response->recruiterId) || empty($response->recruiterId) ? 0: $response->recruiterId;
            //$uploades=CABDAY_INTERVIEW_CONTRACT_PATH.$taskApplicantId;
            $filename = '';
            if ((string)$xml->EnvelopeStatus->Status === "Completed") {
                // Loop through the DocumentPDFs element, storing each document.
                $filename_save = $taskApplicantId.'_signed.pdf';
                foreach ($xml->DocumentPDFs->DocumentPDF as $pdf) {
                    //$filename = $envelope_id.'_'.(string)$pdf->DocumentID.'.pdf';
                    $filename = (string)$pdf->Name.'.pdf';
                    $full_filename = $envelope_dir. $filename;
                    file_put_contents($full_filename, base64_decode ( (string)$pdf->PDFBytes ));
                    if(isset($pdf->DocumentID) && (string)$pdf->DocumentID>0 && strtolower((string)$pdf->PDFBytes)==strtolower('CONTENT')){
                        $filename_save = $filename;
                    }
    
                }
    
                $update_data=[];
                $update_data['signed_file'] = $filename_save;
                $update_data['signed_status'] = 1;
                $update_data['signed_date'] = DATE_TIME;
                $where_update =['envelope_id'=>$envelope_id,'id'=>$contractId];
                $this->basic_model->update_records('recruitment_applicant_contract', $update_data,$where_update);
                if($applicantStatus==1){
                    $this->basic_model->update_records('recruitment_applicant',['status'=>'3'],['id'=>$applicantId]);
                    require_once APPPATH . 'Classes/recruitment/ApplicantMoveToHCM.php';
                    $applicantMoveObj = new ApplicantMoveToHCM();
                    $applicantMoveObj->setUser_type('external_staff');
                    $applicantMoveObj->setApplicant_id($applicantId);
                    $responseData = $applicantMoveObj->move_applicant();
                    if($responseData['status']){
                        $this->load->model('Recruitment_applicant_model');
                        $reqDataTo = ['status'=>3,'applicant_id'=>$applicantId,'stageId'=>$stageId];
                        $this->Recruitment_applicant_model->update_applicant_stage_status((object)$reqDataTo,$recruiterId);
                    }
                }
                file_put_contents (FCPATH.'testresponse_qury2.txt', print_r($update_data,true));
        }else{
            file_put_contents (FCPATH.'testresponse_qury2.txt', 'envelope id not found in cabday contratct');
            echo "envelope id not found in cabday contratct";
            exit();
        }
		}
    }
    
    public function commit_cabday_interview()
    {
        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData->data->applicant_rule = json_encode(['type_of_checks' => ['cab_day_interview_permission_edit'],
                'applicant_id' => $reqData->data->applicant_id,
                'extra_params' => ['type'=>'taskId','taskIdOrCabdayDetailId' =>  $reqData->data->taskId,'adminId'=>$reqData->adminId]]);
                $validation_rules = array(
                    array('field' => 'taskId', 'label' => 'Task id', 'rules' => 'required'),
                    array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks')
                );
    
                $this->form_validation->set_data((array) $reqData->data);
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {
                    $response = $this->Recruitment_cab_day_model->commit_cabday_interview($reqData);
                } else {
                    $errors = $this->form_validation->error_array();
                    $response = ['status' => false, 'error' => implode(', ', $errors)];
                }
                echo json_encode($response);
                exit();
        }   
    }

    public function call_bypass(){
        $response = ['status' => false, 'error' => 'Invalid Request.'];
        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            if($reqData->data->stageNumber=='3'){
                $response = $this->group_interview_stage_bypass($reqData->data->applicant_id,$reqData->adminId);
            }else if($reqData->data->stageNumber=='6'){
                $response = $this->cab_interview_stage_bypass($reqData->data->applicant_id,$reqData->adminId);
            }
        }
        echo json_encode($response);
        exit();
    }


}
