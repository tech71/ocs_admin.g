<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Recruitment_question extends MX_Controller 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Recruitment_question_model');
        $this->load->model('Basic_model');
        $this->form_validation->CI = & $this;
        $this->loges->setModule(2);
    }

    public function insert_update_question() 
    {

        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) 
        {
            #pr($reqData->data);die;
            $admin_id = $reqData->adminId;
            $reqData =  $reqData->data;
            $data =(array) $reqData;
            $validation_rules = array(
                array('field' => 'question', 'label' => 'Question', 'rules' => 'required'),
                array('field' => 'question_category', 'label' => 'Training Category', 'rules' => 'required'),
                array('field' => 'question_topic', 'label' => 'Question Topic', 'rules' => 'required'),
                array('field' => 'question_status', 'label' => 'Question Status', 'rules' => 'required'),
                array('field' => 'answer_type', 'label' => 'Answer type', 'rules' => 'required'),
                array('field' => 'phone', 'label' => 'Phone', 'rules' => 'callback_check_question_data[' . json_encode($reqData) . ']'),  
                array('field' => 'answers', 'label' => 'Answer option', 'rules' => 'callback_check_answer_data['.json_encode($reqData->answers).']'),  
            );

            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) 
            {
                require_once APPPATH . 'Classes/recruitment/Questions.php';
                $objQuestions = new QuestionsClass\Questions();

                $other_msg = '';
                if($reqData->question_status == 2)
                    $other_msg = 'But the question is not active, it will not be used in assessments, please change the status to ‘Active’ for it to be used.';

                if (isset($reqData->question_id) && $reqData->question_id > 0) {
                    $count =  $this->Recruitment_question_model->get_applican_count($reqData->question_id);
                    if($count > 0){
                        $objQuestions->setId(0);
                        $this->Basic_model->update_records('recruitment_additional_questions', array('status'=>2),array('id'=>$reqData->question_id));
                    }else{
                        $objQuestions->setId($reqData->question_id);
                    }                    
                    $msg = 'Question is updated successfully.'.$other_msg;
                } else {
                    $objQuestions->setId(0);
                    $msg = 'New Question is submitted successfully.'.$other_msg;                        
                }

                $objQuestions->setQuestion($reqData->question);
                $objQuestions->setStatus($reqData->question_status);
                $objQuestions->setCreated(DATE_TIME);
                $objQuestions->setCreated_by($admin_id);
                $objQuestions->setQuestionTopic($reqData->question_topic);
                $objQuestions->setTrainingCategory($reqData->question_category);
                $objQuestions->setQuestion_Type($reqData->answer_type);
                $objQuestions->setAnswer($reqData->answers);
                $response = $objQuestions->create_Questions();
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function check_answer_data($data, $questionData)
    {
        if($questionData!='')
        {
            $questionData = json_decode($questionData);
            $a=[];
            foreach ($questionData as $key => $opt) {
               $a[] = $opt->value;
            }
            if(!empty($a) && count($a) !== count(array_unique($a)))
            {
                $this->form_validation->set_message('check_answer_data', 'Please write distinct answer option.');
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public function update_question() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            require_once APPPATH . 'Classes/recruitment/Questions.php';
            $objQuestions = new QuestionsClass\Questions();

            $objQuestions->setId(3);
            $objQuestions->setQuestion($reqData->question);
            $objQuestions->setStatus($reqData->question_status);
            $objQuestions->setCreated(DATE_TIME);
            $objQuestions->setCreated_by(1);
            $objQuestions->setQuestionTopic($reqData->question_topic);
            $objQuestions->setTrainingCategory($reqData->question_category);
            $objQuestions->setQuestion_Type($reqData->answer_type);
            $anser_array = array();
            foreach ($reqData->answers as $answer) {
                if ($answer->type == 'answer_a') {
                    $objQuestions->setAnswer_A($answer->value);
                }
                if ($answer->type == 'answer_b') {
                    $objQuestions->setAnswer_B($answer->value);
                }
                if ($answer->type == 'answer_c') {
                    $objQuestions->setAnswer_C($answer->value);
                }
                if ($answer->type == 'answer_d') {
                    $objQuestions->setAnswer_D($answer->value);
                }

                if ($answer->checked) {
                    $anser_array[] = $answer->lebel;
                }
            }
            $objQuestions->setAnswer($anser_array);
            $response = $objQuestions->update_Questions();

            echo json_encode(array('status' => true, 'data' => $response));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function delete_Question() {
        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            require_once APPPATH . 'Classes/recruitment/Questions.php';
            $objQuestions = new QuestionsClass\Questions();
            $objQuestions->setId($reqData->id);
            $objQuestions->delete_Question();
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function get_recruitment_topic_list() {
        require_once APPPATH . 'Classes/recruitment/Recruitment.php';
        $objRequirment = new RecruitmentClass\Recruitment();
        $requirment = $objRequirment->recruitment_topic_list();
        echo json_encode(array('status' => true, 'data' => $requirment));
    }

    public function get_questions_list() {
        $reqData = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $result = $this->Recruitment_question_model->get_questions_list($reqData->data);
            echo json_encode($result);
        }

    }  

    public function check_question_data($data, $questionData)
    {
        $question_Data = json_decode($questionData);
        if (!empty($question_Data)) 
        {

            if($question_Data->answer_type == 1) 
            {
                $cntAnswer = 0;
                foreach ($question_Data->answers as $answer) {
                    if ($answer->checked) {
                        $cntAnswer++;
                    }
                }
                if($cntAnswer <= 1){
                    $this->form_validation->set_message('check_question_data', 'Please select multiple answer for the question.');
                    return false;
                }
            } 
            else if($question_Data->answer_type == 2 || $question_Data->answer_type == 3) 
            {
                $cntAnswer = 0;
                foreach ($question_Data->answers as $answer) {
                    if ($answer->checked) {
                        $cntAnswer++;
                    }
                }
                if($cntAnswer <= 0){
                    $this->form_validation->set_message('check_question_data', 'Please select answer for the question.');
                    return false;
                }  
            }           
        }
        else
        {
            $this->form_validation->set_message('check_question_data', 'Please select Correct answer.');
            return false;
        }
    }

    public function get_question_detail()
    {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data; 
            $result = $this->Recruitment_question_model->get_question_detail($reqData);
            echo json_encode($result);
        }
    }

    public function update_question_status() 
    {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData)) 
        {   
            $reqData = $reqData->data; 
            $question_id = $reqData->queId;
            $updated_val = $reqData->updatedVal;
            
            $id = $this->Basic_model->update_records('recruitment_additional_questions', array('status'=>$updated_val),array('id'=>$question_id));
            if($id){
                echo json_encode( array('status' => true));
                exit();
            }
            else{
                echo json_encode (array('status' => false));
                exit();
            }
        }
    }

}
