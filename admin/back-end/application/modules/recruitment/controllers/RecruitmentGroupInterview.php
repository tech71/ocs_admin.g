<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RecruitmentGroupInterview extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Recruitment_group_interview_model','group_interview');
        $this->load->model('Basic_model');
        #$this->load->library('form_validation');
        #$this->form_validation->CI = & $this;
        #$this->loges->setModule(9);
    }

    public function get_group_interview_list() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->group_interview->get_group_interview_list($reqData);
            echo json_encode($result);
        }
    }

    public function get_applicant_question() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $result = $this->group_interview->get_applicant_question($reqData);
            echo json_encode($result);
        }
    }

    public function get_applicant_selected_question() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $result = $this->group_interview->get_applicant_selected_question($reqData);
            echo json_encode(array('status'=>true,'data'=>$result));
        }
    }

    public function commit_n_save_ques()
    {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $result = $this->group_interview->commit_n_save_ques($reqData);
            echo json_encode($result);
        }
    }

    public function get_applicant_detail()
    {
      $reqData = $reqData1 = request_handler('access_recruitment');
      if (!empty($reqData->data)) {
        $result = $this->group_interview->get_applicant_detail($reqData);
        echo json_encode($result);
    }   
}

public function get_question_details()
{
  $reqData = $reqData1 = request_handler('access_recruitment');
  if (!empty($reqData->data)) {
    $result = $this->group_interview->get_question_details($reqData);
    echo json_encode($result);
}           
}

public function update_answer()
{
  $reqData = $reqData1 = request_handler('access_recruitment');
  if (!empty($reqData->data)) {
    $result = $this->group_interview->update_answer($reqData);
    echo json_encode($result);
}           
} 

public function mark_applicant_quiz_status()
{
  $reqData = $reqData1 = request_handler('access_recruitment');
  if (!empty($reqData->data)) 
  {
    $type = isset($reqData->data->type)? $reqData->data->type:''; 
    $result = $this->group_interview->mark_applicant_quiz_status($reqData);
    
    if(isset($result) && !empty($result) && $result['status'] && $result['quiz_status'] == 1)
    {
        $iff = $type =='cab_day'? true : $this->group_interview->generate_draft_contract($reqData->data->applicantId,$result['recruitment_task_applicant_id']);
        if($iff)
            echo json_encode($result);
        else
            echo json_encode(array('status'=>false,'msg'=>'Error in creating Contract draft.'));
    }
    else
    {
        echo json_encode($result);
    }
}           
}

public function mark_no_show()
{
  $reqData = $reqData1 = request_handler('access_recruitment');
  if (!empty($reqData->data)) {
    $result = $this->group_interview->mark_no_show($reqData);
    echo json_encode($result);
}           
}

public function copy_question()
{
    #die;
    $taskId = '20';
    $interview_type = '1';
    $applicant_id = '64';

    $data = $this->load->Basic_model->get_row($table_name = 'recruitment_task_applicant', $columns = array('id'), $id_array = array('taskId'=>$taskId,'applicant_id'=>$applicant_id));
    $recruitment_task_applicant_id = $data->id;

    $all_ques_sql = "SELECT raqfa.id,raqfa.created_by,raqfa.created, GROUP_CONCAT(raqa.answer SEPARATOR  '@#_BREAKER_#@') as answer_correct,GROUP_CONCAT(raqa.question_option SEPARATOR  '@#_BREAKER_#@') as answer_option, GROUP_CONCAT(raqa.serial SEPARATOR  '@#_BREAKER_#@') as que_serial,raqfa.question, raqfa.question_type, raqfa.question_topic
    FROM `tbl_recruitment_additional_questions` as raqfa
    INNER JOIN `tbl_recruitment_additional_questions_answer` as `raqa` ON `raqa`.`question` = `raqfa`.`id` 
    WHERE NOT EXISTS (SELECT nass.question_id
    FROM tbl_recruitment_applicant_not_assign_question as nass
    WHERE  nass.question_id = raqfa.id AND nass.archive = 0) AND raqfa.archive = 0 AND raqfa.status = 1 AND raqfa.training_category = '1'  AND raqa.archive = 0
    group by raqfa.id";

    $exe_query = $this->db->query($all_ques_sql);
    $rows = $exe_query->num_rows();

    if($rows > 0)
    {
        $records = $exe_query->result_array();
        $que_insert_ary = [];
        $ans_insert_ary = [];
        $que_id_ary = [];
        
        foreach ($records as $key => $value)
        {
            $temp_que['recruitment_task_applicant_id'] = $recruitment_task_applicant_id;
            $temp_que['question_id'] = $value['id'];
            $temp_que['is_answer_correct'] = 0;
            
            #$temp_que['question'] = $value['question'];
            #$temp_que['question_type'] = $value['question_type'];
            #$temp_que['question_topic'] = $value['question_topic'];
            #$temp_que['training_category'] = 1;
            #$temp_que['created_by'] = $value['created_by'];
            #$temp_que['created'] = $value['created'];

            $que_id_ary[] = $value['id'];
            $que_insert_ary[] = $temp_que;

            $que_serial = explode('@#_BREAKER_#@', $value['que_serial']);
            $answer_option = explode('@#_BREAKER_#@', $value['answer_option']);
            $answer_correct = explode('@#_BREAKER_#@', $value['answer_correct']);

            if(!empty($answer_option) && $value['question_type']!=4)
            {
                foreach ($answer_option as $key => $val)
                {
                   $temp_ans['question'] = $value['id'];
                   $temp_ans['serial'] = $que_serial[$key];
                   $temp_ans['question_option'] = $answer_option[$key];
                   $temp_ans['answer'] = $answer_correct[$key];
                   $x[] = $temp_ans;
               } 
           }
       }

       if(!empty($que_insert_ary))
           $this->load->Basic_model->insert_update_batch($action = 'insert', $table_name = 'recruitment_additional_questions_for_applicant', $que_insert_ary);

       if(!empty($que_id_ary))
        $this->db->where_in('question_id',$que_id_ary);

    $get_all_last_insered_que_id = $this->load->Basic_model->get_result('recruitment_additional_questions_for_applicant', array('recruitment_task_applicant_id'=>$recruitment_task_applicant_id), $columns = array('id','question_id'),'',true);

    if(!empty($get_all_last_insered_que_id))
     $xs = pos_index_change_array_data($get_all_last_insered_que_id, 'question_id');

 if(!empty($x))
 {
    $c = [];
    foreach ($x as $key => $q_val)
    { 
        if(array_key_exists($q_val['question'], $xs))
        {
            $c['additional_questions_for_applicant_id'] = $xs[$q_val['question']]['id'];
            $c['question_id'] = $q_val['question'];
            $c['answer'] = $q_val['answer'];
            $c['question_option'] = $q_val['question_option'];
            $c['serial'] = $q_val['serial'];
            $c['archive'] = 0;
            $dd[] = $c;
        }
    }
   # if(!empty($dd))
        #$this->load->Basic_model->insert_update_batch($action = 'insert', $table_name = 'recruitment_additional_questions_answer_for_applicant', $dd);
}
}
}

public function commit_group_interview()
{
    $reqData = $reqData1 = request_handler('access_recruitment');
    if (!empty($reqData->data)) {
        $result = $this->group_interview->commit_group_interview($reqData);
        echo json_encode($result);
    }   
}

public function get_recruitment_topic_list()
{
    $reqData = $reqData1 = request_handler('access_recruitment');
    $taskId = isset($reqData->data->taskId) && $reqData->data->taskId != '' ? $reqData->data->taskId : '';
    require_once APPPATH . 'Classes/recruitment/Recruitment.php';
    $objRequirment = new RecruitmentClass\Recruitment();
    $requirment = $objRequirment->recruitment_topic_list();
   
    if (!empty($requirment)) {
        foreach ($requirment as $key => $value) {
            $value->selected = false;
        }
    }
    $assign_recruiter = $this->Basic_model->get_result('recruitment_task_recruiter_assign', array('taskId'=>$taskId,'archive'=>0), $columns = array('recruiterId'), '', true);
    $assign_recruiter = array_column($assign_recruiter, 'recruiterId');
    #pr([$assign_recruiter,$reqData->adminId]);
    $permission = false;
    if(in_array($reqData->adminId, $assign_recruiter))
    {
     $permission = true;
    }
    echo json_encode(array('status' => true, 'topic_list' => $requirment,'assign_recruiter'=>$assign_recruiter,'permission'=>$permission));
}


}
