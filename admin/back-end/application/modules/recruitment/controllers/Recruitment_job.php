<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Recruitment_job extends MX_Controller {

    use formCustomValidation;

    function __construct() {

        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Recruitment_jobs_model');
        $this->load->model('Basic_model');
        $this->form_validation->CI = & $this;
        $this->loges->setLogType('recruitment_job');
    }

    public function insert_update_job_delete() {

        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            //$response_valid = $this->validate_jobs_data((array)$reqData);
            //if($response_valid['status']){

            require_once APPPATH . 'Classes/recruitment/Jobs.php';
            $objJobs = new JobsClass\Jobs();

            if ($reqData->mode == 'add') {
                $objJobs->setId(0);
            } else {
                $objJobs->setId($reqData->job_id);
            }
            $objJobs->setJobName($reqData->job_name);
            $objJobs->setStatus(0);
            $objJobs->setCreated(DATE_TIME);
            //$objJobs->setRecruiter(1);
            $objJobs->setJobPosition(1); //$reqData->job_position
            $objJobs->setPhone($reqData->job_phone);
            $objJobs->setEmail($reqData->job_email);
            $objJobs->setWebLink($reqData->job_weblink);
            $objJobs->setJobDescription($reqData->job_content);
            //$objJobs->setJobCategory($reqData->answers);
            //$objJobs->setJobTemplate($reqData->answers);
            $objJobs->setJobStartDate(date("Y-m-d", strtotime($reqData->PostDate)));
            $objJobs->setJobEndDate(date("Y-m-d", strtotime($reqData->EndDate)));
            //$objJobs->setPublish($reqData->answers);
            //$objJobs->setJobStatus($reqData->answers);


            $response = $objJobs->create_Jobs();
            echo json_encode(array('status' => true, 'data' => $response));
            //}else{
            //	echo json_encode($response_valid);
            //}
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function delete_job() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            require_once APPPATH . 'Classes/recruitment/Questions.php';
            $objQuestions = new QuestionsClass\Questions();
            $objQuestions->setId($reqData->id);
            $objQuestions->delete_Question();
            echo json_encode(array('status' => true));
        } else {
            echo json_encode(array('status' => false));
        }
    }

    public function get_recruitment_job_position_list() {

    }

    public function get_job_questions_list() {

    }

    /* Question Validation start */

    function validate_jobs_data($rosterData) {
        try {
            $validation_rules = array(
                array('field' => 'start_date', 'label' => 'validate_jobs_data', 'rules' => 'callback_check_jobs_data[' . json_encode($rosterData) . ']')
            );
            $this->form_validation->set_data($rosterData);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        return $return;
    }

    function check_jobs_data($data, $questionData) {

        $question_Data = json_decode($questionData);
        if (!empty($question_Data)) {
            if (empty($question_Data->question)) {
                $this->form_validation->set_message('check_question_data', 'Question can not empty');
                return false;
            }
            if (empty($question_Data->question_topic)) {
                $this->form_validation->set_message('check_question_data', 'Question topic can not empty');
                return false;
            }
            if (empty($question_Data->question_category)) {
                $this->form_validation->set_message('check_question_data', 'Question category can not empty');
                return false;
            }
            if (empty($question_Data->answers)) {
                $this->form_validation->set_message('check_question_data', 'Answer can not empty');
                return false;
            }
            $cntAnswer = 0;
            foreach ($question_Data->answers as $answer) {
                if ($answer->checked) {
                    $cntAnswer++;
                }
            }
        } else {
            $this->form_validation->set_message('check_roster_data', 'Roster details can not empty');
            return false;
        }
        return true;
    }

    /* Question Validation end */

    public function get_job_master_list() {
        $reqData = request_handler('access_recruitment_admin');
        $data = [];

        $response = $this->basic_model->get_record_where('recruitment_job_employment_type', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
        $data['employmentType'] = isset($response) && !empty($response) ? $response : array();

        $responsePosition = $this->basic_model->get_record_where('recruitment_job_position', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
        $data['position'] = isset($responsePosition) && !empty($responsePosition) ? $responsePosition : array();

        $responseJobType = $this->basic_model->get_record_where('recruitment_job_type', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
        $data['jobType'] = isset($responseJobType) && !empty($responseJobType) ? $responseJobType : array();


        $responseSalaryRange = $this->basic_model->get_record_where('recruitment_job_salary_range', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));
        $data['salaryRange'] = isset($responseSalaryRange) && !empty($responseSalaryRange) ? $responseSalaryRange : array();

        $responseCategory = $this->basic_model->get_record_where('recruitment_job_category', $column = array('parent_id', 'id as value', 'name as label'), $where = array('archive' => '0', 'parent_id' => 0));
        $data['category'] = isset($responseCategory) && !empty($responseCategory) ? $responseCategory : array();

        $responseJobTemplate = $this->basic_model->get_record_where('recruitment_job_template', $column = array('id', 'template', 'name', 'thumb'), $where = array('archive' => '0'));
        $data['jobTemplate'] = isset($responseJobTemplate) && !empty($responseJobTemplate) ? $responseJobTemplate : array();

        echo json_encode(array('status' => true, 'data' => $data));
    }

    public function get_job_all_documents() {
        $reqData = request_handler('access_recruitment_admin');
        $data = [];
        $response = $this->basic_model->get_record_where('recruitment_job_requirement_docs', $column = array('title', 'id'), $where = array('archive' => '0'));
        if (!empty($response)) {
            foreach ($response as $val) {
                $all_documents[] = array('label' => $val->title, 'value' => $val->id, 'optional' => false, 'mandatory' => false, 'clickable' => false, 'selected' => false);
            }
        }
        echo json_encode(array('status' => true, 'data' => $all_documents));
    }

    public function get_job_channel_details() {
        $reqData = request_handler('access_recruitment_admin');
        $data = [];
        $response = $this->basic_model->get_record_where('recruitment_channel', $column = array('id', 'channel_name'), $where = array('archive' => '0'));
        if (!empty($response)) {
            foreach ($response as $val) {
                $all_details[] = array('drp_dwn' => array('label' => $val->channel_name, 'value' => $val->id), 'question' => [], 'channel_name' => $val->channel_name, 'question_tab' => false, 'drp_dwn_val' => $val->id);
            }
        }
        echo json_encode(array('status' => true, 'data' => $all_details));
    }

    public function get_subcategory_from_seek() {
        $reqData = request_handler('access_recruitment_admin');
        $PostDate = $reqData->data;
        $category_id = $PostDate->category_id;
        $responseCategory = $this->basic_model->get_record_where('recruitment_job_category', $column = array('parent_id', 'id as value', 'name as label'), $where = array('archive' => '0', 'parent_id' => $category_id));
        $sub_cat = isset($responseCategory) && !empty($responseCategory) ? $responseCategory : array();
        echo json_encode(array('status' => true, 'data' => $sub_cat));
    }

    public function get_seek_ques_by_cat_subcat() {
        $reqData = request_handler('access_recruitment_admin');
        $PostDate = $reqData->data;
        $category_id = $PostDate->category_id;
        $sub_category = $PostDate->sub_category;
        $all_details = [];

        $responseQuestion = $this->basic_model->get_record_where('recruitment_seek_question', $column = array('question', 'id'), $where = array('archive' => '0', 'category' => $category_id, 'sub_category' => $sub_category));

        if (!empty($responseQuestion)) {
            foreach ($responseQuestion as $val) {
                $all_details[] = array('question' => $val->question, 'id' => $val->id, 'question_edit' => false, 'editable_class' => '', 'btn_txt' => 'Edit');
            }
        }

        echo json_encode(array('status' => true, 'data' => $all_details));
    }

    public function save_job() {
        $request = request_handler('access_recruitment');
        $jobs_data = (array) json_decode($request->data, true);
        $jobs_data['all_documents1'][] = (object) $jobs_data['all_documents'];
        if (!empty($jobs_data)) {
            $validation_rules = array(
                array('field' => 'all_documents1[]', 'label' => 'Required Documents', 'rules' => 'callback_check_atleast_one_required_docs'),
                array('field' => 'type', 'label' => 'Job Type', 'rules' => 'required'),
                array('field' => 'category', 'label' => 'Job Category', 'rules' => 'required'),
                array('field' => 'sub_category', 'label' => 'Job Sub Category', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Job Position', 'rules' => 'required'),
                array('field' => 'employment_type', 'label' => 'Employment Type', 'rules' => 'required'),
                array('field' => 'salary_range', 'label' => 'Salary Range', 'rules' => 'required'),
                array('field' => 'job_location', 'label' => 'Job Location', 'rules' => 'required'),
                array('field' => 'phone', 'label' => 'Phone', 'rules' => 'callback_phone_number_check[phone,required,Phone number is required]'),
                array('field' => 'email', 'label' => 'Email', 'rules' => 'required|valid_email'),
                array('field' => 'website', 'label' => 'Website', 'rules' => 'required|valid_url'), //
                array('field' => 'job_content', 'label' => 'Job Content', 'rules' => 'required'),
                array('field' => 'activeTemplate', 'label' => 'Job Style Template', 'rules' => 'required'),
            );

            $this->form_validation->set_data($jobs_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $job_operation = $jobs_data['job_operation'];

                $ocs_row = $this->Recruitment_jobs_model->save_job($jobs_data);
                /* logs */
                if ($job_operation == 'E')
                    $title = "Edit job: " . $ocs_row['jobId'];
                else
                    $title = "Add job: " . $ocs_row['jobId'];

                $this->loges->setTitle($title);
                $this->loges->setUserId($request->adminId);
                $this->loges->setDescription(json_encode($jobs_data));
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->createLog();
                $return = array('status' => true, 'msg' => $ocs_row['msg']);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function check_atleast_one_required_docs($docs) {
        if (!empty($docs)) {
            $count = count((array) $docs);

            $ii = 0;
            
            #pr($docs);
            foreach ($docs as $key => $value) {

                if (isset($value['clickable']) && $value['clickable'] != 1) {
                    $ii = $ii + 1;
                }              

                if ($ii == $count) {
                    $this->form_validation->set_message('check_atleast_one_required_docs', 'Please select atleast one required Documents from Manage Documents section.');
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            $this->form_validation->set_message('check_atleast_one_required_docs', 'Please select required Documents from Manage Documents section.');
            return false;
        }
        return true;
    }

    public function get_all_jobs() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        #pr($reqData);
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Recruitment_jobs_model->get_all_jobs($reqData);
            echo json_encode($result);
        }
    }

    public function get_job_detail() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $result = $this->Recruitment_jobs_model->get_job_detail($reqData);
            echo json_encode($result);
        }
    }

    public function save_job_required_documents() {
        $request = request_handler('access_recruitment');
        $docs_data = (array) $request->data;
        if (!empty($docs_data)) {
            $validation_rules = array(
                array('field' => 'document_name', 'label' => 'Document name', 'rules' => 'required'),
            );

            $this->form_validation->set_data($docs_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $ocs_row = $this->Recruitment_jobs_model->save_job_required_documents($docs_data);
                if (!empty($ocs_row) && $ocs_row['status'])
                    $return = array('status' => true, 'msg' => 'Documents save successfully.', 'id' => isset($ocs_row['id']) ? $ocs_row['id'] : '', 'data' => $ocs_row['data']);
                else
                    $return = array('status' => false, 'error' => 'Error, Please try again.');
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function update_job_status() {
        $reqData = $reqData1 = request_handler('access_recruitment');
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $title = 'Job status has been changed to '.$reqData->status.' for JobId '.$reqData->job_id;
            $this->loges->setTitle($title);
            $this->loges->setUserId($reqData1->adminId);
            $this->loges->setDescription(json_encode($reqData));
            $this->loges->setCreatedBy($reqData1->adminId);
            $this->loges->createLog();

            $result = $this->Recruitment_jobs_model->update_job_status($reqData);
            echo json_encode($result);
        }
    }

}
