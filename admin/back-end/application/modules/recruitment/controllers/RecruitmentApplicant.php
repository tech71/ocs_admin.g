<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RecruitmentApplicant extends MX_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('Recruitment_applicant_model');

        $this->load->library('form_validation');
        $this->load->library('UserName');
        $this->form_validation->CI = & $this;
        $this->loges->setLogType('recruitment_applicant');
    }

    function get_requirement_applicants() {
        $reqData = request_handler('access_recruitment');

        if (!empty($reqData->data)) {
            $response = $this->Recruitment_applicant_model->get_requirement_applicants($reqData->data, $reqData->adminId);

            echo json_encode($response);
        }
    }

    function get_job_postion_by_create_job() {
        $reqData = request_handler('access_recruitment');

        if (!empty($reqData->data)) {
            $s = $this->Recruitment_applicant_model->get_job_postion_by_create_job();

            $response = ['status' => true, 'data' => $s];
            echo json_encode($response);
        }
    }

    function flage_applicant() {
        require_once APPPATH . 'Classes/recruitment/ActionNotification.php';

        $reqData = request_handler('access_recruitment');
        $adminId = $reqData->adminId;
        $this->loges->setCreatedBy($adminId);
        $reqData = $reqData->data;

        if (!empty($reqData)) {
            $validation_rules = array(
                array('field' => 'reason_title', 'label' => 'reason title', 'rules' => 'required'),
                array('field' => 'reason_note', 'label' => 'reason note', 'rules' => 'required'),
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $res = $this->Recruitment_applicant_model->check_already_flag_applicant($reqData->applicant_id);

                if (!empty($res)) {
                    $response = ['status' => false, 'error' => 'Applicant already flaged'];
                } else {
                    $this->Recruitment_applicant_model->flag_applicant($reqData, $adminId);

                    // set action notificaiton.
                    $objAction = new ActionNotification();
                    $objAction->setApplicant_id($reqData->applicant_id);
                    $objAction->setRecruiterId($adminId);
                    $objAction->setAction_type(1);
                    $objAction->createAction();

                    // create log
                    $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                    $admin = $this->username->getName('admin', $adminId);

                    $this->loges->setSpecific_title('Applicant Marked as flag by ' . $admin);
                    $this->loges->setTitle('Mark as flag applicant : ' . $applicantName);
                    $this->loges->setUserId($reqData->applicant_id);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->createLog();

                    $response = ['status' => true];
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
            exit();
        }
    }

    function archive_applicant() {
        $reqData = request_handler('access_recruitment_admin');
        $this->loges->setCreatedBy($reqData->adminId);
        $adminName = $this->username->getName('admin', $reqData->adminId);
        $reqData = $reqData->data;

        if (!empty($reqData->applicant_id)) {

            $this->basic_model->update_records('recruitment_applicant', ['archive' => 1], ['id' => $reqData->applicant_id]);

            // create log
            $applicantName = $this->username->getName($reqData->applicant_id);
            $this->loges->setTitle('Archive applicant: ' . $applicantName);
            $this->loges->setSpecific_title('Archive applicant by: ' . $adminName);
            $this->loges->setUserId($reqData->applicant_id);
            $this->loges->setDescription(json_encode($reqData));
            $this->loges->createLog();

            echo json_encode(['status' => true]);
            exit();
        }
    }

    function get_requirement_flaged_applicants() {
        $reqData = request_handler('access_recruitment_admin');

        if (!empty($reqData->data)) {
            $response = $this->Recruitment_applicant_model->get_requirement_flaged_applicants($reqData->data, $reqData->adminId);

            echo json_encode($response);
        }
    }

    function dont_flag_applicant() {
        $reqData = request_handler('access_recruitment_admin');
        $this->loges->setCreatedBy($reqData->adminId);
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;

        if (!empty($reqData->applicant_id)) {

            // dont flag applicant handle
            $this->Recruitment_applicant_model->dont_flag_applicant($reqData, $adminId);

            // set log data
            $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
            $adminName = $this->username->getName('admin', $adminId);

            $this->loges->setTitle('Flag request denieded of applicant: ' . $applicantName);
            $this->loges->setSpecific_title('Flag request denieded of applicant by: ' . $adminName);
            $this->loges->setUserId($reqData->applicant_id);
            $this->loges->setDescription(json_encode($reqData));
            $this->loges->createLog();

            echo json_encode(['status' => true]);
        }
    }

    function flag_applicant_approve() {
        $reqData = request_handler('access_recruitment_admin');
        $this->loges->setCreatedBy($reqData->adminId);
        $adminId = $reqData->adminId;
        $reqData = $reqData->data;

        if (!empty($reqData->applicant_id)) {

            // flag applicant
            $this->Recruitment_applicant_model->flag_applicant_approve($reqData, $adminId);

            // set log data
            $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
            $adminName = $this->username->getName('admin', $adminId);

            $this->loges->setTitle('Flag request approve of applicant: ' . $applicantName);
            $this->loges->setSpecific_title('Flag request approve by: ' . $adminName);
            $this->loges->setUserId($reqData->applicant_id);
            $this->loges->setDescription(json_encode($reqData));
            $this->loges->createLog();

            echo json_encode(['status' => true]);
        }
    }

    function get_duplicate_requirement_applicants() {
        $reqData = request_handler('access_recruitment_admin');
        if (!empty($reqData->data)) {
            $response = $this->Recruitment_applicant_model->get_recruitment_duplicate_applicants($reqData->data, $reqData->adminId);
            echo json_encode($response);
        }
    }

    function update_duplicate_application_relevant_note() {
        $reqData = request_handler('access_recruitment_admin');
        $adminId = $reqData->adminId;
        $this->loges->setCreatedBy($reqData->adminId);
        $reqData = $reqData->data;

        if (!empty($reqData)) {
            $validation_rules = array(
                array('field' => 'id', 'label' => 'Application Id', 'rules' => 'required'),
                array('field' => 'relevant_note', 'label' => 'Relevant Note', 'rules' => 'required'),
                array('field' => 'applicationNumber', 'label' => 'Application Number', 'rules' => 'required')
            );
            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == TRUE) {
                $return = $this->Recruitment_applicant_model->update_duplicate_application_relevant_note($reqData);
                if (isset($return['status']) && $return['status']) {

                    // set log details
                    $applicantName = $this->username->getName('applicant', $reqData->id);
                    $adminName = $this->username->getName('admin', $adminId);

                    $this->loges->setUserId($reqData->id);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Applicant - ' . $applicantName . ' Relevant note added on Duplicate applicant application');
                    $this->loges->setSpecific_title('Relevant note added on Duplicate applicant application by ' . $adminName);
                    $this->loges->createLog();
                }
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } else {
            $return = array('status' => false, 'error' => 'Something went wrong.');
        }
        echo json_encode($return);
    }

    function update_duplicate_application_status() {
        $reqData = request_handler('access_recruitment_admin');
        $adminId = $reqData->adminId;
        $this->loges->setCreatedBy($reqData->adminId);
        $statusType = [
            'reject' => 'Pending to Rejected',
            'accept_addnem' => 'Pending to Accepted with add to current applications',
            'accept_editexisting' => 'pending to Accepted with modify existing applications',
        ];
        $reqData = $reqData->data;

        if (!empty($reqData)) {
            $validation_rules = array(
                array('field' => 'id', 'label' => 'Application Id', 'rules' => 'required'),
                array('field' => 'status', 'label' => 'Application Status', 'rules' => 'required'),
                array('field' => 'applicationNumber', 'label' => 'Application Number', 'rules' => 'required')
            );
            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == TRUE) {
                $return = $this->Recruitment_applicant_model->update_duplicate_application_status($reqData, $statusType);
                if (isset($return['status']) && $return['status']) {

                    // set log details
                    $applicantName = $this->username->getName('applicant', $reqData->id);
                    $adminName = $this->username->getName('admin', $adminId);

                    $this->loges->setUserId($reqData->id); // set applicant id in log
                    $this->loges->setDescription(json_encode($reqData));   // set all request data in participant in log
                    $statusText = isset($statusType[$reqData->status]) ? $statusType[$reqData->status] : ' pending to ' . $reqData->status;

                    $this->loges->setTitle('Applicant - ' . $applicantName . ' Duplicate application status change from ' . $statusText);  // set title in log
                    $this->loges->setSpecific_title('Duplicate application status change from ' . $statusText . ' by ' . $adminName);
                    $this->loges->createLog(); // create log
                }
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } else {
            $return = array('status' => false, 'error' => 'Something went wrong.');
        }
        echo json_encode($return);
    }

    function get_applicant_info() {
        $reqData = request_handler('access_recruitment');
        $adminId = $reqData->adminId;

        $reqData = $reqData->data;

        if (!empty($reqData->applicant_id)) {

            // get applicant details
            $result['details'] = $this->Recruitment_applicant_model->get_applicant_info($reqData->applicant_id, $adminId);

            if (!empty($result['details'])) {
                // get applicant phone number
                $result['phones'] = $this->Recruitment_applicant_model->get_applicant_phone($reqData->applicant_id);

                // get applicant emails
                $result['emails'] = $this->Recruitment_applicant_model->get_applicant_email($reqData->applicant_id);

                // get applicant reference
                $result['references'] = $this->Recruitment_applicant_model->get_applicant_reference($reqData->applicant_id);

                // get applicant addresses
                $result['addresses'] = $this->Recruitment_applicant_model->get_applicant_address($reqData->applicant_id);

                $result['applications'] = $this->Recruitment_applicant_model->get_applicant_job_application($reqData->applicant_id);

                $result['last_update'] = $this->Recruitment_applicant_model->get_applicant_last_update($reqData->applicant_id);

                echo json_encode(['status' => true, 'data' => $result]);
            } else {

                echo json_encode(['status' => false, 'data' => 'Applicant not found']);
            }
        }
    }

    function get_applicant_main_stage_details() {
        $reqData = request_handler('access_recruitment');
        $reqData = $reqData->data;

        if (!empty($reqData->applicant_id)) {

            $result['applicant_progress'] = $this->Recruitment_applicant_model->get_applicant_progress($reqData->applicant_id);

            $result['stage_details'] = $this->Recruitment_applicant_model->get_applicant_stage_details($reqData->applicant_id);

            echo json_encode(['status' => true, 'data' => $result]);
        }
    }

    function update_assign_recruiter() {
        $reqData = request_handler('access_recruitment_admin');
        $adminId = $reqData->adminId;
        $this->loges->setCreatedBy($reqData->adminId);
        $reqData = $reqData->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'recruiter', 'label' => 'recruiter id', 'rules' => 'required|callback_check_its_recruiter'),
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->basic_model->update_records('recruitment_applicant', ['recruiter' => $reqData->recruiter], ['id' => $reqData->applicant_id]);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $adminId);

                $this->loges->setTitle('Assign recruiter updated of applicant: ' . $applicantName);
                $this->loges->setSpecific_title('Assign recruiter updated by ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
            exit();
        }
    }

    function check_its_recruiter($recruiterId) {
        $res = check_its_recruiter_admin($recruiterId);
        if (!$res) {
            return true;
        } else {

            $this->form_validation->set_message('check_its_recruiter', 'Please provide valid recruiter id');
            return false;
        }
    }

    function check_applicant_phone_interview_already_exist($applicantId) {
        $res = $this->Recruitment_applicant_model->get_applicant_phone_interview_classification($applicantId);

        if (!empty($res)) {
            $this->form_validation->set_message('check_applicant_phone_interview_already_exist', 'Phone interview classification already exist');
            return false;
        } else {
            return true;
        }
    }

    function update_applicant_stage_status() {
        $reqestData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {
            $reqData->applicant_rule = json_encode(['type_of_checks' => ['not_flagged', 'not_archive', 'on_stage_complete_verified_details'],
                'applicant_id' => $reqData->applicant_id,
                'extra_params' => ['stageId' => $reqData->stageId]]);

            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'status', 'label' => 'status', 'rules' => 'required'),
                array('field' => 'stageId', 'label' => 'stage Id', 'rules' => 'required'),
                array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks'),
            );

            $this->form_validation->set_data(obj_to_arr($reqData));
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $this->Recruitment_applicant_model->update_applicant_stage_status($reqData, $reqestData->adminId);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $stage_label = $this->username->getName('stage_label', $reqData->stageId);

                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));

                $txt = ($reqData->status == 3) ? 'Completed' : 'Unsuccessfull';
                $this->loges->setTitle('Applicant - ' . $applicantName . ' ' . $stage_label . ' ' . $txt);  // set title in log
                $this->loges->setSpecific_title($stage_label . ' ' . $txt);
                $this->loges->createLog(); // create log

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }
        }

        echo json_encode($response);
    }

    function update_applicant_phone_interview_classification() {
        $reqestData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {
            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'callback_check_applicant_phone_interview_already_exist'),
                array('field' => 'classificaiton', 'label' => 'phone classificaiton', 'rules' => 'required'),
            );

            $this->form_validation->set_data(obj_to_arr($reqData));
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $classification_data = array('applicant_id' => $reqData->applicant_id, 'classfication' => $reqData->classificaiton, 'created' => DATE_TIME, 'updated_by' => $reqestData->adminId);
                $this->basic_model->insert_records('recruitment_applicant_phone_interview_classification', $classification_data, FALSE);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $reqestData->adminId);

                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->setTitle('Add phone classification of applicant - ' . $applicantName);  // set title in log
                $this->loges->setSpecific_title('Add phone classification by - ' . $adminName);
                $this->loges->createLog(); // create log

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }
        }

        echo json_encode($response);
    }

    function get_applicant_applicant_option() {
        $reqestData = request_handler('access_recruitment');
        $reqData = $reqestData->data;

        if (!empty($reqData)) {

            $data['employmentTypes'] = $this->basic_model->get_record_where('recruitment_job_employment_type', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));

            $data['jobPositions'] = $this->basic_model->get_record_where('recruitment_job_position', $column = array('title as label', 'id as value'), $where = array('archive' => '0'));

            $data['channels'] = $this->basic_model->get_record_where('recruitment_channel', $column = array('channel_name as label', 'id as value'), $where = array('archive' => '0'));

            $data['recruitmentAreas'] = $this->basic_model->get_record_where('recruitment_department', $column = array('name as label', 'id as value'), $where = array('archive' => '0'));

            echo json_encode(['status' => true, 'data' => $data]);
        }
    }

    function update_applied_application_of_application() {
        $reqestData = request_handler('access_recruitment_admin');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'applications[]', 'label' => 'applications', 'rules' => 'required'),
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
            );

            $this->form_validation->set_data(obj_to_arr($reqData));
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->Recruitment_applicant_model->update_applied_application_of_application($reqData);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $reqestData->adminId);

                $this->loges->setTitle('Update application on applicant: ' . $applicantName);
                $this->loges->setSpecific_title('Update application by ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    function get_applicant_logs() {
        $reqestData = request_handler('access_recruitment');
        $reqData = $reqestData->data;

        if (!empty($reqData->applicant_id)) {

            $result = $this->Recruitment_applicant_model->get_applicant_logs($reqData);

            echo json_encode($result);
        } else {
            echo json_encode(['status' => false, 'error' => 'Applicant id is required']);
        }
    }

    function get_applicant_stage_wise_details() {
        $reqestData = request_handler('access_recruitment');
        $reqData = $reqestData->data;

        if (!empty($reqData->applicant_id && $reqData->stage_number)) {
            $data = [];

            if ($reqData->stage_number == 1) {

                $data['question_answer'] = $this->Recruitment_applicant_model->get_applicant_job_question_answer($reqData->applicant_id);
            } elseif ($reqData->stage_number == 2) {

                $data['phone_interview_classification'] = $this->Recruitment_applicant_model->get_applicant_phone_interview_classification($reqData->applicant_id);
            } elseif ($reqData->stage_number == 3) {

                $group_interview = $this->Recruitment_applicant_model->get_applicant_group_or_cab_interview_details($reqData->applicant_id, 3);

                $data['group_interview'] = $group_interview['current_interview'];
                $data['history_group_interview'] = $group_interview['history_interview'];
            } elseif ($reqData->stage_number == 4) {
                $data['applicant_document_cat'] = $this->Recruitment_applicant_model->get_applicant_mandatory_doucment_list($reqData->applicant_id);

                // for pay rates
                $this->load->model('Recruitment_dashboard_model');
                $res = $this->Recruitment_dashboard_model->pay_scale_approval_work_area_options();

                $data['work_area_option'] = $res['work_area'];
                $data['pay_point_option'] = $res['pay_point'];
                $data['pay_level_option'] = $res['pay_level'];

                // get applicant pay rates
                $pay_scl = $this->Recruitment_applicant_model->get_applicant_pay_scale_approval_data($reqData->applicant_id);
                $data['pay_scale_details'] = $pay_scl['pay_scale_details'];
                $data['pay_scale_approval'] = $pay_scl['pay_scale_approval'];
            } elseif ($reqData->stage_number == 6) {
                $cab_day = $this->Recruitment_applicant_model->get_applicant_group_or_cab_interview_details($reqData->applicant_id, 6);

                $data['cab_day_interview'] = $cab_day['current_interview'];
                $data['history_cab_day_interview'] = $cab_day['history_interview'];
            }


            echo json_encode(['status' => true, 'data' => $data]);
        } else {
            echo json_encode(['status' => false, 'error' => 'Applicant id is required or stage_number is required']);
        }
    }

    function request_for_pay_scal_approval() {
        require_once APPPATH . 'Classes/recruitment/ActionNotification.php';
        $reqestData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'pay_scale', 'label' => 'applications', 'rules' => 'callback_check_pay_scal_approval_data[' . json_encode($reqData) . ']'),
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
            );

            $this->form_validation->set_data(obj_to_arr($reqData));
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->Recruitment_applicant_model->request_for_pay_scal_approval($reqData, $reqestData->adminId);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $reqestData->adminId);

                $this->loges->setTitle('Recruiter send data for pay scale approval of applicant: ' . $applicantName);
                $this->loges->setSpecific_title($adminName . ' recruiter requested for pay scale approval');
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                // set action notificaiton.
                $objAction = new ActionNotification();
                $objAction->setApplicant_id($reqData->applicant_id);
                $objAction->setRecruiterId($reqestData->adminId);
                $objAction->setAction_type(2);
                $objAction->createAction();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    function check_pay_scal_approval_data($d, $reqData) {
        $reqData = json_decode($reqData, true);

        // first check pay scale approval aready submitted
        $res = $this->basic_model->get_row('recruitment_applicant_pay_point_approval', ['id', 'status'], ['applicant_id' => $reqData['applicant_id']]);
        if (!empty($res)) {
            $this->form_validation->set_message('check_pay_scal_approval_data', 'Pay scale data already requested for approval');
            return false;
        }

        // check pay scal approval not empty
        if (!empty($reqData['pay_scale_approval'])) {
            foreach ($reqData['pay_scale_approval'] as $val) {
                if (empty($val['work_area'])) {
                    $this->form_validation->set_message('check_pay_scal_approval_data', 'Please provide work area');
                    return false;
                } elseif (empty($val['pay_point'])) {
                    $this->form_validation->set_message('check_pay_scal_approval_data', 'Please provide pay point');
                    return false;
                } elseif (empty($val['pay_level'])) {
                    $this->form_validation->set_message('check_pay_scal_approval_data', 'Please provide pay level');
                    return false;
                }
            }

            // get all work area for check unique
            $work_areas = array_column($reqData['pay_scale_approval'], 'work_area');
            $uniqe_work_areas = array_unique($work_areas);

            // check existing work area or unique work area array is equal or not
            if (count($work_areas) != count($uniqe_work_areas)) {
                $this->form_validation->set_message('check_pay_scal_approval_data', 'Please select unique work area');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_pay_scal_approval_data', 'pay scal approval data is required');
            return false;
        }
    }

    function update_applicant_reference_status_note() {
        require_once APPPATH . 'Classes/recruitment/ActionNotification.php';
        $reqestData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'reference_id', 'label' => 'reference id', 'rules' => 'required'),
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'status', 'label' => 'status', 'rules' => 'required'),
            );

            $this->form_validation->set_data(obj_to_arr($reqData));
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->Recruitment_applicant_model->update_applicant_reference_status_note($reqData);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $reqestData->adminId);

                $this->loges->setTitle('Update applicant reference of applicant: ' . $applicantName);
                $this->loges->setSpecific_title('Reference updated by admin ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    function update_applicant_details() {
        require_once APPPATH . 'Classes/recruitment/ActionNotification.php';
        $reqestData = request_handler('access_recruitment');
        $this->loges->setCreatedBy($reqestData->adminId);
        $reqData = $reqestData->data;

        if (!empty($reqData)) {
            $reqData->applicant_rule = json_encode(['type_of_checks' => ['not_flagged', 'not_flagged_pending', 'not_duplicate', 'not_duplicate_pending', 'not_hired'], 'applicant_id' => $reqData->id, 'extra_params' => []]);

            $validation_rules = array(
                array('field' => 'id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'firstname', 'label' => 'firstname', 'rules' => 'required|max_length[30]'),
                array('field' => 'lastname', 'label' => 'lastname', 'rules' => 'required|max_length[30]'),
                array('field' => 'phones[]', 'label' => 'phone', 'rules' => 'callback_phone_number_check[phone,required,Please enter valid mobile number.]|callback_check_phone_already_exist_to_another_applicant[' . $reqData->id . ']'),
                array('field' => 'emails[]', 'label' => 'email', 'rules' => 'callback_check_valid_email_address[email]|callback_check_email_already_exist_to_another_applicant[' . $reqData->id . ']'),
                array('field' => 'references[]', 'label' => 'references', 'rules' => 'callback_check_recruitment_applicant_references'),
                array('field' => 'addresses[]', 'label' => 'addresses', 'rules' => 'callback_check_recruitment_applicant_address'),
                array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks'),
                array('field' => 'ex', 'label' => 'addresses', 'rules' => 'callback_check_applicant_duplicate_profile[' . $reqData->id . ']'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->Recruitment_applicant_model->update_applicant_details($reqData);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->id);
                $adminName = $this->username->getName('admin', $reqestData->adminId);

                $this->loges->setTitle('Applicant updated - : ' . $applicantName);
                $this->loges->setSpecific_title('Applicant updated by ' . $adminName);
                $this->loges->setUserId($reqData->id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    /*
     * function upload_attachment_docs
     * return type json 
     */

    public function upload_attachment_docs() {
        // handle request and check permission to upload applicant docs
        $data = request_handlerFile('access_recruitment');

        // include participant docs class
        require_once APPPATH . 'Classes/recruitment/ApplicantDocs.php';
        require_once APPPATH . 'Classes/recruitment/ActionNotification.php';
        $docObj = new ClassApplicantDocs\ApplicantDocs();

        // set requested data for validate
        $this->form_validation->set_data((array) $data);

        // make validation rule
        $validation_rules = array(
            array('field' => 'docsTitle', 'label' => 'Title', 'rules' => 'required'),
            array('field' => 'applicantId', 'label' => 'Applicant id', 'rules' => 'required'),
            array('field' => 'currentStage', 'label' => 'Stage', 'rules' => 'required'),
            array('field' => 'docsCategory', 'label' => 'Docs Category', 'rules' => 'required'),
            array('field' => 'stageMain', 'label' => 'Stage', 'rules' => 'required'),
        );
        if (empty($_FILES['docsFile']['name'])) {
            $validation_rules[] = array('field' => 'docsFile', 'label' => 'Document File', 'rules' => 'required');
        }

        // set rules form validation
        $this->form_validation->set_rules($validation_rules);

        // check requested data return true or false
        if ($this->form_validation->run()) {
            if (!empty($_FILES) && $_FILES['docsFile']['error'] == 0) {

                $docObj->setFilename($_FILES['docsFile']['name']);
                $docObj->setApplicantId($data->applicantId);
                $docObj->setTitle($data->docsTitle);
                $docObj->setCreated(DATE_TIME);
                $docObj->setCreatedBy($data->adminId);

                $docObj->setStage($data->currentStage);
                $docObj->setArchive(0);
                $docObj->setIsMainStage(($data->stageMain == 'true' ? 1 : 0));
                $docObj->setCategoryType($data->docsCategory);
                $dub_result = $docObj->checkDublicateDocs(); // check its duplicate
                $config['upload_path'] = APPLICANT_ATTACHMENT_UPLOAD_PATH; // user here constact for specific path
                $config['input_name'] = 'docsFile';
                $config['remove_spaces'] = false;
                $config['directory_name'] = $data->applicantId;
                $config['allowed_types'] = DEFAULT_ATTACHMENT_UPLOAD_TYPE; //'jpg|jpeg|png|xlx|xls|doc|docx|pdf|pages';

                $is_upload = do_upload($config); // upload file
                // check here file is uploaded or not return key error true
                if (isset($is_upload['error'])) {
                    // return error comes in file uploading
                    echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                    exit();
                } else {
                    $docObj->setFilename($is_upload['upload_data']['file_name']);
                    $docObj->createFileData(); // insert data related doc
                    $this->loges->setCreatedBy($data->adminId);
                    $applicantName = $this->username->getName('applicant', $data->applicantId);
                    $adminName = $this->username->getName('admin', $data->adminId);
                    $this->loges->setTitle('Applicant attachment added - : ' . $applicantName);
                    $this->loges->setSpecific_title('Applicant attachment added by ' . $adminName);
                    $this->loges->setUserId($data->applicantId);
                    $this->loges->setDescription(json_encode($data));
                    $this->loges->createLog();
                    if (!$dub_result['status']) {
                        $return = array('status' => true, 'warn' => $dub_result['warn']);
                    } else {
                        $return = array('status' => true);
                    }
                }
            } else {
                $return = array('status' => false, 'error' => 'Please select a file to upload');
            }
        } else {
            // return error else data data not valid
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
        }

        echo json_encode($return);
        exit();
    }

    public function get_attachment_category_details() {
        $data = request_handler('access_recruitment');
        if (!empty($data->adminId)) {
            $res = $this->basic_model->get_record_where('recruitment_job_requirement_docs', ['id as value', 'title as label'], ['archive' => 0]);
        } else {
            $res = [];
        }
        echo json_encode(['status' => true, 'data' => $res]);
        exit();
    }

    public function get_all_attachments_by_applicant_id() {
        $reqestData = request_handler('access_recruitment');
        if (!empty($reqestData->data)) {
            $applicantId = $reqestData->data->id;
            $res = $this->Recruitment_applicant_model->get_all_attachments_by_applicant_id($applicantId);
        } else {
            $res = [];
        }
        echo json_encode(['status' => true, 'data' => $res]);
        exit();
    }

    public function download_selected_file() {
        $request = request_handler('access_recruitment');
        $responseAry = $request->data;
        if (!empty($responseAry)) {
            $this->load->library('zip');

            $applicantId = $responseAry->applicantId;
            $download_data_id = (array) $responseAry->downloadData;
            $download_data = $this->Recruitment_applicant_model->get_attachment_details_by_ids(array_keys($download_data_id), $applicantId);
            $this->zip->clear_data();

            $x = '';
            $file_count = 0;
            if (!empty($download_data)) {
                $zip_name = time() . '_' . $applicantId . '.zip';
                foreach ($download_data as $file) {
                    $file_path = APPLICANT_ATTACHMENT_UPLOAD_PATH . $applicantId . '/' . $file->filename;
                    $this->zip->read_file($file_path, FALSE);
                    $file_count = 1;
                }
                $x = $this->zip->archive('archieve/' . $zip_name);
            }


            if ($x && $file_count == 1) {
                echo json_encode(array('status' => true, 'zip_name' => $zip_name));
                exit();
            } else {
                echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
                exit();
            }
        }
    }

    public function archive_selected_file() {
        $request = request_handler('access_recruitment');
        $responseAry = $request->data;
        if (!empty($responseAry)) {
            $applicantId = $responseAry->applicantId;
            $archived_data_id = array_keys((array) $responseAry->archiveData);
            $result = $this->Recruitment_applicant_model->archived_attachment_details_by_ids($archived_data_id, $applicantId);

            if ($result) {
                echo json_encode(array('status' => true, 'msg' => 'Selectd attachment has been successfully archived.'));
                exit();
            } else {
                echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
                exit();
            }
        }
    }

    public function approve_deny_applicant_doc_category() {
        $request = request_handler('access_recruitment');
        $this->loges->setCreatedBy($request->adminId);
        $reqData = $request->data;

        if (!empty($reqData)) {

            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'applicant_doc_cat_id', 'label' => 'applicant doc catogory Id', 'rules' => 'required'),
                array('field' => 'action', 'label' => 'action', 'rules' => 'required'),
                array('field' => 'doc_cat', 'label' => 'doc cat', 'rules' => 'required'),
            );
            if(isset($reqData->detailId)){
                $reqData->applicant_rule = json_encode(['type_of_checks' => ['cab_day_interview_permission_edit'],
                'applicant_id' => $reqData->applicant_id,
                'extra_params' => ['taskIdOrCabdayDetailId' =>  $reqData->detailId,'adminId'=>$request->adminId,'type'=>'detailId']]);
                $validation_rules[]=array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks');
            }

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $action = $reqData->action == 2 ? 2 : 1;
                $update_data = ['is_approved' => $action];

                $this->basic_model->update_records('recruitment_applicant_doc_category', $update_data, ['id' => $reqData->applicant_doc_cat_id]);
                if (isset($reqData->attachment_id) && !empty($reqData->attachment_id)) {
                    $update_data_attachment = ['document_status' => $action];
                    if ($action == 2) {
                        $update_data_attachment['archive'] = 1;
                    }
                    $this->basic_model->update_records('recruitment_applicant_stage_attachment', $update_data_attachment, ['applicant_id' => $reqData->applicant_id, 'uploaded_by_applicant' => 1, 'id' => $reqData->attachment_id]);
                }

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $request->adminId);


                $txt = $reqData->doc_cat . " document is " . (($action == 1) ? "approved" : "Rejected");
                $this->loges->setTitle($applicantName . ' - ' . 'Applicant ' . $txt . 'document');
                $this->loges->setSpecific_title($txt . ' updated by ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    public function update_mandatory_optional_docs_for_applicant() {
        $request = request_handler('access_recruitment');
        $this->loges->setCreatedBy($request->adminId);
        $reqData = $request->data;

        if (!empty($reqData)) {
            $applicant_document_cat = isset($reqData->applicant_document_cat) ? json_encode($reqData->applicant_document_cat) : [];
            $validation_rules = array(
                array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'),
                array('field' => 'applicant_document', 'label' => 'applicant_document_cat', 'rules' => 'callback_check_applicant_assign_document_category[' . $applicant_document_cat . ']'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $this->Recruitment_applicant_model->update_mandatory_optional_docs_for_applicant($reqData);

                // set log details
                $applicantName = $this->username->getName('applicant', $reqData->applicant_id);
                $adminName = $this->username->getName('admin', $request->adminId);



                $this->loges->setTitle('Updated Mandatory document category list of applicant - ' . $applicantName);
                $this->loges->setSpecific_title('Updated Mandatory document category list by ' . $adminName);
                $this->loges->setUserId($reqData->applicant_id);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    function save_application_stage_note() {
        $reqData = request_handler('access_recruitment');
        $adminId = $reqData->adminId;
        $this->loges->setCreatedBy($reqData->adminId);
        $reqData = $reqData->data;
        $reqData->recruiterId = $adminId;

        if (!empty($reqData)) {
            $validation_rules = array(
                array('field' => 'is_main_stage', 'label' => 'Stage', 'rules' => 'required'),
                array('field' => 'current_stage', 'label' => 'Stage', 'rules' => 'required'),
                array('field' => 'stage', 'label' => 'Stage', 'rules' => 'required'),
                array('field' => 'note', 'label' => 'Note', 'rules' => 'required'),
                array('field' => 'applicantId', 'label' => 'Applicant', 'rules' => 'required')
            );
            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == TRUE) {
                $return = $this->Recruitment_applicant_model->save_application_stage_note($reqData);
                if (isset($return['status']) && $return['status']) {

                    // set log details
                    $applicantName = $this->username->getName('applicant', $reqData->applicantId);
                    $adminName = $this->username->getName('admin', $adminId);

                    $this->loges->setUserId($reqData->applicantId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Applicant - ' . $applicantName . ' stage-' . $reqData->stage . ' note added on applicant info screen');
                    $this->loges->setSpecific_title('Stage-' . $reqData->stage . ' note added on applicant info by ' . $adminName);
                    $this->loges->createLog();
                }
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } else {
            $return = array('status' => false, 'error' => 'Something went wrong.');
        }
        echo json_encode($return);
    }

    public function get_attachment_stage_notes_by_applicant_id() {
        $reqestData = request_handler('access_recruitment');
        if (!empty($reqestData->data)) {
            $applicantId = $reqestData->data->id;
            $res = $this->Recruitment_applicant_model->get_attachment_stage_notes_by_applicant_id($applicantId);
        } else {
            $res = [];
        }
        echo json_encode(['status' => true, 'data' => $res]);
        exit();
    }

    public function get_lastupdate_by_applicant_id() {
        $reqestData = request_handler('access_recruitment');
        if (!empty($reqestData->data)) {
            $applicantId = $reqestData->data->id;
            $res = $this->Recruitment_applicant_model->get_applicant_last_update($applicantId);
        } else {
            $res = [];
        }
        echo json_encode(['status' => true, 'data' => $res]);
        exit();
    }
    
    public function resend_applicant_docusign_contract(){
        $reqestData = request_handler('access_recruitment');

        if (!empty($reqestData->data)) {
            $reqData=$reqestData->data;
                       
                $validation_rules = array(array('field' => 'task_applicant_id', 'label' => 'task applicant id', 'rules' => 'required'),
                    array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'));
                $this->form_validation->set_data((array) $reqData);
                $this->form_validation->set_rules($validation_rules);
    
                if ($this->form_validation->run() == TRUE) {
                    
                   $res_send_contract=$this->resend_applicant_docusign_user_contract($reqData);
                   if($res_send_contract['status']){
                        echo json_encode(['status' => true, 'msg'=>'applicant contract send successfully']);
                        exit();
                   }else{
                        echo json_encode($res_send_contract);
                        exit();
                   }

                } else {
                    $errors = $this->form_validation->error_array();
                    echo json_encode(['status' => false, 'error' => implode(', ', $errors)]);
                    exit();
                }
            } else {
                echo json_encode(['status' => false, 'error' => 'Something went wrong.']);
                exit();
        }
    }

    function send_reminder_sms_for_docusign() {
        $reqestData = request_handler('access_recruitment');

        if (!empty($reqestData->data)) {
            $reqData=$reqestData->data;
                       
                $validation_rules = array(array('field' => 'task_applicant_id', 'label' => 'task applicant id', 'rules' => 'required'),
                    array('field' => 'applicant_id', 'label' => 'applicant id', 'rules' => 'required'));
                $this->form_validation->set_data((array) $reqData);
                $this->form_validation->set_rules($validation_rules);
    
                if ($this->form_validation->run() == TRUE) {
                    
                   $res_send_contract=$this->resend_applicant_docusign_user_contract($reqData);
                   if($res_send_contract['status']){

                        $request_params['to_email']=$res_send_contract['data']['email'];
                        $request_params['subject']=$res_send_contract['data']['applicant_name'].' Applicant contract send successfully';
                        $request_params['body']=$res_send_contract['data']['applicant_name'].' Applicant contract send successfully';
                        // send_msg_mail
                        $this->load->library('Sms');
                        $msg_response=$this->sms->send_msg_mail($request_params);
                        if($msg_response){
                            echo json_encode(['status' => true, 'msg'=>'Applicant contract send successfully']);
                            exit();
                        }else{
                            // sms funcnality close send only mail
                            echo json_encode(['status' => true, 'msg'=>'Applicant contract send successfully']);
                            exit();
                        }
                        
                   }else{
                        echo json_encode($res_send_contract);
                        exit();
                   }

                } else {
                    $errors = $this->form_validation->error_array();
                    echo json_encode(['status' => false, 'error' => implode(', ', $errors)]);
                    exit();
                }
            } else {
                echo json_encode(['status' => false, 'error' => 'Something went wrong.']);
                exit();
        }
    }
    

    function add_applicant_app_onboarding() {
        $reqestData = request_handler('access_recruitment');
        $reqData = $reqestData->data;
        $type = $reqData->type;
        $status = $reqData->status;
        $text = $type=='orientation' ? 'orientation': 'onboarding';

        if (!empty($reqData->task_applicant_id) && in_array($type,['orientation','onboarding']) && in_array($status,['1','2'])) {
            if(isset($reqData->requestBy) && $reqData->requestBy=='cabday' && isset($reqData->applicant_id)){
                $reqData->applicant_rule = json_encode(['type_of_checks' => ['cab_day_interview_permission_edit'],
                'applicant_id' => $reqData->applicant_id,
                'extra_params' => ['type'=>'taskApplicantId','taskIdOrCabdayDetailId' =>  $reqData->task_applicant_id,'adminId'=>$reqestData->adminId]]);
                $validation_rules = array(
                    array('field' => 'task_applicant_id', 'label' => 'Task applicant id', 'rules' => 'required'),
                    array('field' => 'applicant_rule', 'label' => 'addresses', 'rules' => 'callback_check_applicant_required_checks')
                );
    
                $this->form_validation->set_data((array) $reqestData->data);
                $this->form_validation->set_rules($validation_rules);

                if (!$this->form_validation->run()) {
                    $errors = $this->form_validation->error_array();
                    $response = ['status' => false, 'error' => implode(', ', $errors)];
                    echo json_encode($response);
                    exit;
                }
            }

            $app_orientation =  $this->Recruitment_applicant_model->check_applicant_onboarding_eligibility($reqData->task_applicant_id,$reqData->type);

            if (empty($app_orientation) && $app_orientation != 0) {

                $return = ['status' => false, 'error' => 'Applicant not on stage for add '.$text];
            } elseif ($app_orientation > 0) {

                $return = ['status' => false, 'error' => 'Already update app '.$text];
            } elseif ($app_orientation == 0) {

                if($type=='orientation'){
                    $this->Recruitment_applicant_model->add_applicant_orientation($reqData->task_applicant_id,$status);
                    $return = ['status' => true];
                }else if($type=='onboarding'){
                    $this->Recruitment_applicant_model->add_applicant_onboarding($reqData->task_applicant_id,$status);
                    $return = ['status' => true];
                }else{
                    $return = ['status' => false,'error' => 'invalid request.'];
                }

                
            }
        } else {
            $return = ['status' => false, 'error' => 'Task applicant id not found'];
        }

        echo json_encode($return);
    }

    private function docuSign_sign_position(){
        $position=array();
        $position['position_x']=100;
        $position['position_y']=100;
        $position['document_id']=1;
        $position['page_number']=1;
        $position['recipient_id']=1;
        return $position;
    }

    function resend_applicant_docusign_user_contract($Data) {
                    
            $position=$this->docuSign_sign_position();
            $response_user_details = $this->Recruitment_applicant_model->resend_docusign_enevlope($Data);

            if (!empty($response_user_details)) {
                $validation_rules_user = array(array('field' => 'applicant_name', 'label' => 'applicant name', 'rules' => 'required'),
                    array('field' => 'email', 'label' => 'email id', 'rules' => 'required'),
                    array('field' => 'envelope_id', 'label' => 'envelope id', 'rules' => 'required'));

                    $this->form_validation->set_data($response_user_details);
                    $this->form_validation->set_rules($validation_rules_user);
        
                    if ($this->form_validation->run() == TRUE) {

                        $this->load->library('DocuSignEnvelope');
                        $signerDetails=array();
                        $signerDetails['name']=$response_user_details['applicant_name'];
                       // $signerDetails['email_subject']='cabday interview contract document';
                        $signerDetails['email']=$response_user_details['email'];
                        
                        $envlopDetails=array();
                        $envlopDetails['userdetails']=$signerDetails;			
                        $envlopDetails['position']=$position;			
                        $envlopDetails['envelopeId']=$response_user_details['envelope_id'];	
                        
                        $statusDocuSign=$this->docusignenvelope->ResendEnvelope($envlopDetails);
                       // var_dump($statusDocuSign);
                        if(isset($statusDocuSign['status']) && $statusDocuSign['status']){
                           // $envId = $response_user_details['envelope_id'];
                            //$this->basic_model->update_records('recruitment_applicant_contract', array('signed_status'=>1),array('envelope_id'=>$envId,'archive'=>0));
                            return ['status' => true, 'msg'=>'applicant contract send successfully','data'=>$response_user_details];
                        }else{
                            return ['status' => false,'error' =>json_encode($statusDocuSign)]; // 'applicant contract not send.'];                                        
                        }
                    }else{
                        $errors_details = $this->form_validation->error_array();
                        return ['status' => false, 'error' => implode(', ', $errors_details)];
                        
                    }
            }else{
                return ['status' => false, 'error' => 'Employment Contract not exist.'];                
        }
    }
}
