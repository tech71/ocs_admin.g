<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OrgDashboard extends MX_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('Org_model');
        $this->load->model('Basic_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->loges->setLogType('organization');
    }

    public function get_org() {
        $reqData = request_handler('access_organization');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Org_model->get_org($reqData);
            echo json_encode($result);
        }
    }

    public function get_abn_name() {
        $reqData = request_handler('access_organization');
        $reqData->data = json_decode($reqData->data);
        $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
        $this->load->library('abn_search');
        $srch_record = $this->abn_search->search_abn_by_name($post_data);
        $rows = array();
        if (!empty($srch_record)) {
            $srch_record = str_replace('callback(', '', $srch_record);
            $srch_record = substr($srch_record, 0, strlen($srch_record) - 1); //strip out last paren
            $object = json_decode($srch_record); // stdClass object
            if (!empty($object->Names)) {
                foreach ($object->Names as $val) {
                    $rows[] = array('label' => $val->Name, 'value' => $val->Name, 'abn' => $val->Abn);
                }
            }
        }
        echo json_encode($rows);
    }

    public function get_org_contact() {
        $reqData = request_handler('access_organization');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Org_model->get_org_contact($reqData);
            echo json_encode($result);
        }
    }

    public function get_organisation_profile() {
        require_once APPPATH . 'Classes/organisation/Organisation.php';
        $objOrg = new OrganisationClass\Organisation();
        $request = request_handler('access_organization');
        if (!empty($orgId = $request->data)) {
            if (isset($request->data->subOrgId)) {
                $orgId = $request->data->id;
                $objOrg->setParent_org($orgId);
                $sub_orgid = $request->data->subOrgId;
                $objOrg->setId($sub_orgid);
            } else {
                $objOrg->setParent_org('');
                $orgId = $request->data->id;
                $objOrg->setId($orgId);
            }
        }
        $row = $objOrg->get_organisation_profile();
        echo json_encode($row);
        exit();
    }

    public function organisation_portal_access() {
        if (!empty(request_handler('access_organization'))) {
            $request = request_handler('access_organization');
            $status = $request->data->status;
            $orgId = $request->data->orgId;
            $data = array('enable_portal_access' => $status);
            $where = array('id' => $orgId);
            $this->Basic_model->update_records('organisation', $data, $where);
            echo json_encode(array('status' => true));
            exit();
        }
    }

    public function change_status() {
        if (!empty(request_handler('update_organization'))) {
            $request = request_handler('update_organization');
            $status = $request->data->status;
            $orgId = $request->data->orgId;
            $data = array('status' => $status);
            $where = array('id' => $orgId);
            $this->Basic_model->update_records('organisation', $data, $where);
            echo json_encode(array('status' => true));
            exit();
        }
    }

    public function get_organisation_about() {
        require_once APPPATH . 'Classes/organisation/Organisation.php';
        $objOrg = new OrganisationClass\Organisation();
        $request = request_handler('access_organization');
        $orgId = $request->data->orgId;
        $objOrg->setId($orgId);
        $row = $objOrg->get_organisation_about();
        echo json_encode(array('status' => true, 'data' => $row));
        exit();
    }

    public function get_sub_org() {
        require_once APPPATH . 'Classes/organisation/Organisation.php';
        $objOrg = new OrganisationClass\Organisation();
        $request = request_handler('access_organization');
        $reqData = json_decode($request->data);
        $orgId = $reqData->orgId;
        $orgStatus = $reqData->filtered->status;
        $orgSrch_box = $reqData->filtered->srch_box;
        $limit = isset($reqData->pageSize) ? $reqData->pageSize : 10;
        $page = isset($reqData->page) ? $reqData->page : 0;


        if (isset($orgSrch_box))
            $objOrg->settext_search($orgSrch_box);

        $objOrg->setLimit($limit);
        $objOrg->setPage($page);



        $objOrg->setId($orgId);
        $objOrg->setStatus($orgStatus);
        $row = $objOrg->get_sub_org();
        if (isset($row['data']) && !empty($row['data'])) {
            echo json_encode(array('status' => true, 'data' => $row['data'], 'count' => $row['count']));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => [], 'count' => $row['count']));
            exit();
        }
    }

    public function get_org_sites() {
        require_once APPPATH . 'Classes/organisation/Sites.php';
        $objOrg = new SitesClass\Sites();
        $request = request_handler('access_organization');
        $reqData = json_decode($request->data);

        $orgId = json_decode($request->data)->orgId;
        $objOrg->setId($orgId);
        $orgIsArchive = json_decode($request->data)->filtered->archive;
        $objOrg->setArchive($orgIsArchive);
        $siteSrch_box = json_decode($request->data)->filtered->srch_box;

        $limit = isset($reqData->pageSize) ? $reqData->pageSize : 10;
        $page = isset($reqData->page) ? $reqData->page : 0;

        if (isset($siteSrch_box))
            $objOrg->settext_search($siteSrch_box);

        $objOrg->setLimit($limit);
        $objOrg->setPage($page);

        $row = $objOrg->get_org_sites();

        if (isset($row['data']) && !empty($row['data'])) {
            echo json_encode(array('status' => true, 'data' => $row['data'], 'count' => $row['count']));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => [], 'count' => $row['count']));
            exit();
        }
    }

    public function update_sub_org() {
        $request = request_handler('update_organization');
        $data = $request->data;
        $org_data = (array) $request->data;
        if (!empty($data)) {
            $validation_rules = array(
                array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
                //array('field' => 'key_contact_label', 'label' => 'Key contact', 'rules' => 'required'),
                //array('field' => 'position', 'label' => 'Position', 'rules' => 'required'),
                array('field' => 'street', 'label' => 'Street', 'rules' => 'required'),
                array('field' => 'city', 'label' => 'Suburb', 'rules' => 'callback_suburb_check[value,required, Suburb can not be empty.]'),
                array('field' => 'state', 'label' => 'State', 'rules' => 'required'),
                array('field' => 'postal', 'label' => 'Postcode', 'rules' => 'required|callback_postal_code_check[postal]'),
                array('field' => 'OrganisationEmail[]', 'label' => 'Email', 'rules' => 'callback_check_email_address'),
                array('field' => 'OrganisationPh[]', 'label' => 'Phone', 'rules' => 'callback_phone_number_check[phone,required,Contact should be enter valid phone number.]'),
            );

            $this->form_validation->set_data($org_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                /* logs */
                $this->loges->setUserId($request->adminId);
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->setTitle("Update Sub-Org: " . $org_data['ocs_id']);
                $this->loges->setDescription(json_encode($data));
                $this->loges->createLog();

                $this->Org_model->update_sub_org($org_data);
                $return = array('status' => true, 'msg' => 'Organisation updated successfully.');
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function add_house_contact() {
        $request = request_handler('update_organization');
        $data = $request->data;
        $house_data = (array) $request->data;

        if (!empty($data)) {
            $validation_rules = array(
                array('field' => 'firstname', 'label' => 'First name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Position', 'rules' => 'required'),
                array('field' => 'department', 'label' => 'Department', 'rules' => 'required'),
                array('field' => 'contact_type', 'label' => 'Contact type', 'rules' => 'required'),
                array('field' => 'OrganisationPh[]', 'label' => 'Contact number', 'rules' => 'callback_phone_number_check[phone,required,Contact should be enter valid phone number.]'),
                array('field' => 'OrganisationEmail[]', 'label' => 'Email', 'rules' => 'callback_check_email_address'),
            );

            $this->form_validation->set_data($house_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                /* logs */
                $this->loges->setUserId($request->adminId);
                $this->loges->setCreatedBy($request->adminId);

                if (isset($house_data['edit_mode']) && $house_data['edit_mode'] == 1)
                    $this->loges->setTitle("Update house Contact: " . $house_data['orgId'] . ", houseId: " . $house_data['houseId']);
                else
                    $this->loges->setTitle("Add house Contact: " . $house_data['orgId'] . ", houseId: " . $house_data['houseId']);

                $this->loges->setDescription(json_encode($data));
                $this->loges->createLog();

                require_once APPPATH . 'Classes/organisation/Organisation_site_all_contact.php';
                $objOrg = new Organisation_site_all_contactClass\Organisation_all_contact();
                $objOrg->setsiteId($house_data['houseId']);
                $objOrg->setName($house_data['firstname']);
                $objOrg->setLastname($house_data['lastname']);
                $objOrg->setPosition($house_data['position']);
                $objOrg->setDepartment($house_data['department']);
                $objOrg->setType($house_data['contact_type']);

                if (isset($house_data['edit_mode']) && $house_data['edit_mode'] == 1) {
                    $objOrg->setId($house_data['id']);
                    $objOrg->update_site_contact();
                    $contact_id = $house_data['id'];
                    $msg = 'Contact updated successfully.';
                } else {
                    $contact_id = $objOrg->add_site_contact();
                    $msg = 'Contact added successfully.';
                }
                $this->Org_model->add_house_contact($house_data, $contact_id);
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function get_org_fms() {
        $reqData = request_handler('access_fms');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Org_model->get_org_fms($reqData);
            echo json_encode($result);
        }
    }

    public function add_org_contact() {
        $request = request_handler('update_organization');
        $data = $request->data;
        $org_data = (array) $request->data;

        if (!empty($data)) {
            $validation_rules = array(
                array('field' => 'firstname', 'label' => 'First name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Position', 'rules' => 'required'),
                array('field' => 'department', 'label' => 'Department', 'rules' => 'required'),
                array('field' => 'contact_type', 'label' => 'Contact type', 'rules' => 'required'),
                array('field' => 'OrganisationPh[]', 'label' => 'Contact number', 'rules' => 'callback_phone_number_check[phone,required,Contact should be enter valid phone number.]'),
                    //array('field' => 'email', 'label' => 'Email', 'rules' => 'required|valid_email'),
            );

            $this->form_validation->set_data($org_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                /* logs */
                $this->loges->setUserId($request->adminId);
                $this->loges->setCreatedBy($request->adminId);

                if (isset($org_data['edit_mode']) && $org_data['edit_mode'] == 1)
                    $this->loges->setTitle("Update site Contact: " . $org_data['orgId']);
                else
                    $this->loges->setTitle("Add site Contact: " . $org_data['orgId']);

                $this->loges->setDescription(json_encode($data));
                $this->loges->createLog();

                require_once APPPATH . 'Classes/organisation/Organisation_all_contact.php';
                $objOrg = new Organisation_all_contactClass\Organisation_all_contact();
                $objOrg->setOrganisationId($org_data['orgId']);
                $objOrg->setName($org_data['firstname']);
                $objOrg->setLastname($org_data['lastname']);
                $objOrg->setPosition($org_data['position']);
                $objOrg->setDepartment($org_data['department']);
                $objOrg->setType($org_data['contact_type']);

                if (isset($org_data['edit_mode']) && $org_data['edit_mode'] == 1) {
                    $objOrg->setId($org_data['id']);
                    $objOrg->update_org_contact();
                    $contact_id = $org_data['id'];
                    $msg = 'Contact updated successfully.';
                } else {
                    $contact_id = $objOrg->add_org_contact();
                    $msg = 'Contact added successfully.';
                }
                $this->Org_model->add_org_contact($org_data, $contact_id);
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function get_org_docs() {
        $request = request_handler('access_organization');
        $row = $this->Org_model->get_org_docs($request);
        if (!empty($row)) {
            echo json_encode(array('status' => true, 'data' => $row));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => array()));
            exit();
        }
    }

    public function upload_sub_org_docs() {
        $request = request_handlerFile('update_organization');
        $request_body = file_get_contents('php://input', true);
        $request_body = json_decode($request_body);
        #pr($request);
        if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {
            $config['upload_path'] = ORG_UPLOAD_PATH;
            $config['input_name'] = 'myFile';
            $config['directory_name'] = $this->input->post('subOrgId');
            $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

            $is_upload = do_upload($config);

            if (isset($is_upload['error'])) {
                echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                exit();
            } else {
                $insert_ary = array('filename' => $is_upload['upload_data']['file_name'],
                    'organisationId' => $this->input->post('subOrgId'),
                    'title' => $this->input->post('docsTitle'),
                    'expiry' => date('Y-m-d', strtotime($this->input->post('expiry'))),
                    'created' => DATE_TIME,
                    'archive' => '0',
                );
                $rows = $this->Basic_model->insert_records('organisation_docs', $insert_ary, $multiple = FALSE);

                //
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->setUserId($this->input->post('subOrgId'));
                $this->loges->setDescription(json_encode($request));
                $this->loges->setTitle('Added New document by Sub-Org: ' . $this->input->post('subOrgId'));
                $this->loges->createLog();

                if (!empty($rows)) {
                    echo json_encode(array('status' => true));
                    exit();
                } else {
                    echo json_encode(array('status' => false));
                    exit();
                }
            }
        } else {
            echo json_encode(array('status' => false, 'error' => 'Please select a file to upload'));
            exit();
        }
    }

    public function download_selected_file() {
        $request = request_handler('access_organization');
        $responseAry = $request->data;
        $this->load->library('zip');
        $org_id = $responseAry->org_id;
        $download_data = $responseAry->downloadData;
        $this->zip->clear_data();
        $x = '';
        $file_count = 0;
        if (!empty($download_data)) {
            $zip_name = time() . '_' . $org_id . '.zip';
            foreach ($download_data as $file) {
                if (isset($file->is_active) && $file->is_active) {
                    $file_path = HOUSE_UPLOAD_PATH . $org_id . '/' . $file->filename;
                    $this->zip->read_file($file_path, FALSE);
                    $file_count = 1;
                }
            }
            $x = $this->zip->archive('archieve/' . $zip_name);
        }

        if ($x && $file_count == 1) {
            echo json_encode(array('status' => true, 'zip_name' => $zip_name));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
            exit();
        }
    }

    public function archieve_selected_file() {
        $request = request_handler('delete_organization');
        $responseAry = $request->data;
        $org_id = $responseAry->org_id;
        $download_data = $responseAry->downloadData;
        $file_count = 0;
        if (!empty($download_data)) {
            foreach ($download_data as $file) {
                if (isset($file->is_active) && $file->is_active) {
                    $file_count = 1;
                    $this->Basic_model->update_records('house_docs', array('archive' => '1'), array('id' => $file->id));
                }
            }
        }
        if ($file_count == 1) {
            echo json_encode(array('status' => true));
            exit();
        } else {
            echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
            exit();
        }
    }

    public function get_house_profile() {
        require_once APPPATH . 'Classes/organisation/Sites.php';
        $objHouse = new SitesClass\Sites();
        $request = request_handler('access_organization');
        if (!empty($orgId = $request->data)) {
            $houseId = $request->data->houseId;
            $orgId = $request->data->orgId;
            $objHouse->setId($houseId);
            $objHouse->setOrganisationId($orgId);
        }
        $row = $objHouse->get_house_profile();
        echo!empty($row) ? json_encode($row) : json_encode(['status' => false, 'data' => []]);
        exit();
    }

    public function get_house_contact() {
        $reqData = request_handler('access_organization');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Org_model->get_house_contact($reqData);
            echo json_encode($result);
        }
    }

    public function get_house_fms() {
        $reqData = request_handler('access_organization');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Org_model->get_house_fms($reqData);
            echo json_encode($result);
        }
    }

    public function get_house_docs() {
        $request = request_handler('access_organization');
        $row = $this->Org_model->get_house_docs($request);
        if (!empty($row)) {
            echo json_encode(array('status' => true, 'data' => $row));
            exit();
        } else {
            echo json_encode(array('status' => false, 'data' => array()));
            exit();
        }
    }

    public function upload_house_docs() {
        $request = request_handlerFile('update_organization');
        $request_body = file_get_contents('php://input', true);
        $request_body = json_decode($request_body);
        $validation_rules = array(
            array('field' => 'docsTitle', 'label' => 'Title', 'rules' => 'trim|required'),
            array('field' => 'expiry', 'label' => 'Expiry', 'rules' => 'required')
        );
        $this->form_validation->set_data((array) $request);
        $this->form_validation->set_rules($validation_rules);
        if (!$this->form_validation->run()) {
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
            echo json_encode($return);
            exit;
        }
        #pr($request);
        if (!empty($_FILES) && $_FILES['myFile']['error'] == 0) {
            $config['upload_path'] = HOUSE_UPLOAD_PATH;
            $config['input_name'] = 'myFile';
            $config['directory_name'] = $this->input->post('houseId');
            $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

            $is_upload = do_upload($config);

            if (isset($is_upload['error'])) {
                echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                exit();
            } else {
                $insert_ary = array('filename' => $is_upload['upload_data']['file_name'],
                    'houseId' => $this->input->post('houseId'),
                    'title' => $this->input->post('docsTitle'),
                    'expiry' => date('Y-m-d', strtotime($this->input->post('expiry'))),
                    'created' => DATE_TIME,
                    'archive' => '0',
                );
                $rows = $this->Basic_model->insert_records('house_docs', $insert_ary, $multiple = FALSE);

                //
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->setUserId($this->input->post('houseId'));
                $this->loges->setDescription(json_encode($request));
                $this->loges->setTitle('Added New document by House: ' . $this->input->post('houseId'));
                $this->loges->createLog();

                if (!empty($rows)) {
                    echo json_encode(array('status' => true));
                    exit();
                } else {
                    echo json_encode(array('status' => false));
                    exit();
                }
            }
        } else {
            echo json_encode(array('status' => false, 'error' => 'Please select a file to upload'));
            exit();
        }
    }

    public function get_parent_org_name() {
        $reqData = request_handler('access_organization');
        $reqData->data = json_decode($reqData->data);
        $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
        $rows = $this->Org_model->get_parent_org_name($post_data);
        echo json_encode($rows);
    }

    public function create_org() {
        $request = request_handlerFile('update_organization');
        $org_data = (array) json_decode($request->current_state);

        if (!empty($org_data)) {
            $validation_rules = array(
                array('field' => 'organisation_abn', 'label' => 'Organisation ABN', 'rules' => 'callback_check_unique_abn'),
                array('field' => 'organisation_website', 'label' => 'Website', 'rules' => 'required|valid_url'),
                array('field' => 'key_contact_fname', 'label' => 'Key contact First Name', 'rules' => 'required'),
                array('field' => 'key_contact_lname', 'label' => 'Key contact Last Name', 'rules' => 'required'),
                array('field' => 'key_contact_position', 'label' => 'Key contact Position', 'rules' => 'required'),
                //array('field' => 'key_contact_department', 'label' => 'Key contact Department', 'rules' => 'required'),
                array('field' => 'bill_contact_fname', 'label' => 'Billing contact First Name', 'rules' => 'required'),
                array('field' => 'bill_contact_lname', 'label' => 'Billing contact Last Name', 'rules' => 'required'),
                array('field' => 'bill_contact_position', 'label' => 'Billing contact Position', 'rules' => 'required'),
                //array('field' => 'bill_contact_department', 'label' => 'Billing contact Department', 'rules' => 'required'),
                array('field' => 'OrganisationPh[]', 'label' => 'Organisation Phone', 'rules' => 'callback_check_phone_number|callback_phone_number_check[phone,required,New Organisation Contact should be enter valid phone number.]'),
                array('field' => 'OrganisationKeyContactPh[]', 'label' => 'Organisation Key contact Phone', 'rules' => 'callback_check_phone_number|callback_phone_number_check[phone,required,Key Contact should be enter valid phone number.]'),
                array('field' => 'OrganisationBillingContactPh[]', 'label' => 'Organisation Billing contact Phone', 'rules' => 'callback_check_phone_number|callback_phone_number_check[phone,required,Billing Contact should be enter valid phone number.]'),
                array('field' => 'OrganisationEmail[]', 'label' => 'Organisation Email', 'rules' => 'callback_check_email_address'),
                array('field' => 'OrganisationBillingContactEmail[]', 'label' => 'Organisation Billing Contact Email', 'rules' => 'callback_check_email_address'),
                array('field' => 'OrganisationKeyContactEmail[]', 'label' => 'Organisation Key Contact Email', 'rules' => 'callback_check_email_address'),
                array('field' => 'completeAddress[]', 'label' => 'Address', 'rules' => 'callback_check_address|callback_postal_code_check[postal_code]'),
                array('field' => 'site_org[]', 'label' => 'Site', 'rules' => 'callback_check_site_not_alloted[value,label,,is already assigned to other Organisation]'),
            );

            $this->form_validation->set_data($org_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $ocs_row = $this->Org_model->create_org($org_data);
                $img_err = isset($ocs_row['image_status']) ? strip_tags($ocs_row['image_status']) : '';
                /* logs */
                $this->loges->setTitle("Add Organisation: " . $ocs_row['org_id']);
                $this->loges->setUserId($request->adminId);
                $this->loges->setDescription(json_encode($org_data));
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->createLog();

                $return = array('status' => true, 'msg' => 'Organisation created successfully.' . $img_err, 'orgId' => $ocs_row['org_id']);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function check_phone_number($phone_numbers) {
        if (!empty($phone_numbers)) {
            foreach ($phone_numbers as $key => $val) {
                if ($key == 'primary_phone' || $key == 'id')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_phone_number', 'Phone number can not be empty');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
            return false;
        }
        return true;
    }

    public function check_email_address($email_address) {
        if (!empty($email_address)) {
            foreach ($email_address as $key => $val) {
                if ($key == 'primary_email' || $key == 'id')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_email_address', 'Email can not empty');
                    return false;
                } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                    $this->form_validation->set_message('check_email_address', 'Email is not valid');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
            return false;
        }
        return true;
    }

    public function check_address($address) {
        #pr($address);
        if (!empty($address)) {
            if (empty($address->street)) {
                $this->form_validation->set_message('check_address', 'Address can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_address', 'State can not be empty');
                return false;
            } elseif (empty($address->postal_code)) {
                $this->form_validation->set_message('check_address', 'Pincode can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_address', 'Address can not empty');
            return false;
        }
        return true;
    }

    public function update_org_booking_data() {
        if (!empty(request_handler('update_organization'))) {
            $request = request_handler('update_organization');
            $booking_status = isset($request->data->booking_status) && $request->data->booking_status == TRUE ? '1' : '0';
            $booking_date = isset($request->data->booking_status) && $request->data->booking_status == TRUE ? $request->data->booking_date : '';
            $organisationId = $request->data->organisationId;
            $data = array('booking_status' => $booking_status, 'booking_date' => $booking_date);
            $where = array('id' => $organisationId);
            $this->Basic_model->update_records('organisation', $data, $where);
            echo json_encode(array('status' => true));
            exit();
        }
    }

    public function check_unique_abn($abn_numbers) {
        if (!empty($abn_numbers)) {
            $row = $this->basic_model->get_row('organisation', array('abn'), array('abn' => $abn_numbers));
            if (!empty($row)) {
                $this->form_validation->set_message('check_unique_abn', 'This ABN number is already registered with us');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('check_unique_abn', 'ABN number can not be empty');
            return false;
        }
        return true;
    }

    public function update_org() {
        $request = request_handlerFile('update_organization');
        $org_data = (array) json_decode($request->current_state);

        if (!empty($org_data)) {
            $validation_rules = array(
                array('field' => 'payroll_tax', 'label' => 'Payroll tax', 'rules' => 'required'),
                array('field' => 'gst', 'label' => 'GST', 'rules' => 'required'),
                array('field' => 'street', 'label' => 'Street', 'rules' => 'required'),
                array('field' => 'postal', 'label' => 'Post code', 'rules' => 'required|callback_postal_code_check[postal]'),
                array('field' => 'city', 'label' => 'Suburb', 'rules' => 'callback_suburb_check[value,required, Suburb can not be empty.]'),
                array('field' => 'website', 'label' => 'Website', 'rules' => 'required|valid_url'),
                array('field' => 'OrganisationPh[]', 'label' => 'Organisation Phone', 'rules' => 'callback_check_phone_number|callback_phone_number_check[phone,required, Organisation Contact should be enter valid phone number.]'),
                array('field' => 'OrganisationEmail[]', 'label' => 'Organisation Email', 'rules' => 'callback_check_email_address'),
                    //array('field' => 'completeAddress[]', 'label' => 'Address', 'rules' => 'callback_check_address'),
            );

            $this->form_validation->set_data($org_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $ocs_row = $this->Org_model->update_org($org_data);
                $img_err = isset($ocs_row['image_status']) ? $ocs_row['image_status'] : '';
                /* logs */
                $this->loges->setTitle("Update Organisation: " . $ocs_row['org_id']);
                $this->loges->setUserId($request->adminId);
                $this->loges->setDescription(json_encode($org_data));
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->createLog();

                $return = array('status' => true, 'msg' => 'Organisation updated successfully.' . $img_err, 'orgId' => $ocs_row['org_id']);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function save_site() {
        $request = request_handler('update_organization');
        $site_data_check = (array) json_decode($request->data);
        $site_data = (array) json_decode($request->data, TRUE);

        if (!empty($site_data)) {
            $site_id = isset($site_data['site_id']) && $site_data['mode'] == 'edit' ? $site_data['site_id'] : 0;
            $validation_rules = array(
                array('field' => 'title', 'label' => 'Title', 'rules' => 'required|callback_check_unique_site_name[' . $site_id . ']'),
                //array('field' => 'site_type', 'label' => 'Type', 'rules' => 'required'),
                array('field' => 'SitePhone[]', 'label' => 'Site Phone', 'rules' => 'callback_check_phone_number|callback_phone_number_check[phone,required, Contact should be enter valid phone number.]'),
                array('field' => 'SiteEmail[]', 'label' => 'Site Email', 'rules' => 'callback_check_email_address'),
                array('field' => 'site_address', 'label' => 'Site Address', 'rules' => 'required'),
                array('field' => 'city', 'label' => 'Suburb', 'rules' => 'callback_suburb_check[value,required, Suburb can not be empty.]'),
                array('field' => 'state', 'label' => 'State', 'rules' => 'required'),
                array('field' => 'postal', 'label' => 'Postcode', 'rules' => 'required|callback_postal_code_check[postal]'),
                array('field' => 'abn', 'label' => 'Abn', 'rules' => 'required'),
                array('field' => 'siteDetails[]', 'label' => 'Organisation Site', 'rules' => 'callback_site_details_check|callback_multiple_phone_number_check[sitePh,phone,required, Site key contact should be enter valid phone number.]|callback_multiple_email_check[siteMail,email,required, Site key contact should be enter valid email.]'),
            );

            $this->form_validation->set_data($site_data_check);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if (isset($site_data['site_id']) && $site_data['mode'] == 'edit') {
                    $ocs_row = $this->Org_model->update_site($site_data);
                    $this->loges->setTitle("Update site: " . $site_data['site_id']);
                    $msg = 'Site updated successfully.';
                } else {
                    $ocs_row = $this->Org_model->save_site($site_data);
                    $this->loges->setTitle("Add New site: " . $ocs_row['site_id']);
                    $msg = 'Site added successfully.';
                }
                /* logs */
                $this->loges->setUserId($request->adminId);
                $this->loges->setDescription(json_encode($site_data));
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->createLog();
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
    }

    public function check_keycontact($detail) {
        #pr($address);
        if (!empty($detail)) {
            if (empty($detail->firstname)) {
                $this->form_validation->set_message('check_keycontact', 'First Name can not be empty');
                return false;
            } elseif (empty($detail->lastname)) {
                $this->form_validation->set_message('check_keycontact', 'Last Name can not be empty');
                return false;
            } elseif (empty($detail->position)) {
                $this->form_validation->set_message('check_keycontact', 'Position can not be empty');
                return false;
            } elseif (empty($detail->department)) {
                $this->form_validation->set_message('check_keycontact', 'Department can not be empty');
                return false;
            } elseif (!empty($detail->sitePh)) {
                foreach ($detail->sitePh as $key => $val) {
                    if ($key == 'primary_phone' || $key == 'id')
                        continue;

                    if (empty($val)) {
                        $this->form_validation->set_message('check_keycontact', 'Phone number can not be empty');
                        return false;
                    }
                }
            } elseif (!empty($detail->siteMail)) {
                foreach ($detail->siteMail as $key => $val) {
                    if ($key == 'primary_email' || $key == 'id')
                        continue;
                    if (empty($val)) {
                        $this->form_validation->set_message('check_keycontact', 'Email can not empty');
                        return false;
                    } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                        $this->form_validation->set_message('check_keycontact', 'Email is not valid');
                        return false;
                    }
                }
            }
        } else {
            $this->form_validation->set_message('check_keycontact', 'Key contact can not empty');
            return false;
        }
        return true;
    }

    public function get_site_org() {
        $request = request_handler('access_organization');
        $post_data = (array) json_decode($request->data, TRUE);
        $post_data = !empty($post_data) && isset($post_data['query']) ? $post_data['query'] : '';
        $rows = $this->Org_model->get_site_org_unassign($post_data);
        echo json_encode($rows);
    }

    public function add_site_org() {
        $request = request_handler('update_organization');
        $post_data_check = (array) json_decode($request->data);
        $post_data = (array) json_decode($request->data, TRUE);
        if (!empty($post_data)) {
            $validation_rules = array(
                array('field' => 'site_org[]', 'label' => 'Site', 'rules' => 'callback_check_site_not_alloted[value,label,required,is already assigned to other Organisation]'),
                array('field' => 'org_details', 'label' => 'Organisation', 'rules' => 'required')
            );
            $this->form_validation->set_data($post_data_check);
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $this->loges->setTitle("Add New site: " . implode(',', array_column($post_data['site_org'], 'value')) . ' to org id: ' . $post_data['org_details']);
                $this->loges->setUserId($request->adminId);
                $this->loges->setDescription(json_encode($post_data));
                $this->loges->setCreatedBy($request->adminId);
                $this->loges->createLog();
                $sts = $this->Org_model->attach_site_to_org($post_data['site_org'], $post_data['org_details']);

                $label = 'msg';
                $msg = 'site added successfully';
                if (!$sts) {
                    $msg = 'site already assigned to other Organisation';
                    $label = 'error';
                }
                $return = array('status' => $sts, $label => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } else {
            $return = array('status' => false, 'error' => 'Something went wrong');
        }
        echo json_encode($return);
    }

    public function archive_organisation_contact() {
        $request = request_handler('update_organization');

        if (!empty($request->data)) {
            $req = $request->data;
            $org_id = $req->org_id;
            $id = $req->id;
            $arr = array("id" => $id, "organisationId" => $org_id);
            $row = $this->Basic_model->update_records('organisation_all_contact', array('archive' => 1), $arr);
            if (!empty($row)) {
                echo json_encode(array('status' => true));
                exit();
            } else {
                echo json_encode(array('status' => false));
                exit();
            }
        }
    }

    public function archive_organisation_site() {
        $request = request_handler('update_organization');
        if (!empty($request->data)) {
            $req = $request->data;
            $org_id = $req->org_id;
            $id = $req->id;
            $arr = array("id" => $id, "organisationId" => $org_id);
            $row = $this->Basic_model->update_records('organisation_site', array('archive' => 1), $arr);

            if (!empty($row)) {
                echo json_encode(array('status' => true));
                exit();
            } else {
                echo json_encode(array('status' => false));
                exit();
            }
        }
    }

    public function archive_organisation() {
        $request = request_handler('update_organization');
        if (!empty($request->data)) {
            $req = $request->data;
            $parent_org_id = $req->parent_org_id;
            $id = $req->id;
            $arr = array("id" => $id, "parent_org" => $parent_org_id);
            $row = $this->Basic_model->update_records('organisation', array('archive' => 1), $arr);

            if (!empty($row)) {
                echo json_encode(array('status' => true));
                exit();
            } else {
                echo json_encode(array('status' => false));
                exit();
            }
        }
    }

    public function archive_organisation_site_key_contact() {
        $request = request_handler('update_organization');
        if (!empty($request->data)) {
            $req = $request->data;
            $siteId = $req->site_id;
            $id = $req->id;
            $arr = array("id" => $id, "siteId" => $siteId);
            $row = $this->Basic_model->update_records('organisation_site_key_contact', array('archive' => 1), $arr);

            if (!empty($row)) {
                echo json_encode(array('status' => true));
                exit();
            } else {
                echo json_encode(array('status' => false));
                exit();
            }
        }
    }

}
