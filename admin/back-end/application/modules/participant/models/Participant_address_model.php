<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Participant_address_model extends CI_Model
{
    private $table = TBL_PREFIX . 'participant_address';

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function create_participant_address(ParticipantAddress $participantAddress)
    {
        $arr_participant_address = [
            'participantId' => $participantAddress->getParticipantId(),
            'street' => $participantAddress->getStreet(),
            'city' => $participantAddress->getCity(),
            'postal' => $participantAddress->getPostal(),
            'state' => $participantAddress->getState(),
            'lat' => $participantAddress->getLat(),
            'long' => $participantAddress->getLong(),
            'site_category' => $participantAddress->getSiteCategory(),
            'primary_address' => $participantAddress->getPrimaryAddress(),
            'archive' => $participantAddress->getArchive(),
        ];

        $this->db->insert($this->table, $arr_participant_address);

        return $this->db->insert_id();
    }
}
