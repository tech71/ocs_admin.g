<?php

require_once APPPATH . 'Classes/admin/permission.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_profile_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    /*
     * function check partiticipant
     * use for check participant
     */

    public function Check_UserName($objparticipant) {
        $username = $objparticipant->getUserName();
        $this->db->select(array('count(*) as count'));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array('username' => $username));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }

    /*
     * function create_participant
     * use for create participant
     */

    public function create_participant($objparticipant) {

        $arr_participant = array();
        $arr_participant['username'] = $objparticipant->getUserName();
        $arr_participant['password'] = $objparticipant->getPassword();
        $arr_participant['firstname'] = $objparticipant->getFirstname();
        $arr_participant['middlename'] = $objparticipant->getMiddlename();
        $arr_participant['lastname'] = $objparticipant->getLastname();
        $arr_participant['gender'] = $objparticipant->getGender();
        $arr_participant['dob'] = $objparticipant->getDob();
        $arr_participant['ndis_num'] = $objparticipant->getNdisNum();
        $arr_participant['medicare_num'] = $objparticipant->getMedicareNum();
        $arr_participant['crn_num'] = $objparticipant->getCrnNum();
        $arr_participant['referral'] = $objparticipant->getReferral();
        $arr_participant['preferredname'] = $objparticipant->getPreferredname();

        if ($objparticipant->getReferral() == 1) {
            $arr_participant['relation'] = $objparticipant->getParticipantRelation();
            $arr_participant['referral_firstname'] = $objparticipant->getReferralFirstName();
            $arr_participant['referral_lastname'] = $objparticipant->getReferralLastName();
            $arr_participant['referral_email'] = $objparticipant->getReferralEmail();
            $arr_participant['referral_phone'] = $objparticipant->getReferralPhone();
        }

        $arr_participant['living_situation'] = $objparticipant->getLivingSituation();
        $arr_participant['aboriginal_tsi'] = $objparticipant->getAboriginalTsi();
        $arr_participant['oc_departments'] = $objparticipant->getOcDepartments();
        $arr_participant['created'] = $objparticipant->getCreated();
        $arr_participant['status'] = $objparticipant->getStatus();
        $arr_participant['portal_access'] = $objparticipant->getPortalAccess();
        $arr_participant['archive'] = 0;
        $arr_participant['houseId'] = $objparticipant->getHouseid();


        $insert_query = $this->db->insert(TBL_PREFIX . 'participant', $arr_participant);
        $participant_id = $this->db->insert_id();

        if ($participant_id > 0) {
            //Here to start insert all Emails
            $participant_email = array();
            $all_part_emails = $objparticipant->getParticipantEmail();
            if (is_array($all_part_emails)) {
                foreach ($all_part_emails as $emails) {
                    $participant_email[] = array('participantId' => $participant_id, 'email' => $emails['email'], 'primary_email' => $emails['type']);
                }
                $this->db->insert_batch(TBL_PREFIX . 'participant_email', $participant_email);
            }

            //Here to start insert all Phone no.
            $participant_phone = array();
            $all_part_phone = $objparticipant->getParticipantPhone();
            if (is_array($all_part_phone)) {
                foreach ($all_part_phone as $phone) {
                    $participant_phone[] = array('participantId' => $participant_id, 'phone' => $phone['phone'], 'primary_phone' => $phone['type']);
                }
                $this->db->insert_batch(TBL_PREFIX . 'participant_phone', $participant_phone);
            }
        }
        return $participant_id;
    }

    /*
     * function participant_profile
     * return participant profile data
     */

    public function participant_profile($participantID) {
        $colown = array("id", "gender", "status", "portal_access", "firstname", "profile_image", "booking_status", "booking_date");
        $this->db->select($colown);
        $this->db->select(array("concat(firstname,' ',middlename,' ',lastname) as fullName", false));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array('id' => $participantID));
        $query = $this->db->get();

        return $query->row();
    }

    /*
     * function participant about
     * return participant about details like (participant details, phone, email, kin)
     */

    public function participant_about($participantId) {

        // get participant about details query
        $this->db->select("CONCAT(prt.firstname,' ',prt.middlename, ' ', prt.lastname) AS fullname", FALSE);
        $this->db->select(array('prt.id as ocs_id', 'prt.firstname', 'prt.username', 'prt.gender', 'prt.lastname', 'prt.middlename', 'prt.ndis_num', 'prt.medicare_num', 'prt.crn_num', 'prt.preferredname', 'prt.prefer_contact', 'prt.dob', 'p_ph.primary_phone', 'p_em.email', 'p_addrs.street', 'p_addrs.city', 'p_addrs.state', 'p_addrs.postal', 'p_addrs.primary_address', 'p_addrs.site_category', 'p_kin.relation', 'p_kin.phone', 'p_kin.email', 'p_kin.primary_kin'));

        $this->db->from('tbl_participant as prt');
        $this->db->join('tbl_participant_phone as p_ph', 'p_ph.participantId = prt.id AND p_ph.primary_phone = 1', 'left');
        $this->db->join('tbl_participant_email as p_em', 'p_em.participantId = prt.id AND p_em.primary_email = 1', 'left');
        $this->db->join('tbl_participant_address as p_addrs', 'p_addrs.participantId = prt.id AND p_addrs.primary_address = 1', 'left');
        $this->db->join('tbl_participant_kin as p_kin', 'p_kin.participantId = prt.id AND p_kin.primary_kin = 1', 'left');
        $this->db->where(['prt.id' => $participantId]);
        $about_query = $this->db->get();
        $x = $about_query->row_array();

        $arr = $x;

        $dob = $arr['dob'];
        $x = date('Y', strtotime($dob));
        $y = date('Y');
        $arr['age'] = $y - $x;

        // get requirement of participant
        $this->db->select(array("cognition", "communication", "english", "preferred_language", "preferred_language_other", "linguistic_interpreter", "hearing_interpreter", "require_assistance_other", "support_require_other"));
        $this->db->from('tbl_participant_care_requirement');
        $this->db->where(array('participantId' => $participantId));
        $query = $this->db->get();
        $care_requirement = $query->row_array();
        if (!empty($care_requirement)) {
            $arr = array_merge($care_requirement, $arr);
        }


        $genral_Where = array('prt_gen.status' => 1);

        //get participant oc service details
        $this->db->select(array("prt_gen.name as label", "prt_gen.id", "oc_ser.participantId as active"));
        $this->db->from('tbl_participant_genral as prt_gen');
        $this->db->join('tbl_participant_oc_services as oc_ser', 'prt_gen.id = oc_ser.oc_service AND participantId = ' . $participantId, 'left');
        $this->db->where(array('type' => 'oc_service'));
        $this->db->where($genral_Where);
        $this->db->order_by('order', 'asc');
        $query = $this->db->get();
        $arr['oc_service'] = $query->result();

        if (!empty($arr['oc_service'])) {
            $o = [];
            foreach ($arr['oc_service'] as $val) {
                if ($val->active) {
                    $val->active = true;
                    $o[] = $val->label;
                }
            }
            $arr['oc_serviceName'] = implode(', ', $o);
        }

        // get participant support required details
        $this->db->select(array("prt_gen.name as label", "prt_gen.id", "supp_req.participantId as active"));
        $this->db->from('tbl_participant_genral as prt_gen');
        $this->db->join('tbl_participant_support_required as supp_req', 'prt_gen.id = supp_req.support_required AND participantId = ' . $participantId, 'left');
        $this->db->where(array('type' => 'support'));
        $this->db->where($genral_Where);
        $this->db->order_by('order', 'asc');
        $query = $this->db->get();
        $arr['support_required'] = $query->result();
        if (!empty($arr['support_required'])) {
            $s = [];
            foreach ($arr['support_required'] as $val) {
                if ($val->active) {
                    $val->active = true;

                    if ($val->label == 'Other') {
                        $s[] = !empty($arr['support_require_other']) ? $arr['support_require_other'] : '';
                    } else {
                        $s[] = $val->label;
                    }
                }
            }
            $arr['supportName'] = implode(', ', $s);
        }

        // get partucipant asistance detials
        $this->db->select(array("prt_gen.name as label", "prt_gen.id", "prt_ass.participantId as active", "prt_gen.type"));
        $this->db->from('tbl_participant_genral as prt_gen');
        $this->db->join('tbl_participant_assistance as prt_ass', 'prt_gen.id = prt_ass.assistanceId AND participantId = ' . $participantId, 'left');
        $this->db->where_in('prt_gen.type', array('assistance'));
        $this->db->where($genral_Where);
        $this->db->order_by('order', 'asc');
        $query = $this->db->get();

        $temp_mob_assis = $query->result();
        if (!empty($temp_mob_assis)) {
            $a = $m = array();
            foreach ($temp_mob_assis as $val) {
                if ($val->type == 'assistance') {
                    $arr['require_assistance'][] = $val;
                    if ($val->active) {
                        $val->active = true;

                        if ($val->label == 'Other') {
                            $a[] = !empty($arr['require_assistance_other']) ? $arr['require_assistance_other'] : '';
                        } else {
                            $a[] = $val->label;
                        }
                    }
                }
            }
            $arr['assistanceName'] = implode(', ', $a);
        }




        return $arr;
    }

    /*
     * function participant sites
     * return participant address
     * filter address according to client side request
     */

    public function participant_sites($participantID, $viewBy = false) {
        $colown = array("prt_adrs.id", "prt_adrs.street", "prt_adrs.city", "prt_adrs.postal", "prt_adrs.state", "stt.name as stateName", "prt_adrs.site_category", "prt_adrs.primary_address");
        $this->db->select($colown);
        $this->db->from('tbl_participant_address as prt_adrs');
        $this->db->join('tbl_state as stt', 'stt.id = prt_adrs.state', 'inner');
        $this->db->where(array('prt_adrs.participantId' => $participantID));

        if (!empty($viewBy)) {
            $this->db->where(array('prt_adrs.archive' => $viewBy));
        } else {
            $this->db->where(array('prt_adrs.archive' => 0));
        }
        $this->db->where(array('prt_adrs.participantId' => $participantID));
        $this->db->order_by("prt_adrs.primary_address", "asc");
        $query = $this->db->get();

        $result = $query->result_array();

        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $result[$key]['city'] = array('value' => $val['city'], 'label' => $val['city']);
            }
        }

        return $result;
    }

    /*
     * function get_care_abouts
     * return participant about care data
     */

    public function get_care_abouts($participantID, $type, $categories) {
        foreach ($categories as $val) {
            $res[$val] = array();
        }

        // $categories = array('care_notes', 'strategies');
        $arhive = ($type == 'current') ? 0 : 1;

        $colown = array('id', 'title', 'content', 'created', 'categories', 'primary_key');
        $this->db->select($colown);
        $this->db->from('tbl_participant_about_care');
        $this->db->where(array('participantId' => $participantID, 'archive' => $arhive));
        $this->db->where_in('categories', $categories);

        $this->db->order_by("primary_key", "asc");
        $this->db->order_by("updated", "asc");

        $query = $this->db->get();
        $result = $query->result();

        if ($result) {
            foreach ($result as $val) {
                $val->created = date('d/m/y', strtotime($val->created));
                $res[$val->categories][] = $val;
            }
        }
        return $res;
    }

    /*
     * function participant_about
     * update participant prifle about details
     */

    public function participant_update($participant_data) {

        $participantId = $participant_data['ocs_id'];

        // for address update
        $this->update_participant_address($participant_data['address'], $participantId);

        // update phone
        $this->update_participant_phone($participant_data);

        // update email
        $this->update_participant_email($participant_data);

        // update kin detials
        $this->update_kin_details($participant_data['kin_detials'], $participant_data['ocs_id']);

        $participant = array('firstname' => $participant_data['firstname'], 'middlename' => $participant_data['middlename'], 'lastname' => $participant_data['lastname'], 'username' => $participant_data['username'], 'gender' => $participant_data['gender'], 'ndis_num' => $participant_data['ndis_num'], 'medicare_num' => $participant_data['medicare_num'], 'crn_num' => $participant_data['crn_num'], 'preferredname' => $participant_data['preferredname'], 'prefer_contact' => $participant_data['prefer_contact'], 'dob' => DateFormate($participant_data['dob'], 'Y-m-d'));

        $this->db->where($where = array('id' => $participant_data['ocs_id']));
        $this->db->update(TBL_PREFIX . 'participant', $participant);

        return true;
    }

    /*
     * function update_kin_details
     * update participant kin details, according to check previous kin 
     * return nothing
     */

    function update_kin_details($participant_kin, $participantId) {
        $previous_kin = json_decode(json_encode($this->get_participant_kin($participantId)), true);
        $new_kin = json_decode(json_encode($participant_kin), true);

        $previous_kin_ids = array_column($previous_kin, 'id');

        if (!empty($new_kin)) {
            foreach ($new_kin as $key => $val) {
                if (!empty($val['id'])) {
                    if (in_array($val['id'], $previous_kin_ids)) {

                        $key = array_search($val['id'], $previous_kin_ids);

                        // those value on done action remove from array
                        unset($previous_kin[$key]);

                        $update_kin = $this->mapping_kin_array($val, $participantId);
                        $this->basic_model->update_records('participant_kin', $update_kin, $where = array('id' => $val['id']));
                    }
                } elseif (!empty($val['firstname']) && !empty($val['lastname'])) {

                    // collect new data
                    $inset_kin[] = $this->mapping_kin_array($val, $participantId);
                }
            }

            if (!empty($inset_kin)) {
                $this->basic_model->insert_records('participant_kin', $inset_kin, true);
            }

            // archive those member who remove
            if (!empty($previous_kin)) {
                foreach ($previous_kin as $val) {
                    $archive_kin = array('archive' => 1);
                    $this->basic_model->update_records('participant_kin', $archive_kin, $where = array('id' => $val['id']));
                }
            }
        }
    }

    /*
     * function mapping_kin_array
     * make kin array for mapping
     * return mapping array
     */

    function mapping_kin_array($data, $participantId) {
        $update_kin = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'relation' => $data['relation'],
            'primary_kin' => (!empty($data['primary_kin']) && ($data['primary_kin'] == 1) ? 1 : 2),
            'participantId' => $participantId,
        );

        return $update_kin;
    }

    /*
     * function update_participant_email
     * update participant email 
     * return true if update successfully
     */

    function update_participant_email($participant_data) {
        // for email number update
        $this->db->delete(TBL_PREFIX . 'participant_email', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['emails'])) {
            foreach ($participant_data['emails'] as $val) {
                $val->participantId = $participant_data['ocs_id'];
                $emails[] = $val;
            }
            return $this->db->insert_batch(TBL_PREFIX . 'participant_email', $emails);
        }
    }

    /*
     * function update_participant_phone
     * update participant phone number 
     * return true
     */

    function update_participant_phone($participant_data) {
        // for phone number update
        $this->db->delete(TBL_PREFIX . 'participant_phone', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['phones'])) {
            foreach ($participant_data['phones'] as $val) {
                $val->participantId = $participant_data['ocs_id'];
                $phones[] = $val;
            }

            return $this->db->insert_batch(TBL_PREFIX . 'participant_phone', $phones);
        }
    }

    /*
     * function update_participant_address
     * update participant address
     * return type none
     */

    function update_participant_address($participant_address, $participantId) {
        // start for rollback
        $this->db->trans_begin();

        $this->db->delete(TBL_PREFIX . 'participant_address', $where = array('participantId' => $participantId));

        if (!empty($participant_address)) {
            foreach ($participant_address as $key => $val) {
                $prim_sec = ($key === 0) ? 1 : 2;

                $val->site_category = !empty($val->site_category) ? $val->site_category : '';
                $address = array('participantId' => $participantId, 'street' => $val->street, 'postal' => $val->postal, 'city' => $val->city->value, 'state' => $val->state, 'site_category' => $val->site_category, 'primary_address' => $prim_sec, 'lat' => '', 'long' => '');

                $city = $val->city->value;

                $state = getStateById($val->state);
                $addr = $val->street . ' ' . $city . ' ' . $state;

                $latLong = getLatLong($addr);

                if (!empty($latLong)) {
                    $address['lat'] = $latLong['lat'];
                    $address['long'] = $latLong['long'];
                }

                $addrees_data[] = $address;
            }

            if (!empty($addrees_data)) {
                $this->db->insert_batch(TBL_PREFIX . 'participant_address', $addrees_data);
                $this->db->trans_commit();
            } else {
                // for previous data rollback 
                $this->db->trans_rollback();
            }
        }
    }

    /*
     * function participant_requirement_update
     * use for update participant requirement
     * return type none
     */

    public function participant_requirement_update($participant_data) {
        // update participant required assistance
        $this->db->delete(TBL_PREFIX . 'participant_assistance', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['require_assistance'])) {
            foreach ($participant_data['require_assistance'] as $val) {
                if ($val->active) {
                    $assistance[] = array('assistanceId' => $val->id, 'participantId' => $participant_data['ocs_id'], 'type' => 'assistance');
                }
            }

            foreach ($participant_data['mobality'] as $val) {
                if ($val->active) {
                    $assistance[] = array('assistanceId' => $val->id, 'participantId' => $participant_data['ocs_id'], 'type' => 'mobality');
                }
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_assistance', $assistance);
        }

        // update participant oc service
        $this->db->delete(TBL_PREFIX . 'participant_oc_services', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['oc_service'])) {
            foreach ($participant_data['oc_service'] as $val) {
                if ($val->active) {
                    $oc_service[] = array('oc_service' => $val->id, 'participantId' => $participant_data['ocs_id']);
                }
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_oc_services', $oc_service);
        }

        // update participant support_required
        $this->db->delete(TBL_PREFIX . 'participant_support_required', $where = array('participantId' => $participant_data['ocs_id']));
        if (!empty($participant_data['support_required'])) {
            foreach ($participant_data['support_required'] as $val) {
                if ($val->active) {
                    $support_required[] = array('support_required' => $val->id, 'participantId' => $participant_data['ocs_id']);
                }
            }
            $this->db->insert_batch(TBL_PREFIX . 'participant_support_required', $support_required);
        }

        // update participant care not to book
        $this->db->delete(TBL_PREFIX . 'participant_care_not_tobook', $where = array('participantId' => $participant_data['ocs_id']));

        $care_not_book_data = array('Religious_female_option' => $participant_data['Religious_female_option'], 'Religious_male_option' => $participant_data['Religious_male_option'], 'Ethnicity_female_option' => $participant_data['Ethnicity_female_option'], 'Ethnicity_male_option' => $participant_data['Ethnicity_male_option']);
        $this->add_care_not_to_book($care_not_book_data, $participant_data['ocs_id']);

        $participant_data['linguistic_interpreter'] = !empty($participant_data['linguistic_interpreter']) ? $participant_data['linguistic_interpreter'] : '';
        $participant_data['hearing_interpreter'] = !empty($participant_data['hearing_interpreter']) ? $participant_data['hearing_interpreter'] : '';

        // update participant other requirement updated
        $other_data = array(
            'require_assistance_other' => (!empty($participant_data['require_assistance_other'])) ? $participant_data['require_assistance_other'] : '',
            'support_require_other' => (!empty($participant_data['support_require_other'])) ? $participant_data['support_require_other'] : '',
            'cognition' => $participant_data['cognition'],
            'communication' => $participant_data['communication'],
            'english' => $participant_data['english'],
            'preferred_language' => $participant_data['preferred_language'],
            'linguistic_interpreter' => $participant_data['linguistic_interpreter'],
            'hearing_interpreter' => $participant_data['hearing_interpreter']
        );

        $res_other = $this->basic_model->get_row('participant_care_requirement', ['require_assistance_other'], array('participantId' => $participant_data['ocs_id']));
        if (!empty($res_other)) {
            $this->basic_model->update_records('participant_care_requirement', $other_data, array('participantId' => $participant_data['ocs_id']));
        } else {
            $this->basic_model->insert_records('participant_care_requirement', $other_data, $multiple = FALSE);
        }

        return true;
    }

    /*
     * function pfeference_participant
     * 1. return participant selected preference like activiy and places 
     * 2. return array of places and activiy
     */

    public function pfeference_participant($participantId) {
        $arr = array('placeFav' => '', 'placeLeastFav' => '', 'activityFav' => '', 'activityLeastFav' => '');

        // get places participant
        $this->db->select(array("plc.name as label", "plc.id", "prt_plc.type"));
        $this->db->from('tbl_place as plc');
        $this->db->join('tbl_participant_place as prt_plc', 'plc.id = prt_plc.placeId AND participantId = ' . $participantId, 'left');
        $query = $this->db->get();
        $arr['places'] = $query->result();

        if (!empty($arr['places'])) {
            $fav = $leastFav = [];
            foreach ($arr['places'] as $val) {
                $val->type = ($val->type) ? $val->type : 'none';
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
            }
            $arr['placeFav'] = implode(', ', $fav);
            $arr['placeLeastFav'] = implode(', ', $leastFav);
        }

        // get activity participant
        $this->db->select(array("act.name as label", "act.id", "prt_act.type"));
        $this->db->from('tbl_activity as act');
        $this->db->join('tbl_participant_activity as prt_act', 'act.id = prt_act.activityId AND participantId = ' . $participantId, 'left');
        $query = $this->db->get();
        $arr['activity'] = $query->result();

        if (!empty($arr['activity'])) {
            $fav = $leastFav = [];
            foreach ($arr['activity'] as $val) {
                $val->type = ($val->type) ? $val->type : 'none';
                if ($val->type == 1) {
                    $fav[] = $val->label;
                } elseif ($val->type == 2) {
                    $leastFav[] = $val->label;
                }
            }
            $arr['activityFav'] = implode(', ', $fav);
            $arr['activityLeastFav'] = implode(', ', $leastFav);
        }

        return $arr;
    }

    /*
     * function participant_upcoming_shifts
     * return participant upcoming shift with status unfilled, unconfirmed, cancelled and confirmed
     * return type array
     */

    public function participant_upcoming_shifts($reqData) {

        $tbl_shift = TBL_PREFIX . 'shift';
        $tbl_shift_participant = TBL_PREFIX . 'shift_participant';

        $default_date = $reqData->date;
        $month = date('m', strtotime($default_date));
        $year = date('Y', strtotime($default_date));

        //$select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time");
        $select_column = array($tbl_shift . ".id", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".status");
        $dt_query = $this->db->select($select_column);
        $this->db->from($tbl_shift);
        $this->db->where(array($tbl_shift_participant . '.participantId' => $reqData->participantId));
        $this->db->where(array("MONTH(" . $tbl_shift . ".shift_date)" => $month, "YEAR(" . $tbl_shift . ".shift_date)" => $year,));
        $this->db->where_in($tbl_shift . '.status', array(1, 2, 5, 7));
        $this->db->join($tbl_shift_participant, $tbl_shift_participant . '.shiftId = ' . $tbl_shift . '.id AND ' . $tbl_shift_participant . '.status = 1', 'left');


        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dataResult = $query->result();
        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->title = '';
                $val->description = '';
                $val->start = date('Y-m-d', strtotime($val->start_time));
                $val->end = date('Y-m-d', strtotime($val->start_time));
            }
        }


        return $dataResult;
    }

    /*
     * function get_participant_docs
     * return participant docs like ssl and service docs and filter docs according to client request like (accending, decending, alphabetically and newest first)
     * 
     */

    public function get_participant_docs($reqData) {
        $tb1_participant_docs = TBL_PREFIX . 'participant_docs';
        $participantId = $reqData->id;
        $archived = 0;

        $type = ($reqData->type == 'service_docs') ? 1 : 2;

        if ($reqData->sort == 'newest_first') {
            $this->db->order_by('created', 'desc');
        } elseif ($reqData->sort == 'older_first') {
            $this->db->order_by('created', 'asc');
        } elseif ($reqData->sort == 'alphabetical') {
            $this->db->order_by('filename', 'asc');
        }
        if ($reqData->filter === 'current') {
            $archived = '0';
        } else {
            $archived = '1';
        }

        $colown = array("id", "type", "filename", "created", "title");
        $this->db->select($colown);
        $this->db->from($tb1_participant_docs);

        $this->db->where(array('participantId' => $participantId, 'archive' => $archived, 'type' => $type));
        $query = $this->db->get();

        return $query->result();
    }

    /*
     * function get_participant_goals
     * return participant goal array and filter goal according active or inactive goal
     */

    function get_participant_goals($participantID, $view_by) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';

        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);

        $this->db->where(array($tbl_participant_goal . '.participantId' => $participantID));

        if ($view_by == 'active') {
            $this->db->where('start_date <=', DATE_TIME);
            $this->db->where('end_date >=', DATE_TIME);
        } else {
            $this->db->where('start_date >=', DATE_TIME);
        }

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $goals = $query->result();

        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->get_goals_data($val->id);
            }
        }

        return $goals;
    }

    /*
     * function get_goals_data
     * return participant goal result according goal it given
     */

    function get_goals_data($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';

        $this->db->select(array('rating', 'created'));
        $this->db->from($tbl_participant_goal_result);

        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));
        $this->db->where('created <=', DATE_TIME);
        $this->db->order_by('created', 'asc');

        $this->db->limit(12);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result['rating_with_date'] = $query->result_array();

        return $result;
    }

    function get_participant_goal_history($participantID) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';

        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);

        $this->db->where(array($tbl_participant_goal . '.participantId' => $participantID));
        //$this->db->where('end_date <=', DATE_TIME);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $goals = $query->result();
       // last_query();
        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->goal_data_history($val->id);
            }
        }

        return $goals;
    }

    function goal_data_history($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';

        $this->db->select(array('rating', 'created'));
        $this->db->from($tbl_participant_goal_result);

        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));
        //$this->db->where('created <=', DATE_TIME);
        $this->db->order_by('created', 'asc');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();

        $graph_data = array();
        $rating_sum = 0;
        if (!empty($result)) {
            
            foreach ($result as $key => $val) {
                $temp_array = [];
                $temp_array[] = DateFormate($val->created, 'Y-m-d');
                $temp_array[] = $val->rating;
                $graph_data['rating_data'][] = $temp_array;
                $rating_sum += $val->rating;               
            }
             $res=obj_to_arr($result);
            $total=count($res);
            $graph_data['average_rating'] = $rating_sum / $total;
           
        }
        return $graph_data;
    }

    /*
     * function get_participant_fms
     * return participant fms with initiated by and initiated on
     */

    function get_participant_fms($reqData, $adminId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';


        $obj_permission = new classPermission\Permission();
        $incident_fms = $obj_permission->check_permission($adminId, 'incident_fms');

        if (!$incident_fms) {
            $this->db->where('fc.fms_type', '0');
        }

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'fc.id';
            $direction = 'DESC';
        }

        $select_column = array("fc.created", "fc.id", "fc.event_date", "fcr.description", "fc.initiated_type", "fc.Initiator_first_name", "fc.Initiator_last_name", "fc.initiated_by", "fcac.name as case_category");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_fms_case as fc');
        $this->db->join('tbl_fms_case_reason as fcr', 'fcr.caseId = fc.id', 'left');
        $this->db->join('tbl_fms_case_category as fcc', 'fcc.caseId = fc.id', 'left');
        $this->db->join('tbl_fms_case_all_category as fcac', 'fcac.id = fcc.categoryId', 'left');
        $this->db->join('tbl_fms_case_against_detail as fcad', 'fcad.caseId = fc.id', 'left');


        $this->db->where('((initiated_type = 2 AND initiated_by =' . $this->db->escape_str($reqData->participantId) . ') OR (fcad.against_category = 3 AND fcad.against_by = ' . $this->db->escape_str($reqData->participantId) . '))');


        $this->db->group_by('fc.id');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));


        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $total_count = $dt_filtered_total;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        if (!empty($dataResult)) {
            foreach ($dataResult as $val) {
                $val->timelapse = time_ago_in_php($val->created);
                if ($val->initiated_type == 5) {
                    $val->initiated_by = $val->Initiator_first_name . ' ' . $val->Initiator_last_name;
                } else {
                    $val->initiated_by = get_fms_initiated_by_name($val->initiated_type, $val->initiated_by);
                }

                $val->event_date = date("d/m/Y", strtotime($val->event_date));
                $val->against_for = get_fms_against_name($val->id);
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    /*
     * function get_participant_goal_history_report
     * return goal report of participant 
     */

    function get_participant_goal_history_report($participantId) {
        $tbl_participant_goal = TBL_PREFIX . 'participant_goal';
        $this->db->select(array('id', 'title', 'start_date', 'end_date', 'status'));
        $this->db->from($tbl_participant_goal);
        $this->db->where(array($tbl_participant_goal . '.participantId' => $participantId));
        $this->db->where($tbl_participant_goal . '.status', '1');
        $query = $this->db->get();
        $goals = $query->result();
        $filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if (!empty($goals)) {
            foreach ($goals as $val) {
                $val->rating = $this->get_goals_data_history_report($val->id);
            }
        }

        return array('status' => true, 'count' => $filtered_total, 'data' => $goals);
        // return $goals;
    }

    function get_goals_data_history_report($goalId) {
        $tbl_participant_goal_result = TBL_PREFIX . 'participant_goal_result';
        $tbl_goal_rating = TBL_PREFIX . 'goal_rating';

        $this->db->select(array($tbl_goal_rating . '.name', $tbl_participant_goal_result . '.created'));
        $this->db->from($tbl_participant_goal_result);
        $this->db->where(array($tbl_participant_goal_result . '.goalId' => $goalId));

        $this->db->join($tbl_goal_rating, $tbl_participant_goal_result . '.rating =' . $tbl_goal_rating . '.rating', 'inner');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    function get_participant_kin($participantId) {
        $where = array('participantId' => $participantId, 'archive' => 0);
        $colown = array('id', 'relation', 'phone', 'email', 'primary_kin', 'firstname', 'lastname', "CONCAT(firstname, ' ', lastname) AS kinfullname");
        return $this->basic_model->get_record_where('participant_kin', $colown, $where);
    }

    function add_kin_details($participant_kin, $participantId) {
        if (!empty($participant_kin)) {
            $kin_details = array();

            foreach ($participant_kin as $key => $val) {
                if (!empty($val->name) && !empty($val->lastname)) {
                    $kin_details[] = array('participantId' => $participantId, 'firstname' => $val->name, 'lastname' => $val->lastname, 'phone' => $val->contact, 'email' => $val->email, 'relation' => $val->relation, 'primary_kin' => ($key == 0) ? 1 : 2);
                }
            }

            if (!empty($kin_details)) {
                $this->basic_model->insert_records('participant_kin', $kin_details, $multiple = true);
            }
        }
    }

    function add_bookers_details($participant_booker, $participantId) {
        if (!empty($participant_booker)) {
            $bookerdetails = array();

            foreach ($participant_booker as $val) {
                if (!empty($val->name) && !empty($val->lastname)) {
                    $bookerdetails[] = array('participantId' => $participantId, 'firstname' => $val->name, 'lastname' => $val->lastname, 'phone' => $val->contact, 'email' => $val->email, 'relation' => $val->relation);
                }
            }

            if (!empty($bookerdetails)) {
                $this->basic_model->insert_records('participant_booking_list', $bookerdetails, $multiple = true);
            }
        }
    }

    function add_oc_service_details($participant_oc_service, $participantId) {
        if (!empty($participant_oc_service)) {
            $oc_service = array();

            foreach ($participant_oc_service as $val) {
                if ($val->active == 1) {
                    $oc_service[] = array('participantId' => $participantId, 'oc_service' => $val->id);
                }
            }

            if (!empty($oc_service)) {
                $this->basic_model->insert_records('participant_oc_services', $oc_service, $multiple = true);
            }
        }
    }

    function add_mobility_details($participant_mobility, $participantId) {
        if (!empty($participant_mobility)) {
            $oc_service = array();

            foreach ($participant_mobility as $val) {
                if ($val->active == 1) {
                    $oc_service[] = array('participantId' => $participantId, 'assistanceId' => $val->id, 'type' => 'mobality');
                }
            }

            if (!empty($oc_service)) {
                $this->basic_model->insert_records('participant_assistance', $oc_service, $multiple = true);
            }
        }
    }

    function add_care_not_to_book($care_not_book_data, $participantId) {
        #pr($care_not_book_data);
        #this method is also used in edit case also
        if (!empty($care_not_book_data)) {
            #$book_type_user = array();
            $female_religion = array();
            $male_religion = array();
            $female_ethnicity = array();
            $male_ethnicity = array();

            if (!empty($care_not_book_data['Religious_female_option'])) {
                foreach ($care_not_book_data['Religious_female_option'] as $key => $val) {
                    if (isset($val->active) && $val->active == true) {
                        $female_religion[] = array('gender' => 2, 'participantId' => $participantId, 'religious' => $val->value);
                    }
                }
                if (!empty($female_religion))
                    $this->basic_model->insert_records('participant_care_not_tobook', $female_religion, $multiple = true);
            }

            if (!empty($care_not_book_data['Religious_male_option'])) {
                foreach ($care_not_book_data['Religious_male_option'] as $key => $vall) {
                    if (isset($vall->active) && $vall->active == true) {
                        $male_religion[] = array('gender' => 1, 'participantId' => $participantId, 'religious' => $vall->value);
                    }
                }
                if (!empty($male_religion))
                    $this->basic_model->insert_records('participant_care_not_tobook', $male_religion, $multiple = true);
            }

            if (!empty($care_not_book_data['Ethnicity_female_option'])) {
                foreach ($care_not_book_data['Ethnicity_female_option'] as $key => $val_ethn) {
                    if (isset($val_ethn->active) && $val_ethn->active == true) {
                        $female_ethnicity[] = array('gender' => 2, 'participantId' => $participantId, 'ethnicity' => $val_ethn->value);
                    }
                }
                if (!empty($female_ethnicity))
                    $this->basic_model->insert_records('participant_care_not_tobook', $female_ethnicity, $multiple = true);
            }

            if (!empty($care_not_book_data['Ethnicity_male_option'])) {
                foreach ($care_not_book_data['Ethnicity_male_option'] as $key => $mal_ethn) {
                    if (isset($mal_ethn->active) && $mal_ethn->active == true) {
                        $male_ethnicity[] = array('gender' => 1, 'participantId' => $participantId, 'ethnicity' => $mal_ethn->value);
                    }
                }
                if (!empty($male_ethnicity))
                    $this->basic_model->insert_records('participant_care_not_tobook', $male_ethnicity, $multiple = true);
            }

            /* foreach ($care_not_book_data as $val) {
              $book_type_user[] = array('participantId' => $participantId, 'gender' => $val->gender, 'ethnicity' => $val->ethnicity, 'religious' => $val->religious);
              }

              if (!empty($book_type_user)) {
              $this->basic_model->insert_records('participant_care_not_tobook', $book_type_user, $multiple = true);
              } */
        }
    }

    function add_require_assistance($participant_require_assistance, $participantId) {
        if (!empty($participant_require_assistance)) {
            $required_assitance = array();

            foreach ($participant_require_assistance as $val) {
                if ($val->active == 1) {
                    $required_assitance[] = array('participantId' => $participantId, 'assistanceId' => $val->id, 'type' => 'mobality');
                }
            }

            if (!empty($required_assitance)) {
                $this->basic_model->insert_records('participant_assistance', $required_assitance, $multiple = true);
            }
        }
    }

    function add_support_required($participant_support_required, $participantId) {
        if (!empty($participant_support_required)) {
            $support_required = array();

            foreach ($participant_support_required as $val) {
                if ($val->active == 1) {
                    $support_required[] = array('participantId' => $participantId, 'support_required' => $val->id);
                }
            }

            if (!empty($support_required)) {
                $this->basic_model->insert_records('participant_support_required', $support_required, $multiple = true);
            }
        }
    }

    function get_participant_name_and_email($participantId) {
        $this->db->select("concat_ws(' ',p.firstname,p.middlename,p.lastname) as fullName");
        $this->db->select("pe.email");
        $this->db->from("tbl_participant as p");
        $this->db->join("tbl_participant_email as pe", "pe.participantId = p.id AND pe.primary_email = 1");
        $this->db->where('p.id', $participantId);
        $this->db->where('p.archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->row();
    }

    function update_participant_app_access($reqData, $adminId) {
        $this->load->library('UserName');
        
        // check status if its zero mean app access is disable
        if ($reqData->status == 0) {
            // logout participant if its login in portal
            $this->basic_model->delete_records('participant_login', $where = array('participantId' => $reqData->participant_id));

            //save disable portal access note
            $note_data = array(
                'userId' => $reqData->participant_id,
                'user_type' => 2,
                'note' => $reqData->note,
                'action_by' => $adminId,
                'created' => DATE_TIME,
            );

            $this->basic_model->insert_records('disable_portal_access_note', $note_data, $multiple = false);

        }
        $participant = $this->get_participant_name_and_email($reqData->participant_id);

        if (!empty($participant)) {
            $mail_data = ['note' => isset($reqData->note)? $reqData->note:'', 'fullName' => $participant->fullName, 'email' => $participant->email];
            send_disable_portal_access_mail($mail_data,$reqData->status);
        }

        // update app access
        $this->basic_model->update_records('participant', $data = array('portal_access' => $reqData->status), $where = array('id' => $reqData->participant_id));
    }

    public function add_update_participant_goal($participant_goal_data){
        $arr_participant_goal=[];
        $arr_participant_goal['participantId'] = $participant_goal_data['participant_id'];
        $arr_participant_goal['title'] = $participant_goal_data['title'];
        $arr_participant_goal['start_date'] = $participant_goal_data['start_date'];
        $arr_participant_goal['end_date'] = $participant_goal_data['end_date'];
        $arr_participant_goal['status'] = 1;
        $arr_participant_goal['created'] = DATE_TIME;
        $insert_query = $this->db->insert(TBL_PREFIX . 'participant_goal', $arr_participant_goal);
       return $participant_id = $this->db->insert_id();
    }

}
