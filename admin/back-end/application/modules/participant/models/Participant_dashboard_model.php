<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_dashboard_model extends CI_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct();
    }

    public function participant_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $status_con = array(1);


        $src_columns = array('pr.id', 'pr.ndis_num', 'concat_ws(" ",pr.preferredname,pr.firstname,pr.lastname) as FullName', 'pr_ph.phone', 'pr_adrs.street', ' pr_adrs.postal', 'pr_adrs.state', 'pr_em.email');

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;

                if ($orderBy == 'FullName') {
                    $orderBy = 'pr.firstname';
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'pr.id';
            $direction = 'DESC';
        }

        if (!empty($filter)) {
            if ($filter->inactive) {
                $status_con = array(0, 1);
            }

            if (!empty($filter->search)) {
                $this->db->group_start();

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search);
                    }
                    else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search);
                    }
                }

                $this->db->group_end();
            }

            if (!empty($filter->search_by)) {
                if ($filter->search_by == 'with_shift') {
                    $this->db->join('tbl_shift_participant as sf_pr', 'sf_pr.participantId = pr.id AND sf_pr.status = 1', 'left');
                    $this->db->join('tbl_shift as sf', 'sf.id = sf_pr.shiftId', 'left');
                    $this->db->where_in('sf.status', array(1, 2, 3, 7));

                    $this->db->where('sf.shift_date >=', date("Y-m-d"));
                } elseif ($filter->search_by == 'without_shift') {

                    $this->db->where("pr.id NOT IN (SELECT  pr.id FROM tbl_participant as pr
                        LEFT JOIN tbl_shift_participant as sf_pr ON sf_pr.participantId = pr.id AND sf_pr.status = 1
                        LEFT JOIN tbl_shift as sf ON sf.id = sf_pr.shiftId AND sf.status IN(1, 2, 3, 7)
                        WHERE pr.status = 1 AND sf.shift_date >= '" . date("Y-m-d") . "' AND pr.archive = 0 GROUP BY pr.id)", null, false);
                } else if ($filter->search_by == 'portal_access') {
                    $this->db->where('pr.portal_access', 1);
                } else if ($filter->search_by == 'without_portal_access') {
                    $this->db->where('pr.portal_access', 0);
                } else if ($filter->search_by == 'new_paticipant_in_30days') {
                    $this->db->where('pr.created >=', date('Y-m-d', strtotime("-1 months")));
                } else if ($filter->search_by == 'below_age_18year') {
                    $this->db->where('pr.dob >=', date('Y-m-d', strtotime("-18 year")));
                } else if ($filter->search_by == 'open_fms') {
                    $this->db->join('tbl_fms_case as fm_cs', 'fm_cs.initiated_by = pr.id and fm_cs.initiated_type = 2', 'left');
                    $this->db->where_not_in('fm_cs.status', array(1, 2, 4));
                }
            }
        }

        $select_column = array('pr.id', 'pr.ndis_num', 'concat_ws(" ",pr.preferredname,pr.firstname,pr.lastname) as FullName', 'pr.status', 'pr_ph.phone', 'pr.gender', 'concat_ws(", ",pr_adrs.street,pr_adrs.city,pr_adrs.postal,state.name) as address', 'pr_adrs.lat', 'pr_adrs.long');


        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_participant as pr');

        $this->db->join('tbl_participant_address as pr_adrs', 'pr_adrs.participantId = pr.id AND pr_adrs.primary_address = 1', 'left');
        $this->db->join('tbl_participant_email as pr_em', 'pr_em.participantId = pr.id AND pr_em.primary_email = 1', 'left');
        $this->db->join('tbl_participant_phone as pr_ph', 'pr_ph.participantId = pr.id AND pr_ph.primary_phone = 1', 'left');
        $this->db->join('tbl_state as state', 'state.id = pr_adrs.state', 'left');

        $this->db->where_in('pr.status', $status_con);
        $this->db->where_in('pr.archive', 0);

        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('pr.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $total_coount = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();

        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();

                if (!empty($filter->incomplete)) {
                    $percent = get_profile_complete('PARTICIPANT', $val->id);
                    if ($percent == 100) {
                        continue;
                    }
                }

                $row['id'] = $val->id;
                $row['ndis_num'] = $val->ndis_num;
                $row['FullName'] = $val->FullName;
                $row['gender'] = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                $row['street'] = $val->address;
                $row['phone'] = $val->phone;
                $row['status'] = $val->status;
                $row['long'] = $val->long;
                $row['lat'] = $val->lat;
                $dataResult[] = $row;
            }
        }

        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_coount);
        return $return;
    }

    public function get_participant_with_shift() {
        $this->db->select("tbl_participant.id");

        $this->db->from('tbl_participant');
        $this->db->join('tbl_shift_participant', 'tbl_shift_participant.participantId = tbl_participant.id AND tbl_shift_participant.status = 1', 'LEFT');
        $this->db->join('tbl_shift', 'tbl_shift.id = tbl_shift_participant.shiftId AND tbl_shift.status IN(1, 2, 3, 7)', 'LEFT');

        $this->db->where(array('tbl_participant.status' => 1, 'tbl_shift.shift_date >=' => date('Y-m-d'), 'tbl_participant.archive' => 0));
        $this->db->group_by('tbl_participant.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $res = $query->result();

        return $res;
    }

    public function get_count_all_participant() {
        $tb1 = TBL_PREFIX . 'participant';
        $this->db->from($tb1);
        $query = $this->db->get();
        return $result = $query->num_rows();
    }

    function get_testimonial() {
        $tbl_testimonial = TBL_PREFIX . 'testimonial';


        $this->db->select(array($tbl_testimonial . '.title', $tbl_testimonial . '.testimonial', $tbl_testimonial . '.full_name'));
        $this->db->from($tbl_testimonial);
        $this->db->where(array($tbl_testimonial . '.module_type' => 2));
        $this->db->order_by('RAND()');
        $this->db->limit(1);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $x = $query->result();
        return $x;
    }

    function get_participant_phone($participantId) {
        $tbl_participant_phone = TBL_PREFIX . 'participant_phone';
        $this->db->select(array($tbl_participant_phone . '.phone'));
        $this->db->from($tbl_participant_phone);
        $this->db->where($tbl_participant_phone . '.participantId', $participantId);

        $query = $this->db->get();

        $participant_number = $query->result_array();

        $number = array();
        if (!empty($participant_number)) {
            foreach ($participant_number as $value) {
                $number[] = $value['phone'];
            }
        }
        return implode(',', $number);
    }

    public function get_imail_call_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $participantId = $reqData->participantId;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                if ($sorted[0]->id == 'time') {
                    $orderBy = $sorted[0]->id;
                } else {
                    $orderBy = $sorted[0]->id;
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'created';
            $direction = 'desc';
        }

        // searching for imail
        if (!empty($filter->search)) {
            $search = $this->db->escape_str($filter->search);
            $this->db->where('emc.adminId', $search);
        }

        $sWhere = array('emr.recipinent_type' => 2, 'emr.recipinentId' => $participantId);

        $this->db->select(array('em.id as ID', 'emc.userId as HCMID', "pe.email as contact_used", "DATE_FORMAT(em.created, '%d/%m/%Y') as created", "'N/A' as duration", "'Imail' as  type"));

        $this->db->from("tbl_external_message em");
        $this->db->join("tbl_external_message_content as emc", 'em.id = emc.messageId', 'inner');
        $this->db->join('tbl_external_message_recipient as emr', 'emr.messageContentId = emc.id', 'inner');
        $this->db->join('tbl_participant_email pe', 'pe.participantId = emr.recipinentId AND pe.primary_email = 1', 'inner');


        $this->db->where($sWhere);

        $this->db->group_by('em.id');
        $query1 = $this->db->get_compiled_select();


//        echo $query1;
        // get from here participant contact (phone)
        $all_number = $this->get_participant_phone($participantId);



        // searching for call 
        if (!empty($filter->search)) {
            $search = $this->db->escape_str($filter->search);
            $this->db->where('tbl_member_phone.memberId', $search);
        }

        $this->db->select(array('cl.id as ID', "mp.memberId as HCMID", "cl.receiver_number as contact_used", "DATE_FORMAT(cl.created, '%d/%m/%Y') as created", "cl.duration", "'Call' as  type"));
        $this->db->from('tbl_call_log as cl');
        $this->db->join('tbl_participant_phone as pp', 'pp.phone = cl.receiver_number AND pp.participantId = ' . $participantId, 'inner');
        $this->db->join('tbl_member_phone as mp', 'mp.phone = cl.caller_number', 'inner');
        $this->db->join('tbl_member as m', 'm.id = mp.memberId', 'inner');

        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "internal_staff"', 'inner');

        $this->db->where_in('mp.phone', $all_number);
        $query2 = $this->db->get_compiled_select();


// combine query and get data
        $limit_string = "limit " . $limit;
        if ($page > 0) {
            $limit_string = "limit " . $limit . ", " . ($page * $limit);
        }
        $query = $this->db->query($query1 . " UNION " . $query2 . " order by " . $orderBy . " " . $direction . " " . $limit_string);
        $result = $query->result();


        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $return = array('count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

}
