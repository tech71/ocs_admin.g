<?php

// add namespace of admin roles
use classRoles as adminRoles;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class ParticipantDashboard extends MX_Controller {

    // add here our custom form validation
    use formCustomValidation;

    // defualt contructor function call every time when any function called on this class
    function __construct() {
        parent::__construct();

        $this->load->library('form_validation'); // load form validation library
        $this->form_validation->CI = & $this;
        $this->load->model('Participant_profile_model'); // load participant profile model
        $this->load->model('Participant_dashboard_model'); // load dashboard model


        $this->loges->setLogType('participant'); //here set log type participant mean set default module type
    }

    /*
     *  uses
     *  handle request from client side and in reponse give listing of participant
     */

    public function list_participant() {
        // handle client request
        $reqData = request_handler('access_participant');

        // check request not  empty
        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            // get participant data according to request 
            $response = $this->Participant_dashboard_model->participant_list($reqData);

            echo json_encode($response);
        }
    }

    /*
     *  uses
     *  provide listing of state without cheack any type and module parmission  
     */

    public function get_state() {
        request_handler();

        // get all state as value and lable form according to fron-end requirement
        $response = $this->basic_model->get_record_where('state', $column = array('name as label', 'id as value'), $where = array('archive' => 0));

        echo json_encode(array('status' => true, 'data' => $response));
    }

    /*
     * uses
     * handle client side request and create participant in participant module
     */

    public function create_participant() {
        // handle request and check permission of create participant
        $reqData = request_handler('create_participant');

        // set addminId in log for who create this participant
        $this->loges->setCreatedBy($reqData->adminId);

        try {
            $requestData = (array) $reqData->data;
            $first_step_data = (array) json_decode($requestData['step_first']);

            // first and second form data merge in single variable
            $participant_data = array_merge($first_step_data, $requestData);


            global $globalparticipant;
            // include class of participant profile
            require_once APPPATH . 'Classes/participant/Participant.php';
            $objparticipant = new ParticipantClass\Participant();

            // include class of participant requirement
            require_once APPPATH . 'Classes/participant/ParticipantCareRequirement.php';
            $objParticipantCareRequirment = new classParticipantCareRequirement\ParticipantCareRequirement();

            // validate here of participant data
            $response = $this->validate_participant_data($participant_data);

            // check participant data is valid
            if (!empty($response['status'])) {

                // set particicpant data in participant profile object
                $this->setParticipantvalues($objparticipant, $participant_data);

                $globalparticipant = $objparticipant;

                // generate password for participant
                $objparticipant->genratePassword();

                // password send to mail to participant
                $objparticipant->WelcomeMailParticipant();

                // encrypt password
                $objparticipant->encryptPassword();

                // create participant
                $participant_id = $objparticipant->AddParticipant();


                $this->loges->setUserId($participant_id); // set participant id in log
                $this->loges->setDescription(json_encode($participant_data));   // set all request data in participant in log
                $this->loges->setTitle('Added new participant : ' . trim($objparticipant->getFirstname() . ' ' . $objparticipant->getLastname()));  // set title in log
                $this->loges->createLog(); // create log

                if ($participant_id > 0) {

                    // for add address of participant
                    $this->Participant_profile_model->update_participant_address($participant_data['AddressInput'], $participant_id);

                    // add participant kin details
                    $this->Participant_profile_model->add_kin_details($participant_data['kindetails'], $participant_id);

                    // add participant booker details
                    $this->Participant_profile_model->add_bookers_details($participant_data['bookerdetails'], $participant_id);

                    // add participant oc service data
                    $this->Participant_profile_model->add_oc_service_details($participant_data['os_Services'], $participant_id);

                    // add participant mobility data
//                    $this->Participant_profile_model->add_mobility_details($participant_data['mobality'], $participant_id);

                    // about care person not required
                    $care_not_book_data = array('Religious_female_option'=>$participant_data['Religious_female_option'],'Religious_male_option'=>$participant_data['Religious_male_option'],'Ethnicity_female_option'=>$participant_data['Ethnicity_female_option'],'Ethnicity_male_option'=>$participant_data['Ethnicity_male_option']);
                    
                    $this->Participant_profile_model->add_care_not_to_book($care_not_book_data, $participant_id);


                    // add require_assistance insert data
                    $this->Participant_profile_model->add_require_assistance($participant_data['require_assistance'], $participant_id);


                    // add support_required for participant
                    $this->Participant_profile_model->add_support_required($participant_data['support_required'], $participant_id);


                    // participant care info and formal primary diagnosis and secondary diagnosis
                    if (!empty($participant_data['participantcareinfo'])) {
                        $care_data[] = array('content' => $participant_data['participantcareinfo'], 'categories' => 'care_notes', 'primary_key' => 1, 'participantId' => $participant_id);
                        $care_data[] = array('content' => $participant_data['formaldiagnosisprimary'], 'categories' => 'diagnosis', 'primary_key' => 1, 'participantId' => $participant_id);

                        if (!empty($participant_data['formaldiagnosissecondary'])) {
                            $care_data[] = array('content' => $participant_data['formaldiagnosissecondary'], 'categories' => 'diagnosis', 'primary_key' => 2, 'participantId' => $participant_id);
                        }
                        $this->basic_model->insert_records('participant_about_care', $care_data, $multiple = true);
                    }

                    // participant care requirement
                    $objParticipantCareRequirment->setParticipantid($participant_id);
                    $objParticipantCareRequirment->setParticipantCare($participant_data['participantcareinfo']);
                    $objParticipantCareRequirment->setCognition($participant_data['participantCognition']);
                    $objParticipantCareRequirment->setCommunication($participant_data['participantCommunication']);
                    $objParticipantCareRequirment->setEnglish($participant_data['participantenglish']);
                    $objParticipantCareRequirment->setPreferredLanguage($participant_data['participantPreferredlang']);

                    // if preffer language id is 11 mean user selected other language
                    if ($objParticipantCareRequirment->getPreferredLanguage() == 11) {
                        $objParticipantCareRequirment->setPreferredLanguageOther($participant_data['preferred_language_other']);
                    }
                    $objParticipantCareRequirment->setLinguisticInterpreter(!empty($participant_data['participantLinguistic']) ? $participant_data['participantLinguistic'] : '' );
                    $objParticipantCareRequirment->setHearingInterpreter(!empty($participant_data['participantHearing']) ? $participant_data['participantHearing'] : '');
                    $objParticipantCareRequirment->setRequireAssistanceOther($participant_data['participantReqAssistanceOther']);
                    $objParticipantCareRequirment->setSupportRequireOther($participant_data['participantsupportother']);

                    // add participant care requirement
                    $objParticipantCareRequirment->create_care_requirement();
                }

                if ($participant_id > 0) {
                    // if participant succeffully created 
                    $response = array('status' => true);
                } else {
                    $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                }
            } else {
                // if user data not validate
                $response = array('status' => false, 'error' => $response['error']);
            }
        } catch (Exception $ex) {
            $response = array('status' => false, 'error' => $ex->getMessage());
        }

        echo json_encode($response);
    }

    /*
     *  set class properties values in Participant class
     */

    function setParticipantvalues($objparticipant, $participant_data) {

        try {
            $EmailInput = $participant_data['EmailInput'];
            if (is_array($EmailInput) && !empty($EmailInput)) {
                foreach ($EmailInput as $key => $participantemail) {
                    $emails = array();
                    $emails['email'] = $participantemail->name;
                    if ($key == 0)
                        $emails['type'] = '1';
                    else
                        $emails['type'] = '2';

                    $array_email[] = $emails;
                }
            }

            // Set contact values
            $array_phone = array();
            $PhoneInput = $participant_data['PhoneInput'];
            if (is_array($PhoneInput) && !empty($PhoneInput)) {
                foreach ($PhoneInput as $key => $participantephone) {
                    $phones = array();
                    $phones['phone'] = $participantephone->name;
                    if ($key == 0)
                        $phones['type'] = '1';
                    else
                        $phones['type'] = '2';

                    $array_phone[] = $phones;
                }
            }

            $participant_data['referral'] = !empty($participant_data['referral']) ? $participant_data['referral'] : 0;
            $objparticipant->setUserName($participant_data['username']);
            $objparticipant->setFirstname($participant_data['firstname']);
            $objparticipant->setMiddlename(!empty($participant_data['middlename']) ? $participant_data['middlename'] : '');
            $objparticipant->setLastname($participant_data['lastname']);
            $objparticipant->setGender($participant_data['gender']);
            $objparticipant->setPreferredname(!empty($participant_data['Preferredname']) ? $participant_data['Preferredname'] : '');
            $objparticipant->setDob($participant_data['Dob']);

            $objparticipant->setNdisNum(!empty($participant_data['Preferredndis']) ? $participant_data['Preferredndis'] : '');
            $objparticipant->setMedicareNum(!empty($participant_data['Preferredmedicare']) ? $participant_data['Preferredmedicare'] : '');
            $objparticipant->setCrnNum(!empty($participant_data['Preferredcrn']) ? $participant_data['Preferredcrn'] : '');

            $objparticipant->setLivingSituation($participant_data['ParticipantSituation']);
            $objparticipant->setAboriginalTsi($participant_data['ParticipantTSI']);
            $objparticipant->setOcDepartments(!empty($participant_data['AssignOcDepartment']) ? $participant_data['AssignOcDepartment'] : '');
            $objparticipant->setHouseid((!empty($participant_data['AssignOcHouse']) ? $participant_data['AssignOcHouse'] : ''));
            $objparticipant->setCreated(DATE_TIME);
            $objparticipant->setStatus(1);
            $objparticipant->setReferral(($participant_data['referral'] == 2) ? 0 : 1);

            if ($participant_data['referral'] == 1) {
                $objparticipant->setParticipantRelation($participant_data['refferalRelation']);
                $objparticipant->setReferralFirstName($participant_data['Referrer_first_name']);
                $objparticipant->setReferralLastName($participant_data['Referrer_last_name']);
                $objparticipant->setReferralEmail($participant_data['Referrer_email']);
                $objparticipant->setReferralPhone($participant_data['Referrer_phone']);
            }

            $objparticipant->setArchive(1);
            $objparticipant->setPortalAccess(1);
            $objparticipant->setParticipantEmail($array_email);
            $objparticipant->setParticipantPhone($array_phone);
        } catch (Exception $e) {
            return false;
        }
        return $objparticipant;
    }

    /*
     *  uses
     *  validate create participant data
     */

    function validate_participant_data($participant_data) {
       # pr($participant_data);
        try {
            $validation_rules = array(
                array('field' => 'PhoneInput[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number|callback_phone_number_check[name,required,Participant Personal Details contact should be enter valid phone number.]'),
                array('field' => 'EmailInput[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                array('field' => 'AddressInput[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address|callback_postal_code_check[postal]'),
                array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist'),
                array('field' => 'Preferredndis', 'label' => 'NDIS', 'rules' => 'required'),
                array('field' => 'firstname', 'label' => 'First name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last name', 'rules' => 'required'),
                array('field' => 'gender', 'label' => 'gender', 'rules' => 'required'),
                array('field' => 'Dob', 'label' => 'D.O.B', 'rules' => 'required|callback_date_check[Dob,current,%s cannot enter a date in the future]'),
                array('field' => 'Preferredcontacttype', 'label' => 'Preferred contact type', 'rules' => 'required'),
                array('field' => 'ParticipantSituation', 'label' => 'Participant Situation', 'rules' => 'required'),
                array('field' => 'ParticipantTSI', 'label' => 'Participant TSI', 'rules' => 'required'),
                array('field' => 'formaldiagnosisprimary', 'label' => 'formal diagnosis primary TSI', 'rules' => 'required'),
                array('field' => 'participantCognition', 'label' => 'participant Cognition', 'rules' => 'required'),
                array('field' => 'participantCommunication', 'label' => 'participant Communication', 'rules' => 'required'),
                array('field' => 'participantenglish', 'label' => 'participant english', 'rules' => 'required'),
                array('field' => 'participantPreferredlang', 'label' => 'participant Preferred language', 'rules' => 'required'),
                //array('field' => 'CarersInput[]', 'label' => 'CarersInput', 'rules' => 'callback_check_carers_data'),
                array('field' => 'CarersInput', 'label' => 'CarersInput', 'rules' => "callback_check_carers_data_new[" . json_encode(array('Religious_female_option'=>$participant_data['Religious_female_option'],'Religious_male_option'=>$participant_data['Religious_male_option'],'Ethnicity_female_option'=>$participant_data['Ethnicity_female_option'],'Ethnicity_male_option'=>$participant_data['Ethnicity_male_option'])) . "]"),
                array('field' => 'bookerdetails[]', 'label' => 'bookerdetailsInput', 'rules' => 'callback_phone_number_check[contact,,Booker Details contact should be enter valid phone number.]'),
                array('field' => 'kindetails[]', 'label' => 'bookerdetailsInput', 'rules' => 'callback_phone_number_check[contact,,Next of Kin Details contact should be enter valid phone number.]'),
                array('field' => 'Referrer_phone', 'label' => 'Referrer phone', 'rules' => 'callback_phone_number_check[Referrer_phone,,Referrer contact should be enter valid phone number.]'),
            );

            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        } catch (Exception $e) {
            $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }

        return $return;
    }

    /*
     * uses
     * 1. custom validation of participant phone number
     * 2. validate participant phone number
     */

    function check_participant_number($phone_numbers) {
        if (!empty($phone_numbers)) {

            foreach ($phone_numbers as $key => $val) {
                if ($key == 'primary_phone')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
                    return false;
                }
            }
        } else {

            $this->form_validation->set_message('check_participant_number', 'phone number can not be empty');
            return false;
        }
        return true;
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. check participant mutilple email valid or not
     */

    function check_participant_email_address($email_address) {
        if (!empty($email_address)) {
            foreach ($email_address as $key => $val) {
                if ($key == 'primary_email')
                    continue;

                if (empty($val)) {
                    $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
                    return false;
                } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                    $this->form_validation->set_message('check_participant_email_address', 'participant email not valid');
                    return false;
                }
            }
        } else {
            $this->form_validation->set_message('check_participant_email_address', 'participant email can not empty');
            return false;
        }
        return true;
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. validate participant address
     */

    function check_participant_address($address) {
        if (!empty($address)) {
            if (empty($address->street)) {
                $this->form_validation->set_message('check_participant_address', 'participant primary address can not be empty');
                return false;
            } elseif (empty($address->state)) {
                $this->form_validation->set_message('check_participant_address', 'participant state can not be empty');
                return false;
            } elseif (empty($address->postal)) {
                $this->form_validation->set_message('check_participant_address', 'participant pincode can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_participant_address', 'participant address can not empty');
            return false;
        }
        return true;
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. check participant care not to book 
     */

    function check_carers_data($caters_input) {

        if (!empty($caters_input)) {
            if (empty($caters_input->gender)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) gender can not be empty');
                return false;
            }
            if (empty($caters_input->ethnicity)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) Ethnicity can not be empty');
                return false;
            }
            if (empty($caters_input->religious)) {
                $this->form_validation->set_message('check_carers_data', 'Carers (Members) Religious can not be empty');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_carers_data', 'Carers (Members) can not empty');
            return false;
        }

        return true;
    }

    function check_carers_data_new($param,$caters_input) {
        #pr($caters_input);
        if (!empty($caters_input))
        {
            $input_data = json_decode($caters_input);

            $Religious_female_option = $input_data->Religious_female_option;
            $check_active_femal_religious = array_column($Religious_female_option, 'active');

            $Religious_male_option = $input_data->Religious_male_option;
            $check_active_male_religious = array_column($Religious_male_option, 'active');

            $Ethnicity_female_option = $input_data->Ethnicity_female_option;
            $check_active_femal_ethn = array_column($Ethnicity_female_option, 'active');

            $Ethnicity_male_option = $input_data->Ethnicity_male_option;
            $check_active_male_ethn = array_column($Ethnicity_male_option, 'active');

            if(!in_array(true, $check_active_femal_religious))
            {
               $this->form_validation->set_message('check_carers_data_new', 'Select atleast one Female Religious Beliefs, can not be empty');
               return false;
           }

           if(!in_array(true, $check_active_male_religious))
           {
               $this->form_validation->set_message('check_carers_data_new', 'Select atleast one Male Religious Beliefs, can not be empty');
               return false;
           }

           if(!in_array(true, $check_active_femal_ethn))
           {   
               $this->form_validation->set_message('check_carers_data_new', 'Select atleast one Female Ethnicity, can not be empty');
               return false;
           }

            if(!in_array(true, $check_active_male_ethn))
           {
               $this->form_validation->set_message('check_carers_data_new', 'Select atleast one Male Ethnicity, can not be empty');
               return false;
           }
       } 
       else
       {
        $this->form_validation->set_message('check_carers_data_new', 'Carers (Members) can not empty');
        return false;
    }

    return true;
}

    /*
     * uses
     * 1. custom validation of server side and client side
     * 2. validate username of participant and check this username already exist or not
     */

    function check_participant_username_already_exist($sever_end_username = false, $server_ocs_id = false) {

        if ($this->input->get('username') || $sever_end_username) {
            $username = ($this->input->get('username')) ? $this->input->get('username') : $sever_end_username;
            $ocs_id = ($this->input->get('id')) ? $this->input->get('id') : $server_ocs_id;


            if ($ocs_id > 0) {
                $where = array('id !=' => $ocs_id);
            }
            $where['username'] = $username;
            $result = $this->basic_model->get_row('participant', array('username'), $where);

            if ($sever_end_username) {
                if (!empty($result)) {
                    $this->form_validation->set_message('check_participant_username_already_exist', 'Participant user Name Allready Exist');
                    return false;
                }
                return true;
            }

            if (!empty($result)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    /*
     * uses
     * return json of pariticpant general like(oc service, Assistance Required, Support Required and Mobility  )
     */

    public function get_participant_general() {
        $reqData = request_handler('access_participant');
        $column = array('name', 'id', 'type', '"active" as false');
        $where = array('status' => 1);
        $response = $this->basic_model->get_record_where_orderby('participant_genral', $column, $where, 'order', 'asc');
        
        $genral = [];
        foreach ($response as $val) {
            $genral[$val->type][] = array('id' => $val->id, 'label' => $val->name, 'active' => false);
        }
        echo json_encode(array('status' => true, 'data' => $genral));
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. validate kin details of participant
     */

    public function check_kin_detials($kin_detailObject) {
        // here kin details is not mandatory field but if user enter any field of kin then all field of kin is required
        $kin_details = (array) $kin_detailObject;


        // return true if all field of kin empty
        if (empty($kin_details['relation']) && empty($kin_details['phone']) && empty($kin_details['email']) && empty($kin_details['firstname']) && empty($kin_details['lastname'])) {
            return true;
        } else {
            // in else check one by one all field on kin
            if (empty($kin_details['relation'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin relation can not be empty');
                return false;
            } elseif (Empty($kin_details['phone'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin phone can not be empty');
                return false;
            } elseif (Empty($kin_details['email'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin email can not be empty');
                return false;
            } elseif (Empty($kin_details['firstname'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin firname can not be empty');
                return false;
            } elseif (Empty($kin_details['lastname'])) {
                $this->form_validation->set_message('check_kin_detials', 'kin lastname can not be empty');
                return false;
            }
        }
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. check oc service of participant, In which check oc service at least one checked
     */

    public function check_oc_service($service, $service_array) {
        $return = false;

        // here passing oc_service only passing for dummy actual check oc_service second paramter
        $service_array = json_decode($service_array);

        foreach ($service_array as $val) {
            if ($val->active) {
                $return = true;
            }
        }

        $this->form_validation->set_message('check_oc_service', 'Please select at least one oc-service');
        return $return;
    }

    /*
     * used
     * 1. custom validation of server side
     * 2. validate participant support required, In which check support required at least one checked
     */

    public function check_support_required($service, $service_array) {
        $return = false;
        // here passing oc_service only passing for dummy actual check oc_service second paramter
        $service_array = json_decode($service_array);

        foreach ($service_array as $val) {
            if (!empty($val->active)) {
                $return = true;
            }
        }

        $this->form_validation->set_message('check_support_required', 'Please select at least one support required');

        return $return;
    }

    /*
     * uses
     * 1. custom validation of server side
     * 2. validate require assistance of participant, n which check require assistance at least one checked
     */

    public function check_require_assistance($service, $service_array) {
        $return = false;
        // here passing oc_service only passing for dummy actual check oc_service second paramter
        $service_array = json_decode($service_array);
        foreach ($service_array as $val) {
            if ($val->active) {
                $return = true;
            }
        }

        $this->form_validation->set_message('check_require_assistance', 'Please select at least one required assistance');
        return $return;
    }

    /*
     * uses 
     * update participant about details
     */

    public function update_participant_about() {

        // handle client side request and check permission of update participant
        $reqData = request_handler('update_participant');
        $this->loges->setCreatedBy($reqData->adminId); // set creted by in log

        if (!empty($reqData->data)) {
            $participant_data = (array) $reqData->data;

            // make validation rule array for validate client side request
            $validation_rules = array(
                array('field' => 'phones[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number|callback_phone_number_check[phone,required,Participant Personal Details contact should be enter valid phone number.]'),
                array('field' => 'emails[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                array('field' => 'address[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address|callback_postal_code_check[postal]'),
                array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist[' . $participant_data['ocs_id'] . ']'),
                array('field' => 'kin_detials[]', 'label' => 'Address', 'rules' => 'callback_phone_number_check[phone,,Next of Kin Details contact should be enter valid phone number.]|callback_check_kin_detials'),
                array('field' => 'firstname', 'label' => 'First name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last name', 'rules' => 'required'),
                array('field' => 'gender', 'label' => 'gender', 'rules' => 'required'),
                array('field' => 'dob', 'label' => 'D.O.B', 'rules' => 'required|callback_date_check[dob,current,%s cannot enter a date in the future]'),
                array('field' => 'prefer_contact', 'label' => 'Preferred contact type', 'rules' => 'required'),
            );

            $this->form_validation->set_data($participant_data); // set participant data
            $this->form_validation->set_rules($validation_rules); // set rule to validate
            // check participant data is valid
            if ($this->form_validation->run()) {

                // update participant details
                $this->Participant_profile_model->participant_update($participant_data);

                $this->loges->setUserId($participant_data['ocs_id']); // set participant id for log
                $this->loges->setTitle("Update profile: " . trim($participant_data['fullname']));
                $this->loges->setDescription(json_encode($participant_data));
                $this->loges->createLog(); // create log

                $return = array('status' => true);
            } else {

                // return error if participant data is not valid
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        }
        echo json_encode($return);
    }

    /*
     * used
     * update participant requirement
     */

    public function update_participant_requirement() {
        // handle request and check permission of update participant
        $reqData = request_handler('update_participant');

        $this->loges->setCreatedBy($reqData->adminId); // set created by in log

        if (!empty($reqData->data)) {
            $participant_data = (array) $reqData->data;
            // here passing oc_service only passing for dummy actual check oc_service second paramter
            //make validate rule for participant requirement data
            $validation_rules = array(
                array('field' => 'oc_serviceName', 'label' => 'participant Phone', 'rules' => "callback_check_oc_service[" . json_encode($participant_data['oc_service']) . "]"),
                array('field' => 'supportName', 'label' => 'participant Email', 'rules' => "callback_check_support_required[" . json_encode($participant_data['support_required']) . "]"),
                array('field' => 'assistanceName', 'label' => 'Address', 'rules' => "callback_check_require_assistance[" . json_encode($participant_data['require_assistance']) . "]"),
                //array('field' => 'care_not_tobook[]', 'label' => 'username', 'rules' => 'callback_check_carers_data'),
                array('field' => 'CarersInput', 'label' => 'CarersInput', 'rules' => "callback_check_carers_data_new[" . json_encode(array('Religious_female_option'=>$participant_data['Religious_female_option'],'Religious_male_option'=>$participant_data['Religious_male_option'],'Ethnicity_female_option'=>$participant_data['Ethnicity_female_option'],'Ethnicity_male_option'=>$participant_data['Ethnicity_male_option'])) . "]"),
                array('field' => 'cognition', 'label' => 'Cognition', 'rules' => 'required'),
                array('field' => 'communication', 'label' => 'Communication', 'rules' => 'required'),
                array('field' => 'english', 'label' => 'English', 'rules' => 'required'),
                array('field' => 'preferred_language', 'label' => 'Preferred language', 'rules' => 'required'),
            );

            $this->form_validation->set_data($participant_data); // set participant requirement data
            $this->form_validation->set_rules($validation_rules); // set validate rule
            // check participant valid or not
            if ($this->form_validation->run()) {
                // update participant requirement
                $this->Participant_profile_model->participant_requirement_update($participant_data);
                $return = array('status' => true);

                $this->loges->setUserId($participant_data['ocs_id']);
                $this->loges->setTitle("Update requirement: " . trim($participant_data['fullname']));
                $this->loges->setDescription(json_encode($participant_data));
                $this->loges->createLog();
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
        }
        echo json_encode($return);
    }

    /*
     * uses
     * return participant count (according to week, year and month) 
     */

    public function participant_count() {
        // handle request and check permission access participant
        $reqData = request_handler('access_participant');
        if (!empty($reqData->data)) {
            $post_data = $reqData->data;

            // make where clouse for get participant count
            // if user want to get count according to year
            if ($post_data->view_type == 'year') {
                $where['YEAR(created)'] = date('Y');
            } else if ($post_data->view_type == 'week') {  // if user want to get count according to week
                $where = '';
                $where1 = 'created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()';
                $this->db->where($where1);
            } else {
                // if user want to get count according to month
                $where['MONTH(created)'] = date('m');
            }

            $rows = $this->basic_model->get_result('participant', $where, array('id'));
            $all_total = $this->Participant_dashboard_model->get_count_all_participant();

            $count = (!empty($rows)) ? count($rows) : 0;
            echo json_encode(array('participant_count' => $count, 'status' => TRUE, 'all_participant_count' => $all_total));
            exit();
        }
    }

    /*
     * used
     * return testimonial of participant module
     */

    function get_testimonial() {
        request_handler('access_participant');
        $result = $this->Participant_dashboard_model->get_testimonial();
        echo json_encode(array('status' => true, 'data' => $result));
    }

}
