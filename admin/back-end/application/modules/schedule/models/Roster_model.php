<?php

class Roster_model extends CI_Model {

    // get unfilled shift
    public function get_active_roster($reqData, $participantId = false) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $src_columns = ['pr.id', 'pr.title', 'p.firstname', 'p.middlename', 'p.lastname', 'CONCAT(p.firstname," ",p.middlename," ",p.lastname)', 'CONCAT(p.firstname," ",p.lastname)'];

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'pr.id';
            $direction = 'ASC';
        }

        if (!empty($filter->search_box)) {
            //Search by all site table
            $this->db->group_start();

            for ($i = 0; $i < count($src_columns); ++$i) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, 'as') !== false) {
                    $serch_column = explode(' as ', $column_search);
                    $this->db->or_like($serch_column[0], $filter->search_box);
                } else {
                    $this->db->or_like($column_search, $filter->search_box);
                }
            }
            $this->db->group_end();
        }

        if (!empty($participantId)) {
            $this->db->where('participantId', $participantId);
        }

        if (!empty($filter->start_date) || !empty($filter->end_date)) {
            if (!empty($filter->start_date)) {
                $this->db->where("pr.start_date >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
            }
            if (!empty($filter->end_date)) {
                $this->db->where("pr.start_date <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
            }
        }

        if (!empty($filter->roster_type)) {
            if ($filter->roster_type == 'default') {
                $this->db->where('pr.is_default', 0);
            } elseif ($filter->roster_type == 'temporary') {
                $this->db->where('pr.is_default', 1);
            }
        }

        $select_column = ['pr.id', 'pr.title', 'pr.start_date', 'pr.end_date'];

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        $this->db->select("CASE pr.is_default
            WHEN 1 THEN (pr.title)
            WHEN 0 THEN ('Default')
            ELSE NULL
            END as title");

        $this->db->select("CONCAT( p.firstname,' ',p.middlename,' ',p.lastname) as participantName");
        $this->db->from('tbl_participant_roster as pr');

        // join with participant
        $this->db->join('tbl_participant as p', 'p.id = pr.participantId', 'left');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $this->db->where_in('pr.status', explode(',', $filter->status));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        // last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $total_count = $dt_filtered_total;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();

        $return = ['count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => (int) $total_count];

        return $return;
    }

    public function get_roster_details($rosterId) {

        // get roster main data
        $this->db->select(['pr.id', 'pr.title', 'pr.start_date', 'pr.end_date', 'pr.shift_round', 'pr.is_default', 'pr.participantId', 'pr.status']);
        $this->db->select("CASE pr.is_default
            WHEN 1 THEN (pr.title)
            WHEN 0 THEN ('Default')
            ELSE NULL
            END as title");
        $this->db->select('CONCAT("\'",p.firstname,"\' ",p.middlename, " ", p.lastname ) as participantName');
        $this->db->from('tbl_participant_roster as pr');
        $this->db->join('tbl_participant as p', 'p.id = pr.participantId', 'left');

        $this->db->where('pr.id', $rosterId);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $roster = $query->row_array();

        $date1 = date_create(DateFormate($roster['start_date'], 'Y-m-d'));
        $date2 = date_create(DATE_TIME);

        $diff = date_diff($date1, $date2);
        $roster['day_count'] = $diff->days;

        // $roster['update_disabled'] = ($diff->days > 57) ? false : true;
        $roster['update_disabled'] = true;
        $roster['is_default'] = ($roster['is_default'] > 1) ? 2 : 1;

        $roster['participant'] = ['value' => $roster['participantId'], 'label' => $roster['participantName']];

        $roster['end_date'] = ($roster['end_date'] == '0000-00-00 00:00:00') ? '' : $roster['end_date'];

        // get roster week details
        $this->db->select(['week_day', 'start_time', 'end_time', 'week_number']);
        $this->db->from('tbl_participant_roster_data');
        $this->db->where('rosterId', $rosterId);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $rosterData = $query->result();

        // create initial per day shift is not active (mean blank shift)
        $tempList = [['is_active' => false], ['is_active' => false], ['is_active' => false], ['is_active' => false]];
        $tempRoster = [];

        if (!empty($rosterData)) {
            $cnt = 0;
            foreach ($rosterData as $key => $val) {
                $weekKey = $val->week_number - 1;
                $dynamicIndex = 0;
                $dayName = numberToDay($val->week_day);

                if (!isset($tempRoster[$weekKey][$dayName])) {
                    $tempRoster[$weekKey][$dayName] = $tempList;
                }


                if (array_key_exists($weekKey, $tempRoster)) {

                    if (array_key_exists($dayName, $tempRoster[$weekKey])) {
                        foreach ($tempRoster[$weekKey][$dayName] as $check) {
                            if ($check['is_active']) {
                                $dynamicIndex++;
                            }
                        }
                    }
                }


                $tempRoster[$weekKey][$dayName][$dynamicIndex]['is_active'] = true;
                $tempRoster[$weekKey][$dayName][$dynamicIndex]['start_time'] = $val->start_time;
                $tempRoster[$weekKey][$dayName][$dynamicIndex]['end_time'] = ($val->end_time == '0000-00-00 00:00:00') ? '' : $val->end_time;
                ++$cnt;
            }
        }

        $resosterList = [];

        $shift_round = json_decode($roster['shift_round']);

        $weekKeyArray = array_keys($shift_round);
        foreach ($weekKeyArray as $weekNumber) {
            foreach (numberToDay() as $weekDayNum => $weekDay) {
                $status = true;
                // $resosterList[$weekNumber][$weekDay] = $tempList;

                if (array_key_exists($weekNumber, $tempRoster)) {
                    if (array_key_exists($weekDay, $tempRoster[$weekNumber])) {
                        $status = false;
                        $dynamicIndex = 0;

                        $resosterList[$weekNumber][$weekDay] = $tempRoster[$weekNumber][$weekDay];
                    }
                }

                if ($status) {
                    $resosterList[$weekNumber][$weekDay] = $tempList;
                }
            }
        }

        $roster['rosterList'] = $resosterList;

        return $roster;
    }

    public function getRosterTempData($rosterId) {
        $tbl_participant_roster = TBL_PREFIX . 'participant_roster';
        $tbl_participant_roster_temp_data = TBL_PREFIX . 'participant_roster_temp_data';

        // get roster main data
        $this->db->select([$tbl_participant_roster . '.is_default', $tbl_participant_roster . '.id', $tbl_participant_roster . '.participantId', $tbl_participant_roster_temp_data . '.rosterData']);

        $this->db->from($tbl_participant_roster);
        $this->db->join($tbl_participant_roster_temp_data, $tbl_participant_roster_temp_data . '.rosterId = ' . $tbl_participant_roster . '.id', 'left');

        $this->db->where($tbl_participant_roster . '.id', $rosterId);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->row();
    }

    public function check_roster_start_end_date_already_exist($rosObj) {
        $tbl_participant_roster = TBL_PREFIX . 'participant_roster';

        $this->db->select([$tbl_participant_roster . '.id']);

        $this->db->from($tbl_participant_roster);

        $where = 'is_default = 1 AND participantId = ' . $rosObj->getParticipantId() . " AND 
        (('" . $rosObj->getStart_date() . "' BETWEEN date(start_date) AND date(end_date))  or ('" . $rosObj->getEnd_date() . "' BETWEEN date(start_date) AND date(end_date)) OR
        (date(start_date) BETWEEN '" . $rosObj->getStart_date() . "' AND '" . $rosObj->getEnd_date() . "')  or (date(end_date) BETWEEN '" . $rosObj->getStart_date() . "' AND '" . $rosObj->getEnd_date() . "')) ";

        if ($rosObj->getRosterId() > 0) {
            $this->db->where('id !=', $rosObj->getRosterId());
        }
        $this->db->where($where);
        $this->db->where_in('status', [1, 2]);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->row();
    }

    function get_roster_logs($reqData) {

        $colowmn = array('lg.title', "DATE_FORMAT(lg.created, '%d/%m/%Y') as created", "DATE_FORMAT(lg.created, '%H:%i %p') as time");
        $this->db->select($colowmn);
        $this->db->from('tbl_logs as lg');
        $this->db->join('tbl_module_title as mt', 'mt.id = lg.sub_module', 'INNER');
        $this->db->where('lg.userId', $reqData->rosterId);
        $this->db->where('mt.key_name', 'roster');

        $query = $this->db->get();
        return $query->result_array();
    }

}
