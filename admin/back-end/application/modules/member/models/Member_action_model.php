<?php

class Member_action_model extends CI_Model {

    function get_testimonial() {
        $tbl_testimonial = TBL_PREFIX . 'testimonial';
        $tbl_member = TBL_PREFIX . 'member';

        $this->db->select(array($tbl_testimonial . '.title', $tbl_testimonial . '.testimonial', $tbl_testimonial . '.full_name'));
        $this->db->from($tbl_testimonial);
        $this->db->where(array($tbl_testimonial . '.module_type' => 1));
        $this->db->order_by('RAND()');
        $this->db->limit(1);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $x = $query->result();
        return $x;
    }

    function get_all_member_count() {
        $this->db->select(array('m.id'));
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');

        $this->db->where('m.archive', 0);
        $this->db->where('d.short_code', 'external_staff');

        $query = $this->db->get();
        $x = $query->result();
        return $x;
    }

    function member_count_based_created($view_type) {

        if ($view_type == 'year') {
            $where['YEAR(m.created)'] = date('Y');
            $where['m.archive'] = '0';
            $this->db->where($where);
        } else if ($view_type == 'week') {
            $where = '';
            $where1 = "m.created > DATE_SUB(NOW(), INTERVAL 1 WEEK) AND m.archive = '0'";
            $this->db->where($where1);
        } else {
            $where['MONTH(m.created)'] = date('m');
            $where['YEAR(m.created)'] = date('Y');
            $where['m.archive'] = '0';
            $this->db->where($where);
        }

        $this->db->select(array('m.id'));
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department', 'inner');

        $this->db->where('m.archive', 0);
        $this->db->where('d.short_code', 'external_staff');

        $query = $this->db->get();
        $x = $query->result();
        return $x;
    }

    function get_member_name_and_email($id) {
        $this->db->select("concat_ws(' ',m.firstname,m.middlename,m.lastname) as fullName");
        $this->db->select("mm.email");
        $this->db->from("tbl_member as m");
        $this->db->join("tbl_member_email as mm", "mm.memberId = m.id AND mm.primary_email = 1");
        $this->db->where('m.id', $id);
        $this->db->where('m.archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->row();
    }

    function get_work_area_of_member($memberId) {
        $this->db->select("mwa.work_area");
        $this->db->from("tbl_member_work_area as mwa");
        $this->db->join("tbl_member_email as mm", "mm.memberId = m.id AND mm.primary_email = 1");
        
        $this->db->where('mwa.memberId', $memberId);
        $this->db->where('mwa.archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->row();
    }

}
