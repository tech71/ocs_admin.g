<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FinanceLineItem extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Finance_line_item');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->library('UserName');
        $this->loges->setLogType('finance_line_item');
    }

    function get_create_line_item_option_and_details() {
        $requestData = request_handler('access_finance_admin');
        $reqData = $requestData->data;

        $res = [];
        if (!empty($reqData)) {

            $lineItemId = 0;
            if (!empty($reqData->lineItemId)) {
                $lineItemId = (int) $reqData->lineItemId;
                $res = $this->Finance_line_item->get_line_item_details($reqData->lineItemId);
            }

            $res['funding_type_option'] = $this->basic_model->get_record_where('funding_type', ['name as label', 'id as value'], '');
            $res['outcome_domain_option'] = $this->basic_model->get_record_where('finance_support_outcome_domain', ['name', 'id'], '');
            $res['registration_group_option'] = $this->basic_model->get_record_where('finance_support_registration_group', ['name', 'id'], '');
            $res['support_category_option'] = $this->basic_model->get_record_where('finance_support_category', ['name', 'id'], '');
            $res['level_option'] = $this->basic_model->get_record_where('classification_level', ['level_name as label', 'id as value'], ['archive' => 0]);
            $res['point_name_option'] = $this->basic_model->get_record_where('classification_point', ['point_name as label', 'id as value'], ['archive' => 0]);

            $res['time_of_the_days'] = $this->Finance_line_item->get_time_of_day_of_line_item($lineItemId);

            $res['week_days'] = $this->Finance_line_item->get_week_days_of_line_item($lineItemId);

            $res['state'] = $this->Finance_line_item->get_state_of_line_item($lineItemId);

            echo json_encode(['status' => true, 'data' => $res]);
        }
    }

    function add_update_line_item() {
        $requestData = request_handler('access_finance_admin');
        $this->loges->setUserId($requestData->adminId);
        $reqData = $requestData->data;


        if (!empty($reqData)) {
            $reqData->lineItemId = !empty($reqData->lineItemId) ? $reqData->lineItemId : '';

            $validation_rules = array(
                array('field' => 'funding_type', 'label' => 'funding type', 'rules' => 'required'),
                array('field' => 'line_item_number', 'label' => 'line item number', 'rules' => 'callback_check_line_item_number_already_exist[' . json_encode($reqData) . ']'),
                array('field' => 'start_date', 'label' => 'start date', 'rules' => 'required'),
                array('field' => 'end_date', 'label' => 'end date', 'rules' => 'required'),
                array('field' => 'member_ratio', 'label' => 'member ratio', 'rules' => 'required'),
                array('field' => 'participant_ratio', 'label' => 'participant ratio', 'rules' => 'required'),
                array('field' => 'upper_price_limit', 'label' => 'upper price limit', 'rules' => 'required|numeric'),
                array('field' => 'national_price_limit', 'label' => 'upper price limit', 'rules' => 'numeric'),
                array('field' => 'national_very_price_limit', 'label' => 'upper price limit', 'rules' => 'numeric'),
                array('field' => 'levelId', 'label' => 'level', 'rules' => 'numeric'),
                array('field' => 'pay_pointId', 'label' => 'pay point', 'rules' => 'numeric'),
                array('field' => 'lineItemId', 'label' => 'line item', 'rules' => 'callback_check_line_item_status_for_update[' . json_encode($reqData) . ']'),
            );

            $this->form_validation->set_data((array) $reqData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $lineItemId = $this->Finance_line_item->add_update_line_item($reqData);

                $txt = (!empty($reqData->lineItemId)) ? "Update line Item number" : "Create New line Item number";
                $this->loges->setTitle($txt . ' : ' . $reqData->line_item_number);
                $this->loges->setUserId($lineItemId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();

                $response = ['status' => true];
            } else {
                $errors = $this->form_validation->error_array();
                $response = ['status' => false, 'error' => implode(', ', $errors)];
            }

            echo json_encode($response);
        }
    }

    function check_line_item_status_for_update($lineItemId, $reqData) {
        // here check only for edit line item

        if (!empty($lineItemId)) {
            $reqData = json_decode($reqData);
            $res = $this->Finance_line_item->get_line_item_details($lineItemId);

            if (!empty($res['status'])) {

                $start_date = DateFormate($reqData->start_date, 'Y-m-d');
                $end_date = DateFormate($reqData->end_date, 'Y-m-d');

                $prev_start_date = (DateFormate($res['start_date'], 'Y-m-d'));
                $prev_end_date = (DateFormate($res['end_date'], 'Y-m-d'));
                $curr_date = (date('Y-m-d'));

                // check if current line end date or update end date is same then need to show message
                if ($prev_end_date == $curr_date && $curr_date == $end_date) {
                    $this->form_validation->set_message('check_line_item_status_for_update', 'Tomorrow this line item will be archive so its can not be edited');
                    return false;
                }

                if ($res['status'] == 3) {

                    $this->form_validation->set_message('check_line_item_status_for_update', 'Archive line item can not updated');
                    return false;
                } elseif ($res['status'] == 1) {
                    return true;
                } else {
                    return true;
                }
            } else {
                $this->form_validation->set_message('check_line_item_status_for_update', 'Please provide valid line item id');
                return false;
            }
        }

        return true;
    }

    function check_line_item_number_already_exist($line_item_number, $reqData) {
        if (!empty($line_item_number)) {
            $reqData = json_decode($reqData);

            $res = $this->Finance_line_item->check_line_item_number_already_exist($reqData);

            if (empty($res)) {
                return true;
            } else {
                $this->form_validation->set_message('check_line_item_number_already_exist', 'On current duration another line item number already exist.');
                return false;
            }
        } else {
            $this->form_validation->set_message('check_line_item_number_already_exist', 'Please provide line item number.');
            return false;
        }
    }

    function get_finance_line_item_listing() {
        $reqData = request_handler('access_finance');

        if (!empty($reqData->data)) {
            $result = $this->Finance_line_item->get_finance_line_item_listing($reqData->data);
            echo json_encode($result);
        }
    }

    function archive_line_item() {
        $requestData = request_handler('access_finance_admin');
        $this->loges->setUserId($requestData->adminId);
        $reqData = $requestData->data;

        if (!empty($reqData->lineItemId)) {

            $response = $this->Finance_line_item->archive_line_item($reqData->lineItemId);

            if (!empty($response['status'])) {
                $this->loges->setTitle('Archive Line item : ' . $reqData->lineItemId);
                $this->loges->setUserId($reqData->lineItemId);
                $this->loges->setDescription(json_encode($reqData));
                $this->loges->createLog();
            }
        } else {
            $response = ['status' => false, 'error' => "line item id not found"];
        }

        echo json_encode($response);
    }

    function get_finance_line_item_listing_filter_option() {
        request_handler('access_finance');
        $response = [];
        $response['funding_type_option'] = $this->basic_model->get_record_where('funding_type', ['name as label', 'id as value'], ['archive' => 0]);
        $response['state_option'] = $this->basic_model->get_record_where('state', ['name as label', 'id as value'], ['archive' => 0]);

        echo json_encode(['status' => true, 'data' => $response]);
    }        
    // import CSV code start 
    function read_csv_line_items(){
        $data = request_handlerFile('access_finance_admin');
        $currunt_date = date("d-m-Y", strtotime(DATE_TIME));
        $str_currunt_date =strtotime(DATE_TIME);
        
        if(empty($data->funding_type) || $data->funding_type==null || $data->funding_type=='undefined'){
            echo json_encode(['status' => false, 'error' => 'Funding Type required please provide funding type']);
            exit();
        }
        if(empty($data->start_date) || $data->start_date==null || $data->start_date=='undefined'){
            echo json_encode(['status' => false, 'error' => 'Start date required']);
            exit();
        }
        if(empty($data->end_date) || $data->end_date==null || $data->end_date=='undefined'){
            echo json_encode(['status' => false, 'error' => 'End date required']);
            exit();
        }
        
        $start_date=date("Y-m-d", strtotime($data->start_date));
        $end_date=date("Y-m-d", strtotime($data->end_date));

        $str_start_date=strtotime($data->start_date);
        $str_end_date=strtotime($data->end_date);

        $funding_type=$data->funding_type;       
        if($str_currunt_date >= $str_start_date){
            // Start date is greater then currunt date
            echo json_encode(['status' => false, 'error' => 'Start date must be greater than currunt date']);
            exit();
        }
        if ($str_start_date > $str_end_date) {
            echo json_encode(['status' => false, 'error' => 'Start date must be greater than or equal to end date']);
            exit();
        }   
        
        if (empty($_FILES['docsFile']['name'])) {            
            echo json_encode(['status' => false, 'error' => 'Line Item CSV File not exist please file upload first']);
            exit();
        }

        if (!empty($_FILES) && $_FILES['docsFile']['error'] == 0) {
            
            
            $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(in_array($_FILES['docsFile']['type'],$mimes)){                
                $tmpName = $_FILES['docsFile']['tmp_name'];     
                $file = array_map('str_getcsv', file($tmpName));                               
                $header = array_shift($file);
                $line_item_data=[];
                if(!empty($file)){
                    foreach($file as $row) {
                        if(count($row)== count($header)){
                            $row = array_map("utf8_encode", $row);
                            $line_item_data[] = array_combine($header, $row);
                        }else{
                            echo json_encode(["status"=>false,"error" => "Unsuccessful file import. Please try importing the file again."]);
                            exit();                    
                        }
                    }
                    if(!empty($line_item_data)){
                        $response=$this->check_insert_update_line_item_data($line_item_data,$funding_type,$start_date,$end_date,$currunt_date);
                        echo json_encode($response);
                        exit();
                    }else{
                        echo json_encode(['status' => false, 'error' => 'Line Item invalid data']);
                        exit();    
                    }

                }else{
                    echo json_encode(['status' => false, 'error' => 'Line Item invalid data']);
                    exit();
                }
            }else{
                echo json_encode(['status' => false, 'error' => 'Line Item file not valid ext']);
                exit();
            }
        }else {
            echo json_encode(['status' => false, 'error' => 'Unsuccessful file import. Please try importing the file again.']);            
            exit();
        }
    }   


    function check_insert_update_line_item_data($line_item_data,$funding_type,$start_date,$end_date,$currunt_date){     

        $weekOfdays= $this->Finance_line_item->get_week_day_list();
        $timeOfdays= $this->Finance_line_item->get_time_of_day_list();
        $states= $this->Finance_line_item->get_state_list();
        
        $column_arr = array("Registration Group Number" => 'support_registration_group', "Support Category Number" => 'support_category', "Support Outcome Domain Number" => "support_outcome_domain",
            "Support Item Number" => 'line_item_number', "Support Item Name" => 'line_item_name', "Support Item Description" => 'description', "Non-F2F" => 'non_f2f',
            "Member Ratio" => 'member_ratio',"Participant Ratio" => 'participant_ratio', "National Non-Remote" => 'upper_price_limit', "National Remote" => 'national_price_limit',
            "National Very Remote" => 'national_very_price_limit', "Cancellations" => 'cancellation_fees', "Travel" => 'travel_required', "Price Controlled" => 'price_control',
            "Quote Required" => 'quote_required', "NDIA Reporting" => 'ndis_reporting');

        $column_value_type=array("Non-F2F");

        /*
          [Unit] => EA          
          [schedule constraint] => y */
        $cnt = 0;
        $insert_array = array();
        $update_array = array();
        foreach ($line_item_data as $row_value) {
            $cnt++;

            $row_array = array();
            $row_array['start_date'] = $start_date;
            $row_array['end_date'] = $end_date;
            $row_array['funding_type'] = $funding_type;
            $state_array=array();
            $timeOfdays_array=array();
            $weekOfdays_array=array();
         
            foreach ($row_value as $key => $value) {

                if(isset($states[$key])){
                     $state_check=strtoupper($value);
                     if($state_check=='Y'){
                        $state['stateId']=$states[$key];
                        $state['line_itemId']=0;
                        $state_array[]=$state;
                      
                     }
                }

                if(isset($timeOfdays[$key])){
                    $time_check=strtoupper($value);                   
                    if($time_check=='Y'){
                        $timeday['finance_timeId']=$timeOfdays[$key];
                        $timeday['line_itemId']=0;
                        $timeOfdays_array[]=$timeday;
                    }
               }
               
               if(isset($weekOfdays[$key])){
                $week_check=strtoupper($value);
                if($week_check=='Y'){
                    $weekDay['week_dayId']=$weekOfdays[$key];
                    $weekDay['line_itemId']=0;
                    $weekOfdays_array[]=$weekDay;
                }
                }

                if (isset($column_arr[$key])) {
                    if(in_array($key,$column_value_type)){
                        $value=strtoupper($value);   
                        $row_array[$column_arr[$key]] =($value=='Y')?'1':'0';
                    }else{
                        $row_array[$column_arr[$key]] = $value;
                    }
                }
                // status is if start date not start then inactive if date between start and and date then active and if currunt date end date then archived
                //if($key== Registration Group Number
                /*
                  $line_item['funding_type']=$funding_type; , $line_item['support_registration_group']=
                  $line_item['support_category']=,   $line_item['support_outcome_domain']=
                  $line_item['line_item_number']=,   $line_item['non_f2f']=
                  $line_item['line_item_name']=,    $line_item['start_date']=$start_date;
                  $line_item['end_date']=$end_date;,  $line_item['description']=
                  $line_item['quote_required']=,   $line_item['price_control']=
                  $line_item['travel_required']=,        $line_item['cancellation_fees']=
                  $line_item['ndis_reporting']=,        $line_item['upper_price_limit']= , $line_item['national_price_limit']=,  $line_item['national_very_price_limit']=,   $line_item['schedule_constraint']=
                  $line_item['public_holiday']=,    $line_item['member_ratio']=, $line_item['participant_ratio']=, $line_item['created']=, $line_item['updated']= */
            }
            $row_array['states']=$state_array;
            $row_array['weekofday']=$weekOfdays_array;
            $row_array['timeofday']=$timeOfdays_array;
            
            $validate_res=$this->csv_validate_line_items($row_array);
            
            if($validate_res['status']){
                $validate_line_item_db=$this->check_line_item_exist($row_array,$insert_array);                        
                if($validate_line_item_db['status']){ 
                    $insert_array[]=$row_array;
                }else{
                    return $validate_line_item_db;                
                    break;
                }
            } else {
                return $validate_res;
                break;
            }
        }      
       $res_insert=$this->Finance_line_item->insert_bulk_import_line_item($insert_array);
       if($res_insert){
            return ["status"=>true, "message"=>"Successful file import"];
       }else{
            return ["status"=>false,"error" => "Unsuccessful submit file import. Please try importing the file again."];
       }
    }

    function check_line_item_exist($row_array, $insert_array) {
        $get_all_line_item = array_column($insert_array, 'line_item_number');
        if (in_array($row_array['line_item_number'], $get_all_line_item)) {           
            return ["status" => false, "error" => "Line item '" . $row_array['line_item_number'] . "' already exists in the CSV for the specified dates. Please try importing the file again."];
        }

        $response = $this->Finance_line_item->check_line_item_number_already_exist((object) $row_array);
        if (!empty($response)) {
            return ["status" => false, "error" => "Line item '" . $row_array['line_item_number'] . "' already exists in the system for the specified dates. Please try importing the file again."];
        }
        return ["status" => true];
    }

    public function csv_validate_line_items($row_array) {

        $validation_rules = array(
            array('field' => 'support_registration_group', 'label' => 'support registration group', 'rules' => 'required'),
            array('field' => 'support_category', 'label' => 'Support category', 'rules' => 'required'),
            array('field' => 'line_item_number', 'label' => 'Line Item Number', 'rules' => 'required'),
            array('field' => 'line_item_name', 'label' => 'Line Item Name', 'rules' => 'required')
        );

        $this->form_validation->set_data($row_array);
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $response = ['status' => true];
        } else {
            $errors = $this->form_validation->error_array();
            $response = ['status' => false, 'error' => implode(', ', $errors)];
        }
        return $response;
    }
    
    public function check_line_item($item_name){      
        $this->db->select(array("id"));
        $this->db->from("tbl_finance_support_registration_group");
        $this->db->where("line_item_name='" . $item_name . "'");
        $res = $this->db->get();
        $res_array = $res->row_array();
        if (!empty($res_array)) {
            return ["status" => true, "data" => $res_array['id']];
        }
        return ["status"=>false];
    }

    public function getAllSupportBatchId() {
        $getData = $this->basic_model->get_record_where('finance_support_registration_group', ['id', 'batchId'], ['archive' => 0]);
        $arr_support_batch = array();
        foreach ($getData as $data) {
            $arr_support_batch[$data->batchId] = $data->id;
        }
        return $arr_support_batch;
    }

    public function updateSupportNumber() {
        $getData = $this->basic_model->get_record_where('finance_support_registration_group', ['id'], ['archive' => 0]);
        foreach ($getData as $data) {
           // echo $data->id;
            $where_columns = ["id" => $data->id];
            $data = ["batchId" => $data->id + 100];
            $this->db->update('tbl_finance_support_registration_group', $data, $where_columns);
        }
    }

    public function get_csv_line_item()
	{
        $requestData = request_handler('access_finance_admin');
        
        $data=$requestData->data;
        if(isset($data->exportType) && !empty($data->exportType)){            
        
        $record =  $this->Finance_line_item->get_line_item_csv_report($data->exportType);        
        $weekOfdays_list= $this->Finance_line_item->get_week_day_list();
        $timeOfdays_list= $this->Finance_line_item->get_time_of_day_list();
        $states_list= $this->Finance_line_item->get_state_list();
     
		$date = date('Y-m-d');
		
		$this->load->library("Excel");
		$object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        
        $colName=0; 
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Registration Group Number');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Registration Group Name');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Category Number');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Category');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Outcome Domain Number');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Outcome Domain');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Item Number');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Item Name');
		$object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Support Item Description');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Unit');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Price Controlled');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Quote Required');
        
        foreach($states_list as $key=>$value){
            $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, $key);            
        }
        foreach($weekOfdays_list as $key=>$value){
            $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, $key);            
        }
        foreach($timeOfdays_list as $key=>$value){
            $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, $key);            
        }
       
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'National Non-Remote');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'National Remote');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'National Very Remote');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'schedule constraint');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Travel');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Cancellations');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'NDIA Reporting');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Member Ratio');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Participant Ratio');
        $object->getActiveSheet()->setCellValueByColumnAndRow($colName++, 1, 'Non-F2F');

		if(!empty($record))
		{
            $var_row = 2;
            
			foreach ($record as $data) 
			{
                $col=0;

                $states = explode('@#_BREAKER_#@', $data->states);
                $timeofday = explode('@#_BREAKER_#@', $data->timeofday);
                $weekofday = explode('@#_BREAKER_#@', $data->weekofday);

                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_registration_group);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_registration_group_name);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_category);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_category_name);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_outcome_domain_name);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->support_outcome_domain);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->line_item_number);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->line_item_name);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->description);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'Unit');
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->price_control);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->quote_required);
                
            
                foreach($states_list as $key=>$value){                   
                    if(in_array($value,$states)){
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'Y');
                    }else{
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'N');
                    }
                }

                foreach($weekOfdays_list as $key=>$value){                   
                    if(in_array($value,$weekofday)){
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'Y');
                    }else{
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'N');
                    }
                }
                foreach($timeOfdays_list as $key=>$value){                   
                    if(in_array($value,$timeofday)){
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'Y');
                    }else{
                        $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'N');
                    }
                }
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->upper_price_limit);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->national_price_limit);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->national_very_price_limit);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, 'schedule constraint');
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->travel_required);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->cancellation_fees);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->ndis_reporting);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->member_ratio );
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->participant_ratio);
                $object->getActiveSheet()->SetCellValue(getNameFromNumber($col++).$var_row, $data->non_f2f);
                
				$var_row++;
			}          

			$object->setActiveSheetIndex()
			->getStyle('A1:D1')
			->applyFromArray(
				array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'C0C0C0:')
					)
				)
			);
		}
		else
		{
			$object->setActiveSheetIndex()
			->mergeCells('A2:I2');
			$object->getActiveSheet()
			->getCell('A2')
			->setValue('No detail found for selected filters.'); 
        }
		$filename = time().'_line__item_report'.'.csv';
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');	
        $filePath = FCPATH;
        $filePath .= ARCHIEVE_DIR . '/';	
        $response=$object_writer->save($filePath.$filename);        
        $csv_fileFCpath =$filePath.$filename;
        if(file_exists($csv_fileFCpath)) {          
            echo json_encode(['status' => true, 'filename' => $filename]);
            exit();
        }
        echo json_encode(['status' => false, 'error' => 'line item csv file not exist']);
        exit();
    }else{
        echo json_encode(['status' => false, 'error' => 'Please select line item export type first']);
        exit();
    }
    }
  
}