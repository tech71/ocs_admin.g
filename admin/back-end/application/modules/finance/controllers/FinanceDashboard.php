<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FinanceDashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Finance_quote_model');
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $this->loges->setLogType('finance_quote');
    }

    public function get_quote_dashboard_graph() {
        $reqData = request_handler('access_finance');
        $reqData = $reqData->data;

        if (isset($reqData)) {
            if ($reqData->type === 'genrated_quote' || $reqData->type == 'all') {
                $res['genrated_quote'] = $this->Finance_quote_model->get_quote_dashboard_graph('genrated_quote', $reqData);
            }

            echo json_encode(['status' => true, 'data' => $res]);
        }
    }

}
