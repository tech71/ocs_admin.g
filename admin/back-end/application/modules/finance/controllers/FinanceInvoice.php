<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FinanceInvoice extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(['Finance_invoice_model']);
        $this->load->model(['Finance_common_model']);
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
    }

    public function srch_shift_bydate() {
        $reqData = request_handler('access_finance');
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $result = $this->Finance_invoice_model->srch_shift_bydate($reqData);
            echo json_encode($result);
            exit();
        }
    }
    public function srch_statement_bydate() {
        $reqData = request_handler('access_finance');
        if (!empty($reqData->data)) {            
            $reqData = json_decode($reqData->data);
            
            $result = $this->Finance_invoice_model->srch_statement_bydate($reqData);
            echo json_encode($result);
            exit();
        }
    }    

    public function get_funding_type(){
        $reqData = request_handler('access_finance');
        $response = $this->basic_model->get_record_where('funding_type', $column = array('name as label', 'id as value'), $where = array('archive' => 0));
        $response = isset($response) && !empty($response) ? $response : array();
        echo json_encode(array('status'=>true,'data'=>$response));
        exit();
    }

    public function get_line_item_by_funding_type() {
        $reqData = request_handler('access_finance');
        if (!empty($reqData->data)) {
            $result = $this->Finance_invoice_model->get_line_item_by_funding_type($reqData->data);
            echo json_encode($result);
            exit();
        }
    } 

    public function save_invoice() {
        $reqData = request_handler('access_finance_payrate');
        if (!empty($reqData->data)) 
        {
            $admin_id = $reqData->adminId;
            $reqData =  $reqData->data;
            $data =(array) $reqData;
            $validation_rules = array(
                array('field' => 'pay_by', 'label' => 'Pay By', 'rules' => 'required'),
                //array('field' => 'invoice_shift_start_date', 'label' => 'PayRate Start Date', 'rules' => 'required'),
                //array('field' => 'invoice_shift_end_date', 'label' => 'PayRate End Date', 'rules' => 'required'),
                //array('field' => 'srchShiftList[]', 'label' => 'Shifts', 'rules' => 'callback_check_srch_shift[' . json_encode($reqData->srchShiftList) . ']'),
                array('field' => 'lineItemInvoice[]', 'label' => 'lineItemInvoice', 'rules' => 'callback_check_invoice_lineitem[' . json_encode($reqData->lineItemInvoice) . ']'),
                array('field' => 'manualInvoiceItem[]', 'label' => 'manualInvoiceItem', 'rules' => 'callback_check_miscellaneous_item[' . json_encode($reqData->lineItemInvoice) . ']'),
            );

            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) 
            {
                if (isset($reqData->payrate_id) && $reqData->payrate_id > 0) {              
                    #$this->Finance_invoice_model->add_payrate($reqData);
                    $msg = 'Invoice Updated successfully.';
                } else {
                    $this->Finance_invoice_model->save_invoice($reqData);
                    $msg = 'Invoice created successfully.';                       
                }
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'msg' => implode("\n", $errors));
            }
            echo json_encode($return);
        }
    }

    public function save_statemenets() {
        $reqData = request_handler('access_finance_payrate');
               
        if (!empty($reqData->data)) 
        {            
            $admin_id = $reqData->adminId;
            $reqData =  $reqData->data;
            $data =(array) $reqData;           
            $validation_rules = array(
                array('field' => 'statement_from_date', 'label' => 'statement from date', 'rules' => 'required'),
                array('field' => 'statement_to_date', 'label' => 'statement to date', 'rules' => 'required'),
                array('field' => 'statement_issue_date', 'label' => 'statement issue date', 'rules' => 'required'),             
                array('field' => 'statement_due_date', 'label' => 'statement due date', 'rules' => 'required'),                
                array('field' => 'statement_shift_notes', 'label' => 'Add Notes To Quote', 'rules' => 'required'),
                array('field' => 'UserId', 'label' => 'user (participant) ', 'rules' => 'required'),
                array('field' => 'UserTypeInt', 'label' => 'User Type required', 'rules' => 'required'),
                array('field' => 'srchShiftList_data', 'label' => 'manualInvoiceItem', 'rules' => 'callback_check_invoice_data[' . json_encode($reqData) . ']'),
            );

            $this->form_validation->set_data($data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) 
            {
               $response=$this->Finance_invoice_model->save_statemenets($reqData);
               if($response['status']){
                    $UserId=$reqData->UserId;
                    $statement_id=$response['statement'];                   
                   // $statement_id=$reqData->data->statement_id;
                    $result = $this->Finance_invoice_model->get_send_statement_mail($statement_id);
                    $result['statement']= $statement_id;
                    $sendMail=$this->send_statement_email($result);             
                  
                    if($sendMail['status']){
                        $this->basic_model->update_records('finance_statement', ['status' => 1], ['id' => $statement_id]);
                        $return = array('status' => true, 'msg' => 'Statement created successfully.'); 
                    }else{
                        $return = array('status' => true, 'msg' => 'Statement created successfully send mail failed.'); 
                    }                    
               }else{
                    $return = array('status' => true, 'error' => 'Create Statement failed'); 
                }
            }else{
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'msg' => implode("\n", $errors));
            }
            echo json_encode($return);
        }
    }
    public function send_statement_email($statementData){

            $filePath=FINANCE_STATEMENT_FILE_PATH;
            $fileFCPath=FCPATH.$filePath.$statementData['file'];
            if(file_exists($fileFCPath)){ 
                $statementData['filepath']=$fileFCPath;  
                $statementData['subject']=$statementData['file'];              
                send_finanance_statement_email($statementData);               
                return array('status' => true);
            }else{
                return array('status' => false, 'error' => "file not found");                
            }
    }



    public function check_invoice_data($sourceData,$reqData) {        
        $reqData=json_decode($reqData,true);      
        if(!empty($reqData['srchShiftList'])){
            foreach($reqData['srchShiftList'] as $shiftList){
                if($shiftList['id'] > 0){

                }else{
                    $this->form_validation->set_message('check_invoice_data', 'null invoice not accepted please select right invoice.');
                    return false; 
                }
            }
        }else{
            $this->form_validation->set_message('check_invoice_data', 'Invoice statement list required.');
            return false;
        }
        return true;
    }

    public function check_srch_shift($shift_data){
        if (empty($shift_data)) {
            $this->form_validation->set_message('check_srch_shift', 'Need atleat 1 shift to create invoice.');
            return false;
        }
    }

    public function check_invoice_lineitem($invoice_lineitem) {
        if (!empty($invoice_lineitem)) {
            if(!empty($invoice_lineitem->fundingType)){
                if(empty($invoice_lineitem->lineItem)){
                    $this->form_validation->set_message('check_invoice_lineitem', 'Please select line item.');
                    return false;
                }

                if(empty($invoice_lineitem->quantity)){
                    $this->form_validation->set_message('check_invoice_lineitem', 'Please add quantity.');
                    return false;
                }
            }
        } 
        return true;
    } 

    public function check_miscellaneous_item($manualInvoiceItem) {
        if (!empty($manualInvoiceItem)) {
            if(!empty($manualInvoiceItem->itemName)){
                if(empty($manualInvoiceItem->itemDescription)){
                    $this->form_validation->set_message('check_miscellaneous_item', 'Please add description.');
                    return false;
                }

                if(empty($manualInvoiceItem->itemCost)){
                    $this->form_validation->set_message('check_miscellaneous_item', 'Please add itemcost.');
                    return false;
                }
            }
        } 
        return true;
    }

    public function dashboard_invoice_list(){
        $reqData = request_handler('access_finance_invoice');
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $result = $this->Finance_invoice_model->dashboard_invoice_list($reqData);
            echo json_encode($result);
            exit();
        }
    }

    public function ndis_invoice_list(){
        $reqData = request_handler('access_finance_ndis_invoice');
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $result = $this->Finance_invoice_model->dashboard_invoice_list($reqData);
            echo json_encode($result);
            exit();
        }
    }

    public function get_participant_organization_name() {
        $reqData = request_handler('access_finance');

        if (isset($reqData->data->search)) {
            $res = $this->Finance_invoice_model->get_participant_organization_name($reqData->data);

            echo json_encode(['status' => true, 'data' => $res]);
        }
    }

    public function invoice_scheduler_list(){
        $reqData = request_handler('access_finance_invoice');
      
        if (!empty($reqData->data)) {
            //$reqData = json_decode($reqData->data);
            $result = $this->Finance_invoice_model->invoice_scheduler_list($reqData->data);
            echo json_encode($result);
            exit();
        }
    }
    public function auto_invoice_create_shift_completed_by_shfit_id(){
        $shiftId = $this->input->post('shift_id');
        if(!empty($shiftId)){
            $res =$this->Finance_invoice_model->create_invoice_by_shift_id($shiftId);
        }
        exit();
    }
    public function get_invoice_scheduler_history_list(){
        $reqData = request_handler('access_finance_invoice');
        
        if (!empty($reqData->data)) {
            $result = $this->Finance_invoice_model->get_invoice_scheduler_history_list($reqData->data);
            echo json_encode($result);
            exit();
        }
    }

    function get_invoice_pdf() {
        $reqData = request_handler('access_finance_invoice');
        
        if (isset($reqData->data->invoiceId)) {
            $res = $this->Finance_invoice_model->get_invoice_pdf($reqData->data->invoiceId);
            //print_r($res);
            if (!empty($res)) {
                if (!empty($res->invoice_file_path)) {
                    $pdf_fileFCpath = FCPATH . FINANCE_INVOICE_FILE_PATH . $res->invoice_file_path;
                    if (file_exists($pdf_fileFCpath)) {
                        $pdf_filePath = base_url() . 'mediaShowDocument/fi/' . urlencode(base64_encode('0')).'/'.urlencode(base64_encode($res->invoice_file_path)).'/'. urlencode($res->invoice_file_path) ;
                        $data = ['file_path' => $pdf_filePath, 'file_name' => $res->invoice_file_path, 'invoice_for' => $res->invoice_for];
                        
                        echo json_encode(['status' => true, 'data' => $data]);
                        exit();
                    } else {
                        echo json_encode(['status' => false, 'error' => 'Invoice pdf file not exist.']);
                        exit();
                    }
                } else {
                    echo json_encode(['status' => false, 'error' => 'Invoice pdf file name not exist.']);
                    exit();
                }
            } else {
                echo json_encode(['status' => false, 'error' => 'Records not exist.']);
                exit();
            }
        }
    }

    public function resend_invoice_mail(){
        $reqData = request_handler('access_finance_invoice');
        if (isset($reqData->data->invoiceId)) {
            $res = $this->Finance_invoice_model->resend_invoice_mail($reqData->data->invoiceId);
            if($res){
                echo json_encode(['status' => true, 'msg' => 'Mail send successfully.']);
                exit();
            }else{
                echo json_encode(['status' => false, 'error' => 'Something went wrong.']);
                exit(); 
            }
        }else {
            echo json_encode(['status' => false, 'error' => 'Invalid Request.']);
            exit();
        }
    }

    public function update_invoice_status(){
        $reqData = request_handler('access_finance_invoice');
        $invoiceStatusData = json_decode(INVOICE_PAYMENT_STATUS_DATA);
        $invoiceStatusData = array_map('strtolower',$invoiceStatusData);
        $invoiceStatusData = array_flip($invoiceStatusData);
        
        if (isset($reqData->data->invoiceId) && isset($reqData->data->status)&& isset($invoiceStatusData[$reqData->data->status]) && in_array($invoiceStatusData[$reqData->data->status],['1','2'])) {
            $res = $this->Finance_invoice_model->update_invoice_status($reqData->data->invoiceId,$invoiceStatusData[$reqData->data->status]);
            echo json_encode($res);
            exit();
        }else {
            echo json_encode(['status' => false, 'error' => 'Invalid Request.']);
            exit();
        }
    }
    public function dashboard_finance_invoice_statement_list(){
        $reqData = request_handler('access_finance');
        if (!empty($reqData->data)) {
            //$reqData = json_decode($reqData->data,true);
            $result = $this->Finance_invoice_model->dashboard_statement_list($reqData);
            echo json_encode($result);
            exit();
        }
    }

    public function ndis_export_csv(){
        $reqData = request_handler('access_finance_ndis_invoice');
        
        if (!empty($reqData->data)) {
            $this->load->model('Finance_common_model');
            $res = $this->Finance_invoice_model->dashboard_invoice_list($reqData->data,['exportCall'=>true]);
            $res = !empty($res) ? obj_to_arr($res): [];
            $dataRes = !empty($res) && isset($res['data']) ? $res['data']:[];
            $dataHearder= ['invoice_number'=>'Invoice Number','description'=>'Description','invoice_for'=>'Invoice For','addressto'=>'Address To','amount'=>'Amount','invoice_date'=>'Invoice Date','pay_by'=>'Payment By','invoice_status'=> 'Status','ndis_status'=> 'NDIS Status'];
            $response = $this->Finance_common_model->export_csv($dataHearder,$dataRes,['file_name'=>'NDIS_invoice_'.date('d_m_Y_H_i_s').'.csv']);
            if($response['status'] == true){
                $ins_data = [
                    'file_name'=>$response['filename'],
                    'created_by'=>$reqData->adminId,
                    'created'=>DATE_TIME
                ];
                $this->basic_model->insert_records('finance_invoice_ndis_export_log',$ins_data);
            }
            echo json_encode($response);
            exit();
        }else{
            echo json_encode(['status' => false, 'error' => 'Invalid Request.']);
            exit();
        }
    }


    public function read_csv_ndis_invoice_status(){
        $data = request_handlerFile('access_finance_ndis_invoice');
        $checkData = (array) $data;
        $validation_rules = array(
            array('field' => 'file_title', 'label' => 'File Title', 'rules' => 'required'),
        );
        if (empty($_FILES['docsFile']['name'])) {
            $validation_rules[] = array('field' => 'docsFile', 'label' => 'Document File', 'rules' => 'required');
        }
        // set rules form validation
        $this->form_validation->set_rules($validation_rules);
        $this->form_validation->set_data($checkData);
        // check requested data return true or false
        if ($this->form_validation->run()) {
            $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(in_array($_FILES['docsFile']['type'],$mimes)){ 
                $tmpName = $_FILES['docsFile']['tmp_name'];     
                $file = array_map('str_getcsv', file($tmpName));                               
                $invoice_status_data=[];
                if(!empty($file)){
                    $header = array_shift($file);
                    $header = array_map('strtolower',$header);
                    $header = array_map('trim',$header);
                    $columnRequired = array_diff(['invoice number','ndis status'],$header);
                    if(count($columnRequired)>0){
                        $return =["status"=>false,"error" => "Required column missing in file import.Please try importing the file again."];
                    }else{
                        foreach($file as $row) {
                            if(count($row)== count($header)){
                                $row = array_map("utf8_encode", $row);
                                $row = array_map("strtolower", $row);
                                $row = array_map("trim", $row);
                                $invoice_status_data[] = array_combine($header, $row);
                            }else{
                                $return =["status"=>false,"error" => "Unsuccessful file import. Please try importing the file again."];
                            break;                  
                            }
                        }
                                  
                        if(!empty($invoice_status_data)){
                            $return=$this->Finance_invoice_model->check_and_update_invoice_status_data($invoice_status_data);
                            if($return['status']){
                                
                                $config['upload_path'] = FINANCE_IMPORT_NDIS_STATUS_UPLOAD_PATH; // user here constact for specific path
                                $config['input_name'] = 'docsFile';
                                $config['remove_spaces'] = false;
                                $config['directory_name'] = '';
                                $config['allowed_types'] = FINANCE_IMPORT_NDIS_STATUS_UPLOAD_TYPE; //'csv';

                                $is_upload = do_upload($config); // upload file
                                // check here file is uploaded or not return key error true
                                if (isset($is_upload['error'])) {
                                    // return error comes in file uploading
                                    //echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                                    ///exit();
                                } else {
                                    $insert_ary = array(
                                        'file_path' => $is_upload['upload_data']['file_name'],
                                        'file_title' => $data->file_title,
                                        'response' => json_encode($return),
                                        'created_by' => $data->adminId,
                                        'created' => DATE_TIME,
    
                                    );
                                     $this->basic_model->insert_records('finance_invoice_ndis_status_import_log', $insert_ary, $multiple = FALSE);
                                }
                            }
                        }else{
                            $return = ['status' => false, 'error' => 'file item row invalid data'];
                        }
                    }
                }else{
                    $return =['status' => false, 'error' => 'File invalid data'];
                }
            }else{
                $return =['status' => false, 'error' => 'File not valid extension.'];
            }      

        }else{
            // return error else data data not valid
            $errors = $this->form_validation->error_array();
            $return = array('status' => false, 'error' => implode(', ', $errors));
        }
        echo json_encode($return);
        exit();
    }

    public function get_dashboard_graph_count(){
        $reqData = request_handler('access_finance_invoice');
        $return =['status' => false, 'data' =>[],'error'=>'Invalid Request.'];
        if (!empty($reqData->data)) {
            $res = false;
            if($reqData->data->mode=='invoice_income'){
                $res = $this->Finance_invoice_model->get_income_from_invoice($reqData->data->type);
            }else if($reqData->data->mode=='loss_credit_note'){
                $res = $this->Finance_invoice_model->get_loss_credit_notes_invoice($reqData->data->type);
            }else if($reqData->data->mode=='loss_of_refund'){
                $res = $this->Finance_invoice_model->get_loss_refund_invoice($reqData->data->type);
            }else if($reqData->data->mode=='invoice_income_this_month'){
                $res = $this->Finance_invoice_model->get_invoice_money('month');
            }
            else if($reqData->data->mode=='invoice_credited_and_refund_paid_this_month'){
                $res = $this->Finance_invoice_model->get_invoice_credited_and_refund_paid('month');
            }
            else if($reqData->data->mode=='invoice_profit_this_month'){
                $res = $this->Finance_invoice_model->get_invoice_profit_money('month');
            }
            if($res!==false){
                $return = ['status'=>true,'data'=>$res];
            }
        }
        echo json_encode($return);
        exit();
    }

    function get_statement_pdf() {
        $reqData = request_handler('access_finance_invoice');      
       
        if (isset($reqData->data->invoiceId)) {          
            $res = $this->Finance_invoice_model->get_statement_pdf($reqData->data->invoiceId);            
            $res=obj_to_arr($res);
            if (!empty($res['status'])) {

                if (!empty($res['data']['statement_file_path'])) {
                    $fileName=$res['data']['statement_file_path'];
                    $pdf_fileFCpath = FCPATH.FINANCE_STATEMENT_FILE_PATH.$fileName;
                    if (file_exists($pdf_fileFCpath)) {
                        $pdf_filePath = base_url() . 'mediaShowDocument/fs/' . urlencode(base64_encode('0')).'/'.urlencode(base64_encode($fileName)).'/'. urlencode($fileName);
                        $data = ['file_path' => $pdf_filePath, 'file_name' => $fileName, 'invoice_for' => $fileName];
                        
                        echo json_encode(['status' => true, 'data' => $data]);
                        exit();
                    } else {
                        echo json_encode(['status' => false, 'error' => 'Invoice pdf file not exist.']);
                        exit();
                    }
                } else {
                    echo json_encode(['status' => false, 'error' => 'statement pdf file name not exist.']);
                    exit();
                }
            } else {
                echo json_encode(['status' => false, 'error' => 'Records not exist.']);
                exit();
            }
        }
    }

    public function get_ndis_dashboard_graph_count(){
        $reqData = request_handler('access_finance_ndis_invoice');
        $return =['status' => false, 'data' =>[],'error'=>'Invalid Request.'];
        if (!empty($reqData->data)) {
            $res = -1;
            if($reqData->data->mode=='ndis_money_in'){
                $res = $this->Finance_invoice_model->get_ndis_invoice_money($reqData->data->type);
            }else if($reqData->data->mode=='ndis_invoice_error_rejected'){
                $res = $this->Finance_invoice_model->get_invoice_ndis_rejected($reqData->data->type);
            }else if($reqData->data->mode=='invoice_error_rejected'){
                $res = $this->Finance_invoice_model->get_invoice_rejected($reqData->data->type);
            }else if($reqData->data->mode=='last_ndis_billing'){
                $res = $this->Finance_invoice_model->get_last_ndis_billing();
            }else if($reqData->data->mode=='ndis_invoice_error_rejected_monthwise_financial_year'){
                $res = $this->Finance_invoice_model->get_ndis_invoice_rejected_monthwise_financial_year();
            }
            if($res>-1){
                $return = ['status'=>true,'data'=>$res];
            }
        }
        echo json_encode($return);
        exit();
    }

    public function get_pending_invoice_list_for_credit_notes(){
        $reqData = request_handler('access_finance_credit_note_and_refund');
        if (!empty($reqData->data)) {
            $reqData = ($reqData->data);
            $result = $this->Finance_invoice_model->dashboard_invoice_list($reqData,['creditNoteCall'=>true]);
            echo json_encode($result);
            exit();
        }
    }   
    function resend_statemenets(){
       $reqData = request_handler('access_finance_ndis_invoice');       
       if (!empty($reqData->data)) {
            $statement_id=$reqData->data->statement_id;
            $result = $this->Finance_invoice_model->get_send_statement_mail($statement_id);
            $result['statement']= $statement_id;
            $sendMail=$this->send_statement_email($result); 
            if($sendMail['status']){            
                $return = array('status' => true, 'msg' => 'Send mail statement successfully.'); 
            }else{
                $return = array('status' => true, 'msg' => 'Send maile Statement failed.'); 
            }
        }else{
            $return = array('status' => true, 'msg' => 'something went wrong.'); 
        }
    }
    function get_outstanding_statements_participants_count(){
        $reqData = request_handler('access_finance_ndis_invoice');       
        if (!empty($reqData->data)) {             
             $result = $this->Finance_invoice_model->get_outstanding_statements_participants_count($reqData->data->view_type);
             echo json_encode(['status' => true, 'data' => $result]);
         }else{
            echo json_encode(['status' => true, 'msg' => 'something went wrong.']); 
         }
    }
    function get_outstanding_statements_organization_count(){
        $reqData = request_handler('access_finance_ndis_invoice');       
        if (!empty($reqData->data)) {
             $result = $this->Finance_invoice_model->get_outstanding_statements_organization_count($reqData->data->view_type);
             echo json_encode(['status' => true, 'data' => $result]);
         }else{
            echo json_encode(['status' => true, 'msg' => 'something went wrong.']); 
         }
    }
}
