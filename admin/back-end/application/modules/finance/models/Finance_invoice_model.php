<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Finance_invoice_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function srch_shift_bydate($reqData) {

        $this->load->model('Finanace_shift_payroll_model');
        $shift_partcipant_name = $this->Finanace_shift_payroll_model->get_shift_participant_sub_query_by_other();
        $shift_site_name = $this->Finanace_shift_payroll_model->get_shift_site_sub_query_by_other();

        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 's.id';
            $direction = 'DESC';
        }

        if (!empty($filter->invoice_shift_start_date)) {
            $this->db->where("DATE(s.start_time) >= '" . date('Y-m-d', strtotime($filter->invoice_shift_start_date)) . "'");
        }
        if (!empty($filter->invoice_shift_end_date)) {
            $this->db->where("DATE(s.start_time) <= '" . date('Y-m-d', strtotime($filter->invoice_shift_end_date)) . "'");
        }

        $select_column = array("s.id", "s.start_time", "s.end_time", 's.booked_by', '"Saturday -Active Over Night" as invoiceschedule', '"400" as cost', '"outstanding" as outstanding');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_shift as s');

        $this->db->select("CASE WHEN s.booked_by=1 THEN (" . $shift_site_name . ") WHEN s.booked_by=2 THEN (" . $shift_partcipant_name . ") ELSE '' END as invoice_for", false);

        if (isset($filter->UserType) && $filter->UserType == 'participant')
            $this->db->join('tbl_shift_participant as sp', 'sp.participantId = ' . $filter->UserId, 'inner');
        else if (isset($filter->UserType) && $filter->UserType == 'finance')
            $this->db->join('tbl_shift_site as ss', 'ss.siteId = ' . $filter->UserId, 'inner');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where(array('s.status' => 6, 's.status!=' => 8));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $dataResult = $query->result();
        if (!empty($dataResult)) {
            
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }
    
    public function srch_statement_bydate($reqData){

        $filter = $reqData->filtered;
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;

         
        $userType=$filter->UserType;
        $userId=$filter->UserId;

        $select_column = array("f.id", "f.invoice_id","DATE_FORMAT(f.pay_by, '%d/%m/%Y') as shift_due");
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);       
        $this->db->select("CASE WHEN f.invoice_type=1 THEN 'shift' ELSE 'manual invoice' END AS invoice_type", FALSE);
        $this->db->where(array('f.archive' => 0, 'f.invoice_for =' =>$userId, 'f.booked_by =' => $userType));

        if (!empty($filter->statement_from_date)) {
            $this->db->where("DATE(f.invoice_shift_start_date) >= '" . date('Y-m-d', strtotime($filter->statement_from_date)) . "'");
        }
        if (!empty($filter->statement_to_date)) {
            $this->db->where("DATE(f.invoice_shift_end_date) <= '" . date('Y-m-d', strtotime($filter->statement_to_date)) . "'");
        }

        $this->db->from('tbl_finance_invoice as f');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());        
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
       
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $dataResult = $query->result();
        if (!empty($dataResult)) {
            
        }
       
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    public function get_line_item_by_funding_type($reqData) {
        if (!empty($reqData->previous_selected_item)) {
            $lineitemIds = array_column(obj_to_arr($reqData->previous_selected_item), 'line_ItemId');
            if (!empty($lineitemIds)) {
                $lineitemIds = array_column($lineitemIds, 'value');
                $this->db->where_not_in('id', $lineitemIds);
            }
        }

        $this->db->select(["line_item_number as label", "id as value", "upper_price_limit", "national_price_limit", "national_very_price_limit"]);
        $this->db->from('tbl_finance_line_item');
        $this->db->where('funding_type', $reqData->funding_type);
        $this->db->group_start();
        $this->db->like("line_item_name", $reqData->search);
        $this->db->or_like("line_item_number", $reqData->search);
        $this->db->group_end();
        $query = $this->db->get();
        $res = $query->result_array();
        return array('status' => true, 'data' => $res);
    }

    public function save_invoice($reqData) {
        if (!empty($reqData)) {
            /*if (isset($reqData->filtered)) {
                $filter = $reqData->filtered;
            }*/
            
            $invoice_for = $reqData->UserId;
            $booked_by = $reqData->UserTypeInt;
            $invoice_data = array('pay_by' => isset($reqData->pay_by) ? $reqData->pay_by : '',
                //'invoice_shift_start_date' => isset($reqData->invoice_shift_start_date) ? DateFormate($reqData->invoice_shift_start_date) : '',
                //'invoice_shift_end_date' => isset($reqData->invoice_shift_end_date) ? DateFormate($reqData->invoice_shift_end_date) : '',
                'invoice_shift_notes' => isset($reqData->invoice_shift_notes) ? $reqData->invoice_shift_notes : '',
                'line_item_notes' => isset($reqData->line_item_notes) ? $reqData->line_item_notes : '',
                'manual_invoice_notes' => isset($reqData->manual_invoice_notes) ? $reqData->manual_invoice_notes : '',
                'invoice_for' => isset($invoice_for)?$invoice_for:'',
                'booked_by' => isset($booked_by)?$booked_by:'',
            );

            if (isset($reqData->invoice_id) && $reqData->invoice_id > 0) {
                
            } else {
                $invoice_data['invoice_type'] =2;
                $invoice_data['created'] = DATE_TIME;
                $invoice_data['invoice_date'] = DATE_CURRENT;
                $invoice_id = $this->basic_model->insert_records('finance_invoice', $invoice_data, $multiple = FALSE);
            }
            $sub_total = 0;

            if (!empty($reqData->lineItemInvoice)) {
                $ins_ary = [];
                foreach ($reqData->lineItemInvoice as $key => $value) {
                    if ($value->funding_type != '') {

                        $cost = $value->lineItem->national_price_limit ;

                        $ins_ary[] = array('invoice_id' => $invoice_id,
                            'funding_type' => $value->funding_type,
                            'line_item' => $value->lineItem->value,
                            'quantity' => $value->quantity,
                            'cost' => $cost
                        );
                        $sub_total = $sub_total + ($cost * $value->quantity);
                    }
                }

                if (!empty($ins_ary)) {
                    if (isset($reqData->mode) && $reqData->mode == 'edit')
                        $this->basic_model->update_records('finance_manual_invoice_line_items', ['archive' => 1], ['invoice_id' => $invoice_id]);

                    $this->basic_model->insert_records('finance_manual_invoice_line_items', $ins_ary, $multiple = TRUE);
                }
            }

            if (!empty($reqData->manualInvoiceItem)) {
                $item_mis = [];
                foreach ($reqData->manualInvoiceItem as $key => $val) {
                    if ($val->item_name != '') {
                        $item_mis[] = array('invoice_id' => $invoice_id,
                            'item_name' => $val->item_name,
                            'item_description' => $val->item_description,
                            'item_cost' => $val->item_cost ?? 0,
                        );
                        $sub_total = $sub_total+$val->item_cost??0;
                    }
                }

                if (isset($reqData->mode) && $reqData->mode == 'edit')
                    $this->basic_model->update_records('finance_manual_invoice_miscellaneous_items', ['archive' => 1], ['invoice_id' => $invoice_id]);

                if (!empty($item_mis))
                    $this->basic_model->insert_records('finance_manual_invoice_miscellaneous_items', $item_mis, TRUE);
            }

            if (!empty($reqData->srchShiftList)) {
               /* $shifts = [];
                foreach ($reqData->srchShiftList as $shift) {
                    $shifts[] = array('invoice_id' => $invoice_id,
                        'shift_id' => $shift->id,
                    );
                }

                if (isset($reqData->mode) && $reqData->mode == 'edit')
                    $this->basic_model->update_records('finance_invoice_shifts', ['archive' => 1], ['invoice_id' => $invoice_id]);

                $this->basic_model->insert_records('finance_invoice_shifts', $shifts, TRUE);*/
            }

            $gst = calculate_gst($sub_total);
            $total = $sub_total + $gst;
            $this->basic_model->update_records('finance_invoice', ['total' => $total, 'gst' => $gst, 'sub_total' => $sub_total], ['id' => $invoice_id]);

            if ($invoice_id > 0)
                return TRUE;
            else
                return FALSE;
        }
        return FALSE;
    }

    public function save_statemenets($reqData) {
        $statement_id=0;
        if (!empty($reqData)) {          
            $fileName='statement.pdf';
            
            $shift=$reqData->srchShiftList;
            $ids = array_column($shift, 'id');
            $str = implode (",",$ids);
            $amountData=$this->get_invoice_amount_data($str);
            $result_amounts=pos_index_change_array_data($amountData,'id');
            
            $statement_data = array('from_date' => isset($reqData->statement_from_date) ? date('Y-m-d', strtotime($reqData->statement_from_date)) : '',
                'to_date' => isset($reqData->statement_to_date) ? date('Y-m-d', strtotime($reqData->statement_to_date)) : '',
                'issue_date' => isset($reqData->statement_issue_date) ? date('Y-m-d', strtotime($reqData->statement_issue_date)) : '',
                'due_date' => isset($reqData->statement_due_date) ? date('Y-m-d', strtotime($reqData->statement_due_date)) : '',
                'statement_notes' => isset($reqData->statement_shift_notes) ? $reqData->statement_shift_notes : '',
                'statement_for' => isset($reqData->filtered->UserId) ? $reqData->filtered->UserId : 0,
                'statement_file_path'=>$fileName,
                'statement_type' => 2,
                'booked_by'=>isset($reqData->UserTypeInt) ? $reqData->UserTypeInt : 0,                
                'status' =>0,
                'booker_mail'=>isset($reqData->bookerCheck) ? 1:0,
                'total'=>0.00,
                'archive' => 0,
                'created' => DATE_TIME,                
            );
           
            $statement_id = $this->basic_model->insert_records('finance_statement', $statement_data, $multiple = FALSE);

            $invoice_pdf_rows='';         
            $statment_list=array();
            $total_amount=0.00;
            $sub_total_ex_gst=0.00;
            $gst_total=0.00;
            if(!empty($reqData->srchShiftList)){
                foreach($reqData->srchShiftList as $shiftList){
                    $invoice=array('total'=>0.00,'gst'=>0.00,'sub_total'=>0.00);
                    if(!empty($result_amounts[$shiftList->id]) && $result_amounts[$shiftList->id]){                        
                        $invoice_amt=$result_amounts[$shiftList->id];
                        $invoice['total']=$invoice_amt['total'];
                        $invoice['gst']=$invoice_amt['gst'];
                        $invoice['sub_total']=$invoice_amt['sub_total'];
                        
                        $total_amount+=$invoice_amt['total'];
                        $sub_total_ex_gst+=$invoice_amt['sub_total'];
                        $gst_total+=$invoice_amt['gst'];

                        $invoice_pdf_rows.='<tr><td class="text-center  br">'.$invoice_amt['invoice_id'].'</td> <td class="text-left  br">Assistance with self care activites - weekdays (Daytime)</td>
                        <td class="text-left">$'.$invoice_amt['sub_total'].'</td></tr>';
                    }

                   
                   
                    $data=array('invoice_id'=>$shiftList->id,'statement_id'=>$statement_id,'archive'=>0);
                    $data=array_merge($data,$invoice);
                    $statment_list[]=$data;
                }
            }            
            
            if(!empty($statment_list)){
                 $booked_by=isset($reqData->UserTypeInt) ? $reqData->UserTypeInt : 0;
                 $invoice_for=isset($reqData->filtered->UserId) ? $reqData->filtered->UserId : 0;
                 /* user details start */
                 if($booked_by==1){
                    $rowUserDetails = $this->Finance_common_model->get_site_details_by_id($invoice_for);
                }else if($booked_by==2 || $booked_by==3){
                    $rowUserDetails = $this->Finance_common_model->get_participant_details_by_id($invoice_for);
                }else if($booked_by==4 || $booked_by==5){
                    $rowUserDetails = $this->Finance_common_model->get_organisation_details_by_id($invoice_for);
                }

                /* user details end */                 
                $this->basic_model->insert_update_batch($action='insert',$table_name='finance_statement_attach',$statment_list);
                $this->basic_model->update_records('finance_statement', array('total' => $total_amount), $where = array('id' => $statement_id));
                $res_statement = $this->basic_model->get_record_where('finance_statement', $column = array('statement_number'), $where = array('id'=>$statement_id,'archive'=>0));
                
                $statement_pdf_data['statement_no']=0;
                if(!empty($res_statement)){
                    $statement_pdf_data['statement_no']=$res_statement[0]->statement_number;
                }
                
                $statement_pdf_data['due_date']=isset($reqData->statement_due_date) ? date('d/m/Y', strtotime($reqData->statement_due_date)):'';
                $statement_pdf_data['issue_date']=isset($reqData->statement_issue_date) ? date('d/m/Y', strtotime($reqData->statement_issue_date)):'';
                $statement_pdf_data['invoice_rows']=$invoice_pdf_rows;
                $statement_pdf_data['total_amount']=$total_amount;
                $statement_pdf_data['sub_total_amount']=$sub_total_ex_gst;
                $statement_pdf_data['total_gst']=$gst_total;
                $statement_pdf_data['user_details']=$rowUserDetails;

                $statement_file_name=$this->get_statement_pdf($statement_id);
                $res=obj_to_arr($statement_file_name);
                $fileName='';
                if (!empty($res['status'])) {
                    if (!empty($res['data']['statement_file_path'])) {
                        $fileName=$res['data']['statement_file_path'];
                    }
                    $this->basic_model->update_records('finance_statement', array('statement_file_path' => $fileName), $where = array('id' => $statement_id));
                }
            }
            
            if ($statement_id > 0)
               return array('status'=>true,'statement'=>$statement_id,'filepath'=>$fileName);
        else
            return array('status'=>false);
        }
        return array('status'=>false);
    }

    public function get_invoice_amount_data($invoiceIds){
        $this->db->select("fi.invoice_id,fi.id,fi.total,fi.sub_total,fi.gst");
        $this->db->from("tbl_finance_invoice as fi");        
        $this->db->where_in("fi.id",$invoiceIds,FALSE);
       
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $result = $query->result();
    }
    
    public function create_statement_pdf($statement_pdf_data) {
        $path = base_url('assets/img/ocs_logo.png');
        error_reporting(0);

        $invoice_pdf_rows='';
        $total_amount=0.00;
        $sub_total_ex_gst=0.00;
        $gst_total=0.00;                       
        if(!empty($statement_pdf_data['attach'])){
            foreach($statement_pdf_data['attach'] as $attachList){
                    
                    $invoice_pdf_rows.='<tr><td class="text-center  br">'.$attachList['invoice_number'].'</td> <td class="text-left  br">Assistance with self care activites - weekdays (Daytime)</td>
                    <td class="text-left">$'.$attachList['sub_total'].'</td></tr>';
                   
                    $total_amount+=$attachList['total'];
                    $gst_total+=$attachList['gst'];
                    $sub_total_ex_gst+=$attachList['sub_total'];                   
            }

            $statement_pdf_data['pdf_total']=$total_amount;
            $statement_pdf_data['pdf_sub_total']=$sub_total_ex_gst;
            $statement_pdf_data['pdf_gst']=$gst_total;
        } else{
            $statement_pdf_data['pdf_total']=0.00;
            $statement_pdf_data['pdf_sub_total']=0.00;
            $statement_pdf_data['pdf_gst']=0.00;
        }
        $statement_pdf_data['invoice_rows']=$invoice_pdf_rows;               
        $data=$statement_pdf_data;
        $data['logo_path'] = $path;
                
        $data['type'] = 'footer';
        $footerData = $this->load->view('create_invoice_statement_pdf', $data, true);

        $data['type'] = 'content';
        $file = $this->load->view('create_invoice_statement_pdf', $data, true);

        $this->load->library('m_pdf');
        $pdf = $this->m_pdf->load(); 
        $pdf->AddPage('L');
        $pdf->WriteHTML($file);
        $pdf->setFooter($footerData); 
        $rand = date('d_m_Y_hisa');
        $filename = 'statement_'.$statement_pdf_data['statement_number'].'_'.$rand.'.pdf';
        $pdfFilePath = FINANCE_STATEMENT_FILE_PATH . $filename;
        $pdf->Output($pdfFilePath, 'F');
        return $filename;     
    }

    public function shift_caller_sub_query() {
       /*  (SELECT CONCAT_WS(sub_sc.firstname,sub_sc.lastname) from tbl_shift_caller as sub_sc 
        INNER JOIN tbl_finance_invoice_shifts as sub_fis ON sub_fis.shift_id=sub_sc.shiftId AND sub_fis.archive=0
        where sub_fis.invoice_id=fi.id LIMIT 1) */
        $this->db->select("CONCAT_WS(' ',sub_sc.firstname,sub_sc.lastname)",false);
        $this->db->from("tbl_shift_caller as sub_sc");
        $this->db->join("tbl_finance_invoice_shifts as sub_fis","sub_fis.shift_id=sub_sc.shiftId AND sub_fis.archive=0","inner");
        $this->db->where("sub_fis.invoice_id=fi.id",NULL,FALSE);
        $this->db->limit(1);
        return $this->db->get_compiled_select();
        
    }
     public function invoice_shift_notes_sub_query(){
         $this->db->select('sub_sn.notes');
         $this->db->from("tbl_shift_notes as sub_sn");
         $this->db->join("tbl_finance_invoice_shifts as sub_fis","sub_fis.shift_id=sub_sn.shiftId AND sub_sn.archive=0 AND sub_fis.archive=sub_sn.archive","inner");
         $this->db->where("sub_fis.invoice_id=fi.id",NULL,FALSE);
         $this->db->order_by("sub_sn.id","DESC");
         $this->db->limit(1);
         return $this->db->get_compiled_select();

     }

    public function dashboard_invoice_list($reqData,$extraParm=[]) {
        $exportCall = $extraParm['exportCall'] ?? false;
        $creditNoteCall = $extraParm['creditNoteCall'] ?? false;
        $invoiceShiftNotesQuery = $this->invoice_shift_notes_sub_query();
        $invoiceStatusCaseQuery = $this->inovice_status_query();
        $invoiceForQueryCase = $this->inovice_for_status_query();
        $invoiceGenderQueryCase = $this->inovice_for_status_gender_query("fi","invoice_for");
        $shift_partcipant_name = $this->get_invoice_participant_sub_query();
        $invoiceQueryFor = $this->invoice_for_user_query();
        //$shift_site_name = $this->get_invoice_site_sub_query();
        $shiftCallerSubQuery= $this->shift_caller_sub_query();
        $shiftFundingSubQuery= $this->get_invoic_fund_type_sub_query(1);

        $filter = $reqData->filtered ?? [];
        $sorted = $reqData->sorted ?? [];
        $orderBy = '';
        $direction = '';
        if($exportCall || $creditNoteCall){
           
        }else{
            $limit = $reqData->pageSize;
            $page = $reqData->page;
        }
        

        if(isset($filter->fundType) && !empty($filter->fundType)){
            $fundTypeFilter = $this->basic_model->get_row('funding_type',['name'],['id'=>$filter->fundType]);
            $this->db->where('fund_type',$fundTypeFilter->name ?? 0);
        }

        $src_columns = array('invoice_number', 'description', 'invoice_for', 'addressto', 'amount', 'fund_type', 'invoice_date','invoice_status');
        if (isset($filter->search) && $filter->search != '') {

            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (!empty($filter->filter_by) && $filter->filter_by != 'all' && $filter->filter_by != $column_search) {
                    continue;
                }
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                } else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }
        
        $queryHavingData = $this->db->get_compiled_select();
        $queryHavingData = explode('WHERE', $queryHavingData);
        $queryHaving = isset($queryHavingData[1]) ? $queryHavingData[1] : '';


        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $this->db->order_by('fi.created','DESC');
            $orderBy = 'fi.id';
            $direction = 'DESC';
        }

        if (isset($filter->start_date) && !empty($filter->start_date)) {
            $this->db->where("DATE(fi.invoice_date) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
        }
        if (isset($filter->end_date) &&!empty($filter->end_date)) {
            $this->db->where("DATE(fi.invoice_date) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
        }

        if($exportCall){
            $this->db->where_in('status',['0']);
        }

        $select_column = array(
        "fi.id", "fi.total as amount", "fi.invoice_date", "fi.invoice_id as invoice_number", "DATE_FORMAT(fi.invoice_date,'%d/%m/%Y') as invoice_date", 
        "DATE_FORMAT(fi.pay_by,'%d/%m/%Y') as pay_by", 'fi.line_item_notes', 'fi.manual_invoice_notes', 'fi.status', 'CASE WHEN fi.invoice_type=1 THEN "Shift" WHEN fi.invoice_type=2 THEN "Manual" ELSE "" END as description', 
        "CASE WHEN fi.invoice_type=1 THEN COALESCE((".$shiftFundingSubQuery."),'') 
        WHEN fi.invoice_type=2 THEN (SELECT  GROUP_concat(distinct(ft.name)) FROM tbl_finance_manual_invoice_line_items fmili inner join tbl_funding_type as ft on ft.id= fmili.funding_type and fmili.archive=0 where fi.id=fmili.invoice_id)
        ELSE '' END fund_type",

        $invoiceForQueryCase." AS invoice_for_booked",
        "CASE 
            WHEN fi.invoice_type=1 AND fi.booked_by=1 THEN COALESCE((".$shiftCallerSubQuery."),'')
            WHEN fi.invoice_type=1 AND (fi.booked_by=2 OR fi.booked_by=3) THEN COALESCE((".$shiftCallerSubQuery."),(".$shift_partcipant_name."),'')
            ELSE ''
        END as addressto",
        $invoiceStatusCaseQuery." as invoice_status",
        $invoiceGenderQueryCase." as booked_gender",
        "CASE WHEN fi.invoice_type=1 THEN (".$invoiceShiftNotesQuery.") ELSE fi.invoice_shift_notes END as invoice_shift_notes"
        );
        
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_finance_invoice as fi');
        //$this->db->select("CASE WHEN fi.booked_by=1 THEN (" . $shift_site_name . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $shift_partcipant_name . ") ELSE '' END as invoice_for", false);
        $this->db->select($invoiceQueryFor." as invoice_for", false);
        $this->db->order_by($orderBy, $direction);
        if(!$exportCall && !$creditNoteCall){
            $this->db->limit($limit, ($page * $limit));
        }
        $this->db->where(array('fi.archive' => 0));
        if (!empty($queryHaving)) {
            $this->db->having($queryHaving);
        }
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if(!$exportCall && !$creditNoteCall){
            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }
        }
        $dataResult = $query->result();
        if (!empty($dataResult)) {
            if($creditNoteCall){
                $invoiceIds = array_column($dataResult,'id');
                $creditNoteData = !empty($invoiceIds) ? $this->get_invoice_credit_note_from_or_to_amount_by_invoice_ids($invoiceIds):[];
            }
            foreach ($dataResult as $key => $value) {
                #$temp = explode('#_SEPARATER_#', $value->line_items_details);
                if($creditNoteCall){
                    $value->credit_note_from_used = $creditNoteData[$value->id]['amount_used_from'] ?? 0;
                    $value->credit_note_to_used = $creditNoteData[$value->id]['amount_used_to'] ?? 0;
                }
                #$value->fund_type = $temp[0]??'';
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    public function get_invoice_participant_sub_query($type=1) {
        $query_res=array();
        if($type==1){       
            $this->db->select("REPLACE(concat(COALESCE(sub_p.firstname,''),' ',COALESCE(sub_p.middlename,''),' ',COALESCE(sub_p.lastname,'')),'  ',' ')", false);
            $this->db->from('tbl_participant as sub_p');
            $this->db->where('sub_p.id=fi.invoice_for AND fi.archive=0');
            $query_res = $this->db->get_compiled_select();
        }else if($type==2){
            $this->db->select("REPLACE(concat(COALESCE(sub_p.firstname,''),' ',COALESCE(sub_p.middlename,''),' ',COALESCE(sub_p.lastname,'')),'  ',' ')", false);
            $this->db->from('tbl_participant as sub_p');
            $this->db->where('sub_p.id=fs.statement_for AND fs.archive=0');
            $query_res = $this->db->get_compiled_select();
        }
        return $query_res;
    }

    public function get_invoice_credit_note_from_or_to_amount_by_invoice_ids($invoiceIds=[]){
        $invoiceIds = !empty($invoiceIds) ? $invoiceIds:0;
        $invoiceIds = !empty($invoiceIds) && is_array($invoiceIds) ? $invoiceIds :[$invoiceIds];
        $this->db->select(["fcnia.invoice_id, CAST(SUM(CASE WHEN fcnia.attached_type=1 THEN fcnia.amount ELSE '0' END) as DECIMAL(10,2)) as amount_used_from","CAST(SUM(CASE WHEN fcnia.attached_type=2 THEN fcnia.amount ELSE '0' END) as DECIMAL(10,2)) as amount_used_to"]);
        $this->db->from("tbl_finance_credit_note_invoice_attached fcnia");
        $this->db->where(["archive=0"]);
        $this->db->where_in("invoice_id",$invoiceIds);
        $this->db->group_by("invoice_id");
        $query = $this->db->get();
        return  $query->num_rows()>0 ? pos_index_change_array_data($query->result_array(),'invoice_id'):[];
    }

    public function get_invoice_site_sub_query($type=1){
        $query_res=array();
        if($type==1){
            $this->db->select("sub_os.site_name", false);
            $this->db->from('tbl_organisation_site as sub_os');
            $this->db->where('fi.invoice_for=sub_os.id AND fi.archive=0');
            $query_res = $this->db->get_compiled_select();
        }else if($type==2){
            $this->db->select("sub_os.site_name", false);
            $this->db->from('tbl_organisation_site as sub_os');
            $this->db->where('fs.statement_for=sub_os.id AND fs.archive=0');
            $query_res = $this->db->get_compiled_select();
        }
        
        return $query_res;
    }

    public function get_invoice_site_email_sub_query() {
        $this->db->select("sub_ose.email", false);
        $this->db->from('tbl_organisation_site_email as sub_ose');
        $this->db->where('fi.invoice_for=sub_ose.siteId AND fi.archive=0 AND sub_ose.primary_email=1 AND sub_ose.archive=0');
        $query_res = $this->db->get_compiled_select();
        return $query_res;
    }

    public function get_invoice_participant_email_sub_query() {
        $this->db->select("sub_pe.email", false);
        $this->db->from('tbl_participant_email as sub_pe');
        $this->db->where('sub_pe.participantId=fi.invoice_for AND fi.archive=0 AND sub_pe.primary_email=1');
        $query_res = $this->db->get_compiled_select();
        return $query_res;
    }

    function get_manual_invoice_items_for_pdf($invoiceId) {
        $this->db->select(['line_item.cost', 'line_item.quantity']);
        $this->db->select("(select line_item_name from tbl_finance_line_item where id = line_item.line_item) as item_name");
        $this->db->from('tbl_finance_manual_invoice_line_items as line_item');
        $this->db->where('line_item.archive', 0);
        $this->db->where('line_item.invoice_id', $invoiceId);
        $query_1 = $this->db->get_compiled_select();

        $this->db->select(['misc_item.item_cost as cost', "'1' as quantity", "misc_item.item_name"]);
        $this->db->from('tbl_finance_manual_invoice_miscellaneous_items as misc_item');
        $this->db->where('misc_item.archive', 0);
        $this->db->where('misc_item.invoice_id', $invoiceId);
        $query_2 = $this->db->get_compiled_select();

        $query = $this->db->query($query_1 . ' UNION ' . $query_2);
        return $query->result_array();
    }

    function get_invoice_items_for_pdf($invoiceId) {
        $this->db->select(['slia.cost', 'slia.quantity']);
        $this->db->select("(select sub_fli.line_item_name from tbl_finance_line_item as sub_fli where sub_fli.id = slia.line_item) as item_name");
        $this->db->from('tbl_shift_line_item_attached slia');
        $this->db->join('tbl_finance_invoice_shifts fis','fis.shift_id=slia.shiftId AND fis.archive=slia.archive','inner');
        $this->db->where('slia.archive', 0);
        $this->db->where('fis.invoice_id', $invoiceId);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_shift_invoice_address_to($invoiceId){

        $this->db->select("CONCAT_WS(' ',sub_sc.firstname,sub_sc.lastname) as name,sub_sc.email,sub_sc.phone",false);
        $this->db->from("tbl_shift_caller as sub_sc");
        $this->db->join("tbl_finance_invoice_shifts as sub_fis","sub_fis.shift_id=sub_sc.shiftId AND sub_fis.archive=0","inner");
        $this->db->where("sub_fis.invoice_id",$invoiceId);
        $this->db->limit(1);
        $query = $this->db->get();
        $res= $query->num_rows()>0 ? $query->row_array(): [];

        $this->db->select(["CONCAT_WS(', ',sl.address,sl.suburb,(SELECT sub_s.name FROM tbl_state as sub_s where sub_s.id=sl.state),sl.postal) as address"]);
        $this->db->from('tbl_shift_location sl');
        $this->db->join('tbl_finance_invoice_shifts fis','fis.shift_id=sl.shiftId AND fis.archive=0','inner');
        $this->db->where('fis.invoice_id', $invoiceId);
        $this->db->order_by('sl.id','ASC');
        $query1 = $this->db->get();
        $res1= $query1->num_rows()>0 ? $query1->row_array(): [];
        return array_merge($res,$res1);
    }

    function get_invoice_details($invoiceId) {
        $this->load->model(['finance/Finance_common_model']);
        $select_column = array('fi.id', 'fi.invoice_type', 'fi.booked_by', 'fi.invoice_for', 'fi.manual_invoice_notes', 'fi.invoice_date','fi.pay_by', "fi.sub_total", "fi.gst", "fi.total","fi.invoice_id");
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_finance_invoice as fi');
        $this->db->where('fi.archive', 0);
        $this->db->where('fi.id', $invoiceId);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result=[];
        $rowDetails=[];
        $invoice_items=[];
        if($query->num_rows()>0){
            $result = $query->row_array();
            if($result['booked_by']=='1'){
                $rowDetails = $this->Finance_common_model->get_site_details_by_id($result['invoice_for']);
            }else if($result['booked_by']=='2' || $result['booked_by']=='3'){
                $rowDetails = $this->Finance_common_model->get_participant_details_by_id($result['invoice_for']);
            }else if($result['booked_by']=='4' || $result['booked_by']=='5'){
                $rowDetails = $this->Finance_common_model->get_organisation_details_by_id($result['invoice_for']);
            }
            
            $result['invoice_for'] = isset($rowDetails['name']) && !empty($rowDetails['name']) ? $rowDetails['name']:'';
            $result['invoice_email'] = isset($rowDetails['email']) && !empty($rowDetails['email']) ? $rowDetails['email']:'';
            $result['invoice_phone'] = isset($rowDetails['phone']) && !empty($rowDetails['phone']) ? $rowDetails['phone']:'';
            $result['invoice_address_for'] = isset($rowDetails['address']) && !empty($rowDetails['address']) ? $rowDetails['address']:'';
            if($result['invoice_type']=='1'){
                $invoice_items = $this->get_invoice_items_for_pdf($invoiceId);
                $addressToData = $this->get_shift_invoice_address_to($invoiceId);
                $result['invoice_to'] = isset($addressToData['name']) && !empty($addressToData['name']) ? $addressToData['name']: (!isset($addressToData['name']) ? $result['invoice_for']:'');
                $result['invoice_email_to'] = isset($addressToData['email']) && !empty($addressToData['email']) ? $addressToData['email']: (!isset($addressToData['email']) ? $result['invoice_email']:'');
                $result['invoice_phone_to'] = isset($addressToData['phone']) && !empty($addressToData['phone']) ? $addressToData['phone']:(!isset($addressToData['phone']) ? $result['invoice_phone']:'');
                $result['invoice_address_to'] = !empty($addressToData) && isset($addressToData['address']) && !empty($addressToData['address']) && isset($addressToData['name']) ? $addressToData['address']: (!isset($addressToData['name']) ? $result['invoice_address_for']:'');
            }else if($result['invoice_type']=='2'){
                $invoice_items = $this->get_manual_invoice_items_for_pdf($invoiceId);
                $result['invoice_to'] = $result['invoice_for'];
                $result['invoice_email_to'] =  $result['invoice_email'];
                $result['invoice_phone_to'] = $result['invoice_phone'];
                $result['invoice_address_to'] =  $result['invoice_address_for'];
            }
            $result['invoice_item'] = $invoice_items;
        }


        return $result;
    }

    public function get_participant_organization_name($data) {
        $search = $data->search;
        
        $this->db->select(["concat_ws(' ',p.firstname,p.middlename,p.lastname) as label", "p.id as value", "'2' as type, CASE WHEN p.gender=1 THEN 'male' WHEN p.gender=2 THEN 'female' ELSE '' END as booked_gender","'participant' as booked_by"]);
        $this->db->from('tbl_participant as p');
        $this->db->where('p.status', 1);
        $this->db->where('p.archive', 0);
        $this->db->like("concat_ws(' ',p.firstname,p.middlename,p.lastname)", $search);

        $query1 = $this->db->get_compiled_select();

        $this->db->select(["org.name as label", "org.id as value", "CASE WHEN org.parent_org=0 THEN '4' ELSE '5' END as type", "'' as booked_gender","CASE WHEN org.parent_org=0 THEN 'org' ELSE 'sub_org' END as booked_by" ]);
        $this->db->from(TBL_PREFIX . 'organisation as org');
        $this->db->where('org.status', 1);
        $this->db->where('org.archive', 0);
        $this->db->like("org.name", $search);
        $query2 = $this->db->get_compiled_select();
        $query = $this->db->query($query1 . ' UNION ' . $query2);
        $res = $query->result_array();

        return $res;
    }

    private function get_invoic_fund_type_sub_query($type=1) {
        if($type==3){
            $this->db->select("1",false);
        }else{
            $this->db->select("sub_ft.name");
        }
        $this->db->from('tbl_shift as sub_s');
        $this->db->join("tbl_finance_invoice_shifts as sub_fis","sub_fis.shift_id=sub_s.id AND sub_fis.archive=0","inner");
        $this->db->join("tbl_funding_type as sub_ft","sub_ft.id=sub_s.funding_type AND sub_ft.archive=0","inner");
        if($type==1){
            $this->db->where('sub_fis.invoice_id=fi.id AND fi.archive=0',null,false);
        }else if($type==2){
            $this->db->where("sub_fis.invoice_id=substring_index(group_concat(fi.id order by fi.invoice_date DESC ,fi.id DESC SEPARATOR '@@__BREAKER__@@'),'@@__BREAKER__@@',1) AND fi.archive=0",null,false);
        }else if($type=3){
            $this->db->where('sub_fis.invoice_id=fi.id AND fi.archive=0',null,false);
            $this->db->where('sub_ft.name','ndis');
        }
        $this->db->limit(1);
        $query_res = $this->db->get_compiled_select();
        return $query_res;
    }


    public function invoice_scheduler_list($reqData) {
        $invoiceForQueryCase = $this->inovice_for_status_query();
        $invoiceGenderQueryCase = $this->inovice_for_status_gender_query("fi","invoice_for");
        $invoiceParticipantSubQuery = $this->get_invoice_participant_sub_query();
        $invoiceSiteSubQuery = $this->get_invoice_site_sub_query();
        $invoiceFundSingleSubQuery = $this->get_invoic_fund_type_sub_query(1);
        $invoiceFundlastInvoiceSubQuery = $this->get_invoic_fund_type_sub_query(2);

        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $src_columns = array('invoice_for_name','booked_from', /* 'invoice_fund_type', */ 'invoice_schedule', 'last_invoice_date');
        if (isset($filter->search) && $filter->search != '') {

            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (!empty($filter->filter_by) && $filter->filter_by != 'all' && $filter->filter_by != $column_search) {
                    continue;
                }
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                } else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }
        $queryHavingData = $this->db->get_compiled_select();
        $queryHavingData = explode('WHERE', $queryHavingData);
        $queryHaving = isset($queryHavingData[1]) ? $queryHavingData[1] : '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'fi.id';
            $direction = 'DESC';
        }

        if (!empty($filter->start_date)) {
            $this->db->where("DATE(fi.invoice_date) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
        }
        if (!empty($filter->end_date)) {
            $this->db->where("DATE(fi.invoice_date) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
        }

        /* $select_column = array("fi.id", "fi.total as amount", "fi.invoice_date", "fi.invoice_id as invoice_number", "fi.invoice_date", "fi.pay_by", "fi.invoice_shift_notes", 'fi.line_item_notes', 'fi.manual_invoice_notes', 'fi.status', '"Shift" as description', "(SELECT  GROUP_concat(distinct(ft.name)) FROM tbl_finance_manual_invoice_line_items fmili inner join tbl_funding_type as ft on ft.id= fmili.funding_type and fmili.archive=0 where fi.id=fmili.invoice_id) as fund_type", "(CASE WHEN fi.booked_by=1 THEN 'Organisation' WHEN fi.booked_by=2 THEN 'Participant' WHEN fi.booked_by=3 THEN 'Location' END) AS addressto ");

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false); */
        $this->db->from('tbl_finance_invoice as fi');
        $this->db->select([
            "CASE WHEN count(fi.invoice_date)>1 THEN date_format(substring_index(substring_index(group_concat(fi.invoice_date order by fi.invoice_date DESC ,fi.id DESC SEPARATOR '@@__BREAKER__@@' ),'@@__BREAKER__@@',2),'@@__BREAKER__@@',-1),'%d/%m/%Y') ELSE '' END as last_invoice_date",
            "'Instant' as invoice_schedule",
            "CASE 
            WHEN count(fi.invoice_date) >1 THEN coalesce((".$invoiceFundlastInvoiceSubQuery."),'')
            WHEN count(fi.invoice_date) =1 THEN coalesce((".$invoiceFundSingleSubQuery."),'')
            ELSE ''
            END as invoice_fund_type",
            "CASE WHEN fi.booked_by=1 THEN (" . $invoiceSiteSubQuery . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $invoiceParticipantSubQuery . ") ELSE '' END as invoice_for_name",
            $invoiceForQueryCase." as booked_from",
            //"CASE WHEN fi.booked_by=2 OR fi.booked_by=3 THEN 'participant' WHEN fi.booked_by=1 then 'site' ELSE '' END as booked_from",
            "fi.invoice_for as booked_for",
            "fi.booked_by as invoice_booked",
            $invoiceGenderQueryCase." as booked_gender"
        ], false);
        $this->db->where(array('fi.archive' => 0, 'fi.invoice_type' => 1));
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('fi.booked_by,fi.invoice_for');
        $this->db->limit($limit, ($page * $limit));
        /* it is useed for subquery filter */
        if (!empty($queryHaving)) {
            $this->db->having($queryHaving);
        }
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $dataResult = $query->result();
        $return = array('status'=>true,'count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }


    private function get_shift_details_by_shift_id(int $shiftId=0){
        $this->db->select([
            's.invoice_created',
            's.funding_type',
            "(SELECT sub_ft.name FROM tbl_funding_type as sub_ft where sub_ft.id=s.funding_type) as funding_type_name",
            's.start_time as shift_start_date',
            's.end_time as shift_end_date',
            's.booked_by',
            "CASE 
                WHEN s.booked_by=2 OR s.booked_by=3 
                THEN COALESCE((SELECT sub_sp.participantId FROM tbl_shift_participant as sub_sp WHERE sub_sp.shiftId=s.id and sub_sp.status=1 LIMIT 1),0) 
                WHEN s.booked_by=1  
                THEN COALESCE((SELECT sub_ss.siteId FROM tbl_shift_site as sub_ss WHERE sub_ss.shiftId=s.id LIMIT 1),0)
            ELSE '0'
            END as booked_for"
            ]);
            $this->db->from('tbl_shift as s');
            $this->db->where(['s.id'=>$shiftId,'s.status'=>6,'s.invoice_created'=>0]);
            $query = $this->db->get();
            return $query->num_rows()>0 ? $query->row_array() :[];
    }
    public function create_invoice_by_shift_id(int $shiftId=0){
        $this->load->model(['finance/Finance_common_model']);
        $shiftDetails = $this->get_shift_details_by_shift_id($shiftId);
       
        if(empty($shiftDetails)){
            return ['status'=>false,'error'=>'Shift not completed or already invoice created yet.'];
        }
        $ins_invoice=[
            'invoice_date' => DATE_CURRENT,
            'pay_by' => date(DB_DATE_FORMAT, strtotime(DATE_CURRENT. ' +30 days')),
            'invoice_shift_start_date'=>$shiftDetails['shift_start_date'],
            'invoice_shift_end_date'=>$shiftDetails['shift_end_date'],
            'booked_by'=>$shiftDetails['booked_by'],
            'invoice_for'=>$shiftDetails['booked_for'],
            'status'=>0,
            'total'=>0,
            'gst'=>0,
            'sub_total'=>0,
            'archive'=>0,
            'invoice_type'=>1,
            'created'=>DATE_TIME,
        ];
        $invoice_Id = $this->basic_model->insert_records('finance_invoice',$ins_invoice);
        $this->basic_model->insert_records('finance_invoice_shifts',['invoice_id'=>$invoice_Id,'shift_id'=>$shiftId,'archive'=>0,'created'=>DATE_TIME]);
        $this->basic_model->update_records('shift',['invoice_created'=>1],['id'=>$shiftId]);
        $invoiceNumberData = $this->basic_model->get_row('finance_invoice',['invoice_id'],['id'=>$invoice_Id]);
        if($invoiceNumberData){
            $invoiceNumber = $invoiceNumberData->invoice_id;
        }else{
            $invoiceNumber = $invoice_Id;
        }
        $getContactTbl= $shiftDetails['booked_by']=='1' ? 'organisation_site' : (($shiftDetails['booked_by']=='2' || $shiftDetails['booked_by']=='3') ? 'participant':''); 
       $getColumn =  $getContactTbl == 'organisation_site'? 'site_id':($getContactTbl == 'participant' ?'participant_id':'');
       $tableXeroContact = [
           'organisation_site'=>'organisation_site_xero_contact_mapping',
           'participant'=>'participant_xero_contact_mapping'
        ];
        if(!empty($getContactTbl) && $shiftDetails['booked_for']){
            $companyId = XERO_DEFAULT_COMPANY_ID;
            $params = array('company_id' => $companyId);
            $this->load->library('XeroContact',$params);
            $this->load->library('XeroInvoice',$params);
            $getExistsContactId = $this->basic_model->get_row($tableXeroContact[$getContactTbl],['xero_contact_id'],[$getColumn=>$shiftDetails['booked_for'],'archive'=>0]);
            if(!$getExistsContactId){
                $contactDetails =[];
                $xeroContactId = '';
                $insDataContact = [];
                if($getContactTbl=='organisation_site'){
                    $siteDetails = $this->Finance_common_model->get_site_details_by_id($shiftDetails['booked_for']);
                    $insData = ['Name' => $siteDetails['name'].'(site-'.$shiftDetails['booked_for'].')','EmailAddress'=>$siteDetails['email']];
                    $contactDetails = $this->xerocontact->create_contact($companyId,$insData);
                    $insDataContact=[
                        'site_id'=>$shiftDetails['booked_for'],
                        'archive'=>0,
                        'created'=>DATE_TIME
                    ];

                }else if($getContactTbl=='participant'){
                    $participantDetails = $this->Finance_common_model->get_participant_details_by_id($shiftDetails['booked_for']);
                    $insData = ['FirstName'=> $participantDetails['firstname'],'LastName'=> $participantDetails['lastname'],'Name' => $participantDetails['name'].'(participant-'.$shiftDetails['booked_for'].')','EmailAddress'=>$participantDetails['email']];
                    $contactDetails = $this->xerocontact->create_contact($companyId,$insData);
                    $insDataContact=[
                        'participant_id'=>$shiftDetails['booked_for'],
                        'archive'=>0,
                        'created'=>DATE_TIME
                    ];
                }
                
                if(!empty($contactDetails) && isset($contactDetails['status']) && $contactDetails['status']){
                    $xeroContactId =$contactDetails['data'][0]['ContactID'];
                    if(!empty($insDataContact)){
                        $insDataContact['xero_contact_id'] = $xeroContactId;
                        $this->basic_model->insert_records($tableXeroContact[$getContactTbl],$insDataContact);
                    }

                }else{
                    return ['status'=>false,'msg'=>'invoice not created because conatct not create on xero.'];
                }


            }else{
                $xeroContactId = $getExistsContactId->xero_contact_id;
            }

            $insData = [
                'Type' => 'ACCREC',
                'ContactID'=>$xeroContactId,
                'LineItems' => [
                    [
                        'Description'=>'Consulting services as agreed (20% off standard rate',
                        'Quantity'=>1,
                        'UnitAmount'=>10.00,
                        'DiscountRate'=>0,
                    ],
                    [
                        'Description'=>'Consulting services as agreed (20% off standard rate)data',
                        'Quantity'=>1,
                        'UnitAmount'=>5.00,
                        //'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
                        //'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
                        'DiscountRate'=>0,
                        //'Tracking'=>['Name'=>'Region','Option'=>'North']
                    ],
    
                ],
                'Date'=>DATE_CURRENT,
                'DueDate'=>date(DB_DATE_FORMAT,strtotime(DATE_CURRENT. ' +30 days')),
                'LineAmountTypes'=>'Exclusive',// get option list from get_invoice_lineamount_list function
                'Reference'=>$invoiceNumber,//ACCREC type only,
                'InvoiceNumber'=>'INV'.$invoiceNumber,//ACCREC type only,
                
                'Url'=>base_url(),//URL link to a source document - shown as "Go to [appName]" in the Xero app, 
                'CurrencyCode'=>'AUD',//get CurrencyCode form get_currency_code function,
                'Status'=>'DRAFT',//get CurrencyCode form get_currency_code function,
                'SentToContact'=>0,//Boolean to set whether the invoice in the Xero app should be marked as "sent". This can be set only on invoices that have been approved
                'ExpectedPaymentDate'=>date('Y-m-d'),//Shown on sales invoices (Accounts Receivable) when this has been set
                'PlannedPaymentDate'=>date('Y-m-d'),//Shown on bills (Accounts Payable) when this has been set
    
            ];
            $invoiceDetails =$this->xeroinvoice->create_inovice($companyId,$insData);
            if($invoiceDetails['status'] && !empty($invoiceDetails['data'])){
                $invoiceIdXero = $invoiceDetails['data'][0]['InvoiceID'];
                $this->basic_model->update_records('finance_invoice',['xero_invoice_id'=>$invoiceIdXero],['id'=>$invoice_Id]);
                return ['status'=>true,'msg'=>'Shift auto invoice creation successfully'];
            }else{
                return ['status'=>false,'msg'=>'invoice not created on xero.'];
            }
        }else{
            return ['status'=>false,'msg'=>'Shift created but not sent to xero'];
        }
    }
    private function inovice_status_query($tableAlias ='fi'){
        return "CASE WHEN ".$tableAlias.".status=1 THEN 'Payment Received' WHEN ".$tableAlias.".status=2 THEN 'Payment Not Received' WHEN ".$tableAlias.".status=2 THEN 'Payment Pending' WHEN ".$tableAlias.".status=0 THEN 'Payment Pending' ELSE '' END ";
    }
    private function inovice_for_status_query($tableAlias ='fi'){
        return "CASE WHEN ".$tableAlias.".booked_by=1 THEN 'site' WHEN ".$tableAlias.".booked_by=2 OR booked_by=3 THEN 'participant' WHEN booked_by=4 THEN 'org' WHEN booked_by=5 THEN 'sub_org' WHEN booked_by=6 THEN 'house' ELSE '' END ";
    }

    private function invoice_for_user_query(){
        $shift_partcipant_name = $this->get_invoice_participant_sub_query();
        $shift_site_name = $this->get_invoice_site_sub_query();
        $org_name = $this->get_invoice_org_sub_query(1);
        $sub_org_name = $this->get_invoice_org_sub_query(2);
        return "CASE WHEN fi.booked_by=1 THEN (" . $shift_site_name . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $shift_partcipant_name . ") WHEN fi.booked_by=4 THEN (".$org_name.") WHEN fi.booked_by=5 THEN (".$sub_org_name.") ELSE '' END ";
    }

    function get_participant_gender_subQuery($tableAliasWithColumn="fi.invoice_for",$type='1'){
        $this->db->select("CASE WHEN sub_p.gender=1 THEN 'male' WHEN sub_p.gender=2 THEN 'female' ELSE '' END",false);
        $this->db->from('tbl_participant sub_p');
        $this->db->where($tableAliasWithColumn."=sub_p.id",NULL,FALSE);
        return $this->db->get_compiled_select();
    }
    private function inovice_for_status_gender_query($tableAlias ='fi',$column_name='invoice_for'){
        $participaintQuery = $this->get_participant_gender_subQuery($tableAlias.".".$column_name,1);
        return "CASE  WHEN ".$tableAlias.".booked_by=2 OR booked_by=3 THEN (".$participaintQuery.") ELSE '' END ";
    }

    public function get_invoice_scheduler_history_list($reqData) {
        $invoiceStatusCaseQuery = $this->inovice_status_query();

        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $invoiceFor = !empty($filter->inf)?$filter->inf:0 ;
        $bookedBy = !empty($filter->booked_by)?$filter->booked_by:0 ;

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'fi.id';
            $direction = 'DESC';
            $this->db->order_by('fi.created', 'DESC');
        }

        $this->db->from('tbl_finance_invoice as fi');
        $this->db->select([
            "fi.invoice_id as invoice_number",
            "fi.total as invoice_amount",
            "DATE_FORMAT(fi.invoice_date,'%d/%m/%Y') as invoice_send_date",
            "CASE WHEN fi.status=1 THEN fi.total ELSE '' END as invoice_amount_paid",
            "CASE WHEN fi.status=1 OR  fi.status=2 THEN DATE_FORMAT(fi.invoice_finalised_date,'%d/%m/%Y') ELSE '' END as invoice_finalised_date",
            $invoiceStatusCaseQuery." as invoice_status",
            
        ], false);
        $this->db->where(array('fi.archive' => 0,'invoice_for'=>$invoiceFor,'booked_by'=>$bookedBy));
        
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $dataResult = $query->result();
        $return = array('status'=>true,'count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
    }

    public function get_invoice_pdf($invoiceId) {
        $response = $this->get_invoice_pdf_data($invoiceId);
        if (!empty($response->invoice_file_path)) {
            $pdfFileFCpath = FCPATH . FINANCE_INVOICE_FILE_PATH. $response->invoice_file_path;
            if (!file_exists($pdfFileFCpath)) {
                $this->create_invoice_pdf($invoiceId);
                $response = $this->get_invoice_pdf_data($invoiceId);
            }
        } else {
            $this->create_invoice_pdf($invoiceId);
            $response = $this->get_invoice_pdf_data($invoiceId);
        }
        return $response;
    }

    private function get_invoice_pdf_data($invoiceId,$returnEmail=false) {
        $where = ['id' => $invoiceId];
        $invoiceStatusCaseQuery = $this->inovice_status_query();
        $invoiceForQueryCase = $this->inovice_for_status_query();
        $shiftPartcipantName = $this->get_invoice_participant_sub_query();
        $shiftSiteName = $this->get_invoice_site_sub_query();
        if($returnEmail){
            $shiftSiteEmail = $this->get_invoice_site_email_sub_query();
            $shiftPartcipantEmail = $this->get_invoice_participant_email_sub_query();
        }
        $select_column = array(
        "fi.id",  'fi.status', 'fi.booked_by', 'fi.invoice_for',
        $invoiceForQueryCase." AS invoice_for_booked",
        $invoiceStatusCaseQuery." as invoice_status"
        );

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->select("CASE WHEN fi.booked_by=1 THEN (" . $shiftSiteName . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $shiftPartcipantName . ") ELSE '' END as invoice_for", false);
        if($returnEmail){
            $this->db->select("CASE WHEN fi.booked_by=1 THEN (" . $shiftSiteEmail . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $shiftPartcipantEmail . ") ELSE '' END as invoice_email", false);
        }
        return $this->basic_model->get_row('finance_invoice as fi', ['fi.invoice_file_path','fi.invoice_id'], $where);
    }

    public function create_invoice_pdf($invoiceId){

            $invoice_data = $this->get_invoice_details($invoiceId);
            $path = base_url('assets/img/ocs_logo.png');
            error_reporting(0);
    
            $data['logo_path'] = $path;
            $data['invoice_data'] = $invoice_data;
    
            $data['type'] = 'footer';
            $footerData = $this->load->view('create_invoice_pdf', $data, true);
    
            $data['type'] = 'content';
           
            $file = $this->load->view('create_invoice_pdf', $data, true);
            # pr($data);
            $this->load->library('m_pdf');
            $pdf = $this->m_pdf->load(); //"'en-GB-x','A4-L','','',10,10,10,10,6,3,'L'");
            $pdf->AddPage('L');
            $pdf->WriteHTML($file);
            $pdf->setFooter($footerData); // $_SERVER['HTTP_HOST'] . '|{PAGENO}/{nbpg}|' . date('d-m-Y H:i:s')); // Add a footer       
            $rand = date('d_m_Y_hisa');
            $filename = 'Invoice_' . $invoiceId . '_' . $rand . '.pdf';
            $pdfFilePath = FINANCE_INVOICE_FILE_PATH . $filename;
            create_directory(FCPATH.FINANCE_INVOICE_FILE_PATH);
            $pdf->Output($pdfFilePath, 'F');
            //echo $pdf->Output();die;
            $quote_data = $this->save_invoice_file($invoiceId, $filename);

    }

    function resend_invoice_mail($invoiceId) {    
       $sendMailDetail = $this->get_invoice_pdf_data($invoiceId,true);
       $mail_res =false;
        if (!empty($sendMailDetail->invoice_file_path)) {
            $pdf_fileFCpath = FCPATH . FINANCE_INVOICE_FILE_PATH . $sendMailDetail->invoice_file_path;
            if (file_exists($pdf_fileFCpath)) {
                $sendMailDetail->invoicePdfPath = $pdf_fileFCpath;      
                $mail_res = send_invoice_email($sendMailDetail);
            } else {
                // create quote pdf if not exist
                $this->create_invoice_pdf($invoiceId);
                $sendMailDetail = $this->get_invoice_pdf_data($invoiceId);
                if (!empty($sendMailDetail->invoice_file_path)) {
                    $pdf_fileFCpath = FCPATH . FINANCE_INVOICE_FILE_PATH . $sendMailDetail->invoice_file_path;
                    if (file_exists($pdf_fileFCpath)) {
                        $sendMailDetail->invoicePdfPath = $pdf_fileFCpath;
                        $mail_res = send_invoice_email($sendMailDetail);
                    }
                }
            }
        }
            return $mail_res;
    }

    function save_invoice_file($invoiceId, $fileName) {
        $where = array('id' => $invoiceId);
        return $quoteId = $this->basic_model->update_records('finance_invoice', ['invoice_file_path' => $fileName], ['id'=>$invoiceId]);
    }

    function update_invoice_status($invoiceId,$status){
       $invoiceData = $this->basic_model->get_row('finance_invoice',['status'],['id'=>$invoiceId]);
       if($invoiceData){
           if($invoiceData->status==0){
                $this->basic_model->update_records('finance_invoice',['status'=>$status,'invoice_finalised_date'=>DATE_CURRENT],['id'=>$invoiceId]);
                $res = ['status'=>true,'msg'=>'Invoice status updated successfully.'];
           }else{
                $res = ['status'=>false,'error'=>'Selected invoice status already updated.'];
           }

       }else{
           $res = ['status'=>false,'error'=>'Invalid request.'];
       }
       return $res;
    }

    public function check_and_update_invoice_status_data($rowData=[]){
        if(!empty($rowData)){
            $allowedStatus = json_decode(INVOICE_PAYMENT_NDIS_IMPORT_STATUS_DATA,true);//['paid'=>1,'rejected'=>2];
            $hcmPaymentStatus = json_decode(INVOICE_PAYMENT_STATUS_DATA);
            
            $currentDatetime = DATE_TIME;
            $table = 'invoice_import_ndis_status_number_'.time();
            $tablePreFix = TBL_PREFIX.$table;
            $this->db->query("DROP TABLE IF EXISTS ".$tablePreFix);
            $this->db->query(" CREATE TEMPORARY TABLE ".$tablePreFix." (
                     invoice_number VARCHAR(255) COLLATE 'utf8mb4_unicode_ci' NOT NULL)");
            $invoice_number = array_map(function($row){
                return ['invoice_number'=>$row['invoice number']];
            },$rowData);
            $this->basic_model->insert_update_batch('insert',$table,$invoice_number);
            $this->db->select(['fi.status','fi.invoice_id as invoice_number','fi.id']);
            $this->db->from('tbl_finance_invoice fi');
            $this->db->join($tablePreFix.' as tmp_fi','fi.invoice_id = tmp_fi.invoice_number and fi.archive=0','inner');
            $qurey = $this->db->get();
            $invoiceExistsRes = $qurey->num_rows()>0 ? pos_index_change_array_data($qurey->result_array(),'invoice_number') :[];
            $errors = [];
            $updatedStausMsg = [];
            $updatedStausData = [];
            foreach($rowData as $key =>$val){
                $rowNumber = $key+2;
                if(!isset($invoiceExistsRes[$val['invoice number']])){
                    $errors[] = "Row - ".($rowNumber)." Records not found in HCM application."; 
                    continue;
                }
                if(empty($val['ndis status']) || !in_array($val['ndis status'],array_keys($allowedStatus))){
                    $errors[] = "Row - ".($rowNumber)." ndis status empty or not allowed.(only allowed status is " .implode(' ,' ,array_keys($allowedStatus)).")"; 
                    continue;
                }
                if(isset($invoiceExistsRes[$val['invoice number']]) && $invoiceExistsRes[$val['invoice number']]['status']!=0){
                    $errors[] = "Row - ".($rowNumber)." Application status already updated or current status not in payment pending."; 
                    continue;
                }

                $updatedStausMsg[] = "Row - ".($rowNumber)." invoice data updated sucessfully from ".$hcmPaymentStatus[$invoiceExistsRes[$val['invoice number']]['status']] ." to ".$hcmPaymentStatus[$allowedStatus[$val['ndis status']]];
                $updatedStausData[]=[
                    'id'=>$invoiceExistsRes[$val['invoice number']]['id'], 
                    'status'=>$allowedStatus[$val['ndis status']],
                    'invoice_finalised_date' => $currentDatetime
                ];
            }
            $this->db->query("DROP TABLE IF EXISTS ".$tablePreFix);
            $errorStr = !empty($errors) ? '<p>'.implode('</p><p>',$errors).'</p>':'';
            if(!empty($updatedStausData)){
                $this->basic_model->insert_update_batch('update','finance_invoice',$updatedStausData,'id');
                $successStr = '<p>'.implode('</p><p>',$updatedStausMsg).'</p>';
                 $res1=['status'=>true,'msg'=>$successStr];
                 if(!empty($errorStr)){
                     $res1['warraing']=$errorStr;
                 }
                 return $res1;
            }else{
                return ['status'=>false,'error'=>$errorStr];
            }
        }else{
            return ['status'=>false,'error'=>'file row is empty.'];
        }
    }

    function get_income_from_invoice($type = 'week') {
        $response = ['0', '0', '0'];
        $checkType = in_array(strtolower($type), ['week', 'year', 'month']) ? strtolower($type) : 'week';
        $this->db->select("CAST(COALESCE(SUM(total),0) as DECIMAL(10,2)) as total, CAST(COALESCE(SUM(CASE when booked_by=2 OR booked_by=3 THEN total ELSE '0' END),0) as DECIMAL(10,2)) as participant , CAST(COALESCE(SUM(CASE when booked_by!=2 AND booked_by!=3 THEN total ELSE '0' END),0) as DECIMAL(10,2)) as orgs  ", false);
        $this->db->where(['status' => 1, 'archive' => 0]);
        $this->db->where("DATE(invoice_finalised_date)!= '0000-00-00' AND DATE(invoice_finalised_date) <=CURDATE()", NULL, false);
        if ($checkType == 'week') {
            $this->db->where(' YEARWEEK(invoice_finalised_date,7) = YEARWEEK(CURDATE(),7)', NULL, false);
        } else if ($checkType == 'month') {
            $this->db->where('YEAR(invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
            $this->db->where('MONTH(invoice_finalised_date) = MONTH(CURDATE())', NULL, false);
        } else if ($checkType == 'year') {
            $this->db->where('YEAR(invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
        }
        $query = $this->db->get('tbl_finance_invoice');
        if ($query->num_rows() > 0) {
            $response = array_values($query->row_array());   
        }
        return $response;
    }

    function get_loss_credit_notes_invoice($type = 'week') {
        $response = ['0', '0', '0'];
        return $response;
    }

    function get_loss_refund_invoice($type = 'week') {
        $response = ['0', '0', '0'];
        return $response;
    }
    function get_invoice_credited_and_refund_paid($type = 'week') {
        $response = 0;
        return $response;
    }
    function get_invoice_profit_money($type = 'week') {
        $response = 0;
        return $response;
    }

    function get_invoice_money($type = 'week') {
        $checkType = in_array(strtolower($type), ['week', 'year', 'month']) ? strtolower($type) : 'week';
        $fundTypeSubquery = $this->get_invoic_fund_type_sub_query(3);
        $manualFundTypeSubquery = $this->manual_fund_type_check(3);
        $this->db->select("CAST(SUM(fi.total) as DECIMAL(10,2)) as total_amount ");
        $this->db->from("tbl_finance_invoice as fi");
        $this->db->where(["fi.status"=>1,"fi.archive"=>0]);
        if ($checkType == 'week') {
            $this->db->where(' YEARWEEK(fi.invoice_finalised_date,7) = YEARWEEK(CURDATE(),7)', NULL, false);
        } else if ($checkType == 'month') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
            $this->db->where('MONTH(fi.invoice_finalised_date) = MONTH(CURDATE())', NULL, false);
        } else if ($checkType == 'year') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
        }
        $query = $this->db->get();
        $amounts= $query->num_rows()>0 ? $query->row()->total_amount : 0;
        return $amounts=='0.00'? '0':$amounts;
    }



    function get_ndis_invoice_money($type = 'week') {
        $checkType = in_array(strtolower($type), ['week', 'year', 'month']) ? strtolower($type) : 'week';
        $fundTypeSubquery = $this->get_invoic_fund_type_sub_query(3);
        $manualFundTypeSubquery = $this->manual_fund_type_check(3);
        $this->db->select("CAST(SUM( CASE 
        WHEN invoice_type=1 AND COALESCE((".$fundTypeSubquery."),0)=1 THEN fi.total 
        WHEN invoice_type=2 AND COALESCE((".$manualFundTypeSubquery."),0)=1 THEN fi.total 
        ELSE '0' END ) as DECIMAL(10,2)) as total_amount ");
        $this->db->from("tbl_finance_invoice as fi");
        $this->db->where(["fi.status"=>1,"fi.archive"=>0]);
        if ($checkType == 'week') {
            $this->db->where(' YEARWEEK(fi.invoice_finalised_date,7) = YEARWEEK(CURDATE(),7)', NULL, false);
        } else if ($checkType == 'month') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
            $this->db->where('MONTH(fi.invoice_finalised_date) = MONTH(CURDATE())', NULL, false);
        } else if ($checkType == 'year') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
        }
        $query = $this->db->get();
        $amounts= $query->num_rows()>0 ? $query->row()->total_amount : 0;
        return $amounts=='0.00'? '0':$amounts;
    }

    function get_invoice_ndis_rejected($type = 'week') {
        $checkType = in_array(strtolower($type), ['week', 'year', 'month']) ? strtolower($type) : 'week';
        $fundTypeSubquery = $this->get_invoic_fund_type_sub_query(3);
        $manualFundTypeSubquery = $this->manual_fund_type_check(3);
        $this->db->select("CAST(SUM( CASE 
        WHEN invoice_type=1 AND COALESCE((".$fundTypeSubquery."),0)=1 THEN fi.total 
        WHEN invoice_type=1 AND COALESCE((".$manualFundTypeSubquery."),0)=1 THEN fi.total 
        ELSE '0' END ) as DECIMAL(10,2)) as total_amount ");
        $this->db->from("tbl_finance_invoice as fi");
        $this->db->where(["fi.archive"=>0]);
        $this->db->where_in("fi.status",[2]);
        if ($checkType == 'week') {
            $this->db->where(' YEARWEEK(fi.invoice_finalised_date,7) = YEARWEEK(CURDATE(),7)', NULL, false);
        } else if ($checkType == 'month') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
            $this->db->where('MONTH(fi.invoice_finalised_date) = MONTH(CURDATE())', NULL, false);
        } else if ($checkType == 'year') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
        }
        $query = $this->db->get();
        $amounts= $query->num_rows()>0 ? $query->row()->total_amount : 0;
        return $amounts=='0.00'? '0':$amounts;
    }

    function get_invoice_rejected($type = 'week') {
    
        $checkType = in_array(strtolower($type), ['week', 'year', 'month']) ? strtolower($type) : 'week';
        $this->db->select("CAST(SUM(fi.total) as DECIMAL(10,2)) as total_amount ");
        $this->db->from("tbl_finance_invoice as fi");
        $this->db->where(["fi.archive"=>0]);
        $this->db->where_in("fi.status",[2]);
        if ($checkType == 'week') {
            $this->db->where(' YEARWEEK(fi.invoice_finalised_date,7) = YEARWEEK(CURDATE(),7)', NULL, false);
        } else if ($checkType == 'month') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
            $this->db->where('MONTH(fi.invoice_finalised_date) = MONTH(CURDATE())', NULL, false);
        } else if ($checkType == 'year') {
            $this->db->where('YEAR(fi.invoice_finalised_date) = YEAR(CURDATE())', NULL, false);
        }
        $query = $this->db->get();
        $amounts= $query->num_rows()>0 ? $query->row()->total_amount : 0;
        return $amounts=='0.00'? '0':$amounts;
    }

    function get_last_ndis_billing() {
        $this->db->select("COALESCE(DATEDIFF(CURDATE(),created),0) as total_count",false);
        $this->db->from('tbl_finance_invoice_ndis_export_log');
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->num_rows()>0 ? $query->row()->total_count : 0;
    }

    function manual_fund_type_check($type=1){

        if($type==3){
            $this->db->select("1",false);
        }else{
            $this->db->select("GROUP_concat(distinct(ft.name)) as name",false);
        }
        $this->db->from('tbl_finance_manual_invoice_line_items fmili');
        $this->db->join('tbl_funding_type as ft','ft.id= fmili.funding_type and fmili.archive=0');
        $this->db->where("fi.id=fmili.invoice_id",NULL,FALSE);
        if($type==3){
            $this->db->where("ft.name","ndis");
        }
        $query = $this->db->get_compiled_select();
        return $query;
    }

    function get_ndis_invoice_rejected_monthwise_financial_year(){
        $fundTypeSubquery = $this->get_invoic_fund_type_sub_query(3);
        $manualFundTypeSubquery = $this->manual_fund_type_check(3);
        $dataYear =get_current_finacial_year_data();
        $fromDate = $dataYear[0]['from_date'];
        $toDate = $dataYear[0]['to_date'];
        $this->db->select(["CAST(SUM(CASE 
        WHEN invoice_type=1 AND COALESCE((".$fundTypeSubquery."),0)=1 THEN fi.total 
        WHEN invoice_type=1 AND COALESCE((".$manualFundTypeSubquery."),0)=1 THEN fi.total 
        ELSE '0' END ) as DECIMAL(10,2)) as total_amount","DATE_FORMAT(fi.invoice_finalised_date,'%Y-%m') as data_month"]);
        $this->db->from("tbl_finance_invoice as fi");
        $this->db->where(["fi.archive"=>0]);
        $this->db->where_in("fi.status",[2]);
        $this->db->where("DATE(fi.invoice_finalised_date) BETWEEN '".$fromDate."' AND '".$toDate."'",NULL, FALSE);
        $this->db->group_by("DATE_FORMAT(fi.invoice_finalised_date,'%Y-%m')");
        $query = $this->db->get();
        $result = $query->num_rows() >0 ? $query->result_array():[];
        $result = !empty($result) ? pos_index_change_array_data($result,'data_month'): $result;
        $yearResult = get_interval_month_wise($fromDate,$toDate);
        $dataUpdate = [];
        if(!empty($yearResult) && !empty($result)){
            foreach($yearResult as $key=>$val){
                $dataUpdate[] = isset($result[Date('Y-m',strtotime($val['from_date']))]) ? $result[Date('Y-m',strtotime($val['from_date']))]['total_amount']:0;
            }
        }else{
            $dataUpdate = array_fill(0,12,0);
        }
        return $dataUpdate;
    }
    function dashboard_statement_list(){
        
        /*$invoiceShiftNotesQuery = $this->invoice_shift_notes_sub_query();
        $invoiceStatusCaseQuery = $this->inovice_status_query();
        $invoiceForQueryCase = $this->inovice_for_status_query();           
        $shiftCallerSubQuery= $this->shift_caller_sub_query();
        $shiftFundingSubQuery= $this->get_invoic_fund_type_sub_query(1); */


        $shift_partcipant_name = $this->get_invoice_participant_sub_query(2);
        $shift_site_name = $this->get_invoice_site_sub_query(2);

        $select_column = array("fs.id", "fs.statement_number","fs.statement_notes","DATE_FORMAT(fs.due_date, '%d/%m/%Y') as last_issue_date","fs.status" , "fs.total");   
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from('tbl_finance_statement as fs');
        $this->db->select("CASE WHEN fs.statement_type=1 THEN 'shift' ELSE 'manual' END AS statement_type", FALSE);
        //1= Site / Home, 2= Participant, 3 =Location , 4 =org ,5 =sub org	
        $this->db->select("CASE WHEN fs.booked_by=1 THEN (" . $shift_site_name . ") WHEN fs.booked_by=2 OR fs.booked_by=3 THEN (" . $shift_partcipant_name . ") ELSE '' END as statement_for", false);
        $this->db->select("CASE WHEN fs.booked_by=1 THEN 'shift' ELSE 'manual' END AS statement_type", FALSE);

       // $this->db->select("CASE WHEN fi.booked_by=1 THEN (" . $shift_site_name . ") WHEN fi.booked_by=2 OR fi.booked_by=3 THEN (" . $shift_partcipant_name . ") ELSE '' END as invoice_for", false);
       // $this->db->order_by($orderBy, $direction);
       // $this->db->limit($limit, ($page * $limit));        

        $this->db->where(array('fs.archive' => 0));           
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        $dataResult = $query->result();
        $return = array('status'=>true,'count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
        return $return;
}
public function get_statement_pdf($statementId) {
        $response = $this->get_statement_pdf_data($statementId);       
        if (!empty($response->statement_file_path)) {           
            $pdfFileFCpath =FCPATH.FINANCE_STATEMENT_FILE_PATH.$response->statement_file_path;
            if (!file_exists($pdfFileFCpath)) {
                
                $response = $this->create_statement_pdf_By_stid($statementId);                     
               // print_r($response);
                return $response;
            }
        } else {
            
            return  $this->create_statement_pdf_By_stid($statementId);             
        }        
        return ['status'=>true,"data"=>$response];
}
private function get_statement_pdf_data($statementId) {
    $where = ['id' => $statementId];   
    return $this->basic_model->get_row('finance_statement as fs', ['fs.statement_file_path','fs.statement_number'], $where);
} 
public function create_statement_pdf_By_stid($statement_id) {
    if(!$statement_id>0){
        return ['status'=>false,"error"=>"statement id required"];
    }
    $select_column = array("fs.id", "fs.statement_number","fs.from_date","fs.to_date","DATE_FORMAT(fs.issue_date, '%d/%m/%Y') as issue_date","DATE_FORMAT(fs.due_date, '%d/%m/%Y') as due_date","fs.statement_notes","fs.statement_type","fs.statement_for","fs.booked_by","fs.total");
    $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
    $this->db->where("fs.id",$statement_id);
    $this->db->from('tbl_finance_statement as fs');    
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $statementResult = $query->result_array();
    
    if(!empty($statementResult)){
        $statementResult=$statementResult[0];
        $select_column = array("fsa.id", "fsa.invoice_id","fsa.total","fsa.gst","fsa.sub_total","fi.invoice_id as invoice_number");
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->join('tbl_finance_invoice as fi', 'fi.id = fsa.invoice_id AND fi.archive = 0');
        $this->db->where("fsa.statement_id",$statement_id);
        $this->db->where("fsa.archive",0);        
        $this->db->from('tbl_finance_statement_attach as fsa');    
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $attachResult = $query->result_array();
        $statementResult['attach']=$attachResult;        
            
            // Get user details
            $booked_by=isset($statementResult['booked_by']) ? $statementResult['booked_by'] : 0;
            $invoice_for=isset($statementResult['statement_for']) ? $statementResult['statement_for'] : 0;
            // user details start 
            if($booked_by==1){
                $statementResult['user_details']  = $this->Finance_common_model->get_site_details_by_id($invoice_for);
            }else if($booked_by==2 || $booked_by==3){
                $statementResult['user_details'] = $this->Finance_common_model->get_participant_details_by_id($invoice_for);
            }else if($booked_by==4 || $booked_by==5){
                $statementResult['user_details'] = $this->Finance_common_model->get_organisation_details_by_id($invoice_for);
            }
           $response=$this->create_statement_pdf($statementResult);
           $pdf_fileFCpath = FCPATH.FINANCE_STATEMENT_FILE_PATH.$response;
           
           if (file_exists($pdf_fileFCpath)) {
                $statementResult['statement_file_path']=$response;  
                $this->basic_model->update_records('finance_statement', array('statement_file_path' => $response), $where = array('id' => $statement_id));
                return ['status'=>true,"data"=>$statementResult];
           }else{
            return ['status'=>false,"error"=>"File not created"];
           }          
    }else{
        return ['status'=>false,"error"=>"statement not found"];
    }

}
  function get_send_statement_mail($statement_id){
    if(!$statement_id>0){
        return ['status'=>false,"error"=>"statement id required"];
    }
    $select_column = array("fs.id", "fs.statement_number","fs.from_date","fs.to_date","DATE_FORMAT(fs.issue_date, '%d/%m/%Y') as issue_date","DATE_FORMAT(fs.due_date, '%d/%m/%Y') as due_date","fs.statement_notes","fs.statement_type","fs.statement_for","fs.booked_by","fs.total","fs.statement_file_path as file");
    $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
    $this->db->where("fs.id",$statement_id);
    $this->db->from('tbl_finance_statement as fs');    
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $statementResult = $query->result_array();
    $mail_array=array();
    if(!empty($statementResult)){
        $statementResult=$statementResult[0];
        $mail_array=$statementResult;
        // Get user details
        $booked_by=isset($statementResult['booked_by']) ? $statementResult['booked_by'] : 0;
        $invoice_for=isset($statementResult['statement_for']) ? $statementResult['statement_for'] : 0;
        // user details start 
        if($booked_by==1){
            $mail_array['booker']= $this->get_site_key_contact_email_by_id($invoice_for); 
            $mail_array['user_details']  = $this->Finance_common_model->get_site_details_by_id($invoice_for);              
        }else if($booked_by==2 || $booked_by==3){
            $mail_array['booker']= $this->get_participant_email_by_id($invoice_for); 
            $mail_array['user_details'] = $this->Finance_common_model->get_participant_details_by_id($invoice_for);                
        }else if($booked_by==4 || $booked_by==5){
            $mail_array['booker']= $this->get_organisation_contact_email_by_id($invoice_for); 
            $mail_array['user_details']  =  $this->Finance_common_model->get_organisation_details_by_id($invoice_for);            
        }
    }
    return $mail_array;   
}

public function get_participant_email_by_id($patId) {    
    $this->db->select(array("pbl.email"), false);
    $this->db->from('tbl_participant_booking_list as pbl');
    $this->db->where('pbl.participantId', $patId);
    $this->db->where('pbl.archive', 0);
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $result = $query->num_rows() > 0 ? $query->result_array() : [];
    return $result;
}
public function get_organisation_contact_email_by_id($orgId){
    $this->db->select(array("contact_email.email"), false);
    $this->db->from('tbl_organisation_all_contact_email as contact_email');
    $this->db->join('tbl_organisation_all_contact as contact', 'contact.id = contact_email.contactId AND contact.archive = 0 AND contact.type = 3');
    $this->db->where('contact.organisationId', $orgId);    
    $where = array('contact_email.archive ' => 0 , 'contact_email.primary_email' => 1);
    $this->db->where($where);    
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $result = $query->num_rows() > 0 ? $query->result_array() : [];
    return $result;
}
public function get_site_key_contact_email_by_id($siteId){
    $this->db->select(array("contact_email.email"), false);
    $this->db->from('tbl_organisation_site_key_contact_email as contact_email');
    $this->db->join('tbl_organisation_site_key_contact as contact', 'contact.id = contact_email.contactId AND contact.archive = 0 AND contact.type = 3');    
    $where = array('contact_email.archive ' => 0 , 'contact_email.primary_email' => 1);
    $this->db->where($where); 
    $this->db->where('contact.siteId', $siteId);
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
    $result = $query->num_rows() > 0 ? $query->result_array() : [];
    return $result;
}

public function get_invoice_org_sub_query($type=1) {
    $query_res=array();
        
        $this->db->select("sub_o.name", false);
        $this->db->from('tbl_organisation as sub_o');
        $this->db->where('sub_o.id=fi.invoice_for AND fi.archive=0');
        if($type==1){  
            $this->db->where('sub_o.parent_org',0);
        }else if($type==2){
            $this->db->where('sub_o.parent_org !=',0);
        }
        $this->db->limit(1);
        $query_res = $this->db->get_compiled_select();
    return $query_res;
}
function get_outstanding_statements_participants_count($view_type='week'){    
    
    $select_column = array("count(DISTINCT(fsa.invoice_id)) unpaid_participant");   
    $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
    $this->db->from('tbl_finance_statement as fs');    
    $this->db->where_in("fs.booked_by",[2,3]);
    $this->db->join('tbl_finance_statement_attach as fsa', 'fsa.statement_id = fs.id AND fs.archive = 0 AND fsa.archive = 0');
    $this->db->join('tbl_finance_invoice as f', 'f.id = fsa.invoice_id AND f.archive = 0 AND f.status = 0');    
    if ($view_type == 'year') {
        $where['YEAR(f.created)'] = date('Y');
        $this->db->where($where); 
    } else if ($view_type == 'week') {        
        $where = 'f.created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()';
        $this->db->where($where);
    } else {        
        $where['MONTH(f.created)'] = date('m');
        $this->db->where($where); 
    }
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());        
    return  $query->row();
}

function get_outstanding_statements_organization_count($view_type='week'){
    
    $select_column = array("count(DISTINCT(fsa.invoice_id)) unpaid_org");   
    $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
    $this->db->from('tbl_finance_statement as fs');    
    $this->db->where_in("fs.booked_by",[4,5]); 
    $this->db->join('tbl_finance_statement_attach as fsa', 'fsa.statement_id = fs.id AND fs.archive = 0 AND fsa.archive = 0');
    $this->db->join('tbl_finance_invoice as f', 'f.id = fsa.invoice_id AND f.archive = 0 AND f.status = 0');    
    if ($view_type == 'year') {
        $where['YEAR(f.created)'] = date('Y');
        $this->db->where($where); 
    } else if ($view_type == 'week') {        
        $where = 'f.created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()';
        $this->db->where($where);
    } else {        
        $where['MONTH(f.created)'] = date('m');
        $this->db->where($where); 
    }
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());  
   
    return  $query->row();
}

}
