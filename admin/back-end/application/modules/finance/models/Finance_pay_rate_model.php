<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Finance_pay_rate_model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function add_payrate($reqData) 
    {
        if (!empty($reqData))
        {
            $pay_rate_data = array('category'=>isset($reqData->category)?$reqData->category:'',
                'type'=>isset($reqData->type)?$reqData->type:'',
                'level_number'=>isset($reqData->level_number)?$reqData->level_number:'',
                'paypoint'=>isset($reqData->paypoint)?$reqData->paypoint:'',
                'start_date'=>isset($reqData->start_date)?DateFormate($reqData->start_date):'',
                'end_date'=>isset($reqData->end_date)?DateFormate($reqData->end_date):'',
                'comments'=>isset($reqData->comments)?$reqData->comments:'',
                'status'=>isset($reqData->status)?$reqData->status:2,
                'created'=>DATE_TIME,
            );

            if(strtotime(DateFormate($reqData->start_date)) == strtotime(date('Y-m-d')))
                $status = 1;
            else if(strtotime(DateFormate($reqData->start_date)) > strtotime(date('Y-m-d')))
                $status = 2;
            else
                $status = 0;

            $pay_rate_data['status'] = $status; 

            if(isset($reqData->category) && $reqData->category ==5)
            {
                $pay_rate_data['name'] = $reqData->allowance_name;
            }

            if(isset($reqData->payrate_id) && $reqData->payrate_id > 0)
            {
                if(isset($reqData->category) && $reqData->category ==5)
                {
                    $pay_rate_data['name'] = $reqData->allowance_name;
                }

                $opertion_type = 'edit';
                $payrate_id = $reqData->payrate_id;
                $row = $this->basic_model->get_row('finance_payrate',array('status','end_date'),array('id'=>$payrate_id));

                $update_old_record = [];
                if(strtotime(DateFormate($row->end_date)) < strtotime(date('Y-m-d')))
                    $update_old_record['archive'] = 0;

                if(!empty($row) && $row->status == 1){
                    $update_old_record['end_date'] = date('Y-m-d');
                    $update_old_record = array_merge($update_old_record,$pay_rate_data);
                    $this->basic_model->update_records('finance_payrate', $update_old_record, ['id' => $payrate_id]);  

                    $opertion_type = 'insert';
                    $pay_rate_data['status'] = $row->status;
                    $pay_rate_data['start_date'] = date('Y-m-d',(strtotime ( '+1 day' , strtotime ( date('Y-m-d')) ) ));
                    $payrate_id = $this->basic_model->insert_records('finance_payrate', $pay_rate_data, $multiple = FALSE);
                }
                else{
                    $this->basic_model->update_records('finance_payrate', $pay_rate_data, ['id' => $payrate_id]);              
                }
            }         
            else
            {
                $payrate_id = $this->basic_model->insert_records('finance_payrate', $pay_rate_data, $multiple = FALSE);
            }

            if(isset($reqData->category) && $reqData->category == 5)
            {
                $ins_ary = array('payrate_id'=>$payrate_id,
                    'dollar_value'=>isset($reqData->allowance_rate)?$reqData->allowance_rate:'',
                    'increased_by'=>0,
                    'rate_type'=>0,
                    'created'=>DATE_TIME
                );

                if(isset($reqData->mode) && $reqData->mode=='edit' && $opertion_type == 'edit')
                    $this->basic_model->update_records('finance_payrate_paypoint', ['archive'=>1], ['payrate_id' => $payrate_id]);

                $this->basic_model->insert_records('finance_payrate_paypoint', $ins_ary);
            }
            else
            {
                if(!empty($reqData->payPoints))
                {
                    $ins_ary = [];
                    foreach ($reqData->payPoints as $key => $value)
                    {
                        $ins_ary[] = array('payrate_id'=>$payrate_id,
                            'rate_type'=>$value->rate,
                            'increased_by'=>$value->increasedBy,
                            'dollar_value'=>$value->dollarValue,
                            'created'=>DATE_TIME
                        );
                    }

                    if(!empty($ins_ary)){
                        if(isset($reqData->mode) && $reqData->mode=='edit' && $opertion_type == 'edit')
                            $this->basic_model->update_records('finance_payrate_paypoint', ['archive'=>1], ['payrate_id' => $payrate_id]);

                        $this->basic_model->insert_records('finance_payrate_paypoint', $ins_ary, $multiple = TRUE);
                    }
                }
            }

            if($payrate_id>0)
                return TRUE;
            else
                return FALSE;
        }
        return FALSE;
    }

    public function get_payrate_list($reqData)
    {        
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';
        $tbl_payrate = TBL_PREFIX . 'finance_payrate';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = $tbl_payrate . '.id';
            $direction = 'DESC';
        }

        if (isset($filter->start_date) && $filter->start_date != '')
            $this->db->where("DATE(tbl_finance_payrate.start_date) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");

        if(isset($filter->end_date) && $filter->end_date != '')
            $this->db->where("DATE(tbl_finance_payrate.start_date) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");

        if (!empty($filter->srch_box) && !empty($filter->search_key)) {
            $search_key = $filter->search_key;

            if($search_key == 'rate_name'){
                $this->db->or_like($tbl_payrate . ".name", $filter->srch_box);
            }
            else if($search_key == 'status' && strtolower($filter->srch_box) == 'active'){
                $this->db->or_like($tbl_payrate . ".status", 1);
                $this->db->where($tbl_payrate . ".archive",0);
            }
            else if($search_key == 'status' && strtolower($filter->srch_box) == 'inactive'){
                $this->db->or_like($tbl_payrate . ".status", 2);
                $this->db->where($tbl_payrate . ".archive",0);
            }
            else if( ($search_key == 'status' && strtolower($filter->srch_box) == 'archive') || ($search_key == 'status' && strtolower($filter->srch_box) == 'archived') ){
                $this->db->or_like($tbl_payrate . ".archive", 1);
            }
            else if($search_key == 'category'){
                $this->db->or_like("fpc.name", $filter->srch_box);
            }
            else if($search_key == 'type'){
                $this->db->or_like("fpt.name", $filter->srch_box);
            }
            else if($search_key == 'hourly_rate'){
                $this->db->join('tbl_finance_payrate_paypoint as fpp', 'fpp.payrate_id =  ' . $tbl_payrate . '.id AND fpp.rate_type=1 AND fpp.archive=0', 'INNER');
                $this->db->or_like("fpp.dollar_value", $filter->srch_box);                
            }
            else if ($search_key == 'default') {
                $this->db->group_start();

                $src_columns = array($tbl_payrate . ".id","level_number", $tbl_payrate . ".paypoint","fpc.name as category","point_name","payrate_type");

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        $this->db->or_like($serch_column[0], $filter->srch_box);
                    } else {
                        $this->db->or_like($column_search, $filter->srch_box);
                    }
                }
                $this->db->group_end();
            }
        }
        $queryHavingData = $this->db->get_compiled_select();
        $queryHavingData = explode('WHERE', $queryHavingData);
        $queryHaving = isset($queryHavingData[1]) ? $queryHavingData[1] : '';

        if (!empty($queryHaving)) {

            $this->db->having($queryHaving);
        }

        $select_column = array($tbl_payrate . ".id", $tbl_payrate . ".level_number", $tbl_payrate . ".start_date", $tbl_payrate . ".end_date", $tbl_payrate . ".paypoint",$tbl_payrate . ".status",$tbl_payrate . ".archive",$tbl_payrate . ".level_number","fpc.name as category",$tbl_payrate . ".category as category_int",$tbl_payrate . ".name as payrate_name");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        $this->db->select("case when $tbl_payrate.level_number!=0 THEN (SELECT level_name FROM tbl_classification_level as cl where cl.id=$tbl_payrate.level_number AND cl.archive=0) ELSE '' END as level_number", false);
        $this->db->select("case when $tbl_payrate.paypoint!=0 THEN (SELECT point_name FROM tbl_classification_point as cp where cp.id=$tbl_payrate.paypoint AND cp.archive=0) ELSE '' END as point_name", false);
        $this->db->select("case when $tbl_payrate.paypoint!=0 THEN (SELECT name FROM tbl_finance_payrate_type as fpt where fpt.id=$tbl_payrate.type AND fpt.archive=0) ELSE '' END as payrate_type", false);
       
        $this->db->from($tbl_payrate);
        $this->db->join('tbl_finance_payrate_category as fpc', 'fpc.id =  ' . $tbl_payrate . '.category AND fpc.archive=0', 'INNER');
        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where(array($tbl_payrate.".archive"=>0));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $all_count = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = $query->result();
        $data = [];

        if (!empty($dataResult)) {

            $payrateIds = array_column(obj_to_arr($dataResult), 'id');
            $payrateIds = empty($payrateIds) ? [0] : $payrateIds;
            $hourly_rate = $this->get_addtitonal_paypoint_ratetype($payrateIds);

            $color_class= '';
            foreach ($dataResult as $val) 
            {
                if($val->category_int == 5){
                    $val->ratename = isset($val->payrate_name)?$val->payrate_name:'';
                    $val->title = $val->category;
                }
                else{
                    $val->ratename = isset($val->level_name)?$val->level_name:''.' - '.isset($val->point_name)?$val->point_name:'';
                    $val->title = $val->category.' - '.$val->payrate_type;
                }
                
                $next_date = date('Y-m-d', strtotime( ' +1 day')); 
                                
                if( strtotime($val->start_date) == strtotime(date('Y-m-d')) || strtotime($val->start_date) < strtotime(date('Y-m-d')) &&  strtotime($val->end_date) >= strtotime(date('Y-m-d')) ){
                    $color_class = 'btn_color_avaiable short_buttons_01';
                    $status_title = 'Active';
                    $val->archive = 1;
                }
                else if( strtotime($val->start_date) > strtotime(date('Y-m-d'))){
                    $color_class = 'btn_color_unavailable short_buttons_01';
                    $status_title = 'Inactive';
                    $val->archive = 1;
                }
                else if( strtotime($val->end_date) < strtotime(date('Y-m-d'))){
                    $color_class = ' btn_color_archive short_buttons_01';
                    $status_title = 'Archieve';
                }
                else{
                    $color_class = '';
                    $status_title = '';
                }

                $val->color_class = $color_class;
                $val->status_title = $status_title;

                $val->start_date = date('d/m/Y',strtotime($val->start_date));
                $val->end_date = date('d/m/Y',strtotime($val->end_date));

                $val->payrateDetail = isset($hourly_rate[$val->id]) ? $hourly_rate[$val->id] : [];
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'all_count' => $all_count);
        return $return;
    }

    public function get_addtitonal_paypoint_ratetype($payrateIds) 
    {
        $this->db->select(['fpp.id','fpp.payrate_id','fpp.rate_type','fpp.increased_by','fpp.dollar_value']);
        $this->db->select("case when fpp.rate_type!=0 THEN (SELECT name FROM tbl_finance_addtitonal_paypoint_ratetype as fapr where fapr.id=fpp.rate_type AND fapr.archive=0) ELSE '' END as name", false);        
        $this->db->from('tbl_finance_payrate_paypoint as fpp');
        $this->db->join('tbl_finance_payrate as fp', 'fp.id = fpp.payrate_id AND fpp.archive=0', 'inner');
        #$this->db->join('tbl_finance_addtitonal_paypoint_ratetype as fapr', 'fapr.id = fpp.rate_type', 'inner');
        $this->db->where_in('fpp.payrate_id', $payrateIds);
        $this->db->order_by('fpp.increased_by','ASC');
        $query = $this->db->get();

        $res = $query->result_array();
        $complete_ary = [];
        if (!empty($res)) {
            $temp_hold = [];
            foreach ($res as $key => $value) {
                $payrate_id = $value['payrate_id'];
                if (!in_array($payrate_id, $temp_hold)) {
                    $temp_hold[] = $payrate_id;
                }
                $temp['payrate_id'] = $payrate_id;
                $temp['rate_type'] = $value['rate_type'];
                $temp['increased_by'] = $value['increased_by'];
                $temp['dollar_value'] = $value['dollar_value'];
                $temp['name'] = $value['name'];
                $complete_ary[$payrate_id][] = $temp;
            }
        }
        return $complete_ary;
    }

    public function get_payrate_data($reqData)
    {
        if(!empty($reqData))
        {
            $payrate_id = $reqData->data->id;
            $tbl_payrate = TBL_PREFIX . 'finance_payrate'; 
            $select_column = array($tbl_payrate . ".id", $tbl_payrate . ".status", $tbl_payrate . ".start_date", $tbl_payrate . ".end_date", $tbl_payrate . ".paypoint",$tbl_payrate . ".status",$tbl_payrate . ".archive",$tbl_payrate . ".level_number",$tbl_payrate . ".category",$tbl_payrate . ".type",$tbl_payrate . ".name",$tbl_payrate . ".comments","(select group_concat(rate_type) from tbl_finance_payrate_paypoint where payrate_id = $payrate_id AND archive=0  ) as selected_payrate_paypoint");

            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from($tbl_payrate);
            $this->db->where('id', $payrate_id);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $job_data_ary = $query->row_array();

            if(empty($job_data_ary) || is_null($job_data_ary))
            {
                $return = array('status' => false);
                return $return;
            }

            $main_aray = array(); 

            $this->db->select(['fpp.rate_type as rate','fpp.increased_by as increasedBy','fpp.dollar_value as dollarValue']);
            $this->db->from('tbl_finance_payrate_paypoint as fpp');
            $this->db->where('fpp.payrate_id', $payrate_id);
            $this->db->where('fpp.archive', 0);

            $this->db->order_by('fpp.rate_type','ASC');
            $query = $this->db->get();
            #last_query();
            $res = $query->result_array();
            $main_aray['payPoints'] = !empty($res)?$res:[];

            $response = $this->basic_model->get_record_where('finance_addtitonal_paypoint_ratetype', $column = array('name as label', 'id as value'), $where = array('archive' => 0));
            if (!empty($response)) {
                foreach ($response as $val) {
                    $disabled = false;
                    if(in_array($val->value, explode(',', $job_data_ary['selected_payrate_paypoint'])))
                        $disabled = true;
                    $paypoint_ratetype[] = array('label' => $val->label, 'value' => $val->value, 'disabled' => $disabled);
                }
            }

            $main_aray['addtitonalPaypointRatetype'] = isset($paypoint_ratetype) && !empty($paypoint_ratetype) ? $paypoint_ratetype : array();

            $main_aray['start_date'] = $job_data_ary['start_date']!=''?$job_data_ary['start_date']:'';
            $main_aray['end_date'] = $job_data_ary['end_date']!=''?$job_data_ary['end_date']:'';
            $main_aray['paypoint'] = $job_data_ary['paypoint']!=''?$job_data_ary['paypoint']:'';
            $main_aray['level_number'] = $job_data_ary['level_number']!=''?$job_data_ary['level_number']:'';
            $main_aray['category'] = $job_data_ary['category']!=''?$job_data_ary['category']:'';
            $main_aray['type'] = $job_data_ary['type']!=''?$job_data_ary['type']:'';
            $main_aray['page_title'] = 'Edit ';
            $main_aray['comments'] = $job_data_ary['comments']!=''?$job_data_ary['comments']:'';
            $return = array('status' => true, 'data' => $main_aray);
            return $return;
        }
    }


}
