<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Finance_line_item extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function index() {
        
    }

    public function add_update_line_item($reqData) {
        $create_new_line_item = false;

        $start_date = DateFormate($reqData->start_date, 'Y-m-d');
        $end_date = DateFormate($reqData->end_date, 'Y-m-d');

        if (!empty($reqData->lineItemId)) {

            $line_item_details = $this->get_line_item_details($reqData->lineItemId);

            $prev_start_date = (DateFormate($line_item_details['start_date'], 'Y-m-d'));
            $prev_end_date = (DateFormate($line_item_details['end_date'], 'Y-m-d'));

            $curr_date = strtotime(date('Y-m-d'));

            // 1 for active  2  inactive  3 archive
            if ($line_item_details['status'] == 1) {
                // check active line item start date updated?

                if (strtotime($start_date) <= $curr_date && $curr_date < strtotime($end_date)) {
                    $update_date = ['end_date' => DATE_TIME, 'updated' => DATE_TIME];
                    $this->basic_model->update_records('finance_line_item', $update_date, ['id' => $reqData->lineItemId]);

                    $start_date = date('Y-m-d', strtotime('+1 day', strtotime(DATE_TIME)));
                    $create_new_line_item = true;
                } elseif (strtotime($start_date) >= strtotime($prev_end_date)) {
                    $create_new_line_item = true;
                }
            }
        }


        $line_item_data = array(
            'funding_type' => $reqData->funding_type,
            'support_registration_group' => ((!empty($reqData->support_registration_group)) ? $reqData->support_registration_group : ''),
            'support_category' => ((!empty($reqData->support_category)) ? $reqData->support_category : ''),
            'support_outcome_domain' => ((!empty($reqData->support_outcome_domain)) ? $reqData->support_outcome_domain : ''),
            'line_item_number' => $reqData->line_item_number,
            'line_item_name' => ((!empty($reqData->line_item_name)) ? $reqData->line_item_name : ''),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'description' => ((!empty($reqData->description)) ? $reqData->description : ''),
            'quote_required' => ((!empty($reqData->quote_required) && $reqData->quote_required == 1) ? 1 : 0),
            'price_control' => ((!empty($reqData->price_control) && $reqData->price_control == 1) ? 1 : 0),
            'travel_required' => ((!empty($reqData->travel_required) && $reqData->travel_required == 1) ? 1 : 0),
            'cancellation_fees' => ((!empty($reqData->cancellation_fees) && $reqData->cancellation_fees == 1) ? 1 : 0),
            'ndis_reporting' => ((!empty($reqData->ndis_reporting) && $reqData->ndis_reporting == 1) ? 1 : 0),
            'non_f2f' => ((!empty($reqData->non_f2f) && $reqData->non_f2f == 1) ? 1 : 0),
            'upper_price_limit' => ((!empty($reqData->upper_price_limit)) ? $reqData->upper_price_limit : ''),
            'national_price_limit' => ((!empty($reqData->national_price_limit)) ? $reqData->national_price_limit : ''),
            'national_very_price_limit' => ((!empty($reqData->national_very_price_limit)) ? $reqData->national_very_price_limit : ''),
            'levelId' => ((!empty($reqData->levelId)) ? $reqData->levelId : ''),
            'pay_pointId' => ((!empty($reqData->pay_pointId)) ? $reqData->pay_pointId : ''),
            'schedule_constraint' => ((!empty($reqData->schedule_constraint)) ? $reqData->schedule_constraint : ''),
            'public_holiday' => ((!empty($reqData->public_holiday)) ? $reqData->public_holiday : ''),
            'member_ratio' => $reqData->member_ratio,
            'participant_ratio' => $reqData->participant_ratio,
            'created' => DATE_TIME,
            'updated' => DATE_TIME,
        );


        // according to check condition insert or update
        if (!empty($reqData->lineItemId) && !$create_new_line_item) {
            $line_itemId = $reqData->lineItemId;
            $this->basic_model->update_records('finance_line_item', $line_item_data, ['id' => $reqData->lineItemId]);
        } else {
            $line_itemId = $this->basic_model->insert_records('finance_line_item', $line_item_data, false);
        }

        // insert update line item week days
        if (!empty($reqData->week_days)) {
            $this->update_line_item_week_days($reqData->week_days, $line_itemId);
        }

        // insert update line item time of the day
        if (!empty($reqData->time_of_the_days)) {
            $this->update_line_item_time($reqData->time_of_the_days, $line_itemId);
        }

        // insert update line item state
        if (!empty($reqData->state)) {
            $this->update_line_item_state($reqData->state, $line_itemId);
        }

        return $line_itemId;
    }

    function update_line_item_state($states, $line_itemId) {
        $previous_data = $this->basic_model->get_record_where('finance_line_item_applied_state', ['stateId'], $where = ['line_itemId' => $line_itemId, 'archive' => 0]);
        $previous_data = array_column(obj_to_arr($previous_data), 'stateId');

        $archive_ids = [];
        if (!empty($states)) {

            foreach ($states as $val) {
                if (!empty($val->selected) && !in_array($val->id, $previous_data)) {

                    $state[] = ['line_itemId' => $line_itemId, 'stateId' => $val->id, 'archive' => 0, 'created' => DATE_TIME];
                } elseif (in_array($val->id, $previous_data) && empty($val->selected)) {
                    $archive_ids[] = $val->id;
                }
            }

            if (!empty($state)) {
                $this->basic_model->insert_records('finance_line_item_applied_state', $state, true);
            }

            if (!empty($archive_ids)) {
                $this->db->where('line_itemId', $line_itemId);
                $this->db->where_in('stateId', $archive_ids);
                $this->db->update('tbl_finance_line_item_applied_state', ['archive' => 1]);
            }
        }
    }

    function update_line_item_time($time_of_the_days, $line_itemId) {
        $previous_data = $this->basic_model->get_record_where('finance_line_item_applied_time', ['finance_timeId'], $where = ['line_itemId' => $line_itemId, 'archive' => 0]);
        $previous_data = array_column(obj_to_arr($previous_data), 'finance_timeId');

        $archive_ids = [];
        if (!empty($time_of_the_days)) {
            foreach ($time_of_the_days as $val) {
                if (!empty($val->selected) && !in_array($val->id, $previous_data)) {

                    $times[] = ['line_itemId' => $line_itemId, 'finance_timeId' => $val->id, 'archive' => 0, 'created' => DATE_TIME];
                } elseif (in_array($val->id, $previous_data) && empty($val->selected)) {
                    $archive_ids[] = $val->id;
                }
            }

            if (!empty($times)) {
                $this->basic_model->insert_records('finance_line_item_applied_time', $times, true);
            }

            if (!empty($archive_ids)) {
                $this->db->where('line_itemId', $line_itemId);
                $this->db->where_in('finance_timeId', $archive_ids);
                $this->db->update('tbl_finance_line_item_applied_time', ['archive' => 1]);
            }
        }
    }

    function update_line_item_week_days($week_days, $line_itemId) {
        $previous_data = $this->basic_model->get_record_where('finance_line_item_applied_days', ['week_dayId'], $where = ['line_itemId' => $line_itemId, 'archive' => 0]);
        $previous_data = array_column(obj_to_arr($previous_data), 'week_dayId');

        $archive_ids = [];
        if (!empty($week_days)) {
            foreach ($week_days as $val) {
                if (!empty($val->selected) && !in_array($val->id, $previous_data)) {

                    $days[] = ['line_itemId' => $line_itemId, 'week_dayId' => $val->id, 'archive' => 0, 'created' => DATE_TIME];
                } elseif (in_array($val->id, $previous_data) && empty($val->selected)) {
                    $archive_ids[] = $val->id;
                }
            }

            if (!empty($days)) {
                $this->basic_model->insert_records('finance_line_item_applied_days', $days, true);
            }

            if (!empty($archive_ids)) {
                $this->db->where('line_itemId', $line_itemId);
                $this->db->where_in('week_dayId', $archive_ids);
                $this->db->update('tbl_finance_line_item_applied_days', ['archive' => 1]);
            }
        }
    }

    function get_finance_line_item_listing($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $src_columns = array('fli.line_item_number', 'fli.line_item_name', "fli.description", "fli.upper_price_limit", "fli.national_price_limit", "fli.national_very_price_limit");

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'fli.id';
            $direction = 'DESC';
        }

        if (!empty($filter->filter_by_status)) {
            $cur_date = date('Y-m-d');

            if ($filter->filter_by_status == 'active') {
                $this->db->where("'$cur_date' BETWEEN start_date AND end_date", null, false);
            } elseif ($filter->filter_by_status == 'archive') {
                $this->db->where("end_date < ", $cur_date);
            } elseif ($filter->filter_by_status == 'inactive') {
                $this->db->where("start_date > ", $cur_date);
            }
        }

        if (!empty($filter->filter_by_funding_type)) {
            if ($filter->filter_by_funding_type !== 'all') {
                $this->db->where("fli.funding_type", $filter->filter_by_funding_type);
            }
        }

        if (!empty($filter->filter_by_state)) {
            if ($filter->filter_by_state !== 'all') {
                $this->db->where("applied_state.stateId", $filter->filter_by_state);
                $this->db->join('tbl_finance_line_item_applied_state as applied_state', 'applied_state.line_itemId = fli.id', 'inner');
            }
        }

        if (!empty($filter->start_date)) {
            $this->db->where('DATE_FORMAT(fli.start_date, "%Y-%m-%d") = ', DateFormate($filter->start_date, 'Y-m-d'));
        }if (!empty($filter->end_date)) {
            $this->db->where('DATE_FORMAT(fli.end_date, "%Y-%m-%d") = ', DateFormate($filter->end_date, 'Y-m-d'));
        }


        if (!empty($filter->search)) {
            $this->db->group_start();
            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    if ($serch_column[0] != 'null')
                        $this->db->or_like($serch_column[0], $filter->search);
                }
                else if ($column_search != 'null') {
                    $this->db->or_like($column_search, $filter->search);
                }
            }
            $this->db->group_end();
        }


        $select_column = array("fli.id", 'fli.line_item_number', 'fli.line_item_name', 'fli.start_date', "fli.end_date", 'fli.upper_price_limit', "fft.name as funding_type", "fli.description", "fli.national_price_limit", "fli.national_very_price_limit", "fli.member_ratio", "fli.participant_ratio", "fli.schedule_constraint");

        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);

        $this->db->from('tbl_finance_line_item as fli');
        $this->db->join('tbl_funding_type as fft', 'fft.id = fli.funding_type', 'inner');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $start_date = (int) strtotime(DateFormate($val->start_date, "Y-m-d"));
                $end_date = (int) strtotime(DateFormate($val->end_date, "Y-m-d"));
                $current_date = (int) strtotime(date('Y-m-d'));

                if ($start_date <= $current_date && $current_date <= $end_date) {
                    $val->status = "1"; //1 for active
                } elseif ($start_date > $current_date) {
                    $val->status = "2"; //2 inactive
                } else {
                    $val->status = "3"; //3 archive
                }
            }
        }
        $return = array('count' => $dt_filtered_total, 'data' => $result, 'status' => true);
        return $return;
    }

    function archive_line_item($lineItemId) {
        $this->db->select(['fli.id', "fli.start_date", "fli.end_date"]);
        $this->db->from('tbl_finance_line_item as fli');
        $this->db->where('fli.id', $lineItemId);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row();

        if (!empty($result)) {
            $current_strtotime = strtotime(date('Y-m-d'));

            if (strtotime($result->start_date) > $current_strtotime) {
                $res = ['status' => false, 'error' => 'Inactive line item can not be archive'];
            } elseif (strtotime($result->end_date) < $current_strtotime) {
                $res = ['status' => false, 'error' => 'line item already archived'];
            } elseif (strtotime($result->end_date) == $current_strtotime) {
                $res = ['status' => false, 'error' => 'Today line line item will be archived'];
            } else {
                $this->basic_model->update_records('finance_line_item', ['end_date' => DATE_TIME], ['id' => $lineItemId]);

                $res = ['status' => true];
            }
        } else {
            $res = ['status' => false, 'error' => 'Line Item not found'];
        }

        return $res;
    }

    function get_line_item_details($lineItemId) {
        $this->db->select(['fli.id', "fli.funding_type", "fli.support_registration_group", "fli.support_category", "fli.support_outcome_domain", "fli.line_item_number", "fli.line_item_name", "fli.start_date", "fli.end_date", "fli.description", "fli.quote_required", "fli.price_control", "fli.travel_required", "fli.cancellation_fees", "fli.ndis_reporting", "fli.non_f2f", "fli.upper_price_limit", "fli.national_price_limit", "fli.national_very_price_limit", "fli.schedule_constraint", "fli.public_holiday", "fli.member_ratio", "fli.participant_ratio", "fli.pay_pointId", "fli.levelId"]);

        $this->db->from('tbl_finance_line_item as fli');
        $this->db->where('fli.id', $lineItemId);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row_array();

        if (!empty($result)) {
            $start_date = (int) strtotime(DateFormate($result['start_date'], "Y-m-d"));
            $end_date = (int) strtotime(DateFormate($result['end_date'], "Y-m-d"));
            $current_date = (int) strtotime(date('Y-m-d'));

            if ($start_date <= $current_date && $current_date <= $end_date) {
                $result['status'] = 1; // 1 for active
            } elseif ($start_date > $current_date) {
                $result['status'] = 2; // inactive
            } else {
                $result['status'] = 3; // archive
            }
        } else {
            $result = [];
        }

        return $result;
    }

    function get_time_of_day_of_line_item($lineItemId) {
        $this->db->select(['f_time.id', "f_time.name"]);
        if ($lineItemId > 0) {
            $this->db->select('(select 1 from tbl_finance_line_item_applied_time where finance_timeId = f_time.id AND archive = 0 AND line_itemId = "' . $lineItemId . '" limit 1) as selected');
        }

        $this->db->from('tbl_finance_time_of_the_day as f_time');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                if (!empty($val->selected)) {
                    $val->selected = $val->selected == 1 ? true : false;
                }
            }
        }

        return $result;
    }

    function get_week_days_of_line_item($lineItemId) {
        $this->db->select(['w_day.id', "w_day.name"]);
        if ($lineItemId > 0) {
            $this->db->select('(select 1 from tbl_finance_line_item_applied_days where week_dayId = w_day.id AND archive = 0 AND line_itemId = "' . $lineItemId . '" limit 1) as selected');
        }

        $this->db->from('tbl_week_day as w_day');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                if (!empty($val->selected)) {
                    $val->selected = $val->selected == 1 ? true : false;
                }
            }
        }

        return $result;
    }

    function get_state_of_line_item($lineItemId) {
        $this->db->select(['s.id', "s.long_name"]);
        if ($lineItemId > 0) {
            $this->db->select('(select 1 from tbl_finance_line_item_applied_state where stateId = s.id AND archive = 0 AND line_itemId = "' . $lineItemId . '" limit 1) as selected');
        }

        $this->db->from('tbl_state as s');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                if (!empty($val->selected)) {
                    $val->selected = $val->selected == 1 ? true : false;
                }
            }
        }

        return $result;
    }

    function check_line_item_number_already_exist($reqData) {
        if (!empty($reqData->lineItemId)) {
            $this->db->where('id !=', $reqData->lineItemId, false);
        }

        if (!empty($reqData->start_date) && !empty($reqData->end_date)) {
            $start_date = DateFormate($reqData->start_date, 'Y-m-d');
            $end_date = DateFormate($reqData->end_date, 'Y-m-d');

            $this->db->where('("' . $start_date . '" BETWEEN start_date and end_date OR "' . $end_date . '" BETWEEN start_date and end_date)', null, false);
        }

        $this->db->select(['id']);
        $this->db->from('tbl_finance_line_item');
        $this->db->where('line_item_number', $reqData->line_item_number);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        return $query->row();
    }

    function insert_import_line_item($row_array){       
        $this->db->insert('tbl_finance_line_item',$row_array);
    }

    function insert_bulk_import_line_item($data_array){
        $this->db->trans_start();
        $line_item_id =0;
        foreach($data_array as $data){

            $list_item=$data;
            $states=$data['states'];
            $weekofday=$data['weekofday'];
            $timeofday=$data['timeofday'];
            
            unset($list_item['states']);
            unset($list_item['weekofday']);
            unset($list_item['timeofday']);
            
            $this->db->insert('tbl_finance_line_item',$list_item);
            $line_item_id = $this->db->insert_id();
            if($line_item_id>0){
                if(!empty($states)){

                    $states= array_map(function($row) use ($line_item_id){
                        if(isset($row['line_itemId']) && empty($row['line_itemId'])){  $row['line_itemId']=$line_item_id; }
                        return $row;
                    },$states);                    
                    $this->db->insert_batch('tbl_finance_line_item_applied_state',$states);
                }

                if(!empty($weekofday)){
                    $weekofday= array_map(function($row) use ($line_item_id){
                        if(isset($row['line_itemId']) && empty($row['line_itemId'])){  $row['line_itemId']=$line_item_id; }
                        return $row;
                    },$weekofday);

                    $this->db->insert_batch('tbl_finance_line_item_applied_days',$weekofday);
                }
                if(!empty($weekofday)){                   
                    $timeofday= array_map(function($row) use ($line_item_id){
                        if(isset($row['line_itemId']) && empty($row['line_itemId'])){  $row['line_itemId']=$line_item_id; }
                        return $row;
                    },$timeofday);                    
                    $this->db->insert_batch('tbl_finance_line_item_applied_time',$timeofday);
                }
            }
            
        }
        
        
        $this->db->trans_complete(); 
        if ($this->db->trans_status() === FALSE) {
            # Something went wrong.
            $this->db->trans_rollback();
            return false;
        }else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_commit();
            return true;
        }
    }
    function get_line_item_csv_report($exportType){

        $cur_date = DateFormate(DATE_TIME, 'Y-m-d');      
      
        $this->db->select('(select name from tbl_finance_support_registration_group where batchId = fli.support_registration_group AND archive = 0) as support_registration_group_name', null, false);
        $this->db->select('(select name from tbl_finance_support_category where id = fli.support_category AND archive = 0) as support_category_name', null, false);
        $this->db->select('(select name from tbl_finance_support_outcome_domain where id = fli.support_outcome_domain AND archive = 0) as support_outcome_domain_name', null, false);
        
        $this->db->select('(select GROUP_CONCAT(stateId SEPARATOR  "@#_BREAKER_#@") as stateId from tbl_finance_line_item_applied_state where line_itemId = fli.id AND archive = 0) as states', null, false);
        $this->db->select('(select GROUP_CONCAT(finance_timeId SEPARATOR  "@#_BREAKER_#@") as finance_timeId from tbl_finance_line_item_applied_time where line_itemId = fli.id AND archive = 0) as timeofday', null, false);
        $this->db->select('(select GROUP_CONCAT(week_dayId SEPARATOR  "@#_BREAKER_#@") as week_dayId from tbl_finance_line_item_applied_days where line_itemId = fli.id AND archive = 0) as weekofday', null, false);
        
        $select_column = array("fli.id", 'fli.support_registration_group', 'fli.support_category', 'fli.support_outcome_domain', "fli.line_item_number", 'fli.line_item_name', 
        "fli.description", "fli.quote_required", "fli.price_control", "fli.travel_required", "fli.cancellation_fees", "fli.ndis_reporting", "fli.non_f2f",
        "fli.upper_price_limit", "fli.national_price_limit", "fli.national_very_price_limit", "fli.schedule_constraint", "fli.public_holiday", "fli.member_ratio", "fli.non_f2f" , "fli.member_ratio","fli.participant_ratio" );

        $this->db->select($select_column);
        $this->db->from('tbl_finance_line_item as fli');        
            if ($exportType == 'active') {
                $this->db->where("'$cur_date' BETWEEN fli.start_date AND fli.end_date", null, false);
            } elseif ($exportType == 'archive') {
                $this->db->where("fli.end_date < ", $cur_date);
            } elseif ($exportType == 'inactive') {
                $this->db->where("fli.start_date > ", $cur_date);
            }
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->result();
       
        return $result;
        
    }

    function get_week_day_list(){
        $this->db->select(['id', "short_name as name"]);
        $this->db->from('tbl_week_day');
        $this->db->order_by("id", "asc");
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());        
        $result=$query->result();
        return array_column($result,'id','name');
    }
    function get_time_of_day_list() {
        $this->db->select(['id', "name"]);
        $this->db->from('tbl_finance_time_of_the_day');
        $this->db->order_by("id", "asc");
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result= $query->result();
        return array_column($result,'id','name');
        
    }
    function get_state_list(){
        $this->db->select(['id', "name"]);
        $this->db->from('tbl_state');
        $this->db->order_by("name", "asc");
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result=$query->result_array();
        return array_column($result,'id','name');
    }
}