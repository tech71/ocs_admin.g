<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Finance_cron extends MX_Controller 
{
	use callBackGroundProcess;
	function __construct() {
		parent::__construct();
		$this->load->model(['Finance_cron_model','finance/Finanace_shift_payroll_model','finance/Finance_common_model']);
		$this->load->model('Basic_model');
		
	}
	
	public function update_payrate_status()
	{
		$result = $this->Finance_cron_model->update_payrate_status();
		echo json_encode($result);
	}
	private function payroll_exemption_send_renewal_email_data($cronId,$extraParms=[]){
		
		$record = $this->Finanace_shift_payroll_model->get_org_payroll_exemption_current_status_by_current_date(0,$extraParms);	
		
		if (!empty($record)) 
		{ 
			$orgIds = array_column($record,'organisation_id');
			$orgDetails = $this->Finance_common_model->get_organisation_details_by_ids($orgIds);
			$orgDetailsData = !empty($orgDetails) ? pos_index_change_array_data($orgDetails,'org_id') :[];
			foreach($record as $row){
				$orgId = $row['organisation_id'];
				$orgDetails = isset($orgDetailsData[$orgId])? $orgDetailsData[$orgId] :[];
				if(empty($orgDetails)){
					continue;
				}
				$contactName = !empty( $orgDetails['contact_name']) ?  $orgDetails['contact_name']:$orgDetails['org_name'];
           		$email =  $orgDetails['email'];
            	$expiredOn =  DateFormate($row['fin_valid_until'],'d/m/Y');
				send_reminder_renewal_payroll_exemption_email((object)['to_email'=>$email,'org_contact_name'=>$contactName,'expire_untill'=>$expiredOn]);
			}
			$extraParms['current_page'] = $extraParms['current_page']+1;
			return $this->payroll_exemption_send_renewal_email_data($cronId,$extraParms);
			
		}else{
			$this->Basic_model->update_records('cron_status', array('status'=>'1'),['id'=>$cronId]);
			return ['status'=>true,'msg'=>'cron run successfully','parms'=>$extraParms];
		}

	}

	public function payroll_exemption_send_renewal_email(){
		$parms = [
			'cron_call'=>'1',
			'return_type'=>'2',
			'limit_size'=>'30',
			'current_page'=>'0',
		];
		$response = $this->call_background_process('payroll_send_renewal_email',['interval_minute'=>25,'method_call'=>'payroll_exemption_send_renewal_email_data','method_params'=>$parms]);
		echo json_encode($response);
		exit;
	}
}