<?php

use classRoles as adminRoles;
use AdminClass as AdminClass;

require_once APPPATH . 'traits/formCustomValidation.php';
require_once APPPATH . 'Classes/websocket/Websocket.php';

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller  
class Dashboard extends CI_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->loges->setLogType('user_admin');
        $this->load->library('form_validation');
    }

    public function index() {
        
    }

    public function get_user_details() {
        $reqData = request_handler('access_admin', 1, 1);
        require_once APPPATH . 'Classes/admin/admin.php';
        $objAdmin = new AdminClass\Admin();

        if ($reqData->data) {
            $objAdmin->setAdminid($reqData->data->AdminId);

            $result = $objAdmin->get_admin_details();
            if (!empty($result)) {
                $result['PhoneInput'] = $objAdmin->get_admin_phone_number();
                $result['EmailInput'] = $objAdmin->get_admin_email();

                $admins_its = $this->config->item('super_admins');

                $result['its_super_admin'] = in_array($objAdmin->getAdminid(), $admins_its);
                $response = array('status' => true, 'data' => $result);
            } else {
                $response = array('status' => false, 'data' => 'Invalid request');
            }

            echo json_encode($response);
        }
    }

    public function create_edit_user() {
        // get request data

        $reqData = request_handler('access_admin', 1, 1);


        $this->loges->setCreatedBy($reqData->adminId);

        require_once APPPATH . 'Classes/admin/admin.php';
        $objAdmin = new AdminClass\Admin();

        if (!empty($reqData->data)) {
            $data = (array) $reqData->data;

            $data['id'] = (!empty($data['id'])) ? $data['id'] : false;

            $this->form_validation->set_data($data);
            $validation_rules = array(
                array('field' => 'username', 'label' => 'UserName', 'rules' => 'callback_check_username_already_exist[' . $data['id'] . ']'),
                array('field' => 'firstname', 'label' => 'First Name', 'rules' => 'required'),
                array('field' => 'lastname', 'label' => 'Last Name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Date Of Birth', 'rules' => 'required'),
                array('field' => 'EmailInput[]', 'label' => 'Email address', 'rules' => 'callback_check_user_emailaddress_already_exist[' . $data['id'] . ']'),
                array('field' => 'PhoneInput[]', 'label' => 'phone number', 'rules' => 'callback_check_phone_number_validation|callback_phone_number_check[name,required,User contact should be enter valid phone number.]'),
            );


            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $objAdmin->setUsername($data['username']);
                $objAdmin->setPassword($data['password']);
                $objAdmin->setFirstname($data['firstname']);
                $objAdmin->setLastname($data['lastname']);
                $objAdmin->setDepartment($data['department']);
                $objAdmin->setPosition($data['position']);
                $objAdmin->setSecondaryEmails($data['EmailInput']);
                $objAdmin->setPrimaryEmail($data['EmailInput'][0]->name);
                $objAdmin->setSecondaryPhone($data['PhoneInput']);
                $objAdmin->setRoles($data['rolesList']);

                $rand = mt_rand(10, 100000);
                $token = encrypt_decrypt('encrypt', $rand);

                $objAdmin->setToken($token);

                if ($data['id'] > 0) {
                    //check permission
                    check_permission($reqData->adminId, 'update_admin');

                    // update user
                    $objAdmin->setAdminid($data['id']);
                    $objAdmin->updateUser();
                    $objAdmin->updateRoleToAdmin();

                    $this->loges->setTitle('User profile update: ' . $objAdmin->getFirstname() . ' ' . $objAdmin->getLastname());
                } else {
                    //check permission
                    check_permission($reqData->adminId, 'create_admin');

                    // create user
                    $objAdmin->createUser();
                    $objAdmin->updateRoleToAdmin();
                    // send welcome to admin
                    $objAdmin->send_welcome_mail();

                    $this->loges->setTitle('New admin create: ' . $objAdmin->getFirstname() . ' ' . $objAdmin->getLastname());
                }

                $this->loges->setDescription(json_encode($reqData->data));
                $this->loges->createLog();

                // insert secondary email
                $objAdmin->insertEmail();

                // insert secondary email
                $objAdmin->insertPhone();

                $wbObj = new Websocket();
                // check websoket here send and update permission
                if ($wbObj->check_webscoket_on()) {
                    $data = array('chanel' => 'server', 'req_type' => 'update_user_admin', 'token' => $wbObj->get_token(), 'data' => ['adminId' => $objAdmin->getAdminid(), 'update_type' => 'update_permission']);
                    $wbObj->send_data_on_socket($data);
                }

                $response = array('status' => true);
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function check_phone_number_validation($phone_numbers) {
        foreach ($phone_numbers as $val) {
            if (empty($val)) {
                $this->form_validation->set_message('phone number can not empty', 'phone number can not be empty');
                return false;
            }
        }
        return true;
    }

    // this function used for add and edit role
    public function add_role() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();

        if ($this->input->post()) {
            $role_id = $this->input->post('role_id');
            $objRoles->setrolename($this->input->post('role_name'));
            $objRoles->setroleid($role_id);
            $res = $objRoles->checkAlreadyExist();

            if ($res) {
                if ($role_id > 0) {
                    $objRoles->UpdateAllRoles();
                } else {
                    $objRoles->CreateRole();
                }
                $response = array('status' => true);
            } else {
                $response = array('status' => false, 'error' => 'This role already exist');
            }

            echo json_encode($response);
        }
    }

    public function list_role() {
        $this->load->model('admin_model');

        if ($this->input->post()) {
            $reqData = json_decode($this->input->post('request'));
            $response = $this->admin_model->list_role_dataquery($reqData);

            echo json_encode($response);
        }
    }

    public function delete_role() {
        require_once APPPATH . 'Classes/admin/role.php';
        $objRoles = new adminRoles\Roles();

        if ($this->input->post()) {
            $objRoles->setArchive(1);
            $objRoles->setroleid($this->input->post('role_id'));

            $objRoles->UpdateRoles();

            echo json_encode(array('status' => true));
        }
    }

    public function active_inactive_role() {
        if ($this->input->post()) {
            $role_id = $this->input->post('role_id');
            $status = $this->input->post('status');

            $result = $this->basic_model->update_records('role', $role_data = array('status' => $status), $where = array('id' => $role_id));

            echo json_encode(array('status' => true));
        }
    }

    // this method use for check user email already exist
    public function check_user_emailaddress_already_exist($sever_emailAddress = array(), $adminId_NEW = false) {
        $this->load->model('admin_model');
        $adminId = false;

        if ($this->input->get() || $sever_emailAddress) {
            $emails = ($this->input->get()) ? $this->input->get() : (array) $sever_emailAddress;

            if ($this->input->get('adminId') || $adminId_NEW) {
                $adminId = ($this->input->get('adminId')) ? $this->input->get('adminId') : $adminId_NEW;
                if (array_key_exists('adminId', $emails)) {
                    unset($emails['adminId']);
                };
            }


            foreach ($emails as $val) {
                $result = $this->admin_model->check_dublicate_email($val, $adminId);
                if ($sever_emailAddress) {
                    if (!empty($result)) {
                        $this->form_validation->set_message('check_user_emailaddress_already_exist', 'this ' . $result[0]->email . ' Email address Allready Exist');
                        return false;
                    }
                }

                if ($this->input->get()) {
                    if (!empty($result)) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }

                return true;
            }
        }
    }

    // this method use for check username already exist
    public function check_username_already_exist($sever_end_username = false, $adminId = false) {
        if ($this->input->get('username') || $sever_end_username) {
            $username = ($this->input->get('username')) ? $this->input->get('username') : $sever_end_username;
            $where = array('username' => $username);


            if ($this->input->get('adminId') || $adminId) {
                $where['id !='] = ($this->input->get('adminId')) ? $this->input->get('adminId') : $adminId;
            }

            $result = $this->basic_model->get_row('member', array('username'), $where);

            if ($sever_end_username) {
                if (!empty($result)) {
                    $this->form_validation->set_message('check_username_already_exist', 'User Name Allready Exist');
                    return false;
                }
                return true;
            }

            if (!empty($result)) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    // using this method get admin list
    public function list_admins() {
        $reqData = request_handler('access_admin', 1, 1);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $reqData = json_decode($reqData);
            $response = $this->admin_model->list_admins_dataquery($reqData);

            echo json_encode($response);
        }
    }

    // this mehtod use for delete user
    public function delete_user() {
        $reqData = request_handler('delete_admin', 1, 1);
        $reqData = $reqData->data;

        $wbObj = new Websocket();

        if (!empty($reqData->id)) {
            $adminID = $reqData->id;

            if (in_array($adminID, get_super_admins())) {
                $return = array('status' => false, 'error' => 'Sorry supar admin can not be delete');
            } else {
                $this->basic_model->update_records('member', array('archive' => 1), $where = array('id' => $adminID));
                $this->basic_model->delete_records('member_login', $where = array('memberId' => $adminID));
                $return = array('status' => true);

                // check websoket here send and logout user
                if ($wbObj->check_webscoket_on()) {
                    $data = array('chanel' => 'server', 'req_type' => 'update_user_admin', 'token' => $wbObj->get_token(), 'data' => ['adminId' => $adminID, 'update_type' => 'logout_admin_user']);
                    $wbObj->send_data_on_socket($data);
                }
            }
        } else {
            $return = array('status' => false, 'error' => 'Invalid adminId');
        }

        echo json_encode($return);
    }

    // this method use for active and inactive user
    public function active_inactive_user() {
        $reqestData = request_handler('update_admin', 1, 1);
        $reqData = $reqestData->data;

        $wbObj = new Websocket();

        if (!empty($reqData->adminID)) {
            $adminID = $reqData->adminID;
            if (in_array($adminID, get_super_admins())) {
                $return = array('status' => false, 'error' => 'Sorry supar admin can not be deactive');
            } else {
                $status = $reqData->status;
                $this->basic_model->update_records('member', $role_data = array('status' => $status), $where = array('id' => $adminID));
                $this->basic_model->delete_records('member_login', $where = array('memberId' => $adminID));
                $return = array('status' => true);

                // check websoket here send and logout user
                if ($wbObj->check_webscoket_on()) {
                    $data = array('chanel' => 'server', 'req_type' => 'update_user_admin', 'token' => $wbObj->get_token(), 'data' => ['adminId' => $adminID, 'update_type' => 'logout_admin_user']);
                    $wbObj->send_data_on_socket($data);
                }
            }
        } else {
            $return = array('status' => false, 'error' => 'Invalid adminId');
        }

        echo json_encode($return);
    }

    public function get_listing_roles() {
        // get request data
        $reqData = request_handler('access_admin', 1, 1);

        require_once APPPATH . 'Classes/admin/admin.php';
        $objAdmin = new AdminClass\Admin();

        if (!empty($reqData->data->AdminId)) {
            $objAdmin->setAdminid($reqData->data->AdminId);
            $response = $objAdmin->getUserBasedRoles(1);
        } else {
            $response = $this->basic_model->get_record_where('role', $column = array('name', 'id', 'role_key'), $where = array('status' => 1, 'archive' => 0));
        }
        echo json_encode(array('status' => true, 'data' => $response));
    }

    public function forgotten_pin() {
        $reqData = request_handler();

        if (!empty($reqData)) {
            require_once APPPATH . 'Classes/admin/admin.php';
            require_once APPPATH . 'Classes/admin/permission.php';

            $obj_permission = new classPermission\Permission();
            $check_admin_res = $obj_permission->check_permission($reqData->adminId, 'access_admin');
            $check_fms_res = $obj_permission->check_permission($reqData->adminId, 'access_fms');

            if ($check_admin_res || $check_fms_res) {

                $objAdmin = new AdminClass\Admin();

                $objAdmin->setAdminid($reqData->adminId);

                $rand = mt_rand(10, 100000);
                $token = encrypt_decrypt('encrypt', $rand);
                $objAdmin->setToken($token);
                $this->basic_model->update_records('member', array('otp' => $token), $where = array('id' => $reqData->adminId));

                $result = $this->basic_model->get_row('member_email', $columns = array('email'), $where = array('memberId' => $reqData->adminId, 'primary_email' => 1));
                $adminDetails = $this->basic_model->get_row('member', $columns = array('firstname', 'lastname'), $where = array('id' => $reqData->adminId));
                $objAdmin->setPrimaryEmail($result->email);

                $objAdmin->setFirstname($adminDetails->firstname);
                $objAdmin->setLastname($adminDetails->lastname);

                $objAdmin->sendResetPinMailToAdmin();
                echo json_encode(array('status' => true, 'message' => 'Please visit your mail inbox for reset your pin.'));
            } else {
                echo json_encode(array('status' => false, 'error' => 'You do not have permission to reset pin.'));
            }
        }
    }

    public function get_all_rolles() {
        $reqData = request_handler();
        if (!empty($reqData)) {
            require_once APPPATH . 'Classes/admin/admin.php';
            $objAdmin = new AdminClass\Admin();

            $objAdmin->setAdminid($reqData->adminId);
            $response = $objAdmin->getUserBasedRoles();

            echo json_encode(array('status' => true, 'data' => $response));
        }
    }

    public function get_all_permission() {
        $reqData = request_handler();
        if ($reqData) {
            $presmission['permission'] = get_all_permission($reqData->adminId);

            echo json_encode(array('status' => true, 'data' => $presmission['permission']));
        }
    }

    public function get_loges() {
        $reqData = request_handler('access_admin', 1, 1);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $reqData = ($reqData);
            $response = $this->admin_model->get_all_loges($reqData);

            echo json_encode($response);
        }
    }

    public function update_password() {
        $reqData = request_handler('access_admin');

        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $adminAuth->setAdminid($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'password', 'label' => 'confirm current', 'rules' => 'required'),
                array('field' => 'new_password', 'label' => 'new password', 'rules' => 'required|min_length[6]'),
                array('field' => 'confirm_password', 'label' => 'confirm new', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $adminAuth->setPassword($reqData->password);
                $check_password = $adminAuth->verifyCurrentPassword($reqData->password);

                if ($check_password) {
                    // set new password we need to update in db
                    $adminAuth->setPassword($reqData->new_password);

                    // update password of admin
                    $adminAuth->reset_password();

                    $response = array('status' => true, 'success' => 'Your password update successfully');
                } else {
                    $response = array('status' => false, 'error' => 'Your password not match with current password');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_pin() {
        $reqData = request_handler('access_admin');

        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $adminAuth->setAdminid($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'pin', 'label' => 'confirm current', 'rules' => 'required'),
                array('field' => 'new_pin', 'label' => 'new password', 'rules' => 'required|min_length[6]'),
                array('field' => 'confirm_pin', 'label' => 'confirm new', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $adminAuth->setPin($reqData->pin);
                $check_pin = $adminAuth->checkCurrentPin();

                if ($check_pin) {
                    // set new pin we need to update in db
                    $adminAuth->setPin($reqData->new_pin);

                    // update pin of admin
                    $adminAuth->updatePinAdmin();

                    $response = array('status' => true, 'success' => 'Your pin update successfully');
                } else {
                    $response = array('status' => false, 'error' => 'Your pin not match with current pin');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_password_recovery_email() {
        $reqData = request_handler('access_admin');

        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $adminAuth->setAdminid($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'email', 'label' => 'email', 'rules' => 'required|valid_email'),
                array('field' => 'confirm_email', 'label' => 'confir email', 'rules' => 'required|valid_email'),
                array('field' => 'password', 'label' => 'password', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $adminAuth->setPassword($reqData->password);
                $check_password = $adminAuth->verifyCurrentPassword();

                if (!$check_password) {
                    $response = array('status' => false, 'error' => 'Your enter password not match with your current password');
                    echo json_encode($response);
                    exit();
                }

                $adminAuth->setPrimaryEmail($reqData->email);

                $check_email = $adminAuth->checkExistingEmail();


                if (empty($check_email)) {
                    $token = array('email' => $reqData->email, 'time' => DATE_TIME, 'adminId' => $adminAuth->getAdminid());
                    $token = encrypt_decrypt('encrypt', json_encode($token));

                    $adminAuth->setToken($token);
                    $this->basic_model->update_records('member', array('otp' => $token), array('id' => $adminAuth->getAdminid()));

                    $result = $this->basic_model->get_row('member', array("firstname", "lastname"), array('id' => $adminAuth->getAdminid()));

                    $adminAuth->setFirstname($result->firstname);
                    $adminAuth->setLastname($result->lastname);

                    // send email verification email
                    $adminAuth->sendUpdatePasswordRecoveryEmail();

                    $response = array('status' => true, 'success' => 'Please verify your entered email address, we sended email to your entered email');
                } else {
                    $response = array('status' => false, 'error' => 'This email already exist to another user');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

}
