<?php

use classRoles as adminRoles;
use AdminClass as AdminClass;

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller  
class Notification extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('notification_model');
        $this->loges->setModule(1);
        $this->load->library('form_validation');
        $this->load->model('Notification_model');
    }

    public function get_all_notification() {
        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $request = $reqData->data;
            $reqData = json_decode($request);

            $response = $this->Notification_model->get_all_notification($reqData);
            echo json_encode($response);
        }
    }

    public function get_notification_alert() {
        // get request data

        $reqData = request_handler();
        if (!empty($reqData)) {

            $this->load->model('Notification_model');
            $response = $this->Notification_model->get_notification_alert($reqData->adminId);
            echo json_encode($response);
        }
    }

    public function clear_all_notification() {
        $reqData = request_handler();
        if (!empty($reqData)) {
            $this->load->model('Notification_model');
            $response = $this->Notification_model->clear_all_notification($reqData->adminId);
            echo json_encode($response);
        }
    }

    public function get_imail_notification() {
        $reqData = request_handler('access_imail');

        $result_ex_im = $this->Notification_model->get_external_imail_notification($reqData->adminId);
        $result_int = $this->Notification_model->get_internal_imail_notification($reqData->adminId);

        $result = array_merge($result_ex_im, $result_int);
        $res = array('ImailNotificationData' => $result, 'internal_imail_count' => count($result_int), 'external_imail_count' => count($result_ex_im));

        echo json_encode(array('status' => true, 'data' => $res));
    }

    public function clear_imail_notification() {
        $reqData = request_handler('access_imail');

        if (!empty($reqData->data)) {
            $this->Notification_model->clear_imail_notification($reqData->data, $reqData->adminId);

            echo json_encode(array('status' => true));
        }
    }

}
