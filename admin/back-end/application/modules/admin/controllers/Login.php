<?php

use Admin\Auth;

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
    }

    public function index() {
        
    }

    public function check_login() {
        $this->load->helper('cookie');
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $reqData = request_handler(0, 0);

        if (!empty($reqData)) {
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'username', 'label' => 'UserName', 'rules' => 'required'),
                array('field' => 'password', 'label' => 'First Name', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $adminAuth->setUsername($reqData->username);
                $adminAuth->setPassword($reqData->password);

                $response = $adminAuth->check_auth();

                if (!empty($response['status'])) {
                    // make history of login
                    add_login_history($adminAuth->getAdminid(), $adminAuth->getOcsToken());

                    // get all permission
                    $presmission = get_all_permission($adminAuth->getAdminid());
                    $response['permission'] = $presmission;
                    $response['type'] = ($adminAuth->getAdminid() == 1) ? 1 : 2;
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }


            echo json_encode($response);
        }
    }

    public function logout() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $adminAuth->unsetAdminLogin($reqData->data);

            // make logout histry
            logout_login_history($reqData->adminId);
            $response = array('status' => true);
        } else {
            $response = array('status' => false);
        }
        echo json_encode($response);
    }

    public function request_reset_password() {
        $this->load->helper('email_template_helper');

        // get request data
        $reqData = request_handler(0, 0);

        if (!empty($reqData)) {
            $email = $reqData->email;
            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'email', 'label' => 'email', 'rules' => 'required|valid_email'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $this->load->model('Admin_model');
                $result = $this->Admin_model->check_valid_email($email);

                if (!empty($result)) {
                    $rand = mt_rand(10, 100000);
                    $token = encrypt_decrypt('encrypt', $rand);

                    $where = array('id' => $result->id);
                    $this->basic_model->update_records('member', $data = array('otp' => $token), $where);

                    $userdata = array(
                        'firstname' => $result->firstname,
                        'lastname' => $result->lastname,
                        'email' => $email,
                        'url' => $this->config->item('server_url') . "reset_password/" . encrypt_decrypt('encrypt', $result->id) . '/' . $token . '/' . encrypt_decrypt('encrypt', strtotime(DATE_TIME)),
                    );

                    forgot_password_mail($userdata, $cc_email_address = null);
                    $response = array('status' => true, 'success' => system_msgs('forgot_password_send_mail_succefully'));
                } else {
                    $response = array('status' => false, 'error' => 'Invalid email address');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function is_password_strong($password) {
        if (preg_match('#[0-9]#', $password) && preg_match('#.*^(?=.{6,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#', $password)) {

            return TRUE;
        }
        $this->form_validation->set_message('is_password_strong', 'Password must be contain alphanumeric, number and spacial character');
        return FALSE;
    }

    function reset_password() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);
        $adminAuth = new Admin\Auth\Auth();

        $this->form_validation->set_data((array) $reqData);
        $validation_rules = array(
            array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
            array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
            array('field' => 'password', 'label' => 'password', 'rules' => 'required|min_length[6]|max_length[25]|callback_is_password_strong'),
        );

        // set rules form validation
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $user_id = encrypt_decrypt('decrypt', $reqData->id);
            $adminAuth->setAdminid($user_id);
            $adminAuth->setOcsToken($reqData->token);

            $result = $adminAuth->verify_token();

            if (!empty($result)) {
                $adminAuth->setPassword($reqData->password);
                $adminAuth->reset_password();
                $response = array('status' => true, 'success' => system_msgs('password_reset_successfully'));
            } else {
                $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
            }
        } else {
            $errors = $this->form_validation->error_array();
            $response = array('status' => false, 'error' => implode(', ', $errors));
        }

        echo json_encode($response);
    }

    public function verify_reset_password_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);

        if ($reqData) {
            $this->form_validation->set_data((array) $reqData);
            $validation_rules = array(
                array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
                array('field' => 'dateTime', 'label' => 'missing something', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $user_id = encrypt_decrypt('decrypt', $reqData->id);

                $adminAuth = new Admin\Auth\Auth();
                $adminAuth->setAdminid($user_id);
                $adminAuth->setOcsToken($reqData->token);
                $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
                $diff = strtotime(DATE_TIME) - $recieve_date_time;

                if ($diff > 3600) {
                    $response = array('status' => false, 'error' => system_msgs('link_exprire'));
                } else {
                    $result = $adminAuth->verify_token();
                    if (!empty($result)) {
                        $response = array('status' => true);
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function check_pin() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $reqData = request_handler();

        if (!empty($reqData)) {

            $this->form_validation->set_data((array) $reqData->data);
            $validation_rules = array(
                array('field' => 'pinData', 'label' => 'pin', 'rules' => 'required'),
                array('field' => 'pinType', 'label' => 'token', 'rules' => 'required'),
            );
            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $adminAuth = new Admin\Auth\Auth();

                $adminAuth->setPin($reqData->data->pinData);
                $adminAuth->setAdminid($reqData->adminId);
                $adminAuth->setPinType($reqData->data->pinType);
                $adminAuth->setToken($reqData->token);
                $response = $adminAuth->verfiy_pin();
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function verify_email_update() {
        require_once APPPATH . 'Classes/admin/auth.php';
        $reqData = request_handler('', 0, 0);

        if (!empty($reqData->token)) {
            $adminAuth = new Admin\Auth\Auth();
            $token = $reqData->token;

            $adminAuth->setToken($token);
            $data = json_decode(encrypt_decrypt('decrypt', $token));

            $auth_res = '';

            if ($data->adminId) {
                $adminAuth->setAdminid($data->adminId);
                $auth_res = $adminAuth->checkAuthToken();
            }

            if (!empty($auth_res)) {
                $adminAuth->setPrimaryEmail($data->email);
                $adminAuth->UpdatePrimaryEmail($data->adminId);

                $return = array('status' => true, 'success' => 'Your email address changed');
            } else {
                $return = array('status' => false, 'error' => 'Invalid Request');
            }
        } else {
            $return = array('status' => false, 'error' => 'Invalid Request');
        }

        echo json_encode($return);
    }

    public function verify_generate_password_pin_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);

        if ($reqData) {
            $this->form_validation->set_data((array) $reqData);
            $validation_rules = array(
                array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
                array('field' => 'dateTime', 'label' => 'missing something', 'rules' => 'required'),
            );
            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $user_id = encrypt_decrypt('decrypt', $reqData->id);

                $adminAuth = new Admin\Auth\Auth();
                $adminAuth->setAdminid($user_id);
                $adminAuth->setOcsToken($reqData->token);
                $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
                $diff = strtotime(DATE_TIME) - $recieve_date_time;
                $roles_result = (array) $adminAuth->getUserBasedRoles();
                $roles = !empty($roles_result) ? array_column($roles_result, 'id') : array();
                $pin_access = false;

                foreach ($roles as $val) {
                    $pin_access = ($val == 1 || $val == 7 || $pin_access) ? true : false;
                }

                if ($diff > 3600) {
                    $response = array('status' => false, 'error' => system_msgs('link_exprire'), 'pin_access' => $pin_access);
                } else {
                    $result = $adminAuth->verify_token();
                    if (!empty($result)) {
                        $response = array('status' => true, 'pin_access' => $pin_access);
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($response);
        }
    }

    function reset_password_pin() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);
        $adminAuth = new Admin\Auth\Auth();

        $this->form_validation->set_data((array) $reqData);
        $validation_rules = array(
            array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
            array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
            array('field' => 'password', 'label' => 'password', 'rules' => 'required|min_length[6]|max_length[25]|callback_is_password_strong'),
            array('field' => 'pin', 'label' => 'pin', 'rules' => 'callback_is_pin_required[' . json_encode(array('id' => $reqData->id, 'pin' => $reqData->pin)) . ']'),
        );

        // set rules form validation
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $user_id = encrypt_decrypt('decrypt', $reqData->id);
            $adminAuth->setAdminid($user_id);
            $adminAuth->setOcsToken($reqData->token);

            $result = $adminAuth->verify_token();

            if (!empty($result)) {
                $adminAuth->setPassword($reqData->password);
                $adminAuth->setPin($reqData->pin);
                $adminAuth->reset_password();
                $adminAuth->updatePinAdmin();
                $response = array('status' => true, 'success' => system_msgs('password_pin_update_success'));
            } else {
                $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
            }
        } else {
            $errors = $this->form_validation->error_array();
            $response = array('status' => false, 'error' => implode(', ', $errors));
        }

        echo json_encode($response);
    }

    public function is_pin_required($pin, $param) {
        $param = json_decode($param);
        $id = $param->id;
        $pin = $param->pin;
        $admin_id = encrypt_decrypt('decrypt', $id);
        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();
        $adminAuth->setAdminid($admin_id);
        $roles_result = (array) $adminAuth->getUserBasedRoles();
        $roles = !empty($roles_result) ? array_column($roles_result, 'id') : array();
        $pin_access = false;

        foreach ($roles as $val) {
            $pin_access = ($val == 1 || $val == 7 || $pin_access) ? true : false;
        }

        if ($pin_access) {
            if (empty($pin)) {
                $this->form_validation->set_message('is_pin_required', 'The pin field is required.');
                return false;
            }
            if (strlen($pin) < 6) {
                $this->form_validation->set_message('is_pin_required', 'Pin should be of six digit.');
                return false;
            }
            if (!is_numeric($pin)) {
                $this->form_validation->set_message('is_pin_required', 'Only Numbers are allowed in Pin.');
                return false;
            }
        } else {
            return TRUE;
        }
    }

    public function verify_forgot_pin_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);

        if ($reqData) {
            $this->form_validation->set_data((array) $reqData);
            $validation_rules = array(
                array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
                array('field' => 'dateTime', 'label' => 'missing something', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $user_id = encrypt_decrypt('decrypt', $reqData->id);

                $adminAuth = new Admin\Auth\Auth();
                $adminAuth->setAdminid($user_id);
                $adminAuth->setOcsToken($reqData->token);
                $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
                $diff = strtotime(DATE_TIME) - $recieve_date_time;

                if ($diff > 3600) {
                    $response = array('status' => false, 'error' => system_msgs('link_exprire'));
                } else {
                    $result = $adminAuth->verify_token();
                    if (!empty($result)) {
                        $response = array('status' => true);
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    function reset_forgot_pin_token() {
        require_once APPPATH . 'Classes/admin/auth.php';
        // get request data
        $reqData = request_handler(0, 0);

        if ($reqData) {
            $this->form_validation->set_data((array) $reqData);
            $validation_rules = array(
                array('field' => 'id', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'token', 'label' => 'token', 'rules' => 'required'),
                array('field' => 'dateTime', 'label' => 'missing something', 'rules' => 'required'),
                array('field' => 'pin', 'label' => 'pin', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $user_id = encrypt_decrypt('decrypt', $reqData->id);

                $adminAuth = new Admin\Auth\Auth();
                $adminAuth->setAdminid($user_id);
                $adminAuth->setOcsToken($reqData->token);
                $recieve_date_time = encrypt_decrypt('decrypt', $reqData->dateTime);
                $diff = strtotime(DATE_TIME) - $recieve_date_time;

                if ($diff > 3600) {
                    $response = array('status' => false, 'error' => system_msgs('link_exprire'));
                } else {
                    $result = $adminAuth->verify_token();
                    if (!empty($result)) {
                        $adminAuth->setPin($reqData->pin);
                        $adminAuth->updatePinAdmin();

                        // reset token of forgot pin
                        $this->basic_model->update_records('member', array('otp' => ''), $where = array('id' => $user_id));

                        $response = array('status' => true, 'success' => 'Pin reset successfully');
                    } else {
                        $response = array('status' => false, 'error' => system_msgs('verfiy_password_error'));
                    }
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

}
