<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_token_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function check_auth_token($token) {
        $this->db->select(['updated', 'memberId as adminId', 'token', 'ip_address']);
        $this->db->from('tbl_member_login');
        $this->db->where(['token' => $token]);

        $query = $this->db->get();

        return $query->row();
    }

    function update_token_time($token) {
        $where_al = array('token' => $token);
        return $this->basic_model->update_records('member_login', $columns = array('updated' => DATE_TIME), $where_al);
    }

}
