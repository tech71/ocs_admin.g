<?php


defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmSchedule extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmSchedule_model');
        $this->load->model('Dashboard_model');
        $this->loges->setModule(11);
    }


    public function schedules_calendar_data(){
      $reqData = request_handler();
      if (!empty($reqData->data)) {
          $response = $this->CrmSchedule_model->schedules_calendar_data($reqData);
          echo json_encode(array('status' => true, 'data' => $response));
      }
    }

    public function schedules_calendar_data_by_staffid(){
      $reqData = request_handler();
      if (!empty($reqData->data)) {
          $response = $this->CrmSchedule_model->schedules_calendar_data_by_staffid($reqData);
          echo json_encode(array('status' => true, 'data' => $response));
      }
    }


}
