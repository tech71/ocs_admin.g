<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Dashboard extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmParticipant_model');
        $this->load->model('Dashboard_model');

        $this->loges->setModule(11);
    }

    //Participant Intake (Admin)
    //Participant Status
    public function crm_participant_status() {
        $reqData = request_handler();      
        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole( $reqData->adminId);
        $role_array = array();
        foreach($role as $value){
          array_push($role_array,$value->permission);
        }        
        if (!empty($reqData->data)) {
            $post_data = $reqData->data;
            if ($post_data->view_type == 'year') {
                $where['YEAR(created)'] = date('Y');
                if(!empty($post_data->staffId)){
                  $where['assigned_to'] =$post_data->staffId;
                }
                else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
                {
                    $where['assigned_to'] =$reqData->adminId;  
                }
            } else if ($post_data->view_type == 'week') {               
                if(!empty($post_data->staffId)){
                    $where['assigned_to'] =$post_data->staffId;
                  }
                  else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
                  {
                      $where['assigned_to'] =$reqData->adminId;  
                  }
                 //$where1 = 'created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()';
                  $where1 = 'YEARWEEK(`created`, 1) = YEARWEEK(CURDATE(), 1)';
                  $this->db->where($where1);

            } else {
                if(!empty($post_data->staffId)){
                  $where['assigned_to'] =$post_data->staffId;
                }
                else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
                {
                    $where['assigned_to'] =$reqData->adminId;  
                }
                $where['MONTH(created)'] = date('m');
            }
            $where['status'] =1;
            $all_status = $this->basic_model->get_record_where('crm_participant', 'booking_status', $where);
            $success = 0; $processing=0; $rejected=0;
            $values = array();
            foreach($all_status as $status){
                $values = array(
                  'successful' =>   ($status->booking_status=='3')? ++$success : $success,
                  'processing' =>     ($status->booking_status=='4')? ++$processing : $processing,
                  'rejected' =>     ($status->booking_status=='5')? ++$rejected : $rejected
                );
            }
            $count = (!empty($all_status)) ? count($all_status) : 0;
            echo json_encode(array('crm_participant_count' => $count,  'grapheachdata'=>$values,    'status' => TRUE));
            exit();
        }
    }

	//Participant Vs Member (Not clear with Recruitment)
  public function crm_participant_member() {
      $reqData = request_handler();
      if (!empty($reqData->data)) {
        $post_data = $reqData->data;
        $count =  $this->Dashboard_model->crm_participant_member($post_data);
        echo json_encode(array( 'count'=>$count,    'status' => TRUE));
        exit();
      }
  }


  public function crm_participant_count() {
      $reqData = request_handler();
      require_once APPPATH . 'Classes/crm/CrmRole.php';
      $permission = new classRoles\Roles();
      $role = $permission->getRole( $reqData->adminId);
      $role_array = array();
      foreach($role as $value){
        array_push($role_array,$value->permission);
      } 
      if (!empty($reqData->data)) {
          $post_data = $reqData->data;
          if ($post_data->view_type == 'year') {
              $where['YEAR(created)'] = date('Y');
              if(!empty($post_data->staffId)){
                $where['assigned_to'] =$post_data->staffId;
              }
              else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
              {
                  $where['assigned_to'] =$reqData->adminId;  
              }
          } else if ($post_data->view_type == 'week') {
              if(!empty($post_data->staffId)){
                $where['assigned_to'] =$post_data->staffId;
              }
              else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
              {
                  $where['assigned_to'] =$reqData->adminId;  
              }
             //$where1 = 'created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()';
              $where1 = 'YEARWEEK(`created`, 1) = YEARWEEK(CURDATE(), 1)';
              $this->db->where($where1);
          } else {
              if(!empty($post_data->staffId)){
                $where['assigned_to'] =$post_data->staffId;
              }
              else if((in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)))
              {
                  $where['assigned_to'] =$reqData->adminId;  
              }
              $where['MONTH(created)'] = date('m');
          }

          $where['status'] =1;
          $rows = $this->basic_model->get_result('crm_participant', $where, array('id'));
          $all_total = $this->Dashboard_model->get_count_all_crmparticipant($reqData->adminId);
          $count = (!empty($rows)) ? count($rows) : 0;
          echo json_encode(array('crm_participant_count' => $count, 'status' => TRUE, 'all_crmparticipant_count' => $all_total));
          exit();
      }
  }

	public function latest_updates() {
        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $request = $reqData->data;
            $response = $this->Dashboard_model->get_all_crm_logs($request);
            echo json_encode($response);
        }
    }
	public function latest_stage_updates() {
        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $request = $reqData->data;
            $response = $this->Dashboard_model->latest_stage_updates($request);
            echo json_encode($response);
        }
    }
    public function crm_task_list(){
          $reqData = request_handler();
          if (!empty($reqData->data)) {
              $post_data = $reqData->data;
              $where = "task_status<>1";
              $rows = $this->Dashboard_model->get_crm_task_list($reqData->adminId);

              echo json_encode(array( 'status' => TRUE, 'data' => $rows));
              exit();
          }
        }
    public function crm_task_list_user(){
          $reqData = request_handler();
          if (!empty($reqData->data)) {
              $post_data = $reqData->data;
              $where = "task_status<>1";
              $rows = $this->Dashboard_model->get_crm_task_list_user($reqData->adminId);

              echo json_encode(array( 'status' => TRUE, 'data' => $rows));
              exit();
          }
        }
        public function crm_latest_action(){
            $reqData = request_handler();
            if (!empty($reqData->data)) {
                $post_data = $reqData->data;
                $where = "task_status<>1";
                $rows = $this->Dashboard_model->get_crm_latest_action($reqData->adminId);
                echo json_encode(array( 'status' => TRUE, 'data' => $rows));
                exit();
            }
          }
        public function crm_latest_action_admin(){
          $reqData = request_handler();
          if (!empty($reqData->data)) {
              $post_data = $reqData->data;
              $where = "task_status<>1";
              $rows = $this->Dashboard_model->get_crm_latest_action_admin($reqData->adminId);
              echo json_encode(array( 'status' => TRUE, 'data' => $rows));
              exit();
          }
        }




}
