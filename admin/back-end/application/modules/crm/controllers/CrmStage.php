<?php


defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmStage extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('sms');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmStage_model');
        $this->load->helper('pdf_helper');
        $this->load->model('Basic_model');
        $this->loges->setLogType('crm_prospective_participant');

    }

    public function receive_signed_docs(){
        $data = file_get_contents('php://input');
        // $this->basic_model->update_records('crm_participant',array('document_signed'=>'1'),array('id'=>4));
          var_dump($data);exit;
      echo "Run here";
    }




    public function list_intake_info() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_participant_stage_notes';
            $column="*";
            $orderby="id";
            $direction="desc";

            if($reqData['stage_id'] == '2' || $reqData['stage_id']=='3'){
              $stage_ids = ($reqData['stage_id'])==2?array(2,4,5,6,7):array(3,8,9,10,11);
              $this->db->select("*");
              $this->db->from("tbl_crm_participant_stage_notes");
              $this->db->where('crm_participant_id='.$reqData['crm_participant_id']);
              $this->db->where('status=1');
              $this->db->where_in('stage_id',$stage_ids);
              $response = $this->db->get()->result();
              $this->db->select("title,CONCAT('".base_url()."uploads/crmparticipant/".$reqData['crm_participant_id']."/',	filename) as file_url,'filename' as file_path");
              $this->db->from("tbl_crm_participant_docs");
              $this->db->where('crm_participant_id='.$reqData['crm_participant_id']);
              $this->db->where_in('stage_id',$stage_ids);
              $query = $this->db->get()->result();
              foreach($response as $key => $value){
                $response[$key]->docs  = $query;

              }
            }
            else {

            $condition=array("crm_participant_id"=>$reqData['crm_participant_id'],"stage_id"=>$reqData['stage_id'],'status'=>1);
            $response = array();
            $response = $this->Basic_model->get_record_where_orderby($table,$column,$condition,$orderby, $direction);
            $query = $this->db->query("select title,CONCAT('".base_url()."uploads/crmparticipant/".$reqData['crm_participant_id']."/',	filename) as file_url,'filename' as file_path  from tbl_crm_participant_docs where crm_participant_id=".$reqData['crm_participant_id']." AND stage_id=".$reqData['stage_id'])->result();
            $data= array();
            $d = array();
            foreach($response as $key => $value){
              $response[$key]->docs  = $query;

            }
          }

            echo json_encode($response);
       }
    }
    public function intake_docs_list() {
      $reqData = request_handler();
      if (!empty($reqData->data)) {
          $reqData = (array) json_decode($reqData->data);

          $result = array();
          if($reqData['stage_id'] == '2' || $reqData['stage_id']=='3'){
            $stage_ids = ($reqData['stage_id'])==2?array(2,4,5,6,7):array(3,8,9,10,11);
            $response = array();
            $this->db->select("CASE
            WHEN type=1 THEN 'NDIS Plan'
            WHEN type=2 THEN 'Behavioral Support Plan'
            WHEN type=5 THEN 'Other Relevant Plan'
            WHEN type=6 THEN 'Service Agreement Document'
            WHEN type=7 THEN 'Funding Consent'
            WHEN type=8 THEN 'Final Service Agreement Document'
            WHEN type=9 THEN 'Other Attachments'
            END as type,title,CONCAT('".base_url()."uploads/crmparticipant/".$reqData['crm_participant_id']."/',	filename) as file_url,tbl_crm_participant_docs.filename as file_path");
            $this->db->from("tbl_crm_participant_docs");
            $this->db->where('crm_participant_id='.$reqData['crm_participant_id']);
            $this->db->where_in('stage_id',$stage_ids);
            $query = $this->db->get()->result();
           if($query !=NULL)
           {
            $response["docs"]  = $query;
           }
          else{
            $response='';
          }


          }
          else {


          $response = array();

          $query = $this->db->query("select CASE
          WHEN type=1 THEN 'NDIS Plan'
          WHEN type=2 THEN 'Behavioral Support Plan'
          WHEN type=5 THEN 'Other Relevant Plan'
          WHEN type=6 THEN 'Service Agreement Document'
          WHEN type=7 THEN 'Funding Consent'
          WHEN type=8 THEN 'Final Service Agreement Document'
          WHEN type=9 THEN 'Other Attachments'
          END as type,title,CONCAT('".base_url()."uploads/crmparticipant/".$reqData['crm_participant_id']."/',filename) as file_url,tbl_crm_participant_docs.filename as file_path  from tbl_crm_participant_docs where crm_participant_id=".$reqData['crm_participant_id']." AND stage_id=".$reqData['stage_id'])->result();

          if($query !=NULL)
          {
           $response["docs"]  = $query;
          }
          else{
            $response='';
          }


        }
        array_push($result,$response);


          echo json_encode($result);
     }
  }
    public function notes_list_with_staff_member_and_stage_id() {
        $reqData = request_handler();

        if (!empty($reqData->data)) {
            $reqData = (array)$reqData->data;
            $response=array();
            $a = array();
            $a['notes'] = $this->CrmStage_model->get_note_record_where($reqData);
           // $a['docs'] = $this->CrmStage_model->get_docs_record_where($reqData);
            $response= array('allupdates' => $a, 'status' => true);
            echo json_encode($response);
       }
    }
    public function get_latest_stage() {
        $reqData = request_handler();
        $status = false;
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $stage_id = $this->db->query('Select stage_status,booking_status,(select created from tbl_logs where userId=tbl_crm_participant.id order by id desc limit 1) as created from tbl_crm_participant where id='.$reqData['crm_participant_id'])->row_array();
            $table='crm_stage';
            $column=array('name as latest_stage_name','level  as latest_stage');
            $where=array("id"=>$stage_id['stage_status']);
            $orderby="id";
            $direction="desc";
            $response = $this->Basic_model->get_record_where_orderby($table, $column, $where, $orderby, $direction);
            switch ($stage_id['booking_status']) {
              case '1':
                $response['booking_status'] = 'Pending';
                break;
              case '2':
                $response['booking_status'] = 'Parked';
                break;
              case '3':
                $response['booking_status'] = 'Successful';
                break;
              case '4':
                $response['booking_status'] = 'In progress';
                break;
              case '5':
                $response['booking_status'] = 'Rejected';
                break;
             }
             $response['latest_date'] =  date('d/m/Y - h:iA',strtotime($stage_id['created']));
            if($stage_id['stage_status'] == 11){
              $table2 = 'crm_participant_stage';
              $selectcolumn = 'status';
              $where2 = array('stage_id'=> 10,'status'=> 1);
              $result = $this->Basic_model->get_record_where($table2, $selectcolumn, $where2);
              if(!empty($result))
                $status = true;
            }
            if(!empty($response))
            $res= array( 'status' => true,'data'=>$response,'locked'=>$status);
            else
            $res= array( 'status' => false,'data'=>[], 'locked'=>$status);
            echo json_encode($res);
       }
    }
    public function get_stage_info_by_id() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_stage';
            $column="name";
            $where=array("id"=>$reqData['id'],"status"=>1);

            $response = $this->Basic_model->get_record_where($table, $column, $where);

            echo json_encode($response);
       }
    }
    public function get_all_stage() {
        $reqData = request_handler();
        $stage = array();
        if (!empty($reqData->data)) {
            $reqData = (array) json_decode($reqData->data);
            $table='crm_stage';
            $column="id as value,name as label";
            $where ="id!=2 AND id!=3";
            $response = $this->Basic_model->get_record_where($table, $column);
            $stage = $this->Basic_model->get_record_where($table, $column,$where);

            $response = array('status' => true, 'data' => $response,'stage' => $stage);
            echo json_encode($response);
       }
    }
        public function delete_intake(){
            $reqData = request_handler();
                if (!empty($reqData->data)) {

                    $reqData = (array)$reqData->data;
                    $table='crm_participant_stage_notes';
                    $where=array("id"=> $reqData['intake_id']);
                    $data=array("status"=>0);
                    $response = $this->Basic_model->update_records($table, $data, $where);
                    if($response)
                      $response = array('status' => true, 'msg' => "Note Deleted Successfully");
                    else
                        $response = array('status' => false, 'msg' =>'Note Not Deleted');
                      echo json_encode($response);
                }
        }
        public function add_stage_docs(){
                $reqData = request_handler();
                $this->loges->setCreatedBy($reqData->adminId);

                try {
                  var_dump($reqData);
                }
                catch (Exception $ex) {
                   $response = array('status' => false, 'error' => $ex->getMessage());
               }
          }


        public function create_intake_info(){
                $reqData = request_handler();
                $this->loges->setCreatedBy($reqData->adminId);

                try {

                    $requestData =  $reqData->data;
                    $intake_data = (array) json_decode($requestData);

                    global $globalparticipant;
                    require_once APPPATH . 'Classes/crm/CrmStage.php';
                    $intake = new CrmIntakeClass\CrmStage();



                    $response = $this->validate_intake_data($intake_data);


                    if (!empty($response['status'])) {
                        // set particicpant
                        $temp = $this->setIntakeValues($intake, $intake_data);
                        $crm_participant_id=$intake->getParticipantid();
                        if ($temp) {
                          // for crm_participant_stage
                          $stage_id = $intake->getSatgeId();
                          $intake->setParticipantStageId($intake->stageId);
                            // create Intake
                        $intakeID = $intake->AddIntakeInformation();



						//Logs Added
						$stageName = $intake->getStageName($stage_id);
                        $participantName = $intake->getParticipantName($crm_participant_id);
						$this->loges->setUserId($reqData->adminId);
                        $this->loges->setDescription(json_encode('Added New Note for Stage -'.$intake_data['stage_id'].'('.$stageName.') for participant ('.$participantName.')'));
                        $this->loges->setTitle('For Participant ('.$participantName.') added a New Note for Stage '.$intake_data['stage_id']);
                        $this->loges->createLog();






                        } else {
                            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                        }
                    } else {
                        $response = array('status' => false, 'error' => $response['error']);
                    }
                } catch (Exception $ex) {
                    $response = array('status' => false, 'error' => $ex->getMessage());
                }

                echo json_encode($response);

            }

            function setIntakeValues($intake, $intake_data) {
                try {

                    $intake->setNote($intake_data['notes']);
                    $intake->setParticipantId($intake_data['crm_participant_id']);
                    $intake->setStageId($intake_data['stage_id']);
                    $intake->setArchive(1);
                    $intake->setPortalAccess(1);

                } catch (Exception $e) {
                    return false;
                }
                return $intake;
            }
            function validate_intake_data($intake_data) {
                try {
                    $validation_rules = array(
                        array('field' => 'notes', 'label' => 'Note', 'rules' => 'required'),
                        array('field' => 'crm_participant_id', 'label' => 'Participant ID', 'rules' => 'required'),
                        array('field' => 'stage_id', 'label' => 'Stage ID', 'rules' => 'required')
                    );

                    $this->form_validation->set_data($intake_data);
                    $this->form_validation->set_rules($validation_rules);

                    if ($this->form_validation->run()) {
                        $return = array('status' => true);
                    } else {
                        $errors = $this->form_validation->error_array();
                        $return = array('status' => false, 'error' => implode(', ', $errors));
                    }
                } catch (Exception $e) {
                    $return = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                }

                return $return;
            }
            public function update_stage_status(){
                $reqData = request_handler();
                $adminId = $reqData->adminId;               
                if (!empty($reqData->data)) {
                    $reqData = (array) json_decode($reqData->data,true);
                    $response = $this->CrmStage_model->update_stage_status($reqData,$adminId);
                }
              echo json_encode($response);
            }
            public function get_stage_option() {
                $reqData = request_handler();
                if (!empty($reqData->data)) {
                    $reqData = (array) json_decode($reqData->data);
                    $response = $this->CrmStage_model->get_state_options($reqData);
                    $stage_tree = $this->CrmStage_model->get_stage_tree_data($reqData);
                    if(!empty($response)){
                      $res = array('status' => true,'data'=>$response,'stage'=>$stage_tree);
                    }
                    else {
                      $res =  array('status' => false);
                    }
                    echo json_encode($res);
               }
            }

            public function modified_status(){
                $reqData = request_handler();
                $adminId = $reqData->adminId;
                if (!empty($reqData->data)) {
                    // $reqData =  json_decode($reqData->data,true);
                    $response = $this->CrmStage_model->modified_status($reqData,$adminId);
                }
              echo json_encode($response);
            }

            public function renewed_status(){
                $reqData = request_handler();
                $adminId = $reqData->adminId;
                if (!empty($reqData->data)) {
                    // $reqData =  json_decode($reqData->data,true);
                    $response = $this->CrmStage_model->renewed_status($reqData,$adminId);
                }
              echo json_encode($response);
            }

            //Stage 3.1 Generate Document
             public function generate_document(){

               $reqData = request_handler();
               if (!empty($reqData->data)) {
                 $id = $reqData->data->participant_id;
                 $select_column = array('tbl_crm_participant.ndis_num','tbl_crm_participant.middlename','tbl_crm_participant.firstname', 'tbl_crm_participant.lastname','CONCAT(tbl_crm_participant.firstname," ",tbl_crm_participant.middlename," ",tbl_crm_participant.lastname) as fullname','tbl_crm_participant_email.email');
                 $this->db->select($select_column);
                 $this->db->from(TBL_PREFIX . 'crm_participant');
                 $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_email.primary_email = 1', 'left');
                 $this->db->where('tbl_crm_participant.id',$id);
                 $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
                 // var_dump($query);exit;
                 // $result = $this->basic_model->get_record_where('crm_participant',array('ndis_num'),array('id' => $id));
                 // $ndisNumber =  $result[0]->ndis_num;
                 // require_once APPPATH . 'Classes/crm/CrmParticipant.php';
                 // $pd = new CrmParticipantClass\CrmParticipant();
                 // $pd->setNdisNum($ndisNumber);
                 // require_once APPPATH . 'Classes/crm/DocSign.php';
                 //require_once APPPATH . 'modules/crm/controllers/CrmDocusign.php';
                 //$doc = new CrmDocusign();
                  
                 if($query->num_rows() > 0){

                 $data['crmPDetails'] = array();
                 // $data['crmPDetails'] = (array) $pd->getParticipantDetails();
                 $data['crmPDetails'] = $query->row_array();                
                 $filePath = FCPATH.'uploads/crmparticipant/'.$id.'/'.'Stage3'.'/';
                 $html = $this->load->view('crm/service_document', $data, true);
                 $convertedpdf = pdf_create($filePath, $html,'service_document_'.$data['crmPDetails']['ndis_num']);
                 if($convertedpdf == true){

                  $fileName='service_document_'.$data['crmPDetails']['ndis_num'].'.pdf';                
                  $documentPath=$filePath.$fileName;

                  // start code
                  if(file_exists($documentPath)){

                      $this->load->library('DocuSignEnvelope');
                      $signerDetails=array();
                      $signerDetails['name']=$data['crmPDetails']['fullname'];
                      $signerDetails['email_subject']='Participant Service Agreement Document';
                      $signerDetails['email']=$data['crmPDetails']['email'];
                                      
                      $web_hook_url=base_url()."crm/CrmStage/callback_participant_service_agreement";
                      $documents=array();
                      $documents['doc_id']='1';
                      $documents['doc_name']=$fileName;
                      $documents['doc_path']=$documentPath;
                      $documents['web_hook_url']=$web_hook_url;
                      
                      $position=array();
                      $position['position_x']=100;
                      $position['position_y']=100;
                      $position['document_id']=1;
                      $position['page_number']=1;
                      $position['recipient_id']=1;
    
                      $envlopDetails=array();
                      $envlopDetails['userdetails']=$signerDetails;
                      $envlopDetails['document']=$documents;
                      $envlopDetails['position']=$position;
                      
                      $resDocuSign=$this->docusignenvelope->CreateEnvelope($envlopDetails);
                      
                      if($resDocuSign['status']){
                          $envelopeId=$resDocuSign['response']['envelopeId'];                      
                          $res=$this->basic_model->insert_records('crm_participant_docs',array('crm_participant_id'=>$id,'stage_id'=>"8",'filename'=>$fileName,'document_type'=>1,'document_signed'=>0,'envelope_id'=>$envelopeId));
                          if($res>0){
                            $response =  array('status' => true);
                            echo json_encode($response);
                            exit();
                          }else{
                            $response =  array('status' => false);
                            echo json_encode($response);
                            exit();
                          }
                        }else{
                          $response =  array('status' => false);
                          echo json_encode($response);
                          exit();
                        }
                  }else{
                    $response =  array('status' => false);
                    echo json_encode($response);
                    exit();
                  }
                }else{
                  $response =  array('status' => false);
                  echo json_encode($response);
                  exit();
                }
              }else{
                $response =  array('status' => false);
                echo json_encode($response);
                exit();
              }
               }
             }

             

            public function send_sms_reminder(){
                $reqData = request_handler();
                if (!empty($reqData->data)) {
                  $id = $reqData->data->participant_id;
                  $msg = $reqData->data->msg;
                  $where = array('crm_participant_id'=>$id);
                  $sWhere = array('id'=>$id);
                  $data = $this->basic_model->get_record_where('crm_participant_email', 'email', $where);
                  $to_email = $data[0]->email;
                  $name = $this->basic_model->get_record_where('crm_participant', 'firstname', $sWhere);
                  $name = $name[0]->firstname;
                  $subject = "Reminder for Sign Document";
                  $body = "Dear $name, " .$msg;
                  send_mail($to_email, $subject, $body);
                  $res =  array('status' => true);
                  echo json_encode($res);
                }
            }
			public function saveNdisService(){
              $reqData= request_handler();            
              if (!empty($reqData)) {
                $data=$reqData->data;
                $response= $this->validateFundData((array)$data);
                
                if($response['status']){
                  $save_response = $this->CrmStage_model->saveNdisService($data);
                  if (!empty($save_response)) {
                        echo json_encode(array('status' => true, 'data' => $response));
                        $data=  $reqData->data;
                        $crm_participant_id= $data->crmParticipantId;
					            	$this->loges->setUserId($crm_participant_id);
                        $this->loges->setDescription(json_encode('Added service for participant '.$crm_participant_id));
                        $this->loges->setTitle('Added service for participant '.$crm_participant_id);
                        $this->loges->createLog();
                    }
                }else{
                  echo json_encode($response);
                  exit();
                }
              }else{
                  echo json_encode(array('status' => false, 'error' => 'error in saving'));
                  exit();
              }
              
          }
         
          public function get_ndis_services_categories(){
              $reqData          = request_handler();
              if (!empty($reqData)) {

                  $response      = $this->CrmStage_model->get_ndis_services_categories();
                  if (!empty($response)) {
                      echo json_encode(array(
                          'status' => true,
                          'data' => $response
                      ));
                  } else {
                      echo json_encode(array(
                          'status' => false,
                          'error' => 'Sorry no data found'
                      ));
                  }
              }
          }
          public function get_ndis_service_by_id(){
              $reqData= request_handler();
              if (!empty($reqData)) {
                 $data=json_decode($reqData->data);

                 
                
                 $fincanceListCatId=$data->id;
                 $support_item_number='';//$data->support_item_number;
                  $response = $this->CrmStage_model->get_ndis_service_by_id($fincanceListCatId,$support_item_number);
                  if (!empty($response)) {
                      echo json_encode(array(
                          'status' => true,
                          'data' => $response
                      ));
                  } else {
                      echo json_encode(array(
                          'status' => false,
                          'error' => 'Sorry no data found'
                      ));
                  }
              }
          }
          public function plan_list(){
            $reqData = request_handler();
            if (!empty($reqData->data)) {
              $data=$this->CrmStage_model->plan_list($reqData->data);
              $res =  array('status' => true, 'data' => $data);
              echo json_encode($res);
            }
        }
        public function resend_document(){

          $reqData = request_handler();
          if (!empty($reqData->data)) {
            $response =array('status' => false);
            $id = $reqData->data->participant_id;
            $stage_id = $reqData->data->stage_id;
            $select_column = array(
                'tbl_crm_participant.ndis_num',
                'tbl_crm_participant.middlename','tbl_crm_participant.firstname', 'tbl_crm_participant.lastname ',
                'tbl_crm_participant_email.email'
            );
            $this->db->select($select_column);
            $this->db->from(TBL_PREFIX . 'crm_participant');
            $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_email.primary_email = 1', 'left');
            $this->db->where('tbl_crm_participant.id',$id);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            // var_dump($query);exit;
            // $result = $this->basic_model->get_record_where('crm_participant',array('ndis_num'),array('id' => $id));
            // $ndisNumber =  $result[0]->ndis_num;
            // require_once APPPATH . 'Classes/crm/CrmParticipant.php';
            // $pd = new CrmParticipantClass\CrmParticipant();
            // $pd->setNdisNum($ndisNumber);
            // require_once APPPATH . 'Classes/crm/DocSign.php';
            require_once APPPATH . 'modules/crm/controllers/CrmDocusign.php';
            $doc = new CrmDocusign();
            $data['crmPDetails'] = array();
            // $data['crmPDetails'] = (array) $pd->getParticipantDetails();
            $data['crmPDetails'] = $query->row_array();

           // var_dump( $data['crmPDetails'][0]->email);
            $filePath = 'uploads/crmparticipant/'.$id.'/'.'Stage3'.'/';
            if(unlink($filePath.'service_document_'.$data['crmPDetails']['ndis_num'].'.pdf')){
              $html = $this->load->view('crm/service_document', $data, true);
              $convertedpdf = pdf_create($filePath, $html, 'service_document_'.$data['crmPDetails']['ndis_num']);
              if($convertedpdf == true){
                $filePath = $filePath.'service_document_'.$data['crmPDetails']['ndis_num'].'.pdf';
                $recipient_email = $data['crmPDetails']['email'];
                // $recipient_email = 'developer@yourdevelopmentteam.com.au';
                $name = $data['crmPDetails']['firstname'];
                $document_name = base_url().'uploads/crmparticipant/'.$id.'/'.'Stage3'.'/service_document_'.$data['crmPDetails']['ndis_num'].'.pdf';
                $envelopeId = $doc->CreateEnvelope($filePath,$recipient_email,$name,$document_name);
                $res= $this->basic_model->update_records('crm_participant_docs',array('filename'=>$filePath,'envelope_id'=>$envelopeId),array('crm_participant_id'=>$id,'stage_id'=>'8', 'document_signed' =>'0'));
                $response = array('status' => true);
              }
            }
            echo json_encode($response);
          }
        }
    function callback_participant_service_agreement(){          
          
          //file_put_contents (FCPATH.'crm_test_response.txt', 'call callback_participant_service_agreement');
          $data = file_get_contents('php://input');
          //$data = file_get_contents(FCPATH.'uploads/testcrmresponse1.txt');
          file_put_contents (FCPATH.'testcrmresponse.txt', $data);
          $xml = simplexml_load_string ($data, "SimpleXMLElement", LIBXML_PARSEHUGE);
          if(!$xml){
                  echo "null content response";
            exit();
          }
          if(empty($xml->EnvelopeStatus->EnvelopeID)){
                  echo "Envelope Id not exist";
            exit();
          }
          if(empty($xml->EnvelopeStatus->TimeGenerated)){
            echo "Generated time not exist";
            exit();
          }

              $envelope_id = (string)$xml->EnvelopeStatus->EnvelopeID;
              $time_generated = (string)$xml->EnvelopeStatus->TimeGenerated;
              $envelope_dir = FCPATH.'archieve';                        

              if(! is_dir($envelope_dir)) {mkdir ($envelope_dir, 0755);}
              $filename_xml = $envelope_dir . "service_document_" . str_replace (':' , '_' , $time_generated) . ".xml";
              $ok = file_put_contents ($filename_xml, $data);
              if ($ok === false) {
                // Here to handel if file not generated
                echo "Xml file content not available";
                exit();
              }

              $res=$this->get_participant_by_enevlopId($envelope_id);
              if($res['status']){

                $filename = '';
                if ((string)$xml->EnvelopeStatus->Status === "Completed") {
                    // Loop through the DocumentPDFs element, storing each document.
                    $stage_id=$res['stage_id'];
                    if($stage_id==8){
                      file_put_contents (FCPATH.'crm_test_response.txt', 'if stage 8');
                      
                      $participantId=$res['crm_participant_id'];
                      $crm_participant_id=$res['crm_participant_id'];
                      $id=$res['id'];
                      $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $res['filename']);
                      $filename_save = $filename.'_signed.pdf';
  
                      $Save_dir_path= FCPATH.'uploads/crmparticipant/'.$crm_participant_id.'/'.'Stage3'.'/';
                      
                      foreach ($xml->DocumentPDFs->DocumentPDF as $pdf) {
                          //$filename = $envelope_id.'_'.(string)$pdf->DocumentID.'.pdf';
                          $filename = (string)$pdf->Name;
                          
                          $filename_doc = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                          $filename_doc =$filename_doc.'_signed.pdf';

                          $full_filename = $Save_dir_path. $filename_doc;
                          file_put_contents($full_filename, base64_decode ( (string)$pdf->PDFBytes ));
                          if(isset($pdf->DocumentID) && (string)$pdf->DocumentID>0 && strtolower((string)$pdf->PDFBytes)==strtolower('CONTENT')){
                              $filename_save = $filename;
                          }
                      }

                      $update_data=[];
                      $update_data['signed_file_path'] = $filename_save;
                      $update_data['document_signed'] = 2;
                      $update_data['created'] = DATE_TIME;
                      $where_update =['envelope_id'=>$envelope_id,'id'=>$id];
                      $this->basic_model->update_records('crm_participant_docs', $update_data,$where_update);
                      
                      file_put_contents (FCPATH.'crm_test_response.txt', 'signed document saved successfully');
                      echo "envelope data saved in crm participant";
                      exit();
                    }elseif($stage_id==9){
                      file_put_contents (FCPATH.'crm_test_response.txt', 'if stage 9 updated');
                      // Ndis stage 3.2 hook
                      $participantId=$res['crm_participant_id'];
                      $crm_participant_id=$res['crm_participant_id'];

                      // Get ndis num

                        // Get ndis num
            
                        $select_column = array('tbl_crm_participant.ndis_num');
                        $this->db->select($select_column);
                        $this->db->from('tbl_crm_participant');
                        $this->db->where('tbl_crm_participant.id',$crm_participant_id);
                        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
                        if($query->num_rows() > 0){                        
                            $data_ndis=$query->row();
                            $ndis_num=$data_ndis->ndis_num;


                            $id=$res['id'];
                            $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $res['filename']);
                            $filename_save = $filename.'_signed.pdf';
        
                            $Save_dir_path= FCPATH.'uploads/crmparticipant/'.$crm_participant_id.'/'.'Stage3'.'/';
                            
                            foreach($xml->DocumentPDFs->DocumentPDF as $pdf){
                                //$filename = $envelope_id.'_'.(string)$pdf->DocumentID.'.pdf';
                                $filename = (string)$pdf->Name;
                                
                                $filename_doc = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                                $filename_doc =$filename_doc.'_signed.pdf';

                                $full_filename = $Save_dir_path. $filename_doc;
                                file_put_contents($full_filename, base64_decode ( (string)$pdf->PDFBytes ));
                                if(isset($pdf->DocumentID) && (string)$pdf->DocumentID>0 && strtolower((string)$pdf->PDFBytes)==strtolower('CONTENT')){
                                    $filename_save = $filename;
                                }
                            }
                            
                            $update_data=[];
                            $update_data['signed_file_path'] = $filename_save;
                            $update_data['document_signed'] = 2;
                            $update_data['created'] = DATE_TIME;
                            $where_update =['envelope_id'=>$envelope_id,'id'=>$id];
                            $this->basic_model->update_records('crm_participant_docs', $update_data,$where_update);
                            
                            file_put_contents (FCPATH.'crm_test_response.txt', 'NDIS signed document saved successfully');
                            echo "envelope data saved in crm participant";
                            exit();


                        }else{

                          file_put_contents (FCPATH.'crm_test_response.txt', 'Ndis no not exist');
                          echo "envelope data saved in crm participant";
                          exit();

                        }

                    }else{
                      file_put_contents (FCPATH.'crm_test_response.txt', 'Ndis stage not exist');
                      echo "Ndis stage not exist";
                      exit();
                    }
                   
                    
                }else{
                  file_put_contents (FCPATH.'crm_test_response.txt', 'crm envelope status is not completed');
                  echo "envelope status Completed not found in crm participant";
                  exit();
                }
                

              }else{                
                file_put_contents (FCPATH.'crm_test_response.txt', 'envelope id not found in crm contratct');
                echo "envelope data not found in crm participant";
                exit();           
              }

              // document_signed

    }

    function get_participant_by_enevlopId($envelope_id){
        //envelope_id
      $select_column = array('tbl_crm_participant_docs.id','tbl_crm_participant_docs.filename','tbl_crm_participant_docs.crm_participant_id','tbl_crm_participant_docs.stage_id');
      $this->db->select($select_column);
      $this->db->from(TBL_PREFIX.'crm_participant_docs');     
      $this->db->where('tbl_crm_participant_docs.envelope_id',$envelope_id);
      $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
      if($query->num_rows() > 0){
        $data=(array)$query->row();
        $data['status']=true;
        return $data;
        //return ['status'=>true,'id'=>$data->id,'filename'=>$data->filename];
      }
      return ['status'=>false];        
    }

    public function download_selected_file() {
      $reqData = request_handler();
      $responseAry = $reqData->data;
     
      if (!empty($responseAry)) {
          $this->load->library('zip');
          $this->zip->clear_data();

          $participant_id = $responseAry->participant_id;
          $stage_id = $responseAry->stage_id;
          $download_data = $this->get_attachment_details_by_ids($participant_id, $stage_id);
         
          if (!empty($download_data)){           
           
               $dir_path= FCPATH.'uploads/crmparticipant/'.$participant_id.'/'.'Stage3'.'/';
               $x = '';
               $file_count = 0;               
               $zip_name = time() . '_' . $participant_id . '.zip';
                  foreach ($download_data as $file) {
                      $file_path=$dir_path.$file->signed_file_path;
                      if(!empty($file->signed_file_path) && file_exists($file_path)){
                        $this->zip->read_file($file_path, FALSE);
                        $file_count = 1;
                      }else{
                        echo json_encode(array('status' => false, 'error' => 'Signed file not exist.'));
                        exit();
                      }
                      
                  }

                   $x = $this->zip->archive('archieve/' . $zip_name);
                  
                   if ($x && $file_count == 1) {
                    echo json_encode(array('status' => true, 'zip_name' => $zip_name));
                    exit();
                } else {
                    echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
                    exit();
                }               
          } else {
              echo json_encode(array('status' => false, 'error' => 'Signed file not exist.'));
              exit();
          }
          
          
      }
  }
  function get_attachment_details_by_ids($participant_id, $stage_id){
    $select_column = array('tbl_crm_participant_docs.signed_file_path');
    $this->db->select($select_column);
    $this->db->from(TBL_PREFIX.'crm_participant_docs');     
    $this->db->where('tbl_crm_participant_docs.crm_participant_id',$participant_id);
    $this->db->where('tbl_crm_participant_docs.document_signed=2');
    $this->db->where('tbl_crm_participant_docs.stage_id',$stage_id);
    $this->db->where('tbl_crm_participant_docs.archive=0');
    $this->db->order_by('tbl_crm_participant_docs.id', 'DESC');
    $this->db->limit(1);
    $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
   // last_query();
    return $query->result();    
}
public function validateFundData($fundData){

  try {
    
    $validation_rules = array(                 
          array('field' => 'totalNdisAmount','label' => 'total Ndis Amount','rules' => 'required|greater_than[0]'),
          array('field' => 'crmParticipantId','label' => 'Crm Participant','rules' => 'required'),                 
          array('field' => 'ndisServiceData', 'label' => 'Ndis service data ', 'rules' => 'callback_validate_NdisServiceData['.json_encode($fundData['ndisService']).']'),
          array('field' => 'ndisPlanStartDate','label' => 'support fund start date','rules' => 'required'),
          array('field' => 'ndisPlanEndDate', 'label' => 'support fund end date', 'rules' => 'callback_validate_NdisService_fund_StartEndDate['.json_encode($fundData).']'),
        );

    $this->form_validation->set_data($fundData);
    $this->form_validation->set_rules($validation_rules);

    if ($this->form_validation->run()) {
        $return = array(
            'status' => true,
            'error' => ''
        );
    } else {
        $errors = $this->form_validation->error_array();
        $return = array(
            'status' => false,
            'error' => implode(' ', $errors)
        );
    }
}
catch (Exception $e) {
    $return = array(
        'status' => false,
        'error' => system_msgs('something_went_wrong')
    );
}

return $return;

}
public function validate_NdisServiceData($sourceData, $fieldData){

       $data=json_decode($fieldData,true);
      

        if(!empty($data)){            
                foreach($data as $fund){
                    if(empty($fund['support_category_number'])){
                        $this->form_validation->set_message('validate_NdisServiceData','Please select support category type required');
                        return false;
                    }
                    if(empty($fund['support_item_number'])){
                        $this->form_validation->set_message('validate_NdisServiceData','Please select support list item required');
                        return false;
                    }
                    if(empty($fund['amount'])){
                        $this->form_validation->set_message('validate_NdisServiceData','support item amount required');
                        return false;
                    }
                    if(max($fund['amount'], 0)==0){
                      $this->form_validation->set_message('validate_NdisServiceData','support item positive amount required');
                      return false;
                    }
                    
                }
            }else{
            $this->form_validation->set_message('validate_NdisServiceData','Funding Support Items required');
            return false;
        }
        return true;


}
public function validate_NdisService_fund_StartEndDate($sourceData, $fieldData){
  $data=json_decode($fieldData,true);
  
  if(empty($data['ndisPlanEndDate'])){
    $this->form_validation->set_message('validate_NdisService_fund_StartEndDate','The support fund end date field is required');
    return false;
  }
  $strTime = strtotime(DateFormate($data['ndisPlanStartDate'], 'Y-m-d'));
  $endTime = strtotime(DateFormate($data['ndisPlanEndDate'], 'Y-m-d'));
  if($strTime>$endTime){
    $this->form_validation->set_message('validate_NdisService_fund_StartEndDate','Funding duration end date not greater end start date');
    return false;
  }
  return true;
}

}
