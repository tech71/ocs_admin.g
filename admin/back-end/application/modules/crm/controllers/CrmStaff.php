<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmStaff extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmStaff_model');
        $this->load->model('Basic_model');
        $this->load->model('Dashboard_model');
        $this->loges->setLogType('crm_staff');
    }
    public function list_user_management() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmStaff_model->user_management_list($reqData);
            echo json_encode($response);
        }
    }
    public function get_user_access_permission() {
      $reqData = request_handler();
       if (!empty($reqData->data)) {
          $staffId = $reqData->adminId;
          $where = array('id' => $staffId);
          $response = $this->CrmStaff_model->staff_details($staffId);
          if ($response['status']!=0 || array_key_exists("access_crm_admin", (array)$reqData->data->permission) || $staffId==1) {
            echo json_encode(array('status' => true));
          }
          else {
            echo json_encode(array('status' => false));
          }
       }
    }
    public function update_alloted_department()
   {
       $reqData = request_handler();
       $allotedData = $reqData->data;
       if(!empty($allotedData))
       {
           $update_dept = $this->CrmStaff_model->update_alloted_department($allotedData);
           $response = array('status' => true,'update_dept'=>$update_dept);
           echo json_encode($response);
       }
   }
     public function create_user(){
      $reqData = request_handler();
      $this->loges->setCreatedBy($reqData->adminId);
      require_once APPPATH . 'Classes/crm/CrmAdmin.php';
      $objAdmin = new AdminClass\Admin();
      if (!empty($reqData->data))
      {
          $data = (array) $reqData->data;
          $data['ocs_id'] = (!empty($data['ocs_id'])) ? $data['ocs_id'] : false;
          $this->form_validation->set_data($data);
          $validation_rules = array(
            array('field' => 'add_user_to_crm', 'label' => 'UserName', 'rules' => 'callback_check_user_already_exist[' . $data['ocs_id'] . ']')
          );
          // set rules form validation
          $this->form_validation->set_rules($validation_rules);
          if ($this->form_validation->run()) {

                $objAdmin->setRoles(array('id'=>'11','access'=>true));
                $objAdmin->setAdminid($data['ocs_id']);
                $checkExist = $this->basic_model->get_row('admin_role', array('adminId'), array('adminId' => $data['ocs_id'], 'roleId'=>'11'));
                if(!$checkExist){
                  $objAdmin->insertRoleToAdmin();
                }
                $objAdmin->insertStaffDetail();
                  // send welcome to admin
                  if(ENABLE_MAIL)
                  $objAdmin->send_welcome_mail();
                  $this->loges->setTitle('New User staff: ' . $objAdmin->getFirstname() . ' ' . $objAdmin->getLastname());

              $this->loges->setDescription(json_encode($reqData->data));
              $this->loges->createLog();
              $response = array('status' => true);
          } else {
              $errors = $this->form_validation->error_array();
              $response = array('status' => false, 'error' => implode(', ', $errors));
          }
          echo json_encode($response);
      }
    }


    public function get_staff_details(){
      $reqData = request_handler();
      if (!empty($reqData->data)) {
          $staffId = $reqData->data->id;
          $where = array('id' => $staffId);
          $response = $this->CrmStaff_model->staff_details($staffId);
          if (!empty($response)) {
            echo json_encode(array('status' => true, 'data' => $response));
          }
          else {
              echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
          }
      }
    }
    public function get_all_staffs() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmStaff_model->get_all_staffs($reqData);
            if (!empty($response)) {
                echo json_encode(array('status' => true, 'data' => $response));
              }
              else {
                  echo json_encode(array('status' => false, 'error' => 'Sorry no staff found'));
              }
        }
    }


    public function get_staff_disable(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $staffId = $reqData->data->id;
            $where = array('id' => $staffId);
            $response = $this->CrmStaff_model->get_staff_disable($staffId);
            if (!empty($response)) {
              echo json_encode(array('status' => true, 'data' => $response));
            }
            else {
                echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
      }

    public function get_all_users() {
        $post_data = $this->input->get('query');
        $rows = $this->CrmStaff_model->get_new_crm_staff_name($post_data);
        echo json_encode($rows);
    }

    public function get_staff_name() {
        $post_data = $this->input->get('query');
        $rows = $this->CrmStaff_model->get_staff_name($post_data);
        echo json_encode($rows);
    }

    // Check User Existed
    public function check_user_already_exist($name_ary,$ocs_id){
       $where = array('admin_id' => $ocs_id);
       $result = $this->basic_model->get_row('crm_staff', array('admin_id'), $where);
       if (!empty($result) && $result) {
           $this->form_validation->set_message('check_user_already_exist', 'User  Allready Exist');
           return false;
       }else {
         return true;
       }
    }

    public function add_update_disable_recruiter_staff()
   {
       $request = request_handler();
       $data = $request->data;
       $disable_recruiter_data = (array) $request->data;
       #pr($org_data);
       if(!empty($data))
       {
          $validation_rules = array(
               array('field' => 'disableAccount', 'label' => 'Disable Account', 'rules' => 'required'),
               array('field' => 'allocatedAccount', 'label' => 'Allocated Account', 'rules' => 'required'),
               // array('field' => 'account_allocated_staff_to', 'label' => 'Allocated Account To?', 'rules' => 'required'),
               array('field' => 'staff_disable_note', 'label' => 'Relavant Note', 'rules' => 'required'),
           );

          $this->form_validation->set_data($disable_recruiter_data);
          $this->form_validation->set_rules($validation_rules);

          if ($this->form_validation->run() == TRUE) {
               /*logs*/
               $this->loges->setUserId($request->adminId);

               if(isset($disable_recruiter_data['update_mode']) && $disable_recruiter_data['update_mode'] == 1){
                   $this->loges->setTitle("Update Disable Recruiter Staff: " . $disable_recruiter_data['staff_id']);
               }

               $this->loges->setDescription(json_encode($data));
               $this->loges->createLog();
               $assign = isset($disable_recruiter_data['account_allocated_staff_to'])?$disable_recruiter_data['account_allocated_staff_to']:'';

               require_once APPPATH . 'Classes/crm/DisableUser.php';
               $objOrg = new DisableUserClass\DisableUser();
               $objOrg->setCrmStaffId($disable_recruiter_data['staff_id']);
               $objOrg->setDisableAccount($disable_recruiter_data['disableAccount']);
               $objOrg->setAccountAllocated($disable_recruiter_data['allocatedAccount']);
               $objOrg->setAccountAllocatedTo($assign);
               $objOrg->setRelevantNote($disable_recruiter_data['staff_disable_note']);
               $objOrg->setStaffDetails($disable_recruiter_data['staff_details']);

               $disable_recruiter_exist_id = $objOrg->check_exist_disable_recruiter();

               if($disable_recruiter_exist_id){
                   $objOrg->setId($disable_recruiter_exist_id);
                   $objOrg->update_disable_recruiter();
                   $msg = 'CRM User Disable data  updated successfully.';
                   $return = array('status' => true,'msg'=>$msg);
               } else {
                   $disable_recruiter = $objOrg->add_disable_recruiter();
                   if($disable_recruiter){
                     $msg = 'CRM User Disabled successfully.';
                     $return = array('status' => true,'msg'=>$msg);
                   }
                   else{
                     $msg = 'There is no active user to allocate participant.';
                     $return = array('status' => false,'error'=>$msg);
                   }
               }
           } else {
               $errors = $this->form_validation->error_array();
               $return = array('status' => false, 'error' => implode(', ', $errors));
           }
           echo json_encode($return);
       }
   }
   public function enable_crm_user(){
    $request = request_handler();
    $data = $request->data;
    $disable_recruiter_data =  $request->data;
    if(!empty($data))
    {
        $response = $this->CrmStaff_model->enable_crm_user($disable_recruiter_data);
         if($response){

                $msg = 'CRM User Enabled Successfully.';


                $return = array('status' => true,'msg'=>$msg);

            }else{

                $return = array('status' => false, 'error' => implode(', ', $response['error']));
            }
        echo json_encode($return);
    }

   }
      public function assign_staff_member(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmStaff_model->assign_staff_member($reqData);
            if($response){
              $return = array('status' => true);
              } else {
                  $return = array('status' => false, 'error' => implode(', ', $response['error']));
              }
            echo json_encode($response);
        }
    }
  }
