<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmParticipant extends MX_Controller
{
    use formCustomValidation;
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        $this->load->model('CrmParticipant_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Basic_model');
        $this->loges->setLogType('crm_prospective_participant');        
    }

    public function list_prospective_participant()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        $adminId = $reqData->adminId;

        if (!empty($reqData->data)) {
            require_once APPPATH . 'Classes/crm/CrmRole.php';
            $permission = new classRoles\Roles();
            $role = $permission->getRole($adminId);

            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->prospective_participant_list($reqData, $adminId,$role);
            echo json_encode($response);
        }
    }
    public function get_prospective_participant_details()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;
            $where         = array(
                'participantId' => $participantId
            );
            $response      = $this->CrmParticipant_model->prospective_participant_details($participantId);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }


    public function get_prospective_participant_ability()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $participantId = $reqData->data->id;
            $where         = array(
                'participantId' => $participantId
            );
            $response      = $this->CrmParticipant_model->prospective_participant_ability($participantId);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }


    public function create_crm_participant()
    {
        $reqData   = request_handler(); 
        
        if(!empty($reqData->data)) {
            $participant_data=$reqData->data;
            
            require_once APPPATH . 'Classes/crm/CrmParticipant.php';
            $objparticipant = new CrmParticipantClass\CrmParticipant();
            $response = array();
            $participantRefer       = ((array) $participant_data->refererDetails);
            $participantAbility     = ((array) $participant_data->participantAbility);
            $participantShift       = ((array) $participant_data->participantShift);
            $participantDetails       = ((array) $participant_data->participantDetails);
            
            $participantDetailsData = array_merge($participantRefer,$participantAbility,$participantDetails,$participantShift);            
            $participant_response = $this->validate_participant_data($participantDetailsData);
           
            if (($participant_response['status']) ) {

                $response = $this->CrmParticipant_model->create_crm_participant($participant_data);               
                $roster_response=$this->create_roster($response,$participantShift);
                if($response>0 && $roster_response){
                    echo json_encode(array('status' => true));
                    exit();
                }else{
                    echo json_encode(array('status' => false, 'error' => 'something went wrong'));
                    exit();
                }
                
            }else{
                echo json_encode($participant_response);
                exit();
            }
        }else{
            echo json_encode(array('status' => false, 'error' => 'Sorry no data found'));
            exit();
        }
    }
    function tempdata_remove(){
        die;
        $action = $reqData->action;
        $participant_data = json_decode($reqData->data);        
        $this->loges->setCreatedBy($reqData->adminId);

        try {
                //  global $globalparticipant;
               require_once APPPATH . 'Classes/crm/CrmParticipant.php';
               $objparticipant = new CrmParticipantClass\CrmParticipant();
               $response = array();
               $refer       = ((array) $participant_data->refererDetails);
               $ability     = ((array) $participant_data->participantAbility);
               $shift       = ((array) $participant_data->participantShift);
               $participantDetails       = ((array) $participant_data->participantDetails);
               $participantDetailsData = array_merge($refer,$ability,$participantDetails,$shift);
               $participant_response = $this->validate_participant_data($participantDetailsData);
               if (($participant_response['status']) ) {
                // set particicpant
                $temp = $this->setParticipantvalues($objparticipant, $participant_data);
                if ($temp) {
                    $globalparticipant = $objparticipant;
                    // generate password
                    $objparticipant->genratePassword();
                    // password send to mail
                    //   $objparticipant->WelcomeMailParticipant();
                    // encrypt password
                    $objparticipant->encryptPassword();
                    //ndis no checking
					if(!$objparticipant->getParticipantid() && empty($participant_data->p_id))
                    {
						if ($objparticipant->checkNdisNumber()) {
							$response = array(
								'status' => false,
								'error' => system_msgs('ndis_exist')
							);
							echo json_encode($response);
							exit;
                        }
                        if ($objparticipant->checkEmailId()) {
							$response = array(
								'status' => false,
								'error' => system_msgs('email_id_exist')
							);
							echo json_encode($response);
							exit;
						}
                        if (!$this->CrmParticipant_model->assign_participant()) {
                            $response = array(
                                'status' => false,
                                'error' => system_msgs('no_staff_create')
                            );
                            echo json_encode($response);
                            exit;
                        }						
					 }
                    // create participant
                    $participant_id = $objparticipant->AddCrmParticipant($action,$reqData->adminId);
                    if (!$participant_id) {
                        $response = array(
                            'status' => false,
                            'error' => system_msgs('no_staff')
                        );
                        echo json_encode($response);
                        exit;
                    }
                     if(!$objparticipant->getParticipantid())
                    {
                      $shifts = $this->create_roster($participant_id,$shift);
                    }
                    if (!empty($_FILES['ndis']) ) {
                       $ndis_file = $this->upload_docs('ndis',$participant_id,1,'NDIS');

                       if(!$ndis_file){
                         echo json_encode(array(
                             'status' => false,
                             'error' => 'Error in uploading  ndis file.'
                         ));
                         exit();
                       }
                        /*logs*/
                        $this->loges->setCreatedBy($reqData->adminId);
                        $this->loges->setUserId($participant_id);
                        $this->loges->setDescription(json_encode($reqData));
                        $this->loges->setTitle('Uploaded  CRM Participant Ndis DOCS : ' . trim($objparticipant->getFirstname() . ' ' . $objparticipant->getLastname()));
                        $this->loges->createLog();

                   }
                   if (!empty($_FILES['hearing']) ) {
                      $hearing = $this->upload_docs('hearing',$participant_id,2,'Behavioural');
                      if(!$hearing){
                        echo json_encode(array(
                            'status' => false,
                            'error' => 'Error in uploading hearing file.'
                        ));
                        exit();
                      }

                        $this->loges->setCreatedBy($reqData->adminId);
                        $this->loges->setUserId($participant_id);
                        $this->loges->setDescription(json_encode($reqData));
                        $this->loges->setTitle('Uploaded  CRM Participant Behavioural DOCS : ' . trim($objparticipant->getFirstname() . ' ' . $objparticipant->getLastname()));
                        $this->loges->createLog();

                  }
                    $participant_ability                       = (array) $participant_data->participantAbility;
                    $participant_ability['crm_participant_id'] = $participant_id;
                    // for participantability
                    if (!empty($participant_ability)) {
                        $this->CrmParticipant_model->participant_ability_disability_crm_update($participant_ability,$reqData->adminId,$objparticipant->getParticipantid());
                    }
                    $participant_shift['crm_participant_id'] = $participant_id;
                    if(!$objparticipant->getParticipantid() && empty($participant_data->p_id))
                    {
                        $this->loges->setUserId($participant_id);
                        $this->loges->setDescription(json_encode($participant_data));
                        $this->loges->setTitle('Added new participant :' . trim($objparticipant->getFirstname() . ' ' . $objparticipant->getLastname()));
                        $this->loges->createLog();
                    }
                    // $assignTo=(array)$objparticipant->getAssignTo();

                    $response = array(
                        'status' => true
                    );

                } else {
                    $response = array(
                        'status' => false,
                        'error' => system_msgs('something_went_wrong')
                    );
                }
            } else {
              $response['error']    = $participant_response['error'];
                $response = array(
                    'status' => false,
                    'error' => $response['error']
                );
            }
        }
        catch (Exception $ex) {
            $response = array(
                'status' => false,
                'error' => $ex->getMessage()
            );
        }

        echo json_encode($response);
    }

    // set class properties values in Participant class
    function setParticipantvalues($objparticipant, $participant_data)
    {       

            $participant_data = (array) $participant_data;
        
            if(isset($participant_data['participant_id'])){
                $objparticipant->setParticipantid($participant_data['participant_id']);           
            }

            if(!empty($participant_data['p_id']))
                $objparticipant->setOldParticipantid($participant_data['p_id']);
            else
              $objparticipant->setOldParticipantid(0);
            
            print_r($participant_data);
            // Referer Details 
            $objparticipant->setReferralFirstName($participant_data['refererDetails']->first_name);
            $objparticipant->setReferralLastName($participant_data['refererDetails']->last_name);
            $objparticipant->setReferralOrg($participant_data['refererDetails']->organisation);
            $objparticipant->setReferralEmail($participant_data['refererDetails']->remail);
            $objparticipant->setReferralPhone($participant_data['refererDetails']->phone_number);
            $objparticipant->setReferralParticipantRelation($participant_data['refererDetails']->relation);
            // participantDetails

            $objparticipant->setFirstname($participant_data['participantDetails']->firstname);
			$objparticipant->setAddress($participant_data['participantDetails']->Address);
            $objparticipant->setLastname($participant_data['participantDetails']->lastname);
            $objparticipant->setPreferredname(!empty($participant_data['participantDetails']->preferredfirstname) ? $participant_data['participantDetails']->preferredfirstname : '');
            $dob = date('Y-m-d', strtotime($participant_data['participantDetails']->Dob));
            $objparticipant->setDob($dob);
            $objparticipant->setNdisNum(!empty($participant_data['participantDetails']->ndisno) ? $participant_data['participantDetails']->ndisno : '');
            $objparticipant->setLivingSituation($participant_data['participantDetails']->livingsituation);
            $objparticipant->setRelevantPlan($participant_data['participantDetails']->other_relevent_plans);
            $objparticipant->setBehavioural($participant_data['participantDetails']->current_behavioural);
            $objparticipant->setMaritalStatus($participant_data['participantDetails']->martialstatus);
            $objparticipant->setNdisPlan($participant_data['participantDetails']->plan_management);
            $objparticipant->setState($participant_data['participantDetails']->state);
            $objparticipant->setCity($participant_data['participantDetails']->city);
            $objparticipant->setPostcode($participant_data['participantDetails']->postcode);
            $objparticipant->setAboriginalTsi($participant_data['participantDetails']->aboriginal_tsi);
            $objparticipant->setProvidePlan($participant_data['participantDetails']->provide_plan);
            $objparticipant->setProvideEmail($participant_data['participantDetails']->provide_email);
            $objparticipant->setProvideState($participant_data['participantDetails']->provide_state);
            $objparticipant->setProvideAddress($participant_data['participantDetails']->provide_address);
            $objparticipant->setProvidePostcode($participant_data['participantDetails']->provide_postcode);
            $objparticipant->setCreated(DATE_TIME);
            $objparticipant->setStatus(1);
            $objparticipant->setGender($participant_data['participantDetails']->gender);
            // $objparticipant->setReferral(($participant_data['referral'] == 2) ? 0 : 1);
            //
            // if ($participant_data['referral'] == 1) {

            
            
            // }

            $objparticipant->setArchive(1);
            $objparticipant->setPortalAccess(1);
            $objparticipant->setParticipantEmail($participant_data['participantDetails']->email);
            $objparticipant->setParticipantPhone($participant_data['participantDetails']->phonenumber);
       
        
        return $objparticipant;
    }
    function check_empty_array($array,$msg){
      if (!empty($array)) {
        return true;
      }
      else{
        $this->form_validation->set_message('check_empty_array', $msg);
        return false;
      }
    }
    function validate_participant_data($participant_data)
    {

        try {
         
            $validation_rules = array(                  
                  // Reffer detail page                  
                  array('field' => 'first_name','label' => 'Referer First name','rules' => 'required'),
                  array('field' => 'last_name','label' => 'Referer Last name','rules' => 'required'),                 
                  array('field' => 'phone_number', 'label' => 'Referer phone number', 'rules' => 'required|callback_phone_number_check[phone_number,required,Please provide valid Referer phone number.]'),
                  array('field' => 'remail', 'label' => 'Referer Email', 'rules' => 'required|callback_valid_email_check[remail,required,Please provide valid email address]'),
                  array('field' => 'relation','label' => 'Referer Relation','rules' => 'required'),

                  // Participant ability
                  array('field' => 'communication','label' => 'Participant ability communication','rules' => 'required'),
                  array('field' => 'cognitive_level','label' => 'Participant ability cognitive level','rules' => 'required'),
                  array('field' => 'require_mobility_data','label' => 'Participant require mobility','rules' => 'callback_validate_mobility_data['.json_encode($participant_data['require_mobility']).']'),
                  array('field' => 'require_assistance_data','label' => 'Participant require mobility','rules' => 'callback_validate_Assistance_data['.json_encode($participant_data['require_assistance']).']'),
                  array('field' => 'language_interpreter','label' => 'Participant require mobility','rules' => 'callback_validate_language_interpreter_data['.json_encode($participant_data['languages_spoken']).']'),
                  array('field' => 'primary_fomal_diagnosis_desc','label' => 'Participant fomal diagnosis (primary) detail ','rules' => 'required'),
                  
                  // participantDetails
                  array('field' => 'ndisno','label' => 'Participant NDIS number','rules' => 'required|numeric|min_length[9]|max_length[9]'),
                  array('field' => 'firstname','label' => 'Participant first name','rules' => 'required'),
                  array('field' => 'lastname','label' => 'Participant last name','rules' => 'required'),
                  array('field' => 'email','label' => 'Participant email','rules' => 'required'),
                  array('field' => 'phonenumber','label' => 'Participant phone number','rules' => 'required'),
                  array('field' => 'Dob','label' => 'Participant Dob','rules' => 'required'),
                  array('field' => 'Address','label' => 'Participant street address','rules' => 'required'),
                  array('field' => 'state','label' => 'Participant state','rules' => 'required'),
                  array('field' => 'postcode','label' => 'Participant post code','rules' => 'required'),                  
                  array('field' => 'medicare','label' => 'Participant medicare','rules' => 'required'),
                  array('field' => 'crn','label' => 'Participant CRN ','rules' => 'required'),
                  array('field' => 'preferred_contact','label' => 'Participant preferred contact ','rules' => 'required'),                  
                  array('field' => 'maritalstatus','label' => 'Participant marital status ','rules' => 'required'),
                  array('field' => 'livingsituation','label' => 'Participant living situation ','rules' => 'required'),
                  array('field' => 'plan_management','label' => 'Participant plan management','rules' => 'callback_validate_plan_management_data['.json_encode($participant_data).']'),
                  array('field' => 'kin_details_data','label' => 'Participant kin details','rules' => 'callback_validate_participant_next_of_kin_data['.json_encode($participant_data).']'),
                  array('field' => 'booker_details_data','label' => 'Participant kin details','rules' => 'callback_validate_participant_booker_details_data['.json_encode($participant_data).']'),
                  
                  // plan_management is 3 then check (provide_plan,provide_email,provide_address)
                  
                );

            $this->form_validation->set_data($participant_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array(
                    'status' => true,
                    'error' => ''
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(' ', $errors)
                );
            }
        }
        catch (Exception $e) {
            $return = array(
                'status' => false,
                'error' => system_msgs('something_went_wrong')
            );
        }

        return $return;
    }
    // Validation method start
    public function validate_mobility_data($sourceData, $fieldData) {
        $data=json_decode($fieldData);        
        if(empty($data)){
            $this->form_validation->set_message('validate_mobility_data','Participant ability mobility required');
            return false;
        }
        return true;        
    }
    public function validate_Assistance_data($sourceData, $fieldData) {
        $data=json_decode($fieldData);        
        if(empty($data)){
            $this->form_validation->set_message('validate_Assistance_data','Participant ability assistance required');
            return false;
        }
        return true;
    }
    public function validate_language_interpreter_data($sourceData, $fieldData) {
        $data=json_decode($fieldData);
        if($sourceData){       
            if(empty($data)){
                $this->form_validation->set_message('validate_language_interpreter_data','Participant ability Language Spoken required');
                return false;
            }
        }
        return true;
    }
    public function validate_plan_management_data($sourceData, $fieldData) {
        $data=json_decode($fieldData,true);
        if($sourceData==3){
            if(empty($data['provide_plan'])){
                $this->form_validation->set_message('validate_plan_management_data','Please provide the name of the Participant plan Management Provider required');
                return false;
            }
            if(empty($data['provide_email'])){
                $this->form_validation->set_message('validate_plan_management_data','Please Provide the Plan Manager`s Email Address required');
                return false;
            }
            if(empty($data['provide_address'])){
                $this->form_validation->set_message('validate_plan_management_data','Please Provide the Participant Plan Managements Address required');
                return false;
            }
            if(empty($data['provide_postcode'])){
                $this->form_validation->set_message('validate_plan_management_data','Please Provide the Participant Plan Managements post code required');
                return false;
            }
            if(empty($data['provide_state'])){
                $this->form_validation->set_message('validate_plan_management_data','Please Provide the Participant Plan Managements state required');
                return false;
            }
        }
        return true;
    }
    
    public function validate_participant_next_of_kin_data($sourceData, $fieldData) {
        
        $data=json_decode($fieldData,true);
        if(!empty($data['kin_details'])){
              //  print_r($data['kin_details']);
                foreach($data['kin_details'] as $kin){
                    if(empty($kin['first_name'])){
                        $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant next in kin first name required');
                        return false;
                    }
                    if(empty($kin['last_name'])){
                        $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant next in kin last name required');
                        return false;
                    }
                    if(empty($kin['relation'])){
                        $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant next in kin relation required');
                        return false;
                    }
                    if(empty($kin['phone'])){
                        $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant next in kin phone required');
                        return false;
                    }
                    if(empty($kin['email'])){
                        $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant next in kin email required');
                        return false;
                    }
                }
            }else{
            $this->form_validation->set_message('validate_participant_next_of_kin_data','Participant detail Next of Kin required');
            return false;
        }
        return true;
    }
    
    public function validate_participant_booker_details_data($sourceData, $fieldData) {
        
        $data=json_decode($fieldData,true);
        if(!empty($data['booker_details'])){
              //  print_r($data['kin_details']);
                foreach($data['booker_details'] as $kin){
                    if(empty($kin['first_name'])){
                        $this->form_validation->set_message('validate_participant_booker_details_data','Participant booker detail first name required');
                        return false;
                    }
                    if(empty($kin['last_name'])){
                        $this->form_validation->set_message('validate_participant_booker_details_data','Participant booker detail last name required');
                        return false;
                    }
                    if(empty($kin['relation'])){
                        $this->form_validation->set_message('validate_participant_booker_details_data','Participant booker detail relation required');
                        return false;
                    }
                    if(empty($kin['phone'])){
                        $this->form_validation->set_message('validate_participant_booker_details_data','Participant booker detail phone required');
                        return false;
                    }
                    if(empty($kin['email'])){
                        $this->form_validation->set_message('validate_participant_booker_details_data','Participant booker detail email required');
                        return false;
                    }
                }
            }else{
            $this->form_validation->set_message('validate_participant_booker_details_data','Participant detail Next of Kin required');
            return false;
        }
        return true;
    }
    // Validation method end
    public function get_document_by_status()
    {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = (array) $reqData->data;
            $table    = 'crm_participant_docs';
            $column   = "*";
            $where    = array(
                "archive" => $reqData['crm_doc_status']
            );
            $response = $this->Basic_model->get_record_where($table, $column, $where);

            echo json_encode($response);
        }
    }





    public function update_crm_participant()
    {

        $request     = request_handlerFile();
        $member_data = (array) $request;
        //  var_dump($member_data);exit;
        $this->loges->setCreatedBy($request->adminId);

        if (!empty($member_data)) {
            // $participant_crm_data = (array) $reqData->data;

            $validation_rules = array(
                // array('field' => 'phones[]', 'label' => 'participant Phone', 'rules' => 'callback_check_participant_number'),
                // array('field' => 'emails[]', 'label' => 'participant Email', 'rules' => 'callback_check_participant_email_address'),
                // array('field' => 'address[]', 'label' => 'Address', 'rules' => 'callback_check_participant_address'),
                // array('field' => 'username', 'label' => 'username', 'rules' => 'callback_check_participant_username_already_exist[' . $participant_data['ocs_id'] . ']'),
                //array('field' => 'kin_detials[]', 'label' => 'Address', 'rules' => 'callback_check_kin_detials'),
                array(
                    'field' => 'firstname',
                    'label' => 'Firs name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'lastname',
                    'label' => 'Last name',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'gender',
                    'label' => 'gender',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'dob',
                    'label' => 'dob',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'prefer_contact',
                    'label' => 'Preferred contact type',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            // $this->loges->setUserId($participant_data['ocs_id']);
            // $this->loges->setTitle("Update profile: " . trim($participant_data['fullname']));
            $this->loges->setDescription(json_encode($member_data));
            $this->loges->createLog();

            if ($this->form_validation->run()) {
                if (!empty($_FILES)) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $member_data['crm_participant_id'];
                    $config['allowed_types']  = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';

                    $is_upload = do_muliple_upload($config);


                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {

                        for ($i = 0; $i < count($is_upload); $i++) {
                            // $image[$i] = $is_upload[$i]['upload_data']['file_name'];
                            $insert_ary = array(
                                'filename' => $is_upload[$i]['upload_data']['file_name'],
                                'crm_participant_id' => $member_data['crm_participant_id'],
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0
                            );
                            $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);

                        }

                    }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Please select a file to upload'
                    ));
                    exit();
                }
                $this->CrmParticipant_model->participant_crm_update($member_data);
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        echo json_encode($return);
    }

    public function crm_inteck_info()
    {
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);

        try {

            $requestData     = $reqData->data;
            $first_step_data = (array) json_decode($requestData);

            $intake_data = array_merge($first_step_data, (array) $requestData);


            global $globalparticipant;
            require_once APPPATH . 'Classes/crm/CrmStage.php';
            $intake = new CrmIntakeClass\CrmStage();



            $response = $this->validate_intake_data($intake_data);


            if (!empty($response['status'])) {
                // set particicpant
                $temp = $this->setIntakeValues($intake, $intake_data);
                if ($temp) {
                    // create Intake
                    $intakeID = $intake->AddIntakeInformation();





                } else {
                    $response = array(
                        'status' => false,
                        'error' => system_msgs('something_went_wrong')
                    );
                }
            } else {
                $response = array(
                    'status' => false,
                    'error' => $response['error']
                );
            }
        }
        catch (Exception $ex) {
            $response = array(
                'status' => false,
                'error' => $ex->getMessage()
            );
        }

        echo json_encode($response);

    }

    public function get_intake_percent()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->intake_percent($reqData->crm_participant_id);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    function setIntakeValues($intake, $intake_data)
    {
        try {

            $intake->setNote($intake_data['notes']);
            $intake->setParticipantId($intake_data['crm_participant_id']);
            $intake->setStage($intake_data['stage']);
            $intake->setArchive(1);
            $intake->setPortalAccess(1);

        }
        catch (Exception $e) {
            return false;
        }
        return $intake;
    }
    function validate_intake_data($intake_data)
    {
        try {
            $validation_rules = array(
                array(
                    'field' => 'notes',
                    'label' => 'Note',
                    'rules' => 'required'
                )

            );

            $this->form_validation->set_data($intake_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        catch (Exception $e) {
            $return = array(
                'status' => false,
                'error' => system_msgs('something_went_wrong')
            );
        }

        return $return;
    }
    public function uploading_crm_paricipant_docs()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        $crmParticipantId = $this->input->post("crmParticipantId");
        $category         = ($this->input->post("category")) ? $this->input->post("category") : 2;
        if (!empty($member_data)) {
            $validation_rules = array(

                array(
                    'field' => 'docsTitle',
                    'label' => 'Title',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                if (!empty($_FILES) && $_FILES['crmParticipantFiles']['error'] == 0) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $crmParticipantId;
                    $config['allowed_types']  = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';

                    $is_upload = do_upload($config);

                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {
                        if ($category == 1) {
                            $insert_ary = array(
                                'filename' => $is_upload['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0,
                                'type' => 1
                            );
                            $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                        } else {
                            $insert_ary        = array(
                                'filename' => $is_upload['upload_data']['file_name'],
                                'crm_participant_id' => $crmParticipantId,
                                'title' => $this->input->post('docsTitle'),
                                'created' => DATE_TIME,
                                'archive' => 0,
                                "stage_id" => $category

                            );
                            $rows              = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                            // for crm_participant_stage
                            $participant_stage = array(
                                "stage_id" => $category,
                                "crm_participant_id" => $crmParticipantId,
                                "crm_member_id" => $request->adminId,
                                "status" => 1
                            );
                            $this->Basic_model->insert_records('crm_participant_stage', $participant_stage, $multiple = false);
                        }
                        /*logs*/
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($crmParticipantId);
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added CRM Participant DOCS : ' . $crmParticipantId);
                        $this->loges->createLog();
                        if (!empty($rows)) {
                            echo json_encode(array(
                                'status' => true
                            ));
                            exit();
                        } else {
                            echo json_encode(array(
                                'status' => false
                            ));
                            exit();
                        }
                    }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Please select a file to upload'
                    ));
                    exit();
                }
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
    }
    public function update_crm_participant_ability_disability()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        $adminId=$request->adminId;
        // var_dump($member_data);exit;
        $crmParticipantId = $this->input->post("crm_participant_id");
        $type = $this->input->post("type");
        $title = ($type==3)?'Ability':'Disability';
        if (!empty($member_data)) {
            $validation_rules = array(
                array(
                    'field' => 'cognitive_level',
                    'label' => 'Cognitive level',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'communication',
                    'label' => 'Communication',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'primary_fomal_diagnosis_desc',
                    'label' => 'Primary Fomal Diagnosis Description',
                    'rules' => 'required'
                )
                // array('field' => 'require_assistance[]', 'label' => 'Require Assistance', 'rules' => 'required'),
                // array('field' => 'require_mobility[]', 'label' => 'Require Mobility', 'rules' => 'required'),
                // array('field' => 'linguistic_diverse', 'label' => 'Linguistic Diverse', 'rules' => 'required'),
                // array('field' => 'language_interpreter', 'label' => 'Language Interpreter', 'rules' => 'required'),
                // array('field' => 'languages_spoken[]', 'label' => 'Languages Spoken', 'rules' => 'required'),
                // array('field' => 'hearing_interpreter', 'label' => 'Hearing Interpreter', 'rules' => 'required'),
                // array('field' => 'secondary_fomal_diagnosis_desc', 'label' => 'Secondary Primary Fomal Diagnosis Description', 'rules' => 'required'),
                // array('field' => 'legal_issues', 'label' => 'Legal Issues', 'rules' => 'required'),

            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $data  = array();
                $image = array();
                if (!empty($_FILES)) {
                   $file = $this->upload_docs('crmParticipantFiles',$crmParticipantId,$type,$title);
                }


                $data     = implode(',', $data);
                $image1[] = $image;
                $status   = $this->CrmParticipant_model->participant_ability_disability_crm_update($member_data,$adminId,$crmParticipantId);

                /*logs*/
                // $this->loges->setCreatedBy($request->adminId);
                // $this->loges->setUserId($crmParticipantId);
                // $this->loges->setDescription(json_encode($request));
                // $this->loges->setTitle('Update CRM Participant Ability/Disability Info : ' . $crmParticipantId);
                // $this->loges->createLog();
                if ($status) {
                    echo json_encode(array(
                        'status' => true
                    ));
                    exit();
                } else {
                    echo json_encode(array(
                        'status' => false
                    ));
                    exit();
                }

                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
        $reqData = request_handler();
 //       $this->loges->setCreatedBy($reqData->adminId);

        if (!empty($reqData->data)) {
            $participant_ability_crm_data = (array) $reqData->data->data; //  $data['data'];
            $validation_rules             = array(
                array(
                    'field' => 'cognitive_level',
                    'label' => 'Cognitive level',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'communication',
                    'label' => 'Communication',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'primary_fomal_diagnosis_desc',
                    'label' => 'Primary Fomal Diagnosis Description',
                    'rules' => 'required'
                )
                // array('field' => 'require_assistance[]', 'label' => 'Require Assistance', 'rules' => 'required'),
                // array('field' => 'require_mobility[]', 'label' => 'Require Mobility', 'rules' => 'required'),
                // array('field' => 'linguistic_diverse', 'label' => 'Linguistic Diverse', 'rules' => 'required'),
                // array('field' => 'language_interpreter', 'label' => 'Language Interpreter', 'rules' => 'required'),
                // array('field' => 'languages_spoken[]', 'label' => 'Languages Spoken', 'rules' => 'required'),
                // array('field' => 'hearing_interpreter', 'label' => 'Hearing Interpreter', 'rules' => 'required'),
                // array('field' => 'secondary_fomal_diagnosis_desc', 'label' => 'Secondary Primary Fomal Diagnosis Description', 'rules' => 'required'),
                // array('field' => 'legal_issues', 'label' => 'Legal Issues', 'rules' => 'required'),

            );
            $this->form_validation->set_data($participant_ability_crm_data);
            $this->form_validation->set_rules($validation_rules);
            // $this->loges->setTitle('Participant Ability, Disability Updated');
            // $this->loges->setDescription(json_encode($participant_ability_crm_data));
            // $this->loges->createLog();
            if ($this->form_validation->run()) {
                $this->CrmParticipant_model->participant_ability_disability_crm_update($participant_ability_crm_data,$adminId,$crmParticipantId);
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode(', ', $errors)
                );
            }
        }
        echo json_encode($return);
    }



    public function change_participant_state()
    {
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);
        // var_dump($reqData->data);exit;
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            // var_dump($reqData->crm_participant_id);exit;
            $response = $this->CrmParticipant_model->set_participant_state($reqData->crm_participant_id, $reqData->state);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }
    public function uploading_crm_paricipant_stage_docs()
    {
        $request          = request_handlerFile();
        $member_data      = (array) $request;
        // var_dump($member_data);exit;
        $crmParticipantId = $this->input->post("crmParticipantId");
        $category         = ($this->input->post("category")) ? $this->input->post("category") : 2;
        if (!empty($member_data)) {
            $validation_rules = array(

                array(
                    'field' => 'docsTitle',
                    'label' => 'Title',
                    'rules' => 'required'
                )
            );

            $this->form_validation->set_data($member_data);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                if (!empty($_FILES)) {
                    $config['upload_path']    = CRM_UPLOAD_PATH;
                    $config['input_name']     = 'crmParticipantFiles';
                    $config['directory_name'] = $crmParticipantId;
                    $config['allowed_types']  = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';
                    $this->make_path(CRM_UPLOAD_PATH.$crmParticipantId);
                    $is_uploads = do_muliple_upload($config);

                    foreach($is_uploads as $is_upload){
                    if (isset($is_upload['error'])) {
                        echo json_encode(array(
                            'status' => false,
                            'error' => strip_tags($is_upload['error'])
                        ));
                        exit();
                    } else {
                      $rows ='';
                        $image = array();
                      $data = '';
                            $insert_ary = array(
                                    'filename' => $is_upload['upload_data']['file_name'],
                                    'crm_participant_id' => $crmParticipantId,
                                    'title' => $this->input->post('docsTitle'),
                                    'created' => DATE_TIME,
                                    'archive' => 0,
                                    "stage_id" => $category
                                );
                                $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                            $image1[] = $image;
                            $participant_stage = array(
                                "stage_id" => $category,
                                "crm_participant_id" => $crmParticipantId,
                                "crm_member_id" => $request->adminId,
                                "status" => 1
                            );

                        /*logs*/
                        $this->loges->setCreatedBy($request->adminId);
                        $this->loges->setUserId($crmParticipantId);
                        $this->loges->setDescription(json_encode($request));
                        $this->loges->setTitle('Added CRM Participant DOCS : ' . $crmParticipantId);
                        $this->loges->createLog();
                        if (!empty($rows)) {
                            echo json_encode(array(
                                'status' => true
                            ));
                            exit();
                        } else {
                            echo json_encode(array(
                                'status' => false
                            ));
                            exit();
                        }
                    }
                  }
                } else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Please Select file.'
                    ));
                    exit();
                }
                $return = array(
                    'status' => true
                );
            } else {
                $errors = $this->form_validation->error_array();
                $return = array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                );
            }
            echo json_encode($return);
        }
    }


    public function get_participant_stage_docs()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->participant_stage_docs($reqData->crm_participant_id);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }
    public function archive_partcipant_stage_docs()
   {
       $reqData = request_handler();
       $where   = array();
       $rows    = array();
       $file_count = 0;
       if (!empty($reqData->data)) {
           $reqData = json_decode($reqData->data);
           $stage_data = $reqData->ids;
           // $archive_data = array_merge($stage_data,$partipant_data);
           // var_dump($reqData);
           foreach ($stage_data as $file) {
                if (isset($file->is_active) && $file->is_active) {
                  $file_count = 1;
               $data  = array(
                   'archive' => 1
               );
               $where = array(
                   'id' => $file->id
               );
               $rows  = $this->Basic_model->update_records($file->table_name, $data, $where);
             }
           }


           if ( ($file_count == 1) ) {
               echo json_encode(array(
                   'status' => true,
                   'data' => $rows
               ));
           } else {
               echo json_encode(array(
                   'status' => false,
                   'error' => 'Please select atleast one file to continue.'
               ));
           }
       }
   }

   public function download_participant_stage_selected_file() {
       $request = request_handler();
       $responseAry = $request->data;
       $this->load->library('zip');
        $this->load->helper('download');
       $participant_id = $responseAry->participant_id;
       $download_data = $responseAry->downloadData;
       $this->zip->clear_data();
       $x = '';
       $file_count = 0;
       if (!empty($download_data)) {
             $zip_name = time() . '_' . $participant_id . '.zip';
           foreach ($download_data as $file) {
                if (isset($file->is_active) && $file->is_active) {

                   $file_path = CRM_UPLOAD_PATH . $participant_id . '/' . $file->file_path;
                   $this->zip->read_file($file_path, FALSE);
                   $file_count = 1;
               }
           }
         //  $this->zip->download($zip_name.'.zip');
           $x = $this->zip->archive('archieve/' . $zip_name);
       // var_dump($x); exit;
       }
       if ($file_count == 1) {
           echo json_encode(array('status' => true, 'zip_name' => $zip_name));
           exit();
       } else {
           echo json_encode(array('status' => false, 'error' => 'Please select atleast one file to continue.'));
           exit();
       }
   }

    public function download_participant_stage_docs()
    {
        $reqData   = request_handlerFile();
        $response  = array();
        $response1 = array();
        $file_name = array();
        if (!empty($reqData)) {
            $reqData = json_decode($reqData->rows);
            if($reqData->docType == 'stage'){
              $table   = TBL_PREFIX . 'crm_participant_docs';
              $column  = array(
                  $table . ".filename as file_path",
                  $table . ".crm_participant_id",
                  $table . ".filename as name"
              );
            }
            else{
              $table  = TBL_PREFIX . 'crm_participant_docs';
              $column  = array(
                  $table . ".filename as file_path",
                  $table . ".crm_participant_id",
                  $table . ".filename as name",
                  $table . ".type"
              );
            }

            $this->db->select($column);
            $this->db->from($table);
            $this->db->where_in('id', $reqData->ids);
            $response = $this->db->get()->result_array();

            // $results=array();
            // $contents = array();

            $fielDoc = 'uploads/crmparticipant/' . $response[0]['crm_participant_id'] . '/' . $response[0]['file_path'];
            // header("Content-Type: 'image/jpg'");
            //header('Content-Length: ' . filesize(FCPATH.$fielDoc));
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: 'PUT,GET,POST,DELETE,OPTIONS'");
            $allow_headers = "Referer,Accept,Origin,User-Agent,Content-Type";
            header("Access-Control-Allow-Headers: $allow_headers");


            header("Content-Type: application/octet-stream");
            header('Content-Disposition: attachment; filename=' . filesize(FCPATH . $fielDoc));
            $file     = fopen(FCPATH . $fielDoc, "r");
            $contents = fread($file, filesize(FCPATH . $fielDoc));
            // $results = 'data:image/jpg'  . ';base64,' . base64_encode($contents);
            // $results = utf8_encode($contents);
            //  $results = json_decode($contents);
            //$fileName = $response[0]['name'];.
            $response = array(
                'status' => true,
                'file_content' => base_url() . $fielDoc,
                'file_name' => $response[0]['name']
            );
            echo json_encode($response);

            exit;







            // $response['file_path'] = base_url().'/uploads/crmparticipant/'.$response[0]->crm_participant_id.'/'.$rows[0]->file_path;
            for ($i = 0; $i < count($response); $i++) {
                $doc = 'uploads/crmparticipant/' . $response[$i]['crm_participant_id'] . '/' . $response[$i]['file_path'];
                if (file_exists(FCPATH . $doc)) {

                    $response1[$i] = base_url() . $doc;
                    $file_name[$i] = $response[$i]['name'];
                    header("Content-Description: File Transfer");
                    // header("Content-Disposition: attachment; filename=$file_name[$i]");
                    header("Content-Disposition: attachment; filename=\"" . $file_name[$i] . "\"");
                    // header("Content-Type: 'application/octet-stream'");
                    // header("Content-Transfer-Encoding: binary");
                    // header('Expires: 0');
                    // header('Pragma: no-cache');
                    // header('Content-Length: ' . filesize(FCPATH.$doc));
                    // $contents[$i]=    file_get_contents(FCPATH.$doc);
                    //
                    // $contents[$i] = utf8_encode($contents[$i]);
                    // $results[$i] = json_decode($contents[$i]);
                } else {
                    $response1[$i] = '';
                    $file_name[$i] = '';
                }
            }

            $response = array(
                'status' => true,
                'file_content' => $response1,
                'file_name' => $file_name
            );

        } else {
            $response = array(
                'status' => false,
                'msg' => 'You Do not have permission'
            );

        }
        echo json_encode($response);
    }

    public function get_fms_cases()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = ($reqData->data);
            $response = $this->CrmParticipant_model->fms_cases($reqData->crm_participant_id);

            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    public function get_state()
    {
        request_handler();

        // get all state as value and lable form according to fron-end requirement
        $response = $this->basic_model->get_record_where('state', $column = array(
            'name as label',
            'id as value'
        ), $where = array(
            'archive' => 0
        ));

        echo json_encode(array(
            'status' => true,
            'data' => $response
        ));
    }
    public function update_crm_participant_ability_docs(){
      $reqData          = request_handlerFile();
      $member_data      =(array)$reqData;
     // var_dump($member_data);
      $crmParticipantId  = $reqData->crm_participant_id;
      $type = $reqData->type;
      $title = $reqData->title;
      if (!empty($member_data)) {
        $validation_rules = array(

            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_data($member_data);
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
                if (!empty($_FILES)) {
                    $file = $this->upload_docs('crmParticipantFiles',$crmParticipantId,$type,$title);
                    if(!$file){
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Error in uploading  ndis file.'
                    ));
                    exit();
                    }
                    else {
                    echo json_encode(array(
                        'status' => true,
                        'data' => 'File Uploaded successfully'
                    ));
                        /*logs*/
                        $this->loges->setCreatedBy($reqData->adminId);
                        $this->loges->setUserId($crmParticipantId);
                        $this->loges->setDescription(json_encode($reqData));
                        $this->loges->setTitle('Uploaded  CRM Participant Ability/Disability DOCS : ' . $crmParticipantId);
                        $this->loges->createLog();
                    exit();
                    }
                }
                else {
                    echo json_encode(array(
                        'status' => false,
                        'error' => 'Please select a file to upload'
                    ));
                    exit();
                }
            } else {
                $errors = $this->form_validation->error_array();
                echo json_encode(array(
                    'status' => false,
                    'error' => implode("\n", $errors)
                ));
                exit();
            }
        }
    }
    public function upload_docs($name,$participant_id,$type,$title){
      $config['upload_path']    = CRM_UPLOAD_PATH;
      $config['input_name']     = $name;
      $config['directory_name'] = $participant_id;
      $config['allowed_types']  = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';
      $this->make_path(CRM_UPLOAD_PATH.$participant_id);
      $is_upload = do_muliple_upload($config);
      if (isset($is_upload['error'])) {
          echo json_encode(array(
              'status' => false,
              'error' => strip_tags($is_upload['error'])
          ));
          exit();
      } else {
              $rows ='';
              // $image[$i] = $is_upload[$i]['upload_data']['file_name'];
              for ($i = 0; $i < count($is_upload); $i++) {
              $insert_ary = array(
                  'filename' => $is_upload[$i]['upload_data']['file_name'],
                  'crm_participant_id' => $participant_id,
                  'created' => DATE_TIME,
                  'type' =>   $type,
                  'archive' => 0,
                  'title' => $title
              );
              // $where=array('crm_participant_id'=>$participant_id,'type'=>$type);
              // $check_participant_doc       = $this->Basic_model->get_record_where('crm_participant_docs','id', $where);
              //
              //
              //
              //   if($check_participant_doc)
              //   {
              //       $rows       = $this->Basic_model->update_records('crm_participant_docs', $insert_ary, $where);
              //   }
              //   else{
                    $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
              // }

            }
            return !empty($rows)?true:false;
      }
    }
    public function make_path($path){
    	$dir = pathinfo($path , PATHINFO_DIRNAME);
      if( is_dir($dir) ){
    		return true;
    	}else{
    		if( $this->make_path($dir) ){
    			if( mkdir($dir) ){
    				chmod($dir , 0777);
    				return true;
    			}
    		}
    	}
      return false;
    }

    public function get_participant_docs()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->participant_docs($reqData->crm_participant_id);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response['data'],
                    'docs' =>$response['docs']
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }
    public function update_assignee(){
          $this->load->model('CrmParticipant_model');
          $reqData = request_handler();
            if (!empty($reqData->data)) {
              $reqData  = ($reqData->data);
              $data = array('assigned_to'=>$reqData->user->value);
              $where = array('id'=>$reqData->participant_id);
              $response = $this->Basic_model->update_records('crm_participant', $data, $where);

              $data_task = array('assign_to'=>$reqData->user->value);
              $where_task = array('crm_participant_id'=>$reqData->participant_id);
              $response1 = $this->Basic_model->update_records('crm_participant_schedule_task', $data_task, $where_task);

              if(!empty($response)){
                echo json_encode(array('status' => true));
              }
              else{
              echo json_encode(array('status' => false,'error'=>'Please choose different user'));
              }
            }
        }

        public function create_shift() {
            $reqData = request_handler();
            $this->loges->setCreatedBy($reqData->adminId);

            $shift_data = (array) $reqData->data;
            $reqestData = $reqData->data;
    //        print_r($shift_data);

            $validation_rules = array(
                array('field' => 'participant_member_ary', 'label' => 'Participant', 'rules' => 'callback_check_selected_val[' . json_encode($shift_data) . ']'),
                array('field' => 'completeAddress[]', 'label' => 'Address', 'rules' => 'callback_check_shift_address|callback_postal_code_check[postal]'),
                array('field' => 'shift_requirement', 'label' => 'Participant', 'rules' => 'callback_check_shift_requirement[' . json_encode($shift_data) . ']'),
                array('field' => 'caller_name', 'label' => 'booker firstname', 'rules' => 'required'),
                array('field' => 'caller_lastname', 'label' => 'booker lastname', 'rules' => 'required'),
                array('field' => 'caller_phone', 'label' => 'booker phone', 'rules' => 'required|callback_phone_number_check[caller_phone,required,Booking Details contact should be enter valid phone number.]'),
                array('field' => 'caller_email', 'label' => 'booker name', 'rules' => 'required|valid_email'),
                array('field' => 'start_time', 'label' => 'start date time', 'rules' => 'callback_check_shift_calendar_date_time[' . json_encode($shift_data) . ']'),
                array('field' => 'end_time', 'label' => 'end date time', 'rules' => 'required'),
                array('field' => 'booking_method', 'label' => 'booking for', 'rules' => 'required'),
                array('field' => 'confirm_with_f_name', 'label' => 'confimation firstname', 'rules' => 'required'),
                array('field' => 'confirm_with_l_name', 'label' => 'confimation lastname', 'rules' => 'required'),
                array('field' => 'confirm_with_email', 'label' => 'confimation email', 'rules' => 'required|valid_email'),
                array('field' => 'confirm_with_mobile', 'label' => 'confimation mobile number', 'rules' => 'required|callback_phone_number_check[confirm_with_mobile,required,Confimation Details contact should be enter valid phone number.]'),
                array('field' => 'push_to_app', 'label' => 'push on app', 'rules' => 'required'),
                array('field' => 'autofill_shift', 'label' => 'auto fill shift', 'rules' => 'required'),
            );

            $this->form_validation->set_data($shift_data);
            $this->form_validation->set_rules($validation_rules);


            if ($this->form_validation->run()) {

                require_once APPPATH . 'Classes/crm/Shift.php';
                $objShift = new ShiftClass\Shift();

                $objShift->setBookedBy($reqestData->booked_by);
                $objShift->setShiftDate($reqestData->start_time);
                $objShift->setStartTime($reqestData->start_time);
                $objShift->setEndTime($reqestData->end_time);

                $check_shift_exist = array();
                if ($reqestData->booked_by == 2) {
                    $objShift->setShiftParticipant($reqestData->participant_member_ary[0]->name->value);
                    $check_shift_exist = $objShift->check_shift_exist();

                    if (!empty($check_shift_exist)) {
                        $shift_exist = array('status' => false, 'error' => 'Shift is already exist.');
                        echo json_encode($shift_exist);
                        exit();
                    }
                } elseif ($reqestData->booked_by == 2) {
                    #$objShift->setShiftSite($reqestData->site_lookup_ary[0]->name->value);
                }

                $result = $objShift->create_shift($reqData);

                if (!empty($result['shiftId'])) {
                    $objShift->shiftCreateMail();
                    // create log
                    $this->loges->setTitle('New shift created : Shift Id ' . $result['shiftId']);
                    $this->loges->setUserId($result['shiftId']);
                    $this->loges->setDescription(json_encode($reqestData));
                    $this->loges->createLog();
                    $redirect = 1;
                    $msg = '';
                    if ($reqData->data->autofill_shift == 1 && !$result['allocation_res']) {
                        $msg = 'Autofill Shift Member not found.';
                    } else if ($reqData->data->autofill_shift == 1 && $result['allocation_res']) {
                        $redirect = 2;
                    } else if (isset($reqData->data->allocate_pre_member) && !empty($reqData->data->allocate_pre_member)) {
                        $redirect = 2;
                    }

                    $return = array('status' => true, 'shift_id' => $result['shiftId'], 'redirect_type' => $redirect, 'msg' => $msg);
                } else {
                    $return = array('status' => false);
                }
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
            exit();
        }

        public function check_selected_val($name_ary, $param_type) {
            $return = false;

            $param_type = json_decode($param_type);
            // $booked_by = $param_type->booked_by;
            // if ($booked_by == 1) {
            //     $message = 'Please select Site Look-up.';
            //     if (isset($param_type->site_lookup_ary[0]->name->value)) {
            //         $return = true;
            //     }
            // } else if ($booked_by == 2) {
            //     $message = 'Please select Participant Name.';
            //     if (isset($param_type->participant_member_ary[0]->name->value)) {
            //         $return = true;
            //     }
            // } else {
            //     $message = '';
            //     $return = true;
            // }

            $this->form_validation->set_message('check_selected_val', $message);
            return $return;
        }

        public function check_shift_address($address, $param_type) {
            if (!empty($address)) {
                if (empty($address->address)) {
                    $this->form_validation->set_message('check_shift_address', 'Street can not be empty');
                    return false;
                } elseif (empty((array) $address->suburb)) {
                    $this->form_validation->set_message('check_shift_address', 'Suburb can not be empty');
                    return false;
                } elseif (empty($address->state)) {
                    $this->form_validation->set_message('check_shift_address', 'State can not be empty');
                    return false;
                } elseif (empty($address->postal)) {
                    $this->form_validation->set_message('check_shift_address', 'Postal can not be empty');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_address', 'Address can not empty');
                return false;
            }
        }

        public function check_shift_requirement($shift_requirement, $param_type) {
            $param_type = json_decode($param_type);
            $status = false;

            if (!empty($param_type->shift_requirement)) {
                foreach ($param_type->shift_requirement as $val) {
                    if (!empty($val->checked) || !empty($val->active)) {
                        $status = true;
                    }
                }
            }
            if (!$status) {
                $this->form_validation->set_message('check_shift_requirement', 'Please chosse at least one shift requirement');
                return false;
            }
        }

        function check_shift_calendar_date_time($start_time, $shiftData) {

            $shiftData = json_decode($shiftData);

            if (!empty($shiftData)) {

                /* time calculation */
                $strTime = strtotime(DateFormate($shiftData->start_time, 'Y-m-d H:i:s'));
                $endTime = strtotime(DateFormate($shiftData->end_time, 'Y-m-d H:i:s'));
                $next_day_time = strtotime('+24 hours', $strTime);

                $strTimeCurrunt = strtotime(DATE_TIME);
                if (!empty($strTime)) {
                    if ($strTime > $strTimeCurrunt) {

                    } else {
                        $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start time can not less then current time');
                        return false;
                    }
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift start date time required can not empty');
                    return false;
                }


                if (!empty($endTime)) {
                    if ($endTime < $next_day_time) {

                    } else {
                        $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not greater then 24 hours');
                        return false;
                    }
                } else {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end date time required can not empty');
                    return false;
                }

                if ($strTime > $endTime) {
                    $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift end time not less then start time');
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_shift_calendar_date_time', 'Shift data required can not empty');
                return false;
            }
            return true;
        }

        public function get_participant_name() {
            $reqData = request_handler();
            $reqData->data = json_decode($reqData->data);
            $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
            $rows = $this->CrmParticipant_model->get_participant_name($post_data);
            echo json_encode($rows);
        }

        public function get_site_name() {
            $reqData = request_handler();
            $reqData->data = json_decode($reqData->data);
            $post_data = isset($reqData->data->query) ? $reqData->data->query : '';
            $rows = $this->CrmParticipant_model->get_site_name($post_data);
            echo json_encode($rows);
        }

        public function cancel_shift() {
            $reqData = request_handler();
            $this->loges->setCreatedBy($reqData->adminId);

            if (!empty($reqData->data)) {
                $reqData = $reqData->data;

                $this->form_validation->set_data((array) $reqData);

                $validation_rules = array(
                    array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                    array('field' => 'cancel_method', 'label' => 'cancel method', 'rules' => 'required'),
                    array('field' => 'cancel_type', 'label' => 'who cancel', 'rules' => 'required'),
                    array('field' => 'reason', 'label' => 'reason', 'rules' => 'required'),
                );

                // set rules form validation
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {

                    $return = $this->CrmParticipant_model->cancel_shift($reqData);
                } else {
                    $errors = $this->form_validation->error_array();
                    $return = array('status' => false, 'error' => implode(', ', $errors));
                }

                echo json_encode($return);
            }
        }

        public function get_shift_notes() {
            $reqData = request_handler();

            if (!empty($reqData->data)) {
                $reqData = $reqData->data;

                $result = $this->basic_model->get_record_where('shift_notes', $column = array('id', 'adminId', 'title', 'notes', "DATE_FORMAT(created,'%d/%m/%Y') as created"), $where = array('shiftId' => $reqData->shiftId, 'archive' => 0));

                echo json_encode(array('status' => true, 'data' => $result));
            }
        }

        public function create_shift_notes() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $reqData = $reqestData->data;

                $this->form_validation->set_data((array) $reqData);

                $validation_rules = array(
                    array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                    array('field' => 'notes', 'label' => 'notes', 'rules' => 'required'),
                    array('field' => 'title', 'label' => 'title', 'rules' => 'required'),
                );

                // set rules form validation
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {
                    $this->loges->setUserId($reqData->shiftId);
                    $this->loges->setDescription(json_encode($reqData));

                    $data = array('title' => $reqData->title, 'notes' => $reqData->notes, 'shiftId' => $reqData->shiftId, 'adminId' => $reqestData->adminId);

                    if (!empty($reqData->id)) {
                        // check permission
                        check_permission($reqestData->adminId, 'update_schedule');

                        $this->loges->setTitle('Update notes: Shift Id ' . $reqData->shiftId);
                        $this->basic_model->update_records('shift_notes', $data, $where = array('id' => $reqData->id));
                    } else {
                        // check permission
                        check_permission($reqestData->adminId, 'create_schedule');

                        $this->loges->setTitle('Added new notes: Shift Id ' . $reqData->shiftId);
                        $data['created'] = DATE_TIME;
                        $this->basic_model->insert_records('shift_notes', $data);
                    }

                    $this->loges->createLog();
                    $response = array('status' => true);
                } else {
                    $errors = $this->form_validation->error_array();
                    $response = array('status' => false, 'error' => implode(', ', $errors));
                }

                echo json_encode($response);
            }
        }

        public function update_shift_date_time() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $reqData = $reqestData->data;

                $this->form_validation->set_data((array) $reqData);

                $validation_rules = array(
                    array('field' => 'shiftId', 'label' => 'shift Id', 'rules' => 'required'),
                    array('field' => 'start_time', 'label' => 'start date time', 'rules' => 'required'),
                    array('field' => 'end_time', 'label' => 'end date time', 'rules' => 'required'),
                );

                // set rules form validation
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {
                    $this->loges->setUserId($reqData->uId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Updated shift date time: Shift Id ' . $reqData->shiftId);
                    $this->loges->createLog();

                    $shfit_date = date('Y-m-d H:i:s', strtotime($reqData->start_time));

                    $start_time = DateFormate($reqData->start_time, 'Y-m-d H:i:s');
                    $end_time = DateFormate($reqData->end_time, 'Y-m-d H:i:s');

                    $where = array('id' => $reqData->shiftId);
                    $data = array('shift_date' => $shfit_date, 'start_time' => $start_time, 'end_time' => $end_time);
                    $this->basic_model->update_records('crm_participant_shifts', $data, $where);
                    $response = array('status' => true);
                } else {
                    $errors = $this->form_validation->error_array();
                    $response = array('status' => false, 'error' => implode(', ', $errors));
                }

                echo json_encode($response);
            }
        }

        public function update_shift_address() {
            $reqestData = request_handler();

            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $reqData = $reqestData->data;
                $location = array();

                $this->form_validation->set_data((array) $reqData);

                $validation_rules = array(
                    array('field' => 'location[]', 'label' => 'Address', 'rules' => 'callback_check_shift_address|callback_postal_code_check[postal]'),
                );

                // set rules form validation
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {
                    if ($reqData->location) {
                        foreach ($reqData->location as $key => $val) {

                            $val->suburb = $val->suburb->value;
                            $state = getStateById($val->state);
                            $adds = $val->address . ' ' . $val->suburb . ' ' . $state;
                            $lat_long = getLatLong($adds);

                            $location[$key] = (array) $val;

                            if (!empty($lat_long)) {
                                $location[$key] = array_merge($location[$key], $lat_long);
                            }

                            unset($location[$key]['site']);
                            $location[$key]['shiftId'] = $reqData->shiftId;
                        }


                        $this->loges->setUserId($reqData->shiftId);
                        $this->loges->setDescription(json_encode($reqData));
                        $this->loges->setTitle('Update address : Shift Id ' . $reqData->shiftId);
                        $this->loges->createLog();
                        $this->basic_model->delete_records('shift_location', $where = array('shiftId' => $reqData->shiftId));
                        $this->basic_model->insert_records('shift_location', $location, $multiple = true);
                    }

                    $response = array('status' => true);
                } else {
                    $errors = $this->form_validation->error_array();
                    $response = array('status' => false, 'error' => implode(', ', $errors));
                }

                echo json_encode($response);
            }
        }

        public function update_shift_requirement() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $reqData = $reqestData->data;
                $reqData->shift_requirement = $reqData->requirement;

                $this->form_validation->set_data((array) $reqData);

                $validation_rules = array(
                    array('field' => 'requirement', 'label' => 'Participant', 'rules' => 'callback_check_shift_requirement[' . json_encode($reqData) . ']')
                );

                // set rules form validation
                $this->form_validation->set_rules($validation_rules);

                if ($this->form_validation->run()) {
                    $requirement = $reqData->requirement;

                    $updated_requirement = array();

                    $this->basic_model->delete_records('shift_requirements', $where = array('shiftId' => $reqData->shiftId));

                    if (!empty($requirement)) {
                        foreach ($requirement as $key => $val) {
                            if ($val->active) {
                                $updated_requirement[$key]['shiftId'] = $reqData->shiftId;
                                $updated_requirement[$key]['requirementId'] = $val->id;
                            }
                        }

                        $this->loges->setUserId($reqData->shiftId);
                        $this->loges->setDescription(json_encode($reqData));
                        $this->loges->setTitle('Update shift requirement : Shift Id ' . $reqData->shiftId);
                        $this->loges->createLog();

                        $this->basic_model->insert_records('shift_requirements', $updated_requirement, $multiple = true);

                        $response = array('status' => true);
                    }
                } else {
                    $errors = $this->form_validation->error_array();
                    $response = array('status' => false, 'error' => implode(', ', $errors));
                }

                echo json_encode($response);
            }
        }

        function update_preffered_member() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $reqData = $reqestData->data;
                $preffer_member = array();

                $this->basic_model->delete_records('shift_preferred_member', $where = array('shiftId' => $reqData->shiftId));
                if (!empty($reqData->preferred_members)) {
                    foreach ($reqData->preferred_members as $val) {
                        if (!empty($val->select->value)) {
                            $preffer_member[] = array('memberId' => $val->select->value, 'shiftId' => $reqData->shiftId);
                        }
                    }
                }
                if (!empty($preffer_member)) {
                    $this->basic_model->insert_records('shift_preferred_member', $preffer_member, $multiple = true);

                    $this->loges->setUserId($reqData->shiftId);
                    $this->loges->setDescription(json_encode($reqData));
                    $this->loges->setTitle('Update prefered member : Shift Id ' . $reqData->shiftId);
                    $this->loges->createLog();
                }

                echo json_encode(array('status' => true));
            }
        }

        public function call_allocated() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $shiftId = $reqestData->data->shiftId;

                $this->loges->setUserId($shiftId);
                $this->loges->setDescription(json_encode($reqestData));
                $this->loges->setTitle('Call to allocater : Shift Id ' . $shiftId);
                $this->loges->createLog();

                $data = array('confirmed_on' => DATE_TIME);
                $this->basic_model->update_records('shift_confirmation', $data, $where = array('shiftId' => $shiftId));

                echo json_encode(array('status' => true));
            }
        }

        public function call_booker() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);

            if (!empty($reqestData->data)) {
                $shiftId = $reqestData->data->shiftId;

                $this->loges->setUserId($shiftId);
                $this->loges->setDescription(json_encode($reqestData));
                $this->loges->setTitle('Call to booker : Shift Id ' . $shiftId);
                $this->loges->createLog();

                $data = array('confirmed_with_allocated' => DATE_TIME);
                $this->basic_model->update_records('shift_confirmation', $data, $where = array('shiftId' => $shiftId));
                echo json_encode(array('status' => true));
            }
        }

        public function get_booking_list() {
            $reqestData = request_handler();
            $rows = $this->CrmParticipant_model->get_booking_list($reqestData);
            echo json_encode(array('status' => true, 'shift_data' => $rows));
        }

        public function get_auto_fill_shift_member() {
            $reqestData = request_handler();
            require_once APPPATH . 'Classes/crm/Shift.php';
            $objShift = new ShiftClass\Shift();

            $pre_selected_member = array();

            if ($reqestData->data) {
                $reqData = $reqestData->data;


                if ($reqData->listShiftsIDs) {
                    $listShifts = array_keys((array) $reqData->listShiftsIDs, true);

                    $objShift->setShiftId($listShifts);

                    // true for multiple shift id details
                    $result = $objShift->get_shift_details(true);
                    if (!empty($result)) {
                        foreach ($result as $key => $val) {
                            $objShift->setShiftId($val['id']);

                            // if booked by 1 mean this booking by organization so get shift relation details get form organization
                            if ($val['booked_by'] == 1) {
                                // get shift organization
                                $result[$key]['shift_organiztion_site'] = $val['shift_organiztion_site'] = $objShift->get_shift_oganization();
                            } else {
                                // get shift participant
                                $result[$key]['shift_participant'] = $val['shift_participant'] = $objShift->get_shift_participant();
                            }


                            $result[$key]['preferred_member'] = $val['preferred_member'] = $participants = $objShift->get_preferred_member();
                            $result[$key]['location'] = $val['location'] = $objShift->get_shift_location();

                            $result[$key]['available_member'] = $this->get_available_member($val, $participants, $pre_selected_member);

                            if (!empty($result[$key]['available_member']['memberId'])) {
                                $pre_selected_member[$val['shift_date']][] = $result[$key]['available_member']['memberId'];
                            }

                            $result[$key]['accepted'] = false;
                        }
                    }
                }
            }
            echo json_encode(array('status' => true, 'data' => $result));
        }

        public function get_available_member($shiftDetails, $participants, $pre_selected_member) {
            require_once APPPATH . 'Classes/crm/Shift.php';
            $objShift = new ShiftClass\Shift();


            // check all available member according city
            $near_city_members = $objShift->get_available_member_by_city($shiftDetails['id'], $shiftDetails['shift_date'], $shiftDetails['start_time'], $pre_selected_member);
            // check and filter memeber accroding of minimum pay level and pay point skill
            if (!empty($near_city_members['available_members']) && count($near_city_members['available_members']) > 0) {
                $memberPayPoint = $this->CrmParticipant_model->get_member_allocation_by_pay_point($near_city_members['available_members'], ['support_type' => 1]);
                if (count($memberPayPoint) > 0) {
                    $memberIds = array_column($memberPayPoint, 'memberId');
                    $near_city_members['available_members'] = $memberIds;
                }
            }
            $available_member = $near_city_members['available_members'];
            // check here preffer member if availble then alot
            if (!empty($shiftDetails['preferred_member']) && count($available_member) > 1) {
                foreach ($shiftDetails['preferred_member'] as $val) {
    //                print_r($available_member);
                    if (in_array($val->memberId, $available_member)) {
                        //array_column('memberId', $available_member)
                        $available_member = array($val->memberId);
                    }
                }
            }


            // check that highest match mean how many time previous work
            if (count($available_member) > 1 && $shiftDetails['booked_by'] == 2) {
                $participantIds = array_column($shiftDetails['shift_participant'], 'participantId');

                $result = $objShift->get_available_member_by_previous_work($available_member, $participantIds[0]);

                $available_member = (!empty($result)) ? $result : $available_member;
            }



            if (empty($available_member)) {
                return array('shared_activity' => [], 'shared_place' => []);
            }

            // get member details accoding to organization
            if ($shiftDetails['booked_by'] == 1) {
                // get participant ids
                $siteId = array_column($shiftDetails['shift_organiztion_site'], 'siteId');

                // get member details
                $result = (array) $objShift->get_member_details($available_member[0]);
                $result['shared_activity'] = array();
                $result['shared_place'] = array();

                return $result;
            } elseif ($shiftDetails['booked_by'] == 2 || $shiftDetails['booked_by'] == 3) { // get member details accoding to participants and location
                // get participant ids
                $participantIds = array_column($shiftDetails['shift_participant'], 'participantId');

                // if get more member then check its shared places and activity
                if (count($available_member) > 0) {
                    $available_member[] = $objShift->get_available_member_by_preferences($available_member, $participantIds);
                }


                if (!empty($available_member)) {
                    // get member details
                    $result = (array) $objShift->get_member_details($available_member[0]);

                    //get all perfered places and activity
                    $shared = $objShift->get_prefference_activity_places($available_member[0], $participantIds[0]);

                    $result = array_merge($result, $shared);
                    $result['distance_km'] = $near_city_members['memberIdWithDistance'][$result['memberId']];

                    return $result;
                }
            }
        }

        public function assign_autofill_member() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);
            $memberAssign = $updateShift = array();

            if ($reqestData->data) {
                $shift_details = $reqestData->data;

                if (!empty($shift_details)) {
                    foreach ($shift_details as $val) {
                        if ($val->accepted) {
                            $updateShift = array('updated' => DATE_TIME, 'status' => 2);
                            $this->basic_model->update_records('shift', $updateShift, $where = array('id' => $val->id));

                            $memberAssign[] = array('shiftId' => $val->id, 'memberId' => $val->available_member->memberId, 'status' => 1, 'created' => DATE_TIME);

                            $this->loges->setUserId($val->id);
                            $this->loges->setDescription(json_encode($val));
                            $this->loges->setTitle('Assign shift to member ' . $val->available_member->memberName . ' : Shift Id ' . $val->id);
                            $this->loges->createLog();
                        }
                    }
                }

                if (!empty($memberAssign)) {
                    $this->basic_model->insert_records('shift_member', $memberAssign, $multiple = true);

                    echo json_encode(array('status' => true));
                } else {
                    echo json_encode(array('status' => false, 'error' => 'Please assign member to at least one shift'));
                }
            }
        }

        public function get_shift_loges() {
            $reqestData = request_handler();
            if ($reqestData->data) {
                $reqData = $reqestData->data;
                $colowmn = array('title', "DATE_FORMAT(created, '%d/%m/%Y') as created", "DATE_FORMAT(created, '%H:%i %p') as time");
                $result = $this->basic_model->get_record_where('logs', $colowmn, $where = array('userId' => $reqData->shiftId, 'module' => 4));

                echo json_encode(array('status' => true, 'data' => $result));
            }
        }

        public function make_roster_data($data, $rosterId = false) {
            foreach ($data as $key => $weekData) {
                $dayCounter = 1;
                foreach ($weekData as $day => $multipleShift) {
                    foreach ($multipleShift as $val) {
                        if ($val->is_active) {

                            $start_time = date('Y-m-d H:i:s', strtotime($val->start_time));
                            $end_time = date('Y-m-d H:i:s', strtotime($val->end_time));

                            $roster_data = array('week_day' => $dayCounter, 'start_time' => $start_time, 'end_time' => $end_time, 'week_number' => ($key + 1));

                            if (!empty($rosterId))
                                $roster_data['rosterId'] = $rosterId;

                            $rosters[] = $roster_data;
                        }
                    }
                    $dayCounter++;
                }
            }

            return $rosters;
        }

        public function create_roster($participantId,$reqData) {
            require_once APPPATH . 'Classes/crm/Roster.php';
            $objRoster = new RosterClass\Roster();

          if (!empty($participantId)) {
                $reqData = (object)$reqData;
               
                $roster_listing = $reqData->rosterList;
               
          
                if (!empty($roster_listing)) {

                    // get shift round
                    $shift_round = json_encode($this->get_shift_round($roster_listing));

                    $objRoster->setParticipantId($participantId);
                    $objRoster->setStart_date(DateFormate($reqData->start_date, 'Y-m-d'));
                    $objRoster->setCreated(DATE_TIME);
                    $objRoster->setStatus(1);
                    $objRoster->setShift_round($shift_round);

                    $is_default = ($reqData->is_default == 1) ? 1 : 0;
                    $objRoster->setIs_default($is_default);

                    if ($is_default == 1) {
                        $objRoster->setTitle($reqData->title);
                        $objRoster->setEnd_date(DateFormate($reqData->end_date, 'Y-m-d'));
                    }

                    // start for rollback
                    $this->db->trans_begin();

                    if (!empty($reqData->rosterId)) {
                        // update roster
                        $rosterId = $reqData->rosterId;
                        $objRoster->setRosterId($rosterId);
                        $where = array('rosterId'=>$rosterId);
                        $this->basic_model->delete_records('crm_participant_shifts',$where);
                        $this->basic_model->delete_records('crm_participant_roster_data',$where);
                        $objRoster->updateRoster();
                        $this->loges->setSubModule(2);
                        $this->loges->setTitle('Update Roster : Roster Id ' . $rosterId);
                    } else {
                        if ($objRoster->getIs_default() == 0) {
                            // old defualt roster remove if new roster is default roster
                            $this->basic_model->update_records('crm_participant_roster', array('status' => 5, 'updated' => DATE_TIME), $where = array('participantId' => $objRoster->getParticipantId(), 'is_default' => 0));
                        }

                        // create roster
                        $rosterId = $objRoster->createRoster();
                        $objRoster->setRosterId($rosterId);
                        $objRoster->rosterCreateMail();

                        $this->loges->setTitle('Added New Roster : Roster Id ' . $rosterId);
                    }
                    $this->loges->setModule(11);
                    $this->loges->setUserId($participantId);
                    $this->loges->createLog();
                    $roster = array('start_date' => $reqData->start_date, 'participantId' => $participantId, 'status' => 1, 'created' => DATE_TIME);

                    $roster['is_default'] = ($reqData->is_default == 1) ? 1 : 0;
                    $objRoster->setIs_default($roster['is_default']);
                    if ($roster['is_default'] == 1) {
                        $objRoster->setEnd_date($reqData->end_date);
                        $roster['end_date'] = $reqData->end_date;
                    } else {
                        $roster['end_date'] = date('Y-m-d', strtotime($roster['start_date'] . ' + 91 days'));
                    }

                    // get shift round
                    $shift_round = $this->get_shift_round($reqData->rosterList);

                    // make roster data
                    $roster_data = $this->make_roster_data($reqData->rosterList, $rosterId );

                    if(!empty($roster_data)){
                      $this->db->trans_commit();

                      $this->basic_model->delete_records('crm_participant_roster_data', $where = array('id' => $rosterId));

                      $this->basic_model->insert_records('crm_participant_roster_data', $roster_data, $multiple = true);
                    }

                    // $rosterId = $objRoster->getRosterId();

                    $new_roster_shifts = $this->create_shift_from_roster($rosterId, (object) $roster, $roster_data, $shift_round);
                    $new_shiftDate = array_column($new_roster_shifts, 'shift_date');
                    $old_shifts = array();
                    if (!empty($new_shiftDate)) {
                        $old_shifts = $this->CrmParticipant_model->get_previous_shifts($roster['participantId'], $new_shiftDate);
                    }

                    $collapse_shifts = array();
                    $oldShiftDates = array_column($old_shifts, 'shift_date');

                    $array_status = array(1 => 'Unfilled', 2 => 'Unconfirmed', 3 => 'Quote', 4 => 'Rejected', 5 => 'Cancelled', 6 => 'Confirmed');

                    if (!empty($new_roster_shifts)) {

                        foreach ($new_roster_shifts as $key => $val) {
                            $collapse_shifts[$key]['is_collapse'] = false;

                            $responseMatchedDate = $this->mappingDate($oldShiftDates, $val['shift_date']);

                            if (!empty($responseMatchedDate)) {
                                $indexKey = 0;
                                foreach ($responseMatchedDate as $index => $old_shift_key) {

                                    $o_str_time = $old_shifts[$old_shift_key]['start_time'];
                                    $o_end_time = $old_shifts[$old_shift_key]['end_time'];

                                    $condtion1 = (((strTime($val['start_time']) < strTime($o_str_time)) && (strTime($o_str_time) < strTime($val['end_time']))));
                                    $condtion2 = (((strTime($val['start_time']) < strTime($o_end_time)) && (strTime($o_end_time) < strTime($val['end_time']))));


                                    $condtion3 = (((strTime($o_str_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_str_time))));
                                    $condtion4 = (((strTime($o_end_time) < strTime($val['start_time'])) && (strTime($val['end_time']) < strTime($o_end_time))));

                                    if ($condtion1 || $condtion2 || $condtion3 || $condtion4) {
                                        $collapse_shifts_count++;
                                        $collapse_shifts[$key]['is_collapse'] = true;
                                        $collapse_shifts[$key]['status'] = 2;

                                        $collapse_shifts[$key]['old'][$indexKey] = $old_shifts[$old_shift_key];
                                        $collapse_shifts[$key]['old'][$indexKey]['status'] = $array_status[$old_shifts[$old_shift_key]['status']];
                                        $collapse_shifts[$key]['old'][$indexKey]['active'] = false;
                                        $indexKey++;
                                    }
                                }
                            }

                            $collapse_shifts[$key]['new'] = $val;

                            $startTime = new DateTime($val['start_time']);
                            $endTime = new DateTime($val['end_time']);
                            $interval = $endTime->diff($startTime);
                            $difference = $interval->format('%H.%i hrs.');


                            $collapse_shifts[$key]['new']['start_time'] = $val['start_time'];
                            $collapse_shifts[$key]['new']['end_time'] = $val['end_time'];
                            $collapse_shifts[$key]['new']['shift_date'] = $val['shift_date'];
                            $collapse_shifts[$key]['new']['duration'] = $difference;
                            $collapse_shifts[$key]['new']['status'] = 'Unfilled';
                        }
                    }
                    if (!empty($roster_data)) {
                        $this->db->trans_commit();
                        // first delete all data and then insert


                        // check if collapse shift found
                        if (!empty($collapse_shifts)) {
                            $this->resolve_collapse_shift_insert($collapse_shifts, $participantId);
                        }


                        return true;
                    } else {
                        // for previous data remove from roster table
                        $this->db->trans_rollback();

                        return false;
                    }
                }
            }
        }

        function resolve_collapse_shift_insert($rosterShift, $participantId) {
           $participantInfo = $this->CrmParticipant_model->get_participant_shift_related_information($participantId);
           if (!empty($rosterShift)) {
               foreach ($rosterShift as $key => $val) {
                 $val = (object)$val;
                   $shift = array();

                   $rost =(object) $val->new;
                   $rosterCancel = false;

                   if ($val->is_collapse == 'true') {
                       if ($val->status == 2) {
                           if (!empty($val->old)) {
                               $this->archiveShifts($val->old);
                           }
                       } elseif (!empty($val->old)) {

                           $rosterCancel = true;
                           $this->archiveShifts($val->old);
                       }
                   }


                   if ($rosterCancel == false) {
                       $shift = array('crm_participant_id' => $participantId,'rosterId'=>$rosterId, 'shift_date' => DateFormate($rost->shift_date, "Y-m-d H:i:s"), 'start_time' => concatDateTime($rost->shift_date, $rost->start_time), 'end_time' => concatDateTime($rost->shift_date, $rost->end_time), 'status' => 1, 'created' => DATE_TIME, 'updated' => DATE_TIME);

                       // create shift
                       $shiftId = $this->basic_model->insert_records('crm_participant_shifts', $shift, $multiple = FALSE);

                       // insert participant data
                       // $shift_participant = array('shiftId' => $shiftId, 'participantId' => $participantId, 'status' => 1, 'created' => DATE_TIME);
                       // $this->basic_model->insert_records('shift_participant', $shift_participant, $multiple = FALSE);

                       // insert shift location
                       // $shift_location = array('address' => $participantInfo->address, 'suburb' => $participantInfo->suburb, 'state' => $participantInfo->state, 'postal' => $participantInfo->postal, 'lat' => $participantInfo->lat, 'long' => $participantInfo->long, 'shiftId' => $shiftId);
                       // $this->basic_model->insert_records('shift_location', $shift_location, $multiple = FALSE);

                       // insert confirmation detials
                       // $shift_confirmation = array('firstname' => $participantInfo->firstname, 'lastname' => $participantInfo->lastname, 'email' => $participantInfo->email, 'phone' => $participantInfo->phone, 'confirm_by' => $participantInfo->prefer_contact, 'confirm_with' => 1, 'shiftId' => $shiftId);
                       // $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);

                       // insert shift requirement
                       // $shiftRequirement = array();
                       // if (!empty($participantInfo->requirement)) {
                       //     foreach ($participantInfo->requirement as $val) {
                       //         $shiftRequirement[] = array('requirementId' => $val->assistanceId, 'shiftId' => $shiftId);
                       //     }
                       //
                       //     $this->basic_model->insert_records('shift_requirements', $shiftRequirement, $multiple = true);
                       // }

                       // $this->basic_model->insert_records('shift_confirmation', $shift_confirmation, $multiple = FALSE);
                   }
               }
           }
       }

       function create_shift_from_roster($rosterId = false, $rosterData = false, $rosterDayData = false, $totalWeek = false) {
           $shiftRoster = array();
           /*
            *  here all parameter is optional
            *  but here roster id or rosterData , rosterDayData and totlWeek is mandatory to create shift
            *  if give roster only roster id mean also want insert in database
            *  and if given data mean want shift data
            */

           // if roster id is absent mean data send manually for only get shifts
           if (!empty($rosterId)) {
               $colown = array('is_default', 'start_date', 'end_date', 'status', 'participantId', 'shift_round');
               // get roster data
               $rosterData = $this->basic_model->get_row('crm_participant_roster', $colown, $where = array('id' => $rosterId));

               // get participant information
             //  $participantInfo = $this->CrmParticipant_model->get_participant_shift_related_information($rosterData->participantId);

               // get roster data of time
               $rosterDayData = $this->CrmParticipant_model->get_roster_data($rosterId);

               $totalWeek = json_decode($rosterData->shift_round);
           }

           $newRosterData = array();
           if (!empty($rosterDayData)) {
               foreach ($rosterDayData as $val) {
                   $newRosterData[$val['week_number']][numberToDay($val['week_day'])][] = array('start_time' => $val['start_time'], 'end_time' => $val['end_time']);
               }
           }


           if($rosterData->is_default==1){
             $dateDiff = $this->dateDiffInDays($rosterData->start_date, $rosterData->end_date);
             $shiftCreateEndDate = date('Y-m-d', strtotime($rosterData->start_date . ' + '.$dateDiff.' days'));
           } else {
              $shiftCreateEndDate = date('Y-m-d', strtotime($rosterData->start_date . ' + 91 days'));
            }


           $date_range = dateRangeBetweenDateWithWeek($rosterData->start_date, $shiftCreateEndDate, $totalWeek);
           $shifts = array();
           if (!empty($date_range)) {
               foreach ($date_range as $week_number => $week) {
                   foreach ($week as $days) {
                       foreach ($days as $date => $day) {


                           if (array_key_exists($week_number, $newRosterData)) {
                               if (array_key_exists($day, $newRosterData[$week_number])) {

                                   for ($i = 0; $i < count($newRosterData[$week_number][$day]); $i++) {
                                       $start_time = $newRosterData[$week_number][$day][$i]['start_time'];
                                       $end_time = $newRosterData[$week_number][$day][$i]['end_time'];
                                       $shift = array('crm_participant_id' => $rosterData->participantId,'rosterId'=>$rosterId, 'shift_date' => $date, 'start_time' => concatDateTime($date, $start_time),  'end_time' => concatDateTime($date, $end_time), 'status' => 1, 'created' => DATE_TIME, 'updated' => DATE_TIME);

                                       if (!empty($rosterId)) {
                                           // create shift
                                           $shiftId = $this->basic_model->insert_records('crm_participant_shifts', $shift, $multiple = FALSE);

                                        } else {
                                           $shiftRoster[] = $shift;
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
           }

           return $shiftRoster;

       }


       function dateDiffInDays($date1, $date2)
{
   // Calulating the difference in timestamps
   $diff = strtotime($date2) - strtotime($date1);

   // 1 day = 24 hours
   // 24 * 60 * 60 = 86400 seconds
   return abs(round($diff / 86400));
}
        function archiveShifts($shifts) {
            if (!empty($shifts)) {
                foreach ($shifts as $oldShift) {
                    if (!$oldShift->active) {
                        $this->basic_model->update_records('crm_participant_shifts', array('status' => 8), $where = array('id' => $oldShift->id));
                    }
                }
            }
        }

        function mappingDate($shiftDates, $singleDate) {
            $matchDateKey = array();
            if (!empty($shiftDates)) {
                foreach ($shiftDates as $key => $val) {
                    if ($val == $singleDate) {
                        $matchDateKey[] = $key;
                    }
                }

                return $matchDateKey;
            }
        }



        function get_shift_round($rosterList) {
            $shift_round = array();
            if (!empty($rosterList)) {
                foreach ($rosterList as $key => $val) {
                    $shift_round[] = $key + 1;
                }
            }
            return $shift_round;
        }

        function approve_and_deny_roster() {
            $reqestData = request_handler();
            $this->loges->setCreatedBy($reqestData->adminId);
            $this->loges->setSubModule(2);
            $this->load->model('Roster_model');

            $this->loges->setDescription(json_encode($reqestData));
            if ($reqestData->data) {
                $reqData = $reqestData->data;

                if ($reqData->status == 4) {

                    $this->basic_model->update_records('crm_participant_roster', array('status' => 4), $where = array('id' => $reqData->rosterId));
                    $message = 'Roster reject successfully';
                    $this->loges->setTitle('Roster rejected : Roster Id ' . $reqData->rosterId);
                } else {

                    // check that its tempary collapse shift
                    $result = $this->Roster_model->getRosterTempData($reqData->rosterId);

                    if (!empty($result)) {
                        $rosterData = json_decode($result->rosterData);
                        $this->resolve_collapse_shift_insert($rosterData, $result->participantId);
                    }

                    $this->loges->setTitle('Roster approve : Roster Id ' . $reqData->rosterId);
                    $message = 'Roster approve successfully';
                    if ($result->is_default == 0) {
                        // old defualt roster remove if new roster is default roster
                        $this->basic_model->update_records('participant_roster', array('status' => 5), $where = array('participantId' => $result->participantId, 'is_default' => 0));
                    }
                    $this->basic_model->update_records('participant_roster', array('status' => 1, 'updated' => DATE_TIME), $where = array('id' => $reqData->rosterId));
                }

                $this->loges->setUserId($reqData->rosterId);
                $this->loges->createLog();

                echo json_encode(array('status' => true, 'message' => $message));
            }
        }

        public function get_shift_dashboard_count() {
            $reqData = request_handler();
            if (!empty($reqData->data)) {
                $post_data = $reqData->data;

                $data = $this->CrmParticipant_model->get_count_shift_graph($post_data->view_type_graph);
                $data['count_fill_by_admin'] = $this->CrmParticipant_model->get_count_filled_shift($post_data->view_type_by_admin, 'by_admin');
                $data['count_fill_by_app'] = $this->CrmParticipant_model->get_count_filled_shift($post_data->view_type_by_app, 'by_app');

                echo json_encode(array('data' => $data, 'status' => TRUE));
                exit();
            }
        }

        public function participant_shifts() {
          $reqData = request_handler();
          if (!empty($reqData->data)) {
            $reqData = $reqData->data;
            $limit = $reqData->pageSize;
            $page = $reqData->page;
            $sorted = $reqData->sorted;
            $filter = $reqData->filtered;
            $orderBy = '';
            $direction = ''; 

            $tbl_shift = TBL_PREFIX . 'crm_participant_shifts';
            $tbl_shift_participnt = TBL_PREFIX . 'shift_crm_participant';
            $tbl_participnt = TBL_PREFIX . 'crm_participant';
             $src_columns = array($tbl_shift . ".id", $tbl_shift . ".shift_date",  $tbl_shift . '.crm_participant_id', $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_participnt . '.firstname', $tbl_participnt . '.middlename', $tbl_participnt . '.lastname', "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname)", "CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".lastname)"
            , "CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

            if (!empty($sorted)) {
                if (!empty($sorted[0]->id)) {
                    $orderBy = $sorted[0]->id;
                    $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
                }
            } else {
                $orderBy = $tbl_shift . '.id';
                $direction = 'ASC';
            }


            if (!empty($filter->search_box)) {
                $this->db->group_start();

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search_box);
                    }
                    else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search_box);
                    }
                }

                $this->db->group_end();
            }

            if (!empty($filter->shift_date)) {
                $this->db->where(array("DATE(tbl_crm_participant_shifts.start_time)" => date('Y-m-d', strtotime($filter->shift_date))));
            } else {
                if (!empty($filter->start_date)) {
                    $this->db->where("DATE(tbl_crm_participant_shifts.start_time) >= '" . date('Y-m-d', strtotime($filter->start_date)) . "'");
                }
                if (!empty($filter->end_date)) {
                    $this->db->where("DATE(tbl_crm_participant_shifts.start_time) <= '" . date('Y-m-d', strtotime($filter->end_date)) . "'");
                }
            }

            if (!empty($filter->shift_type)) {
                if ($filter->shift_type == 'am' || $filter->shift_type == 'pm' || $filter->shift_type == 'so') {
                    $insterval = shift_type_interval($filter->shift_type);
                    $this->db->group_start();

                    $this->db->where("DATE_FORMAT(tbl_crm_participant_shifts.start_time,'%H:%i') >=", date($insterval['start_time']));

                    if ($insterval['spacial']) {
                        $this->db->where("DATE_FORMAT(tbl_crm_participant_shifts.end_time,'%H:%i') <=", "DATE_FORMAT(DATE_ADD(tbl_crm_participant_shifts.end_time, INTERVAL 1 DAY), '%Y-%m-%d 06:00'");
                    } else {
                        $this->db->where("DATE_FORMAT(tbl_crm_participant_shifts.end_time,'%H:%i') <=", date($insterval['end_time']));
                    }

                    $this->db->group_end();
                }
            }

            $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time",  $tbl_shift . '.crm_participant_id');

            $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from($tbl_shift);

            $this->db->select("CONCAT(" . $tbl_participnt . ".firstname,' '," . $tbl_participnt . ".middlename,' '," . $tbl_participnt . ".lastname ) as participantName");
            $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");

            #$this->db->join($tbl_shift_participnt, $tbl_shift_participnt . '.id = ' . $tbl_shift . '.id AND ' . $tbl_shift_participnt . '.status = 1', 'left');
            $this->db->join($tbl_participnt, $tbl_participnt . '.id = ' . $tbl_shift . '.crm_participant_id AND tbl_crm_participant.archive = 0', 'left');


            $this->db->order_by($orderBy, $direction);
            $this->db->limit($limit, ($page * $limit));

            $this->db->where($tbl_shift . '.shift_date >=', date('Y-m-d'));
            $this->db->where($tbl_shift . ".crm_participant_id=".$reqData->participantId);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
           
            $total_count = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
            // print_r($total_count); die;

            if ($dt_filtered_total % $limit == 0) {
                $dt_filtered_total = ($dt_filtered_total / $limit);
            } else {
                $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
            }

            $dataResult = $query->result();

            if (!empty($dataResult)) {
                foreach ($dataResult as $val) {
                    // if ($val->booked_by == 1) {
                    //     $val->participantName = $val->site_name;
                    // }

                    // $val->address = $this->get_shift_location($val->id);
                    // $val->memberName = $this->get_allocated_member($val->id);

                    $val->diff = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

                    $val->duration = ($val->duration);
                }
            }

            $return = array('count' => $dt_filtered_total, 'data' => $dataResult, 'total_count' => $total_count);
            echo json_encode(array('data' => $return, 'status' => TRUE));
            exit();
        }

      }
      public function get_shift_details() {
          $reqData = request_handler();

          require_once APPPATH . 'Classes/crm/Shift.php';
          $objShift = new ShiftClass\Shift();

          if (!empty($reqData->data)) {
              $shiftId = $reqData->data->id;

              $objShift->setId($shiftId);

              // get shift details
              $result = $objShift->get_shift_details();

              // if booked by 1 mean this booking by organization so get shift relation details get form organization
              // if ($result['booked_by'] == 1) {
              //     // get shift organization
              //     $result['shift_organiztion_site'] = $objShift->get_shift_oganization();
              // } else {
              //     // get shift participant
              //     $result['shift_participant'] = $objShift->get_shift_participant();
              // }

              // get shift location
              // $result['location'] = $objShift->get_shift_location();

              // if (!empty($result['location'])) {
              //     foreach ($result['location'] as $key => $val) {
              //         $result['location'][$key]->suburb = array('value' => $val->suburb, 'label' => $val->suburb);
              //     }
              // }

              // if status 7 that mean shift is confirmed then get confirmed member details
              // if ($result['status'] == 7) {
              //     $result['allocated_member'] = $objShift->get_accepted_shift_member();
              // } elseif ($result['status'] == 2) {
              //     $result['allocated_member'] = $objShift->get_allocated_member();
              // } else if ($result['status'] == 5) {
              //     $result['cancel_data'] = $objShift->get_cancelled_details();
              // } else if ($result['status'] == 4) {
              //     $result['rejected_data'] = $objShift->get_rejected_member();
              // } else {
              //     $preffered_member = $objShift->get_preferred_member();
              //     if (!empty($preffered_member)) {
              //         foreach ($preffered_member as $val) {
              //             $val->select = array('value' => $val->memberId, 'label' => $val->memberName);
              //         }
              //     }
              //     $result['preferred_member'] = $preffered_member;
              // }

              // get shift caller
              // $result['shift_caller'] = (array) $objShift->get_shift_caller();
              //
              // // get shift confirmation details
              // $result['confirmation_details'] = (array) $objShift->get_shift_confirmation_details();

              // get shift requirement
              // $response = $objShift->get_shift_requirement();

              // $result = array_merge( $result);
              echo json_encode(array('status' => true, 'data' => $result));
          }
      }

      public function get_roster_details() {
          $reqData = request_handler();
          if (!empty($reqData->data)) {
            $roster = array();
            $crm_participant_id = $reqData->data->id;
            // get roster main data
            $this->db->select(['pr.id','pr.id as rosterId', 'pr.title', 'pr.start_date', 'pr.end_date', 'pr.shift_round', 'pr.is_default', 'pr.participantId', 'pr.status']);
            $this->db->select("CASE pr.is_default
                WHEN 1 THEN (pr.title)
                WHEN 0 THEN ('Default')
                ELSE NULL
                END as title");
            $this->db->select('CONCAT("\'",p.firstname,"\' ",p.middlename, " ", p.lastname ) as participantName');
            $this->db->from('tbl_crm_participant_roster as pr');
            $this->db->join('tbl_crm_participant as p', 'p.id = pr.participantId', 'left');

            $this->db->where('pr.participantId', $crm_participant_id);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $roster = $query->row_array();
            $rosterData = array();
            if(!empty($roster)){
            $date1 = date_create(DateFormate($roster['start_date'], 'Y-m-d'));
            $date2 = date_create(DATE_TIME);

            $diff = date_diff($date1, $date2);
            $roster['day_count'] = $diff->days;

            // $roster['update_disabled'] = ($diff->days > 57) ? false : true;
            $roster['update_disabled'] = true;
            $roster['is_default'] = ($roster['is_default'] > 0) ? 1 : 2;

            $roster['participant'] = ['value' => $roster['participantId'], 'label' => $roster['participantName']];

            $roster['end_date'] = ($roster['end_date'] == '0000-00-00 00:00:00') ? '' : $roster['end_date'];
            $rosterId = $roster['id'];
            // get roster week details
            $this->db->select(['week_day', 'start_time', 'end_time', 'week_number']);
            $this->db->from('tbl_crm_participant_roster_data');
            $this->db->where('rosterId', $rosterId);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $rosterData = $query->result();
          }

            // create initial per day shift is not active (mean blank shift)
            $tempList = [['is_active' => false], ['is_active' => false], ['is_active' => false], ['is_active' => false]];
            $tempRoster = [];

            if (!empty($rosterData)) {
                $cnt = 0;
                foreach ($rosterData as $key => $val) {
                    $weekKey = $val->week_number - 1;
                    $dynamicIndex = 0;
                    $dayName = numberToDay($val->week_day);

                    if (!isset($tempRoster[$weekKey][$dayName])) {
                        $tempRoster[$weekKey][$dayName] = $tempList;
                    }


                    if (array_key_exists($weekKey, $tempRoster)) {

                        if (array_key_exists($dayName, $tempRoster[$weekKey])) {
                            foreach ($tempRoster[$weekKey][$dayName] as $check) {
                                if ($check['is_active']) {
                                    $dynamicIndex++;
                                }
                            }
                        }
                    }


                    $tempRoster[$weekKey][$dayName][$dynamicIndex]['is_active'] = true;
                    $tempRoster[$weekKey][$dayName][$dynamicIndex]['start_time'] = $val->start_time;
                    $tempRoster[$weekKey][$dayName][$dynamicIndex]['end_time'] = ($val->end_time == '0000-00-00 00:00:00') ? '' : $val->end_time;
                    ++$cnt;
                }
            }

            $resosterList = [];

            $shift_round = json_decode($roster['shift_round']);

            $weekKeyArray = array_keys($shift_round);
            foreach ($weekKeyArray as $weekNumber) {
                foreach (numberToDay() as $weekDayNum => $weekDay) {
                    $status = true;
                    // $resosterList[$weekNumber][$weekDay] = $tempList;

                    if (array_key_exists($weekNumber, $tempRoster)) {
                        if (array_key_exists($weekDay, $tempRoster[$weekNumber])) {
                            $status = false;
                            $dynamicIndex = 0;

                            $resosterList[$weekNumber][$weekDay] = $tempRoster[$weekNumber][$weekDay];
                        }
                    }

                    if ($status) {
                        $resosterList[$weekNumber][$weekDay] = $tempList;
                    }
                }
            }

            $roster['rosterList'] = $resosterList;

            echo json_encode(array('status' => true, 'data' => $roster));

        }
      }

      public function checkNdis(){
       $reqData = request_handler();
       if (!empty($reqData->data)) {
       $reqData  = json_decode($reqData->data);
       $where=array("ndis_num"=>$reqData->ndis_num,"status"=>1);
       $result = $this->Basic_model->get_record_where("crm_participant", "*", $where);
       // var_dump($result);
     if (!empty($result)) {
       $res   = $this->CrmParticipant_model->prospective_participant_details($result[0]->id);

         $response = array(
           'status' => false,
           'error' => system_msgs('ndis_exist'),
           'data' => $res
         );
       }else{
         $response = array(
           'status' => true,
         );
       }
       echo json_encode($response);
     }
 }


 public function upload_documents(){
      $request          = request_handlerFile();
      $member_data      = (array) $request;

      $crmParticipantId = $this->input->post("crmParticipantId");
      $category         = $this->input->post("category");
      $stage_id         = $this->input->post("stage_id");
      $type         = $this->input->post("type");
      if (!empty($member_data)) {
          $validation_rules = array(

              array(
                  'field' => 'docsTitle',
                  'label' => 'Title',
                  'rules' => 'required'
              )
          );

          $this->form_validation->set_data($member_data);
          $this->form_validation->set_rules($validation_rules);

          if ($this->form_validation->run()) {

              if (!empty($_FILES)) {
                  $config['upload_path']    = CRM_UPLOAD_PATH;
                  $config['input_name']     = 'crmParticipantFiles';
                  $config['directory_name'] = $crmParticipantId;
                  $config['allowed_types']  = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';
                  $this->make_path(CRM_UPLOAD_PATH.$crmParticipantId);
                  $is_uploads = do_muliple_upload($config);

                  foreach($is_uploads as $is_upload){
                  if (isset($is_upload['error'])) {
                      echo json_encode(array(
                          'status' => false,
                          'error' => strip_tags($is_upload['error'])
                      ));
                      exit();
                  } else {
                    $rows ='';
                      $image = array();
                    $data = '';
                    if($type=='stage'){
                          $insert_ary = array(
                                  'filename' => $is_upload['upload_data']['file_name'],
                                  'crm_participant_id' => $crmParticipantId,
                                  'title' => $this->input->post('docsTitle'),
                                  'created' => DATE_TIME,
                                  'archive' => 0,
                                  "stage_id" =>  $stage_id,
                                  "type" => $category
                              );
                              $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);
                          }else{
                            $insert_ary = array(
                                  'filename' => $is_upload['upload_data']['file_name'],
                                  'crm_participant_id' => $crmParticipantId,
                                  'title' => $this->input->post('docsTitle'),
                                  'created' => DATE_TIME,
                                  'archive' => 0,
                                  "type" => $category
                              );
                              $rows       = $this->Basic_model->insert_records('crm_participant_docs', $insert_ary, $multiple = FALSE);

                            }
                      /*logs*/
                      $this->loges->setCreatedBy($request->adminId);
                      $this->loges->setUserId($crmParticipantId);
                      $this->loges->setDescription(json_encode($request));
                      $this->loges->setTitle('Added CRM Participant DOCS : ' . $crmParticipantId);
                      $this->loges->createLog();
                      if (!empty($rows)) {
                          echo json_encode(array(
                              'status' => true
                          ));
                          exit();
                      } else {
                          echo json_encode(array(
                              'status' => false
                          ));
                          exit();
                      }
                  }
                }
              } else {
                  echo json_encode(array(
                      'status' => false,
                      'error' => 'Please Select file.'
                  ));
                  exit();
              }
              $return = array(
                  'status' => true
              );
          } else {
              $errors = $this->form_validation->error_array();
              $return = array(
                  'status' => false,
                  'error' => implode("\n", $errors)
              );
          }
          echo json_encode($return);
      }
    }

    public function get_all_docs()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->all_participant_docs($reqData->crm_participant_id,$reqData->type);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response,
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    public function get_all_docs_by_category()
    {
        $this->load->model('CrmParticipant_model');
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData  = json_decode($reqData->data);
            $response = $this->CrmParticipant_model->all_participant_docs_by_category($reqData->crm_participant_id,$reqData->type);
            if (!empty($response)) {
                echo json_encode(array(
                    'status' => true,
                    'data' => $response,
                ));
            } else {
                echo json_encode(array(
                    'status' => false,
                    'error' => 'Sorry no data found'
                ));
            }
        }
    }

    public function latest_updates() {
        // get request data
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $request = $reqData->data;
            $response = $this->CrmParticipant_model->get_all_crm_logs($request);
            echo json_encode($response);
        }
    }
    public function get_ndis_services_p_participant_id(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $request = $reqData->data;
            $result = $this->CrmParticipant_model->get_ndis_services_p_participant_id($request);
            echo json_encode(array('status' => true, 'data' => $result));
        }
    }
   public function check_participant_email_id(){
    $reqData = request_handler();
    if (!empty($reqData->data)) {
        require_once APPPATH . 'Classes/crm/CrmParticipant.php';
        $objparticipant = new CrmParticipantClass\CrmParticipant();
        $objparticipant->setParticipantEmail($reqData->data->email);
        if ($objparticipant->checkEmailId()) {
            $response = array(
                'status' => false,
                'error' => system_msgs('email_id_exist')
            );
        }
        else{
            $response = array(
                'status' => true
            );
        }
        echo json_encode($response);
        exit();
    }
   }
   public function delete_participant_docs(){
    $reqData = request_handler();
    if (!empty($reqData->data)) {
        if ($this->CrmParticipant_model->delete_participant_docs($reqData->data)) {
            $response = array(
                'status' => true
            );
        }
        else{
            $response = array(
                'status' => false
            );
        }
        echo json_encode($response);
        exit();
    }
   }
   public function get_participant_dropdown_list(){  
    $this->load->model('CrmParticipant_model'); 
    $reqData = request_handler();     
    $response = $this->CrmParticipant_model->get_all_participant_dropdown_list();
   
    if (!empty($response)) {
        echo json_encode(array(
            'status' => true,
            'data' => $response
        ));
    } else {
        echo json_encode(array(
            'status' => false,
            'error' => 'Sorry no data found'
        ));
    }
}
    public function uploadNdisAttchments(){
         
            $uploadData=array();
            $successData=array();
            $failedMsg=NULL;
           if (!empty($_FILES) && count($_FILES['files']['name'])>0){
                $config['upload_path'] = FCPATH.ARCHIEVE_DIR;              
                $config['directory_name'] = ARCHIEVE_DIR; //$this->input->post('caseId');
                $config['allowed_types'] = 'jpg|jpeg|png|xlsx|xls|doc|docx|pdf';
                $config['max_size'] = DEFAULT_MAX_UPLOAD_SIZE;
                $filesCount = count($_FILES['files']['name']);
            
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['files']['size'][$i];
                    
                    // Load and initialize upload library
                    $this->load->library('upload');
                    // $this->load->library('upload', $config);
                    $this->upload->initialize($config);                
                    // Upload file to server
                    $is_upload = $this->upload->do_upload('file');
                    if(!$is_upload){                    
                        //$uploadData[$i]=array('status' => false, 'filename'=>$_FILES['ndis_file']['name'][$i],'error' => strip_tags($this->upload->display_errors()));
                        $failedMsg.='File Name ='.$_FILES['files']['name'][$i].' '.strip_tags($this->upload->display_errors()).', ';
                    }else{
                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $file_name = $upload_data['file_name'];
                        if (!empty($file_name)) {
                            $uploadData[$i]=array('name'=>$file_name);                        
                        }
                    }               
                }
            
                echo json_encode(array('status' => true,'files'=>$uploadData,'error'=>$failedMsg));
                exit();
           }
           echo json_encode(array('status' => false,'error'=>'Please select files first!'));
           exit();

    }

   


}
