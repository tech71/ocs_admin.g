<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CrmReporting extends MX_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmReporting_model');
    }
    public function location_analytics() {
        $reqData = request_handler();
        $adminId = $reqData->adminId;
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmReporting_model->location_analytics_list($reqData);
            echo json_encode($response);
        }
    }
    public function crm_participant_bar_status() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $all_status = $this->CrmReporting_model->crm_participant_bar_status();
            echo json_encode($all_status);
            exit();
        }
    }
    public function crm_participant_intake_volume_left() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $all_status = $this->CrmReporting_model->crm_participant_intake_volume_left($reqData->data);
            echo json_encode(array( 'status' => TRUE, 'data' => $all_status));
            exit();
        }
    }
    public function crm_participant_intake_volume_percentage_left() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $all_status = $this->CrmReporting_model->crm_participant_intake_volume_percentage_left($reqData->data);
            echo json_encode(array( 'status' => TRUE, 'data' => $all_status));
            exit();
        }
    }
    public function crm_participant_intake_volume_right() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $all_status = $this->CrmReporting_model->crm_participant_intake_volume_right($reqData->data);
            echo json_encode(array( 'status' => TRUE, 'data' => $all_status));
            exit();
        }
    }
    public function crm_participant_intake_volume_percentage_right() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $all_status = $this->CrmReporting_model->crm_participant_intake_volume_percentage_right($reqData->data);
            echo json_encode(array( 'status' => TRUE, 'data' => $all_status));
            exit();
        }
    }
    public function service_analytics(){
      $reqData = request_handler();
      $adminId = $reqData->adminId;
      if (!empty($reqData->data)) {
        $reqData = json_decode($reqData->data);
        $response = $this->CrmReporting_model->service_analytics($reqData);
        echo json_encode($response);
      }
    }
}
