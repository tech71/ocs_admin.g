<?php



defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class CrmDepartment extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('CrmDepartment_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Basic_model');        
        $this->loges->setLogType('crm_staff');
    }

    public function get_department()
    {
        $reqData =  $reqData1 = request_handler();
        if (!empty($reqData->data))
        {
            $reqData = json_decode($reqData->data);
            $result = $this->CrmDepartment_model->get_department($reqData);
            echo json_encode($result);
        }
    }

    public function get_all_department() {
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = json_decode($reqData->data);
            $response = $this->CrmDepartment_model->get_all_department($reqData);
            echo json_encode($response);
        }
    }

    public function create_department()
    {
        $reqData = request_handler();
        $this->loges->setCreatedBy($reqData->adminId);
        #pr($reqData);
        require_once APPPATH . 'Classes/crm/CrmDepartment.php';
        $objAdmin = new classCrmDepartment\CrmDepartment();

        if (!empty($reqData->data))
        {
            $data = (array) $reqData->data;
            $data['id'] = (!empty($data['id'])) ? $data['id'] : false;

            $this->form_validation->set_data($data);

            $validation_rules = array(
                array('field' => 'name', 'label' => 'Department Name','rules' => 'required'),
            );
            // set rules form validation
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {

                $objAdmin->setName($data['name']);

                if ($data['id'] > 0) {
                    // update department
                    $objAdmin->setId($data['id']);
                    $objAdmin->updateCrmDepartment();
                    $this->loges->setTitle('Department update: ' . $objAdmin->getName() );
                } else {
                    // create department
                    $dep_id = $objAdmin->creatCrmDepartment();
                    $this->loges->setTitle('New Department created: ' . $objAdmin->getName() );
                }
                $this->loges->setDescription(json_encode($reqData->data));
                $this->loges->createLog();
                $response = array('status' => true);
            } else {  die('0');
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($response);
        }
    }
    public function department_task_analytics(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $reqData = (array)($reqData->data);
            $response = $this->CrmDepartment_model->department_task_analytics($reqData);
            echo json_encode($response);
        }
    }

	 public function department_member_export(){
        $reqData = request_handler();
        if (!empty($reqData->data)) {
            $csv_data = $reqData->data->depExportList;
            $export_for = $reqData->data->export_for;
            //pr($csv_data); exit;
            $this->load->library("excel");
            $object = new PHPExcel();
            if(!empty($csv_data))
            {

                $object->setActiveSheetIndex(0);
                $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'HCMGR Id');
                $object->getActiveSheet()->setCellValueByColumnAndRow(1, 1, 'Department name');
                $object->getActiveSheet()->setCellValueByColumnAndRow(2, 1, 'Start');
                $object->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Member Name');


                $var_row = 2;
                foreach ($csv_data as $one_row)
                {
                    $object->getActiveSheet()->SetCellValue('A'.$var_row, $one_row->hcmgr_id);
                    $object->getActiveSheet()->SetCellValue('B'.$var_row, ucfirst($one_row->department_name));
                    $object->getActiveSheet()->SetCellValue('C'.$var_row, $one_row->created);
                    $object->getActiveSheet()->SetCellValue('D'.$var_row, $one_row->FullName);
                    $var_row++;
                }

                $object->setActiveSheetIndex()
                ->getStyle('A1:D1')
                ->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'C0C0C0:')
                        )
                    )
                );

                $object->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
                $object_writer = PHPExcel_IOFactory::createWriter($object, 'CSV');
                $filename = $export_for.'.csv';

                $object_writer->save(ARCHIEVE_DIR.'/'.$filename);

                echo json_encode(array('status'=>true,'csv_url'=>$filename));exit();
            }
            else
            {
                echo json_encode(array('status'=>false,'error'=>'No record to export'));exit();
            }
        }
    }
}
