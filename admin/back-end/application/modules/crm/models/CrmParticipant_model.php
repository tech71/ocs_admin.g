<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmParticipant_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->model('Basic_model');
		$this->loges->setLogType('crm_prospective_participant');
    }

    public function Check_UserName($objparticipant)
    {
        $username = $objparticipant->getUserName();
        $this->db->select(array(
            'count(*) as count'
        ));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array(
            'username' => $username
        ));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }

    public function prospective_participant_list($reqData, $loginId,$role)
    {
        $limit      = $reqData->pageSize;
        $page       = $reqData->page;
        $sorted     = $reqData->sorted;
        $filter     = $reqData->filtered;
        $orderBy    = '';
        $direction  = '';
        $status_con = array(
            1
        );

        $src_columns = array(
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant_phone.phone',
            'tbl_crm_participant_address.street',
            'tbl_crm_participant_address.postal',
            'tbl_crm_participant_address.state',
            'tbl_crm_participant_email.email',
            'tbl_crm_participant.updated'
        );
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;

                if ($orderBy == 'FullName') {
                    $orderBy = 'tbl_crm_participant.firstname';
                }
                if ($orderBy == 'latest_stage_name') {
                    $orderBy   = 'tbl_crm_participant.id';
                }
                if ($orderBy == 'date') {
                    $orderBy   = 'tbl_crm_participant.id';
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy   = 'tbl_crm_participant.id';
            $direction = 'DESC';
        }

        $where = '';
        $where = "tbl_crm_participant.archive = 0";
        $sWhere = $where;

        if (!empty($filter)) {
            // if ($filter->inactive) {
            //     $status_con = array(0, 1);
            // }

            if (!empty($filter->search)) {
                $this->db->group_start();

                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search);
                    } else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search);
                    }
                }

                $this->db->group_end();
            }

            if (!empty($filter->filterVal)) {
                $this->db->where('tbl_crm_participant.booking_status', $filter->filterVal);
            }

            if (!empty($filter->search_by)) {
                $this->db->where('tbl_crm_participant.action_status', $filter->search_by);
            }

        }

        $role_array = array();
        foreach($role as $value){
          array_push($role_array,$value->permission);
        }
        $select_column = array(
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant.status',
            'tbl_crm_participant_email.email',
            'concat(tbl_crm_participant.referral_firstname," ",tbl_crm_participant.referral_lastname) as ref_fullName',
            '(CASE
            WHEN tbl_crm_participant.intake_type =1 THEN "New"
            WHEN tbl_crm_participant.intake_type =2 THEN "Rejected"
            WHEN tbl_crm_participant.intake_type =3 THEN "Renewed"
            WHEN tbl_crm_participant.intake_type =4 THEN "Returning"
            WHEN tbl_crm_participant.intake_type =5 THEN "Modified"
            END) as intake_type',
            'tbl_crm_participant.referral_email',
            'tbl_crm_participant.referral_phone',
            'tbl_crm_participant.referral_org',
            'tbl_crm_participant.referral_relation',
            '(select concat(firstname," ",lastname) from tbl_member where id=tbl_crm_participant.assigned_to) as assigned_to',
            //'tbl_department.name as participant_department',
            'tbl_crm_participant_phone.phone',
            'tbl_crm_participant.gender,
             tbl_crm_participant_address.street,
             tbl_crm_participant_address.city,
             tbl_crm_participant_address.postal',
            'tbl_crm_participant_address.lat',
            'tbl_crm_participant.updated',
            'tbl_crm_participant_address.long',
            'tbl_crm_participant.booking_date',
            'tbl_crm_participant.booking_status',
            'tbl_crm_participant.action_status'
        );
        $dt_query      = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'crm_participant');

        $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_address.primary_address = 1', 'left');
        $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_email.primary_email = 1', 'left');
        $this->db->join('tbl_crm_participant_phone', 'tbl_crm_participant_phone.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_phone.primary_phone = 1', 'left');
        //  $this->db->join('tbl_state', 'tbl_state.id = tbl_crm_participant_address.state', 'left');
       // $this->db->join('tbl_department', 'tbl_department.id = tbl_crm_participant.oc_departments', 'left');

        $this->db->where_in('tbl_crm_participant.status', $status_con);

        if(in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array)){
            $this->db->where_in('tbl_crm_participant.assigned_to', $loginId);
        }
        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('tbl_crm_participant.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //        last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();

        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();

                // if (!empty($filter->incomplete)) {
                //     $percent = get_profile_complete('PARTICIPANT', $val->id);
                //     if ($percent == 100) {
                //         continue;
                //     }
                // }

                $row['id']                = $val->id;
                $row['ndis_num']          = $val->ndis_num;
                $row['FullName']          = $val->FullName;
                $row['gender']            = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                //   $row['street'] = $val->street . ', ' . $val->state . ', ' . $val->postal;
                $row['address']           = $val->street.' '.$val->city.' '.$val->postal;
                $row['phone']             = $val->phone;
                $row['email']             = $val->email;
                $row['status']            = isset($val->booking_status) && $val->booking_status == 1 ? 'Pending Contact' : 'Unassigned';
                $row['long']              = $val->long;
                $row['lat']               = $val->lat;
                $row['date']              = date('d-m-Y',strtotime($val->booking_date));
                $row['assigned_to']       = $val->assigned_to;
                $row['intake_type']       = $val->intake_type;
                $row['latest_stage_name'] = (($val->booking_status==4)?'In-Progress  : ':(($val->booking_status==2)?'Parked :': ($val->booking_status==5)?'Rejected :':'Successful :')). $this->get_latest_stage($val->id);
                $row['ref_name']          = $val->ref_fullName;
                $row['ref_email']         = $val->referral_email;
                $row['ref_phone']         = $val->referral_phone;
                $row['ref_org']           = $val->referral_org;
                $row['ref_relation']      = $val->referral_relation;
                $row['now']               = $this->intake_percent($val->id);
                $row['progress_count']    = $this->intake_percent($val->id)['level'];
                //$row['department']        = $val->participant_department;
                $updated                  = $val->updated;
                $row['updated']           = $this->get_time_ago(strtotime($updated));
                $row['action']            = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                $dataResult[]             = $row;
            }
        }
        $return = array(
            'count' => $dt_filtered_total,
            'data' => $dataResult
        );
        return $return;
    }

    function get_time_ago($time)
    {
        $time_difference = time() - $time;

        if ($time_difference < 1) {
            return 'less than 1 second ago';
        }
        $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($condition as $secs => $str) {
            $d = $time_difference / $secs;

            if ($d >= 1) {
                $t = round($d);
                return 'about ' . $t . ' ' . $str . ($t > 1 ? 's' : '') . ' ago';
            }
        }
    }

    function get_latest_stage($crm_participant_id)
    {

        $table     = 'crm_participant';
        $column    = "(select name from tbl_crm_stage where id=stage_status) as latest_stage_name";
         $where     = array(
             "id" => $crm_participant_id,
             "status" => 1
         );
        $orderby   = "id";
        $direction = "desc";
        $response  = $this->Basic_model->get_record_where_orderby($table, $column, $where, $orderby, $direction);
        if ($response) {
            $result = ($response[0]->latest_stage_name)?$response[0]->latest_stage_name:"NDIS Intake Participant Submission Information";
        } else {
            $result = '';
        }
        return $result;
    }
    public function prospective_participant_details($participantId)
    {
        if (!empty($participantId)) {
            $tbl_1         = TBL_PREFIX . 'crm_participant';
            $tbl_2         = TBL_PREFIX . 'crm_participant_phone';
            $tbl_3         = TBL_PREFIX . 'crm_participant_email';
            $tbl_4         = TBL_PREFIX . 'crm_participant_address';
            $tbl_5         = TBL_PREFIX . 'department';
            $tbl_6         = TBL_PREFIX . 'crm_participant_kin';
            $tbl_7         = TBL_PREFIX . 'crm_participant_stage';
            $tbl_8         = TBL_PREFIX . 'member';
            $tbl_9         = TBL_PREFIX . 'crm_participant_roster';
            $tbl_10        = TBL_PREFIX . 'crm_participant_shifts';
            $tbl_11        = TBL_PREFIX . 'crm_participant_docs';
            $tbl_12        = TBL_PREFIX . 'crm_ndis_plan';
            $select_column = array(
                $tbl_1 . '.*',
                'concat(' . $tbl_1 . '.preferredname," ",' . $tbl_1 . '.firstname," ",' . $tbl_1 . '.lastname) as FullName',
                'DATE_FORMAT('.$tbl_1.'.dob,"%d-%m-%Y") as dob',
                $tbl_2 . '.phone',
                $tbl_3 . '.crm_participant_id',
                $tbl_3 . '.email',
                $tbl_1 . '.assigned_to as assigned_id' ,
                $tbl_3 . '.primary_email',
                'concat(' . $tbl_1 . '.referral_firstname," ",' . $tbl_1 . '.referral_lastname) as ref_fullName',
                'a1.street as primary_address',
                'a1.postal as primary_address_postcode',
                'a1.postal as primary_address_city',
                'a2.street as secondary_address',
                'a1.address_type  as address_primary_type',
                'a2.address_type  as secondary_address_type',
				'a1.state',
                'a1.postal',
                'a1.city',
                $tbl_5 . '.name as participant_department',
                'concat(' . $tbl_6 . '.firstname," ",' . $tbl_6 . '.lastname) as kin_fullname',
                $tbl_6 . '.relation as kin_relation',
                $tbl_6 . '.phone as kin_phone',
                $tbl_6 . '.firstname as kin_firstname',
                $tbl_6 . '.lastname as kin_lastname',
                $tbl_6 . '.email as kin_email',
                $tbl_7 . '.stage_id',
                $tbl_7 . '.crm_participant_id',
                $tbl_7 . '.crm_member_id',
                $tbl_7 . '.status',
                '(select concat(firstname," ",lastname) from ' . $tbl_8 . ' where id=' . $tbl_1 . '.assigned_to) as assigned_to',

                //'b1.id  as hearing_file_id',
                //'b2.id  as ndis_file_id',
                //'b1.filename  as hearing_file_name',
                //'b2.filename  as ndis_file_name',
                //'b1.type  as hearing_type',
                //'b2.type  as ndis_type',
                $tbl_12 . '.manager_plan',
                $tbl_12 . '.manager_email',
                $tbl_12 . '.manager_address',
                $tbl_12 . '.state as manager_state',
                $tbl_12 . '.post_code as manager_postcode',

            );
            $sWhere        = array(
                $tbl_1 . '.id' => $participantId
            );
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2 . '.crm_participant_id = ' . $tbl_1 . '.id AND ' . $tbl_2 . '.primary_phone = 1', 'left');
            $this->db->join($tbl_3, $tbl_3 . '.crm_participant_id = ' . $tbl_1 . '.id AND ' . $tbl_3 . '.primary_email = 1', 'left');
            $this->db->join($tbl_4 . ' as a1', 'a1.crm_participant_id = ' . $tbl_1 . '.id AND a1.primary_address = 1', 'left');
            $this->db->join($tbl_4 . ' as a2', 'a2.crm_participant_id = ' . $tbl_1 . '.id AND a2.primary_address = 2', 'left');
            $this->db->join($tbl_5, $tbl_5 . '.id = ' . $tbl_1 . '.oc_departments', 'left');
            $this->db->join($tbl_6, $tbl_6 . '.crm_participant_id = ' . $tbl_1 . '.id', 'left');
            $this->db->join($tbl_7, $tbl_7 . '.crm_participant_id = ' . $tbl_1 . '.id', 'left');
            //$this->db->join($tbl_11 . ' as b1', 'b1.crm_participant_id = ' . $tbl_1 . '.id AND b1.type = 2', 'left');
            //$this->db->join($tbl_11 . ' as b2', 'b2.crm_participant_id = ' . $tbl_1 . '.id AND b2.type = 1', 'left');
            $this->db->join($tbl_12, $tbl_12 . '.crm_participant_id = ' . $tbl_1 . '.id', 'left');
            $this->db->where($sWhere);
            $query = $this->db->get();
            $data = array();
            $data = $query->row_array();

                $this->db->select(array('filename as name','title','type','id as docs_id',"(CASE when type=2 THEN 'hearing_type' WHEN type=1 THEN 'ndis_type' ELSE '' END ) as docs_type"),false);
                $this->db->from('tbl_crm_participant_docs');
                $this->db->where_in('type',['1','2']);
                $this->db->where('archive',0);
                $this->db->where('crm_participant_id',$participantId);
                $queryDoc =$this->db->get();
                $docsData = $queryDoc->num_rows()>0? $queryDoc->result_array():[];
                $docsNdisData = !empty($docsData) ? array_filter($docsData,function($val){
                    return $val['type']==1? true: false;
                }):[];
                $docsHearingData = !empty($docsData) ? array_filter($docsData,function($val){
                    return $val['type']==2? true: false;
                }):[];
                $data['ndis_file'] =  array_values($docsNdisData);
                $data['hearing_file'] = array_values($docsHearingData);
            $data['ability'] = $this->prospective_participant_ability($participantId);
            $data['fms_cms'] = $this->fms_cases($participantId);
            return $data;
        }
    }

    function shift_data($participantId)
    {
        $tbl_1         = TBL_PREFIX . 'crm_participant';
        $tbl_9         = TBL_PREFIX . 'crm_participant_roster';
        $tbl_10        = TBL_PREFIX . 'crm_participant_shifts';
        $select_column = array(
            'date('.$tbl_9 . '.start_date) as start_date',

            // $tbl_10 . '.crmRosterId',
        );
        $sWhere        = array(
            $tbl_9 . '.participantId' => $participantId
        );

        $this->db->select($select_column);
        $this->db->from($tbl_9);
         // $this->db->join($tbl_10, $tbl_10 . '.crmRosterId = ' . $tbl_9 . '.id', 'left');
        $this->db->where($sWhere);
        $query = $this->db->get();
           $data = array();
            $shift = array();
            $days = array('','Mon','Tue','Wed','Thu','Fri','Sat','Sun');
            $class = array('no_shift','am_shift','pm_shift','so_shift','na_shift');
            $s = array();
            foreach($query->result_array() as $value){
              $shift_details = array();
              $data = $query->row_array();



              // $shift['shift_requirement'] =  explode(',',$value['shift_requirement']);
            //   if(!empty($value['shift_day']) && !empty($value['shift_type']))
            //   {
              // $s[$value['shift_day']] = explode(',',$value['shift_type']) ;
            //
            //   }

            }
            $shift['shift_values'] = $s;
           return $shift;
    }
    public function prospective_participant_ability($participantId)
     {
         if (!empty($participantId)) {
             $tbl_1 = TBL_PREFIX . 'crm_participant';
             $tbl_2 = TBL_PREFIX . 'crm_participant_ability';
             $tbl_3 = TBL_PREFIX . 'crm_participant_disability';
             $tbl_4 = TBL_PREFIX . 'crm_participant_docs';
             $select_column = array(
                 $tbl_2 . '.*',
                 $tbl_3 . '.*',
                 $tbl_4 . '.*'
             );
             $sWhere        = array(
                 $tbl_1 . '.id' => $participantId
             );
             $this->db->select($select_column);
             $this->db->from($tbl_1);
             $this->db->join($tbl_2, $tbl_2 . '.crm_participant_id = ' . $tbl_1 . '.id ', 'left');
             $this->db->join($tbl_3, $tbl_3 . '.crm_participant_id = ' . $tbl_1 . '.id ', 'left');
             $this->db->join($tbl_4, $tbl_4 . '.crm_participant_id = ' . $tbl_1 . '.id ', 'left');
             $this->db->where($sWhere);
             $query      = $this->db->get();
             $dataResult = array();
             $dataResult = $query->row_array();
             $dataResult['languages_spoken'] = (!empty($dataResult['languages_spoken'])) ? explode(",", $dataResult['languages_spoken']) :[];
             $dataResult['require_assistance'] = (!empty($dataResult['require_assistance'])) ? explode(",", $dataResult['require_assistance']) :[];
             $dataResult['require_mobility'] = (!empty($dataResult['require_mobility'])) ? explode(",", $dataResult['require_mobility']) :[];
             $dataResult['ability_docs']  = $this->getDocsBasedOnType($participantId,3);
             $dataResult['disability_docs'] = $this->getDocsBasedOnType($participantId,4);
             return $dataResult;
         }
     }

     public function getDocsBasedOnType($participantId,$type){
       $table='crm_participant_docs';
       $where=array('crm_participant_id'=>$participantId,'type'=>$type,'archive'=>0);
       $result = $this->basic_model->get_record_where($table,array("CONCAT('".base_url()."/uploads/crmparticipant/".$participantId."/',filename) as filename",'filename as filepath','title'),$where);
       return $result;
     }


    public function create_intake_info($intakeInfo)
    {
        $arr_intake              = array();
        $arr_intake['firstname'] = $intakeInfo->getFirstname();
        $insert_query            = $this->db->insert(TBL_PREFIX . 'crm_participant', $arr_intake);
        $intake_id               = $this->db->insert_id();
        return $intake_id;

    }

    

    public function create_crm_participant_old($objparticipant,$action,$adminId)
    {
        $edit_participant_id= $objparticipant->getParticipantid();
        $arr_participant = array();
        $old_participant_id=0;
        $arr_participant['ndis_plan'] = $objparticipant->getNdisPlan();
        $participant_id = '';
        $arr_participant['firstname']      = $objparticipant->getFirstname();
        $arr_participant['other_relevant_plans'] = $objparticipant->getRelevantPlan();
        $arr_participant['lastname']       = $objparticipant->getLastname();
        $arr_participant['behavioural_support_plan']         = $objparticipant->getBehavioural();
        $arr_participant['dob']            = $objparticipant->getDob();
        $arr_participant['ndis_num']       = $objparticipant->getNdisNum();
        $arr_participant['medicare_num']   = $objparticipant->getMedicareNum();
        $arr_participant['marital_status']   = $objparticipant->getMaritalStatus();
        $arr_participant['action_status']  = 2;
        $arr_participant['booking_status'] = 4;
        if(($action!='edit'))
          $arr_participant['stage_status']   = 1;
        $departmentID                      = "";
        $assigned_to                       = '';
        $arr_participant['referral']    = 1;
        $old_participant_id = $objparticipant->getOldParticipantid();
        if(!empty($old_participant_id) || $old_participant_id!=0){
          $arr_participant['old_participant_id']  = $old_participant_id;
          $check_participant = $this->db->query("SELECT id FROM tbl_participant WHERE ndis_num=".$objparticipant->getNdisNum())->result();
          $arr_participant['intake_type'] = !empty($check_participant)?4:1;
        }
        $arr_participant['referral_relation']  = $objparticipant->getParticipantRelation();
        $arr_participant['preferredname'] = $objparticipant->getPreferredname();
        $arr_participant['referral_firstname'] = $objparticipant->getReferralFirstName();
        $arr_participant['referral_lastname']  = $objparticipant->getReferralLastName();
        $arr_participant['referral_email']     = $objparticipant->getReferralEmail();
        $arr_participant['referral_phone']     = $objparticipant->getReferralPhone();
        $arr_participant['referral_org']       = $objparticipant->getReferralOrg();


        $arr_participant['living_situation'] = $objparticipant->getLivingSituation();
        $arr_participant['aboriginal_tsi'] = $objparticipant->getAboriginalTsi();
        $arr_participant['oc_departments']   = $departmentID;
        $arr_participant['created']          = $objparticipant->getCreated();
        $arr_participant['status']           = $objparticipant->getStatus();
        $arr_participant['gender']           = $objparticipant->getGender();

        $arr_participant['booking_date']     = date($objparticipant->getCreated());
        $arr_participant['archive']          = 0;



         if($edit_participant_id !=null){
		   $this->edit_participant_log($arr_participant,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
           $this->update_participant_crm_address($edit_participant_id,$objparticipant->getCity(),$objparticipant->getAddress(),$objparticipant->getState(),$objparticipant->getPostcode(),$edit_participant_id,$arr_participant['firstname']." ".$arr_participant['lastname'],$adminId);

            $participant_id = $edit_participant_id;
            $where           = array(
                'tbl_crm_participant.id' => $participant_id
            );
            $res             = $this->Basic_model->update_records('crm_participant', $arr_participant, $where);

        }
        else{
            $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_participant', $arr_participant);
            $participant_id = $this->db->insert_id();
        }
        if(($old_participant_id)!=0){
          $res =  $this->Basic_model->update_records('crm_participant', array('status'=>0), array('id'=>$old_participant_id));
        }
        if ($participant_id > 0) {
            //Here to start insert all Emails
            $participant_email = array();
            $all_part_emails   = $objparticipant->getParticipantEmail();
            if (is_array($all_part_emails)) {
                foreach ($all_part_emails as $emails) {
                    $participant_email[] = array(
                        'crm_participant_id' => $participant_id,
                        'email' => $emails['email'],
                        'primary_email' => $emails['type']
                    );
                 }
                if($edit_participant_id !=null){
                    $where           = array(
                        'crm_participant_id' => $participant_id
                    );
				 $this->edit_participant_log($participant_email,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
                  $this->Basic_model->update_records('crm_participant_email', $participant_email, $where);

                }
                else{
                    $this->db->insert_batch(TBL_PREFIX . 'crm_participant_email', $participant_email);

                }
            }
           else{
            $participant_email[] = array(
                'crm_participant_id' => $participant_id,
                'email' => $all_part_emails,
                'primary_email' => 1
            );
             if($edit_participant_id !=null){
                $where           = array(
                    'crm_participant_id' => $participant_id
                );
				$this->edit_participant_log($participant_email,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
                $res             = $this->Basic_model->update_records('crm_participant_email', $participant_email[0], $where);
            }
            else{
                $this->db->insert_batch(TBL_PREFIX . 'crm_participant_email', $participant_email);

            }
          }
          $this->update_participant_crm_address($edit_participant_id,$objparticipant->getCity(),$objparticipant->getAddress(),$objparticipant->getState(),$objparticipant->getPostcode(),$participant_id,$arr_participant['firstname']." ".$arr_participant['lastname'],$adminId);


            //Here to start insert all Phone no.
            $participant_phone = array();
            $all_part_phone    = $objparticipant->getParticipantPhone();
            if (is_array($all_part_phone)) {
                foreach ($all_part_phone as $phone) {
                    $participant_phone[] = array(
                        'crm_participant_id' => $participant_id,
                        'phone' => $phone['phone'],
                        'primary_phone' => $phone['type']
                    );
                }
                if($edit_participant_id !=null){
                    $where           = array(
                        'crm_participant_id' => $participant_id
                    );
				  $this->edit_participant_log($participant_phone,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
                  $this->Basic_model->update_records('crm_participant_phone', $participant_phone, $where);
                }
                else{
                    $this->db->insert_batch(TBL_PREFIX . 'crm_participant_phone', $participant_phone);

                }
            }
          else{
            $participant_phone[] = array(
                'crm_participant_id' => $participant_id,
                'phone' => $all_part_phone,
                'primary_phone' => 1
            );
             if($edit_participant_id !=null){
                $where           = array(
                    'crm_participant_id' => $participant_id
                );
			  $this->edit_participant_log($participant_phone,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
              $this->Basic_model->update_records('crm_participant_phone', $participant_phone[0], $where);
            }
            else{
                $this->db->insert_batch(TBL_PREFIX . 'crm_participant_phone', $participant_phone);

            }
          }
		            // for crm_ndis_plan
					$ndis_plan=array();
            if($objparticipant->getNdisPlan()==3){

                $ndis_plan=array();
                $ndis_plan['manager_plan']=$objparticipant->getProvidePlan();
                $ndis_plan['manager_email']=$objparticipant->getProvideEmail();
                $ndis_plan['manager_address']=$objparticipant->getProvideAddress();
                $ndis_plan['state']=$objparticipant->getProvideState();
                $ndis_plan['post_code']=$objparticipant->getProvidePostcode();
                $where=array('crm_participant_id'=>$participant_id);
                $ndis_plan_id  = $this->Basic_model->get_record_where('crm_ndis_plan','id',$where);
                if($ndis_plan_id)
                {
                    $this->edit_participant_log($ndis_plan,$edit_participant_id,$adminId,$arr_participant['firstname']." ".$arr_participant['lastname']);
                    $this->Basic_model->update_records('crm_ndis_plan', $ndis_plan, $where);
                }
                else{
                    $ndis_plan['crm_participant_id']=$participant_id;
                    $this->db->insert(TBL_PREFIX . 'crm_ndis_plan', $ndis_plan);
                }
            }
            // for crm_participant_schedule_task
            //     $participant_schedule = array('task_name' => "New participant",'crm_participant_id' => $participant_id, 'priority' => 1, 'assign_to' => $assigned_to,"parent_id"=>0,"note"=>"","task_status"=>2);
            //     $this->db->insert(TBL_PREFIX . 'crm_participant_schedule_task', $participant_schedule);
            // }
            if(empty($action)){
              $tbl_2    = TBL_PREFIX . 'crm_participant';
              $assigned_counts = $this->assign_participant();
              $min             = min($assigned_counts);
              $key             = array_search($min, $assigned_counts);
              $where           = array(
                  $tbl_2 . ".id" => $participant_id
              );
              $data            = array(
                  $tbl_2 . ".assigned_to" => $key
              );
              $res             = $this->Basic_model->update_records('crm_participant', $data, $where);


            $arr_stage_participant['stage_id'] = 4;
            $arr_stage_participant['crm_participant_id']  = $participant_id ;
            $arr_stage_participant['crm_member_id'] = $key;
            $arr_stage_participant['status'] = 3;
            $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_participant_stage', $arr_stage_participant);
          }
            return $participant_id;
        }
    }
	function edit_participant_log($edit_data,$edit_participant_id,$adminId,$participant_name){



      foreach($edit_data as $key=>$participant){
        if($key !='medicare_num' && $key !='created' && $key !='booking_date')
        {

        $this->db->select('*');
        $this->db->from('tbl_crm_participant');
        $this->db->join('tbl_crm_ndis_plan', 'tbl_crm_participant.id = tbl_crm_ndis_plan.crm_participant_id', 'left');
        $this->db->join('tbl_crm_participant_phone', 'tbl_crm_participant.id = tbl_crm_participant_phone.crm_participant_id', 'left');
        $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant.id = tbl_crm_participant_email.crm_participant_id', 'left');
        $this->db->like($key, $participant);
        $this->db->like("tbl_crm_participant.id", $edit_participant_id);
        $participant_tbl_check  =  $this->db->get()->result_array();
        //echo $this->db->last_query();
          if(count($participant_tbl_check)<=0)
          {
              $changed_data='';
              switch($key)
              {
                  case 'firstname':
                  $changed_data='First Name';
                  break;
                  case 'other_relevant_plans':
                  $changed_data='Other Relavent Plan';
                  break;
                  case 'lastname':
                  $changed_data='Last Name';
                  break;
                  case 'behavioural_support_plan':
                  $changed_data='Behavioural Support';
                  break;
                  case 'dob':
                  $changed_data='Date Of Birth';
                  break;
                  case 'ndis_num':
                  $changed_data='Ndis Number';
                  break;
                  case 'marital_status':
                  $changed_data='Merital Status';
                  break;
                  case 'referral_relation':
                  $changed_data='Referral Relation';
                  break;
                  case 'preferredname':
                  $changed_data='Preferred Name';
                  break;
                  case 'referral_firstname':
                  $changed_data='Referral First Name';
                  break;
                  case 'referral_lastname':
                  $changed_data='Referral Last Name';
                  break;
                  case 'referral_email':
                  $changed_data='Referral Email';
                  break;
                  case 'referral_phone':
                  $changed_data='Referral Phone';
                  break;
                  case 'referral_org':
                  $changed_data='Referral Organisation';
                  break;
                  case 'living_situation':
                  $changed_data='Living Situation';
                  break;
                  case 'aboriginal_tsi':
                  $changed_data='Aboriginal Tsi';
                  break;
                  case 'oc_departments':
                  $changed_data='Department';
                  break;
                  case 'email':
                  $changed_data='Email Id';
                  break;
                  case 'phone':
                  $changed_data='Phone number';
                  break;
                  case 'manager_plan':
                  $changed_data='Ndis Plan';
                  break;
                  case 'manager_email':
                  $changed_data='Ndis Email';
                  break;
                  case 'manager_address':
                  $changed_data='Ndis Address';
                  break;
                  case 'state':
                  $changed_data='Ndis State';
                  break;
                  case 'post_code':
                  $changed_data='Ndis Postal Code';
                  break;
                  case 'gender':
                  $changed_data='Gender';
                  break;
              }

              $participant_data['companyId'] = 1;
              $participant_data['userId']  = $edit_participant_id;
              $participant_data['module'] = 10;
              $participant_data['title'] = $changed_data.' field value changed in ('.$participant_name.') at participant details';
              $participant_data['description'] = $changed_data.' field value changed in ('.$participant_name.') at participant details';
              $participant_data['created_by']  = $adminId;
			  $participant_data['created']  = DATE_TIME;
              $participant_data['created_type'] = $adminId;
              $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_logs', $participant_data);


          }
        }

      }

    }
    function assign_participant(){
      $tbl_1 = TBL_PREFIX . 'member';
      $tbl_2 = TBL_PREFIX . 'crm_participant';
      $tbl_3 = TBL_PREFIX . 'crm_staff';
      $dt_query = $this->db->select(array($tbl_1.'.id as ocs_id',$tbl_1.'.status',"CONCAT(tbl_member.firstname,' ',tbl_member.lastname) AS name","count(tbl_member.id) as count"));

      $this->db->from($tbl_3);
      $this->db->join('tbl_member', 'tbl_crm_staff.admin_id = tbl_member.id', 'left');
      $this->db->join('tbl_crm_participant', 'tbl_crm_participant.assigned_to = tbl_member.id', 'left');
      $this->db->where($tbl_1.'.archive=',"0");
      $this->db->where($tbl_3.'.status=',"1");
      $this->db->group_by($tbl_1.'.id');
      $data = $this->db->get();
      //echo   $this->CI->db->last_query();

      $assigned_counts= array();
      foreach($data->result_array() as $value){
        $assigned_counts[$value['ocs_id']] =  $value['count'];

      }
      return $assigned_counts;
    }


    function update_crm_kin_details($participant_crm_kin, $participant_crm_id)
    {
        $previous_kin = json_decode(json_encode($this->get_participant_crm_kin($participant_crm_id)), true);
        $new_kin      = json_decode(json_encode($participant_crm_kin), true);

        $previous_kin_ids = array_column($previous_kin, 'id');

        if (!empty($new_kin)) {
            foreach ($new_kin as $key => $val) {
                if (!empty($val['id'])) {
                    if (in_array($val['id'], $previous_kin_ids)) {

                        $key = array_search($val['id'], $previous_kin_ids);

                        // those value on done action remove from array
                        unset($previous_kin[$key]);

                        $update_kin = $this->mapping_kin_array($val, $participant_crm_id);
                        $this->basic_model->update_records('crm_participant_kin', $update_kin, $where = array(
                            'id' => $val['id']
                        ));
                    }
                } else {

                    // collect new data
                    $inset_kin[] = $this->mapping_kin_array($val, $participant_crm_id);
                }
            }

            if (!empty($inset_kin)) {
                $this->basic_model->insert_records('crm_participant_kin', $inset_kin, true);
            }

            // archive those member who remove
            if (!empty($previous_kin)) {
                foreach ($previous_kin as $val) {
                    $archive_kin = array(
                        'archive' => 1
                    );
                    $this->basic_model->update_records('crm_participant_kin', $archive_kin, $where = array(
                        'id' => $val['id']
                    ));
                }
            }
        }
    }

    function mapping_kin_array($data, $participant_crm_id)
    {
        $update_kin = array(
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'relation' => $data['relation'],
            'primary_kin' => (!empty($data['primary_kin']) && ($data['primary_kin'] == 1) ? 1 : 2),
            'crm_participant_id' => $participant_crm_id
        );

        return $update_kin;
    }
    function update_participant_crm_address($edit_participant,$city,$participant_crm_data,$state,$postcode,$crm_participant_id,$participant_name,$adminId)
    {
            $participant_data=array();
            $this->db->select('*');
            $this->db->from('tbl_crm_participant_address');
            $this->db->where('street',$participant_crm_data);
            $this->db->where('crm_participant_id',$crm_participant_id);
            $street_check = $this->db->get()->result_array();
            $this->db->from('tbl_crm_participant_address');
            $this->db->where('state',$state);
            $this->db->where('crm_participant_id',$crm_participant_id);
            $state_check = $this->db->get()->result_array();
            $this->db->from('tbl_crm_participant_address');
            $this->db->where('postal',$postcode);
            $this->db->where('crm_participant_id',$crm_participant_id);
            $data = $this->db->get()->result_array();
            if(count($data)<=0 || count($street_check)<=0 || count($state_check)<=0)
            {

                $participant_data['companyId'] = 1;
                $participant_data['userId']  = $crm_participant_id;
                $participant_data['module'] = 10;
                $participant_data['title'] = 'Address field value changed in ('.$participant_name.') at participant details';
                $participant_data['description'] = 'Address  field value changed in ('.$participant_name.') at participant details';
                $participant_data['created_by']  = $adminId;
				$participant_data['created']  = DATE_TIME;
                $participant_data['created_type'] = $adminId;
                if(isset($edit_participant))
                {
                    $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_logs', $participant_data);
                }
            }

        $this->db->delete(TBL_PREFIX . 'crm_participant_address', $where = array(
            'crm_participant_id' => $crm_participant_id
        ));

        $latLong = getLatLong($participant_crm_data);

          $addrees_data=array(
              "lat"=>$latLong['lat'],
              "long"=>$latLong['long'],
              "street"=>$participant_crm_data,
              "crm_participant_id"=>$crm_participant_id,
              "city"=>isset($city->value)?$city->value:$city,
              "state"=>$state,
              "postal"=>$postcode,
              "primary_address"=>1
          );

            $this->basic_model->insert_records('crm_participant_address', $addrees_data);

    }
    function get_participant_crm_kin($participantId)
    {
        $where  = array(
            'crm_participant_id' => $participantId,
            'archive' => 0
        );
        $colown = array(
            'id',
            'relation',
            'phone',
            'email',
            'primary_kin',
            'firstname',
            'lastname',
            "CONCAT(firstname, ' ', lastname) AS kinfullname"
        );
        return $this->basic_model->get_record_where('crm_participant_kin', $colown, $where);
    }
    public function participant_shift($data)
    {
        $start_date        = $data['start_date'];
        $shiftsvalues      = (array) $data['shiftsvalues'];
        $participant_id    = $data['crm_participant_id'];
        $roster_data       = array(
            'participantId' => $participant_id,
            'start_date' => $start_date,
            'status' => 2,
            'archived' => 0
        );
        $this->db->insert(TBL_PREFIX . 'crm_participant_roster', $roster_data);
        $roster_id = $this->db->insert_id();
        foreach ($shiftsvalues as $key => $value) {

            $shift_data = array(
                // 'crmRosterId' => $roster_id,
                // 'shift_type' => implode(',', $value),
                'archived' => 0
            );
            $result     = $this->db->insert(TBL_PREFIX . 'crm_participant_shifts', $shift_data);
        }

    }
    public function participant_ability_disability_crm_update($data,$adminId,$edit_participant)
    {
        $require_assistance = is_array($data['require_assistance'])?implode(',', $data['require_assistance']):$data['require_assistance'];
        $require_mobility   = is_array($data['require_mobility'])?implode(',', $data['require_mobility']):$data['require_mobility'];
        $languages_spoken   = is_array($data['languages_spoken'])?implode(',', $data['languages_spoken']):$data['languages_spoken'];

        $this->db->select("CONCAT(firstname,' ',lastname) as name");
        $this->db->from('tbl_crm_participant');
        $this->db->where('id', $data['crm_participant_id']);
        $name_data  =  $this->db->get()->result_array();
        $participant_ability_crm = array(
            'cognitive_level' => $data['cognitive_level'],
            'communication' => $data['communication'],
            'hearing_interpreter' => $data['hearing_interpreter'],
            'language_interpreter' => $data['language_interpreter'],
            'languages_spoken' => $data['language_interpreter']==1? $languages_spoken:'',
            'linguistic_diverse' => $data['linguistic_diverse'],
            'require_assistance' => $require_assistance,
            'require_mobility' => $require_mobility,
             'languages_spoken_other' =>$data['languages_spoken_other'],
             'require_mobility_other' =>$data['require_mobility_other'],
             'require_assistance_other' =>$data['require_assistance_other']
            );
            $changed_data='';
            $abiity_data='';
            $participant_data=[];
            foreach($participant_ability_crm as $key=>$participant){


                $this->db->select('*');
                $this->db->from('tbl_crm_participant_ability');
                $this->db->join('tbl_crm_participant', 'tbl_crm_participant.id = tbl_crm_participant_ability.crm_participant_id', 'left');
                $this->db->like($key, $participant);
                $this->db->like('tbl_crm_participant.id', $data['crm_participant_id']);
                $participant_tbl_check  =  $this->db->get()->result_array();

                  if(count($participant_tbl_check)<=0)
                  {
                      $changed_data='';
                      switch($key)
                      {
                        case 'cognitive_level':
                        $changed_data='Cognitive Level';
                        break;
                        case 'communication':
                        $changed_data='Communication';
                        break;
                        case 'hearing_interpreter':
                        $changed_data='Hearing Interpreter';
                        break;
                        case 'language_interpreter':
                        $changed_data='Behavioural Support';
                        break;
                        case 'languages_spoken':
                        $changed_data='Languages Spoken';
                        break;
                        case 'linguistic_diverse':
                        $changed_data='Linguistic Diverse';
                        break;
                        case 'require_assistance':
                        $changed_data='Require Assistance';
                        break;
                        case 'require_mobility':
                        $changed_data='Require Mobility';
                        break;
                        case 'languages_spoken_other':
                        $changed_data='Languages Spoken Other';
                        break;
                        case 'require_mobility_other':
                        $changed_data='Require Mobility Other';
                        break;
                        case 'require_assistance_other':
                        $changed_data='Require Assistance Other';
                        break;

                      }

                    $participant_data['companyId'] = 1;
                    $participant_data['userId']  = $data['crm_participant_id'];
                    $participant_data['module'] = 10;
                    $participant_data['title'] =  $changed_data.' field value changed in ('.$name_data[0]['name'].') at participant details';
                    $participant_data['description'] =  $changed_data.' field value changed in ('.$name_data[0]['name'].') at participant details';
                    $participant_data['created_by']  = $adminId;
					$participant_data['created']  = DATE_TIME;
                    $participant_data['created_type'] = $adminId;
                        if(isset($edit_participant))
                        {
                            $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_logs', $participant_data);
                        }
                    }
                }



        $abiity_data             = $this->db->query("select cognitive_level from tbl_crm_participant_ability
        where crm_participant_id=" . $data['crm_participant_id']);


        if (!empty($abiity_data->result_array())) {
            $this->db->where($where = array(
                'crm_participant_id' => $data['crm_participant_id']
            ));

        $ability = $this->db->update(TBL_PREFIX . 'crm_participant_ability', $participant_ability_crm);
        } else {
            $participant_ability_crm['crm_participant_id'] = $data['crm_participant_id'];
            $ability                                       = $this->db->insert(TBL_PREFIX . 'crm_participant_ability', $participant_ability_crm);
        }
        $participant_disability_crm = array(
            'other_relevant_information' => $data['other_relevant_conformation'],
            'secondary_fomal_diagnosis_desc' => $data['secondary_fomal_diagnosis_desc'],
            'primary_fomal_diagnosis_desc' => $data['primary_fomal_diagnosis_desc'],
            'legal_issues' => $data['legal_issues']
        );
        foreach($participant_disability_crm as $key=>$participant){


            $this->db->select('*');
            $this->db->from('tbl_crm_participant_disability');
            $this->db->join('tbl_crm_participant', 'tbl_crm_participant.id = tbl_crm_participant_disability.crm_participant_id', 'left');
            $this->db->like($key, $participant);
            $this->db->like('tbl_crm_participant.id', $data['crm_participant_id']);
            $participant_tbl_check  =  $this->db->get()->result_array();

              if(count($participant_tbl_check)<=0)
              {
                  $changed_data='';
                  switch($key)
                  {

                    case 'primary_fomal_diagnosis_desc':
                    $changed_data='Primary Diagnosis';
                    break;
                    case 'secondary_fomal_diagnosis_desc':
                    $changed_data='Secondary Diagnosis';
                    break;
                    case 'other_relevant_information':
                    $changed_data='Other Relevant Confirmation';
                    break;
                    case 'legal_issues':
                    $changed_data='Legal Issues';
                    break;

                  }

                $participant_data['companyId'] = 1;
                $participant_data['userId']  = $data['crm_participant_id'];
                $participant_data['module'] = 10;
                $participant_data['title'] =  $changed_data.' field value changed in ('.$name_data[0]['name'].') at participant details';
                $participant_data['description'] =  $changed_data.' field value changed in ('.$name_data[0]['name'].') at participant details';
                $participant_data['created_by']  = $adminId;
				$participant_data['created']  = DATE_TIME;
                $participant_data['created_type'] = $adminId;
                    if(isset($edit_participant))
                    {
                      $insert_query   = $this->db->insert(TBL_PREFIX . 'crm_logs', $participant_data);
                    }
                }
            }
        $disability_data            = $this->db->query("select * from tbl_crm_participant_disability where crm_participant_id=" . $data['crm_participant_id']);
        if (!empty($disability_data->result_array())) {
            $this->db->where($where = array(
                'crm_participant_id' => $data['crm_participant_id']
            ));
            $disability = $this->db->update(TBL_PREFIX . 'crm_participant_disability', $participant_disability_crm);
        } else {
            $participant_disability_crm['crm_participant_id'] = $data['crm_participant_id'];
            $disability                                       = $this->db->insert(TBL_PREFIX . 'crm_participant_disability', $participant_disability_crm);
        }

        if ($ability && $disability)
            return true;
        else
            return false;
    }
    public function intake_percent($crmParticipantId)
 {
     $table         = TBL_PREFIX . 'crm_participant';
     $column        = $table . ".stage_id";
     $tbl_2         = TBL_PREFIX . "crm_stage";
     $tbl_3         = TBL_PREFIX. "crm_participant_stage";
     $select_column = array($tbl_2 . ".level",$tbl_2. ".status");
     $this->db->select($select_column);
     $this->db->from($tbl_2);
     $this->db->join($table, $table . '.stage_status = ' . $tbl_2 . '.id', 'left');
     $where = array(
         $table . ".id" => $crmParticipantId
     );
     $this->db->where($where);
     $response          = $this->db->get()->row_array();

     if(!empty($response) && ($response['status'] == 2 || $response['status'] == 4))
      $response['level'] = (int) (($response['level']) / 0.09);
     else{
       $where2 = array($tbl_3 . ".crm_participant_id" => $crmParticipantId,$tbl_3 . ".status" => 1);
       $this->db->select_max($tbl_2.'.id','id');
       $this->db->from($tbl_3);
       $this->db->join($tbl_2, $tbl_3 . '.stage_id = ' . $tbl_2 . '.id', 'left');
       $this->db->where($where2);
       $res         = $this->db->get()->row_array();
       if(!empty($res['id'])){
        $result  = $this->basic_model->get_row("crm_stage",array('level'),array('id'=>$res['id']));
       $response['level'] = (int) (($result->level) / 0.09);
       $response['status'] = $res['id'];}
       else {
         $response['level'] = (int) (1 / 0.09);
         $response['status'] = 1;
       }
     }
     return $response;
 }

    public function set_participant_state($crmParticipantId, $state)
    {
        $tbl_1         = TBL_PREFIX . 'crm_participant_stage';
        $tbl_2         = 'crm_participant';
        $tbl_3         = 'participant';
        $select_column = array(
            $tbl_1 . ".stage_id",
            $tbl_1 . ".status"
        );
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $where = array(
            $tbl_1 . ".crm_participant_id" => $crmParticipantId
        );
        $this->db->where($where);
        $this->db->order_by($tbl_1 . '.id', 'DESC');
        $this->db->limit(1);
        $response = $this->db->get()->row_array();
        $data     = array();
        if (!empty($response)) {
            switch ($state) {
                case '4':
                    if ($response['status'] == 1 && $response['stage_id'] == '11') {
                        $update = array(
                            "status" => 4
                        );
                        $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                            'id' => $crmParticipantId
                        ));
                        if (!empty($result)) {
                            $this->db->select('*');
                            $this->db->from(TBL_PREFIX . $tbl_2);
                            $where = array(
                                TBL_PREFIX . $tbl_2 . ".id" => $crmParticipantId
                            );
                            $this->db->where($where);
                            $objparticipant                        = $this->db->get()->row_array();
                            $objparticipant                        = (object) $objparticipant;
                            $arr_participant                       = array();
                            $arr_participant['username']           = $objparticipant->username;
                            $arr_participant['firstname']          = $objparticipant->firstname;
                            $arr_participant['middlename']         = $objparticipant->middlename;
                            $arr_participant['lastname']           = $objparticipant->lastname;
                            $arr_participant['gender']             = $objparticipant->gender;
                            $arr_participant['dob']                = $objparticipant->dob;
                            $arr_participant['ndis_num']           = $objparticipant->ndis_num;
                            $arr_participant['medicare_num']       = $objparticipant->medicare_num;
                            $arr_participant['referral']           = $objparticipant->referral;
                            $arr_participant['preferredname']      = $objparticipant->preferredname;
                            $arr_participant['relation']           = $objparticipant->relation;
                            $arr_participant['referral_firstname'] = $objparticipant->referral_firstname;
                            $arr_participant['referral_lastname']  = $objparticipant->referral_lastname;
                            $arr_participant['referral_email']     = $objparticipant->referral_email;
                            $arr_participant['referral_phone']     = $objparticipant->referral_phone;
                            $arr_participant['living_situation']   = $objparticipant->living_situation;
                            $arr_participant['aboriginal_tsi']     = $objparticipant->aboriginal_tsi;
                            $arr_participant['oc_departments']     = $objparticipant->oc_departments;
                            $arr_participant['created']            = date("Y-m-d H:i:s");
                            $arr_participant['status']             = '1';
                            $arr_participant['archive']            = 0;
                            $arr_participant['houseId']            = $objparticipant->houseId;
                            $insert_query                          = $this->db->insert(TBL_PREFIX . $tbl_3, $arr_participant);
                            $participant_id                        = $this->db->insert_id();
                            if ($participant_id > 0) {
                                $data = array(
                                    'msg' => "Participant state changed to Active"
                                );
                            } else {
                                $data = array(
                                    'msg' => "Cannot change state"
                                );
                            }
                        }
                    } else {
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    }
                    break;
                case '3':
                    $update = array(
                        "status" => 3
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Rejected"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                case '2':
                    $update = array(
                        "status" => 2
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Parked"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                case '1':
                    $update = array(
                        "status" => 1
                    );
                    $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                        'id' => $crmParticipantId
                    ));
                    if (!empty($result)) {
                        $data = array(
                            'msg' => "Participant state changed to Prospective"
                        );
                    } else
                        $data = array(
                            'msg' => "Cannot change state"
                        );
                    break;
                default:
                    break;
            }
            return $data;
        }
    }
    public function participant_stage_docs($crmParticipantId)
    {
        $table    = 'crm_participant_docs';
        $column   = "*";
        $where    = array(
            "crm_participant_id" => $crmParticipantId,
            "archive" => 0
        );
        $response = $this->Basic_model->get_record_where($table, $column, $where);
        return $response;
    }

    public function fms_cases($crmParticipantid)
    {
      $table    = 'crm_participant';
      $column   = "ndis_num";
      $where    = array(
          "id" => $crmParticipantid,
      );
      $result = $this->Basic_model->get_record_where($table, $column, $where);
      if(!empty($result)){
        $tbl_1         = TBL_PREFIX . 'participant';
        $tbl_2         = TBL_PREFIX . 'fms_case_against_detail';
        $tbl_3         = TBL_PREFIX . 'fms_case';
        $tbl_4         = TBL_PREFIX . 'fms_case_notes';
        $tbl_5         = TBL_PREFIX . 'fms_case_category';
        $tbl_6         = TBL_PREFIX . 'fms_case_all_category';
        $select_column = array(
            $tbl_1 . '.id',
            $tbl_2 . '.caseId',
            $tbl_2 . '.created',
            $tbl_4 . '.title',
            $tbl_6 . '.name',
            $tbl_4 . '.description'
        );
        $sWhere        = array(
            $tbl_1 . '.ndis_num' => $result[0]->ndis_num
        );
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, $tbl_2 . '.against_by = ' . $tbl_1 . '.id AND ' . $tbl_2 . '.against_category = 3', 'left');
        $this->db->join($tbl_4, $tbl_4 . '.caseId = ' . $tbl_2 . '.caseId ', 'left');
        $this->db->join($tbl_5, $tbl_5 . '.caseId = ' . $tbl_2 . '.caseId ', 'left');
        $this->db->join($tbl_6, $tbl_6 . '.id = ' . $tbl_5 . '.categoryId ', 'left');
        $this->db->where($sWhere);
        $query = $this->db->get();
        return $query->result_array();
      }
    }


    public function participant_stage_docs2($crmParticipantId)
    {
        $table    = 'crm_participant_docs';
        $column   = "*";
        $where    = array(
            "crm_participant_id" => $crmParticipantId,
            "archive" => 0
        );
        $response = $this->Basic_model->get_record_where($table, $column, $where);
        return $response;
    }

    public function all_participant_docs($crmParticipantId,$type)
    {
     $select_column = "id,crm_participant_id,filename,title,type";
     $where = array("crm_participant_id" => $crmParticipantId,  "archive" => 0);
     $response = $this->basic_model->get_record_where('crm_participant_docs', $select_column, $where);
     $responses= array();
     $row= array();
     foreach($response as $key => $val){
       $row['crm_participant_id']  = $val->crm_participant_id;
       $row['file_path'] = $val->filename;
       $row['file_url']  = base_url().'uploads/crmparticipant/'.$val->crm_participant_id.'/'.$val->filename;
       $row['id']  = $val->id;
       $row['title']  = $val->title;
       $row['type_id'] = $val->type;
       $row['is_active'] = false;
       $row['table_name'] = 'crm_participant_docs';
       $responses[] = $row;
     }
     return $responses;
   }

   public function all_participant_docs_by_category($crmParticipantId,$type)
   {
    $select_column = "id,crm_participant_id,filename,title,CASE
    WHEN type=1 THEN 'NDIS Plan'
    WHEN type=2 THEN 'Behavioral Support Plan'
    WHEN type=5 THEN 'Other Relevant Plan'
    WHEN type=6 THEN 'Service Agreement Document'
    WHEN type=7 THEN 'Funding Consent'
    WHEN type=8 THEN 'Final Service Agreement Document'
    WHEN type=9 THEN 'Other Attachments'
    END as type,type as type_id";
    switch ($type) {
      case '0':
        $where = array("crm_participant_id" => $crmParticipantId,   "archive" => 0);
        break;
      case '123':
          $where = array("crm_participant_id" => $crmParticipantId,  "archive" => 1);
          break;
      default:
       $where = array("crm_participant_id" => $crmParticipantId, 'type'=>$type,  "archive" => 0);
        break;
    }

    // if($type!='0'){
    //    $where = array("crm_participant_id" => $crmParticipantId, 'type'=>$type,  "archive" => 0);
    // } else
    //    if($type=='123'){
    //      $where = array("crm_participant_id" => $crmParticipantId,  "archive" => 1);
    //    }
    //     else  {
    // $where = array("crm_participant_id" => $crmParticipantId,  "archive" => 0);
    //   }

    $response = $this->basic_model->get_record_where('crm_participant_docs', $select_column, $where);
    $responses= array();
    $row= array();
    foreach($response as $key => $val){
      $row['crm_participant_id']  = $val->crm_participant_id;
      $row['file_path'] = $val->filename;
      $row['file_url']  = base_url().'uploads/crmparticipant/'.$val->crm_participant_id.'/'.$val->filename;
      $row['id']  = $val->id;
      $row['title']  = $val->title;
      $row['type_id'] = $val->type_id;
      $row['type'] = $val->type;
      $row['is_active'] = false;
      $row['table_name'] = 'crm_participant_docs';
      $responses[] = $row;
    }
    return $responses;
  }

    public function participant_docs($crmParticipantId)
   {
       $table    = 'crm_participant_docs';
       $column   = "id,crm_participant_id,type,title,filename as file_path";
       $where    = array(
           "crm_participant_id" => $crmParticipantId,
           "archive" => 0
       );
       $return = array();
       $docs = array();
       $response = $this->Basic_model->get_record_where($table, $column, $where);
       if(!empty($response)){
         foreach ($response as $key => $value) {
           switch ($value->type) {
             case '1':
                $docs['ndis'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
             case '2':
               $docs['behavioural'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
             case '5':
               $docs['other_relevant_plan'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
             case '6':
               $docs['service_agreement'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
             case '7':
               $docs['funding_consent'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
             case '8':
               $docs['final_service_agreement'] = array('file_path'=>$value->file_path,'id'=>$value->id,'is_active'=>false);
               break;
           }
         }
       }
       $return['data'] = $docs;
       $return['docs'] = $response;
       return $return;
   }


   function get_participant_shift_related_information($participantId) {
       $tbl_participant = TBL_PREFIX . 'crm_participant';
       $tbl_participant_address = TBL_PREFIX . 'crm_participant_address';
       $tbl_participant_email = TBL_PREFIX . 'crm_participant_email';
       $tbl_participant_phone = TBL_PREFIX . 'crm_participant_phone';
       $this->db->select(array($tbl_participant_address . '.street as address', $tbl_participant_address . '.city as suburb', $tbl_participant_address . '.postal', $tbl_participant_address . '.state', $tbl_participant_address . '.lat', $tbl_participant_address . '.long'));
       $this->db->select(array($tbl_participant . '.firstname', $tbl_participant . '.lastname', $tbl_participant . '.prefer_contact'));
       $this->db->select(array($tbl_participant_email . '.email', $tbl_participant_phone . '.phone'));
       $this->db->from($tbl_participant);
       $this->db->join($tbl_participant_address, $tbl_participant_address . '.crm_participant_id = ' . $tbl_participant . '.id', 'inner');
       $this->db->join($tbl_participant_email, $tbl_participant_email . '.crm_participant_id = ' . $tbl_participant . '.id AND ' . $tbl_participant_email . '.primary_email = 1', 'inner');
       $this->db->join($tbl_participant_phone, $tbl_participant_phone . '.crm_participant_id = ' . $tbl_participant . '.id AND ' . $tbl_participant_phone . '.primary_phone = 1', 'inner');
       $this->db->where($tbl_participant . '.id', $participantId);
       $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       $response = $query->row();
       // $response->requirement = $this->basic_model->get_record_where('participant_assistance', array('assistanceId'), $where = array('participantId' => $participantId, 'type' => 'assistance'));
       return $response;
   }

   public function get_roster_data($rosterId) {
       $tbl_participant_roster_data = 'tbl_crm_participant_roster_data';
       $colown_r = array('week_day', 'start_time', 'end_time', 'week_number');
       $this->db->select($colown_r);
       $this->db->from($tbl_participant_roster_data);
       $this->db->where(array('rosterId' => $rosterId));
       $query = $this->db->get();
       return $query->result_array();
   }

   public function get_count_shift_graph($view_by) {
       $tb1 = TBL_PREFIX . 'crm_participant_shifts';
       $this->db->from($tb1);
       $this->db->where_in('status', array(1, 2, 4, 7));
       if ($view_by == 'month') {
           $where['MONTH(created)'] = date('m');
           $this->db->where($where);
       } elseif ($view_by == 'week') {
           $this->db->where('shift_date BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()');
       } else {
           $this->db->where('shift_date', date("Y-m-d"));
       }
       $query = $this->db->get();
       $result['sub_count'] = $query->num_rows();
//        last_query();
       // get all shift count
       $this->db->from($tb1);
       $this->db->where_in('status', array(1, 2, 4, 7));
       $query = $this->db->get();
       $result['all_shift'] = $query->num_rows();
       return $result;
   }

   public function get_count_filled_shift($view_by, $filled_type) {
       $tb1 = TBL_PREFIX . 'shift';
       $this->db->from($tb1);
       $this->db->where('status', 7);
       if ($view_by == 'month') {
           $where['MONTH(created)'] = date('m');
           $this->db->where($where);
       } else {
           $this->db->where('shift_date BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW()');
       }
       if ($filled_type == 'by_app') {
           $this->db->where('push_to_app', 1);
       } else {
           $this->db->where('push_to_app', 0);
       }
       $query = $this->db->get();
       $result = $query->num_rows();
       return $result;
   }

   public function get_previous_shifts($participantID, $shiftIds) {
       $tbl_shift = TBL_PREFIX . 'crm_participant_shifts';
       $tbl_shift_participant = TBL_PREFIX . 'shift_crm_participant';
       $select_column = array($tbl_shift . ".id", $tbl_shift . ".shift_date", $tbl_shift . ".start_time", $tbl_shift . ".end_time", $tbl_shift . ".status");
       $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");
       $this->db->select($select_column);
       $this->db->from($tbl_shift_participant);
       $this->db->join($tbl_shift, $tbl_shift_participant . '.id = ' . $tbl_shift . '.id', 'inner');
       $this->db->where_in('shift_date', $shiftIds);
       $this->db->where_in($tbl_shift . '.status', array(1, 2, 3, 4, 5, 6, 7));
       $this->db->where('participantId', $participantID);
       $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       $response = $query->result_array();
       if (!empty($response)) {
           foreach ($response as $key => $val) {
               $response[$key] = $val;
               $response[$key]['duration'] = ($val['duration']);
           }
       }
       return $response;
   }
   public function get_participant_name($post_data) {
       $this->db->select("CONCAT(firstname,' ',middlename,' ',lastname) as label");
       $this->db->select('id as value');
       $this->db->where('archive', 0);
       $this->db->where('status', 1);
       $this->db->group_start();
       $this->db->or_like('firstname', $post_data);
       $this->db->or_like('lastname', $post_data);
       $this->db->group_end();
       $query = $this->db->get(TBL_PREFIX . 'participant');
       $query->result();
       return $query->result();
   }

   public function get_site_name($post_data) {
       $this->db->like('site_name', $post_data, 'both');
       $this->db->where('archive =', 0);
       $this->db->where('organisationId !=', 0);
       $this->db->select('site_name, id');
       $query = $this->db->get(TBL_PREFIX . 'organisation_site');
       $query->result();
       $participant_rows = array();
       if (!empty($query->result())) {
           foreach ($query->result() as $val) {
               $participant_rows[] = array('label' => $val->site_name, 'value' => $val->id);
           }
       }
       return $participant_rows;
   }
   public function get_booking_list($reqData) {
       $srchId = $reqData->data->srchId;
       $tbl_participant_booking_list = 'tbl_participant_booking_list';
       $this->db->select(array("CONCAT(" . $tbl_participant_booking_list . ".firstname,' '," . $tbl_participant_booking_list . ".lastname ) as name", "id", "phone", "email", "firstname", "lastname"));
       $this->db->where('participantId', $srchId);
       $this->db->where('archive', '0');
       $query = $this->db->get($tbl_participant_booking_list);
       $state = array();
       $query->result();
       if (!empty($query->result())) {
           foreach ($query->result() as $val) {
               $state[] = array('label' => $val->name, 'value' => $val->id, 'firstname' => $val->firstname, 'lastname' => $val->lastname, 'email' => $val->email, 'phone' => $val->phone);
           }
       }
 //        $state[] = array('label' => 'Other', 'value' => 'value');
       return $state;
   }
   public function get_member_allocation_by_pay_point($memberId = ['0'], $extra_parameter = []) {
       $supportType = isset($extra_parameter['support_type']) && !empty($extra_parameter['support_type']) ? $extra_parameter['support_type'] : '';
       $memberId = is_array($memberId) ? array_filter($memberId) : ['0'];
       $priortyOrderColumnConcat = "concat(tcl.level_priority,tcp.point_priority)";
       $priortyOrderColumnConcatMin = "min(" . $priortyOrderColumnConcat . ") as min_priorty_order";
       $priortyOrderColumnConcatAs = $priortyOrderColumnConcat . " as priorty_order";
       $this->db->select($priortyOrderColumnConcatMin, false);
       $this->db->from('tbl_member_position_award as tmpa');
       $this->db->join('tbl_classification_level as tcl', 'tmpa.level=tcl.id', 'inner');
       $this->db->join('tbl_classification_point as tcp', 'tmpa.award=tcp.id', 'inner');
       if (!empty($supportType)) {
           $this->db->where('tmpa.support_type', $supportType);
       }
       $this->db->where('tmpa.archive', 0);
       $this->db->where_in('tmpa.memberId', $memberId);
       $whereClause = $this->db->get_compiled_select();
       $this->db->select($priortyOrderColumnConcatAs, false);
       $this->db->select('tmpa.memberId, tcl.level_name,tcp.point_name,tcl.level_priority,tcp.point_priority');
       $this->db->from('tbl_member_position_award as tmpa');
       $this->db->join('tbl_classification_level as tcl', 'tmpa.level=tcl.id', 'inner');
       $this->db->join('tbl_classification_point as tcp', 'tmpa.award=tcp.id', 'inner');
       if (!empty($supportType)) {
           $this->db->where('tmpa.support_type', $supportType);
       }
       $this->db->where('tmpa.archive', 0);
       $this->db->where_in('tmpa.memberId', $memberId);
       $this->db->where($priortyOrderColumnConcat . "=(" . $whereClause . ")", null, false);
       $this->db->group_by('tmpa.memberId');
       $this->db->order_by('tcl.level_priority asc,tcp.point_priority asc');
       $query = $this->db->get();
       return $query->result_array();
   }

   function cancel_shift($reqData) {
       $cancel_by = '';
       $response = $this->Listing_model->get_accepted_shift_member($reqData->shiftId);
       // set member cancelled shift
       if (!empty($response)) {
           $cancel_by = $response[0]->memberId;
       }
       $this->basic_model->update_records('shift_member', $data = array('status' => 4), $where = array('shiftId' => $reqData->shiftId));
       if ($reqData->cancel_type == 'member') {
           $cancel_by = $response[0]->memberId;
       } else if ($reqData->cancel_type == 'participant') {
           $response = $this->Listing_model->get_shift_participant($reqData->shiftId);
           if (!empty($response))
               $cancel_by = $response[0]['participantId'];
       }else if ($reqData->cancel_type == 'kin') {
           $cancel_by = $reqData->cancel_person;
       } else if ($reqData->cancel_type == 'booker') {
           $cancel_by = $reqData->cancel_person;
       } else if ($reqData->cancel_type == 'org') {
           $result = $this->Listing_model->get_shift_oganization($reqData->shiftId);
           if (!empty($result)) {
               $cancel_by = $result[0]->siteId;
           }
       } else if ($reqData->cancel_type == 'site') {
           $result = $this->Listing_model->get_shift_oganization($reqData->shiftId);
           if (!empty($result)) {
               $cancel_by = $result[0]->siteId;
           }
       }
       $this->loges->setUserId($reqData->shiftId);
       $this->loges->setDescription(json_encode($reqData));
       $this->loges->setTitle('Cancel shift : Shift Id ' . $reqData->shiftId);
       $this->loges->createLog();
       $cancel_array = array('shiftId' => $reqData->shiftId, 'reason' => $reqData->reason, 'cancel_type' => $reqData->cancel_type, 'cancel_by' => $cancel_by, 'cancel_method' => $reqData->cancel_method, 'person_name' => '');
       $this->basic_model->insert_records('shift_cancelled', $cancel_array);
       $this->basic_model->update_records('shift', $data = array('status' => 5), $where = array('id' => $reqData->shiftId));
       return $return = array('status' => true);
   }

   public function get_shift_details($shiftId, $mutiple = false) {
       $tbl_shift = TBL_PREFIX . 'crm_participant_shifts';
       $select_column = array("tbl_crm_participant_shifts.id", "tbl_crm_participant_shifts.shift_date", "tbl_crm_participant_shifts.start_time", "tbl_crm_participant_shifts.end_time", "tbl_crm_participant_shifts.created", "tbl_crm_participant_shifts.status","tbl_crm_participant_shifts.crm_participant_id",'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName');
       $tbl_crm_participant = TBL_PREFIX.'crm_participant';
       $this->db->select($select_column);
       $this->db->select("CONCAT(MOD( TIMESTAMPDIFF(hour," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 24), ':',MOD( TIMESTAMPDIFF(minute," . $tbl_shift . ".start_time," . $tbl_shift . ".end_time), 60), ' hrs') as duration");
       $this->db->from($tbl_shift);
       $this->db->join($tbl_crm_participant,$tbl_shift . '.crm_participant_id = ' . $tbl_crm_participant . '.id', 'left');
       $this->db->where($tbl_shift . '.id', $shiftId);

       if ($mutiple) {
           $this->db->where_in($tbl_shift . '.id', $shiftId);
       } else {
           $this->db->where(array($tbl_shift . '.id' => $shiftId));
       }

       $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
       $response = $query->result();

       $result = $mutliple_result = array();
       if (!empty($response)) {
           foreach ($response as $key => $val) {
               $result = (array) $val;


               $result['diff'] = (strtotime(date('Y-m-d H:i:s', strtotime($val->start_time))) - strtotime(date('Y-m-d H:i:s'))) * 1000;

               $result['duration'] = ($val->duration);
               if ($mutiple) {
                   $mutliple_result[] = $result;
               }
           }
       }

       if ($mutiple) {
           return $mutliple_result;
       } else {
           return $result;
       }
   }

   public function get_all_crm_logs($reqData){
     $new_array=array();
     $tbl_2 = TBL_PREFIX .'member';
     $tbl_3 = TBL_PREFIX .'crm_participant';
     $tbl_6 = TBL_PREFIX .'crm_logs';
	 $tbl_4=  TBL_PREFIX.'module_title';
     $select_column = array('distinct concat('.$tbl_3.'.firstname," ",'.$tbl_3.'.lastname) as participant_name',
               '(CASE WHEN '.$tbl_3.'.booking_status=2 THEN "Parked"
                     WHEN '.$tbl_3.'.booking_status=3 THEN "Activated"
                     WHEN '.$tbl_3.'.booking_status=4 THEN "Activate"
                     WHEN '.$tbl_3.'.booking_status=5 THEN "Rejected" END) as action',
               'concat('.$tbl_2.'.firstname," ",'.$tbl_2.'.lastname) as FullName',
         $tbl_6.'.title', 'DATE_FORMAT('.$tbl_6.'.created,"%d-%m-%Y %h:%i:%s") as created',$tbl_6.'.userId'
       );
     $sWhere = array($tbl_6.'.module' => '10', "DATE(".$tbl_6.".created)" => date('Y-m-d'));
     $this->db->select($select_column);
     $this->db->from($tbl_6);
     $this->db->join($tbl_2, $tbl_2.'.id = '.$tbl_6.'.created_by ', 'left');
     $this->db->join($tbl_3, $tbl_3.'.id = '.$tbl_6.'.userId', 'left');
     $this->db->join($tbl_4, $tbl_4.'.id = '.$tbl_6.'.sub_module', 'left');
     if(!empty($reqData->participantId)){
       $where = array($tbl_6.'.userId' => $reqData->participantId);
       $this->db->where($where);
     }else{
       $this->db->where($sWhere);
     }
     $this->db->order_by($tbl_6.'.id','desc');
     $this->db->limit(5);
     $query = $this->db->get();
     $return1 = array('data' => $query->result());
     $return2 =$this->get_all_logs($reqData);
     $return=array_merge_recursive($return1,$return2);
    //sort array by order
    foreach ($return as $key => $row) {
         array_multisort(array_map('strtotime', array_column($row, 'created')), SORT_DESC, $row);
        }
     $new_array['data']=$row;
     return $new_array;
   }
   function get_all_logs($reqData){
    // var_dump($reqData);exit;
     $tbl_1 = TBL_PREFIX .'logs';
     $tbl_2 = TBL_PREFIX .'member';
     $tbl_3 = TBL_PREFIX .'crm_participant';
     $tbl_5 = TBL_PREFIX .'crm_participant_schedule_task';
     $tbl_4=  TBL_PREFIX.'module_title';
     $select_column = array('distinct concat('.$tbl_3.'.firstname," ",'.$tbl_3.'.lastname) as participant_name',
               '(CASE WHEN '.$tbl_3.'.booking_status=2 THEN "Parked"
                     WHEN '.$tbl_3.'.booking_status=3 THEN "Activated"
                     WHEN '.$tbl_3.'.booking_status=4 THEN "Activate"
                     WHEN '.$tbl_3.'.booking_status=5 THEN "Rejected" END) as action',
               'concat('.$tbl_2.'.firstname," ",'.$tbl_2.'.lastname) as FullName',
         $tbl_1.'.title', 'DATE_FORMAT('.$tbl_1.'.created,"%d-%m-%Y %h:%i:%s") as created',$tbl_1.'.userId'
       );
     $sWhere = array($tbl_1.'.module' => '10', "DATE(".$tbl_1.".created)" => date('Y-m-d'));
     $this->db->select($select_column);
     $this->db->from($tbl_1);
     $this->db->join($tbl_2, $tbl_2.'.id = '.$tbl_1.'.created_by ', 'left');
     $this->db->join($tbl_3, $tbl_3.'.id = '.$tbl_1.'.userId', 'left');
     $this->db->join($tbl_4, $tbl_4.'.id = '.$tbl_1.'.sub_module', 'left');
     $this->db->join($tbl_5, $tbl_5.'.crm_participant_id = '.$tbl_3.'.id', 'left');
     if(!empty($reqData->participantId)){
       $where = array($tbl_1.'.userId' => $reqData->participantId,$tbl_1.'.sub_module !='=>2, $tbl_1.'.module'=>10);
       $this->db->where($where);
     }else{
       $this->db->where($sWhere);
     }
     $this->db->order_by($tbl_1.'.id','desc');

     $this->db->limit(5);

     $query = $this->db->get();
     // echo $this->db->last_query();
     $return = array('data' => $query->result());
     // var_dump($return);
     return $return;
   }
	function get_ndis_services_p_participant_id($reqData){
		$colowmn = array("*");
		$result = $this->basic_model->get_record_where('crm_participant_plan_breakdown', $colowmn, $where = array('crm_participant_id' => $reqData->crm_participant_id,'plan_id'=>$reqData->planId));
        $result1 = $this->basic_model->get_record_where('crm_participant', 'ndis_num', $where = array('id' => $reqData->crm_participant_id));
        $result['ndis_num']=$result1[0]->ndis_num;
		return $result;
	}
   function delete_participant_docs($reqData){
    //    echo $reqData->hearing_file_id;
        // echo FCPATH.CRM_UPLOAD_PATH.$reqData->crm_participant_id."/".$reqData->hearing_file_name;exit;
        // echo base_url().'uploads/crmparticipant/'.$reqData->crm_participant_id.'/'.$reqData->hearing_file_name;
    if(isset($reqData->hearing_file_id)&& isset($reqData->hearing_file_name))
    {

        if(file_exists(FCPATH.CRM_UPLOAD_PATH.$reqData->crm_participant_id."/".$reqData->hearing_file_name))
        {
           unlink(FCPATH.CRM_UPLOAD_PATH.$reqData->crm_participant_id."/".$reqData->hearing_file_name);
        }
        $table="crm_participant_docs";
        $where=array('id'=>$reqData->hearing_file_id, 'crm_participant_id'=>$reqData->crm_participant_id);
       $result= $this->basic_model->delete_records($table, $where);
    }
    if(isset($reqData->ndis_file_id)&& isset($reqData->ndis_file_name))
    {

        if(file_exists(FCPATH.CRM_UPLOAD_PATH.$reqData->crm_participant_id."/".$reqData->ndis_file_name))
        {
           unlink(FCPATH.CRM_UPLOAD_PATH.$reqData->crm_participant_id."/".$reqData->ndis_file_name);
        }
        $table="crm_participant_docs";
        $where=array('id'=>$reqData->ndis_file_id, 'crm_participant_id'=>$reqData->crm_participant_id);
       $result= $this->basic_model->delete_records($table, $where);
    }
     return $result;
   } 
   
    function get_all_participant_dropdown_list(){
        $response['mobility']=$this->get_list_mobility();
        $response['assistance']=$this->get_list_assistance();
        $response['living_situation']=$this->get_list_living_situations();
        $response['relations_participant']=$this->get_list_crm_relations_participant();
        $response['communication_type']=$this->get_list_communication_type();
        $response['cognitive_level']=$this->get_list_cognitive_level();
        $response['marital_status']=$this->get_list_marital_status();      
        $response['relations']=$this->get_list_relations();
        $response['state']=$this->get_list_state();
        $response['languages']=$this->get_list_languages();
        $response['religious_beliefs']=$this->get_list_religious_beliefs();
        $response['ethnicity']=$this->get_list_ethnicity();

        return $response;
    }

    private function get_list_living_situations(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_living_situation');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
        
    }
    private function get_list_crm_relations_participant(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_crm_relations_participant');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_communication_type(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_communication_type');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_cognitive_level(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_cognitive_level');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_marital_status(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_marital_status');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_relations(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_relations');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_state(){
        $select_column = array('name as label', 'id as value');
        $this->db->select($select_column);
        $this->db->from('tbl_state');
        $this->db->where("archive",0);     
        $this->db->order_by('id','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_assistance(){
        $select_column = array('name as label', 'id as value');
        $where = array('status' => 1, 'type' => 'assistance');
        
        $this->db->select($select_column);
        $this->db->from('tbl_participant_genral');
        $this->db->where($where);
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_mobility(){
        $select_column = array('name as label', 'id as value');
        $where = array('status' => 1, 'type' => 'mobility');

        $this->db->select($select_column);
        $this->db->from('tbl_participant_genral');
        $this->db->where($where);     
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_languages(){
        $select_column = array('name as label', 'id as value');
        $where = array('archive' => 0);
        $this->db->select($select_column);
        $this->db->from('tbl_language');
        $this->db->where($where);     
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_religious_beliefs(){
        $select_column = array('name as label', 'id as value');
        $where = array('archive' => 0);
        $this->db->select($select_column);
        $this->db->from('tbl_religious_beliefs');
        $this->db->where($where);     
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }
    private function get_list_ethnicity(){
        $select_column = array('name as label', 'id as value');
        $where = array('archive' => 0);
        $this->db->select($select_column);
        $this->db->from('tbl_ethnicity');
        $this->db->where($where);     
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();
    }

    // Create new crm participant code start here

    
    public function create_crm_participant($objparticipant)
    {
        $objparticipant=(array)$objparticipant;
        $participantDetails=(array)$objparticipant['participantDetails'];
        $participantAbility=(array)$objparticipant['participantAbility'];
        
        $participantData=array();        
        $participantData['referral_firstname']=$objparticipant['refererDetails']->first_name;
        $participantData['referral_lastname']=$objparticipant['refererDetails']->last_name;
        $participantData['referral_email']=$objparticipant['refererDetails']->remail;
        $participantData['referral_phone']=$objparticipant['refererDetails']->phone_number;
        $participantData['referral_org']=$objparticipant['refererDetails']->organisation;
        $participantData['referral_relation']=$objparticipant['refererDetails']->relation;

        // Participant details
        $participantData['firstname']=$objparticipant['participantDetails']->firstname;
        $participantData['lastname']=$objparticipant['participantDetails']->lastname;
        $participantData['middlename']='';
        $participantData['gender']=$objparticipant['participantDetails']->gender;
        $participantData['relation']=$objparticipant['participantDetails']->firstname;
        $participantData['preferredname']=$objparticipant['participantDetails']->preferredfirstname;
        $participantData['prefer_contact']=$objparticipant['participantDetails']->preferred_contact;
        $participantData['ndis_num']=$objparticipant['participantDetails']->ndisno;
        $participantData['dob']=$objparticipant['participantDetails']->Dob;
        $participantData['aboriginal_tsi']=$objparticipant['participantDetails']->aboriginal_tsi;
        $participantData['behavioural_support_plan']=$objparticipant['participantDetails']->current_behavioural;
        $participantData['living_situation']=$objparticipant['participantDetails']->livingsituation;
        $participantData['marital_status']=$objparticipant['participantDetails']->maritalstatus;
        
        $participantData['ndis_plan']=$objparticipant['participantDetails']->plan_management;        
       
        
        $participantData['other_relevant_plans']=$objparticipant['participantDetails']->other_relevent_plans;
        $participantData['gender']=$objparticipant['participantDetails']->gender;
        $participantData['medicare_num']=$objparticipant['participantDetails']->medicare;
        $participantData['crn']=$objparticipant['participantDetails']->crn;        
        
        // ndis_file ,hearing_file , primary_contact,        
        // Participant ability 
      
        $languages_spoken=(!empty($objparticipant['participantAbility']->languages_spoken))? implode(',',$objparticipant['participantAbility']->languages_spoken):''; 

        $participantData['cognitive']=$objparticipant['participantAbility']->cognitive_level;
        $participantData['communication']=$objparticipant['participantAbility']->communication;
        $participantData['hearing_interpreter']=$objparticipant['participantAbility']->hearing_interpreter;
        $participantData['linguistically_background']=$objparticipant['participantAbility']->linguistic_diverse        ;
        $participantData['language_interpreter']=$objparticipant['participantAbility']->language_interpreter;
        
        $participantData['language_spoken']=$languages_spoken;        
        $participantData['carers_gender']=$objparticipant['participantAbility']->carer_gender;
        $participantData['booking_status'] = 4;
        $participantData['status'] = 1;
        $participantData['stage_status']   = 1;

        $crm_participant_id= $this->db->insert(TBL_PREFIX.'crm_participant', $participantData); 
        $crm_participant_id= $this->db->insert_id();
        if($crm_participant_id>0){       
            $this->add_crm_participant_care_not_to_book($crm_participant_id,$participantAbility);
            $this->add_crm_manager_plan($crm_participant_id,$participantDetails);
            $this->add_crm_participant_email($crm_participant_id,$participantDetails);
            $this->add_crm_participant_phone($crm_participant_id,$participantDetails);
            $this->create_crm_participant_next_kin($crm_participant_id,$participantDetails['kin_details']);
            $this->create_crm_participant_booker($crm_participant_id,$participantDetails['booker_details']);
            $this->create_crm_participant_ability_requirements($crm_participant_id,$participantAbility);
            $this->add_crm_participant_disability($crm_participant_id,$participantAbility);
            $this->add_crm_participant_address($crm_participant_id,$participantDetails);
            $this->add_crm_attach_documents($crm_participant_id,$participantDetails);
            return $crm_participant_id;
        }       
    }
    private function create_crm_participant_next_kin($crm_participant_id,$nextkins){
        $kinData=array();
        foreach($nextkins as $nextkin){
            $arr_kin['crm_participant_id']=$crm_participant_id;
            $arr_kin['firstname']=$nextkin->first_name;
            $arr_kin['lastname']=$nextkin->last_name;
            $arr_kin['relation']=$nextkin->relation;
            $arr_kin['phone']=$nextkin->phone;
            $arr_kin['email']=$nextkin->email;
            $arr_kin['archive']=0;
            $kinData[]=$arr_kin;           
        }
        $insert_query = $this->db->insert_batch(TBL_PREFIX.'crm_participant_kin', $kinData); 
       
    }
    private function create_crm_participant_booker($crm_participant_id,$bookers){
        $bookerData=array();        
        foreach($bookers as $booker){
            $arrbooker['crm_participant_id']=$crm_participant_id;
            $arrbooker['firstname']=$booker->first_name;
            $arrbooker['lastname']=$booker->last_name;
            $arrbooker['relation']=$booker->relation;
            $arrbooker['phone']=$booker->phone;
            $arrbooker['email']=$booker->email;
            $arrbooker['archive']=0;
            $bookerData[]=$arrbooker;           
        }
        $insert_query   = $this->db->insert_batch(TBL_PREFIX.'crm_participant_booking_list', $bookerData); 
    }
    private function add_crm_manager_plan($crm_participant_id,$manager_details){
        //print_r($manager_details);
        if($manager_details['plan_management']==3){
            $ndis_plan=array();
            $ndis_plan['manager_plan']=$manager_details['provide_plan'];
            $ndis_plan['manager_email']=$manager_details['provide_email'];
            $ndis_plan['manager_address']=$manager_details['provide_address'];
            $ndis_plan['state']=$manager_details['provide_state'];
            $ndis_plan['post_code']=$manager_details['provide_postcode'];
            $ndis_plan['crm_participant_id']=$crm_participant_id;
            $insert_query   = $this->db->insert(TBL_PREFIX.'crm_ndis_plan', $ndis_plan); 
        }
    }
    private function add_crm_participant_email($crm_participant_id,$details_email){
            $participant_email = array();
            $participant_email['crm_participant_id'] = $crm_participant_id;
            $participant_email['email'] = $details_email['email'];
            $participant_email['primary_email'] = 1;
            $insert_query   = $this->db->insert(TBL_PREFIX.'crm_participant_email', $participant_email); 
    }
    private function add_crm_participant_phone($crm_participant_id,$details_phone){
        $participant_phone = array();
        $participant_phone['crm_participant_id'] = $crm_participant_id;
        $participant_phone['phone'] = $details_phone['phonenumber'];
        $participant_phone['primary_phone'] = 1;
        $insert_query   = $this->db->insert(TBL_PREFIX.'crm_participant_phone', $participant_phone);
    }

    private function create_crm_participant_ability_requirements($crm_participant_id,$mobilityAbility){
               
        $arrMobilityAbility=array();
        if(!empty($mobilityAbility['require_mobility'])){
            foreach($mobilityAbility['require_mobility'] as $ability){
                $mobility=array();
                $mobility['requirment']=$ability;
                $mobility['type']='mobility';
                $mobility['status']=1;            
                $arrMobilityAbility[]=$mobility;
            }
        }
        
        if(!empty($mobilityAbility['require_assistance'])){
            foreach($mobilityAbility['require_assistance'] as $ability){
                $assistance=array();
                $assistance['requirment']=$ability;
                $assistance['type']='assistance';
                $assistance['status']=1;            
                $arrMobilityAbility[]=$assistance;
            }
        }
        if(!empty($arrMobilityAbility)){
            $insert_query   = $this->db->insert_batch(TBL_PREFIX.'crm_participant_ability_requirements', $arrMobilityAbility); 
        }
        
    }    
    private function add_crm_participant_care_not_to_book($crm_participant_id,$participantAbility){
               
        // 1 for ethnicity/2 for religious
        $arrEthnicityReligious=array();
        if(!empty($participantAbility['ethnicity'])){
            foreach($participantAbility['ethnicity'] as $ethnicity){
                $arrEthnicity=array();                
                $arrEthnicity['crm_participant_id']=$crm_participant_id;
                $arrEthnicity['carer_type']=$ethnicity;
                $arrEthnicity['type']=1;
                $arrEthnicity['archive']=0;            
                $arrEthnicityReligious[]=$arrEthnicity;
            }
        }
        
        if(!empty($participantAbility['religious_beliefs'])){
            foreach($participantAbility['religious_beliefs'] as $religious){
                $arrReligious=array();                
                $arrReligious['crm_participant_id']=$crm_participant_id;
                $arrReligious['carer_type']=$religious;
                $arrReligious['type']=2;
                $arrReligious['archive']=0;            
                $arrEthnicityReligious[]=$arrReligious;
            }
        }
        if(!empty($arrEthnicityReligious)){
            $insert_query   = $this->db->insert_batch(TBL_PREFIX.'crm_participant_care_not_to_book', $arrEthnicityReligious); 
        }
        
    }

    private function add_crm_participant_disability($crm_participant_id,$participantAbility){  
        $participantDisability=array();
        $participantDisability['crm_participant_id']=$crm_participant_id;
        $participantDisability['primary_fomal_diagnosis_desc']=$participantAbility['primary_fomal_diagnosis_desc'];
        $participantDisability['secondary_fomal_diagnosis_desc']=$participantAbility['secondary_fomal_diagnosis_desc'];
        $participantDisability['other_relevant_information']=$participantAbility['other_relevant_conformation'];
        $participantDisability['legal_issues']=$participantAbility['legal_issues'];
        $participantDisability['status']=0;
        $insert_query   = $this->db->insert(TBL_PREFIX.'crm_participant_disability', $participantDisability); 
        
    }
    private function add_crm_participant_address($crm_participant_id,$participantAddress){
      
        $latLong = getLatLong($participantAddress['Address']);
        $arrAddress=array();
        $arrAddress['lat']=$latLong['lat'];
        $arrAddress['long']=$latLong['long'];
        $arrAddress['street']=$participantAddress['Address'];
        $arrAddress['crm_participant_id']=$crm_participant_id;
        $arrAddress['city']=isset($participantAddress['city']->value)?$participantAddress['city']->value:'';
        $arrAddress['state']=$participantAddress['state'];
        $arrAddress['postal']=$participantAddress['postcode'];
        $arrAddress['primary_address']=1;
        $insert_query   = $this->db->insert(TBL_PREFIX.'crm_participant_address', $arrAddress); 
    }

    private function add_crm_attach_documents($crm_participant_id,$participantdetails){
        
        $attachmentsData=array();
        if(!empty($participantdetails['ndis_file'])){
            
            foreach($participantdetails['ndis_file'] as $ndis_file){
                    
                $arrAttachmnet=array();
                $arrAttachmnet['crm_participant_id']=$crm_participant_id;
                $arrAttachmnet['stage_id']=0;
                $arrAttachmnet['type']=1;
                $arrAttachmnet['title']='';
                $arrAttachmnet['filename']=$ndis_file->name;
                $arrAttachmnet['created']=DATE_TIME;
                $arrAttachmnet['document_type']=1;
                $arrAttachmnet['document_signed']='';
                $arrAttachmnet['envelope_id']='';
                $arrAttachmnet['signed_file_path']='';
                $arrAttachmnet['archive']=0;
                $attachmentsData[]=$arrAttachmnet;

            }

        }

        if(!empty($participantdetails['hearing_file'])){
            
            foreach($participantdetails['hearing_file'] as $hearing_files){
                $arrAttachmnet=array();
                $arrAttachmnet['crm_participant_id']=$crm_participant_id;
                $arrAttachmnet['stage_id']=0;
                $arrAttachmnet['type']=2;
                $arrAttachmnet['title']='';
                $arrAttachmnet['filename']=$hearing_files->name;
                $arrAttachmnet['created']=DATE_TIME;
                $arrAttachmnet['document_type']=1;
                $arrAttachmnet['document_signed']='';
                $arrAttachmnet['envelope_id']='';
                $arrAttachmnet['signed_file_path']='';
                $arrAttachmnet['archive']=0;
                $attachmentsData[]=$arrAttachmnet;
            }            
           
        }
        
        if(!empty($attachmentsData)){
            $insert_query  = $this->db->insert_batch(TBL_PREFIX.'crm_participant_docs', $attachmentsData); 
        }
    }
    
}
