<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmStaff_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function user_management_list($reqData) {

      $limit = $reqData->pageSize;
      $page = $reqData->page;
      $sorted = $reqData->sorted;
      $filter = $reqData->filtered;
      $orderBy = array();
      $direction = '';

     $tbl_1 = TBL_PREFIX . 'member';
     $tbl_2 = TBL_PREFIX . 'crm_staff';

      if(!empty(request_handler()))
      {
          $src_columns = array(
          $tbl_1.'.username', $tbl_1.'.id',
          $tbl_1.'.position',$tbl_1.'.firstname',"DATE_FORMAT(tbl_member.created, '%d/%m/%Y') as created", $tbl_2.'.service_area',
          $tbl_1.'.lastname',"CONCAT(tbl_member.firstname,' ',tbl_member.lastname) as name");
          if (!empty($sorted)) {
              if (!empty($sorted[0]->id)) {
                  if ($sorted[0]->id) {
                      $orderBy = $sorted[0]->id;
                  } else {
                      $orderBy = 'tbl_member.' . $sorted[0]->id;
                  }
                  if ($orderBy == 'service_area') {
                      $orderBy = 'tbl_member.id';
                  }
                  if ($orderBy == 'created') {
                      $orderBy = 'tbl_member.created';
                  }
                  $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
              }
          } else {
              $orderBy = 'tbl_member.id';
              $direction = 'desc';
          }
          // var_dump($filter->filterVal);
          if(!empty($filter)){
            if ($filter->inactive) {
                  $status_con = array(0, 1);
              }
            // Filter Dropdown


            // Search
            if(!empty($filter->search)){
              $this->db->group_start();
              for ($i = 0; $i < count($src_columns); $i++)
              {
                  $column_search = $src_columns[$i];
                  if (strstr($column_search, "as") !== false) {
                      $serch_column = explode(" as ", $column_search);
                      if ($serch_column[0] != 'null')
                          $this->db->or_like($serch_column[0], $filter->search);
                  } else if ($column_search != 'null') {
                      $this->db->or_like($column_search, $filter->search);
                  }
              }

            $this->db->group_end();}
            if((int)$filter->filterVal == 0 || !empty($filter->filterVal)){
            if($filter->filterVal!='All'){
                $this->db->where($tbl_2.'.status='.(int)$filter->filterVal);
               // $this->db->where($tbl_2.'.status=',1);
               // $this->db->or_where($tbl_2.'.status=',0);
            }
            // else{
            //   $this->db->where($tbl_2.'.status='.(int)$filter->filterVal);
            // }
          }
          }
          $select_columns = array(
              'tbl_member.id as ocs_id',
              'tbl_member.username',
              'concat(tbl_member.firstname," ",tbl_member.lastname) as name',
              'tbl_member.status',
              'tbl_member.position',
              'tbl_member.archive',
              'tbl_member.created',
              'tbl_member.firstname',
              'tbl_member.lastname',
              'tbl_crm_staff.service_area',
              'tbl_crm_staff.status as user_status'
          );
          $dt_query  = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_columns)), false);
          $this->db->from('tbl_member');
          $this->db->join('tbl_crm_staff', 'tbl_crm_staff.admin_id = tbl_member.id', 'inner');
          $this->db->where('tbl_member'.'.archive=',"0");
          $this->db->where('tbl_member'.'.status=',"1");
          $this->db->order_by($orderBy, $direction);
          $this->db->group_by('tbl_crm_staff.admin_id');
          $this->db->limit($limit, ($page * $limit));
          $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
// print_r($this->db->last_query());
          $total_coount = $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

          if ($dt_filtered_total % $limit == 0) {
              $dt_filtered_total = ($dt_filtered_total / $limit);
          } else {
              $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
          }


          $x = $query->result_array();
          $site_ary = array();
          foreach ($x as $key => $value)
          {
              $staff_id =  $value['ocs_id'];
              $temp['ocs_id'] = $staff_id;
              $temp['name'] = $value['name'];
              $temp['username'] = $value['username'];
              $temp['firstname'] = $value['firstname'];
              $temp['lastname'] = $value['lastname'];
              $temp['position'] = $value['position'];
              $temp['service_area'] = $value['service_area'];
              $temp['user_status'] =$value['user_status'];
              $temp['created'] = $value['created']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($value['created'])):'';
              $temp['updated'] = $value['created']!='0000-00-00 00:00:00'?date('d/m/Y',strtotime($value['created'])):'';

              /*Get Staff phone no*/
              $z = $this->Basic_model->get_result('member_phone',array('tbl_member_phone.memberId'=>$staff_id,'tbl_member_phone.archive'=>'0'),$columns=array('tbl_member_phone.phone','tbl_member_phone.primary_phone','tbl_member_phone.id'));
              $ph_temp = array();
              if(!empty($z))
              {
                  foreach ($z as $key => $valPh)
                  {
                      $ph_temp[] = array('name'=>$valPh->phone,'id'=>$valPh->id,'primary_phone'=>$valPh->primary_phone);
                  }
              }
              else{
                $ph_temp[] = array('name'=>'');
              }
              $temp['PhoneInput'] = $ph_temp;
              /*Get staff email*/
              $zMail = $this->Basic_model->get_result('member_email',array('tbl_member_email.memberId'=>$staff_id,'tbl_member_email.archive'=>'0'),$columns=array('tbl_member_email.email','tbl_member_email.primary_email','tbl_member_email.id'));
              $email_temp = array();
              if(!empty($zMail))
              {
                  foreach ($zMail as $key => $valmail)
                  {
                      $email_temp[] = array('name'=>$valmail->email,'id'=>$valmail->id,'primary_email'=>$valmail->primary_email);
                  }
              }
              else{
                $email_temp[] = array('name'=>'');
              }
              $temp['EmailInput'] = $email_temp;
              $site_ary[] = $temp;
          }
          $return = array('data' => $site_ary,'count' => $dt_filtered_total,'total_count'=>$total_coount);
          return $return;
        }
      }

      public function get_staff_name($post_data) {

        $this->db->select("CONCAT(tbl_member.firstname,' ',tbl_member.lastname) as staffName, tbl_crm_staff.admin_id as id,tbl_crm_staff.status");
        $this->db->from('tbl_crm_staff');
        $this->db->join('tbl_member', 'tbl_member.id = tbl_crm_staff.admin_id', 'left');
        $this->db->where('tbl_crm_staff.status=', 1);
        $this->db->where('tbl_member.archive =', 0);
        $this->db->like("CONCAT(tbl_member.firstname,' ',tbl_member.lastname)", $post_data);

        $query = $this->db->get();
        $staff_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $staff_rows[] = array('label' => $val->staffName, 'value' => $val->id);
            }
        }
        return $staff_rows;
    }


    public function get_new_crm_staff_name($post_data) {
      //  $search = $reqData->search;

        $this->db->select(array('concat(a.firstname," ",a.lastname) as staffName', 'a.id as id', 'cs.admin_id as is_already_added','a.id as hcm_id','a.position as position','a.username as username', 'ae.email as email' , 'ap.phone as phone' ));
        $this->db->from('tbl_member as a');

        $this->db->join('tbl_admin_role as ar', 'ar.adminId = a.id', 'inner');
        $this->db->join('tbl_member_email as ae', 'ae.memberId = a.id', 'left');
        $this->db->join('tbl_member_phone as ap ', 'ap.memberId = a.id', 'left');
        $this->db->join('tbl_role as r', 'r.id = ar.roleId', 'inner');
        $this->db->join('tbl_role_permission as rp', 'r.id = rp.roleId', 'inner');
        $this->db->join('tbl_permission as p', 'rp.permission = p.id AND p.permission = "access_crm"', 'inner');
        $this->db->join('tbl_crm_staff as cs', 'cs.admin_id = a.id', 'left');

        $this->db->like('concat(a.firstname," ",a.lastname)', $post_data);
        $this->db->where('a.archive', 0);
        $this->db->where('a.status', 1);

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
//        last_query();
        $result = $query->result();
          $staff_rows = array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {

              $res = $this->basic_model->get_record_where('admin_role',array('id'),array('roleId'=>12,'adminId'=>$val->id));

              if ($val->is_already_added > 0 || !empty($res)) {
                    unset($result[$key]);
                } else {
                  $staff_rows[] = array('label' => $val->staffName, 'value' => $val->id, 'data'=>$val);
                }
            }
        }
        return $staff_rows;
    }


    public function staff_details($staffId){
      $select_column = array(
          'tbl_member.id',
          'tbl_member.id as ocs_id',
          'tbl_member.position',
          'tbl_crm_staff.status',
          'tbl_member.profile_image',
          'concat(tbl_member.firstname," ",tbl_member.lastname) as FullName',
          'tbl_crm_department.name as DepartmentName',
          'tbl_member.created as start_date',
          'tbl_member.created as end_date',
          'tbl_member_phone.phone',
          'tbl_member_email.email',


        );
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'member');
        $this->db->join('tbl_member_email', 'tbl_member_email.memberId = tbl_member.id AND tbl_member_email.primary_email = 1', 'left');
        $this->db->join('tbl_member_phone', 'tbl_member_phone.memberId = tbl_member.id AND tbl_member_phone.primary_phone = 1', 'left');
        $this->db->join('tbl_crm_staff', 'tbl_crm_staff.admin_id = tbl_member.id ', 'left');

        $this->db->join('tbl_crm_department', 'tbl_crm_department.id = tbl_member.department AND tbl_member_phone.primary_phone = 1', 'left');
        $sWhere = array(TBL_PREFIX . 'member.id' => $staffId);
        $this->db->where($sWhere);

        $this->db->group_by('tbl_member.id');

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

        $select_assign_participant = array('tbl_crm_participant.id',
          'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
          'tbl_crm_stage.id as stage_id' ,'tbl_crm_stage.name'
        );
        $this->db->select($select_assign_participant);
        $this->db->from('tbl_crm_participant');
        $this->db->join('tbl_member','tbl_member.id = tbl_crm_participant.assigned_to', 'left');
        $this->db->join('tbl_crm_stage','tbl_crm_stage.id = tbl_crm_participant.stage_status', 'left');
        $sWhere = array(TBL_PREFIX . 'member.id' => $staffId );
        $this->db->where($sWhere);
        $this->db->where_in(TBL_PREFIX . 'crm_participant.booking_status', [2,4,5]);
        $assigned_participant = $this->db->get();
        $assigned = array();
        $assign = array();
        $user_name = $query->row_array();
        if (!empty($assigned_participant->result())) {
            foreach ($assigned_participant->result() as $val) {
              $assigned['FullName'] = $val->FullName;
              $assigned['stage_id'] = $val->stage_id;
              $assigned['stage_name'] = $val->name;
              $assigned['user_id'] = $user_name['id'];
              $assigned['changed_id'] = '';
              $assigned['p_id'] = $val->id;
              $assigned['label'] = (object) array('label'=>$user_name['FullName']);
              $assign[] = $assigned;
            }
          }
        $select_task = array(
          'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
          'tbl_member.id as hcmgr_id',
          'tbl_member.status',
          'tbl_crm_participant_schedule_task.relevant_task_note as note',
          'DATE(tbl_crm_participant_schedule_task.due_date) as date',
          'tbl_crm_participant_schedule_task.task_name',
          'tbl_crm_participant_schedule_task.id as task_id',
          'tbl_crm_participant_schedule_task.priority',
          'tbl_crm_participant.prefer_contact'
        );
        $this->db->select($select_task);
        $this->db->from('tbl_crm_participant_schedule_task');
        $this->db->join('tbl_crm_participant','tbl_crm_participant.id = tbl_crm_participant_schedule_task.crm_participant_id', 'left');
        $this->db->join('tbl_member','tbl_member.id = tbl_crm_participant_schedule_task.assign_to', 'left');
        $sWhere = array(TBL_PREFIX . 'member.id' => $staffId);
        $this->db->where($sWhere);
        $this->db->where('tbl_crm_participant_schedule_task.archive', '0');
        $this->db->where('tbl_crm_participant_schedule_task.task_status', '0');
        $this->db->where('tbl_crm_participant_schedule_task.priority!=0');
        $task = $this->db->get();
        $task_list = $task->result_array();
        $row = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {

                $row['id'] = $val->id;
                $row['hcmgr_id'] = $val->ocs_id;
                $row['status'] = $val->status;
                $row['FullName'] = $val->FullName;
                $row['DepartmentName'] = $val->DepartmentName;
                $row['start_date'] = $val->start_date;
                $row['email'] = $val->email;
                $row['phone'] = $val->phone;
                $row['position'] = $val->position;
                $row['profile_image'] = $val->profile_image;
                $row['end_date'] = $val->end_date;
                $row['task_list'] = $task_list;
                $row['assigned_participant'] = $assign;
                $row['action'] = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';

              }
          }

          return $row;
    }




      public function get_all_staffs($reqData) {
        $orderBy = 'tbl_crm_staff.id';
        $direction = 'DESC';
        $status_con = array(1);
        $src_columns = array(
            'tbl_crm_staff.id',
            'tbl_crm_staff.hcmgr_id',
            'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            'tbl_crm_department.name',
            'tbl_crm_staff.start_date',
            'tbl_crm_staff.end_date'
        );


        $where = '';
        $where = "tbl_crm_staff.archive = 0";

        $sWhere = $where;


          $select_column = array(
              'tbl_crm_staff.id',
              'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            );
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_staff');
            $this->db->where_in('tbl_crm_staff.account_status', $status_con);
            $this->db->order_by($orderBy, $direction);
            $this->db->group_by('tbl_crm_staff.id');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
           $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['value'] = $val->id;
                    $row['label'] = $val->FullName;
                    $dataResult[] = $row;
                  }
              }
              $return = array('count' => $dt_filtered_total, 'data' => $dataResult);
              return $return;

      }


      public function get_staff_disable($reqData) {
        $orderBy = 'tbl_crm_staff.id';
        $direction = 'DESC';
        $status_con = array(1);
        $src_columns = array(
            'tbl_crm_staff.id',
            'tbl_crm_staff.hcmgr_id',
            'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
            'tbl_crm_department.name',
            'tbl_crm_staff.start_date',
            'tbl_crm_staff.end_date',

        );


        $where = '';
        $where = "tbl_crm_staff.archive = 0";
        $where = "tbl_crm_staff_disable.crm_staff_id = 10";
        $sWhere = $where;


          $select_column = array(
              'tbl_crm_staff.id',
              'concat(tbl_crm_staff.preferredname," ",tbl_crm_staff.firstname," ",tbl_crm_staff.lastname) as FullName',
              'tbl_crm_staff_disable.disable_account',
              'tbl_crm_staff_disable.account_allocated_to',
              'tbl_crm_staff_disable.account_allocated',
            );
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_staff');
            $this->db->join('tbl_crm_staff_disable','tbl_crm_staff.id = tbl_crm_staff_disable.crm_staff_id', 'left');
          //  $this->db->where_in('tbl_crm_staff.account_status', $status_con);
            $this->db->where($sWhere);
            $this->db->order_by($orderBy, $direction);
            $this->db->group_by('tbl_crm_staff.id');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());

            $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
            $dataResult = array();
            $val=$query->row();
            if (!empty($val)) {
                   $row = array();
                    $row['staff_id'] = $val->id;
                    $row['staff_name'] = $val->FullName;
                    $row['disableAccount'] = $val->disable_account;
                    $row['account_allocated_staff_to'] = $val->account_allocated_to;
                    $row['allocatedAccount'] = $val->account_allocated;
                    $dataResult[] = $row;
              }
              $return =  $dataResult;
              return $return;
      }

      public function assign_staff_member($reqData){
        $details = (array) $reqData->data;
        $staff_details = array('department_id'=>$details['department_id']);
        $this->db->where($where = array('id' => $details['id']));
        $this->db->update(TBL_PREFIX . 'crm_staff', $staff_details);

        return true;
      }
      function enable_crm_user($reqData){
        // sleep(3);
        $where=array('crm_staff_id'=>$reqData);
        $crm_user_id = $this->Basic_model->get_record_where('crm_staff_disable', 'id,crm_staff_id',$where);
        $swhere = array('id'=>$reqData);
        $table='crm_staff_disable';

        if($crm_user_id){

            $result = $this->Basic_model->delete_records($table,$where);

        }
        $this->Basic_model->update_records('crm_staff',array('status'=>'1'),array('admin_id'=>$reqData));
        return true;
      }
      public function update_alloted_department($reqData)
     {
         if(!empty($reqData))
         {
             $this->basic_model->update_records('crm_staff_department_allocations', array('status'=>'0'), array('admin_id'=>$reqData->staffId));

             #pr($reqData->selectedData->options);
             foreach ($reqData->selectedData->options as $key => $myVal)
             {
                 if($myVal->value!=null)
                 {
                     $tbl = 'tbl_crm_staff_department_allocations';
                     $dt_query = $this->db->select(array($tbl.'.id'));
                     $this->db->from($tbl);
                     $sWhere = array($tbl.'.admin_id'=>$reqData->staffId,$tbl.'.allocated_department'=>$myVal->value,$tbl.'.status'=>'0');
                     $this->db->where($sWhere, null, false);
                     $query = $this->db->get();
                     #echo $this->db->last_query();
                     $tbl = 'crm_staff_department_allocations';
                     $row = $query->row_array();
                     if($row)
                     {
                         $this->Basic_model->update_records($tbl, array('status'=>'1'), array('tbl_crm_staff_department_allocations.id'=>$row['id']));
                         #echo $this->db->last_query();
                     }
                     else
                     {
                         $this->Basic_model->insert_records($tbl, array('admin_id'=>$reqData->staffId,'allocated_department'=>$myVal->value,'status'=>'1','created'=>DATE_TIME));
                         #echo $this->db->last_query();
                     }
                 }
             }

             $tbl = 'tbl_crm_staff_department_allocations';
             $dt_query = $this->db->select(array($tbl.'.allocated_department'));
             $this->db->from($tbl);
             $sWhere = array($tbl.'.admin_id'=>$reqData->staffId,$tbl.'.status'=>'1');
             $this->db->where($sWhere, null, false);
             $query = $this->db->get();

             $z_dept = $query->result_array();
             $department = array();
             if(!empty($z_dept))
             {
                 foreach ($z_dept as $key => $valDept)
                 {
                     $department[] = $valDept['allocated_department'];
                 }
             }
             $department = array_filter($department);
             return $department;
         }
     }
    }
