
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CrmReporting_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->model('Basic_model');
    }
    public function location_analytics_list($reqData) {
        $filter = $reqData->filtered;
        $status_con = array(1);
        $this->db->select("tbl_crm_participant_address.postal ");
        $this->db->select('COUNT(DATE_SUB(DATE_FORMAT(tbl_crm_participant.created, "%Y-%m-%d"), INTERVAL 1 WEEK )) as allcreatedinthisweek');

        $this->db->from('tbl_crm_participant');
        $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');

        $this->db->group_by("tbl_crm_participant_address.postal");
        $postcode = $this->db->get()->result();
        $src_columns = array('count(tbl_crm_participant.stage_status) as count',
        // 'CASE  WHEN   tbl_crm_stage.stage_id=1 then count(tbl_crm_participant.stage_id) as stage1Count',
        'tbl_crm_participant.id', 'tbl_crm_participant.stage_status', 'tbl_crm_participant_address.postal', 'tbl_crm_participant_address.lat', 'tbl_crm_participant_address.long', 'tbl_crm_stage.parent_id', 'tbl_crm_stage.name',);
        $data = array();
        $pos2 = array();

        foreach ($postcode as $val2) {
            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $src_columns)), false);
            $this->db->select('COUNT(DATE_SUB(DATE_FORMAT(tbl_crm_participant_stage.created, "%Y-%m-%d"), INTERVAL 1 WEEK )) as allstage');
            $this->db->from(TBL_PREFIX . 'crm_participant');
            $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');
            $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'left');
            $this->db->join('tbl_crm_participant_stage', 'tbl_crm_participant_stage.crm_participant_id = tbl_crm_participant.id', 'left');
            $this->db->where('tbl_crm_participant_address.postal=' . $val2->postal);
            if ($filter == 0) {
                $this->db->group_by("tbl_crm_stage.parent_id");
            } else if ($filter == 1) {
                $this->db->where('tbl_crm_participant.stage_status=' . $filter);
                $this->db->group_by("tbl_crm_participant.stage_status");
            } else {
                $this->db->where('tbl_crm_stage.parent_id=' . $filter);
                $this->db->group_by("tbl_crm_participant.stage_status");
            }
            $query = $this->db->get()->result();
            $data = $query;
            $pos = array();
            foreach ($data as $key => $val) {
                if ($filter == 0) {
                    switch ($val->parent_id) {
                        case 0:
                            $pos['stage1'] = ($val->count) ? $val->count : 0;
                        break;
                        case 2:
                            $pos['stage2'] = ($val->count) ? $val->count : 0;
                        break;
                        case 3:
                            $pos['stage3'] = ($val->count) ? $val->count : 0;
                        break;
                            //default:$pos[$val->postal] =  $val->postal;

                    }
                    $pos['allcreatedinthisweek'] = $val2->allcreatedinthisweek;
                } else {
                    $pos['stage' . $val->stage_status] = $val->count;
                    $pos['stage_count'] = ($val->allstage) ? $val->allstage : 0;
                }

                $pos['latitude'] = $val->lat;
                $pos['longitude'] = $val->long;
                $pos['postal'] = $val->postal;
                $pos['parent_id'] = $val->parent_id;
                $pos['title'] = $val->name;
                $pos2[] = $pos;
            }
        }
        return $pos2;
    }
    public function crm_participant_bar_status() {
        $tbl_1 = TBL_PREFIX . 'crm_participant';
        $select_column = array("SUM(CASE WHEN booking_status = 4 THEN 1 ELSE 0 END) AS Processing,
        SUM(CASE WHEN booking_status = 5 THEN 1 ELSE 0 END) AS Rejected,
        SUM(CASE WHEN booking_status = 3 THEN 1 ELSE 0 END) AS Successful, MONTH(created) as month");
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $this->db->group_by('MONTH(created)');
        $all_status = $this->db->get()->result_array();
        $getCount = array(array('Month', 'Not Accepted', 'Pending', 'Successful'));
        $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
        foreach ($months as $k => $v) {
            if (!empty($all_status)) {
                $getData = $this->getMonthsCount($all_status, $k, $v);
            }
            if (empty($getData)) {
                $getCount[] = array($v, 0, 0, 0);
            } else {
                $getCount[] = $getData;
            }
        }
        $return = array('data' => $getCount, 'status' => true);
        return $return;
    }
    function getMonthsCount($all_status, $monthNumber, $monthName) {
        $values = array();
        foreach ($all_status as $m => $status) {
            if ($monthNumber == $status['month']) {
                $values = array($monthName, (int)$status['Rejected'], (int)$status['Processing'], (int)$status['Successful']);
            }
        }
        return $values;
    }
    function crm_participant_intake_volume_left($intakeData=''){
        $tbl_1 = TBL_PREFIX . 'crm_participant';
        $select_column = array("*");
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        if(isset($intakeData->left_drop_down->value))
        {
            if($intakeData->left_drop_down->value==1)
            {

                $this->db->where('MONTH('.$tbl_1.'.booking_date)= MONTH(CURRENT_DATE) and YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }
            if($intakeData->left_drop_down->value==2)
            {

                $this->db->where('QUARTER('.$tbl_1.'.booking_date)= QUARTER(CURRENT_DATE) and YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }
            if($intakeData->left_drop_down->value==3)
            {

                $this->db->where('YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }

        }
        $result = $this->db->get()->result_array();
            $count_participant=count($result);
            $contact_user=0;$intake_user=0;$plandeligation_user=0;
            $intake_volume=array();
            foreach ($result as $participant_info) {
                if($participant_info['stage_status']<=1)
                {
                    $contact_user++;
                }
                if(4<=$participant_info['stage_status'] && $participant_info['stage_status']<8)
                {

                    $intake_user++;
                }
                if($participant_info['stage_status']>=8)
                {
                    $plandeligation_user++;
                }
            }
            $intake_volume['contact_start']=$count_participant;
            $intake_volume['contact_complete']=$count_participant-$contact_user;
            $intake_volume['contact_complete']=($intake_volume['contact_complete']<0)?0:$intake_volume['contact_complete'];
            $intake_volume['intake_start']=$intake_volume['contact_complete'];
            $intake_volume['intake_complete']=$intake_volume['contact_complete']-$intake_user;
            $intake_volume['intake_complete']=$intake_volume['intake_complete'];
            $intake_volume['plandeligation_start']=$intake_volume['intake_complete'];
            $intake_volume['plandeligation_complete']=$intake_volume['intake_complete']-$plandeligation_user;
            $intake_volume['plandeligation_complete']=$intake_volume['plandeligation_complete'];

      return  $intake_volume;
    }
    function crm_participant_intake_volume_percentage_left($intakeData=''){
        $volume_data=$this->crm_participant_intake_volume_left($intakeData);
        $total_contact_starts= $volume_data['contact_start'];
        $total_contact_completes= $volume_data['contact_complete'];
        $total_intake_starts=  $volume_data["intake_start"];
        $total_plandeligation_starts= $volume_data["plandeligation_start"];

        $tbl_1 = TBL_PREFIX . 'crm_participant';
        $tbl_2 = TBL_PREFIX . 'crm_stage';
        $tbl_3 = TBL_PREFIX . 'crm_participant_stage';

        $select_column = array("*");
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        if(isset($intakeData->left_drop_down->value))
        {
            if($intakeData->left_drop_down->value==1)
            {

                $this->db->where('MONTH('.$tbl_1.'.booking_date)= MONTH(CURRENT_DATE) and YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }
            if($intakeData->left_drop_down->value==2)
            {

                $this->db->where('QUARTER('.$tbl_1.'.booking_date)= QUARTER(CURRENT_DATE) and YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }
            if($intakeData->left_drop_down->value==3)
            {

                $this->db->where('YEAR('.$tbl_1.'.booking_date)= YEAR(CURRENT_DATE)');


            }
        }
        if(isset($intakeData->right_drop_down->value))
        {
            $this->db->where('MONTH('.$tbl_1.'.booking_date)=' . $intakeData->right_drop_down->value);

        }
        $result = $this->db->get()->result_array();
        $select_column1 = array("id","name");
        $this->db->select($select_column1);
        $this->db->from($tbl_2);
        $result1 = $this->db->get()->result_array();


        $intake_volume=array();
        foreach ($result1 as $stage_info) {
            $success=0;$reject=0;
            foreach ($result as $participant_info) {
                $select_column2 = array("status");
                $this->db->select($select_column2);
                $this->db->from($tbl_3);
                $this->db->where('crm_participant_id='.$participant_info['id'].' and stage_id='.$stage_info['id']);
                $result2 = $this->db->get()->result_array();
                if( $result2)
                {
                    $status=array_column($result2,"status");

                    if($status[0]==1)
                    {
                        ++$success;
                    }
                    else if($status[0]==2){

                        ++$reject;
                    }
                }

            }

            if($stage_info['id']<=1)
            {
                if($total_contact_starts !=0)
                {

                    $intake_volume[$stage_info['id']]['success']= round(($total_contact_completes/ $total_contact_starts)*100,1);
                    $intake_volume[$stage_info['id']]['reject']= round(($success/$total_contact_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }
            }
            if(4<=$stage_info['id'] && $stage_info['id']<8)
            {
                if($total_intake_starts !=0)
                {

                   $intake_volume[$stage_info['id']]['success']= round(($success/$total_intake_starts)*100,1);
                   $intake_volume[$stage_info['id']]['reject']=round(($reject/$total_intake_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }

            }
            if($stage_info['id']>=8)
            {
                if($total_plandeligation_starts !=0)
                {
                    $intake_volume[$stage_info['id']]['success']= round(($success/$total_plandeligation_starts)*100,1);
                    $intake_volume[$stage_info['id']]['reject']=round(($reject/$total_plandeligation_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }
            }

        }

       return  $intake_volume;
    }
    function crm_participant_intake_volume_right($intakeData=''){

        $tbl_1 = TBL_PREFIX . 'crm_participant';
        $select_column = array("*");
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        if(isset($intakeData->right_drop_down->value))
        {
            $month_year=explode(',',$intakeData->right_drop_down->label);

            if($intakeData->left_drop_down->value==1)
            {

              $this->db->where('MONTHNAME('.$tbl_1.'.booking_date)="' . $month_year[0].'" and YEAR('.$tbl_1.'.booking_date)= "'.$month_year[1].'"');
            }
            if($intakeData->left_drop_down->value==2)
            {
                $quater=floor((date("m", strtotime($month_year[2])) - 1) / 3) + 1;
                $this->db->where('QUARTER('.$tbl_1.'.booking_date)= "' .$quater.'" and YEAR('.$tbl_1.'.booking_date)= "' . $month_year[3].'"');


            }
            if($intakeData->left_drop_down->value==3)
            {

                $this->db->where('YEAR('.$tbl_1.'.booking_date)= "' . $intakeData->right_drop_down->label.'"');


            }
        }
        $result = $this->db->get()->result_array();

        $count_participant=count($result);
        $contact_user=0;$intake_user=0;$plandeligation_user=0;
        $intake_volume=array();
        foreach ($result as $participant_info) {
            if($participant_info['stage_status']<=1)
            {
                $contact_user++;
            }
            if(4<=$participant_info['stage_status'] && $participant_info['stage_status']<8)
            {

                $intake_user++;
            }
            if($participant_info['stage_status']>=8)
            {
                $plandeligation_user++;
            }
        }
        $intake_volume['contact_start']=$count_participant;
        $intake_volume['contact_complete']=$count_participant-$contact_user;
        $intake_volume['contact_complete']=($intake_volume['contact_complete']<0)?0:$intake_volume['contact_complete'];
        $intake_volume['intake_start']=$intake_volume['contact_complete'];
        $intake_volume['intake_complete']=$intake_volume['contact_complete']-$intake_user;
        $intake_volume['intake_complete']=$intake_volume['intake_complete'];
        $intake_volume['plandeligation_start']=$intake_volume['intake_complete'];
        $intake_volume['plandeligation_complete']=$intake_volume['intake_complete']-$plandeligation_user;
        $intake_volume['plandeligation_complete']=$intake_volume['plandeligation_complete'];
        return  $intake_volume;
    }

    function crm_participant_intake_volume_percentage_right($intakeData=''){
        $volume_data=$this->crm_participant_intake_volume_right($intakeData);
        $total_contact_starts= $volume_data['contact_start'];
        $total_contact_completes= $volume_data['contact_complete'];
        $total_intake_starts=  $volume_data["intake_start"];
        $total_plandeligation_starts= $volume_data["plandeligation_start"];

        $tbl_1 = TBL_PREFIX . 'crm_participant';
        $tbl_2 = TBL_PREFIX . 'crm_stage';
        $tbl_3 = TBL_PREFIX . 'crm_participant_stage';

        $select_column = array("*");
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        if(isset($intakeData->right_drop_down->value))
        {
            $month_year=explode(',',$intakeData->right_drop_down->label);
            if($intakeData->left_drop_down->value==1)
            {

                $this->db->where('MONTHNAME('.$tbl_1.'.booking_date)= "' . $month_year[0].'" and YEAR('.$tbl_1.'.booking_date)= "' . $month_year[1].'"');


            }
            if($intakeData->left_drop_down->value==2)
            {
                $quater=floor((date("m", strtotime($month_year[2])) - 1) / 3) + 1;
                $this->db->where('QUARTER('.$tbl_1.'.booking_date)= "'.$quater.'" and YEAR('.$tbl_1.'.booking_date)= "' . $month_year[3].'"');


            }
            if($intakeData->left_drop_down->value==3)
            {

                $this->db->where('YEAR('.$tbl_1.'.booking_date)="' . $intakeData->right_drop_down->label.'"');


            }
        }
        $result = $this->db->get()->result_array();

        $select_column1 = array("id","name");
        $this->db->select($select_column1);
        $this->db->from($tbl_2);
        $result1 = $this->db->get()->result_array();


        $intake_volume=array();
        foreach ($result1 as $stage_info) {
            $success=0;$reject=0;
            foreach ($result as $participant_info) {
                $select_column2 = array("status");
                $this->db->select($select_column2);
                $this->db->from($tbl_3);
                $this->db->where('crm_participant_id='.$participant_info['id'].' and stage_id='.$stage_info['id']);
                $result2 = $this->db->get()->result_array();
                if( $result2)
                {
                    $status=array_column($result2,"status");

                    if($status[0]==1)
                    {
                        ++$success;
                    }
                    else if($status[0]==2){

                        ++$reject;
                    }
                }

            }

            if($stage_info['id']<=1)
            {
                if($total_contact_starts !=0)
                {

                    $intake_volume[$stage_info['id']]['success']= round(($total_contact_completes/ $total_contact_starts)*100,1);
                    $intake_volume[$stage_info['id']]['reject']= round(($success/$total_contact_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }
            }
            if(4<=$stage_info['id'] && $stage_info['id']<8)
            {
                if($total_intake_starts !=0)
                {

                   $intake_volume[$stage_info['id']]['success']= round(($success/$total_intake_starts)*100,1);
                   $intake_volume[$stage_info['id']]['reject']=round(($reject/$total_intake_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }

            }
            if($stage_info['id']>=8)
            {
                if($total_plandeligation_starts !=0)
                {
                    $intake_volume[$stage_info['id']]['success']= round(($success/$total_plandeligation_starts)*100,1);
                    $intake_volume[$stage_info['id']]['reject']=round(($reject/$total_plandeligation_starts)*100,1);
                }
                else{
                    $intake_volume[$stage_info['id']]['success']=0;
                    $intake_volume[$stage_info['id']]['reject']=0;
                }
            }

        }

       return  $intake_volume;
    }
    public function service_analytics($reqData)
    {
      $tbl_1 = TBL_PREFIX.'crm_participant';
      $tbl_2 = TBL_PREFIX.'crm_participant_address';
      $select_column = ('ndis_plan,tbl_crm_participant_address.postal,

      SUM(CASE WHEN ndis_plan=2  THEN 1 ELSE 0 END) AS portal,
      SUM(CASE WHEN ndis_plan=3  THEN 1 ELSE 0 END) AS thirdparty,
      SUM(CASE WHEN ndis_plan=1  THEN 1 ELSE 0 END) AS private,

      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) between 0 And 18 THEN 1 ELSE 0 END) AS m18,
      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) between 18 And 25   THEN 1 ELSE 0 END) AS m1825,
      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) between 25 And 35   THEN 1 ELSE 0 END) AS m2535,
      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) between 35 And 45   THEN 1 ELSE 0 END) AS m3545,
      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) between 45 And 60   THEN 1 ELSE 0 END) AS m4560,
      SUM(CASE WHEN gender=1 AND YEAR(CURDATE()) - YEAR(dob) > 60   THEN 1 ELSE 0 END) AS m60plus,

      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) between 0 And 18 THEN 1 ELSE 0 END) AS f18,
      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) between 18 And 25   THEN 1 ELSE 0 END) AS f1825,
      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) between 25 And 35   THEN 1 ELSE 0 END) AS f2535,
      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) between 35 And 45   THEN 1 ELSE 0 END) AS f3545,
      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) between 45 And 60   THEN 1 ELSE 0 END) AS f4560,
      SUM(CASE WHEN gender=0 AND YEAR(CURDATE()) - YEAR(dob) > 60   THEN 1 ELSE 0 END) AS f60plus
      '


      );
// 3000, 3220,3233,3500,3850



      $this->db->select($select_column);
      $this->db->from($tbl_1);
      $this->db->join($tbl_2, $tbl_2.'.crm_participant_id = '.$tbl_1.'.id', 'left');
      $this->db->group_by('ndis_plan');
      $all_plans = $this->db->get()->result();


      // $plan1 = 0; $plan2=0; $plan3=0;
        $count1 = 0; $count2=0; $count3=0;
      // $values = array();
       $v = array();
       $v1 = array();
       $v2 = array();
       $v3 = array();
       $graphData= array();
      // $today = date("Y-m-d");
      $ageDetailss= array();
      $cityDetails = array('portal'=>array(),'private'=>array(),'thirdparty'=>array());
      $markerDetails = array('portal'=>array(),'private'=>array(),'thirdparty'=>array());
        foreach($all_plans as $plan){
          switch($plan->ndis_plan){
            case '2' : $v1 =  $plan->portal;
              $ageDetailss['portal'] = array(array(
                'title'=> 'U18', 'data'=> array( 'male'=> $plan->m18, 'female'=> $plan->f18 ) ),
              array(  'title'=> '18-25', 'data'=> array(  'male'=> $plan->m1825, 'female'=> $plan->f1825 )),
              array(  'title'=> '25-35', 'data'=> array(  'male'=> $plan->m2535, 'female'=> $plan->f2535 )),
              array(  'title'=> '35-45', 'data'=> array(  'male'=> $plan->m3545, 'female'=> $plan->f3545 )),
              array(  'title'=> '45-60', 'data'=> array(  'male'=> $plan->m4560, 'female'=> $plan->f4560 )),
              array(  'title'=> '60+', 'data'=> array(  'male'=> $plan->m60plus, 'female'=> $plan->f60plus )
            ));

            break;

            case '3' : $v2 =  $plan->thirdparty;
            $ageDetailss['thirdparty'] = array(
            array(    'title'=> 'U18', 'data'=> array( 'male'=> $plan->m18, 'female'=> $plan->f18 )),
            array(    'title'=> '18-25', 'data'=> array( 'male'=> $plan->m1825, 'female'=> $plan->f1825 )),
            array(    'title'=> '25-35', 'data'=> array( 'male'=> $plan->m2535, 'female'=> $plan->f2535 )),
            array(    'title'=> '35-45', 'data'=> array( 'male'=> $plan->m3545, 'female'=> $plan->f3545 )),
            array(    'title'=> '45-60', 'data'=> array( 'male'=> $plan->m4560, 'female'=> $plan->f4560 )),
            array(    'title'=> '60+', 'data'=> array( 'male'=> $plan->m60plus, 'female'=> $plan->f60plus )),
            );


            break;
            case '1' : $v3 =  $plan->private;
            $ageDetailss['private'] = array(
                  array( 'title'=> 'U18', 'data'=> array( 'male'=> $plan->m18, 'female'=> $plan->f18 )),
                  array( 'title'=> '18-25', 'data'=> array( 'male'=> $plan->m1825, 'female'=> $plan->f1825 )),
                  array( 'title'=> '25-35', 'data'=> array( 'male'=> $plan->m2535, 'female'=> $plan->f2535 )),
                  array( 'title'=> '35-45', 'data'=> array( 'male'=> $plan->m3545, 'female'=> $plan->f3545 )),
                  array( 'title'=> '45-60', 'data'=> array( 'male'=> $plan->m4560, 'female'=> $plan->f4560 )),
                  array( 'title'=> '60+', 'data'=> array( 'male'=> $plan->m60plus, 'female'=> $plan->f60plus )),
            );


            break;
          }

        }
        $this->db->select("count(tbl_crm_participant.id) as count,tbl_crm_participant_address.postal,tbl_crm_participant.ndis_plan,tbl_crm_participant_address.lat,tbl_crm_participant_address.long,tbl_crm_participant_address.street");
        $this->db->from('tbl_crm_participant');
        $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');
        $this->db->group_by("tbl_crm_participant_address.postal");
        $this->db->group_by("tbl_crm_participant.ndis_plan");
        $postcodes = $this->db->get()->result();
        foreach($postcodes as $postcode){
          switch($postcode->ndis_plan){
            case '1' :
              $markerDetails['private'][] =
                array( 'title' => $postcode->street, 'latitude'=>$postcode->lat, 'longitude' => $postcode->long,'count'=>$postcode->count);

            break;
            case '2' :
            $markerDetails['portal'][] =
              array( 'title' => $postcode->street, 'latitude'=>$postcode->lat, 'longitude' => $postcode->long,'count'=>$postcode->count);
            break;
            case '3' :
            $markerDetails['thirdparty'][] =
              array( 'title' => $postcode->street, 'latitude'=>$postcode->lat, 'longitude' => $postcode->long,'count'=>$postcode->count);
            break;
          }
        }
        $cityDetails['private'] = $this->get_hotspot($markerDetails['private']);
        $cityDetails['portal'] = $this->get_hotspot($markerDetails['portal']);
        $cityDetails['thirdparty'] = $this->get_hotspot($markerDetails['thirdparty']);
        $v=array($v1,$v2,$v3);
        $graphData=array('portal'=>$v1,'thirdparty'=>$v2,'private'=>$v3);

      echo json_encode(array('countdata'=>$graphData, 'count'=> $v, 'ageDetails' =>$ageDetailss, 'cityDetails'=>$cityDetails, 'markerDetails' =>$markerDetails , 'status' => TRUE));
      exit();
    }
    public function get_hotspot($array) {

        $all = $array;
        $position = 0;
        $popular = $all;
        usort($popular, function($a, $b) {
            return $b['count'] - $a['count'];
        });
        $data = array_slice($popular, 0, 5);

       return $data;
        }
}
