<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
// Call the CI_Model constructor
        parent::__construct();
    }

    public function get_count_all_crmparticipant($adminId) {
        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($adminId);
        $tb1 = TBL_PREFIX . 'crm_participant';
        $this->db->from($tb1);
        if ($role[0]->permission == 'access_crm') {
            $this->db->join('tbl_crm_participant_schedule_task', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
            $this->db->where('tbl_crm_participant_schedule_task.assign_to', $adminId);
        }
        $query = $this->db->get();
        return $result = $query->num_rows();
    }

    public function get_count_all_crmparticipant_status($adminId) {
        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($adminId);
        $tb1 = TBL_PREFIX . 'crm_participant';
        $this->db->from($tb1);
        if ($role[0]->permission == 'access_crm') {
            $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
            $this->db->where('tbl_crm_participant_schedule_task.assign_to', $adminId);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_testimonial() {
        $tbl_testimonial = TBL_PREFIX . 'testimonial';
        $tbl_participant = TBL_PREFIX . 'participant';

        $this->db->select(array($tbl_testimonial . '.title', $tbl_testimonial . '.testimonial'));
        $this->db->select("concat(firstname,' ',middlename,' ',lastname) as name");


        $this->db->from($tbl_testimonial);
        $this->db->join($tbl_participant, $tbl_participant . '.id =' . $tbl_testimonial . '.userId', 'inner');

        $this->db->where(array($tbl_testimonial . '.user_type' => 2));
        $this->db->order_by('RAND()');
        $this->db->limit(1);


        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        return $query->result();
    }

    function get_participant_phone($participantId) {
        $tbl_participant_phone = TBL_PREFIX . 'participant_phone';
        $this->db->select(array($tbl_participant_phone . '.phone'));
        $this->db->from($tbl_participant_phone);
        $this->db->where($tbl_participant_phone . '.participantId', $participantId);

        $query = $this->db->get();

        $participant_number = $query->result_array();

        $number = array();
        if (!empty($participant_number)) {
            foreach ($participant_number as $value) {
                $number[] = $value['phone'];
            }
        }
        return implode(',', $number);
    }

    public function get_imail_call_list($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $participantId = $reqData->participantId;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                if ($sorted[0]->id == 'time') {
                    $orderBy = $sorted[0]->id;
                } else {
                    $orderBy = $sorted[0]->id;
                }
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'created';
            $direction = 'desc';
        }

// searching for imail
        $tbl_external_message = TBL_PREFIX . 'external_message';
        $tbl_external_message_content = TBL_PREFIX . 'external_message_content';


        if (!empty($filter->search)) {
            $search = $this->db->escape_str($filter->search);
            $this->db->where($tbl_external_message_content . '.adminId', $search);
        }

        $sWhere = array('tbl_external_message.recipient_type' => '2', 'tbl_external_message_content.sender_type' => 1, 'tbl_external_message_content.userId' => $participantId);

        $this->db->select(array($tbl_external_message . '.id as ID', $tbl_external_message_content . '.adminId as HCMID', "tbl_participant_email.email as contact_used", "DATE_FORMAT(tbl_external_message.created, '%d/%m/%Y') as created", "'N/A' as duration", "'Imail' as  type"));

        $this->db->from($tbl_external_message);
        $this->db->join($tbl_external_message_content, $tbl_external_message . '.id = ' . $tbl_external_message_content . '.messageId', 'left');
        $this->db->join('tbl_participant_email', 'tbl_participant_email.participantId = ' . $tbl_external_message_content . '.userId AND tbl_participant_email.primary_email = 1', 'left');
        $this->db->where($sWhere);

        $this->db->group_by($tbl_external_message . '.id');
        $query1 = $this->db->get_compiled_select();


// get from here participant contact (phone)
        $all_number = $this->get_participant_phone($participantId);



// searching for call
        if (!empty($filter->search)) {
            $search = $this->db->escape_str($filter->search);
            $this->db->where('tbl_member_phone.memberId', $search);
        }

        $tbl_call_log = TBL_PREFIX . 'call_log';
        $this->db->select(array($tbl_call_log . '.id as ID', "tbl_member_phone.memberId as HCMID", $tbl_call_log . ".receiver_number as contact_used", "DATE_FORMAT(tbl_call_log.created, '%d/%m/%Y') as created", $tbl_call_log . ".duration", "'Call' as  type"));
        $this->db->from($tbl_call_log);
        $this->db->join('tbl_participant_phone', 'tbl_participant_phone.phone = tbl_call_log.receiver_number AND tbl_participant_phone.participantId = ' . $participantId, 'inner');
        $this->db->join('tbl_member_phone', 'tbl_member_phone.phone = tbl_call_log.caller_number', 'inner');
        $this->db->where_in('tbl_participant_phone.phone', $all_number);
        $query2 = $this->db->get_compiled_select();


// combine query and get data
        $limit_string = "limit " . $limit;
        if ($page > 0) {
            $limit_string = "limit " . $limit . ", " . ($page * $limit);
        }
        $query = $this->db->query($query1 . " UNION " . $query2 . " order by " . $orderBy . " " . $direction . " " . $limit_string);
        $result = $query->result();


        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $return = array('count' => $dt_filtered_total, 'data' => $result);
        return $return;
    }

    public function get_all_crm_logs($reqData) {
        // var_dump($reqData);exit;
        $tbl_1 = TBL_PREFIX . 'logs';
        $tbl_2 = TBL_PREFIX . 'member';
        $tbl_3 = TBL_PREFIX . 'crm_participant';
        $tbl_4 = TBL_PREFIX . 'crm_participant_roster';
        $tbl_5 = TBL_PREFIX . 'crm_participant_schedule_task';

        $select_column = array('concat(' . $tbl_3 . '.firstname," ",' . $tbl_3 . '.lastname) as participant_name',
            '(CASE WHEN ' . $tbl_3 . '.booking_status=2 THEN "Parked"
                    WHEN ' . $tbl_3 . '.booking_status=3 THEN "Activated"
                    WHEN ' . $tbl_3 . '.booking_status=4 THEN "Activate"
                    WHEN ' . $tbl_3 . '.booking_status=5 THEN "Rejected" END) as action',
            'concat(' . $tbl_2 . '.firstname," ",' . $tbl_2 . '.lastname) as FullName',
            $tbl_1 . '.title', 'DATE_FORMAT(' . $tbl_1 . '.created,"%d-%m-%Y %h:%i:%s") as created', $tbl_1 . '.userId'
        );
        $sWhere = array($tbl_1 . '.module' => '10', "DATE(" . $tbl_1 . ".created)" => date('Y-m-d'));
        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, $tbl_2 . '.id = ' . $tbl_1 . '.created_by ', 'left');
        $this->db->join($tbl_3, $tbl_3 . '.id = ' . $tbl_1 . '.userId', 'left');
        $this->db->join($tbl_4, $tbl_4 . '.participantId = ' . $tbl_3 . '.id', 'left');
        $this->db->join($tbl_5, $tbl_5 . '.crm_participant_id = ' . $tbl_3 . '.id', 'left');
        if (!empty($reqData->participantId)) {
            $where = array($tbl_1 . '.userId' => $reqData->participantId);
            $this->db->where($where);
        } else {
            $this->db->where($sWhere);
        }
        $this->db->order_by($tbl_1 . '.id', 'desc');

        $this->db->limit(5);

        $query = $this->db->get();
        // var_dump($return);
        $data = array_reverse($query->result());

        $result = array_reverse(// Reverse array to the initial order.
                array_values(// Get rid of string keys (make array indexed again).
                        array_combine(// Create array taking keys from column and values from the base array.
                                array_column($data, 'participant_name'), $data
                        )
                )
        );
        // echo $this->db->last_query();
        $return = array('data' => $result);
        return $return;
    }

    public function latest_stage_updates($reqData) {
        // var_dump($reqData);exit;
        $tbl_1 = TBL_PREFIX . 'logs';
        $tbl_2 = TBL_PREFIX . 'member';
        $tbl_3 = TBL_PREFIX . 'crm_participant';

        $select_column = array($tbl_1 . '.specific_title as action', 'concat(' . $tbl_3 . '.firstname," ",' . $tbl_3 . '.lastname) as participant_name',
            'concat(' . $tbl_2 . '.firstname," ",' . $tbl_2 . '.lastname) as FullName',
            $tbl_1 . '.title', 'DATE_FORMAT(' . $tbl_1 . '.created,"%d-%m-%Y %h:%i:%s") as created', $tbl_1 . '.userId'
        );
        $sWhere = array($tbl_1 . '.module' => '10', $tbl_1 . '.sub_module' => '19', "DATE(" . $tbl_1 . ".created)" => date('Y-m-d'));

        $this->db->select($select_column);
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, $tbl_2 . '.id = ' . $tbl_1 . '.created_by ', 'left');
        $this->db->join($tbl_3, $tbl_3 . '.id = ' . $tbl_1 . '.userId', 'left');

        if (!empty($reqData->participantId)) {
            $where = array($tbl_1 . '.userId' => $reqData->participantId);
            $this->db->where($where);
        } else {
            $this->db->where($sWhere);
        }
        $this->db->where("tbl_logs.specific_title !=''");
        $this->db->order_by($tbl_1 . '.id', 'desc');
        $this->db->limit(5);
        $this->db->order_by('date(tbl_logs.created)','DESC');
        $this->db->order_by('time(tbl_logs.created)','DESC');

        $query = $this->db->get();
        $data = ($query->result());

        $return = array('data' => $data);
        return $return;
    }

    public function get_crm_task_list($adminId) {
        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($adminId);
        $status_con = array(
            1
        );
        $item1 = '';
        $item2 = '';
        $dataResult = array();
        $role_array = array();
        foreach ($role as $value) {
            array_push($role_array, $value->permission);
        }
        if (in_array('access_crm', $role_array)) {

            $tbl_1 = 'tbl_crm_participant';
            $select_column = array(
                'concat(' . $tbl_1 . '.firstname," ",' . $tbl_1 . '.lastname) as full_name',
                'tbl_crm_participant.status', 'tbl_crm_participant_schedule_task.id as task_id', 'tbl_crm_participant_schedule_task.task_name',
                'DATE_FORMAT(tbl_crm_participant_schedule_task.due_date,"%d-%m-%Y") as due_date', 'tbl_crm_participant.action_status');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_participant_schedule_task');
            $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'inner');
            $this->db->join('tbl_member', 'tbl_member.id = tbl_crm_participant.assigned_to', 'inner');
            $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'inner');


            $this->db->where_in('tbl_crm_participant.status', $status_con);
            #$this->db->where('tbl_crm_participant_schedule_task.task_status<>1');
            $this->db->where('tbl_crm_participant_schedule_task.archive !=', 1);
            $this->db->where('date(tbl_crm_participant_schedule_task.due_date) <= DATE_ADD(CURDATE(), INTERVAL +5 DAY) and date(tbl_crm_participant_schedule_task.due_date)>DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

            $this->db->order_by('tbl_crm_participant_schedule_task.id', 'desc');
            $this->db->limit(10);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            //last_query();
            $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['taskname'] = $val->task_name;
                    $row['duedate'] = $val->due_date;
                    $row['task_id'] = $val->task_id;
                    $row['fullname'] = $val->full_name;
                    $row['action'] = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                    $dataResult[] = $row;
                }
            }
        }
        $return = array(
            'data' => $dataResult
        );
        return $return;
    }

    public function get_crm_task_list_user($adminId) {
        require_once APPPATH . 'Classes/crm/CrmRole.php';
//        $admins_its = get_super_admins();

        $permission = new classRoles\Roles();
        $role = $permission->getRole($adminId);
        $status_con = array(
            1
        );
        $item1 = '';
        $item2 = '';
        $dataResult = array();
        $role_array = array();
        foreach ($role as $value) {
            array_push($role_array, $value->permission);
        }
        if (in_array('access_crm', $role_array)) {

            $tbl_1 = 'tbl_crm_participant';
            $select_column = array(
                'concat(' . $tbl_1 . '.firstname," ",' . $tbl_1 . '.lastname) as full_name',
                'tbl_crm_participant.status', 'tbl_crm_participant_schedule_task.id as task_id', 'tbl_crm_participant_schedule_task.task_name',
                'DATE_FORMAT(tbl_crm_participant_schedule_task.due_date,"%d-%m-%Y") as due_date', 'tbl_crm_participant.action_status');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_participant_schedule_task');
            $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
            $this->db->where_in('tbl_crm_participant.status', $status_con);
            $this->db->where('tbl_crm_participant_schedule_task.task_status<>1');
            $this->db->where('tbl_crm_participant_schedule_task.archive !=', 1);
            $this->db->where('date(tbl_crm_participant_schedule_task.due_date) <= DATE_ADD(CURDATE(), INTERVAL +5 DAY) and date(tbl_crm_participant_schedule_task.due_date)>DATE_ADD(CURDATE(), INTERVAL -1 DAY)');
            // $its_super_admin = in_array($adminId, $admins_its);
            // if (!$its_super_admin) {
                $this->db->where('tbl_crm_participant_schedule_task.assign_to', $adminId);
//            }

            $this->db->order_by('tbl_crm_participant_schedule_task.id', 'desc');
            $this->db->limit(10);
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $dataResult = array();
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['taskname'] = $val->task_name;
                    $row['duedate'] = $val->due_date;
                    $row['task_id'] = $val->task_id;
                    $row['fullname'] = $val->full_name;
                    $row['action'] = isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                    $dataResult[] = $row;
                }
            }
        }
        $return = array(
            'data' => $dataResult
        );
        return $return;
    }

    public function get_crm_latest_action($adminId) {
        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($adminId);
        $status_con = array(
            1
        );
        $item1 = '';
        $item2 = '';
        $dataResult = array();
        $role_array = array();
        foreach ($role as $value) {
            array_push($role_array, $value->permission);
        }
        if (in_array('access_crm', $role_array)) {
            $select_column = array(
                'tbl_crm_participant.status', 'tbl_crm_participant.id', 'concat(tbl_crm_participant.preferredname," ",tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
                'tbl_crm_participant.action_status', 'tbl_crm_participant.intake_type', 'DATE_FORMAT(tbl_crm_participant.created,"%d-%m-%Y") as created');

            $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
            $this->db->from(TBL_PREFIX . 'crm_participant');
            $this->db->where_in('tbl_crm_participant.status', $status_con);

            $admins_its = $this->config->item('super_admins');
            $its_super_admin = in_array($adminId, $admins_its);
            if (!$its_super_admin) {

                $this->db->where('tbl_crm_participant.assigned_to', $adminId);
            }

            $this->db->order_by('tbl_crm_participant.id', 'desc');
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            #last_query();
            $dataResult = array();
            $intake = ['', 'New', 'Rejected', 'Renewed', 'Returning','Modified'];
            #pr($intake);
            if (!empty($query->result())) {
                foreach ($query->result() as $val) {
                    $row = array();
                    $row['FullName'] = $val->FullName;
                    $row['duedate'] = $val->created;
                    $row['id'] = $val->id;
                    $row['stage_name'] = (in_array($val->intake_type, $intake))?$intake[$val->intake_type]:'';
                    $dataResult[] = $row;
                }
            }
        }

        $return = array(
            'data' => $dataResult
        );
        //  echo $this->db->last_query();
        return $return;
    }

    public function get_crm_latest_action_admin($adminId) {

        $status_con = array( 1 );

        $select_column = array(
            'tbl_crm_participant.status', 'tbl_crm_participant_schedule_task.id as task_id', 'tbl_crm_participant_schedule_task.task_name', 'tbl_crm_participant_schedule_task.archive',
            'DATE_FORMAT(tbl_crm_participant_schedule_task.due_date,"%d-%m-%Y") as due_date','DATE_FORMAT(tbl_crm_participant_schedule_task.updated_at,"%d-%m-%Y") as action_date', 'tbl_crm_participant_schedule_task.task_status', '(select concat(firstname," ",lastname) from tbl_member where id=tbl_crm_participant.assigned_to) as user'
        );

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'crm_participant_schedule_task');
        $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
        $this->db->where_in('tbl_crm_participant.status', $status_con);
        //$this->db->where('tbl_crm_participant_schedule_task.task_status<>1');
        //$this->db->where('date(created_at) >= DATE_ADD(CURDATE(), INTERVAL -5 DAY)');
		#$this->db->where('date(due_date) >= DATE_ADD(CURDATE(), INTERVAL -5 DAY) and date(tbl_crm_participant_schedule_task.due_date) <= CURDATE()');
        $this->db->where('date(updated_at) >= DATE_ADD(CURDATE(), INTERVAL -5 DAY) and date(tbl_crm_participant_schedule_task.updated_at) <= CURDATE()');
        $this->db->order_by('date(tbl_crm_participant_schedule_task.updated_at)','DESC');
        $this->db->order_by('time(tbl_crm_participant_schedule_task.updated_at)','DESC');
        $this->db->limit(10);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #echo $this->db->last_query();
        $dataResult = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['user'] = $val->user;
                $row['taskname'] = $val->task_name;
                $row['duedate'] = $val->due_date;
                $row['task_id'] = $val->task_id;
                $row['action_date'] = $val->action_date;
                if ($val->archive == 1) {
                    $row['action'] = 'Deleted';
                } else {
                    $row['action'] = isset($val->task_status) && $val->task_status == 1 ? 'Completed' : 'Created';
                }
                $dataResult[] = $row;
            }
        }
        $return = array(
            'data' => $dataResult
        );
        return $return;
    }

    public function crm_participant_member($post_data) {
        if ($post_data->view_type == 'year') {
            $where = "YEAR(created)=" . date("Y") . " AND status=1";
        } else if ($post_data->view_type == 'week') {
            $where = 'created BETWEEN (DATE_SUB(NOW(), INTERVAL 1 WEEK)) AND NOW() AND status=1';
        } else {
            $where = "MONTH(created) = " . date('m') . " AND status=1";
        }
        $participan = 0;
        $member = 0;
        $processing = 0;
        $values['participant'] = $this->db->query('select count(id) as participant from tbl_participant where ' . $where)->row()->participant;
        $values['member'] = $this->db->query('select count(id) as member from tbl_member where ' . $where)->row()->member;
        $values['processing'] = $this->db->query('select count(id) as processing from tbl_crm_participant where ' . $where)->row()->processing;
        $data[] = $values;
        return $values;
    }

}
