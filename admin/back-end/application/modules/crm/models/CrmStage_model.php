<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmStage_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
		$this->loges->setLogType('crm_prospective_participant');
    }

    public function create_intake_info($intakeInfo) {

        $arr_intake = array();
        $arr_intake['crm_participant_id'] = $intakeInfo->getParticipantid();
        $arr_intake['notes'] = $intakeInfo->getNote();
        $arr_intake['stage_id'] = $intakeInfo->getParticipantStageId();
        $arr_intake['status'] = 1;
        $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_stage_notes', $arr_intake);
        $intake_id = $this->db->insert_id();
        return $intake_id;

    }
    public function get_note_record_where($reqData)
    {
        if (!empty($reqData)) {
           $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
           $tbl_2 = TBL_PREFIX . 'crm_participant_stage_notes';
           $select_column = array();
           $sWhere = array();
           if(!empty($reqData['staff_id'])){


             $staff_id = $this->db->query("select id from tbl_crm_staff where admin_id=".$reqData['staff_id'])->row_array();

            $sWhere[$tbl_1.'.crm_member_id'] = $staff_id['id'];
           }
          if(!empty($reqData['stage_id'])){
             $sWhere[$tbl_1.'.stage_id'] = $reqData['stage_id'];

           }
           $select_column = array($tbl_2.'.*');
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2.'.crm_participant_id = '.$tbl_1.'.crm_participant_id AND '.$tbl_2.'.stage_id = '.$tbl_1.'.id', 'right');

            $this->db->where($sWhere);
            $query = $this->db->get()->result_array();
            return $query;
          }

    }
    public function get_docs_record_where($reqData)
    {

        if (!empty($reqData)) {
            $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
            $tbl_2 = TBL_PREFIX . 'crm_participant_docs';

            $select_column = array(

                $tbl_2.'.*',
                '(select name from tbl_crm_stage where id='.$reqData['stage_id'].') as stage_name',
                '(select concat(firstname," ",lastname) from tbl_member where id='.$reqData['staff_id'].') as member_name'


              );
            $sWhere = array($tbl_1.'.crm_member_id' => $reqData['staff_id'],$tbl_1.'.stage_id' => $reqData['stage_id']);
            $this->db->select($select_column);
            $this->db->from($tbl_1);
            $this->db->join($tbl_2, $tbl_2.'.crm_participant_id = '.$tbl_1.'.crm_participant_id AND '.$tbl_2.'.stage_id = '.$tbl_1.'.stage_id', 'right');

            $this->db->where($sWhere);
            $query = $this->db->get()->result_array();
            return $query;
          }
    }
    public function get_state_options($reqData){
      $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
      $tbl_2 = TBL_PREFIX . 'member';
      $column=array($tbl_1.'.stage_id',$tbl_1.'.status',  'concat(tbl_member.firstname," ",tbl_member.lastname) as name',$tbl_1.'.updated');
      // $column="stage_id,status,crm_member_id,DATE(updated) as updated";
      $this->db->select($column);
      $this->db->from($tbl_1);
      $this->db->join($tbl_2, $tbl_1.'.crm_member_id = '.$tbl_2.'.id ', 'right');
      $where=array($tbl_1.".crm_participant_id"=>$reqData['participant_id']);
      $this->db->where($where);
      $resullt = $this->db->get()->result_array();
     // echo $this->db->last_query();
      return $resullt ;
    }
    public function get_stage_tree_data($reqData){
      $tbl_1 = TBL_PREFIX . 'crm_participant_stage';
      $tbl_2 = TBL_PREFIX . 'crm_participant';
      $column=array($tbl_1.'.stage_id',$tbl_1.'.status',$tbl_1.'.updated',$tbl_1.'.created');
      $this->db->select($column);
      $this->db->from($tbl_1);
      $where=array($tbl_1.".crm_participant_id"=>$reqData['participant_id']);
      $this->db->where($where);
      $result1 = array();
      $result = $this->db->get()->result_array();
      for ($i=0; $i < count($result); $i++) {
        if($result[$i]['stage_id'] === '4' )
          $result1['stage1'] = date('d/m/Y h:i:s a', strtotime($result[$i]['created']));
        if($result[$i]['stage_id'] === '7'  && $result[$i]['status'] == 1)
          $result1['stage2'] = date('d/m/Y h:i:s a', strtotime($result[$i]['updated']));
        if($result[$i]['stage_id'] === '11'  && $result[$i]['status'] == 1)
          $result1['stage3'] = date('d/m/Y h:i:s a', strtotime($result[$i]['updated']));
      }
      return $result1 ;
    }

    public function update_stage_status($reqData,$adminId){
       $response = array();
       $success = 0;
       $p_status = false;
       $condition = array('id'=>$reqData['crm_participant_id']);
       $participant_status = $this->basic_model->get_row('crm_participant',array('booking_status'), $condition);
       if($participant_status->booking_status == 2 || $participant_status->booking_status == 3 || $participant_status->booking_status == 5){
         $p_status = true;
       }
       $crm_participant_stage="crm_participant_stage";
       $stage_where=array('crm_participant_id'=>$reqData['crm_participant_id'],'stage_id'=>$reqData['stage_id']);
       switch($reqData['status']){
         case '0': // Pending
            $stage_data=array('status'=>$reqData['status'],'crm_member_id'=>$adminId);
            $this->bulk_update_status($reqData['crm_participant_id'],$reqData['stage_id']);
            $res=  $this->insert_stage_data($stage_data,$stage_where,$reqData,$adminId);
            /*$intake_type= $this->basic_model->get_row('crm_participant',['intake_type'],['id'=>$reqData['crm_participant_id']]);
            if($intake_type==2){
              $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>$reqData['stage_id'],'intake_type'=>1),array('id'=>$reqData['crm_participant_id']));
            }*/
         break;
         case '1': //Success
         $result = '';
          $this->insert_stage_data(array('status'=>$reqData['status'],'crm_member_id'=>$adminId),$stage_where,$reqData,$adminId);
          $query = $this->basic_model->get_row('crm_participant_stage',array('id'),array('crm_participant_id'=>$reqData['crm_participant_id'],'stage_id'=>$reqData['next_stage']));
          $this->bulk_update_status($reqData['crm_participant_id'],$reqData['stage_id']);
          if($reqData['stage_id'] == 11 && $reqData['status']){
            $res = $this->set_participant_state($reqData['crm_participant_id'],$adminId);
            if(!empty($res)){
              $success = 1;
              //$result = $this->basic_model->update_records('crm_participant',array('intake_type'=>3),array('id'=>$reqData['crm_participant_id']));
            }
          }else
          $result = $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>$reqData['next_stage']),array('id'=>$reqData['crm_participant_id']));

          if(!empty($query)){
            $res = $this->basic_model->update_records('crm_participant_stage', array('status'=>3,'crm_member_id'=>$adminId), array('crm_participant_id'=>$reqData['crm_participant_id'],'stage_id'=>$reqData['next_stage']));
          }
          else{
              $res= $this->basic_model->insert_records('crm_participant_stage',array('crm_participant_id'=>$reqData['crm_participant_id'],'crm_member_id'=>$adminId,'stage_id'=>$reqData['next_stage'],'status'=>3));

            }

        break;
        case '2': // UnSuccess
         $stage_where=array('crm_participant_id'=>$reqData['crm_participant_id'],'stage_id'=>4);
          $this->basic_model->update_records('crm_participant',array('booking_status'=>5,'stage_status'=>4,'intake_type'=>2),array('id'=>$reqData['crm_participant_id']));
          $stage_data=array('status'=>$reqData['status'],'crm_member_id'=>$adminId);
          $note_arr['notes'] = $reqData['notes'];
          $note_arr['stage_id'] = $reqData['stage_id'];
          $note_arr['crm_participant_id'] = $reqData['crm_participant_id'];
          $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_stage_notes', $note_arr);
          $this->bulk_update_status($reqData['crm_participant_id'],4);
          $res=  $this->insert_stage_data($stage_data,$stage_where,$reqData,$adminId);

         break;
         case '3': //In-Progress
         $this->bulk_update_status($reqData['crm_participant_id'],$reqData['stage_id']);
         $stage_data=array('status'=>$reqData['status'],'crm_member_id'=>$adminId);
         $query = $this->basic_model->get_record_where('crm_participant',array('intake_type'),array('id'=>$reqData['crm_participant_id']));
         if(isset($query[0]->intake_type) && $query[0]->intake_type !=2){

           $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>$reqData['stage_id']),array('id'=>$reqData['crm_participant_id']));
         }
         else{
          $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>$reqData['stage_id']),array('id'=>$reqData['crm_participant_id']));
         }
          $res=  $this->insert_stage_data($stage_data,$stage_where,$reqData,$adminId);
         break;
         case '4': // Parked
          $this->bulk_update_status($reqData['crm_participant_id'],$reqData['stage_id']);
          $stage_data=array('status'=>$reqData['status'],'crm_member_id'=>$adminId);
          $res=  $this->insert_stage_data($stage_data,$stage_where,$reqData,$adminId);
          // $notesid = $this->create_intake_info($reqData);
          $task_arr = array();
          if(!empty($reqData['notes'])){
            $note_arr['notes'] = $reqData['notes'];
            $note_arr['stage_id'] = $reqData['stage_id'];
            $note_arr['crm_participant_id'] = $reqData['crm_participant_id'];
            $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_stage_notes', $note_arr);
          }
          if(!empty($reqData['task_name'])){
            $task_arr['crm_participant_id'] = $reqData['crm_participant_id'];
            $task_arr['task_name'] = $reqData['task_name'];
            $task_arr['priority'] = $reqData['priority'];
            $task_arr['due_date'] = $reqData['due_task'];
            $task_arr['assign_to'] = $reqData['assigned_to'];
            $tasksid =  $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_schedule_task', $task_arr);
          }
          $this->basic_model->update_records('crm_participant',array('stage_status'=>$reqData['stage_id'],'booking_status' => '2'),array('id'=>$reqData['crm_participant_id']));

         break;

       }


      if(!empty($res))
      {
          global $globalparticipant;
          require_once APPPATH . 'Classes/crm/CrmStage.php';
          $intake = new CrmIntakeClass\CrmStage();
          $stage_status='';
          $participantName = $intake->getParticipantName($reqData['crm_participant_id']);
          $stageName = $intake->getStageName($reqData['stage_id']);
          if($reqData['status'] == 2){
            $stage_status = " Rejected";
          }
          elseif ($reqData['status'] == 4){
            $stage_status = " Parked";
          }
          elseif ($reqData['status'] == 1 && $reqData['stage_id'] == 11 ) {
            $stage_status = " Activated";
          }
          if($p_status == true && $reqData['status'] != 2 && $reqData['status'] != 4 && !($reqData['status'] == 1 && $reqData['stage_id'] == 11 )){
            $stage_status = " Active";
          }

          $this->loges->setUserId($reqData['crm_participant_id']);
          $this->loges->setCreatedBy($adminId);
          $this->loges->setDescription(json_encode('New status in '.$stageName.' for participant ('.$participantName.')'));
          $this->loges->setTitle($stageName.' status changed');
          $this->loges->setSpecific_title($stage_status);
          $this->loges->createLog();
      $response = array('status' => true, 'msg'=>'Updated Data Successfully' ,'success' => $success);
      }
      else {
        $response = array('status' => false, 'msg'=>'UnSuccessfull. Please try again','success' => $success);
      }
      return $response;
  }
  function insert_stage_data($stage_data,$stage_where,$reqData,$adminId){
    $query = $this->basic_model->get_row('crm_participant_stage',array('id'),array('crm_participant_id'=>$reqData['crm_participant_id'],'stage_id'=>$reqData['stage_id']));
    if($query){
      $res = $this->basic_model->update_records('crm_participant_stage', $stage_data, $stage_where);
    }
    else{
        $res= $this->basic_model->insert_records('crm_participant_stage',array('crm_participant_id'=>$reqData['crm_participant_id'],'crm_member_id'=>$adminId,'stage_id'=>$reqData['stage_id'],'status'=>$reqData['status']));
    }
    return $res?true:false;
  }
  function bulk_update_status($participant_id,$stage_id){
    $stages = array();
    $participant_stages = $this->basic_model->get_record_where_orderby('crm_participant_stage',array('stage_id'),array('crm_participant_id'=>$participant_id),'id');
    foreach($participant_stages as $value){
      array_push($stages, $value->stage_id);
    }
    $update_data = array();
    $where = array();
    $ids=array_slice($stages,array_search($stage_id,$stages)+1);
     for($i=0;$i<count($ids);$i++){
      $query = $this->db->update('tbl_crm_participant_stage', array('status'=>0),array('stage_id'=>$ids[$i],'crm_participant_id'=>$participant_id));
    }
    return (!empty($query))?true:false;

  }
  function set_participant_state($crmParticipantId,$adminId)
  {
      
      $tbl_1         = TBL_PREFIX . 'crm_participant_stage';
      $tbl_2         = 'crm_participant';
      $tbl_3         = 'participant';
      $tbl_4         = 'crm_participant_docs';
      $tbl_5         = 'crm_participant_docs';
      $tbl_6         = 'participant_care_not_tobook';
      $tbl_7         = 'participant_roster';
      $tbl_8         = 'participant_roster_data';
      $tbl_9         = 'shift_participant';
      $tbl_10        = 'shift';
      $tbl_11        = 'participant_email';
      $tbl_12        = 'participant_address';
      $tbl_13        = 'crm_participant_email';
      $tbl_14        = 'crm_participant_address';
      $tbl_15        = 'crm_participant_plan';
      $tbl_16        = 'participant_plan';
      $tbl_17        = 'participant_plan_breakdown';      
      $tbl_18        = 'crm_participant_plan_breakdown';
      $tbl_19        = 'participant_phone';
      $tbl_20        = 'crm_participant_phone';

      $crm_participant_plan        = 'crm_participant_plan';
      $participant_plan        = 'participant_plan';
      $participant_line_items='participant_line_items';
      $crm_participant_line_items='crm_participant_line_items';

      $select_column = array(
          $tbl_1 . ".stage_id",
          $tbl_1 . ".status"
      );
      $this->db->select($select_column);
      $this->db->from($tbl_1);
      $where = array(
          $tbl_1 . ".crm_participant_id" => $crmParticipantId,
          $tbl_1 . ".stage_id" => '11'
      );

      $this->db->where($where);
      $this->db->order_by($tbl_1 . '.id', 'DESC');
      $this->db->limit(1);
      $response = $this->db->get()->row_array();
      $data     = array();
      if (!empty($response)) {
                  $update_userName=array();
                  if ($response['status'] == 1 && $response['stage_id'] == '11') {

                          $update = array(
                              "booking_status" => 3
                          );
                          $result = $this->basic_model->update_records($tbl_2, $update, $where = array(
                              'id' => $crmParticipantId
                          ));
                          $columns = array(TBL_PREFIX .$tbl_20 . ".*",TBL_PREFIX .$tbl_2 . ".*",TBL_PREFIX .$tbl_13 . ".email",TBL_PREFIX .$tbl_13 . ".primary_email",TBL_PREFIX .$tbl_14 . ".street",TBL_PREFIX .$tbl_14 . ".city", TBL_PREFIX .$tbl_14 . ".postal",TBL_PREFIX .$tbl_14 . ".state",TBL_PREFIX .$tbl_14 . ".lat", TBL_PREFIX .$tbl_14. ".long", TBL_PREFIX .$tbl_14 . ".primary_address");
                          $this->db->select($columns);
                          $this->db->from(TBL_PREFIX . $tbl_2);
                          $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id', 'left');
                          $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');
                          $this->db->join('tbl_crm_participant_phone', 'tbl_crm_participant_phone.crm_participant_id = tbl_crm_participant.id', 'left');
                          $where = array(
                              TBL_PREFIX . $tbl_2 . ".id" => $crmParticipantId
                          );
                          $this->db->where($where);
                          $objparticipant                        = $this->db->get()->row_array();
                          $objparticipant                        = (object) $objparticipant;
                          $arr_participant                       = array();
                          $arr_participant['username']           = $objparticipant->username;
                          $arr_participant['firstname']          = $objparticipant->firstname;
                          $arr_participant['middlename']         = $objparticipant->middlename;
                          $arr_participant['lastname']           = $objparticipant->lastname;
                          $arr_participant['gender']             = $objparticipant->gender;
                          $arr_participant['dob']                = $objparticipant->dob;
                          $arr_participant['ndis_num']           = $objparticipant->ndis_num;
                          $arr_participant['medicare_num']       = $objparticipant->medicare_num;
                          $arr_participant['referral']           = $objparticipant->referral;
                          $arr_participant['preferredname']      = $objparticipant->preferredname;
                          $arr_participant['relation']           = $objparticipant->referral_relation;
                          $arr_participant['referral_firstname'] = $objparticipant->referral_firstname;
                          $arr_participant['referral_lastname']  = $objparticipant->referral_lastname;
                          $arr_participant['referral_email']     = $objparticipant->referral_email;
                          $arr_participant['referral_phone']     = $objparticipant->referral_phone;
                          $arr_participant['living_situation']   = $objparticipant->living_situation;
                          $arr_participant['aboriginal_tsi']     = $objparticipant->aboriginal_tsi;
                          $arr_participant['oc_departments']     = 0; //$objparticipant->oc_departments;
                          $arr_participant['created']            = date("Y-m-d H:i:s");
                          $arr_participant['status']             = '1';
                          $arr_participant['archive']            = 0;
                          $arr_participant['houseId']      = $objparticipant->houseId;
                          
                          // $this->db->select('participantId');
                          // $this->db->from('tbl_participant_email');
                          // $swhere = array(
                          //      'tbl_participant_email' . ".email" => $objparticipant->email
                          // );
                          // $this->db->where($swhere);
                          // $p_id                        = $this->db->get()->row_array();
                          $check_participant_email =    $this->basic_model->get_row('participant_email',array('participantId'),array('email'=>$objparticipant->email));
                          if(!empty($check_participant_email)){
                            $participant_id = $check_participant_email->participantId;
                            $update_query = $this->basic_model->update_records( $tbl_3,$arr_participant,array('id'=>$participant_id));
                          }
                          else{
                            $insert_query                          = $this->db->insert(TBL_PREFIX . $tbl_3, $arr_participant);
                            $participant_id                        = $this->db->insert_id();

                          }
                          $check_participant_phone =    $this->basic_model->get_record_where($tbl_19,'*',array('phone'=>$objparticipant->phone));
                        //  echo $this->db->last_query();
                        //   var_dump($check_participant_phone);exit;
                          //if(empty($check_participant_phone)){
                            $arr_phone=array(
                              'participantId'=>$participant_id,
                              'phone'=>$objparticipant->phone,
                              'primary_phone'=>1
                            );
                            $insert_query                          = $this->db->insert(TBL_PREFIX . $tbl_19, $arr_phone);
                            // $participant_id                        = $this->db->insert_id();
                         // }
                          //crm_participant_plan
                          $this->db->select('*');
                          $this->db->from(TBL_PREFIX . $crm_participant_plan);
                          $this->db->where('crm_participant_id',$crmParticipantId);
                          $this->db->order_by('id','desc');
                          $plan_result=$this->db->get()->row_array();
                          $plan_result=(object)$plan_result;
                          
                          $this->db->select('*');
                          $this->db->from(TBL_PREFIX . $crm_participant_plan);                          
                          $this->db->where('crm_participant_id',$crmParticipantId);
                          $this->db->where('status',1);
                          $this->db->where('end_date <','CURRENT_DATE');
                          $plan_result1=$this->db->get()->row_array();

                          if(isset($plan_result->id) && !$plan_result1)
                          {
                            $crm_plan_id=$plan_result->id;

                            $arr_plan                       = array();
                            $arr_plan['participantId']      =  $participant_id; //$plan_result->crm_participant_id;
                            $arr_plan['plan_name']          =  '';//$plan_result->plan_name;
                            $arr_plan['plan_type']          =  ''; // $plan_result->plan_type;
                            $arr_plan['plan_id']            =  ''; // $plan_result->id;
                            $arr_plan['start_date']         = $plan_result->start_date;
                            $arr_plan['end_date']           = $plan_result->end_date;
                            $arr_plan['total_funding']      = $plan_result->total_funding;
                            $arr_plan['fund_used']          = $plan_result->fund_used;
                            $arr_plan['remaing_fund']       = $plan_result->remaing_fund;
							$arr_plan['funding_type']       = 1;
							
                            // participant_plan
                            $insert_query  = $this->db->insert(TBL_PREFIX . $participant_plan, $arr_plan);
                            $participant_plan_id  = $this->db->insert_id();

                            $update_query = $this->basic_model->update_records($tbl_15,array('status'=>0),array('crm_participant_id'=>$plan_result->crm_participant_id,'end_date <'=>'CURRENT_DATE'));
                            $update_query = $this->basic_model->update_records($tbl_15,array('status'=>1),array('id'=>$plan_result->id));

                            
                           $this->db->select('*');
                           //crm_participant_plan_breakdown
                           $this->db->from(TBL_PREFIX . $crm_participant_line_items);
                          // $this->db->where('crm_participant_id',$crmParticipantId);
                           $this->db->where('crm_plan_id',$crm_plan_id);
                           $plan_breakdown=$this->db->get()->result_array();
                           $plan_breakdown=(object)$plan_breakdown;
                           
                           $arr_plan_breakdown=array();
                           if($plan_breakdown)
                           {
                             foreach($plan_breakdown as $breakdown)
                             {
                                $arr_plan_breakdown[]=array(                            
                                'plan_id' => $participant_plan_id, 
                                'line_item_id' => $breakdown['line_item_id'],
                                'amount'   => $breakdown['amount'],
                                'created'  => DATE_TIME,
                                'updated'  => DATE_TIME,
                                'archive'  => $breakdown['archive']);
                             }
                             // participant_plan_breakdown
                             //$this->db->insert_batch(TBL_PREFIX . $tbl_17, $arr_plan_breakdown);
                             $this->db->insert_batch(TBL_PREFIX.$participant_line_items, $arr_plan_breakdown);
                             
                           }
                          }

                          if ($participant_id > 0) {
                            $arr_email = array(); $arr_address = array();
                            $arr_email['participantId'] = $participant_id;
                            $arr_email['email']        = $objparticipant->email;
                            $arr_email['primary_email'] = 1;
                            if(!empty($check_participant_email)){
                              $this->basic_model->update_records( $tbl_11,$arr_email,array('participantId'=>$participant_id));

                            }
                            else{
                             $insert_query                          = $this->db->insert(TBL_PREFIX . $tbl_11, $arr_email);
                            }
                            $this->db->select('participantId');
                            $this->db->from(TBL_PREFIX . $tbl_12);
                            $swhere = array(
                              TBL_PREFIX . $tbl_12 . ".participantId" => $participant_id
                            );
                            $this->db->where($swhere);
                            $add_id = $this->db->get()->row_array();
                            $arr_address = array();
                            $arr_address['participantId'] = $participant_id;
                            $arr_address['street']        = $objparticipant->street;
                            $arr_address['city']        = $objparticipant->city;
                            $arr_address['postal']        = $objparticipant->postal;
                            $arr_address['state']        = $objparticipant->state;
                            $arr_address['lat']        = $objparticipant->lat;
                            $arr_address['long']        = $objparticipant->long;
                            $arr_address['primary_address'] = 1;

                            if(!empty($add_id))
                            {
                              $this->basic_model->update_records($tbl_12,$arr_address,array('participantId'=>$participant_id));

                            }
                            else{
                              $insert_query  = $this->db->insert(TBL_PREFIX . $tbl_12, $arr_address);
                            }

                            
                            // Insert/Update mobility
                            $this->db->select('tbl_crm_participant_disability.*');
                            $this->db->from('tbl_crm_participant_disability');
                            $swhere = array("tbl_crm_participant_disability.crm_participant_id" => $crmParticipantId);
                            $this->db->where($swhere);
                            $records_disability = $this->db->get()->row_array();

                          //  pr($records_disability);

                            $this->db->select('tbl_crm_participant_ability.*');
                            $this->db->from('tbl_crm_participant_ability');
                            $swhere = array("tbl_crm_participant_ability.crm_participant_id" => $crmParticipantId);
                            $this->db->where($swhere);
                            $records = $this->db->get()->row_array();
                            
                            // remove mobility                                                       
                            $this->db->where('participantId', $participant_id);
                            $this->db->where_in('type',['mobality','assistance']);
                            $this->db->delete('tbl_participant_assistance');                            
                            if(!empty($records)){
                              // if mobility then save key as assistance
                              if(!empty($records['require_mobility'])){
                                  $mobility=explode(",",$records['require_mobility']);
                                  $arr_mobility_breakdown=array();
                                  foreach($mobility as $mobilitys){
                                      $arr_mobility_breakdown[]=array('participantId' =>$participant_id, 'assistanceId'   => $mobilitys,'type'=>'mobality');
                                  }
                                  $this->db->insert_batch('tbl_participant_assistance', $arr_mobility_breakdown);                                
                              }

                              // if mobility then save key as assistance
                              
                              if(!empty($records['require_assistance'])){
                                $assistance=explode(",",$records['require_assistance']);
                                $arr_assistance_breakdown=array();
                                foreach($assistance as $assistances){
                                    $arr_assistance_breakdown[]=array('participantId' =>$participant_id, 'assistanceId'   => $assistances,'type'=>'assistance');
                                }
                                $this->db->insert_batch('tbl_participant_assistance', $arr_assistance_breakdown);                                
                              }

                              


                              $careReq=array();
                              // tbl_participant_care_requirement
                              if(!empty($records_disability)){                                
                                $careReq['diagnosis_primary']=$records_disability['primary_fomal_diagnosis_desc'];
                                $careReq['diagnosis_secondary']=$records_disability['secondary_fomal_diagnosis_desc'];
                                //$care_req['']=records_disability['legal_issues'];
                                //$care_req['']=records_disability['other_relevant_information'];
                                //$care_req['']=records_disability['linked_fms_case_id'];
                                //$care_req['']=records_disability['docs'];                                  
                                
                              }

                              $careReq['participantId']=$participant_id;
                             // $careReq['diagnosis_primary']='';
                             // $careReq['diagnosis_secondary']='';
                             $languages_spoken=1;
                            if(!empty($records['languages_spoken'])){
                              $searchForValue = ',';
                              if( strpos($records['languages_spoken'], $searchForValue) !== false ) {
                                  list($first) = explode(',', $records['languages_spoken']);
                                  $languages_spoken=$first;
                              }else{
                                $languages_spoken=$records['languages_spoken'];
                              }
                            }

                              $careReq['participant_care']='';
                              $careReq['cognition']= $records['cognitive_level'];
                              $careReq['communication']= $records['communication'];
                              $careReq['english']='';                           
                              $careReq['preferred_language']= $languages_spoken; //$records['languages_spoken'];
                              $careReq['preferred_language_other']='';
                              $careReq['linguistic_interpreter']= $records['language_interpreter'];
                              $careReq['hearing_interpreter']= $records['hearing_interpreter'];
                              $careReq['require_assistance_other']= $records['require_assistance_other'];
                              $careReq['support_require_other']='';
                             

                              $this->db->select('tbl_participant_care_requirement.participantId');
                              $this->db->from('tbl_participant_care_requirement');
                              $swhere = array("tbl_participant_care_requirement.participantId" => $participant_id);
                              $this->db->where($swhere);
                              $records = $this->db->get()->row_array();
                              if(!empty($records)){
                                $this->db->where('participantId',$participant_id);
                                $insert_query  = $this->db->update('tbl_participant_care_requirement', $careReq);
                              }else{
                                $insert_query  = $this->db->insert('tbl_participant_care_requirement', $careReq);                              
                              }

                            }
                            
                            // end code
                            $carenote = array();
                            $carenote['participantId'] = $participant_id;
                            $carenote['gender'] = $objparticipant->gender;
                            $carenote['ethnicity'] = '';
                            $carenote['religious'] = '';
                            $insert_query  = $this->db->insert(TBL_PREFIX . $tbl_6, $carenote);
                            $updatestagedocs = array(
                                "archive" => 1
                            );
                            $archivestagedocs = $this->basic_model->update_records($tbl_4, $updatestagedocs, $where = array(
                                'id' => $crmParticipantId
                            ));
                            $updatedocs = array(
                                "archive" => 1
                            );
                            $archivedocs = $this->basic_model->update_records($tbl_5, $updatedocs, $where = array(
                                'id' => $crmParticipantId
                            ));
                            $crm_roster = $this->basic_model->get_row('crm_participant_roster',array('*'),array('participantId'=>$crmParticipantId));
                            $roster['participantId'] =  $participant_id;
                            $roster['title'] =  $crm_roster->title;
                            $roster['is_default'] =  $crm_roster->is_default;
                            $roster['start_date'] =  $crm_roster->start_date;
                            $roster['end_date'] =  $crm_roster->end_date;
                            $roster['shift_round'] =  $crm_roster->shift_round;
                            $roster['status'] =  $crm_roster->status;
                            $check_roster = $this->basic_model->get_row($tbl_7,array('*'),array('participantId'=>$participant_id));
                            if(!empty($check_roster)){
                              $roster_id = $check_roster->id;
                              $this->basic_model->update_records($tbl_7,$roster,array('id'=>$roster_id));
                            }else{
                              $roster_id  = $this->basic_model->insert_records( $tbl_7, $roster);
                            }
                            $crm_roster_data =    $this->basic_model->get_record_where('crm_participant_roster_data',array('*'),array('rosterId'=>$crm_roster->id));
                            $crm_shift_data =    $this->basic_model->get_record_where('crm_participant_shifts',array('*'),array('rosterId'=>$crm_roster->id));
                            $shift_data = array();
                            $roster_data = array();
                            foreach($crm_roster_data as $roster){
                              $roster_data['week_day'] = $roster->week_day;
                              $roster_data['start_time'] = $roster->start_time;
                              $roster_data['end_time'] = $roster->end_time;
                              $roster_data['week_number'] = $roster->week_number;
                              $roster_data['rosterId'] = $roster_id;
                              $this->db->insert(TBL_PREFIX . $tbl_8, $roster_data);
                            }
                            foreach($crm_shift_data as $shift){
                              $shift_data['status'] = $shift->status;
                              $shift_data['shift_date'] = $shift->shift_date;
                              $shift_data['start_time'] = $shift->start_time;
                              $shift_data['end_time'] = $shift->end_time;
                              $shift_data['booked_by'] = 2;
                              $this->db->insert(TBL_PREFIX . $tbl_10, $shift_data);
                              $shift_id=$this->db->insert_id();
                              
                              if($shift_id>0){
                              
                                  $shift_participant_data['status'] = $shift->status;
                                  $shift_participant_data['participantId'] = $participant_id;
                                  $shift_participant_data['shiftId'] =  $shift_id;
                                  $shift_participant_data['created'] = date("Y-m-d H:i:s");
                                  $this->db->insert(TBL_PREFIX . $tbl_9, $shift_participant_data);

                                  $arr_shift_location = array();
                                  $arr_shift_location['shiftId'] = $shift_id;
                                  $arr_shift_location['address']        = $objparticipant->street;
                                  $arr_shift_location['suburb']        = $objparticipant->city;
                                  $arr_shift_location['postal']        = $objparticipant->postal;
                                  $arr_shift_location['state']        = $objparticipant->state;
                                  $arr_shift_location['lat']        = $objparticipant->lat;
                                  $arr_shift_location['long']        = $objparticipant->long;
                                  $this->db->insert('tbl_shift_location', $arr_shift_location);
                                  
                              }
                            }
                              
                            // Here to mail password and user name
                            require_once APPPATH . 'Classes/crm/CrmParticipant.php';
                              $obParticipant = new CrmParticipantClass\CrmParticipant();
                              $generatePass= $obParticipant->genratePassword();
                              $obParticipant->encryptPassword();
                              $encryptedPass=$obParticipant->getPassword();
                              // update particiapant name
                              $update_participant_info['username']=$participant_id.$objparticipant->firstname;   
                              $update_participant_info['password']=$encryptedPass;                          
                              
                              $obParticipant->setFirstname($objparticipant->firstname);
                              $obParticipant->setLastname($objparticipant->lastname);
                              $obParticipant->setUserName($update_participant_info['username']);
                              $obParticipant->setPassword($generatePass);
                              $sendmail[0]['email']= $objparticipant->email;
                              $obParticipant->setParticipantEmail($sendmail);
                              $this->basic_model->update_records($tbl_3,$update_participant_info,array('id'=>$participant_id));
                              $obParticipant->WelcomeMailParticipant(); 
                                                                                       
                              $data = true;

                          } else {
                              $data = false;
                          }

                  } else {
                      $data = false;
                  }


          return $data;
      }
  }
    function get_ndis_services_categories(){

        $select_column = array('name as label', 'id as value');
        $where = array('archive' => 0);
        $this->db->select($select_column);
        $this->db->from(TBL_PREFIX.'finance_support_category');
        $this->db->where($where);     
        $this->db->order_by('order','asc');
        $query = $this->db->get();        
        return $query->result_array();

   
    /*$tbl = TBL_PREFIX .'ndis_support';
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->group_by('support_category_number');
    $result = $this->db->get()->result();
    $service_categories = array();
    if (!empty($result)) {
        foreach ($result as $val) {
            $service_categories[] = array('label' => $val->support_category_number."-".$val->support_category_name, 'value' => $val->support_category_number);

        }
    } */
    
   }
   function get_ndis_service_by_id($cat_id,$support_item_number){
      
        $tbl_fincance_line=TBL_PREFIX.'finance_line_item';
        $tbl_fincance_support=TBL_PREFIX.'finance_support_category';
        /*
        $this->db->select($tbl_fincance_support.'.name as support_category_name');
        $this->db->from($tbl_fincance_support);
        
        $this->db->limit(1);
        $sub_query = $this->db->get_compiled_select();
        */
        //$this->db->flush_cache();
        $select_column = array($tbl_fincance_line.'.id',$tbl_fincance_support.'.name as support_category_name' , $tbl_fincance_line.'.id as support_id', $tbl_fincance_line.'.line_item_name as amount', 
        $tbl_fincance_line.'.support_category as support_category_number', $tbl_fincance_line.'.line_item_number as support_item_number',$tbl_fincance_line.'.line_item_name as support_item_name');
        
        $where = array('support_category' => $cat_id);
        //$this->db->select('('.$sub_query.') as support_category_name', false);
        $this->db->select($select_column);
        $this->db->from($tbl_fincance_line);
        $this->db->join($tbl_fincance_support, $tbl_fincance_support.'.id = '.$tbl_fincance_line.'.support_category', 'left');
        $this->db->where($where);     

        //$this->db->where("support_category IN ($sub_query)");
     //   $this->db->order_by('order','asc');
        
        $query = $this->db->get();        
        //return 
        //last_query();
        $new_array['ndis_service']=$query->result_array();
        return $new_array;
        //[] =array('support_id'=>$val->support_id,'amount'=>$amount,'support_category_number'=>$val->support_category_number,
         //   'support_category_name'=>$val->support_category_name,'support_item_number'=>$val->support_item_number,'support_item_name'=>$val->support_item_name);

   /* //$data=  json_decode($reqData);
    $tbl = TBL_PREFIX .'ndis_support';
    $this->db->select('*');
    $this->db->from($tbl);
    if(isset($data->support_item_number)){
      $this->db->where('support_item_number',$data->support_item_number);
    }
    if(isset($data->id)){
    $this->db->where('support_category_number',$cat_id);
    }
    $result = $this->db->get()->result();
    $new_array=array();
    if (!empty($result)) {
        foreach ($result as $val) {
          $amount=preg_split('//', $val->national_very_remote, -1, PREG_SPLIT_NO_EMPTY);
          unset($amount["0"]) ;
          $amount=implode(" ",$amount);
            $count=0;
            if($count==0)
            {
                $new_array['category'] = array('label' => $val->support_category_name, 'value' => $val->support_category_number);
                $count++;
            }
            $new_array['ndis_service'][] =array('support_id'=>$val->support_id,'amount'=>$amount,'support_category_number'=>$val->support_category_number,
            'support_category_name'=>$val->support_category_name,'support_item_number'=>$val->support_item_number,'support_item_name'=>$val->support_item_name);
        }
    }
    return $new_array; */
   }
   function saveNdisService($data){
    
      $crm_participant_id= $data->crmParticipantId;
      $total_ndis_service= $data->totalNdisAmount;
      $ndisPlanStartDate= date('Y-m-d', strtotime($data->ndisPlanStartDate));
      $ndisPlanEndDate= date('Y-m-d', strtotime($data->ndisPlanEndDate));	    
      $saveType= $data->saveType;
      $services = $data->ndisService;
      $strDate = strtotime(DATE_TIME);
      $datas = array();
          $select_column = array(
            'tbl_crm_participant.ndis_num', 'tbl_crm_participant.ndis_plan',
            'tbl_crm_participant.middlename','tbl_crm_participant.firstname', 'tbl_crm_participant.lastname ',
            'tbl_crm_participant_email.email'
        );
        $this->db->select($select_column);
        $this->db->from(TBL_PREFIX . 'crm_participant');
        $this->db->join('tbl_crm_participant_email', 'tbl_crm_participant_email.crm_participant_id = tbl_crm_participant.id AND tbl_crm_participant_email.primary_email = 1', 'left');
        $this->db->where('tbl_crm_participant.id',$crm_participant_id);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $datas['allocateFunDetails'] = $query->row_array();
        $filePath = FCPATH. 'uploads/crmparticipant/'.$crm_participant_id.'/'.'Stage3'.'/';
        $html = $this->load->view('crm/allocated_funds', $datas, true);
        $this->load->helper('pdf_helper');
        
        // created pdf file
        $filename='allocated_funds_'.$datas['allocateFunDetails']['ndis_num'].'_'.$strDate;
        $convertedpdf = pdf_create($filePath, $html,$filename);
        
        // insert into stage_doc
         
        $stage_doc=array();
        $stage_doc['crm_participant_id'] = $crm_participant_id;
        $stage_doc['stage_id'] = 9;
        $stage_doc['filename'] =  $filePath.$filename.'.pdf';
        $stage_doc['document_type'] = 2;
        if(isset($saveType) && $saveType=='Sent')
        {
          require_once APPPATH . 'modules/crm/controllers/CrmDocusign.php';
          $doc = new CrmDocusign();
          $name=$datas['allocateFunDetails']['firstname'].''.$datas['allocateFunDetails']['middlename'].''.$datas['allocateFunDetails']['lastname'];
          // $recipient_email="developer@yourdevelopmentteam.com.au";
          $recipient_email=$datas['allocateFunDetails']['email'];
          $document_name=$filename.'.pdf';
          $email_subject='Funding Consent NDIS DOCUMENT';
          $envelopeId = $doc->CreateEnvelope($stage_doc['filename'],$recipient_email,$name,$document_name,$email_subject);
          $stage_doc['envelope_id'] = $envelopeId;
        }
        $stage_doc['filename']=$filename.'.pdf';
        
        $this->db->insert(TBL_PREFIX . 'crm_participant_docs', $stage_doc);
        // insert into tbl_crm_participant_plan
        $plan_details=array();
        $plan_details['crm_participant_id'] = $crm_participant_id;
        //$plan_details['plan_type'] = $datas['allocateFunDetails']['ndis_plan'];
        //$plan_details['plan_name'] = $datas['allocateFunDetails']['ndis_plan'].'_'.$datas['allocateFunDetails']['firstname'].$datas['allocateFunDetails']['lastname'].'_'.date('Y').'_'.$datas['allocateFunDetails']['ndis_num'];
        $plan_details['start_date'] =$ndisPlanStartDate;
        $plan_details['end_date'] =  $ndisPlanEndDate;
        $plan_details['total_funding'] = $total_ndis_service;
        $insert_query= $this->db->insert(TBL_PREFIX .'crm_participant_plan', $plan_details);
        $plan_id = $this->db->insert_id();

     // instert into tbl_crm_participant_plan_breakdown
      $ndis_services = array();
      foreach($services as $key => $service){
        $ndis_services[] = array(          
          'amount' =>$service->amount,
          'line_item_id' =>$service->support_id,
          'crm_plan_id' =>$plan_id,
          'archive' =>0,
          'updated'=>DATE_TIME,
          'created' =>DATE_TIME,
        );
      }
        // tbl_crm_participant_line_items
        $insert_query  = $this->db->insert_batch(TBL_PREFIX . 'crm_participant_line_items', $ndis_services);
        //$insert_query  = $this->db->insert_batch(TBL_PREFIX . 'crm_participant_plan_breakdown', $ndis_services);
        


       return $insert_query;


   }

  function modified_status($reqData,$adminId){
    $response = array();
    $tbl_1 = 'crm_participant';
    $tbl_2 = 'crm_participant_stage';
    $tbl_3 = 'tbl_crm_participant_stage_notes';
    $ndis_num =  $reqData->data->ndis_num;
    $notes = $reqData->data->notes;
    if(!empty($ndis_num)){
      $crm_participant_data =    $this->basic_model->get_row($tbl_1,array('tbl_'.$tbl_1.'.id','tbl_'.$tbl_1.'.firstname'),array('ndis_num'=>$ndis_num));
      if(!$crm_participant_data){
        return $response = array('status' => false, 'error'=>'NDIS number not found in crm module.');
      }
       $crm_participant_id = $crm_participant_data->id;
      $update = array(
          "modified_renewed_status" => 1
      );
      $result = $this->basic_model->update_records($tbl_1, $update, $where = array(
          'ndis_num' => $ndis_num
      ));
       $stage_where=array('crm_participant_id'=>$crm_participant_id,'stage_id'=>8 );
      // $stage_where=array('crm_participant_id'=>$crm_participant_id,'stage_id'=>4);
       $key = $this->assign_participant();
       $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>8,'intake_type'=>5,'assigned_to'=>$key),array('id'=>$crm_participant_id));
       $this->bulk_update_status($crm_participant_id,7);
       $this->basic_model->update_records('crm_participant_stage',array('status'=>3),array('crm_participant_id'=>$crm_participant_id,'stage_id'=>8));
       $res= $this->basic_model->insert_records('crm_participant_schedule_task',array('crm_participant_id'=>$crm_participant_id,'task_name'=>"Modify the contract",'priority'=>'3','due_date'=>date("Y-m-d H:i:s"),'assign_to'=>$key));
       $res= $this->basic_model->insert_records('crm_participant_stage_notes',array('crm_participant_id'=>$crm_participant_id,'notes'=>$notes,'status'=>1,'stage_id'=>8));

       $this->loges->setUserId($adminId);
       $this->loges->setTitle('"Modified the contract"');
       $this->loges->createLog();
         $response = array('status' => true, 'id'=>$crm_participant_id);
         return $response;
    }else{
      return $response = array('status' => false, 'error'=>'NDIS number not available for this participant.');
    }
  }
  function assign_participant(){
    $tbl_1 = TBL_PREFIX . 'member';
    $tbl_2 = TBL_PREFIX . 'crm_participant';
    $tbl_3 = TBL_PREFIX . 'crm_staff';
    $dt_query = $this->db->select(array($tbl_1.'.id as ocs_id',$tbl_1.'.status',"CONCAT(tbl_member.firstname,' ',tbl_member.lastname) AS name","count(tbl_member.id) as count"));

    $this->db->from($tbl_3);
    $this->db->join('tbl_member', 'tbl_crm_staff.admin_id = tbl_member.id', 'left');
    $this->db->join('tbl_crm_participant', 'tbl_crm_participant.assigned_to = tbl_member.id', 'left');
    $this->db->where($tbl_1.'.archive=',"0");
    $this->db->where($tbl_3.'.status=',"1");
    $this->db->group_by($tbl_1.'.id');
    $data = $this->db->get();
    //echo   $this->CI->db->last_query();

    $assigned_counts= array();
    foreach($data->result_array() as $value){
      $assigned_counts[$value['ocs_id']] =  $value['count'];

    }
    $min             = min($assigned_counts);
    $key             = array_search($min, $assigned_counts);
    return $key;
  }

  function renewed_status($reqData,$adminId){
    $response = array();
    $tbl_1 = 'crm_participant';
    $tbl_2 = 'crm_participant_stage';
    $tbl_3 = 'tbl_crm_participant_stage_notes';
    $ndis_num =  $reqData->data->ndis_num;

    if(!empty($ndis_num)){
      $crm_participant_data =    $this->basic_model->get_row($tbl_1,array('tbl_'.$tbl_1.'.id','tbl_'.$tbl_1.'.assigned_to','tbl_'.$tbl_1.'.firstname'),array('ndis_num'=>$ndis_num));
      if(!$crm_participant_data){
        return $response = array('status' => false, 'error'=>'NDIS number not found in crm module.');
      } 
      $crm_participant_id = $crm_participant_data->id;
      $update = array(
          "modified_renewed_status" => 2
      );
      $result = $this->basic_model->update_records($tbl_1, $update, $where = array(
          'ndis_num' => $ndis_num
      ));
       $stage_where=array('crm_participant_id'=>$crm_participant_id,'stage_id'=>8 );
      // $stage_where=array('crm_participant_id'=>$crm_participant_id,'stage_id'=>4);
       $this->basic_model->update_records('crm_participant',array('booking_status'=>4,'stage_status'=>8,'intake_type'=>3),array('id'=>$crm_participant_id));
       $this->bulk_update_status($crm_participant_id,7);
       $this->basic_model->update_records('crm_participant_stage',array('status'=>3),array('crm_participant_id'=>$crm_participant_id,'stage_id'=>8));
       $res= $this->basic_model->insert_records('crm_participant_schedule_task',array('crm_participant_id'=>$crm_participant_id,'task_name'=>"Renew the contract",'priority'=>'3','due_date'=>date("Y-m-d H:i:s"),'assign_to'=>$crm_participant_data->assigned_to));

       // require_once APPPATH . 'Classes/crm/CrmTask.php';
       // $objtask = new TaskClass\Task();
       //   $objtask->setParticipant($crm_participant_id);
       //   $objtask->setTaskName("Renew the contract");
       //   $objtask->setPriority("3");
       //   $objtask->setDuedate(date("Y-m-d H:i:s"));
       //   $objtask->setTasknote("Renew the contract");
       //   $objtask->setMember($crm_participant_data->assigned_to);
       //
       //       $task_id = $objtask->AddTask();
             $this->loges->setUserId($adminId);
             $this->loges->setTitle('"Renew the contract"');
             $this->loges->createLog();
            // echo $this->db->last_query();



         $response = array('status' => true, 'id'=>$crm_participant_id);
         return $response;
    } else{
      return $response = array('status' => false, 'error'=>'NDIS number not available for this participant.');
    }
  }
    function plan_list($reqData)
  {
    $this->db->select('*');
    $this->db->from(TBL_PREFIX . 'crm_participant_plan');
    $this->db->where('crm_participant_id',$reqData->participant_id);
    if(isset($reqData->status) && ($reqData->status !=''))
    {
      $this->db->where('status',$reqData->status);
    }
    $this->db->order_by('id','desc');
    $result=$this->db->get()->result();
    return $result;
  }

}
