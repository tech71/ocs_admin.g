<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmTask_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function task_list($reqData, $loginId) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        $status_con = array(1);

        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($loginId);
        $role_array = array();

        foreach ($role as $value) {
            array_push($role_array, $value->permission);
        }

        $src_columns = array(
            '(select concat(firstname," ",lastname) from tbl_member where id=tbl_crm_participant.assigned_to)',
            'tbl_crm_participant.id',
            'tbl_crm_participant.ndis_num',
            'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName',
            'tbl_crm_participant.status',
            'tbl_crm_participant_schedule_task.task_name',
            'CASE
             WHEN tbl_crm_participant_schedule_task.task_status = 1 THEN "Completed"
             WHEN tbl_crm_participant_schedule_task.task_status = 0 THEN "Assigned"
             END as task_status',
            'date_format(tbl_crm_participant_schedule_task.due_date, "%d-%m-%Y")',
            'tbl_crm_participant_schedule_task.id'
        );
        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;

                if ($orderBy == 'name') {
                    $orderBy = 'tbl_crm_participant.firstname';
                }

                if ($orderBy == 'taskname') {
                    $orderBy = 'tbl_crm_participant_schedule_task.task_name';
                }

                if ($orderBy == 'duedate') {
                    $orderBy = 'tbl_crm_participant_schedule_task.due_date';
                }

                if ($orderBy == 'assigned_person') {
                    $orderBy = 'tbl_crm_participant.assigned_to';
                }

                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'tbl_crm_participant.id';
            $direction = 'DESC';
        }
        $where = '';
        $where = "tbl_crm_participant.archive = 0";
        $sWhere = $where;
        if (!empty($filter)) {

            if (!empty($filter->search)) {
                $this->db->group_start();
                for ($i = 0; $i < count($src_columns); $i++) {
                    $column_search = $src_columns[$i];
                    if (strstr($column_search, "as") !== false) {
                        $serch_column = explode(" as ", $column_search);
                        if ($serch_column[0] != 'null')
                            $this->db->or_like($serch_column[0], $filter->search);
                    } else if ($column_search != 'null') {
                        $this->db->or_like($column_search, $filter->search);
                    }
                }
                $this->db->group_end();
            }
            if (!empty($filter->filterVal)) {
                $this->db->where('tbl_crm_participant_schedule_task.priority', $filter->filterVal);
            }
            if (!empty($filter->due_date)) {
                $this->db->where('tbl_crm_participant_schedule_task.due_date', $filter->due_date);
            }
            if (!empty($filter->task_status)) {
                $this->db->where('tbl_crm_participant_schedule_task.task_status', $filter->task_status);
            }
            if (!empty($filter->staffId)) {
                $this->db->where('tbl_crm_participant_schedule_task.assign_to', $filter->staffId);
            }
        }

        $select_column = array(
            'tbl_crm_participant.id', 'tbl_crm_participant.ndis_num', 'concat(tbl_crm_participant.firstname," ",tbl_crm_participant.lastname) as FullName', '(tbl_crm_participant.assigned_to) as assigned_staff_id',
            'tbl_crm_participant.status', 'concat(tbl_crm_participant.referral_firstname," ",tbl_crm_participant.referral_lastname) as ref_fullName',
            '(select concat(firstname," ",lastname) from tbl_member where id=tbl_crm_participant.assigned_to) as assigned_to', 'tbl_crm_participant.gender', 'tbl_crm_participant_schedule_task.id as task_id', 'tbl_crm_participant_schedule_task.*',
            'tbl_crm_participant.booking_date', 'tbl_crm_participant.booking_status', 'tbl_crm_participant.action_status', 'tbl_member.firstname as f', 'tbl_member.lastname as l', 'tbl_crm_stage.name as stage_name', '');

        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from(TBL_PREFIX . 'crm_participant_schedule_task');
        $this->db->join('tbl_crm_participant', 'tbl_crm_participant_schedule_task.crm_participant_id = tbl_crm_participant.id', 'left');
        $this->db->join('tbl_member', 'tbl_member.id = tbl_crm_participant.assigned_to', 'left');
        $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'left');
        $this->db->where_in('tbl_crm_participant.status', $status_con);
        $this->db->where('tbl_crm_participant_schedule_task.parent_id', '0');
        $this->db->where('tbl_crm_participant_schedule_task.archive', '0');
        $this->db->where('tbl_crm_participant_schedule_task.priority!=0');
        //$this->db->where('tbl_crm_participant_schedule_task.task_status', '0');

        if (in_array('access_crm', $role_array) && !in_array('access_crm_admin', $role_array)) {
            $this->db->where_in('tbl_crm_participant_schedule_task.assign_to', $loginId);
        }

        $this->db->order_by($orderBy, $direction);
        $this->db->group_by('tbl_crm_participant_schedule_task.id');
        $this->db->limit($limit, ($page * $limit));

        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        #last_query();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;

        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();
        if (!empty($query->result())) {
            $row = array();
            $task = array();
            foreach ($query->result() as $val) {

                $row['id'] = $val->id;
                $row['ndis'] = $val->ndis_num;
                $row['assigned_staff_id'] = $val->assigned_staff_id;
                $row['participant_name'] = $val->FullName;
                $row['gender'] = isset($val->gender) && $val->gender == 1 ? 'Male' : 'Female';
                $row['taskname'] = $val->task_name;
                $row['stage_name'] = $val->stage_name;
                $row['assigned'] = $val->assigned_to;
                $row['duedate'] = date('d-m-Y', strtotime($val->due_date));
                $row['updated_at'] = date('d-m-Y', strtotime($val->updated_at));
                $row['priority'] = $val->priority;
                $row['notes'] = $val->note;
                $row['task_id'] = $val->task_id;
                $row['crm_participant_id'] = $val->crm_participant_id;
                $row['task_status'] = isset($val->task_status) && $val->task_status == 1 ? 'Completed' : 'Assigned';
                $row['date'] = $val->booking_date;
                $row['assigned_to'] = $val->assigned_to;
                $row['priority'] = $val->priority;
                $row['assigned_person'] = $val->f . ' ' . $val->l;
                $row['action'] = ''; //isset($val->action_status) && $val->action_status == 1 ? 'Phone Screening' : 'Call';
                $row['relevant_task_note'] = $val->relevant_task_note;

                $row['taskLists'] = array('id' => $val->id, 'taskname' => $val->task_name, 'assigned' => $val->assigned_to, 'duedate' => $val->due_date, 'notes' => $val->note, 'relevant_task_note' => $val->relevant_task_note, 'priority' => $val->priority, 'updated_at' => $val->updated_at);


                $dataResult[] = $row;
            }
        }
        $return = array(
            'count' => $dt_filtered_total,
            'data' => $dataResult
        );

        return $return;
    }

    public function task_priority_list() {
        $this->db->select('*');
        $this->db->from(TBL_PREFIX . 'crm_task_priority');
        $sWhere = array(TBL_PREFIX . 'crm_task_priority.status' => '1');
        $this->db->where($sWhere);
        $query = $this->db->get();
        $dataResult = array();
        if (!empty($query)) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['value'] = $val->id;
                $row['label'] = $val->name;
                // $row['code'] = $val->status;
                $dataResult[] = $row;
            }
            $data = array("status" => "true", "data" => $dataResult);
        } else {
            $data = array("status" => "false");
        }
        return $data;
    }

    public function create_task($objtask) {

        $arr_task = array();
        $arr_task['crm_participant_id'] = $objtask->getParticipant()->value;
        $arr_task['assign_to'] = $objtask->getParticipant()->assigned_to;
        $arr_task['priority'] = $objtask->getPriority();
        $arr_task['due_date'] = $objtask->getDuedate();
        $arr_task['task_name'] = $objtask->getTaskName();
        $arr_task['updated_at'] = date('Y-m-d h:i:s');
        $arr_task['relevant_task_note'] = $objtask->getTasknote();
        $insert_query = $this->db->insert(TBL_PREFIX . 'crm_participant_schedule_task', $arr_task);
        $task_id = $this->db->insert_id();
        return $task_id;
    }

    public function get_task_details($taskId) {
        if (!empty($taskId)) {
            $tbl = TBL_PREFIX . 'crm_participant_schedule_task';
            $Where = array($tbl . '.id' => $taskId);
            $this->db->select(array($tbl . '.id as task_id', $tbl . '.crm_participant_id', $tbl . '.task_name', $tbl . '.priority', $tbl . '.due_date', $tbl . '.relevant_task_note', $tbl . '.assign_to'));
            $this->db->from($tbl);
            $this->db->where($Where);
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        }
    }

    public function get_task_details_view_pop_up($taskId) {
        if (!empty($taskId)) {
           
            $Where = array('task.id' => $taskId);
            $this->db->select("(select name  from tbl_crm_stage where id = cp.stage_status) as stage_name");
            $this->db->select("concat_ws(' ',cp.firstname,cp.middlename,cp.lastname) as participant_name");
            $this->db->select("concat_ws(' ',m.firstname,m.middlename,m.lastname) as assigned_person");
            $this->db->select(array('task.id as task_id', 'task.crm_participant_id', 'task.task_name as taskname', 'task.priority', 'task.due_date as duedate', 'task.relevant_task_note', 'task.assign_to'));
            $this->db->from('tbl_crm_participant_schedule_task as task');
            $this->db->join('tbl_crm_participant as cp', 'cp.id = task.crm_participant_id');
            $this->db->join('tbl_member as m', 'm.id = task.assign_to');
            
             
             
            $this->db->where($Where);
            $query = $this->db->get();
            $result = $query->row_array();
            
            return $result;
        }
    }

    public function task_update($task_data) {

        $task_details = array(
            'crm_participant_id' => $task_data['crm_participant_id']['value'],
            'task_name' => $task_data['task_name'],
            'priority' => $task_data['priority'],
            'updated_at' => date('Y-m-d h:i:s'),
            'due_date' => date("Y-m-d", strtotime($task_data['due_date'])),
            'relevant_task_note' => $task_data['relevant_task_note'],
            'assign_to' => (isset($task_data['crm_participant_id']['assigned_to'])) ? $task_data['crm_participant_id']['assigned_to'] : $task_data['assigned_to']);
        $this->db->where($where = array('id' => $task_data['task_id']));
        $this->db->update(TBL_PREFIX . 'crm_participant_schedule_task', $task_details);
        return true;
    }

    public function get_participant_name($post_data, $loginId = false) {
        $search_data = $post_data->query;

        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($loginId);
        $role_array = array();
        foreach ($role as $value) {
            array_push($role_array, $value->permission);
        }
        $this->db->like('concat(firstname," ",lastname)', $search_data);
        $this->db->where('archive =', 0);
        $this->db->where('status=', 1);
        $this->db->where('booking_status !=', 3);
        $this->db->where('booking_status !=', 5);
        if (in_array('access_crm', $role_array) && !in_array('access_crm_admin', $role_array)) {
            $this->db->where_in('assigned_to', $loginId);
        }
        $this->db->select("CONCAT(firstname,' ',lastname) as participantName");
        $this->db->select('firstname, id,assigned_to');
        $query = $this->db->get(TBL_PREFIX . 'crm_participant');
        $query->result();
        $participant_rows = array();
        $stageName = '';
        $assign = "";
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $this->db->select("CONCAT(firstname,' ',lastname) as userName");
                $this->db->where('id =', $val->assigned_to);
                $query2 = $this->db->get(TBL_PREFIX . 'member')->row();
                if (!empty($query2))
                    $assign = $query2->userName;
                $this->db->select("tbl_crm_participant.stage_status,tbl_crm_stage.name as stageName");
                $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'inner');
                $this->db->where('tbl_crm_participant.id =' . $val->id);
                $query3 = $this->db->get(TBL_PREFIX . 'crm_participant')->row();
                if (!empty($query3)) {
                    $stageName = $query3->stageName;
                }
                $participant_rows[] = array('value' => $val->id, 'label' => $val->participantName, 'assigned_person' => $assign, 'assigned_to' => $val->assigned_to, 'stageName' => $stageName);
            }
        }
        return $participant_rows;
    }

    public function archive_task($task_id) {
        $where = array();
        $rows = array();
        if (!empty($task_id)) {
            // for ($i=0; $i < count($task_id->ids); $i++) {
            $data = array('updated_at' => date('Y-m-d h:i:s'), 'archive' => 1);
            $where = array('id' => $task_id);
            $rows = $this->basic_model->update_records('crm_participant_schedule_task', $data, $where);
            // }
            if (!empty($rows)) {
                return (array('status' => true, 'data' => $rows));
            } else {
                return (array('status' => false, 'error' => 'Sorry no data found'));
            }
        }
    }

    public function complete_task($task_id) {
        $where = array();
        $rows = array();
        if (!empty($task_id)) {

            $data = array('updated_at' => date('Y-m-d h:i:s'), 'task_status' => 1);
            $where = array('id' => $task_id);
            $rows = $this->basic_model->update_records('crm_participant_schedule_task', $data, $where);


            return (array('status' => true, 'data' => $rows));
        }
    }

}
