<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CrmSchedule_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function Check_UserName($objparticipant)
    {
        $username = $objparticipant->getUserName();
        $this->db->select(array(
            'count(*) as count'
        ));
        $this->db->from(TBL_PREFIX . 'participant');
        $this->db->where(array(
            'username' => $username
        ));
        //return $this->db->last_query();
        return $this->db->get()->row()->count;
    }



    public function schedules_calendar_data($reqData)
    {

        $loginId = $reqData->adminId;
        $default_date = $reqData->data->date;
        $month = date('m', strtotime($default_date));
        $year = date('Y', strtotime($default_date));

        require_once APPPATH . 'Classes/crm/CrmRole.php';
        $permission = new classRoles\Roles();
        $role = $permission->getRole($loginId);
        $role_array = array();
        foreach($role as $value){
          array_push($role_array,$value->permission);
        }

        $tbl = 'tbl_crm_participant_schedule_task';
        $this->db->select('"0" as id');
        $this->db->select('" " as title');
        $this->db->select('1 as allDay');
        $this->db->select('COUNT(IF(task_status = 1, 1, NULL)) as completedData');
        $this->db->select('COUNT(IF(task_status = 0, 1, NULL)) as dueData');
        $this->db->select('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date, "%Y-%m-%d") as start');
        $this->db->select('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date, "%Y-%m-%d") as end');

        $this->db->from($tbl);

        $this->db->where(array("MONTH(" . $tbl . ".due_date)" => $month, "YEAR(" . $tbl . ".due_date)" => $year,));
        $this->db->where('tbl_crm_participant_schedule_task.priority!=0');
        $this->db->where('tbl_crm_participant_schedule_task.archive!=1');
        if(in_array('access_crm',$role_array) && !in_array('access_crm_admin',$role_array) && !empty($loginId)){
            $this->db->where_in('tbl_crm_participant_schedule_task.assign_to', $loginId);
        }

        $this->db->group_by('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date,"%Y %m %d")');

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
           foreach ($result as $val) {
               if ($val->start >= date("Y-m-d") && $val->dueData>0) {
              // $val->completedData = ["status" => true, "count" => $val->completedData, "msg" => "Tasks Completed"];
               $val->dueData = ["status" => true, "count" => $val->dueData, "msg" => "Tasks Due"];
             } else if($val->completedData>0)  {
               $val->completedData = ["status" => true, "count" => $val->completedData, "msg" => "Tasks Completed"];
              // $val->dueData = ["status" => true, "count" => 0, "msg" => "Tasks Due"];
             }
           }
       }
       return $result;
    }

    public function schedules_calendar_data_by_staffid($reqData)
    {

        $default_date = $reqData->data->date;
        $month = date('m', strtotime($default_date));
        $year = date('Y', strtotime($default_date));

        $userId = $reqData->data->staffId;
        $tbl = 'tbl_crm_participant_schedule_task';
        $this->db->select('"0" as id,tbl_crm_participant_schedule_task.task_status');
        $this->db->select('" " as title');
        $this->db->select('1 as allDay');
        $this->db->select('COUNT(IF(priority = 1, 1, NULL)) as lowData');
        $this->db->select('COUNT(IF(priority = 2, 1, NULL)) as mediumData');
        $this->db->select('COUNT(IF(priority = 3, 1, NULL)) as highData');
        $this->db->select('COUNT(IF(task_status = 1, 1, NULL)) as completedData');
        $this->db->select('COUNT(IF(task_status = 0, 1, NULL)) as dueData');
        $this->db->select('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date, "%Y-%m-%d") as start');
        $this->db->select('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date, "%Y-%m-%d") as end');

        $this->db->from($tbl);

        $this->db->where('assign_to',$userId);
        $this->db->where(array("MONTH(" . $tbl . ".due_date)" => $month, "YEAR(" . $tbl . ".due_date)" => $year,));
        $this->db->where('tbl_crm_participant_schedule_task.parent_id', '0');
        $this->db->where('tbl_crm_participant_schedule_task.archive', '0');
        $this->db->where('tbl_crm_participant_schedule_task.priority!=0');
        // $this->db->where('tbl_crm_participant_schedule_task.archive!=0');
        $this->db->group_by('DATE_FORMAT(tbl_crm_participant_schedule_task.due_date,"%Y %m %d"),tbl_crm_participant_schedule_task.priority');

        $query = $this->db->get();
        $result = $query->result();

        if (!empty($result)) {
           foreach ($result as $val) {
               //if ($val->start >= date("Y-m-d")) {
                $val->lowData = ($val->lowData!=0)?["status" => true, "count" => $val->lowData, "msg" => ""]:'';
                $val->mediumData = ($val->mediumData!=0)?["status" => true, "count" => $val->mediumData, "msg" => ""]:'';
                $val->highData = ($val->highData!=0)?["status" => true, "count" => $val->highData, "msg" => ""]:'';
            //  } else if($val->completedData)  {
            //     $val->lowData = ($val->lowData!=0)?["status" => true, "count" => $val->lowData, "msg" => ""]:'';
            //     $val->mediumData = ($val->mediumData!=0)?["status" => true, "count" => $val->mediumData, "msg" => ""]:'';
            //     $val->highData = ($val->highData!=0)?["status" => true, "count" => $val->highData, "msg" => ""]:'';
            //  }
           }
       }

       return $result;
    }


}
