<?php
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Float_;
use Faker\Provider\zh_CN\DateTime;

//use XeroPayment\XeroPayment as XeroPayment;

defined('BASEPATH') OR exit('No direct script access allowed');

		
class Xero_dashboard extends MX_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library(['form_validation']);
		//it is comming from tbl_company auto increment id
		$params = array('company_id' => 1);
		$this->load->library('XeroPayment',$params);
		$this->load->library('XeroContact',$params);
		$this->load->library('XeroInvoice',$params);
		$this->form_validation->CI = & $this;
		$this->load->model(['Basic_model']);
	}
	

	/* 
	$companyId=1; it is comming from tbl_company auto increment id
	following fillter will use for get invoice
	$fillterRequest = ['page'=>$page,
			//'Status'=>'AUTHORISED', // use "get_invoice_status_list()" functon for valid status option value 
			//'Type'=>'ACCREC',// use "get_invoice_types_list()" functon for valid type option value 
			//'InvoiceNumber'=>'ORC1097',
			//'InvoiceID'=>'ff825165-45ca-4308-8a5e-db4528b6a648',
			//'Contact'=>['ContactID'=>'ae948738-1a53-42d8-bfa4-b4b076be64cd','Name'=>'test indore'],
			// if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'Contact_Name' attribute,
			//'Reference'=>['type'=>'Contains','value'=>'test1'],
			//'Reference'=>'test Reference',
			//'Date' =>[['type'=>'fromDate','value'=>'2019-08-05'],['type'=>'toDate','value'=>'2019-08-07']],//use date value format "Y-m-d" string. it give result between two date range;
			//'Date' =>[['type'=>'fromDate','value'=>'2019-08-05']],//use date value format "Y-m-d" string. it give result graterthen equal to given date;
			//'Date' =>[['type'=>'toDate','value'=>'2019-08-05']],//use date value format "Y-m-d" string. it give result lessthen equal to given date;
			//'Date' =>'2019-08-05',//use date format "Y-m-d" string. it give result to equalto date;
			//'DueDate' => '2019-08-05',//use date format "Y-m-d" string. it give result to equalto  duedate;
			//'DueDate' =>[['type'=>'fromDate','value'=>'2019-08-05'],['type'=>'toDate','value'=>'2019-09-07']],//use date format "Y-m-d" string. it give result between two duedate range;
			//'order_by'=>['field_name'=>'InvoiceID','direction'=>'ASC'],
			//'modifiedAfter'=>new \DateTime(date('Y-m-d H:i:s'))
		];
	errorResponseAttribute: status,msg,  
	SuccessResponseAttribute: status,data,currentPage,nextPage  
	*/

	public function get_invoice_list($page=1){
		$companyId = 1;
		$page = !empty($page) && $page>0 ? $page:1;
		$fillterRequest = ['page'=>$page];
		$inoviceDetails = $this->xeroinvoice->get_invoice_list($fillterRequest);
		//$inoviceDetails = $this->xeroinvoice->get_item_code();
		pr($inoviceDetails);	
		if($inoviceDetails['status']){
			echo 'success method if:';
			pr($inoviceDetails);
		}else{
			echo 'Error on method else:';
			pr($inoviceDetails);	
		}
		
	}
/* 
following feild must be required
Type
ContactID
LineItems
*/
	public function create_invoice(){
		$companyId = 1;
		$insData = [
			'Type' => 'ACCREC',
			'ContactID'=>'ae948738-1a53-42d8-bfa4-b4b076be64cd',
			'LineItems' => [
				[
					'Description'=>'Consulting services as agreed (20% off standard rate',
					'Quantity'=>10,
					'UnitAmount'=>100.00,
					'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
					'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
					'DiscountRate'=>12,
					'Tracking'=>['Name'=>'Region','Option'=>'Eastside']
				],
				[
					'Description'=>'Consulting services as agreed (20% off standard rate)data',
					'Quantity'=>20,
					'UnitAmount'=>500.00,
					'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
					'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
					'DiscountRate'=>0,
					'Tracking'=>['Name'=>'Region','Option'=>'North']
				],

			],
			'Date'=>date('Y-m-d'),
			'DueDate'=>date('Y-m-d',strtotime('+7 days')),
			'LineAmountTypes'=>'Exclusive',// get option list from get_invoice_lineamount_list function
			'Reference'=>'test reference',//ACCREC type only,
			'BrandingThemeID'=>'',//get brandingthimeid form get_brandingthemes function, 
			'Url'=>base_url(),//URL link to a source document - shown as "Go to [appName]" in the Xero app, 
			'CurrencyCode'=>'AUD',//get CurrencyCode form get_currency_code function,
			'CurrencyRate'=>'',//The currency rate for a multicurrency invoice. If no rate is specified, the XE.com day rate is used. (max length = [18].[6]),
			'Status'=>'DRAFT',//get CurrencyCode form get_currency_code function,
			'SentToContact'=>1,//Boolean to set whether the invoice in the Xero app should be marked as "sent". This can be set only on invoices that have been approved
			'ExpectedPaymentDate'=>date('Y-m-d'),//Shown on sales invoices (Accounts Receivable) when this has been set
			'PlannedPaymentDate'=>date('Y-m-d'),//Shown on bills (Accounts Payable) when this has been set

		];
		$indoviceDetails =$this->xeroinvoice->create_inovice($companyId,$insData);
		pr($indoviceDetails);
	}

	public function update_invoice(){
		$companyId = 1;
		$updateData = [
			'InvoiceID'=>'fadd2b68-7dfb-4d99-a3f5-27d02163f244',
			'Type' => 'ACCREC',
			'ContactID'=>'ae948738-1a53-42d8-bfa4-b4b076be64cd',
			'LineItems' => [
				[
					'Description'=>'Consulting services as agreed (20% off standard rate',
					'Quantity'=>10,
					'UnitAmount'=>100.00,
					'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
					'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
					'DiscountRate'=>12,
					'Tracking'=>['Name'=>'Region','Option'=>'Eastside'],
					//'LineItemID'=>'7b4784fa-2209-43e1-984c-68f301dd89b6'
				],
				[
					'Description'=>'Consulting services as agreed ',
					'Quantity'=>30,
					'UnitAmount'=>300.00,
					'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
					'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
					'DiscountRate'=>10,
					'Tracking'=>['Name'=>'Region','Option'=>'North'],
					
				],
				[
					'Description'=>'Consulting services as agreed (20% off standard rate)data',
					'Quantity'=>20,
					'UnitAmount'=>500.00,
					'ItemCode'=>'GB1-White',// for getting item code using call get_item_code() in Xero_model
					'AccountCode'=>200,// for getting account code using call get_item_code() in Xero_model, Side result see inside "SalesDetails" attribute 
					'DiscountRate'=>0,
					'Tracking'=>['Name'=>'Region','Option'=>'North'],
					//'LineItemID'=>'f42f9f10-d9f3-412c-bc4d-6e27ce419499'
				],

			],
			'Date'=>date('Y-m-d'),
			'DueDate'=>date('Y-m-d',strtotime('+7 days')),
			'LineAmountTypes'=>'Exclusive',// get option list from get_invoice_lineamount_list function
			'Reference'=>'test reference',//ACCREC type only,
			'BrandingThemeID'=>'',//get brandingthimeid form get_brandingthemes function, 
			'Url'=>base_url(),//URL link to a source document - shown as "Go to [appName]" in the Xero app, 
			'CurrencyCode'=>'AUD',//get CurrencyCode form get_currency_code function,
			'CurrencyRate'=>'',//The currency rate for a multicurrency invoice. If no rate is specified, the XE.com day rate is used. (max length = [18].[6]),
			'Status'=>'AUTHORISED',//get CurrencyCode form get_currency_code function,
			//'SentToContact'=>1,//Boolean to set whether the invoice in the Xero app should be marked as "sent". This can be set only on invoices that have been approved
			'ExpectedPaymentDate'=>date('Y-m-d'),//Shown on sales invoices (Accounts Receivable) when this has been set
			'PlannedPaymentDate'=>date('Y-m-d'),//Shown on bills (Accounts Payable) when this has been set

		];
		$indoviceDetails =$this->xeroinvoice->update_inovice($companyId,$updateData);
		pr($indoviceDetails);
	}

	public function delete_invoice(){
		$companyId = 1;
		$deleteData = ['InvoiceID'=>'ff825165-45ca-4308-8a5e-db4528b6a648'];
		$indoviceDetails =$this->xeroinvoice->delete_inovice($companyId,$deleteData);
	}

	public function invoice_status_mark_as_voided(){
		$companyId = 1;
		$deleteData = ['InvoiceID'=>'ff825165-45ca-4308-8a5e-db4528b6a648'];
		$indoviceDetails =$this->xeroinvoice->invoice_status_mark_as_voided($companyId,$deleteData);
	}
	
	/* $fillterRequest = 
		[	'page'=>'1',
			'ContactID'=>'bd2270c3-8706-4c11-9cfb-000b551c3f51',
			'ContactNumber'=>'ID001',
			'AccountNumber'=>'',
			'ContactStatus'=>'ACTIVE',// use get_contact_status_list on xerocontact 
			'Name'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'Name' attribute,
			'FirstName'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'FirstName' attribute,
			'LastName'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'LastName' attribute,
			'EmailAddress'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'EmailAddress' attribute,
			'EmailAddress'=>'test@xyz.com' //if equal name then use string not array type this 'EmailAddress' attribute,
			'BankAccountDetails'=>'455454545',
			'TaxNumber'=>'5455454',
			'IsCustomer'=>'TRUE',// only pass boolan value
			'IsSupplier'=>'TRUE',// only pass boolan value
			'DefaultCurrency'=>'AUD',
			'Discount'=>'',//use only integere value with type casting int for example (int) $discountRate;
			'modifiedAfter'=>new \DateTime(date('Y-m-d H:i:s'));
			'HasAttachments'=>'TRUE',A boolean to indicate if a contact has an attachment
			'order_by'=>['field_name'=>'ContactID','direction'=>'ASC']
		]; */
	public function get_contact_list($page=1){
		$companyId = 1;
		$page = !empty($page) && $page>0 ? $page:1;
		$order_by = ['field_name'=>'Name','direction'=>'ASC'];
		$contactDetails = $this->xerocontact->get_contact_list($companyId,['page'=>$page,'modifiedAfter'=>new \DateTime(date('2019-07-17')),'order_by'=>$order_by]);
		pr($contactDetails);
	}

	/* $fillterRequest = 
		[	
			'Date'=>'2019-05-16',
			'Amount'=>'20',
			'Reference'=>'DD 112102',
			'IsReconciled'=>true,
			'Status'=>'AUTHORISED',// use get_payment_status_list on xeropayment 
			'PaymentType'=>'APCREDITPAYMENT',// use get_payment_type_list on xeropayment 
			'Invoice'=>['InvoiceNumber'=>['type'=>'Contains','value'=>'118']] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'Invoice' attribute,
			'Invoice'=>['InvoiceNumber'=>'ORC1118'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'Name' attribute,
			'Invoice'=>['Contact'=>['Name'=>['type'=>'Contains','value'=>'test']]] 
			'Invoice'=>['Contact'=>['Name'=>'test indore']] 
			'FirstName'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'FirstName' attribute,
			'LastName'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'LastName' attribute,
			'EmailAddress'=>['type'=>'EndsWith','value'=>'e'] // if use onlyfollowing type 'EndsWith','StartsWith','Contains'. if equal name then use string not array type this 'EmailAddress' attribute,
			'EmailAddress'=>'test@xyz.com' //if equal name then use string not array type this 'EmailAddress' attribute,
			'BankAccountDetails'=>'455454545',
			'TaxNumber'=>'5455454',
			'IsCustomer'=>'TRUE',// only pass boolan value
			'IsSupplier'=>'TRUE',// only pass boolan value
			'DefaultCurrency'=>'AUD',
			'Discount'=>'',//use only integere value with type casting int for example (int) $discountRate;
			'modifiedAfter'=>new \DateTime(date('Y-m-d H:i:s'));// it is use for UpdatedDateUTC value column will use
			'HasAttachments'=>'TRUE',A boolean to indicate if a contact has an attachment
			'order_by'=>['field_name'=>'ContactID','direction'=>'ASC']
		]; */
	public function get_payment_list(){
		$companyId = 1;
		$order_by = ['field_name'=>'Amount','direction'=>'ASC'];
		$contactDetails = $this->xeropayment->get_payment_list(['order_by'=>$order_by]);
		pr($contactDetails);
	}

	/* 
following feild must be required for create
Name : unique name is required

following feild must be required for delete
'ContactID'=>'7918e892-c753-41ea-9cd8-0da82d3d6e74',
'ContactStatus'=>\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ARCHIVED, delete only when current status is active

following feild must be required for update
'ContactID'=>'7918e892-c753-41ea-9cd8-0da82d3d6e74' and another update feild for update
*/
public function create_update_delete_contact(){
	$companyId = 1;
	$insData = [
		/* 'Name' => 'ABC Limited tes110',
		'AccountNumber'=>'474797967050', */
		'ContactID'=>'7918e892-c753-41ea-9cd8-0da82d3d6e74',
		'ContactStatus'=>\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ARCHIVED,
		/* 'FirstName'=>'Test',
		'LastName'=>'Demo',
		'EmailAddress'=>'test@demomailinator.com',
		'SkypeUserName'=>'fdfd',
		'BankAccountDetails'=>'01-0123-0123456-00',
		'TaxNumber'=>'12-345-678',
		'AccountsReceivableTaxType'=>'OUTPUT',
		'AccountsPayableTaxType'=>'INPUT', */
		/* 'Addresses' => [
			[
				'AddressType'=>'POBOX',
				'AddressLine1'=>'P O Box 1232',
				'AddressLine2'=>'P O Box 123',
				'AddressLine3'=>'P O Box 12ddsd3',
				'City'=>'Wellington',
				'PostalCode'=>'6011',
				'Region'=>'northside',
				'Country'=>'AU'
			],
			[
				'AddressType'=>'STREET',
				'AddressLine1'=>'P O Box 123',
				'AddressLine2'=>'P O Box 1234',
				'City'=>'Wellington',
				'PostalCode'=>'6011',
				'Region'=>'northside',
				'Country'=>'AU',
				'AttentionTo'=>'Andrea',
			]

		], */
		/* 'ContactPersons' => [
			[
				'FirstName'=>'test',
				'LastName'=>'demo contact 1',
				'EmailAddress'=>'test@demomailinator.com',
				'IncludeInEmails'=>true
			],
			[
				'FirstName'=>'test',
				'LastName'=>'demo contact 2',
				'EmailAddress'=>'test@demomailinator.com',
				'IncludeInEmails'=>false
			]

		],
		'Phones' => [
			[
				'PhoneType'=>'DEFAULT',
				'PhoneNumber'=>'1111111',
				'PhoneAreaCode'=>'10',
				'PhoneCountryCode'=>'64'
			],
			[
				'PhoneType'=>'DDI',
				'PhoneNumber'=>'11111112',
				'PhoneAreaCode'=>'14',
				'PhoneCountryCode'=>'64'
			]

		],
		'DefaultCurrency'=>'AUD',
		'SalesDefaultAccountCode'=>'200',
		'PurchasesDefaultAccountCode'=>'300',
		'SalesTrackingCategories'=>[['TrackingCategoryName'=>'Region','TrackingCategoryOption'=>'North']],
		'PurchasesTrackingCategories'=>[['TrackingCategoryName'=>'Region','TrackingCategoryOption'=>'North']],
		'PaymentTerms'=>'DAYSAFTERBILLDATE', */

	];
	$indoviceDetails =$this->xerocontact->create_contact($companyId,$insData);
	pr($indoviceDetails);
}
public function create_payment(){
	$insData = [
		//'Invoice' =>['InvoiceID'=>'ff2f5c1e-c896-4b7a-ba5f-0950112a4577'],
		//'CreditNote' =>['CreditNoteNumber'=>'ORC1149'],
		//'Prepayment ' =>['PrepaymentID '=>'ORC1149'],
		//'Overpayment' =>['OverpaymentID'=>'ORC1149'],
		'Account' => ['Code'=>"855"],
		'Date'=> \DateTime::createFromFormat('Y-m-d', "2019-08-07"),
		"Amount"=> 13,
		'Reference' =>'test', 
		'IsReconciled'=>(Boolean) 'flase',
		//'Status'=>'',
		'CurrencyRate'=>'0.8',
	];
	$indoviceDetails =$this->xeropayment->create_payment($insData);
	pr($indoviceDetails);
}
public function delete_payment(){
	$insData = [
		'Status'=>'DELETED',
		'PaymentID' =>'fefe02a4-be0a-4f14-87c5-67a6b2f2d538'
	];
	$indoviceDetails =$this->xeropayment->create_payment($insData);
	pr($indoviceDetails);
}

}