<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Common_model extends CI_Model {

    function __construct() {

        parent::__construct();
    }

    public function get_suburb($post_data, $state) {
        $tbl_suburb_state = TBL_PREFIX . 'suburb_state';

        $this->db->select(array('suburb as value', 'suburb as label', 'postcode'));
        $this->db->from($tbl_suburb_state);
        $this->db->where(array('stateId' => $state));
        $this->db->group_by('suburb');
        $this->db->like('suburb', $post_data);
        $query = $this->db->get();

        return $result = $query->result();
    }

    public function get_user_for_compose_mail($reqData) {

        $name = $this->db->escape_str($reqData->search);
        $sql = array();


        $sql[] = "select concat(firstname, ' ', middlename, ' ', lastname) as  label, '2'  as type , id as value from tbl_participant WHERE archive = 0 and concat(firstname, ' ', middlename, '', lastname) LIKE '%" . $name . "%'";

        $sql[] = "select concat(firstname, ' ', middlename, ' ', lastname) as  label, '3'  as type , tbl_member.id as value from tbl_member LEFT JOIN tbl_department on tbl_member.department = tbl_department.id AND tbl_department.short_code = 'external_staff' WHERE tbl_member.archive = 0 and concat(firstname, ' ', middlename, '', lastname)  LIKE '%" . $name . "%' ";

        $sql[] = "select name as  label, '4'  as type , id as value from tbl_organisation WHERE archive = 0 and name  LIKE '%" . $name . "%' ";

        $sql = implode(' union ', $sql);
        $query = $this->db->query($sql);
//        last_query();
        return $result = $query->result();
    }

    public function get_admin_name($reqData, $currentAdminId) {

        $this->db->select("concat(firstname,' ', lastname) as label");
        $this->db->select("m.id as value");
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "internal_staff"');
        $this->db->like('m.firstname', $reqData->search);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_admin_team_department($name, $currentAdminId) {
        $name = $this->db->escape_str($name);

        $query = $this->db->query("select concat(firstname,' ', lastname,' - (Staff)') as  label, '1'  as type, id as value from tbl_member as m INNER JOIN tbl_department as d on d.id = m.department AND d.short_code = 'internal_staff' WHERE archive = 0 and concat(firstname, ' ', lastname) LIKE '%" . $name . "%' and id != " . $currentAdminId . "
            union 

            select concat(team_name,' - (My Team)') as  label, '2'  as type, id as value from tbl_internal_message_team 
            WHERE archive = 0 and team_name LIKE '%" . $name . "%' and adminId =" . $currentAdminId . "
            union 

            select concat(name,' - (Department)') as  label, '3'  as type, id as value from tbl_department 
            WHERE archive = 0 and name LIKE '%" . $name . "%'");

        return $query->result();
    }

    public function get_global_search_data($search, $adminId) {
        $all_permission = get_all_permission($adminId);


        $search = $this->db->escape_str($search);

        $sql = array();
        if (array_key_exists('access_admin', $all_permission)) {
            $sql[] = "select concat(firstname,' ', lastname) as  label, 'a' as type, tbl_member.id as value, concat('/admin/user/update/', tbl_member.id) as url from tbl_member INNER JOIN tbl_department on tbl_member.department = tbl_department.id AND tbl_department.short_code = 'internal_staff' left join tbl_member_email on tbl_member_email.memberId = tbl_member.id
            WHERE tbl_member.archive = 0 and (concat(firstname, ' ', lastname) LIKE '%" . $search . "%' OR tbl_member.id = '" . $search . "' OR tbl_member_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_participant', $all_permission)) {
            $sql[] = "select concat(firstname, ' ',middlename,' ',lastname) as label, 'p'  as type, tbl_participant.id as value, concat('/admin/participant/about/', tbl_participant.id) as url from tbl_participant 
            left join tbl_participant_email on tbl_participant_email.participantId = tbl_participant.id
            WHERE archive = 0 and (concat(firstname, ' ',middlename,' ',lastname) LIKE '%" . $search . "%' OR tbl_participant.id = '" . $search . "' OR tbl_participant_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_member', $all_permission)) {
            $sql[] = "select concat(firstname, ' ',middlename,' ',lastname) as label, 'm'  as type, tbl_member.id as value, concat('/admin/member/about/', tbl_member.id) as url from tbl_member INNER JOIN tbl_department on tbl_member.department = tbl_department.id AND tbl_department.short_code = 'external_staff' left join tbl_member_email on tbl_member_email.memberId = tbl_member.id
            WHERE tbl_member.archive = 0 and (concat(firstname, ' ',middlename,' ',lastname) LIKE '%" . $search . "%' OR tbl_member.id = '" . $search . "' OR tbl_member_email.email LIKE '%" . $search . "%')";
        }

        if (array_key_exists('access_schedule', $all_permission)) {
            $sql[] = "select 'Shift' as label, 's'  as type, id as value, concat('/admin/schedule/details/', id) as url from tbl_shift 
            WHERE status != 8 and (id = '" . $search . "')";
        }

        if (array_key_exists('access_fms', $all_permission)) {
            $sql[] = "select 'Fms' as label, 'f'  as type, id as value, concat('/admin/fms/case/', id) as url from tbl_fms_case 
            WHERE (id = '" . $search . "') ";
        }

        if (array_key_exists('access_organization', $all_permission)) {
            $sql[] = "select name as label, 'o'  as type, id as value, concat('/admin/organisation/about/', id) as url from tbl_organisation WHERE archive = '0' AND parent_org = 0  AND (name LIKE '%" . $search . "%' OR id = '" . $search . "' OR abn = '" . $search . "') ";
        }

        $sql = implode(' union ', $sql);
        $query = $this->db->query($sql);


        $result = $query->result();

        return $result;
    }

    public function get_member_name($post_data) {
        $this->db->like("CONCAT(m.firstname,' ',m.middlename,' ',m.lastname)", $post_data);

        $this->db->where('m.archive', 0);
        $this->db->where('m.status', 1);
        $this->db->select("CONCAT(m.firstname,' ',m.middlename,' ',m.lastname) as label");
        $this->db->select(array('m.id as value'));
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"');
        $query = $this->db->get(TBL_PREFIX . 'member as m');

        //last_query();
        return $query->result();
    }

    public function get_org_name($post_data) {
        $this->db->like('name', $post_data, 'both');
        $this->db->where('archive', '0');
        $this->db->select(array('name', 'id', 'abn'));
        $query = $this->db->get(TBL_PREFIX . 'organisation');
        //last_query();
        #$query->result();
        $org_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $org_rows[] = array('label' => $val->name, 'value' => $val->id, 'abn' => $val->abn);
            }
        }
        return $org_rows;
    }

    public function get_recruitment_staff($post_data) {

        $this->db->select(array($tbl_1 . '.id', "CONCAT(m.firstname,' ',m.lastname) AS name"));
        $this->db->join('tbl_recruitment_staff', 'tbl_recruitment_staff.adminId = m.id', 'inner');
        $this->db->where('m.archive', "0");
        $this->db->like("CONCAT(m.firstname,' ',m.lastname)", $post_data);
        $query = $this->db->get('tbl_member as m');
        $staff_rows = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $staff_rows[] = array('label' => $val->name, 'value' => $val->id);
            }
        }
        return $staff_rows;
    }

    public function file_content_media($type, $file_name, $userId, $checkToken = 0, $extratParm = []) {
        $this->load->helper('file');
        $token = isset($extratParm['token']) ? $extratParm['token'] : '';
        $genderType = isset($extratParm['genderType']) ? $extratParm['genderType'] : '1';
        $defaultImageShow = isset($extratParm['defaultImageShow']) ? $extratParm['defaultImageShow'] : '0';
        $filePath = FCPATH;
        $permission_key = 0;
        if ($type == 'r') {
            $permission_key = 'access_recruitment';
            $filePath .= APPLICANT_ATTACHMENT_UPLOAD_PATH . $userId . '/' . $file_name;
        }if ($type == 'p') {
            $permission_key = 'access_participant';
        } else if ($type == 'o') {
            $permission_key = 'access_organization';
            $filePath .= ORG_UPLOAD_PATH . $userId . '/' . $file_name;
        } else if ($type == 'fq') {
            $permission_key = 'access_finance_quote';
            $filePath .= QUOTE_FILE_PATH . $file_name;
        } else if ($type == 'fe') {
            $permission_key = 'access_finance_shift_and_payroll';
            $filePath .= FINANCE_PAYROLL_EXEMPTION_ORG_UPLOAD_PATH . $userId . '/' . $file_name;
        } else if ($type == 'rp') {
            // Ipad api view presentation slides
            $permission_key = 'access_finance_shift_and_payroll';
            $filePath .= IPAD_DEVICE_PRESENTATION_PATH . $file_name;
        } else if ($type == 'rc') {
            // Ipad api view recruitment cab day draft contract
            $permission_key = 'access_finance_shift_and_payroll';
            $filePath .= CABDAY_INTERVIEW_CONTRACT_PATH . $file_name;
        } else if ($type == 'rg') {
            // Ipad api view recruitment group intreview draft contract
            $permission_key = 'access_finance_shift_and_payroll';
             $filePath .= GROUP_INTERVIEW_CONTRACT_PATH. $file_name;            
        }else if ($type == 'fi') {
            $permission_key = 'access_finance_invoice';
            $filePath .= FINANCE_INVOICE_FILE_PATH . $file_name;
        }else if ($type == 'fs') {
            $permission_key = 'access_finance_invoice';
            $filePath .= FINANCE_STATEMENT_FILE_PATH . $file_name;
        }  

        if ($checkToken == 1) {
            $response = verifyAdminToken((object) ['token' => $token], $permission_key, 0);

            if (empty($response['status'])) {
                return ['status' => false, 'msg' => 'Access denied'];
            }
        }
        $mimeType = '';
        $string = '';
        $status = true;
        if ($type == 'p') {
            $url = get_participant_img($userId, $file_name, $genderType);
            $mimeType = get_mime_by_extension($url);
            $string = file_get_contents($url);
        } else if (is_file($filePath) && file_exists($filePath)) {
            $mimeType = get_mime_by_extension($filePath);
            header('content-type: ' . $mimeType);
            $string = read_file($filePath);
        } else if (!is_file($filePath) && $defaultImageShow == 1) {
            $filePath = FCPATH . NO_IMAGE_PATH;
            $mimeType = get_mime_by_extension($filePath);
            header('content-type: ' . $mimeType);
            $string = read_file($filePath);
        } else {
            $status = false;
            $string = 'File not found';
        }

        return ['status' => $status, 'msg' => $string, 'mimetype' => $mimeType];
    }

}
