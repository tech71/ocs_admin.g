<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('TBL_PREFIX', 'tbl_'); // table prefix
define('APPLICATION_NAME', 'HCM'); // application Name

define('ROOT_FOLDER', '/ocs_development/');
define('DATE_TIME', date('Y-m-d H:i:s'));

define('MEMBER_UPLOAD_PATH', './uploads/member/');
define('PARTICIPANT_UPLOAD_PATH', './uploads/participant/');
define('CRM_UPLOAD_PATH', './uploads/crmparticipant/');
define('EXTERNAL_IMAIL_PATH', './uploads/external_imail/');
define('INTERNAL_IMAIL_PATH', './uploads/internal_imail/');
define('GROUP_MESSAGE_PATH', './uploads/group_message/');
define('CASE_UPLOAD_PATH', './uploads/case/');
define('ORG_UPLOAD_PATH', './uploads/organisation/');
define('HOUSE_UPLOAD_PATH', './uploads/house/');
define('APPLICANT_ATTACHMENT_UPLOAD_PATH', './uploads/recruitment/applicant/');
define('FINANCE_PAYROLL_EXEMPTION_ORG_UPLOAD_PATH', './uploads/finance/payrollexemption/organisations/');
define('FINANCE_IMPORT_NDIS_STATUS_UPLOAD_PATH', './uploads/finance/import/ndis/');
//define('GOOGLE_MAP_KEY', 'AIzaSyDKpKW5X2adkkMh9Y0ihSM5itzZtHklMOE'); // fnc key 

define('GOOGLE_MAP_KEY', 'AIzaSyAV5zEPxeuJ0zeO_ue62RgqMwZN9cUVgJ4'); // ocs Key
define('GUID', '58be584e-206e-4b04-a870-2fe6563b363b');
define('ARCHIEVE_DIR', 'archieve');
define('JWT_SECRET_KEY', 'J8r$E4v6R-CSS-wmNERn');
define('PARTICIPANT_PROFILE_PATH', (getenv('OCS_PARTICIPANT_PROFILE_PATH') ? getenv('OCS_PARTICIPANT_PROFILE_PATH') : 'http://client.ydtwebstaging.com/client/uploads/participant/'));
define('ADMIN_PROFILE_PATH', './uploads/admin/');
define('ENABLE_MAIL', FALSE);
//define('PHONE_REGEX_KEY','/^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{8,18}$/');
define('PHONE_REGEX_KEY', '/^(?=.{8,18}$)(\(?\+?[0-9]{1,3}\)?)([ ]{0,1})?[0-9_\- \(\)]{8,18}$/');
define('POSTCODE_AU_REGEX_KEY', '/^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/');
define('DEFAULT_ATTACHMENT_UPLOAD_TYPE', 'jpg|jpeg|png|xlx|xls|doc|docx|pdf');
define('FINANCE_PAYROLL_EXEMPTION_ORG_UPLOAD_TYPE', 'jpg|jpeg|png|pdf');
define('NO_IMAGE_PATH', './assets/img/no-image.png');
define('FINANCE_IMPORT_NDIS_STATUS_UPLOAD_TYPE', 'csv');

/*
 *  webscoket contact
 */
define('WEBSOCKET_SERVER_ON', false);
define('WEBSOCKET_SERVER_KEY', 'SDS--58be584eADSS58be584e---SDSD');
define('WEBSOCKET_HOST_NAME', 'localhost');
define('WEBSOCKET_HOST_PORT', '9000');
define('DB_DATE_FORMAT', 'Y-m-d');
define('DATE_CURRENT', date('Y-m-d'));

/*
 * Docu sign credentials
 */

define('DS_STAGING_USERNAME', "developer@yourdevelopmentteam.com.au");
define('DS_STAGING_PASSWORD', "Ydt@2019");
define('DS_STAGING_INTEGRATATION_KEY', "YDTD-265ff68a-f01b-4e5f-91c9-d2dd916829e6");
define('DS_STAGING_HOST', "https://demo.docusign.net/restapi");
define('DS_BASE_PATH', (getenv('OCS_ADMIN_URL') ? getenv('OCS_ADMIN_URL') : 'https://dev.ydtwebstaging.com/'));
define('DS_STAGING_WEBHOOK_URL_CABDAY', 'recruitment/RecruitmentCabDayInterview/callback_cabday_docusign');


define('PAY_SCALE_DOC_CATEGORY', "Resume");

define('GROUP_INTERVIEW_CONTRACT_PATH', './uploads/recruitment/groupinterview_contract/');
define('CABDAY_INTERVIEW_CONTRACT_PATH', './uploads/recruitment/cabdayinterview_contract/');

define('IPAD_DEVICE_PRESENTATION_PATH', './uploads/recruitment/presentations/');

define('FROM_EMAIL', "info@healthcaremgr.net");

define('MESSAGE_CHANNEL', 'MAIL');
#define('MESSAGE_CHANNEL', 'MESSAGE');
define('MESSAGE_SEND_FROM', 'OnCall');
define('ESENDEX_ACCOUNT_REFERENCE', 'EX0276649');
define('SMS_LOGIN_EMAIL_ADDRESS', 'ritesh@cssindiaonline.com');
define('SMS_LOGIN_PASSWORD', 'ritesh1234');
define('QUOTE_FILE_PATH', './uploads/finance/quotes/');
define('FINANCE_INVOICE_FILE_PATH', './uploads/finance/invoice/');
define('FINANCE_STATEMENT_FILE_PATH', './uploads/finance/statement/');

define('ADMIN_EMAIL', 'testteam.developer@gmail.com');
define('MAIL_METHOD', 'PHP_MAILER'); // like PHP_MAILER||SMTP
define('INVOICE_GST', 10);
define('RECRUITMENT_BY_PASS_DEMO_STAGE', 1);
define('XERO_DEFAULT_COMPANY_ID', 1);
define('DEFAULT_MAX_UPLOAD_SIZE', 5120);
define('INVOICE_PAYMENT_STATUS_DATA',json_encode(['0'=>'payment pending','1'=>'payment received','2'=>'payment not received']));
define('INVOICE_PAYMENT_NDIS_IMPORT_STATUS_DATA',json_encode(['paid'=>'1','rejected'=>'2']));
