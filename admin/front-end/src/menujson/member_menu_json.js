export const memberActiveTitle = {
    details: "Details",
    availability: "Availability",
    preferences: "Preferences",
    current_docs: "Current Qualifications",
    qual_docs: "Qualifications Documents",
    current_work_area: "Current Work Areas",
    award_level: "Position/Award Level",
    special_agreement: "Special Agreements",
    fms: "FMS",
    upcoming_shift: "Upcoming Shifts",
    completed_shift: "Completed Shifts",
    cancelled_shift: "Cancelled Shifts",
    contact_history: "Contact History",
    overview: "Overview",
  };
  
  export const memberJson = [
    { name: 'Dashboard',id: "member_dashboard", submenus: [], path: '/admin/member/dashboard' },
    {
        name: "name",
        pathstructure: "/admin/member/about/:id/details",
        type: 1,
        path: "/admin/member/about/",
        linkShow: false,
        id: "member_name",
        className: "active"
      },
    {name: 'Member Details',
    id: "member_details",
    linkShow: false,
        submenus: [
            { name: 'About', path: '#', subSubMenu: 
                [
                    { name: 'Details', path: '#', pathstructure: "/admin/member/about/:id/details", type:1 },
                    { name: 'Availability', path: '#' , pathstructure: "/admin/member/about/:id/availability", type:1},
                    { name: 'Preferences', path: '#' , pathstructure: "/admin/member/about/:id/preferences", type:1},
                ]
            },

            { name: 'Qualifications', path: '#', subSubMenu: 
                [
                    { name: 'Current Qualifications', path: '#' , pathstructure: "/admin/member/quals/:id/current_docs", type:1},
                    { name: 'Qualifications Documents', path: '#' , pathstructure: "/admin/member/quals/:id/qual_docs", type:1},
                ]
            },

            { name: 'Work Areas', path: '#', subSubMenu: 
                [
                    { name: 'Current Work Areas', path: '#' , pathstructure: "/admin/member/work_area/:id/current_work_area", type:1},
                    { name: 'Position/Award Level', path: '#', pathstructure: "/admin/member/work_area/:id/award_level", type:1 },
                    { name: 'Special Agreements', path: '#' , pathstructure: "/admin/member/work_area/:id/special_agreement", type:1}
                ]
            },
            { name: 'FMS', subSubMenu: [], path: '#' , pathstructure: "/admin/member/fms/:id", type:1,moduelname: "fms"},
        ]
    },

    {name: 'Shift & Rosters',
    id: "member_shift_rosters",
    linkShow: false,
        submenus: [
            { name: 'Shift', path: '#', subSubMenu: 
                [
                    { name: 'Upcoming Shifts', path: '#', pathstructure: "/admin/member/shifts/:id/upcoming_shift", type:1 },
                    { name: 'Complete Shifts', path: '#', pathstructure: "/admin/member/shifts/:id/completed_shift", type:1 },
                    { name: 'Cancelled Shifts', path: '#' , pathstructure: "/admin/member/shifts/:id/cancelled_shift", type:1},
                ]
            },
            { name: 'Contact History', path: '#' , pathstructure: "/admin/member/contact_history/:id", type:1},
            { name: 'Overview', path: '#', pathstructure: "/admin/member/overview/:id", type:1},
        ]
    },



];
  