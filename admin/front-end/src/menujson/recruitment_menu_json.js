export const recruitmentActiveTitle = {
    dashboard: 'Dashboard',
    usermanagment: 'Recruiter Management',
    taskschdule: 'Schedules',
    tasklisting: 'Tasks',
    createjob: 'Create New Job',
    editjob: 'Edit Job',
    duplicatejob: 'Duplicate Job',
    jobopening: 'Job Openings',
    applicants: 'Applicants',
    group_question_list: 'Group Interview Questions',
    cabday_question_list: 'CAB Day Questions',
    ipad: 'Training',
    cabday: 'CAB Day',
    applicantinfo: 'Applicants - Applicant Info',
    MyComponents: 'My Components',
    staff_details: 'Recruiter Details',
    flagged_applicants: 'Flagged Applicants',
    duplicate_applicants: 'Duplicate Applicants',
    roundrobin_management: 'Round Robin Management',
    PayRateApproval: 'Pay Scale Approvals',
    manage_group_interview: 'Manage Group Interview',
    cabday_interview: 'Manage CAB Day',
    group_interview_result: 'Applicants - Group Interview Results',
    cabday_interview_result: 'Applicants - CAB Day Interview Results',
    device_list: 'Devices',
    manage_devices_list: 'Manage Devices',
	communication_logs:'Communication Logs'
};

export const recruitmentHideShowSubmenusPermissionBase = {
    "recruiter_managment_menu":{
        'hideLink':true,
        'submenu':{
            'recruiter_managment_recruiter_menu':{'hideLink':true},
            'recruiter_managment_department_menu':{'hideLink':true},
            'recruiter_managment_round_robin_menu':{'hideLink':true}
            }
    },
    "dashboard_menu":{
        "hideLink":false,
        "submenu":{
            "dashboard_duplicate_applicants_menu" :{'hideLink':true},
            "dashboard_flag_applicants_menu" :{'hideLink':true},
            "dashboard_pay_approval_menu" :{'hideLink':true}
        }
    }

    /* 'training_menu':{ 
        'hideLink':true,
        'submenu':{
            'training_group_interview_menu' :{
                'hideLink':true,
                'submenu': {
                    'training_group_interview_question_list_menu':{'hideLink':true},
                    'training_group_interview_manage_group_interview_menu':{'hideLink':true}
                }
            },
        }
    } */
};

export const recruitmentHideShowSubmenus = {
    "applicants_menu":{
        'hideLink':false,
        'submenu':{
            'applicant_info_menu':{
                'hideLink':true,
                'page_type_show':'applicantinfo'
            },
            }
    }
    };


   
export const recruitmentJson = [
    { 
        name: 'Dashboard', 
        id:'dashboard_menu',
        submenus: [
            {name:'Flagged Applicants', path:'/admin/recruitment/dashboard/flagged_applicants',id:'dashboard_flag_applicants_menu'},
            {name:'Duplicate Applicants', path:'/admin/recruitment/dashboard/duplicate_applicants',id:'dashboard_duplicate_applicants_menu'},
            {name:'Pay Scale Confirmations', path:'/admin/recruitment/dashboard/PayRateApproval',id:'dashboard_pay_approval_menu'}
        ], 
        path: '/admin/recruitment/dashboard' 
    },
    { name: 'Schedules', submenus: [{ name: 'Task Schedules', path: '/admin/recruitment/action/schedule' }, { name: 'Tasks List', path: '/admin/recruitment/action/task' }], },
    { name: 'Jobs',path: '/admin/recruitment/job_opening/jobs' /* ,submenus: [{ name: 'Jobs', path: '/admin/recruitment/job_opening/jobs' }, { name: 'Interviews', path: '/admin/recruitment/job_opening/interviews' }] */ },
    // { name: 'Applicants', submenus: [], path: '/admin/recruitment/applicants' },
    { 
        name: 'Applicants', 
        id:'applicants_menu',
        submenus: [
            { name: 'Applicant List', path: '/admin/recruitment/applicants' }, 
            { name: 'Applicant info', path: '/admin/recruitment/applicant/0',pathstructure: "/admin/recruitment/applicant/:id",  linkOnlyHide: true,type: 1 ,id: "applicant_info_menu", },
        ] 
    },
    { 
        name: 'Training', 
        id:'training_menu',
        linkShow:true,
        submenus: [
            { 
                name: 'Group Interview', 
                id:'training_group_interview_menu',
                //path: '/admin/recruitment/group_interview',
                linkOnlyHide: false,
                subSubMenu: [
                    {name:'Manage Group Interview', path:'/admin/recruitment/group_interview/manage_group_interview',linkOnlyHide: false, id:'training_group_interview_manage_group_interview_menu'},
                    {name:'Question List', path:'/admin/recruitment/question_list/group_interview',linkOnlyHide: false,id:'training_group_interview_question_list_menu'}
                ]
            }, 
            { 
                name: 'CAB Day', 
                //path: '/admin/recruitment/manage_cab_day',
                subSubMenu: [
                    {name:'Manage CAB Day', path:'/admin/recruitment/manage_cab_day'},
                    {name:'Question List', path:'/admin/recruitment/question_list/cab_day'}
                ]
            }, 
            { 
                name: 'Devices', 
                //path: '/admin/recruitment/training/cab_day',
                subSubMenu: [
                    {name:'Devices List', path:'/admin/recruitment/device_list'},
                    {name:'Manage Devices', path:'/admin/recruitment/manage_devices_list'}
                ]
            }, 
            { 
                name: 'Ipad', 
                path: '/admin/recruitment/training/ipad' 
            }
        ] 
    },
    { 
        name: 'Recruiter Management', 
        id:'recruiter_managment_menu',
        submenus: [
            { name: 'Recruiter', path: '/admin/recruitment/user_management',linkOnlyHide: false,id:'recruiter_managment_recruiter_menu' }, 
            { name: 'Department', path: '#',linkOnlyHide: false,id:'recruiter_managment_department_menu' },
            { name: 'Round Robin Management', path: '/admin/recruitment/user_management/roundrobin_management',linkOnlyHide: false,id:'recruiter_managment_round_robin_menu'}
        ],
        linkShow:true 
    },
    { 
        name: 'Communication Logs', 
        id:'communication_logs',
        path: '/admin/recruitment/communications_logs',
        linkShow:true 
    }

];