export const orgActiveTitle = {
    inbox: "Inbox",
    draft: "Drafts",
    archive: "Archive",
    group_message: "Group Messages"
};

export const orgJson = [
    { name: 'Dashboard', submenus: [], path: '/admin/organisation/dashboard' },

    {
        name: 'Organisations',
        submenus: [],
        path: '/admin/organisation/dashboard'
    },
    {
        name: 'Sub-Orgs',
        submenus: [],
        linkOnlyHide: false,
        className: 'removeDropdown',
        id: 'sub_orgs',
        path: '#'
    },
    {
        name: 'sub-org',
        submenus: [
            {
                name: 'Org Details',
                path: '/admin/organisation/overview',
                id: 'suborg_details',
                pathstructure: '/admin/organisation/overview/:orgId/:subOrgId',
                subSubMenu: [
                    { name: 'About', path: '/admin/organisation/overview', pathstructure: '/admin/organisation/overview/:orgId/:subOrgId', type: 1 },
                    { name: 'Contacts', path: '/admin/organisation/contacts', pathstructure: '/admin/organisation/contacts/:orgId/:subOrgId', type: 1 },
                    { name: 'FMS', path: '/admin/organisation/fms', pathstructure: '/admin/organisation/fms/:orgId/:subOrgId', type: 3, moduelname: 'fms' }
                ],
                type: 1,
                closeMenus: false,
            },
            {
                name: 'Sites/Houses',
                subSubMenu: [],
                linkOnlyHide: false,
                path: '/admin/organisation/sites',
                pathstructure: '/admin/organisation/sites/:orgId/:subOrgId',
                type: 1,
            }
        ],
        linkShow: false,
        className: 'removeDropdown',
        id: 'suborg_details_menu',
        closeMenus: true,
        path: '#',
        type: 1,
    },
    {
        name: 'sub-org-house',
        submenus: [
            {
                name: 'About',
                path: '/admin/organisation/overview',
                id: 'suborg_details',
                pathstructure: '/admin/organisation/house_about/:houseid/:subOrgId',
                subSubMenu: [],
                type: 1,

            },
            {
                name: 'Billing',
                subSubMenu: [],
                linkOnlyHide: false,
                path: '/admin/organisation/sites',
                pathstructure: '/admin/organisation/contactBilling/:houseid/:subOrgId',
                type: 1,
            },
            {
                name: 'Docs',
                subSubMenu: [],
                linkOnlyHide: false,
                path: '/admin/organisation/sites',
                pathstructure: '/admin/organisation/docs/:houseid/:subOrgId',
                type: 1,
            },
            {
                name: 'FMS',
                subSubMenu: [],
                linkOnlyHide: false,
                path: '/admin/organisation/sites',
                pathstructure: '/admin/organisation/house_fms/:houseid/:subOrgId',
                type: 3,
            }
        ],
        linkShow: false,
        className: 'removeDropdown',
        id: 'suborg_house_menu',
        closeMenus: true,
        path: '#',
        type: 1,
    },
    {
        name: 'My Houses',
        submenus: [{ name: 'All Houses', path: '#' },
        { name: 'Participants', path: '#' },
        { name: 'Shifts', path: '#' },
        { name: 'Documentations', path: '#' },
        { name: 'Management', path: '#' },
        ],
        linkOnlyHide: false
    }
];

export const OrgWillAdded = [
    {
        name: 'Org Details',
        path: '/admin/organisation/overview',
        id: 'org_details',
        pathstructure: '/admin/organisation/overview/:orgId',
        subSubMenu: [
            { name: 'About', path: '/admin/organisation/overview', pathstructure: '/admin/organisation/overview/:orgId', type: 1 },
            { name: 'Contacts', path: '/admin/organisation/contacts', pathstructure: '/admin/organisation/contacts/:orgId', type: 1 },
            { name: 'FMS', path: '/admin/organisation/fms', pathstructure: '/admin/organisation/fms/:orgId', type: 3, moduelname: 'fms' }
        ],
        type: 1,
        closeMenus: false,
    },
    {
        name: 'Sub-Orgs',
        submenus: [],
        linkOnlyHide: false,
        className: 'removeDropdown',
        path: '/admin/organisation/suborg/',
        pathstructure: '/admin/organisation/suborg/:orgId',
        type: 1,
    },
    {
        name: 'Sites/Houses',
        subSubMenu: [],
        linkOnlyHide: false,
        path: '/admin/organisation/sites',
        pathstructure: '/admin/organisation/sites/:orgId',
        type: 1,
    }
]




