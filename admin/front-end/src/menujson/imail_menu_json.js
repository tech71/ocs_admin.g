export const imailActiveTitle = {
    inbox: "Inbox",
    draft: "Drafts",
    archive: "Archive",
    group_message: "Group Messages"
};

export const imailJson = [
    {
        name: 'Dashboard',
        id: 'imail_dashboard',
        submenus: [],
        className: 'removeDropdown',
        path: '/admin/imail/dashboard'
    },
    {
        name: 'External iMail',
        id: 'external_iMail',
        submenus: [
            {name: 'Inbox', path: '/admin/imail/external/inbox'},
            {name: 'Drafts', path: '/admin/imail/external/draft'},
            {name: 'Archived', path: '/admin/imail/external/archive'},
        ]
    },
    {
        name: 'Internal iMail',
        id: 'internal_iMail',
        submenus: [
            {name: 'Inbox', path: "/admin/imail/internal/inbox"},
            {name: 'Drafts', path: "/admin/imail/internal/draft"},
            {name: 'Group Messages', path: "/admin/imail/internal/group_message"},
            {name: 'Archived', path: "/admin/imail/internal/archive"},
        ],

    }
];
  