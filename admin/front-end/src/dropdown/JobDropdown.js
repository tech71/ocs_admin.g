export function jobOpeningStatus(key) {
    var interpreter = [
        {value: 0, label: 'Draft'},
        {value: 2, label: 'Closed'},
        {value: 3, label: 'Live'},
        {value: 5, label: 'Scheduled'},
    ];

    if (key) {
        var index = interpreter.findIndex(x => x.value == key)
        return interpreter[index].label;
    } else {
        return interpreter;
    }
}