import React, { Component } from 'react';

import ReactTable from "react-table";
import 'react-table/react-table.css'
import moment from 'moment-timezone';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { Link} from 'react-router-dom';
import RosterHistory from '../../admin/schedule/RosterHistory';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import { connect } from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import {customHeading } from '../../../service/CustomContentLoader.js';
import {PAGINATION_SHOW} from '../../../config';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import Pagination from "../../../service/Pagination.js";
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';
import {ROUTER_PATH} from 'config.js';


const requestData = (pageSize, page, sorted, filtered, participantId) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered});
        postData('participant/Participant_profile/get_participant_roster', {Request: Request, participantId, participantId}).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class ParticipantCurrentRoster extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
             loading: false,
            rosterListing: [],
        }
      
    }

     fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.id  
                ).then(res => {
            this.setState({
                rosterListing: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }
    
    componentDidMount() {

    }

    closeHistory = () => {
        this.setState({open_history: false})
    }
    
    render() {
         const columns = [
            {Header: 'ID', accessor: 'id', filterable: false},
            {Header: 'Roster Title', accessor: 'title', filterable: false },
            {Header: 'Start Date', Cell: (props) => <span>{props.original.start_date!=null && props.original.start_date!='' ? moment(props.original.start_date).format('DD/MM/YYYY'):''}</span>
            },
            { Header: 'End Date', accessor: 'end_date', filterable: false,
            Cell: (props) => <span>{props.original.end_date!=null && props.original.end_date!='' ? moment(props.original.end_date).format('DD/MM/YYYY'):'N/A'}</span>
            },
          
            {Cell: (props) => <span className="booking_L_T1"><i title={ParticiapntPageIconTitle.par_history_icon} onClick={() => this.setState({historyId : props.original.id, open_history : true})}  className="icon icon-pending-icons icon_h-1 mr-2"></i>
                    <Link title={ParticiapntPageIconTitle.par_view_icon} to={ROUTER_PATH+'admin/schedule/roster_details/'+ props.original.id}><i className="icon icon-views"></i></Link>
                        </span>, Header: <div className="">Total Here: {this.state.rosterListing.length}</div>, headerStyle: {border:"0px solid #fff" }, sortable: false}, 
        ]
        return (
                <div>

<ParticipantProfile id={this.props.props.match.params.id} pageTypeParms="current_roster"/>
          

     

        <div className="row">
            <ParticipantNavigation activeClass={'shiftRoster'} active="current_roster"  />
            <div className="col-lg-12">
               
                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane active" id="Barry_details">
                        <div className="row">
                            <div className="col-lg-12 col-md-12">
                                <div className="row">
                                <div className="col-md-12"><div className="bor_T"></div></div>
                                    <div className="col-md-12 P_7_TB"><h3>Roster Request for '{this.props.firstname}'</h3></div>
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="row">
                        <div className="col-md-12 schedule_listings p_left_table mac_line_e_ ">
                            <ReactTable
                             PaginationComponent={Pagination}
                            columns={columns}
                            manual 
                            data={this.state.rosterListing}
                            pages={this.state.pages}
                            loading={this.state.loading} 
                            onFetchData={this.fetchData} 
                            defaultFiltered ={{status: '1'}}
                            defaultPageSize={10}
                            className="-striped -highlight"  
                            noDataText="No Record Found"
                            minRows={2}

                            previousText={<span className="icon icon-arrow-left privious"></span>}
                            nextText={<span className="icon icon-arrow-right next"></span>}
                            showPagination={this.state.rosterListing.length >= PAGINATION_SHOW ? true : false }
                                    />
                        </div>        
                    </div>
                    </div>

                    <RosterHistory open_history={this.state.open_history} rosterId={this.state.historyId} closeHistory={this.closeHistory} />

                </div>
            </div>
        </div>
    
                </div>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName
})

export default connect(mapStateToProps)(ParticipantCurrentRoster)
