import React, { Component } from 'react';

import { checkItsNotLoggedIn } from '../../../service/common.js';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';


class ParticipantFinancialOverview extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {

        }

    }

    componentDidMount() {

    }

    render() {
        return (
              
                <React.Fragment>
                    <ParticipantProfile id={this.props.props.match.params.id}  pageTypeParms="financial_overview"/>
                    <ParticipantNavigation active="financial_overview" activeClass={'shiftRoster'} />
                    <div className="col-lg-12">
                        
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane active" id="Barry_details">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12">
                                        <div className="row">
                                            <div className="col-md-12 P_7_TB"><h3></h3></div>
                                            <div className="mb-5 mt-2 text-center await_ln" >Awaiting Finance Module</div>
                                        </div>
                                    </div>

                                </div>

                                <table id="example_one" className="display responsive table_def member_s" width="100%">

                                </table>
                            </div>

                            <div role="tabpanel" className="tab-pane" id="Barry_availabitity">
                                <div className="mb-5 mt-2 text-center await_ln" >Awaiting Finance Module</div>
                            </div>

                        </div>
                    </div>
                </React.Fragment>
             

        );
    }
}

export default ParticipantFinancialOverview
