import React, { Component } from 'react';

import ReactTable from "react-table";
import 'react-table/react-table.css'
import { connect } from 'react-redux'
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import RosterHistory from '../../admin/schedule/RosterHistory';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customHeading } from '../../../service/CustomContentLoader.js';
import { PAGINATION_SHOW } from '../../../config';
import Pagination from "../../../service/Pagination.js";
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';
import moment from 'moment';




const requestData = (pageSize, page, sorted, filtered, participantId) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('participant/Participant_profile/get_participant_roster', { Request: Request, participantId, participantId }).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class ParticipantRosterRequest extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
            loading: false,
            rosterListing: [],
        }
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            this.props.props.match.params.id
        ).then(res => {
            this.setState({
                rosterListing: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    componentDidMount() {

    }

    closeHistory = () => {
        this.setState({ open_history: false })
    }

    render() {
        const columns = [
            //{ Header: 'Participant Name', accessor: 'participantName', filterable: false, },
            { Header: 'ID', accessor: 'id', filterable: false, },
            { Header: 'Roster Title', accessor: 'title', filterable: false, },
            {
                Header: 'Start Date', accessor: 'start_date', filterable: false,
                Cell: (props) => <span>{props.original.start_date != null && props.original.start_date != '' ? moment(props.original.start_date).format('DD/MM/YYYY') : ''}</span>
            },
            {
                Header: 'End Date', accessor: 'end_date', filterable: false,
                Cell: (props) => <span>{props.original.end_date != null && props.original.end_date != '' ? moment(props.original.end_date).format('DD/MM/YYYY') : 'N/A'}</span>
            },

            {
                Cell: (props) => <span className="booking_L_T1"><i title={ParticiapntPageIconTitle.par_history_icon} onClick={() => this.setState({ historyId: props.original.id, open_history: true })} className="icon icon-pending-icons icon_h-1 mr-2"></i>
                    <a  title={ParticiapntPageIconTitle.par_view_icon} href={'/admin/schedule/roster_details/' + props.original.id}><i className="icon icon-views"></i></a>
                </span>, Header: <div className="">Total Here: {this.state.rosterListing.length}</div>, headerStyle: { border: "0px solid #fff" }, sortable: false
            },
        ]
        return (
            <div>

                <section >
                    <div className="container-fluid Red">



                        <div className="row">
                            <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms="roster_requests" />
                            <ParticipantNavigation active="roster_request" activeClass={'shiftRoster'} />
                            <div className="col-lg-12">

                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="Barry_details">
                                        <div className="row">
                                            <div><div className="bor_T"></div></div>
                                            <div className="P_7_TB"><h3>Roster Request for '{this.props.firstname}'</h3></div>
                                            <div><div className="bor_T"></div></div>


                                        </div>

                                        <div className="row">
                                            <div className="schedule_listings p_left_table mac_line_e_ ">
                                                <ReactTable
                                                    PaginationComponent={Pagination}
                                                    columns={columns}
                                                    manual
                                                    data={this.state.rosterListing}
                                                    pages={this.state.pages}
                                                    loading={this.state.loading}
                                                    onFetchData={this.fetchData}
                                                    defaultFiltered={{ status: '2' }}
                                                    defaultPageSize={10}
                                                    className="-striped -highlight"
                                                    noDataText="No Record Found"
                                                    minRows={2}
                                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                                    showPagination={this.state.rosterListing.length >= PAGINATION_SHOW ? true : false}
                                                />
                                            </div>
                                        </div>

                                        <RosterHistory open_history={this.state.open_history} rosterId={this.state.historyId} closeHistory={this.closeHistory} />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    firstname: state.ParticipantReducer.participantProfile.firstname,
    fullName: state.ParticipantReducer.participantProfile.fullName
})

export default connect(mapStateToProps)(ParticipantRosterRequest)
