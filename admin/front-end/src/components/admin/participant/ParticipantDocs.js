import React, { Component } from 'react';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn, postData, postImageData, handleChangeSelectDatepicker,archiveALL } from '../../../service/common.js';

import { listViewDocsOption, docsServiceOption, listFiltergDocsOption } from '../../../dropdown/ParticipantDropdown.js'
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';

import Modal from 'react-bootstrap/lib/Modal';
import jQuery from "jquery";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import ReactPlaceholder from 'react-placeholder';
import { custNumberLine } from '../../../service/CustomContentLoader.js';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import { ToastUndo } from 'service/ToastUndo.js'

const errorMessage = (msg) => {
         toast.dismiss(); 
         toast.error(<ToastUndo message={msg} showType={'e'} />, {
        //  toast.error(msg, {
             position: toast.POSITION.TOP_CENTER,
             hideProgressBar: true
        });
    }
    
class ParticipantDocs extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
        
        this.state = {
            doc_type: props.props.match.params.page,
            docs: [],
            view_by: 'newest_first',
            filter_by: 'current',
            participantId: this.props.props.match.params.id
        }
    }
    
    getParticipantDocs = () => {
         this.setState({loading: true});
         var request = {id:this.props.props.match.params.id, type: this.state.doc_type, sort: this.state.view_by, filter: this.state.filter_by}
         postData('participant/Participant_profile/get_participant_docs', request).then((result) => {
            if (result.status) {
                var details = result.data
                
                this.setState({docs: details});         
            } 
            this.setState({loading: false});
        });
    }
    
    selectAll =(status) => {
        var activeDownload = this.state.docs;
  
        if(status){
             activeDownload.map((val, id) => {
                activeDownload[id]['is_active'] = false;
            })
            this.setState({docs:activeDownload, selectAll: false}); 
        }
        else{
             activeDownload.map((val , id) => {
                activeDownload[id]['is_active'] = true;
            })
            this.setState({docs:activeDownload, selectAll: true}); 
        }
    }
    
    clickToDownloadFile=(key)=> {
        var activeDownload = this.state.docs;
        if(activeDownload[key]['is_active'] == true){
            activeDownload[key]['is_active'] = false;
        }
        else{ 
            activeDownload[key]['is_active'] = true;
        }
        this.setState({docs:activeDownload, selectAll: false}); 
    }
    
    cloaseModel= (mode) => {
        this.setState({modal_show: false})
        if(mode){
             this.getParticipantDocs();
        }
    }

    componentDidMount= () => {
       this.getParticipantDocs();
    }
    
    changePanel = (type) => {
        this.setState({doc_type: type, view_by: 'newest_first', selectAll: false}, () => {
            this.getParticipantDocs();
        })
    }
    
    orderChange = (e, key)=> {
        var state = {};
        state[key] = e;
        this.setState(state, () => {
            this.getParticipantDocs();
        });
        
    }
    
    selectArchived = () => {
           var status = this.selectionValidate();
           if(status){
            let url= 'participant/Participant_profile/archive_participant_docs';
            let data = {participantId:this.state.participantId, docs: this.state.docs};
            archiveALL(data, '',  url).then((result) => {
                //postData('participant/Participant_profile/archive_participant_docs', {participantId:this.state.participantId, docs: this.state.docs}).then((result) => {
                    if (result.status) {
                        this.getParticipantDocs()      
                    } 
                    this.setState({loading: false});
                });
            }else{
                errorMessage('Please select at least one document for archive');
         }
    }
    
    selectionValidate = () => {
         var activeDownload = this.state.docs;
         var status = false
               activeDownload.map((val, id) => {
                   if(activeDownload[id]['is_active'] == true)
                    status = true   
               })
             return status;  
    }
    
    downloadSelectedFile = () => {
        var status = this.selectionValidate();
        if(status){
            var requestData = {participantId: this.state.participantId, downloadData: this.state.docs};
            postData('participant/Participant_profile/download_selected_file', requestData).then((result) => {
                if (result.status) {
                    window.location.href = BASE_URL + "archieve/"+result.zip_name;    
                    
                    var removeDownload = this.state.docs;
                    removeDownload.map((value, idx) => {
                        removeDownload[idx]['is_active'] = false;
                    })
                    this.setState({docs : removeDownload});
                } else {
                    errorMessage(result.error);
                }
            });   
        }else{
           errorMessage('Please select at least one document for download.')
        }
    }

    componentWillReceiveProps(nextProps){
        let docsType=['service_docs','SIL_docs'];
        if(this.props.showTypePage!=nextProps.showTypePage &&  docsType.indexOf(nextProps.showTypePage) > -1){
            this.changePanel(nextProps.showTypePage);
        }
    }
    
    
    
    render() {
        return (
                <React.Fragment>
        
<ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page}/>
        <div className="row">
         
            <ParticipantNavigation  active="docs" activeClass={'funding'} />
            <div className="col-lg-12">
           
                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane active" id={this.state.doc_type}>
                        <div className="row">
                            <div className="col-lg-12 col-md-12 mb-3">
                                <div className="row">
                                    <div className="col-md-12"><h3 className="by-1 P_7_TB">01-003 Docs:</h3></div>
                                </div>
                            </div>
                        </div>

                        <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(4)} ready={!this.state.loading}>
                        <div className="row d-flex set_alignment">
                           
                            <div className="col-lg-9 col-md-9">
                                  {(this.state.docs.length > 0)?
                                  <ul className="file_down quali_width P_15_TB">
                                   { this.state.docs.map((doc, id) => (
                                    <li key={id} onClick={ () => this.clickToDownloadFile(id)}  className={(doc.is_active)?'active':''}>
                                         <div className="path_file">{doc.title}</div>
                                        <span className="icon icon-file-icons d-block"></span>
                                         <div className="path_file">{doc.filename}</div>
                                    </li> 
                                    ))}</ul>  :<div className="P_15_T"><div className="no_record no_record1" >No record found.</div></div> }
                            </div>
                             

                            <div className="col-lg-3 col-md-3">
                                <div className="box P_15_T w-100 pb-5">
                                     <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state['view_by'] || ''} onChange={(e) => this.orderChange(e, 'view_by')}  options={listViewDocsOption()} placeholder="View by" />
                                </div>
                                <div><Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state['filter_by'] || ''} onChange={(e) => this.orderChange(e, 'filter_by')}  options={listFiltergDocsOption()} placeholder="Filter" /></div>
                                <div className="w-100 mt-5">
                                <div className="right_align_C P_15_TB pt-5">
                                    <span>
                                        <input onChange={() => this.selectAll(this.state.selectAll)} type="checkbox" className="checkbox2" id="a3" name="" checked={this.state.selectAll || ''} value={this.state.selectAll || ''} />
                                        <label  htmlFor="a3"><span></span>Select All</label>
                                    </span>
                                </div>
                                <span>
                                    <label className="btn btn-default btn-sm center-block btn-file">
                                        <a onClick={() => this.setState({modal_show: true})} className="but" aria-hidden="true">Upload New Doc(s)</a>
                                    </label>
                                    <label className="btn btn-default btn-sm center-block btn-file mt-3">
                                        <a onClick={() => this.downloadSelectedFile()} className="but" aria-hidden="true">Download Selected</a>
                                    </label>
                                    <p className="mt-3"><a onClick={this.selectArchived} className="but">Archive Selected</a></p>
                                </span>
                                </div>

                            </div>
                        </div>
                        </ReactPlaceholder>
                    </div>

                    <div role="tabpanel" className="tab-pane" id="Barry_preferences">
                    </div>

                    <ParticipantUploadDocs doc_type={this.state.doc_type} modal_show={this.state.modal_show}   cloaseModel={this.cloaseModel} firstName={this.props.firstName} participantId={this.state.participantId} />
                </div>


            </div>

        </div>
        </React.Fragment>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName,
     showTypePage:state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantDocs)


class ParticipantUploadDocs extends React.Component {
    constructor(props) {
        super(props); 
        this.uploadDocsInitialState = { 
            selectedFile: null,  
            docsCategory: null,  
            expiry_date: null,  
        }
        this.state =  this.uploadDocsInitialState;

    }
    
    fileChangedHandler = (event) => {
         this.setState({selectedFile: event.target.files[0], filename: event.target.files[0].name})
    }

    uploadHandler = (e) => {
        e.preventDefault();
        var custom_validate = this.custom_validation({  errorClass: 'tooltip-default'});
        jQuery("#special_agreement_form").validate({ /* */});
   
        if (jQuery("#special_agreement_form").valid() && this.custom_validation()){
            this.setState({loading: true})
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('participantId', this.props.participantId)
            formData.append('doc_type', this.props.doc_type)
            formData.append('docsTitle', this.state.docsTitle)
            formData.append('expiry', this.state.expiry_date.format('YYYY-MM-DD'))
            formData.append('docsCategory', this.state.docsCategory)
            
            postImageData('participant/Participant_profile/upload_participant_docs', formData).then((responseData) => {
                var x = responseData;

                if(responseData.status){
                    this.props.cloaseModel(true);
                    this.setState( this.uploadDocsInitialState);
                   
                    var msg = <span>uploaded successfully. <br/>{(responseData.warn)? responseData.warn: ''}</span>
                    toast.success(<ToastUndo message={msg} showType={'s'} />, {
                    // toast.success(msg, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    }); 
                    this.setState({loading: false})
                }else{
                    toast.error(<ToastUndo message={x.error} showType={'e'} />, {
                    
                    //  toast.error(x.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                     this.setState({loading: false})
                }}).catch((error) => {
                    errorMessage("Api Error");
                     this.setState({loading: false})
                });
        }
    }

    custom_validation = () => {
        var return_var = true;
        var state = {};
        var List = [{key:'docsCategory'}];
        List.map((object, sidx) => {
            
                if(this.state[object.key] == null || this.state[object.key] == ''){
                    // alert(object.key)
                     state[object.key+'_error'] = true;
                     this.setState(state);
                     return_var = false;
                }
        });
        return return_var;
    }
    
    componentWillReceiveProps(newProps) {
       if(newProps.modal_show){
            this.setState({docsTitle: '', filename: ''})
       }
    }

    selectChange(selectedOption, fieldname) {
            var state = {};
            state[fieldname] = selectedOption; 
            state[fieldname+'_error'] = false;       
            this.setState(state);        
    } 

    callHtmlForValidation = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }

    render() {
        return (
        <div>
            <Modal className="modal fade-scale Modal_A  Modal_B" show={this.props.modal_show} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
               <form id="special_agreement_form" method="post" autoComplete="off">
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left Popup_h_er_1">Upload New Doc(s):
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.props.cloaseModel(false)}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div className="row P_15_T">
                            <div className="col-md-12 mb-5 mt-5">
                            <label>Title</label>
                            <span className="required">
                                 <input type="text" onChange={(e)=>this.setState({'docsTitle':e.target.value})} value={(this.state.docsTitle)?this.state.docsTitle:''} data-rule-required="true"/>
                             </span>
                            </div>
                            <div className="col-md-6">
                                        <label>Docs Category</label>
                                        <span className="required">
                                            <Select className='custom_select' name="docsCategory"  simpleValue={true} searchable={false} clearable={false} value={this.state.docsCategory} onChange={(e) => handleChangeSelectDatepicker(this, e, 'docsCategory')} options={docsServiceOption(this.props.doc_type)}  />
                                            {this.callHtmlForValidation('docsCategory')}
                                        </span>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="row">
                                            <div className="col-md-12"><label>Docs Expiry Date</label></div>
                                            <div className="col-md-12">
                                                <span className="required">
                                                    <DatePicker autoComplete={'off'} className="text-center" selected={this.state.expiry_date} name="expiry_date" onChange={(e) => this.selectChange(e, 'expiry_date')} minDate={moment()} required dateFormat="DD-MM-YYYY" />
                                                </span>

                                            </div>
                                        </div>
                                    </div>
                        </div>
                         
                   
                        <div className="row P_15_T my-5">
                            <div className="col-md-12 text-center">
                            <label>Please select a file to upload</label>
                            </div>
                            <div className="col-md-6 col-md-offset-3">
                             <span className="required upload_btn">
                                 <label className="btn btn-default btn-sm center-block btn-file">
				    <i className="but" aria-hidden="true">Upload New Doc(s)</i>
				     <input className="p-hidden" type="file" name="special_agreement_file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension= "jpg|jpeg|png|xlx|xls|doc|docx|pdf"  />
		              </label>
                              </span>
                              {(this.state.filename)? <p className="mt-2">File Name: <small>{this.state.filename}</small></p>: ''}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-7"></div>
                            <div className="col-md-7"><p className="text-right">{this.state.loading? 'Please wait...': ''}</p></div>
                            <div className="col-md-5">
                                <input type="submit" className="but" value={'Save'} disabled={this.state.loading} name="content" onClick={(e)=>this.uploadHandler(e)}/>
                            </div>
                        </div>
                    </div>  
                </Modal.Body>
               </form>
            </Modal>
        </div>
        );
    }
}
