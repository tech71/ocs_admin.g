import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal';
import 'react-toastify/dist/ReactToastify.css';
import { checkItsNotLoggedIn, postData, archiveALL } from '../../../service/common.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import Panel from 'react-bootstrap/lib/Panel';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import { customHeading, customNavigation, custNumberLine } from '../../../service/CustomContentLoader.js';
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';
import { connect } from 'react-redux'

// main parant class and defualt this class exported
class ParticipantCaresNotes extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
            care_notes: [],
            strategies: [],
            view_by: 'current'
        }
    }

    getDataStrategiesCareNotes = (type) => {
        this.setState({ loading: true }, () => {
            postData('participant/Participant_profile/get_participant_strategies_care_notes', { id: this.props.props.match.params.id, type: type }).then((result) => {
                if (result.status) {
                    var details = result.data
                    this.setState({ care_notes: details.care_notes });
                    this.setState({ strategies: details.strategies, loading: false });
                }
            });
        });
    }

    componentDidMount() {
        this.getDataStrategiesCareNotes('current');
    }

    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
        this.getDataStrategiesCareNotes(selectedOption);
    }

    render() {
        //console.log('this.props.props.match.params.page',this.props.props.match.params.page);
        return (
            <React.Fragment>
                <ParticipantProfile id={this.props.props.match.params.id}  pageTypeParms={this.props.props.match.params.page}/>

                <div className="row">
                    <ReactPlaceholder showLoadingAnimation={true} defaultValue={''} customPlaceholder={customNavigation()} ready={!this.props.loading}>
                        <ParticipantNavigation active="care_notes" activeClass={'about'} />
                    </ReactPlaceholder>
                    <div className="col-lg-12">
                     

                        <div className="tab-content">

                            <ParticipantCareStratigies uppLoading={this.props.loading} showTypePage={this.props.showTypePage} loading={this.state.loading} fullName={this.props.fullName} strategies={this.state.strategies} participantID={this.props.props.match.params.id} getDataStrategiesCareNotes={this.getDataStrategiesCareNotes} view_by={this.state.view_by} selectChange={this.selectChange} firstname={this.props.firstname} />
                            <ParticipantCareNotes loading={this.state.loading} showTypePage={this.props.showTypePage} fullName={this.props.fullName} care_notes={this.state.care_notes} participantID={this.props.props.match.params.id} getDataStrategiesCareNotes={this.getDataStrategiesCareNotes} view_by={this.state.view_by} selectChange={this.selectChange} firstname={this.props.firstname} />

                        </div>
                    </div>

                </div>
                </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    firstname: state.ParticipantReducer.participantProfile.firstname,
    fullName: state.ParticipantReducer.participantProfile.fullName,
    showTypePage:state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantCaresNotes)


class ParticipantCareStratigies extends Component {
    constructor(props) {
        super(props);

        this.viewByOption = [{ value: 'current', label: 'Current' }, { value: 'archive', label: 'Archive' }]

        this.state = {
            updataData: false,
            strategies: [],
            loading: true,
            success: false,
            view_by: 'current'
        }

        this.closeCreate = this.closeCreate.bind(this);
        this.archiveAddress = this.archiveAddress.bind(this);
        this.updateData = this.updateData.bind(this);
    }

    archiveAddress(index, id) {
         let url = 'participant/Participant_profile/archive_participant_about_care';
        archiveALL({id: id, participantId: this.props.participantID}, '', url).then((result) => {
            if (result.status) {
                var state = {}
                state['strategies'] = this.state.strategies.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }

    updateData(data) {
        this.setState({ updataData: data }, () => {
            this.setState({ create: true });
        });
    }

    createData = () => {
        this.setState({ updataData: false }, () => {
            this.setState({ create: true })
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    closeCreate = (response) => {
        if (response) {
            this.props.getDataStrategiesCareNotes(this.props.view_by);
        }
        this.setState({ create: false });
    }

    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage=='strategies' ? "tab-pane active": 'tab-pane'} id="Barry_preferences">
                <div className="row mt-3">
                    <div className="col-lg-9 col-sm-8">
                        <div className="row">
                        <div className="col-md-12"><div className="bor_T"></div></div>
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20)} ready={!this.props.uppLoading}>
                                <div className="col-md-12 P_7_TB"><h3>{/* this.props.firstname */} Care Strategies</h3></div>
                            </ReactPlaceholder>
                            <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-4">
                        <div className="box">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(80)} ready={!this.props.uppLoading}>
                                <Select name="view_by" simpleValue={true} searchable={false} clearable={false}
                                    value={this.props.view_by} onChange={(e) => this.props.selectChange(e, 'view_by')}
                                    options={this.viewByOption} placeholder="View By:" />
                            </ReactPlaceholder>
                        </div>
                    </div>
                </div>
                <div className="row P_15_TB">
                    <div className="col-md-12">
                        <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">

                            <ReactPlaceholder type='textRow' customPlaceholder={custNumberLine(4)} ready={!this.props.loading}>
                                {(this.state.strategies.length > 0) ?
                                    this.state.strategies.map((object, index) => (
                                        <Panel key={index + 1} eventKey={index + 1}>
                                            <Panel.Heading>
                                                <Panel.Title toggle>
                                                    <span role="button" data-toggle="collapse" data-parent="#accordion" href={'#demo_' + index} aria-expanded="true" aria-controls={'demo_' + index}>
                                                        <i className="more-less glyphicon glyphicon-plus"></i>
                                                        <div className="resident_name py-0">
                                                            <div className="row">
                                                                <div className="col-xs-4 py-3">
                                                                    <small className="color">{object.created}</small>
                                                                    <span className="color pull-right">{object.title}:</span></div>
                                                                <div className="col-xs-8 collapsible collapsed_me  py-3 bl_1_gray P_30_R">
                                                                    {object.content}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body collapsible className="px-1 py-0">
                                                <div className="panel-body py-0 px-0">
                                                    <div className="row">
                                                        <div className="col-md-12">

                                                            <div className="col-xs-4"></div>
                                                            {(this.props.view_by == 'current') ?
                                                                <div className="col-xs-8 bl_1_gray text-right">
                                                                    <a title={ParticiapntPageIconTitle.par_update_icon}><i onClick={() => this.updateData({ title: object.title, content: object.content, notesID: object.id })} className="icon icon-update update_button"></i></a>
                                                                    <a title={ParticiapntPageIconTitle.par_archive_icon} className="ml-2"><i onClick={() => this.archiveAddress(index, object.id)} className="icon icon-email-pending archive_button"></i></a>
                                                                </div> : ''}
                                                        </div>
                                                    </div>
                                                </div>
                                            </Panel.Body>
                                        </Panel>
                                    )) : <div className="no_record py-2">No record available</div>}
                            </ReactPlaceholder>

                            <ParticipantCretesNotes fullName={this.props.fullName} firstname={this.props.firstname} modal_show={this.state.create} categories={'strategies'} closeModal={this.closeCreate} participantID={this.props.participantID} updataData={this.state.updataData} />

                            <div className="mt-3 pull-right">
                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(80)} ready={!this.props.uppLoading}>
                                    {(this.props.view_by == 'current') ? <a className="button_plus__" onClick={this.createData}><span className="icon icon-add-icons Add-2-1"></span></a> : ''}
                                </ReactPlaceholder></div>

                        </div>
                    </div>

                </div>
            </div>

        )
    }

}

class ParticipantCareNotes extends Component {
    constructor(props) {
        super(props);

        this.viewByOption = [{ value: 'current', label: 'Current' }, { value: 'archive', label: 'Archive' }]

        this.initialState = {
            updataData: false,
            care_notes: [],
            loading: true,
            success: false,
            create: false,
        }

        this.state = this.initialState;
        this.selectChangeViewBy = this.selectChangeViewBy.bind(this);
        this.closeCreate = this.closeCreate.bind(this);
        this.archiveAddress = this.archiveAddress.bind(this);
        this.updateData = this.updateData.bind(this);
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    selectChangeViewBy(selected, tagType) {
        this.setState({ view_by: selected });
    }

    archiveAddress(index, id) {
        let url = 'participant/Participant_profile/archive_participant_about_care';
        archiveALL({id: id, participantId: this.props.participantID}, '', url).then((result) => {
            if (result.status) {
                var state = {}
                state['care_notes'] = this.state.care_notes.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }

    closeCreate(response) {
        if (response) {
            this.props.getDataStrategiesCareNotes(this.props.view_by);
        }
        this.setState({ create: false });
    }

    createData = () => {
        this.setState({ updataData: false }, () => {
            this.setState({ create: true })
        });
    }


    updateData(data) {
        this.setState({ updataData: data }, () => {
            this.setState({ create: true });
        });
    }

    render() {
        return (
            <div role="tabpanel" className={this.props.showTypePage=='notes' ? "tab-pane active": 'tab-pane'} id="notes">
                <div className="row mt-3">
                    <div className="col-lg-9 col-sm-8">
                        <div className="row">
                        <div className="col-md-12"><div className="bor_T"></div></div>
                            <div className="col-md-12 P_7_TB"><h3>{/* this.props.firstname */} Notes</h3></div>
                            <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-4">
                        <div className="box">
                            <Select name="view_by" simpleValue={true} searchable={false} clearable={false}
                                value={this.props.view_by} onChange={(e) => this.props.selectChange(e, 'view_by')}
                                options={this.viewByOption} placeholder="View By:" />
                        </div>
                    </div>
                </div>
                <div className="row P_15_TB">
                    <div className="col-md-12">
                        <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">
                            <ReactPlaceholder type='textRow' customPlaceholder={custNumberLine(4)} ready={!this.props.loading}>

                                {(this.state.care_notes.length > 0) ?
                                    this.state.care_notes.map((object, index) => (
                                        <Panel key={index + 1} eventKey={index + 1}>
                                            <Panel.Heading>
                                                <Panel.Title toggle>
                                                    <span role="button" data-toggle="collapse" data-parent="#accordion" href={'#demos_' + index} aria-expanded="true" aria-controls={'demos_' + index}>
                                                        <i className="more-less glyphicon glyphicon-plus"></i>
                                                        <div className="resident_name py-0">
                                                            <div className="row">
                                                                <div className="col-xs-4 py-3">
                                                                    <span className="color">{object.created}</span>
                                                                    <span className="color pull-right">{object.title}:</span></div>
                                                                <div className="col-xs-8 collapsible collapsed_me  py-3 bl_1_gray P_30_R">
                                                                    {object.content}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body collapsible className="px-1 py-0">
                                                <div className="panel-body py-0 px-3px">
                                                    <div className="row">
                                                        <div className="col-md-12 px-1">

                                                            <div className="col-xs-4"></div>
                                                            {(this.props.view_by == 'current') ?
                                                                <div className="col-xs-8 bl_1_gray text-right">
                                                                    <a  title={ParticiapntPageIconTitle.par_update_icon}><i onClick={() => this.updateData({ title: object.title, content: object.content, notesID: object.id })} className="icon icon-update update_button"></i></a>
                                                                    <a  title={ParticiapntPageIconTitle.par_archive_icon} className="ml-2"><i onClick={() => this.archiveAddress(index, object.id)} className="icon icon-email-pending archive_button"></i></a>
                                                                </div> : ''}

                                                        </div>
                                                    </div>
                                                </div>
                                            </Panel.Body>
                                        </Panel>
                                    )) : <div className="no_record py-2">No record available</div>}
                            </ReactPlaceholder>

                            <div onClick={this.createData} className=" mt-3 pull-right">
                                {(this.props.view_by == 'current') ? <a className="button_plus__"><span className="icon icon-add-icons Add-2-1"></span></a> : ''}</div>
                            <ParticipantCretesNotes fullName={this.props.fullName} firstname={this.props.firstname} modal_show={this.state.create} categories={'care_notes'} closeModal={this.closeCreate} participantID={this.props.participantID} updataData={this.state.updataData} />
                        </div>
                    </div>

                </div>
            </div>

        )
    }

}

class ParticipantCretesNotes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notesID: false,
            loading: false,
            success: false,
        }

    }

    onSubmit(e) {
        e.preventDefault();
        jQuery('#create_notes').validate({
            rules: {
                content: "required",
                title: "required",
            }
        });
        if (jQuery('#create_notes').valid()) {
            this.setState({ loading: true }, () => {
                var data = { notesID: this.state.notesID, title: this.state.title, content: this.state.content, categories: this.props.categories, participantID: this.props.participantID, fullName: this.props.fullName };

                postData('participant/Participant_profile/create_cares_notes', data).then((result) => {
                    if (result.status) {
                        this.props.closeModal(true);
                        this.setState({ title: '', content: '' });
                    }
                    this.setState({ loading: false })
                });
            })
        }
    }

    onChange = (e) => {
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    closeModal() {
        this.props.closeModal(false);
    }

    componentWillReceiveProps() {
        if (this.props.updataData) {
            var data = this.props.updataData;
            this.setState({ title: data.title, content: data.content, notesID: data.notesID });
        } else {
            this.setState({ title: '', content: '', notesID: false });
        }
    }
    render() {
        return (
            <div>
                <Modal
                    className="modal fade Modal_A Modal_B "
                    show={this.props.modal_show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <form onSubmit={this.onSubmit.bind(this)} id="create_notes">

                            <div className="text text-left Popup_h_er_1">
                                        <span>{this.props.updataData ? 'Update' : 'Add New'} Care {((this.props.categories == 'care_notes') ? 'Notes' : 'Strategies')} For '{this.props.firstname}'</span>
                                        <a type="button" className="close_i"><i onClick={this.closeModal.bind(this)} className="icon icon-cross-icons"></i></a>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <label>Title:</label>
                                    <span className="required">
                                        <input type="text" name="title" onChange={this.onChange} value={this.state['title'] || ''} data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-md-5">
                                </div>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-12">
                                    <label>{((this.props.categories == 'care_notes') ? 'Notes' : 'Care Strategy')}:</label>
                                    <span className="required">
                                        <textarea className="col-md-12 min-h-120 textarea-max-size" onChange={this.onChange} name="content" value={this.state['content'] || ''} data-rule-required="true" data-rule-maxlength="500"  ></textarea>
                                    </span>

                                </div>

                            </div>

                            <div className="row P_15_T">
                                <div className="col-md-7"></div>
                                <div className="col-md-5">
                                    <input disabled={this.state.loading} type="submit" className="but" value={'Save Care ' + ((this.props.categories == 'care_notes') ? 'Notes' : 'Strategies')} name="content" />
                                </div>
                            </div>

                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}