import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { ROUTER_PATH } from '../../../config.js';

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import { getQueryStringValue, checkItsNotLoggedIn } from '../../../service/common.js';
import { listParticipantDropdown } from '../../../dropdown/ParticipantDropdown.js';


class ParticipantSearchForm extends React.Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);

        this.state = {
            loading: false,
            searching: false,
            url: '',
            inactive: false
        };
        this.search_option = listParticipantDropdown();
    }

    searchData(e) {
        e.preventDefault();
        this.setState({url: '/admin/participant/list'});
        this.setState({searching: true});
        
        if(this.props.search_function){
            var search = (this.state.search == 'undefined')? '': this.state.search;
            
            this.props.search_function(search,this.state.inactive );
        }
    }

    componentDidMount() {
        var search = getQueryStringValue('search');
        var inactive = getQueryStringValue('inactive');
        var search_by = getQueryStringValue('search_by');
        search = (search == 'undefined')? '': search;
        
        
        this.setState({search: search, inactive: inactive, search_by: search_by});
    }

    render() {

        return (
                <div>
                    {(this.state.searching) ? <Redirect to={{pathname: this.state.url, search: '?search=' + this.state.search + '&inactive=' + this.state.inactive, state: {inactive: this.state.inactive, search: this.state.search }}}  /> : ''}
                    <div className="row">
                        <div className="col-lg-9 col-md-9 P_25_TB">
                            <h1 className="my-0 color">Participants</h1>
                        </div>
                        <div className="col-lg-3 col-md-3 P_25_TB">
                        <Link className="Plus_button" to={ROUTER_PATH+'admin/participant/create'} ><i className="icon icon-add-icons create_add_but"></i><span>Create New Participant</span></Link>
                        </div>
                        <div className="col-lg-12"><div className="bor_T"></div></div>
                    </div>
                
                    <div>
                        <form onSubmit={this.searchData.bind(this)}>
                        <div className="row _Common_Search_a">
                            <div className="col-lg-7 col-xs-7">
                            <div className="search_bar">
                                <div className="input_search">
                                    <input type="text" className="form-control" name="search" value={this.state['search'] || ''} onChange={(e) => this.setState({'search': e.target.value})} placeholder="Search" /> 
                                    <button type="submit"><span className="icon icon-search"></span></button> 
                                </div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-xs-3">
                                <div className="row">
                                    <span className="col-lg-6 include_box">
                                        <input type="checkbox" name="inactive" className="checkbox_big" id="c1" checked={this.state['incomplete']} value={this.state.inactive || ''} onChange={(e) => this.setState({inactive: e.target.checked})} />
                                        <label htmlFor="c1"><span></span><small className="pl-1">Incomplete Only</small> </label>
                                    </span>
                                </div>
                            </div>
                            <div className="col-lg-3 col-xs-3">
                                <div className="box">
                                    <Select className="wide" required={true} simpleValue={true} searchable={false}  options={this.search_option} value={this.state['search_by'] || ''} placeholder="Filter by type" onChange={(e) => this.setState({search_by:e})} />
                                </div>
                            </div>
                            </div>
                            <div className="row">
                            <div className="col-lg-12"><div className="bor_T"></div></div>
                            </div>
                        </form>
                    </div>
                </div>
                                );
                    }
                }
                export default ParticipantSearchForm;

