import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, getPermission, postData } from '../../../service/common.js';
import PageNotFound from '../../admin/PageNotFound';


import ParticipantDashboard from '../../admin/participant/ParticipantDashboard';
import CreateParticipant from '../../admin/participant/CreateParticipant';
import CreateParticipantStepSecond from '../../admin/participant/CreateParticipantStepSecond';
import ListParticipant from '../../admin/participant/ListParticipant';

import ParticipantProfile from '../../admin/participant/ParticipantProfile';

import ParticipantAbout from '../../admin/participant/ParticipantAbout';
import ParticipantHealth from '../../admin/participant/ParticipantHealth';
import ParticipantSites from '../../admin/participant/ParticipantSites';
import ParticipantCaresNotes from '../../admin/participant/ParticipantCaresNotes';
import ParticipantFMS from '../../admin/participant/ParticipantFMS';
import ParticipantPlans from '../../admin/participant/ParticipantPlans';
import ParticipantGoals from '../../admin/participant/ParticipantGoals';
import ParticipantDocs from '../../admin/participant/ParticipantDocs';
import ParticipantFounds from '../../admin/participant/ParticipantFounds';
import ParticipantCurrentShift from '../../admin/participant/ParticipantCurrentShift';
import ParticipantRosterRequest from '../../admin/participant/ParticipantRosterRequest';
import ParticipantCurrentRoster from '../../admin/participant/ParticipantCurrentRoster';
import ParticipantFinancialOverview from '../../admin/participant/ParticipantFinancialOverview';
import Sidebar from '../Sidebar';
import 'react-block-ui/style.css';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';

import { connect } from 'react-redux'
import { setProfileData } from './actions/ParticipantAction';
import { participaintJson } from 'menujson/participaint_menu_json';



const idData = { id: 0 }
const permissionRediect = <Redirect to={ROUTER_PATH + 'admin/no_access'} />;
const menuJson = () => {
    let menu = participaintJson;
    return menu;
}

class AppParticipant extends React.Component {
    constructor(props) {
        super(props);

        checkItsNotLoggedIn();
        this.permission = (getPermission() == undefined) ? [] : JSON.parse(getPermission());

        this.state = {
            loading: false,
            loadState: true,
            subMenuShowStatus: false,
            menus: menuJson(),
            replaceData: { ':id': 0 }
        }

        this.props.setFooterColor('Participant_Module');
    }

    componentWillUnmount() {
        this.props.setFooterColor('');
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.subMenuShowStatus != nextProps.getSidebarMenuShow.subMenuShow || (nextProps.getSidebarMenuShow.subMenuShow == false && this.state.subMenuShowStatus == nextProps.getSidebarMenuShow.subMenuShow)) {
            this.setState({ subMenuShowStatus: nextProps.getSidebarMenuShow.subMenuShow }, () => {
                let menuState = this.state.menus;
                let obj = menuState.find(x => x.id === 'participant_name');
                let objIndex = menuState.indexOf(obj);
                if (nextProps.getSidebarMenuShow.subMenuShow) {

                    menuState[objIndex]['linkShow'] = true;
                    menuState[menuState.findIndex(x => x.id == 'participant_details')]['linkShow'] = true;
                    menuState[menuState.findIndex(x => x.id == 'participant_shifts_rosters')]['linkShow'] = true;
                    menuState[menuState.findIndex(x => x.id == 'participant_funding')]['linkShow'] = true;

                } else {
                    menuState[objIndex]['linkShow'] = false;
                    menuState[menuState.findIndex(x => x.id == 'participant_details')]['linkShow'] = false;
                    menuState[menuState.findIndex(x => x.id == 'participant_shifts_rosters')]['linkShow'] = false;
                    menuState[menuState.findIndex(x => x.id == 'participant_funding')]['linkShow'] = false;
                }
                this.setState({ menus: menuState });
            });
        }

        if (this.props.participantProfile.id != nextProps.participantProfile.id) {
            this.setState({ replaceData: { ':id': nextProps.participantProfile.id } }, () => {
                let menuState = this.state.menus;
                let obj = menuState.find(x => x.id === 'participant_name');
                let objIndex = menuState.indexOf(obj);
                if (nextProps.getSidebarMenuShow.subMenuShow) {
                    menuState[objIndex]['name'] = nextProps.participantProfile.fullName;
                }
                this.setState({ menus: menuState });
            });
        }

    }

    render() {

        return (


            <section className='asideSect__ manage_top Participant_Module'>
                <Sidebar
                    heading={'Participants'}
                    menus={this.state.menus}
                    subMenuShowStatus={this.state.subMenuShowStatus}
                    replacePropsData={this.state.replaceData}

                />
                <React.Fragment>
                    <div className="container-fluid px-0 py-0 fixed_size">
                        <div className="row justify-content-center d-flex">
                            <div className="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                <Switch>
                                    <Route exact path={ROUTER_PATH + 'admin/participant/dashboard'} render={(props) => this.permission.access_participant ? <ParticipantDashboard props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/create'} render={(props) => this.permission.access_participant ? <CreateParticipant props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/create_step_second'} render={(props) => this.permission.access_participant ? <CreateParticipantStepSecond props={props} /> : permissionRediect} />
                                    <Route exact name="testdemo" path={ROUTER_PATH + 'admin/participant/about/:id(\\d+)/:page(details|bookers_list|requirements|preferences)?'} render={(props) => this.permission.access_participant ? <ParticipantAbout props={props} /> : this.permissionRediect()} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/health/:id(\\d+)/:page(diagnosis|health_notes|misc)?'} render={(props) => this.permission.access_participant ? <ParticipantHealth props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/sites/:id(\\d+)/:page(sites)'} render={(props) => this.permission.access_participant ? <ParticipantSites props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/care_notes/:id(\\d+)/:page(strategies|notes)?'} render={(props) => this.permission.access_participant ? <ParticipantCaresNotes props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/fms/:id(\\d+)'} render={(props) => this.permission.access_participant ? <ParticipantFMS props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/plan/:id(\\d+)/:page(current_plan|plan_history)'} render={(props) => this.permission.access_participant ? <ParticipantPlans props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/goal/:id(\\d+)/:page(goal_tracker|goal_history)'} render={(props) => this.permission.access_participant ? <ParticipantGoals props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/doc/:id(\\d+)/:page(service_docs|SIL_docs)'} render={(props) => this.permission.access_participant ? <ParticipantDocs props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/funding/:id(\\d+)/:page?'} render={(props) => this.permission.access_participant ? <ParticipantFounds props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/current_shift/:id(\\d+)'} render={(props) => this.permission.access_participant ? <ParticipantCurrentShift props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/roster_request/:id(\\d+)'} render={(props) => this.permission.access_participant ? <ParticipantRosterRequest props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/current_roster/:id(\\d+)'} render={(props) => this.permission.access_participant ? <ParticipantCurrentRoster props={props} /> : permissionRediect} />
                                    <Route exact path={ROUTER_PATH + 'admin/participant/financial_overview/:id(\\d+)'} render={(props) => this.permission.access_participant ? <ParticipantFinancialOverview props={props} /> : permissionRediect} />
                                    <Route path='*' component={PageNotFound} />
                                </Switch>
                            </div>
                        </div>
                    </div>

                </React.Fragment>

            </section>
        );
    }
}

const mapStateToProps = state => ({
    participantProfile: state.ParticipantReducer.participantProfile,
    getSidebarMenuShow: state.sidebarData
})

const mapDispatchtoProps = (dispach) => {
    return {
        getParticipantProf: (result) => dispach(setProfileData(result)),
        setFooterColor: (result) => dispach(setFooterColor(result))
    }
};


const AppParticipantData = connect(mapStateToProps, mapDispatchtoProps)(AppParticipant)
export { AppParticipantData as AppParticipant };