import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { checkItsNotLoggedIn, postData, ConfirmationPopUp } from 'service/common.js';
import {  Link, Redirect  } from 'react-router-dom';
import jQuery from "jquery";
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { CustomProfileImage, customHeading } from 'service/CustomContentLoader.js';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import  "../../../service/custom_script.js";
import { connect } from 'react-redux'
import { setActiveClassProfilePage, setProfileData, setActiveSelectPage,updateParticipantProfile } from './actions/ParticipantAction'
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ROUTER_PATH } from 'config.js';
import DisablePortalExcessPopUp from './../DisablePortalExcessPopUp.js';

class ParticipantProfile extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.statusOption = [{value: 1, label: 'Active'}, {value: 0, label: 'Inactive'}];
        
        this.state = {
            ocsID: '',
            loading: false,
            success: false,
            //portal_access:'',
        }
    }
    
    getParticipantProfile = (id) => {
        if(parseInt(id) !== parseInt(this.props.participantStoreId)){
         this.setState({ loading: true});
            postData('participant/Participant_profile/get_participant_profile', { id: id }).then((result) => {
                if (result.status) {
                    var details = result.data;
                    this.props.getParticipantProf(details);
                    this.setState(details);
                } else {
                    window.location.href = '/admin/participant/dashboard';
                }
                this.setState({ loading: false });
            });
        }
    }
    
    componentDidMount() {
        this.props.setSubmenuShow(1);

          this.getParticipantProfile(this.props.id);
          this.setState(this.props.participantProfile);
          //this.setState({status: this.props.participantProfile.status == 0? 2: this.props.participantProfile.status})
          this.getPageSelect();
    }
   
    componentWillReceiveProps(newProps) {
          this.setState(newProps.participantProfile);
          //this.setState({status: newProps.participantProfile.status == 0? 2: newProps.participantProfile.status})  
    }
    getPageSelect(){
        this.props.setActivePage(this.props.pageTypeParms);
    }
    componentDidUpdate(){
          if(this.props.pageTypeParms!=undefined && this.props.showTypePage!=this.props.pageTypeParms){
              this.getPageSelect();
          }
    }
    
    disableEnableParticipantAccess = (chked_status, note) => {
        var participant_id = this.props.id;
        var requestData = {status: chked_status, participant_id: participant_id, fullName: this.state.fullName, note: note};
         
        postData('participant/Participant_profile/participant_app_access', requestData).then((result) => {
            this.setState({loading: false});
            this.setState({disableParticipantNotePopUp: false}) 
        });
    }
    

    enableAppAccess = (e) => {
        var chked_status = (e.target.checked) ? 1 : 0;       
        var txt = 'Are you sure you want to '+(chked_status == 1? 'enable' : 'disable')+' the portal access';        
        ConfirmationPopUp(txt).then((result) => {
            if(result.status){
                if(!chked_status){
                    this.setState({disableParticipantNotePopUp: true})
                }else{
                    this.disableEnableParticipantAccess(chked_status);
                }
                this.props.updateParticipantProfile({portal_access:chked_status});
            }
            this.setState({loading: false});
        });
    }
    
    statusChange = (selectedOption, fieldname) => {
        var participant_id = this.props.id;
        var state = {};
        state[fieldname] = selectedOption;
        
        var requestData = {status: selectedOption, participant_id: participant_id,  fullName: this.state.fullName};
        this.setState(state);
        postData('participant/Participant_profile/change_active_deactive', requestData).then((result) => {
            this.setState({loading: false});
            this.props.updateParticipantProfile({status:selectedOption});
        });
    }

    render() {
        return (
            <React.Fragment>
                    {(this.state.is_redirect)? <Redirect to={this.state.url} />:''}
                    <div className="row  _Common_back_a">
                    <div className="col-lg-12 col-md-12"><Link className="d-inline-flex" to={ROUTER_PATH+'admin/participant/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                    </div>
                    <div className="row"><div className="col-lg-12 col-md-12"><div className="bor_T"></div></div></div>
                    <div className="row _Common_He_a">
                        <div className="col-lg-9 col-md-12">
                            <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.state.loading}>
                                <h1 className="color"> {this.props.showPageTitle}</h1>
                            </ReactPlaceholder>
                        </div>
                    </div>
                    <div className="row"><div className="col-lg-12 col-md-12"><div className="bor_T"></div></div> </div>
                
                    <div className="row P_20_TB">
                        <div className="col-lg-9 col-sm-12">
                        <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true} type='textRow'  customPlaceholder={ CustomProfileImage} ready={!this.state.loading}>
                            <table width="100%" className="user_profile">
                                <tbody>
                                    <tr>
                                        <td className="width_130">
                                             <div style={{position: 'relative', width: '100px', height: '100px',}}>
                                                <div className="user_img" style={{border: '0px'}}><img src={this.state.profile_image} /> </div>
                                                <div style={{position: 'absolute', width: '110px', height: '110px', top: '0'}}>
                                                    <CircularProgressbar percentage={this.state.profile_complete} initialAnimation={true}  strokeWidth={2.5}></CircularProgressbar>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <ul>
                                                <li> <span>Assigned To:</span> Welfare</li>
                                                <li> <span>Funding Type:</span> NDIS/Private</li>
                                                <li> <span>Job Ready:</span>  Yes</li>
                                                <li className="d-inline-block mt-4"><div className="pull-left pr-2">Status: </div><span className="pull-left box_wide">
                                                <Select name="status" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.status} onChange={(e) => this.statusChange(e, 'status')} 
                                                                options={this.statusOption} placeholder="Change Status" /></span>
                                                                <span className="pull-left">
                                                                <p className="select_all">
                                                                    <input className="input_c" type="checkbox" name="access" value="valuable" id="thing" checked={this.state.portal_access == 1 || ''} value={this.state.portal_access || ''} onChange={this.enableAppAccess} />
                                                                    <label htmlFor="thing"><span></span></label><span className="pl-2">Enable Portal Access</span></p>
                                                            </span>
                                                    </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </ReactPlaceholder>
                        </div>
                    </div>
                
                    {this.state.disableParticipantNotePopUp? <DisablePortalExcessPopUp disableEnableParticipantAccess={this.disableEnableParticipantAccess} showModal={this.state.disableParticipantNotePopUp} />: ''}
                  
                    </React.Fragment>
                );
    }
}

const mapStateToProps = state => ({
    ActiveClass : state.ParticipantReducer.ActiveClassProfilePage,
    participantProfile : state.ParticipantReducer.participantProfile,
    participantStoreId : state.ParticipantReducer.participantProfile.id,
    showPageTitle: state.ParticipantReducer.activePage.pageTitle,
    showTypePage: state.ParticipantReducer.activePage.pageType
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeNavigationClass: (value) => dispach(setActiveClassProfilePage(value)),
           getParticipantProf: (result) => dispach(setProfileData(result)),
           setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
           setActivePage:(result) => dispach(setActiveSelectPage(result)),
           updateParticipantProfile:(result) => dispach(updateParticipantProfile(result)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(ParticipantProfile)

