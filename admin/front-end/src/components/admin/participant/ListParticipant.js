import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import ReactTable from "react-table";
import { PAGINATION_SHOW } from '../../../config.js';

import 'react-select-plus/dist/react-select-plus.css';
import 'react-table/react-table.css'

import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import Pagination from "../../../service/Pagination.js";
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('participant/ParticipantDashboard/list_participant', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_count: result.total_count
            };
            resolve(res);
        });

    });
};


class ListParticipant extends Component {
    constructor(props) {

        checkItsNotLoggedIn();
        super(props);

        this.state = {
            participantList: [],
            counter: 0,
            loading: false,
        };
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                participantList: res.rows,
                pages: res.pages,
                loading: false,
                total_count: res.total_count
            });
        });
    }

    openLocation = (e, props) => {
        e.preventDefault();
        this.setState({ openLocation: true, selectedAddress: props })
    }

    componentDidMount() {
        //        this.setState(this.props.props.location.state);
    }

    closeModel = () => {
        this.setState({ openLocation: false });
    }

    render() {
        const { data, pages, loading } = this.state;
        const columns = [{
            Header: 'HCM-ID', accessor: 'id', filterable: false,
            headerClassName: 'Th_class_d1',
            className: (this.state.activeCol === 'HCM-ID') && this.state.resizing ? 'borderCellCls' : '_align_c__',
            Cell: props => <span>
                <Link className="inherit-color" to={"/admin/participant/about/" + props.value} >{props.value}</Link>
            </span>, maxWidth: 95
        },
        { Header: 'NDIS-No', accessor: 'ndis_num', filterable: false, maxWidth: 200,
        className: (this.state.activeCol === 'NDIS-No') && this.state.resizing ? 'borderCellCls' : '_align_c__',
        Cell: props => <span>
          {props.value}
        </span> },
        { Header: props => <span><div className='ellipsis_line__'>Name: <small>Preferred/First/Last</small></div></span>, accessor: 'FullName',  filterable: false, maxWidth: 200,
        className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : '_align_c__',
        Cell: props => <span><div className='ellipsis_line__'>{props.original.FullName}</div></span>},
        {
            Header: 'Home Address ', accessor: 'street', filterable: false,
            headerClassName: 'Th_class_d1',
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span><div>{props.value}</div>

                <a title={ParticiapntPageIconTitle.par_address_icon} href={"https://www.google.com/maps/place/" + encodeURIComponent(props.value)} target="_blank"><i className="icon icon-location1-ie Tb_icon_L1"></i></a></span>, minWidth: 200
        },
        {
            Header: 'Phone ', accessor: 'phone', filterable: false,
            headerClassName: 'Hclass_d1',
            className: (this.state.activeCol === 'name') && this.state.resizing ? 'borderCellCls' : 'Tb_class_d1',
            Cell: props => <span><div>{props.value}</div><i className="icon icon-call2-ie Tb_icon_P1"></i></span>
        },
        { Header: 'G', accessor: 'gender', filterable: false, maxWidth: 50, Cell: props => <span>{props.original.gender.charAt(0)}</span> },
        {
            accessor: 'id', filterable: false,
            Header: <TotalShowOnTable color="no" countData={this.state.total_count} />,
            Cell: props => <span>
                <Link title={ParticiapntPageIconTitle.par_details_icon} className="inherit-color" to={"/admin/participant/about/" + props.value + "/details"} ><i className="icon icon-views"></i></Link>
            </span>, maxWidth: 75
        }
        ]
        return (

            <div className="row mt-5">
         
                <div className="col-lg-12 col-md-12 listing_table  PL_site th_txt_center__">
                    <ReactTable
                        PaginationComponent={Pagination}
                        columns={columns}
                        manual="true"
                        data={this.state.participantList}
                        pages={this.state.pages}
                        loading={this.state.loading}
                        onFetchData={this.fetchData}
                        filtered={this.props.filtered}
                        defaultPageSize={10}
                        className="-striped -highlight"
                        noDataText="No Record Found"
                        onPageSizeChange={this.onPageSizeChange}
                        minRows={2}
                        previousText={<span className="icon icon-arrow-left privious"></span>}
                        nextText={<span className="icon icon-arrow-right next"></span>}
                        showPagination={this.state.participantList.length >= PAGINATION_SHOW ? true : false}
                    />
                </div>
         
            </div>

        );
    }
}
export default ListParticipant;
