import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {  BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn,  postData } from '../../../service/common.js';
import { getGoalsClassNane, getOptionViewGoals } from '../../../dropdown/ParticipantDropdown.js';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import moment from 'moment';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ReactPlaceholder from 'react-placeholder';
import {PanelGroup, Panel} from 'react-bootstrap';
import "react-placeholder/lib/reactPlaceholder.css";
import { custNumberLine,customHeading } from '../../../service/CustomContentLoader.js';
import { LineChart } from 'react-chartkick'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/lib/Modal';
import DatePicker from 'react-datepicker';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastUndo } from 'service/ToastUndo.js';

class ParticipantGoals extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
         
        this.state = {
            loading: true,
            goals: [],
            view_by: 'active',
            type: 'tracker',
            ModelshowGoalTracker:false
        }
    }
    
    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        this.getParticipantGoals(selectedOption, this.state.type);
        this.setState(state);
    }
    
    getParticipantGoals = (view_by, type) => {
        this.setState({loading: true});
        postData('participant/Participant_profile/get_participant_goals', {id:this.props.props.match.params.id, view_by: view_by, type: type}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({goals: details});        
            } 
            this.setState({loading: false});
        });
    }

    addGoalTracker=(e)=>{
        this.setState({ModelshowGoalTracker :true});
    }
    cloaseModel=()=>{
        this.setState({ModelshowGoalTracker :false});
    }
    componentDidMount() {
        this.setState({loading: true});
        this.getParticipantGoals(this.state.view_by, this.state.type);
    }
    
    render() {
        return (
         <div>
    

        <ParticipantProfile id={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page}/>
        <div className="row">
            <ParticipantNavigation  active="goals" activeClass={'funding'} />
            <div className="col-lg-12">
          
                <div className="tab-content">
                    <div role="tabpanel" className={this.props.showTypePage=='goal_tracker'? "tab-pane active":"tab-pane"} id="GoalTracker">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                
                                    <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.props.loading}>
                                        <div className="col-md-9"><h3 className=" P_7_TB by-1">'{this.props.firstname}' Goals:</h3></div>
                                    </ReactPlaceholder>
                                    <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.props.loading}>
                                    <div className="col-md-3">
                                        <Select name="view_by" simpleValue={true} searchable={false} clearable={false} 
                                        value={this.state.view_by} onChange={(e) => this.selectChange(e, 'view_by')} 
                                        options={getOptionViewGoals()} placeholder="View By:" />
                                        </div>
                                    </ReactPlaceholder>
                                   
                                   
                                </div>
                            </div>
                        </div>
                        <div className="row P_15_TB">
                            <div className="col-md-12">

                             <PanelGroup
                                accordion
                                id="accordion-controlled-example"
                                className="panel-group accordion_me house_accordi"
                                activeKey={this.state.activeKey}
                                onSelect={this.handleSelect}
                              >
                              
                                <ReactPlaceholder defaultValue={''} showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(4)} ready={!this.state.loading}>
                                {(this.state.goals.length > 0)?
                                this.state.goals.map((val, index) => (
                                    <Panel key={index + 1} eventKey={index + 1}>
                                         <Panel.Heading>
                                    <Panel.Title toggle>
                                                <span role="button" data-toggle="collapse" data-parent="#accordion" href="#demo_1" aria-expanded="true" aria-controls="demo_1">
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <div className="resident_name table_a">
                                                        <div className="pull-left">{val.title}</div>
                                                        <span className="pull-right">End Date: {moment(val.end_date).format('DD/MM/YYYY')}</span>
                                                    </div>
                                                </span>
                                            </Panel.Title>
                                       </Panel.Heading>

                                        <Panel.Body collapsible>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <ul className="our_goals">
                                                            {val.rating.rating_with_date.map((rating,r_id) => (
                                                                <li key={r_id+1} className={getGoalsClassNane(rating.rating, true)}> 
                                                                    <span className="tag d-none">{moment(rating.created).format('DD/MM/YYYY')}: "{getGoalsClassNane(rating.rating)} {rating.rating}"</span>
                                                                </li>
                                                            ))}
                                                        </ul>
                                                    </div>

                                                    {/*<div className="col-md-12 text-right">
                                                        <a href=""><i className="icon icon-invoice invoice_button"></i></a>
                                                    </div>*/}

                                                </div>
                                        </Panel.Body>
                                   </Panel>
                                    )): <div className="no_record py-2">No Record Found</div>}
                                </ReactPlaceholder>
                                    
                                <div className="col-lg-3 col-sm-4 col-lg-offset-9 col-sm-offset-8 ">                        
                                        <div onClick={this.addGoalTracker} className="mt-3 pull-right"><a className="button_plus__"><span className="icon icon-add-icons Add-2-1"></span></a></div>
                                </div>

                                </PanelGroup>
                            </div>
                        </div>

                       
                    </div>

                    <ParticipantGoalHistry firstname={this.props.firstname} id={this.props.props.match.params.id}  showTypePage={this.props.showTypePage}/>
                    <ParticipantAddGoalTracker id={this.props.props.match.params.id} ModelshowGoalTracker={this.state.ModelshowGoalTracker} cloaseModel={this.cloaseModel} getParticipantGoals={this.getParticipantGoals} view_by={this.state.view_by} type={this.state.type} />
                </div>
            </div>
        </div>
</div>
                );
    }
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName,
     showTypePage:state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantGoals)
    
class ParticipantGoalHistry extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
         
        this.state = {
            loading: true,
            goals: [],
        }

        this.graph_label = ['', '', '','', '', '','', '', '','', '', '','', '', '','', '', '','', '', '','', '', '',]
    }
    
     graphParams = (rating) => {
        let data = {labels: this.graph_label, 
            datasets: [{
                    label: 'Rating',
                    fill: false,
                    data: rating,
                    backgroundColor: '#6f4dee',
                    borderColor: '#6f4dee',
                }],
                
            };
            return data;
    }
    
     getParticipantGoals = (view_by) => {
        this.setState({loading: true});
        postData('participant/Participant_profile/get_participant_goal_history', {id:this.props.id, view_by: view_by}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({goals: details});        
            } 
            this.setState({loading: false});
        });
    }
    
    exportGoalHistry = (e)=> {
    e.preventDefault();
    postData('participant/Participant_profile/export_goal_report', {id: this.props.id}).then((result) => {
           if (result.status) {                
                    let fileurl= BASE_URL+result.parthName;
                    window.open(fileurl,'_blank');
            }
        });
  }

    componentDidMount() {
        this.setState({loading: true});
        this.getParticipantGoals(this.state.view_by);
    }
       
    render() {
        return (
                <div role="tabpanel" className={this.props.showTypePage=='goal_history'? "tab-pane active":"tab-pane"} id="GoalHistory">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="row">
                                <div className="col-md-12"><div className="bor_T"></div></div>
                                    <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.props.loading}>
                                        <div className="col-md-8 P_7_TB"><h3>'{this.props.firstname}' Goals:</h3></div>
                                    </ReactPlaceholder>
                                    <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(20)} ready={!this.props.loading}>
                                    <div className="col-md-4 P_7_TB">
                                        </div>
                                    </ReactPlaceholder>
                                   
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                        </div>
                        <div className="row P_15_TB">
                            <div className="col-md-12">

                             <PanelGroup
                                accordion
                                id="accordion-controlled-example"
                                className="panel-group accordion_me house_accordi"
                                activeKey={this.state.activeKey}
                                onSelect={this.handleSelect}
                              >
                              
                                <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ custNumberLine(4)} ready={!this.state.loading}>
                                {(this.state.goals.length > 0)?
                                this.state.goals.map((val, index) => (
                                    <Panel key={index + 1} eventKey={index + 1}>
                                         <Panel.Heading>
                                    <Panel.Title toggle>
                                                <span role="button" data-toggle="collapse" data-parent="#accordion" href="#demo_1" aria-expanded="true" aria-controls="demo_1">
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <div className="resident_name table_a">
                                                        <div className="pull-left">{val.title}</div>
                                                        <span className="pull-right">End Date: {moment(val.end_date).format('DD/MM/YYYY')}</span>
                                                    </div>
                                                </span>
                                            </Panel.Title>
                                       </Panel.Heading>

                                        <Panel.Body collapsible>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <ul className="our_goals">
                                                            <LineChart id="users-chart" width="100%" height="220px" data={val.rating.rating_data} download={true}  />
                                                        </ul>
                                                        <span className="color">Average Rating: {val.rating.average_rating} Starts</span>            
                                                    </div>

                                                    {/*<div className="col-md-12 text-right">
                                                        <a href=""><i className="icon icon-invoice invoice_button"></i></a>
                                                    </div>*/}

                                                </div>
                                        </Panel.Body>
                                   </Panel>
                                    )): <div className="no_record py-2">No Record Found</div>}
                                </ReactPlaceholder>
                                   </PanelGroup>
                            </div>
                        </div>
                        
                         <div className="row">
                            <div className="col-lg-3 col-lg-offset-9 col-md-4 col-md-offset-8">
                                <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(60)} ready={!this.state.loading}>
                                <a onClick={this.exportGoalHistry} className="but">
                                    Export Goal Report
                                </a>
                                </ReactPlaceholder>
                            </div>
                        </div>
                    </div>
                );
    }
}

class ParticipantAddGoalTracker extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
         
        this.state = {
            loading: true,
            goals: [],
            start_date:'',
            goal_disabled:false
        }        
    }
    
    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption; 
        state[fieldname+'_error'] = false;       
        this.setState(state);        
    } 
    uploadHandler = (e) => {
        e.preventDefault();
        this.setState({goal_disabled:true});
        toast.dismiss();   
        jQuery('#add_goal_tracker').validate();
        if (jQuery('#add_goal_tracker').valid()) { 
        var data= {participant_id:this.props.id,title: this.state.goal , start_date: this.state.start_date.format('YYYY-MM-DD') ,end_date: this.state.end_date.format('YYYY-MM-DD')}        
        postData('participant/Participant_profile/save_goal_tracker',data).then((result) => {            
            if (result.status) {
                   this.setState({goal_disabled:false});                    
                   this.props.cloaseModel(false);
                   this.props.getParticipantGoals(this.props.view_by,this.props.type);                   
             }else{
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    //    toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.setState({goal_disabled:false});
             }
             
         });
        }else{
            this.setState({goal_disabled:false});
        }
        
    }
    componentDidMount() {      
      console.log(this.props)
    }
    componentWillReceiveProps(newProps) {
        if(newProps.ModelshowGoalTracker){
             this.setState({goal: '', start_date: '',end_date: ''})
        }
     }
    render() {
        return (
            <Modal
            className="modal fade Modal_A Modal_B Participant_Module"
            show={this.props.ModelshowGoalTracker}
            //onHide={this.handleHide}
            container={this}
            aria-labelledby="contained-modal-title"
        >
            <Modal.Body>
                <div className="row">

                    <div className="col-md-12 ">
                        <h2 className="color text text-left Popup_h_er_1"><span>Add New Goal</span><a className="close_i" onClick={() => this.props.cloaseModel(false)} /*onClick={() => this.setState({ ModelshowGoalTracker: false })}*/ ><i className="icon icon-cross-icons"></i></a></h2>
                      <form onSubmit={this.onSubmit} id="add_goal_tracker">
                            <div className="">
                                <div className="row P_25_T d-flex flex-wrap">
                                    <div className="col-md-12 mb-3">
                                        <label>Goal:</label>
                                        <span className="required">
                                        <input type="text" data-rule-required="true" className="input_f mb-1 " value={this.state.goal || ''} 
                                        name={'goal'} 
                                        placeholder='Your Goal' 
                                        onChange={(e)=>this.setState({'goal':e.target.value})}
                                        //onChange={(e) => handleChange(this, e)} 
                                      
                                        />
                                          </span>
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <label>Start Date:</label>
                                        <span className="required">
                                        <DatePicker autoComplete={'off'} className="text-center" 
                                        selected={this.state.start_date} 
                                        name="start_date" 
                                        onChange={(e) => this.selectChange(e, 'start_date')} 
                                        minDate={moment()} 
                                        required dateFormat="DD-MM-YYYY"
                                        maxDate={(this.state.end_date) ? moment(this.state.end_date) : moment().add(60, 'days')}
                                        />     
                                        </span>                                   
                                    </div>
                                    <div className="col-md-4 mb-3">
                                        <label>End Date:</label>
                                        <span className="required">
                                        <DatePicker autoComplete={'off'} className="text-center" 
                                        selected={this.state.end_date} 
                                        name="end_date" 
                                        onChange={(e) => this.selectChange(e, 'end_date')} 
                                        minDate={moment()} 
                                        required dateFormat="DD-MM-YYYY"
                                        minDate={(this.state.start_date && this.state.start_date != '0000/00/00') ? moment(this.state.start_date) : moment()}
                                        /> 
                                        </span>
                                    </div> 
                                </div>
                                <div className="row d-flex justify-content-end">
                                    <div className="col-md-4 mt-3">
                                        <button type="submit" disabled={(this.state.goal_disabled)?true:false} className="default_but_remove button but" onClick={(e)=>this.uploadHandler(e)}>Submit</button>
                                        
                                    </div>
                                </div>

                            </div>

                        </form> 
                    </div>
                </div>
            </Modal.Body>
        </Modal>
                );
    }
}