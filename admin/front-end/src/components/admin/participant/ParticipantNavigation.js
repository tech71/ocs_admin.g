import React, { Component } from 'react';
import { setActiveClassProfilePage } from './actions/ParticipantAction'
import { connect } from 'react-redux';
import PinModal from '../PinModal';
import { checkPin} from '../../../service/common.js';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'config.js';

class ParticipantNavigation extends Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;
        this.initialState = {
            loading: true,
            success: false
        }
        this.state = {
            pinModalOpen:false,
            moduleHed:''
        };
    }

    componentDidMount() {
        this.props.changeNavigationClass(this.props.activeClass);
    }

    closeModal=()=>
    {
        this.setState({pinModalOpen:false})
    }
    checkLoginModule= ()=>{
        this.setState({pinModalOpen:true,moduleHed:'FMS Module',color:'Red_fms',pinType:1})
    }

    fmsHtml= ()=>
    {  
        if(!checkPin('fms'))
           return <li><a onClick={()=>this.checkLoginModule()} className={(this.active == 'fms') ? 'active' : ''}>FMS</a ></li>
        else
            return <li><Link to={ROUTER_PATH+'admin/participant/fms/'+this.props.participantId} className={(this.active == 'fms') ? 'active' : ''}>FMS</Link ></li>
    }

    render() {
        return (
            <React.Fragment>
                    {/* <aside className="col-lg-2 col-sm-3 col-lg-offset-1">
                                  {((this.props.activeClass == 'shiftRoster') ?
                                    <ul className="side_menu">
                                        <li><Link to={ROUTER_PATH+'admin/participant/about/' + this.props.participantId} className="major_button"><i className="icon icon-back-arrow"></i> About '{this.props.firstname}'</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/current_shift/' + this.props.participantId} className={(this.active == 'current_shift') ? 'active' : ''}>Current Shifts</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/roster_request/' + this.props.participantId} className={(this.active == 'roster_request') ? 'active' : ''}>Roster Requests</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/current_roster/' + this.props.participantId} className={(this.active == 'current_roster') ? 'active' : ''}>Current Roster</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/financial_overview/' + this.props.participantId} className={(this.active == 'financial_overview') ? 'active' : ''}>Financial Overview</Link></li>
                                    </ul> : '')}
                                   {((this.props.activeClass == 'about')?  
                                   <ul className="side_menu">
                                        <li><Link to={ROUTER_PATH+'admin/participant/about/' + this.props.participantId} className={(this.active == 'about') ? 'active' : ''}>About</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/health/' + this.props.participantId} className={(this.active == 'health') ? 'active' : ''}>Health</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/sites/' + this.props.participantId} className={(this.active == 'sites') ? 'active' : ''}>Sites</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/care_notes/' + this.props.participantId} className={(this.active == 'care_notes') ? 'active' : ''}>Care Notes</Link></li>
                                        {this.fmsHtml()}
                                    </ul>: '')}
                                    {((this.props.activeClass == 'funding')? 
                                    <ul className="side_menu">
                                        <li><Link to={ROUTER_PATH+'admin/participant/about/' + this.props.participantId} className="major_button"><i className="icon icon-back-arrow"></i> About '{this.props.firstname}'</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/plan/' + this.props.participantId} className={(this.active == 'plans') ? 'active' : ''}>Plan(s)</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/goal/' + this.props.participantId} className={(this.active == 'goals') ? 'active' : ''}>Goals</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/doc/' + this.props.participantId} className={(this.active == 'docs') ? 'active' : ''}>Docs</Link></li>
                                        <li><Link to={ROUTER_PATH+'admin/participant/funding/' + this.props.participantId} className={(this.active == 'funds') ? 'active' : ''}>Funds</Link></li>
                                    </ul>: '')}
                    </aside> */}
                    <PinModal 
                    color={'Red_fms'}  
                    moduleHed={this.state.moduleHed} 
                    pinType = {this.state.pinType}
                    modal_show={this.state.pinModalOpen} 
                    returnUrl={'/admin/participant/fms/' + this.props.participantId}
                    closeModal={this.closeModal}
                />
                </React.Fragment>
                );
    }
}


const mapStateToProps = state => ({
    ActiveClass : state.ParticipantReducer.ActiveClassProfilePage,
    firstname : state.ParticipantReducer.participantProfile.firstname,
    participantId : state.ParticipantReducer.participantProfile.id
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeNavigationClass: (value) => dispach(setActiveClassProfilePage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(ParticipantNavigation)
