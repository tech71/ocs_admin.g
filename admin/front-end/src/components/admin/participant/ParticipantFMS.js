import React, { Component } from 'react';

import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { Link} from 'react-router-dom';
import { checkItsNotLoggedIn, postData, checkPinVerified } from '../../../service/common.js';
import {PAGINATION_SHOW} from '../../../config';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import Pagination from "../../../service/Pagination.js";
import {ROUTER_PATH} from "config.js";


const requestData = (pageSize, page, sorted, filtered,participantId) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, participantId : participantId});
        postData('participant/Participant_profile/get_participant_fms', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_count: (result.total_count),
                total_duration: result.total_duration
            };
            resolve(res);
        });

    });
};

class ParticipantFMS extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();
//        checkPinVerified('fms');
        
        this.state = {
            loading: true,
            caseListing: []
        }
    }

    componentDidMount() {
      
    }
    
    fetchData = (state, instance) => {
    this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.id
                ).then(res => {
            this.setState({
                caseListing: res.rows,
                pages: res.pages,
                total_count: res.total_count,
                loading: false,
                total_duration:res.total_duration
            });
        })
    }
    
    render() {
        const columns = [ 
            {Header: 'ID', accessor: 'id', filterable: false,  },
            {Header: 'Timer', accessor: 'timelapse', filterable: false,sortable:false },
            {Header: 'Category', accessor: 'case_category', filterable: false,sortable:false },
            {Header: 'Date of Event', accessor: 'event_date', filterable: false, },
            {Header: 'Initiated By ', accessor: 'initiated_by', sortable:false},
            {Header: 'Against', accessor: 'against_for', sortable:false},
            {Cell: (props) => <span><a href={'/admin/fms/case/'+ props.original.id }><i className="icon icon-views"></i></a></span>, 
                    //Header: <div className="">Action</div>,
                    Header: <TotalShowOnTable countData={this.state.total_count} />,
                     style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, sortable: false}, 
        ]
        return (
            <React.Fragment>

                <ParticipantProfile id={this.props.props.match.params.id}  pageTypeParms="fms"/>
         
                <div className="row">
                    <ParticipantNavigation  active="fms" activeClass={'about'} />
                    <div className="col-lg-12 col-sm-12 schedule_listings p_left_table">
                       
                        <ReactTable
                                PaginationComponent={Pagination}
                                columns={columns}
                                manual 
                                data={this.state.caseListing}
                                pages={this.state.pages}
                                loading={this.state.loading} 
                                onFetchData={this.fetchData} 
                                filtered={this.state.filtered}
                                defaultFiltered ={[{status: ''}]} 
                                defaultPageSize={10}
                                className="-striped -highlight"  
                                noDataText="No Record Found"
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={this.state.caseListing.length > PAGINATION_SHOW ? true : false }

                                />
                                <div className="row">
                                    <div className="col-md-3 pull-right">
                                        <Link to={ROUTER_PATH+'admin/fms/create_case'} className="but">Create FMS Case</Link>
                                    </div>
                                </div>
                              
                    </div>
                    
                </div>
                
                </React.Fragment>
                );
    }
}

export default ParticipantFMS
