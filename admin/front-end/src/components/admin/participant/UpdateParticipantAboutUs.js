import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn, getJwtToken, postData, handleRemoveShareholder, handleShareholderNameChange, handleAddShareholder, handleChange, getOptionsSuburb, archiveALL, reFreashReactTable } from '../../../service/common.js';
import { customProfile,customHeading, customNavigation } from '../../../service/CustomContentLoader.js';

import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import moment from 'moment';
import { interpretertDropdown, cognitionDropdown, prefterLanguageDropdown, HearingOption, communicationDropdown, religiousDropdown,ethnicityDropdown, genderDropdown, preferContactDropdown, sitCategoryListDropdown, relationDropdown } from '../../../dropdown/ParticipantDropdown.js';
import { connect } from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import ReactTable from "react-table";
import 'react-table/react-table.css'
import ParticipantNavigation from '../../admin/participant/ParticipantNavigation';
import ParticipantProfile from '../../admin/participant/ParticipantProfile';
import BookingDatePopup from '../../admin/participant/BookingDatePopup';
import ContactHistoryLog from '../../admin/participant/ContactHistoryLog';
import ParticipantBookingList from '../../admin/participant/ParticipantBookingList';
import ParticipantPrefers from './inner_component/ParticipantPrefers';
import SingleRow from './inner_component/SingleRow';
import DottedLine from './inner_component/DottedLine';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ToastUndo } from 'service/ToastUndo.js'
import ReactGoogleAutocomplete from './../externl_component/ReactGoogleAutocomplete';
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';

//import {setActiveSelectPage} from 'components/admin/participant/actions/ParticipantAction';

export default class UpdateParticipantAboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: [],
            phones: [],
            emails: [],
            kin_detials: [],
            stateList : [],
            fullName: this.props.fullName
        }
    }
    
    onSubmit =(e) => {
         e.preventDefault();
              jQuery('#update_participant').validate();

       if(jQuery('#update_participant').valid({ignore: []})){
            this.setState({loading: true},() => { 
            postData('participant/ParticipantDashboard/update_participant_about', this.state).then ((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Participant update successfully'} showType={'s'} />, {
                    //    toast.success("Participant update successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                          }); 
                          this.props.cloaseModel();
                          this.props.getAboutDetails();
                    } else{
                        // toast.error(result.error, {
                            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                          });
                    }
                    this.setState({loading: false})
            });
        });
       }
    }

    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        //console.log(fieldName);
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value
        
        if(fieldName == 'state'){
            List[index]['city'] = {}
            List[index]['postal'] = ''
        }
        
        if(fieldName == 'city' && value){
             List[index]['postal'] = value.postcode
        }
   

        state[stateName] = List;
        obj.setState(state);
     }   
     
    componentDidMount() {        
        postData('participant/ParticipantDashboard/get_state', {}).then((result) => {
            if (result.status) {
                this.setState({stateList: result.data});
            } 
        });
    }
    
    googleAddressFill = (index, stateKey, fieldtkey, fieldValue) => {
         if (fieldtkey == 'street') {
         var componentForm = {street_number: 'short_name', route: 'long_name',locality: 'long_name',administrative_area_level_1: 'short_name', postal_code: 'short_name'};
         var addess_key = {street: ''};

         for (var i = 0; i < fieldValue.address_components.length; i++) {
            var addressType = fieldValue.address_components[i].types[0];
            if (componentForm[addressType]) {
              var val = fieldValue.address_components[i][componentForm[addressType]];
              
              if(addressType === 'route'){
                   addess_key['street'] = addess_key['street'] +' '+ val;
              }else if(addressType === 'street_number'){
                   addess_key['street'] =  val;
              }else if(addressType === 'locality'){
                   addess_key['city'] =  {value: val, label: val};
              }else if(addressType === 'administrative_area_level_1'){
                   var t_index = this.state.stateList.findIndex(x => x.label == val);
                   addess_key['state'] =  this.state.stateList[t_index].value;
              }else if(addressType === 'postal_code'){
                   addess_key['postal'] =  val;
              }
            }
          }
           
          var List = this.state[stateKey];
    
          List[index] = addess_key;
          var state = {};
          state[stateKey] = List;
          this.setState(state);
           
        }
    }
    
    handleChange = (e)=> {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    
    
     componentWillReceiveProps(newProps) {
      this.setState(JSON.parse(JSON.stringify(newProps.about)));

      if(newProps.about.kin_detials.length < 1){
          this.setState({kin_detials: [{firstname:'',lastname: '', phone: '', email:'', relation:''}]});
          
      }
    }
   
     selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    render() {
        return (
            <div>
            <Modal
               className="modal fade"
                bsSize="large"
               show={this.props.update}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body>
              <form    id="update_participant">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12 bb-1 text-left px-0 pb-3 Popup_h_er_1">
                                <h2 className="color">Update Participant Details:</h2><a className="close_i" onClick={this.props.cloaseModel}><i className="icon icon-cross-icons"></i></a>
                            </div>
                        </div>
                        <div className="row P_25_TB">
                            <div className="col-md-3 pl-0 col-xs-12 pl-xs-2">
                                <label>User Name:</label> 
                                <span className="required">
                                    <input data-rule-required='true' placeholder="User Name" type="text" name="username" value={this.state['username'] || ''} readOnly="true"/>
                                </span>
                            </div>
			    <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>First Name:</label> 
                                <span className="required">
                                    <input data-rule-required='true' placeholder="First Name" type="text" name="firstname" value={this.state['firstname'] || ''} onChange={this.handleChange} />
                                </span>
                            </div>
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>Middle Name</label>
                                    <input placeholder="Middle Name" type="text" name="middlename" value={this.state['middlename'] || ''} onChange={this.handleChange} /> 
                            </div>
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>Last Name</label> 
                                <span className="required">
                                    <input placeholder="Last Name" type="text" name="lastname" value={this.state['lastname'] || '' }  onChange={this.handleChange} data-rule-required="true" />
                                </span>
                            </div>
                            <div className="col-md-2 P_15_T pl-0 col-xs-12 pl-xs-2 mt-xs-3">
                                <label>Gender</label> 
                                <span className="required">
                                <Select clearable={false} name="gender" className="custom_select" simpleValue={true} data-rule-required={true} value={this.state['gender'] || ''}  onChange={(e)=>this.selectChange(e,'gender')} options={genderDropdown(0)} 
                                 searchable={false} placeholder="Gender" />  
                                 </span>
                            </div>
                            <div className="col-md-2 P_15_T col-xs-12 mt-xs-3">
                                <label>D.O.B</label> 
                                <span className="required">
                                    <DatePicker autoComplete={'off'} showYearDropdown scrollableYearDropdown yearDropdownItemNumber={110} dateFormat="DD/MM/YYYY" maxDate={moment()} data-rule-required={true} data-placement={'bottom'} name="dob" onChange={(date) => this.setState({dob : date })} selected={this.state['dob'] ? moment(this.state['dob']) : null}   className="text-center px-0" placeholderText="00/00/0000" />
                                </span>
                            </div>
                        </div>

                         <div className="row">
                            <div className="col-md-12 col-xs-12 P_15_TB by-1 px-0 ">
                                <h3 className="color">Participant Personal Details:</h3>
                            </div>
                          </div>
                        <div className="row P_25_T">
                            <div className="col-md-3 col-xs-12 pl-0 pl-xs-2 mt-xs-3">
                                <label>Preferred Name:</label>
                                 <input type="text" placeholder="Preferred Name" value={this.state['preferredname'] || ''} name="preferredname" onChange={this.handleChange}/>
                            </div>
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>NDIS #:</label>
                                <input type="text" placeholder="000 000 000" name="ndis_num" value={this.state['ndis_num']  || ''} onChange={this.handleChange}/>
                            </div>
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>Medicare #:</label>
                                <input type="text" placeholder="0000 00000 0"  name="medicare_num" value={this.state['medicare_num']  || ''} onChange={this.handleChange}/>
                            </div>
                            <div className="col-lg-2 col-md-2 col-xs-12 mt-xs-3">
                                <label>CRN:</label>
                                <input type="text" placeholder="000000" name="crn_num" value={this.state['crn_num'] || ''}  onChange={this.handleChange}/>
                            </div>
                        </div>
                        <div className="row P_25_T">
                            <div className="col-md-3 col-xs-12 pl-0 pl-xs-2 mt-xs-3">
                                <label>Phone (Primary):</label>
                                <span className="required">
                                    {this.state.phones.map((PhoneInput, idx) => (
                                    <div className="input_plus__  mb-1" key={idx + 1}> 
                                        <input className="input_f" type="text" 
                                               value={PhoneInput.phone} 
                                               name={'phone_primary' + idx} 
                                               placeholder="Can include area code" 
                                               onChange={(evt) => handleShareholderNameChange(this, 'phones', idx, 'phone', evt.target.value)} 
                                               data-rule-required="true"
                                               data-rule-phonenumber
                                               data-msg-notequaltogroup="Please enter a unique contact number"
                                              
                                               />

                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'phones')}> 
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                        </button> : (this.state.phones.length >= 2)? '':<button onClick={(e) => handleAddShareholder(this, e, 'phones', PhoneInput)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                        </button>}
                                    </div>
                                ))}
                                </span>
                            </div>
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>Email</label>
                                <span className="required">
                                    {this.state.emails.map((EmailInput, idx) => (
                                    <div className="input_plus__  mb-1" key={idx + 1}> 
                                        <input className="input_f distinctEmail pr-5" type="text" 
                                         value={EmailInput.email || ''} 
                                         name={'email_' + idx}
                                         ref="name"
                                         placeholder={idx > 0? 'Secondary email' : 'Passwor recovery'} 
                                         onChange={(evt) => handleShareholderNameChange(this, 'emails', idx, 'email', evt.target.value)} 
                                         data-rule-required="true" 
                                         data-rule-email="true"
                                         data-rule-notequaltogroup='[".distinctEmail"]'
                                        />
                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'emails')}>
                                            <i  className="icon icon-decrease-icon Add-2" ></i>
                                        </button> : (this.state.emails.length >= 2)? '': <button onClick={(e) => handleAddShareholder(this, e, 'emails', EmailInput)}>
                                            <i  className="icon icon-add-icons Add-1" ></i>
                                        </button>} 
                                    </div>
                                ))}
                                </span>
                            </div>							
                            <div className="col-md-3 col-xs-12 mt-xs-3">
                                <label>Preferred Contact:</label>
                                <span className="required">
                                    <Select clearable={false} className="custom_select" data-rule-required={true} simpleValue={true} name="prefer_contact" onChange={(e)=>this.selectChange(e,'prefer_contact')} value={this.state['prefer_contact']} searchable={false}  options={preferContactDropdown(0)} placeholder="Contact" data-rule-required="true" />   
                                </span>
                            </div>
                        </div>
                        <div className="row P_15_T">
                            <div className="col-lg-12">
                                <div className="row">                            
                                {this.state.address.map((AddressInput, idx) => (
                                    <div  key={idx + 1} className="col-md-6 padding-top-50">
                                        <div className="row">
                                            <div className="col-md-6 pl-0 col-xs-12 pl-xs-2 mt-xs-3">
                                                <label>{idx > 0 ? 'Address (Secondary):' : 'Address (Primary):'} </label>                                        
                                                    <span key={idx + 1}>
                                                    <span className="required">
                                                    <div className="add_input " key={idx + 1}> 
                                                    
                                                        <ReactGoogleAutocomplete className="input_f mb-1  pr-5" key={idx + 1}
                                                             required={true}
                                                             data-msg-required="Add address"
                                                             name={"address_primary" + idx}
                                                             onPlaceSelected={(place) => this.googleAddressFill(idx, 'address', 'street', place)}     
                                                             types={['address']}
                                                             value={AddressInput.street || ''}
                                                             onChange={(evt) => handleShareholderNameChange(this, 'address', idx, 'street', evt.target.value)}
                                                             onKeyDown={(evt) => handleShareholderNameChange(this, 'address', idx, 'street', evt.target.value)}
                                                             componentRestrictions={{country: "au"}}
                                                        />
                                                        
                                                    </div>
                                                    </span>
                                                    </span>                                          
                                            </div>
                                            <div className="col-md-5 col-xs-12 mt-xs-3">
                                                <label>{idx > 0 ? ' Site Category:' : ' Site Category:'} </label>   
                                                <Select clearable={false} searchable={false} 
                                                simpleValue={true}
                                                value={AddressInput.site_category || ''}  
                                                name={'site_category' + idx} 
                                                onChange={(evt) => handleShareholderNameChange(this, 'address', idx, 'site_category', evt)} 
                                                options={sitCategoryListDropdown(0)} 
                                                placeholder="Please Select" 
                                                />
                                            </div>
                                        </div>
                                        <div className="row P_15_T AL_flex">
                                            <div className="col-md-3 pl-0 col-xs-12 pl-xs-2 mt-xs-3">
                                             <label>State:</label>
                                                <span className="required">
                                                <Select clearable={false}
                                                className="custom_select"
                                                simpleValue={true}
                                                name={'select_state_primary' + idx}
                                                searchable={false}
                                                value={AddressInput.state || ''}
                                                onChange={(e) => this.handleShareholderNameChange(this, 'address', idx, 'state', e)} 
                                                options={this.state.stateList} 
                                                 placeholder="Please Select" />
                                                
                                                </span>
                                            </div>
                                            <div className="col-md-4 pl-0 col-xs-12 pl-xs-2 mt-xs-3">
                                             <label>Suburb:</label>
                                                <span className="required modify_select">
                                               <Select.Async clearable={false} 
                                                className="default_validation" required={true}
                                                value={AddressInput.city}
                                                cache={false} 
                                                disabled={(AddressInput.state)? false: true} 
                                                loadOptions={(val) => getOptionsSuburb(val, AddressInput.state)} 
                                                onChange={(e) => this.handleShareholderNameChange(this, 'address', idx, 'city', e)} 
                                                options={this.state.stateList} placeholder="Search" />
                                                
                                                </span>
                                            </div>
                                            
                                            <div className="col-md-3  col-xs-12 mt-xs-3">
                                                <label>Postcode:</label>
                                                <span className="required">
                                                <input className="input_f distinctEmail text-center"
                                                 type="text" 
                                                value={AddressInput.postal} 
                                                name={'input_postcode_primary' + idx} 
                                                placeholder='Postcode' 
                                                minLength="4" maxLength="4"
                                                onChange={(evt) => handleShareholderNameChange(this, 'address', idx, 'postal', evt.target.value)} 
                                                data-rule-required="true" data-rule-number="true" data-rule-postcodecheck="true" data-msg-number="Please enter valid Postcode" /> 
                                                </span>
                                            </div>
                                            <div className="col-md-1  col-xs-12 mt-xs-3">

                                            {idx > 0 ? <button title={ParticiapntPageIconTitle.par_remove_icon}  className="button_plus__" onClick={(e) => handleRemoveShareholder(this, e, idx, 'address')}>
                                                            <i  className="icon icon-decrease-icon Add-2-2" ></i>
                                                        </button> : (this.state.address.length >= 3)? '': <button title={ParticiapntPageIconTitle.par_add_icon}  className="button_plus__" onClick={(e) => handleAddShareholder(this, e, 'address', AddressInput)}>
                                                            <i  className="icon icon-add-icons Add-2-1" ></i>
                                                        </button>} 
                                            </div>
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        </div>
                        <div className="row P_25_T">
                            <div className="col-md-12 P_15_TB by-1 px-0"><h3 className="color">Next of kin Details:</h3></div><div className="col-lg-1"></div>
                        </div>

                        <div className="row">                            
                                {this.state.kin_detials.map((obj, idx) => (
                        <div  key={idx + 1} className="row P_25_T">
                            <div className="col-md-3  col-xs-12 mt-xs-3">
                                <label>Next of Kin Name:</label>
                                        <input 
                                         type="text" 
                                        className="input_f mb-1 custom_input"
                                        value={obj.firstname || ''} 
                                        name={'input_kin_first_name' + idx} 
                                        placeholder='First Name' 
                                       
                                        onChange={(evt) => handleShareholderNameChange(this, 'kin_detials', idx, 'firstname', evt.target.value)} 
                                       />   
                            </div>                    
                            <div className="col-md-3  col-xs-12 mt-xs-3">
                                        <label>Next of Kin Last Name:</label>
                                        <input 
                                         type="text" 
                                        className="input_f mb-1 custom_input"
                                        value={obj.lastname || ''} 
                                        name={'input_kin_lastname' + idx} 
                                        placeholder='Last Name' 
                                       
                                        onChange={(evt) => handleShareholderNameChange(this, 'kin_detials', idx, 'lastname', evt.target.value)} 
                                         />
                            </div>
                            <div className="col-md-3  col-xs-12 mt-xs-3">
                                        <label>Next of Kin Phone:</label>
                                        <input 
                                         type="text" 
                                        className="input_f mb-1 custom_input"
                                        value={obj.phone || ''} 
                                        name={'input_kin_contact' + idx} 
                                        placeholder='Phone'
                                        data-rule-phonenumber 
                                      
                                        onChange={(evt) => handleShareholderNameChange(this, 'kin_detials', idx, 'phone', evt.target.value)} 
                                        />
                            </div>
                            <div className="col-md-3  col-xs-12 mt-xs-3">
                                <label>Next of Kin Email:</label>
                                        <input 
                                        className="input_f mb-1 distinctEmail custom_input"
                                        type="text"
                                        value={obj.email || ''} 
                                        name={'input_kin_email' + idx} 
                                        placeholder='Email' 
                                       
                                        onChange={(evt) => handleShareholderNameChange(this, 'kin_detials', idx, 'email', evt.target.value)} 
                                        />
                            </div>
                            <div className="col-md-3 P_15_T  col-xs-12">
                                <label>Relation:</label>
                                <div className="row">
                                    <div className="col-md-9  col-xs-12">
                                    <span className="required">
                                        <Select className="custom_select" clearable={false}
                                        searchable={false} 
                                        simpleValue={true}
                                        value={obj.relation || ''}  
                                        name={'input_relation_primary'+idx} 
                                       
                                        onChange={(evt) => handleShareholderNameChange(this, 'kin_detials', idx, 'relation', evt)} 
                                        options={relationDropdown(0, 'clearable')} 
                                        placeholder="Please Select" 
                                        />
                                    </span>
                                    </div>
                                    <div className="col-md-3  col-xs-12 mt-xs-3">
                                        {idx > 0 ? <button title={ParticiapntPageIconTitle.par_remove_next_of} className="button_plus__" onClick={(e) => handleRemoveShareholder(this, e, idx, 'kin_detials')}>
                                                            <i  className="icon icon-decrease-icon Add-2-2" ></i>
                                                        </button> : (this.state.kin_detials.length == 3)? '': <button title={ParticiapntPageIconTitle.par_add_next_of} className="button_plus__" onClick={(e) => handleAddShareholder(this, e, 'kin_detials', obj)}>
                                                            <i  className="icon icon-add-icons  Add-2-1" ></i>
                                                        </button>}
                                    </div>
                                </div>
                            </div>
                            </div>  
                            ))}
                        </div>
                        <div className="row P_7_T">
                            <div className="col-md-3 col-md-offset-9 pr-0 pl-xs-0 mt-xs-5">
                                <button disabled={this.state.loading} onClick={this.onSubmit} type="submit" href="#" className="but" >Update</button>
                            </div>
                        </div>

                    </div></form >
           
               </Modal.Body>
            </Modal>
        </div>
        );
    }
}