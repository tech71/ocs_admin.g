import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link, Redirect } from 'react-router-dom';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import jQuery from "jquery";
import { postData, handleChangeChkboxInput, handleShareholderNameChange, toastMessageShow } from '../../../service/common.js';
import { ToastUndo } from 'service/ToastUndo.js';
import {  toast } from 'react-toastify';
import { ROUTER_PATH} from 'config.js';
class AddContact extends Component {

    constructor(props) {
        super(props);
        this.state = {
          redirect:false
        }
    }
    addContract = () => {
       if(jQuery('#notes').valid({ignore: []})){
        this.setState({ loading: true }, () => {
            postData('crm/CrmStage/modified_status', { ndis_num: this.props.ndis,notes:this.state.notes }).then((result) => {
                if (result.status) {
                  this.setState({redirect:true,c_id:result.id});

                }else if(!result.status){
                    toastMessageShow(result.error,'e');
                }
                this.setState({ loading: false });
            });
        });
      }
    }
    render() {
      if(this.state.redirect){      
        window.location = ROUTER_PATH + 'admin/crm/participantdetails/'+this.state.c_id;
      }
        return (
            <form id="add_device" method="post" autoComplete="off">
                <div className={'customModal ' + (this.props.showModal ? ' show' : '')}>
                    <div className="cstomDialog widBig" style={{ width: " 800px", minWidth: " 800px" }}>

                        <h3 className="cstmModal_hdng1--">
                            {this.props.popUpTitle}
                            <span className="closeModal icon icon-close1-ie" onClick={() => this.props.closeModal(false)}></span>
                        </h3>
                        <form id="notes">
                        <div className="row  pd_b_20 mt-5 mb-5 d-flex align-content-end">
                            <div className="col-md-12">
                                <div className="csform-group">
                                    <label>Note:</label>
                                    <span className="required">
                                    <textarea className="csForm_control txt_area brRad10 textarea-max-size bl_bor" data-rule-required='true' data-msg-required="Add Note" placeholder="Note" onChange={(e) => this.setState({ notes: e.target.value })}></textarea>
                                    </span>
                                    </div>
                            </div>
                        </div>

                        <div className="row bt-1" style={{ paddingTop: "15px" }}>
                            <div className="col-md-12 text-right">
                                <input type='button' className='btn cmn-btn1 new_task_btn' value='Save' onClick={() => this.addContract()} />
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </form>
        );
    }
}

export default AddContact;
