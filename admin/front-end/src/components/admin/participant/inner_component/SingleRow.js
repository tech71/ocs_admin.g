import React, { Component } from 'react';
class SingleRow extends React.Component {
    render() {
        return <div className={this.props.Myclass}>
            <div className="col-lg-3 col-md-3 f align_e_2 col-sm-3 col-xs-4">{this.props.title}:</div>
            <div className="col-lg-9 col-md-9 f align_e_1 col-sm-9 col-xs-8">{this.props.value || 'N/A'} </div>
        </div>;
    }
}

export default SingleRow