import React, { Component } from 'react';
import Modal from 'react-bootstrap/lib/Modal';
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {  postData } from '../../../../service/common.js';
import { connect } from 'react-redux'
import "react-placeholder/lib/reactPlaceholder.css";
import SingleRow from './SingleRow';
import DottedLine from './DottedLine';
import {ParticiapntPageIconTitle} from 'menujson/pagetitle_json';
import ScrollArea from 'react-scrollbar';


import { ToastUndo } from 'service/ToastUndo.js'

class ParticipantPrefers extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            activity: [],
            places: [],
            preference : [],
            modal: false
        }
    }
    
    componentDidMount() {
       this.getPreferers();
    }
    
    closeModal = () =>{
        this.setState({modal: false});
    }
    
    getPreferers = () => {
         this.setState({loading: true});
        postData('participant/Participant_profile/get_participant_preference', {id:this.props.participantId}).then((result) => {
            if (result.status) {
                var details = result.data;
                this.setState(details);
            } 
            this.setState({loading: false});
        });
    }
    
     render() { 
         return (
                 <div  role="tabpanel" className={this.props.showTypePage=='preferences'? "tab-pane active":'tab-pane'} id="prefers">
                    <div className="row">
                            <div className="col-lg-12 col-md-12">
                                <div className="row">
                                <div className="col-md-12"><div className="bor_T"></div></div>
                                    <div className="col-md-12 P_7_TB"><h3>
                                        {this.props.firstname+"'s "}
                                        Preferences</h3></div>
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                            
                        </div>
                        <div className="row pt-5">
                            <div className="col-md-12"> 
                                <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div className="col-lg-12">
                                        <SingleRow  Myclass={'row f_color_size'} title={'Places (Favourite)'} value={this.state.placeFav || 'N/A'} /><DottedLine/>
                                        <SingleRow  Myclass={'row f_color_size'} title={'Places (Least Favourite)'} value={this.state.placeLeastFav || 'N/A'} /><DottedLine/>
                                        <div className="row text-right mt-2 mb-5">
                                            <a><i className="icon icon-update update_button" onClick={() => this.setState({modal:true, preference: this.state.places, type: 'places'})}></i></a>    
                                         </div>    
                                        <SingleRow  Myclass={'row f_color_size align_top_to_1'} title={'Activities (Favourite)'} value={this.state.activityFav || 'N/A'} /><DottedLine/>
                                        <SingleRow  Myclass={'row f_color_size align_top_to_1'} title={'Activities (Least Favourite)'} value={this.state.activityLeastFav || 'N/A'} /><DottedLine/>
                                        <div className="row text-right mt-2 mb-5">
                                            <a><i className="icon icon-update update_button" onClick={() => this.setState({modal:true, preference: this.state.activity, type: 'activity'})}></i></a>
                                        </div>
                                    </div>
                                    
                                    <UpdatePreference fullName={this.props.fullName} type={this.state.type} getPreferers={this.getPreferers} participantId ={this.props.participantId} closeModal={this.closeModal} preference={this.state.preference} modal ={this.state.modal} />
                                </div>
                            </div>
                        </div>
                 </div>
                    
            )}
}

const mapStateToProps = state => ({
     firstname: state.ParticipantReducer.participantProfile.firstname,
     fullName: state.ParticipantReducer.participantProfile.fullName,
     showTypePage: state.ParticipantReducer.activePage.pageType
})

export default connect(mapStateToProps)(ParticipantPrefers)

class UpdatePreference extends Component {
    constructor(props) {
        super(props);
        this.state = {
            preference: [],
            fullName : this.props.fullName
        }
    }
    
    onSubmit =(e) => {
         e.preventDefault();
              this.setState({loading: true},() => {  
             postData('participant/Participant_profile/update_participant_preference', this.state).then ((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={"Preferences updated successfully"} showType={'s'} />, {      
                    //    toast.success("Preferences updated successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                          }); 
                          this.props.closeModal();
                          this.props.getPreferers();
                    } else{
                        toast.error(<ToastUndo message={'Api error'} showType={'e'} />, {
                        // toast.error('Api error', {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                          });
                    }
                    this.setState({loading: false});
            });
              });
    }
   
    handleChange = (e)=> {
        var state = {};
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }
    
     componentWillReceiveProps(newProps) {
          this.setState(newProps);
    }
    
    setRadio = ( e, idx, tagType, value)=> {
         var List = this.state[tagType];
         List[idx].type = value;
        
        var state = {};
        state[tagType] = List;
        this.setState(state); 
    }
    
    render() {
        return (
            <div>
            <Modal
               className="modal fade Modal_A Modal_B preferences_modal"
               show={this.props.modal}
               onHide={this.handleHide}
               container={this}
               aria-labelledby="contained-modal-title"
             >
              <Modal.Body style={{paddingBottom:0}}>
                <div className="row">
                    <form onSubmit={this.onSubmit}>
                   <div className="col-md-12 multiple_checkbox_tooltip">
                       <div className="Popup_h_er_1 mb-4">
                        <h2 className="color pb-0 ">{(this.props.type == 'activity')? 'Activity' : 'Places'}:</h2> <a className="close_i" onClick={this.props.closeModal}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div className="h-45  bt-1 bl-1 br-1 bb-1">
                        <div className="cstmSCroll1">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "390px" }}>

                                {this.state.preference.map((req, idx) => (
                                <span  className="d-flex s-w-d_div" key={idx + 1}>
                                    <h4 className="font_w_4"> {req.label}</h4>
                                    <div className="cartoon_div">

                                    <div className="d-inline  danger-cartoon">
                                           <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', '2')} name={req.label} checked={(req.type == '2')? true: false} />
                                        <label title={ParticiapntPageIconTitle.LeastFavorite} onClick={(e) => this.setRadio(e, idx, 'preference','2')} ><span><span></span></span></label>
                                    </div>
                                    
                                     <div className="d-inline worning-cartoon">
                                           <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', 'none')} name={req.label} checked={(req.type == 'none')? true: false} />
                                        <label title={ParticiapntPageIconTitle.NoPreference} onClick={(e) => this.setRadio(e, idx, 'preference','none')} ><span><span></span></span></label>
                                    </div>
                                    <div className="d-inline success-cartoon">
                                        <input type="radio" className="radio_input" onChange={(e) => this.setRadio(e, idx, 'preference', '1')}  name={req.label} checked={(req.type == '1')? true: false} />
                                        <label  title={ParticiapntPageIconTitle.Favorites} onClick={(e) => this.setRadio(e, idx, 'preference','1')} ><span><span></span></span></label>
                                    </div>

                                    </div>
                                </span>
                                ))}
                            
                            </ScrollArea>
                            </div>
                            </div>
                        </div>
                        <div className="col-md-4 col-md-offset-4 my-3">
                        <button type="submit" disabled={this.state.loading} className="default_but_remove button but">Submit</button>
                         </div>
                     </form>
                </div>
                
               </Modal.Body>
            </Modal>
            </div>
        );
    }
}