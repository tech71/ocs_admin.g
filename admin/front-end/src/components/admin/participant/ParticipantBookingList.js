import React, { Component } from 'react';
import jQuery from "jquery";
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn, getJwtToken, postData, handleRemoveShareholder, handleShareholderNameChange, handleAddShareholder, handleChange, getOptionsSuburb, archiveALL, reFreashReactTable } from '../../../service/common.js';
import { customProfile, customHeading, customNavigation } from '../../../service/CustomContentLoader.js';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import moment from 'moment';
import { interpretertDropdown, cognitionDropdown, prefterLanguageDropdown, HearingOption, communicationDropdown, religiousDropdown, ethnicityDropdown, genderDropdown, preferContactDropdown, sitCategoryListDropdown, relationDropdown } from '../../../dropdown/ParticipantDropdown.js';
import { connect } from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import ReactTable from "react-table";
import { ToastUndo } from 'service/ToastUndo.js';
import Pagination from "../../../service/Pagination.js";
import { ParticiapntPageIconTitle } from 'menujson/pagetitle_json';
import 'react-table/react-table.css'

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('participant/Participant_profile/get_participant_booking_list', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class ParticipantBookingList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            bookingList: [],
            modal: false,
            participantId: this.props.participantId,
            fullName: this.props.fullName,
            view_by: 'current',
            participantList: []
        }
        this.reactTable = React.createRef();
        this.viewByOption = [{ value: 'current', label: 'Current Bookers' }, { value: 'archive', label: 'Achieved Bookers' }]
    }

    componentWillReceiveProps(newProps) {
        this.setState({ fullName: newProps.fullName });
    }


    updateBooker = (e, booker) => {
        this.setState(booker);
        this.setState({ modal_show: true })
    }

    changeView = (e) => {
        this.setState({ view_by: e });
        this.setState({ filtered: { participantId: this.props.participantId, view_by: e } })
    }

    addBooker = () => {
        this.setState({ firstname: '', lastname: '', phone: '', email: '', relation: relationDropdown(1), id: '' });
        this.setState({ modal_show: true })
    }

    archiveBooker = (e, bookerId) => {
        archiveALL({ participantId: this.props.participantId, bookerId: bookerId }, '', 'participant/Participant_profile/archive_booker').then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        })
    }

    onSubmit = (e) => { 
        e.preventDefault();
        toast.dismiss();
        jQuery('#add_booking_list').validate();
        if (jQuery('#add_booking_list').valid())
         {
            this.setState({ loading: true }, () => {
                postData('participant/Participant_profile/update_participant_booking_list', this.state).then((result) => {
                    if (result.status) {
                        reFreashReactTable(this, 'fetchData');
                        this.setState({ modal_show: false })
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            //    toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }

                    this.setState({ loading: false });
                });
            })
        }
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                participantList: res.rows,
                pages: res.pages,
                loading: false
            });
        });

    }

    render() {
        const { data, pages, loading } = this.state;
        const columns = [
            {
                Header: 'Booker Name', filterable: false,
                Cell: props => <span className="font_issue"><div className="ellipsis-one">{props.original.firstname + ' ' + props.original.lastname}</div></span>
            },
            {
                Header: 'Relationship', accessor: 'relation', filterable: false,
                Cell: props => <span className="font_issue"><div className="ellipsis-one">{props.original.relation}</div></span>
            },
            {
                Header: 'Phone', accessor: 'phone', filterable: false,
                Cell: props => <span className="font_issue"><div className="ellipsis-one">{props.original.phone}</div></span>
            },
            {
                Header: 'Email', accessor: 'email', filterable: false,
                Cell: props => <span className="font_issue"><div className="ellipsis-one">{props.original.email}</div></span>
            },
            {
                accessor: 'id', filterable: false, maxWidth: 130, headerStyle: { border: "0px solid #fff", },
                Cell: props => <span className="booking_L_T1">
                    {props.original.archive == 0 ? <i title={ParticiapntPageIconTitle.par_update_icon} onClick={(e) => this.updateBooker(e, props.original)} className="icon icon-update update_button"></i> : ''}
                    <i title={ParticiapntPageIconTitle.par_history_icon} className="icon icon-pending-icons history_button mx-3"></i>
                    {props.original.archive == 0 ? <i title={ParticiapntPageIconTitle.par_archive_icon} onClick={(e) => this.archiveBooker(e, props.value)} className="icon icon-email-pending archive_button"></i> : ''}
                </span>, Header: <TotalShowOnTable countData={this.state.participantList.length} />,
            }
        ]
        return (
            <div role="tabpanel" className={this.props.showTypePage == 'bookers_list' ? "tab-pane active" : "tab-pane"} id="booking_list">
                <div className="row">
                    <div className="col-lg-9 col-md-9">
                        <div className="row">
                            <div className="col-md-12"><div className="bor_T"></div></div>
                            <div className="col-md-12 P_7_TB">
                                <h3>{this.props.firstname + " "} Booker List</h3></div>
                            <div className="col-md-12"><div className="bor_T">
                            </div></div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <Select name="view_by" simpleValue={true} searchable={false} clearable={false}
                            value={this.state.view_by} onChange={(e) => this.changeView(e)}
                            options={this.viewByOption} placeholder="View By:" />
                    </div>
                </div>
                <div className="row P_15_TB">
                    <div className="col-md-12">
                        <div className="panel-group accordion_me house_accordi Par_care_ac" id="accordion" role="tablist" aria-multiselectable="true">
                            <div className="schedule_listings p_left_table mac_line_e_ mac_line_h__">
                                <ReactTable
                                    PaginationComponent={Pagination}
                                    ref={this.reactTable}
                                    columns={columns}
                                    manual
                                    data={this.state.participantList}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    defaultFiltered={{ participantId: this.props.participantId, view_by: this.state.view_by }}
                                    defaultPageSize={10}
                                    className="-striped -highlight"
                                    noDataText="No Record Found"
                                    onPageSizeChange={this.onPageSizeChange}
                                    minRows={2}
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                    showPagination={false}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-4 col-lg-offset-9 col-sm-offset-8 ">
                        {this.state.view_by === 'current'?
                        <div onClick={this.addBooker} className="mt-3 pull-right"><a className="button_plus__"><span className="icon icon-add-icons Add-2-1"></span></a></div>: ''}
                    </div>
                </div>

                <Modal
                    className="modal fade Modal_A Modal_B"
                    show={this.state.modal_show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <div className="row">

                            <div className="col-md-12 ">
                                <h2 className="color text text-left Popup_h_er_1"><span>{this.state.id ? 'Update ' : 'Add '} Booking List</span><a className="close_i" onClick={() => this.setState({ modal_show: false })}><i className="icon icon-cross-icons"></i></a></h2>
                                <form onSubmit={this.onSubmit} id="add_booking_list">
                                    <div className="">
                                        <div className="row P_25_T d-flex flex-wrap after_before_remove">
                                            <div className="col-md-4 mb-3">
                                                <label>First Name:</label>
                                                <span className="required">
                                                <input type="text" data-rule-required="true" className="input_f mb-1 " value={this.state.firstname || ''} name={'firstname'} placeholder='First Name' onChange={(e) => handleChange(this, e)} />
                                                </span>
                                            </div>
                                            <div className="col-md-4 mb-3">
                                                <label>Last Name:</label>
                                                <span className="required">
                                                <input
                                                    type="text"
                                                    className="input_f mb-1 "
                                                    value={this.state.lastname || ''}
                                                    name={'lastname'}
                                                    placeholder='Last Name' data-rule-required="true"
                                                    onChange={(e) => handleChange(this, e)}
                                                />
                                                </span>
                                            </div>
                                            <div className="col-md-4 mb-3">
                                                <label>Phone:</label>
                                                <span className="required">
                                                <input
                                                    type="text"
                                                    className="input_f mb-1 "
                                                    value={this.state.phone || ''}
                                                    name="phone"
                                                    placeholder='Phone' data-rule-required="true"
                                                    data-rule-phonenumber
                                                    onChange={(e) => handleChange(this, e)}
                                                />
                                                </span>
                                            </div>
                                            <div className="col-md-4 mb-3">
                                                <label>Email:</label>
                                                <span className="required">
                                                <input className="input_f mb-1 distinctEmail "
                                                    type="email"
                                                    value={this.state.email || ''}
                                                    name="email"
                                                    placeholder='Email' data-rule-required="true"
                                                    onChange={(e) => handleChange(this, e)}
                                                />
                                                </span>
                                            </div>
                                            <div className="col-md-4 mb-3">
                                                <label>Relation:</label>
                                                <div className="row">
                                                    <div className="col-md-9">
                                                        <span className="required">
                                                            <Select className="custom_select" clearable={false}
                                                                searchable={false}
                                                                simpleValue={true}
                                                                value={this.state.relation || relationDropdown(1)}
                                                                name="relation"
                                                                required={true}
                                                                isValidNewOption={this.tempFunction}
                                                                onChange={(e) => this.setState({ relation: e })}
                                                                options={relationDropdown(0)}
                                                                placeholder="Please Select"
                                                            />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row d-flex justify-content-end">
                                            <div className="col-md-4 mt-3">
                                                <button disabled={this.state.loading} type="submit" className="default_but_remove button but">Submit</button>
                                            </div>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    firstname: state.ParticipantReducer.participantProfile.firstname,
    fullName: state.ParticipantReducer.participantProfile.fullName,
    showTypePage: state.ParticipantReducer.activePage.pageType
})


export default connect(mapStateToProps)(ParticipantBookingList)
