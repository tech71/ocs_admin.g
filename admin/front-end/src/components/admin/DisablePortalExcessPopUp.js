import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import {Link, Redirect } from 'react-router-dom';
import jQuery from "jquery";
import {postData } from '../../service/common.js';



class DisablePortalExcessPopUp extends Component {
    state = {
        
    }
    AddDisableNote = () => { 
        if(jQuery('#notes').valid({ignore: []})){
            this.props.disableEnableParticipantAccess(0 , this.state.notes);
        }
    }
    
    render() {
        return (
            <form id="add_device" method="post" autoComplete="off">
                <div className={'customModal ' + (this.props.showModal ? ' show' : '')}>
                    <div className="cstomDialog widBig" style={{ width: " 800px", minWidth: " 800px" }}>

                        <h3 className="cstmModal_hdng1--">
                            Reason
                            {/*<span className="closeModal icon icon-close1-ie" onClick={() => this.props.closeModal(false)}></span>*/}
                        </h3>
                        <form id="notes">
                        <div className="row  pd_b_20 mt-5 mb-5 d-flex align-content-end">
                            <div className="col-md-12">
                                <div className="csform-group">
                                    <label>Note:</label>
                                    <div className="">
                                    <textarea className="csForm_control txt_area brRad10 textarea-max-size bl_bor" maxLength="500" required="true" data-msg-required="Add Note" placeholder="Note" onChange={(e) => this.setState({ notes: e.target.value })}></textarea>
                                    </div>
                                    </div>
                            </div>
                        </div>

                        <div className="row bt-1" style={{ paddingTop: "15px" }}>
                            <div className="col-md-12 text-right">
                                <input type='button' className='btn cmn-btn1 new_task_btn' value='Save' onClick={() => this.AddDisableNote()} />
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </form>
        );
    }
}

export default DisablePortalExcessPopUp;