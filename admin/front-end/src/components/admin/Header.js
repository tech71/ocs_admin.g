import React, { Component } from 'react';
import { postData, selectFilterOptions } from 'service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import PropTypes from 'prop-types';
import createClass from 'create-react-class';
import { setNotificationToggel, setNotificationAlert, setNotificationImailAlert } from './notification/actions/NotificationAction.js';
import { connect } from 'react-redux'
import WebsocketUserConnectionHandleData from './externl_component/WebsocketUserConnectionHandleData'
import {getPermission} from 'service/common';

const globalSearch = (input) => {
    if(input){
        if (input.length <= 2 && isNaN(input)) {
            return Promise.resolve({ options: [] });
        }
        
        var request = { search: input };
        return postData('common/Common/get_global_search_option', request).then((result) => {
            return { options: result.data };
        });
    }else{
        return Promise.resolve({ options: [] });
    }
}

const GravatarOption = createClass({
    propTypes: {
        children: PropTypes.node,
        className: PropTypes.string,
        isDisabled: PropTypes.bool,
        isFocused: PropTypes.bool,
        isSelected: PropTypes.bool,
        onFocus: PropTypes.func,
        onSelect: PropTypes.func,
        option: PropTypes.object.isRequired,
    },
    handleMouseDown(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.onSelect(this.props.option, event);
    },
    handleMouseEnter(event) {
        this.props.onFocus(this.props.option, event);
    },
    handleMouseMove(event) {
        if (this.props.isFocused) return;
        this.props.onFocus(this.props.option, event);
    },
    render() {
        return (
            <div className={this.props.className}
                onMouseDown={this.handleMouseDown}
                onMouseEnter={this.handleMouseEnter}
                onMouseMove={this.handleMouseMove}
                title={this.props.option.title}>

                <div className="h_ser_div"><a><span class={"add_access " + this.props.option.type + "-colr"}>{this.props.option.type}</span></a></div>
                <div className="h_ser_div_2">{this.props.option.value} - ID:<br />
                    {this.props.children}</div>
                <div className="h_ser_div_3"><span className={"icon icon-arrow-r " + this.props.option.type + '-arrow'}></span></div>
            </div>
        );
    }
});

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            permission: (getPermission() == undefined)? [] : JSON.parse(getPermission()),
        }
    }

    searchOnChange = (value, site_lookup) => {
        var state = {};
        //value.url
        window.location = value.url;
    }

    componentDidMount() {
        this.notificationAlert();
        if(this.state.permission.access_imail){
                this.imailNotification();
        }
    }

    imailNotification() {
        postData('admin/notification/get_imail_notification', {}).then((result) => {
            if (result.data) {
                this.props.imailnotificationalert(result.data);
            }
        });
    }

    notificationAlert() {
        postData('admin/notification/get_notification_alert', {}).then((result) => {
            if (result.data) {
                this.props.notificationalert(result);
            }
        });
    }
    
    imailCount = () => {
        var count = this.props.external_imail_count + this.props.internal_imail_count;
        if(count > 0)
        return <i>{count}</i>;
    }


    render() {
        return (
            <React.Fragment>
                <div className="overlay"></div>
                <WebsocketUserConnectionHandleData />
                <header>
                    <div className="container-fluid">
                        <div className="row d-flex flex-wrap after_before_remove header_m_view">
                            <div className="col-md-4 col-sm-3 col-xs-3 align-self-center">
                                <button title="Left Menu" type="button" onClick={() => this.props.notificationtoggle({LeftMenuOpen: ((this.props.LeftMenuOpen)? false: true), RightMenuOpen: false, NotificationType: ''})} data-placement="right" className="hamburger is-closed left-i" data-toggle="offcanvas">
                                    <span className="icon icon-menu menu_bar_icon"></span>
                                </button>
                            </div>
                            <div className="col-md-4  col-sm-6 col-xs-6">
                                <div className="header_search">
                                    <Select.Async
                                        cache={false}
                                        newOptionCreator={({ label: '', labelKey: '', valueKey: '' })}
                                        className="header_filter"
                                        name="form-field-name"
                                        clearable={false}
                                        ignoreCase={true}
                                        matchProp={'any'}
                                        filterOptions={selectFilterOptions}
                                        loadOptions={(e) => globalSearch(e)}
                                        optionComponent={GravatarOption}
                                        placeholder='Search'
                                        onChange={(e) => this.searchOnChange(e, 'site_lookup')}
                                        value={this.state.site_lookup}
                                    />
                                    <button className="but_search" type="submit">
                                        <i className="icon icon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-4 text-right right_icons  col-sm-3 col-xs-3">
                                <a className="right-i" href="#" data-placement="bottom" data-toggle="tooltip" title="Info">
                                    <span className="icon icon-info"></span>
                                </a>

                                {((this.props.RightMenuOpen && this.props.NotificationType == 'notification') ?

                                    <a className="" href="#" data-placement="bottom" data-toggle="tooltip" title="Notification">
                                        <span className="icon icon-notification-icons"></span>
                                    </a>
                                    :
                                    <a className="right-i" onClick={() => this.props.notificationtoggle({LeftMenuOpen: false, RightMenuOpen: true, NotificationType: 'notification'})} href="#" data-placement="bottom" data-toggle="tooltip" title="Notification">
                                        <span className="icon icon-notification-icons">{this.props.NotificationCount > 0? <i>{this.props.NotificationCount}</i>:'' }</span>
                                    </a>
                                )}

                                {this.state.permission.access_imail ? ((this.props.RightMenuOpen && this.props.NotificationType == 'imail') ?
                                    <button type="button" id="share" className="" data-toggle="offcanvas_new" title="Share" >
                                        <span className="icon icon-share close_right_menu"></span>
                                    </button>
                                    :
                                    <button type="button" id="share" className="hamburger_new is-closed right-i" data-toggle="offcanvas_new" title="Share" onClick={() => this.props.notificationtoggle({LeftMenuOpen: false, RightMenuOpen: true, NotificationType: 'imail' })}>
                                        <span className="icon icon-share close_right_menu">{this.imailCount()}</span>
                                    </button>
                                ): ''}

                            </div>
                        </div>
                    </div>
                </header>

            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    LeftMenuOpen : state.NotificationReducer.LeftMenuOpen,
    RightMenuOpen: state.NotificationReducer.RightMenuOpen,
    NotificationType: state.NotificationReducer.NotificationType,
    NotificationCount: state.NotificationReducer.NotificationCount,
    external_imail_count: state.NotificationReducer.external_imail_count,
    internal_imail_count: state.NotificationReducer.internal_imail_count,
})

const mapDispatchtoProps = (dispach) => {
    return {
        notificationtoggle: (object) => dispach(setNotificationToggel(object)),
        notificationalert: (object) => dispach(setNotificationAlert(object)),
        imailnotificationalert: (object) => dispach(setNotificationImailAlert(object))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Header);