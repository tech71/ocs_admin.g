import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import ScheduleNavigation from '../../admin/schedule/ScheduleNavigation';
import { checkItsNotLoggedIn, postData, changeTimeZone, handleDateChangeRaw } from '../../../service/common.js';
import moment from 'moment';
import { RosterDropdown, AnalysisDropdown, shiftTypeDepartmentOption } from '../../../dropdown/ScheduleDropdown.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Countdown from 'react-countdown-now';
import DatePicker from 'react-datepicker';
import ScheduleMenu from './ScheduleMenu';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ScheduleHistory from './ScheduleHistory';
import {TotalShowOnTable} from '../../../service/TotalShowOnTable';
import { connect } from 'react-redux'
import SchedulePage from './SchedulePage';
import Pagination from "../../../service/Pagination.js";
import {SchedulePageIconTitle, ParticiapntPageIconTitle} from 'menujson/pagetitle_json';


const requestData = (pageSize, page, sorted, filtered, pageType) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, pageType: pageType};
        postData('schedule/ScheduleListing/get_unconfirmed_and_quote_shifts', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_count: (result.total_count),
            };
            resolve(res);
        });

    });
};


class ScheduleUnconfirmed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            roster: 'roster',
            analysis: 'analysis',
            loading: false,
            shiftListing: [],
            counter: 0,
            selected: [],
            selectAll: 0,
            start_date: '',
            end_date: '',
        }
        
        this.pageType = this.props.props.match.params.page;

    }

    fetchData = (state, pageType) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            pageType
        ).then(res => {
            this.setState({
                shiftListing: res.rows,
                pages: res.pages,
                total_count: res.total_count,
                loading: false,
                selectAll: 0,
                userSelectedList: [],
                selected: []
            });
        });
    }

 

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll = () => {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.shiftListing.forEach(x => {
                newSelected[x.id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = { search_box: this.state.search_box, shift_type: this.state.shift_type, shift_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date}
            this.setState({ filtered: filter });
        });
    }

    closeHistory = () => {
        this.setState({ open_history: false })
    }
    
    changePanelTable = (pageType) => {
         this.setState({search_box: ''});
         var state = { pageSize: 10, page: 0, sorted: [], filtered: []}
         this.fetchData(state, pageType);
    }
    
    reFreashReactTable = () => {
        var ReactOption = this.reactTable.current.state;
        var state = { pageSize: ReactOption.pageSize, page: ReactOption.page, sorted: ReactOption.sorted, filtered: ReactOption.filtered}
        this.fetchData(state, this.pageType);
    }
    
    componentWillReceiveProps(newProps){
        if(this.props.props.match.params.page != newProps.props.match.params.page){
            this.changePanelTable(newProps.props.match.params.page);
        }
    }

    render() {
        this.pageType = this.props.props.match.params.page;
         
        const columns = [{
            id: "checkbox", accessor: "",
            Cell: ({ original }) => {
                return (
                  <span className="Set_Sec___ pl-2">
                  <label class="Cus_Check_1">
                      <input type="checkbox" checked={this.state.selected[original.id] === true} onChange={() => this.toggleRow(original.id)}/>
                      <div class="chk_Labs_1"></div>
                  </label>
                  </span>
                );
            },
            Header: x => {
                return (

                    <span className="Set_Sec___ pl-2">
                    <label class={"Cus_Check_1 "+(this.state.selectAll === 1?"minus_select__":'')}>
                    <input type='checkbox' checked={this.state.selectAll === 1} ref={input => {
                            if(input){input.indeterminate = this.state.selectAll === 2}
                        }}
                            onChange={() => this.toggleSelectAll()} />
                        <div class="chk_Labs_1"></div>
                    </label>
                    </span>

                  

                );
            },
            sortable: false,

        },
        { Header: 'ID', accessor: 'id', filterable: false, },
        { Header: 'Date', accessor: 'shift_date', filterable: false, Cell: props => <span>{changeTimeZone(props.original.shift_date, "DD/MM/YYYY")}</span> },
        { Header: 'For', accessor: 'participantName', filterable: false, },
        { Header: 'Start', accessor: 'start_time', filterable: false, Cell: props => <span>{changeTimeZone(props.original.start_time, 'LT')}</span> },
        { Header: 'Duration', accessor: 'duration', filterable: false, },
        {
            Header: 'Suburb', accessor: '', sortable: false, filterable: false,
            Cell: props => <span>
                {(props.original.address.length > 0) ? props.original.address[0].suburb : "N/A"}

            </span>
        },
        {
            Cell: (props) => <span className="action_ix__"><i title={ParticiapntPageIconTitle.par_history_icon} onClick={() => this.setState({ historyId: props.original.id, open_history: true })} className="icon icon-pending-icons icon_h-1 mr-2"></i>
            <Link title={ParticiapntPageIconTitle.par_view_icon} to={{ pathname: '/admin/schedule/details/' + props.original.id, state: this.props.props.location.pathname }}><i className="icon icon-views"></i></Link>
            </span>, 
            Header: <div className="">Action</div>, style: {
                "textAlign": "right",
            }, 
            headerStyle: { border: "0px solid #fff" },
            Header:<TotalShowOnTable countData={this.state.total_count} />,
            sortable: false
        },
        {
            expander: true, sortable: false,
            Expander: ({ isExpanded, ...rest }) =>
                <div>{isExpanded ? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
            headerStyle: { border: "0px solid #fff" },

        }
        ]



        return (
           <React.Fragment>
                        <ScheduleMenu back_url={'/admin/dashboard'} default={true} />
                        <SchedulePage pageTypeParms={this.props.props.match.params.page}/>


                        <div className="row">
                            <div className="col-lg-12 col-sm-12">
                                
                                
                                <div className="tab-content">

                                    <div role="tabpanel" className="tab-pane active" id={this.pageType}>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="row">
                                                    <div className="col-md-12 P_7_TB"><h3>Shifts:</h3></div>
                                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row P_25_T d-flex flex-wrap after_before_remove">
                                            <div className="col-md-4 col-sm-8 align-self-end">
                                                <label>Search</label>
                                                <div className="table_search_new">
                                                    <input type="text" onChange={(e) => this.searchBox('search_box', e.target.value)} name="" value={this.state.search_box || ''} />
                                                    <button type="submit">
                                                        <span className="icon icon-search"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div className="col-md-3 col-sm-4 align-self-end">
                                                <label></label>
                                                <div className="box">
                                                     <Select clearable={false} name="shift_type" simpleValue={true} searchable={false} onChange={(e) => this.searchBox('shift_type', e)}
                                                        options={shiftTypeDepartmentOption(0)} value={this.state.shift_type} placeholder="Shift Type/Department" />

                                                </div>
                                            </div>

                                            <div className="col-md-5 col-sm-12 align-self-end">
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <label>On</label>
                                                        <DatePicker autoComplete={'off'} isClearable={true} onChangeRaw={handleDateChangeRaw} utcOffset={0} name="shift_date" onChange={(e) => this.searchBox('shift_date', e)} selected={this.state['shift_date'] ? moment(this.state['shift_date'], 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY" className="text-center px-0" placeholderText="00/00/0000" />
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <label>From</label>
                                                        <DatePicker autoComplete={'off'} minDate={moment()} onChangeRaw={handleDateChangeRaw} utcOffset={0} isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                                                    </div>
                                                    <div className="col-sm-4">
                                                        <label>To</label>
                                                        <DatePicker autoComplete={'off'} minDate={moment()} onChangeRaw={handleDateChangeRaw} utcOffset={0} isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div className="row">
                                            <div className="col-md-12 schedule_listings">
                                                <ReactTable
                                                  PaginationComponent={Pagination}
                                                    columns={columns}
                                                    manual
                                                    data={this.state.shiftListing}
                                                    pages={this.state.pages}
                                                    loading={this.state.loading}
                                                    onFetchData={(state) => this.fetchData(state, this.pageType)}
                                                    filtered={this.state.filtered}
                                                    defaultPageSize={10}
                                                    className="-striped -highlight"
                                                    noDataText="No Record Found"
                                                    minRows={2}

                                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                                    SubComponent={(props) => <div className="other_conter"><div className="col-md-6">
                                                        <ul>
                                                            <li><span>End: </span> {changeTimeZone(props.original.end_time, 'LT')}</li>
                                                            {props.original.address.map((site, id) => (
                                                                <li key={id + 1}><span className="color">Site: </span>{site.site}</li>
                                                            ))}

                                                            {props.original.memberName.map((memberName, id) => (
                                                                <li key={id + 1}><span className="color">Allocated To: </span>{memberName.memberName} <span>{(memberName.preferred == 1) ? '- Preferred' : ''}</span></li>
                                                            ))}
                                                        </ul>
                                                    </div>
                                                        <div className="col-md-6 text-right">
                                                            <ul>
                                                                <li><span className="color">Expenses: </span>{props.original.expenses ? '$' + props.original.expenses : 'N/A'}</li>
                                                                <li><span className="color">KMs: </span>15 km</li>
                                                                <li><span className="start_in_color">Start In: </span><Countdown date={Date.now() + props.original.diff} /></li>
                                                            </ul>
                                                        </div>
                                                    </div>}
                                                />
                                                <ScheduleHistory open_history={this.state.open_history} shiftId={this.state.historyId} closeHistory={this.closeHistory} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>



                 </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.ScheduleDetailsData.activePage.pageTitle,
    showTypePage: state.ScheduleDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(ScheduleUnconfirmed);
