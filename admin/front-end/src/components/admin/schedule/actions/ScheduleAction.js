import {scheduleActiveTitle} from 'menujson/schedule_menu_json';
export const setShiftDetails = (detailsData) => ({
        type: 'set_schedule_shfits_details_data',
        detailsData
    });
export const setRosterDetails = (detailsData) => ({
    type: 'set_schedule_roster_details_data',
    detailsData
});
export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_schedule',
        value
}}

    
export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =scheduleActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}