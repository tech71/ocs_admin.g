import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import ScheduleNavigation from '../../admin/schedule/ScheduleNavigation';
import {
    checkItsNotLoggedIn, changeTimeZone, postData, handleDateChangeRaw, archiveALL, handleShareholderNameChange,
    handleAddShareholder, handleRemoveShareholder, handleCheckboxValue, getOptionsSuburb, getOptionsMember
} from '../../../service/common.js';
import moment from 'moment';
import { BASE_URL } from '../../../config.js';
import { RosterDropdown, AnalysisDropdown, confirmBy, availableMemberLookup, availableMemberMatches, cancelShiftMethod, cancelShiftWhoOption } from '../../../dropdown/ScheduleDropdown.js';
import Select, { Async } from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Countdown from 'react-countdown-now';
import Modal from 'react-bootstrap/lib/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import jQuery from "jquery";
import DatePicker from 'react-datepicker';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { custNumberLine, customHeading } from '../../../service/CustomContentLoader.js';

import MemberLookUpPopUp from './MemberLookUpPopUp';
import ManualMemberLookUp from './ManualMemberLookUp';
import { connect } from 'react-redux'
import { setShiftDetails } from './actions/ScheduleAction';
import SchedulePage from './SchedulePage';
import { SchedulePageIconTitle, ParticiapntPageIconTitle } from 'menujson/pagetitle_json';
import { ToastUndo } from 'service/ToastUndo.js'
import ScrollArea from "react-scrollbar";

class ScheduleDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: [],
            shift_participant: [],
            confirmation_details: [],
            shift_caller: [],
            back_url: this.props.props.location.state
        }
    }

    getShiftDetails = (ShiftId) => {
        this.setState({ loading: true })
        postData('schedule/ScheduleDashboard/get_shift_details', { id: ShiftId }).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState(details);
                this.props.setShiftDetails(details);
                this.setState({ loading: false });
            }
        });

    }

    componentDidMount() {
        this.getShiftDetails(this.props.props.match.params.id);
    }

    render() {
        return (
            <React.Fragment>
                <SchedulePage pageTypeParms={'schedule_details'} />


                <div className="row  _Common_back_a">
                    <div className="col-lg-12 col-md-12">
                        <Link className="d-inline-flex" to={((this.props.props.location.state) ? this.props.props.location.state : '/admin/schedule/unfilled/unfilled')}><div className="icon icon-back-arrow back_arrow"></div></Link>
                    </div>
                </div>
                <div className="row"><div className="col-lg-12 col-md-12"><div className="bor_T"></div></div></div>


                <div className="row">
                    <div className="col-lg-12 col-sm-12 P_15_TB text-center">

                        <h1 className="color">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20, 'center')} ready={!this.state.loading}>
                                Shift ID<span> - {this.state.id}</span>
                            </ReactPlaceholder>
                        </h1>

                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(60, 'center')} ready={!this.state.loading}>
                            <ul className="user_info P_15_T">
                                <li>Date: <span>{changeTimeZone(this.state.shift_date)}</span></li>
                                {(this.state.booked_by == 1) ? <li> Site: <span>{this.state.shift_organiztion_site.length > 0 ? this.state.shift_organiztion_site[0].site_name : "N/A"}</span></li> :
                                    <li> Participant: <span>{this.state.shift_participant.map((participant, index) => (
                                        <span key={index + 1} >{participant.participantName + ((this.state.shift_participant.lenght < index) ? ', ' : '')}</span>
                                    ))}</span></li>}
                                <li> Start:  <span>{changeTimeZone(this.state.start_time, "LT")}</span></li>
                                <li> Duration: <span>{this.state.duration}</span></li>
                                <li> End: <span>{changeTimeZone(this.state.end_time, "LT")}</span></li>
                                <li> Suburb: <span>{(this.state.location.length > 0) ? this.state.location[0].suburb.label : 'N/A'}</span></li>
                            </ul>
                        </ReactPlaceholder>
                        {(this.state.status == 7) ? 'Confirmed' : ''}
                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20, 'center')} ready={!this.state.loading}>
                            <h2 className="shift_start_heading pt-3">{(this.state.status == 7) ? <span className="g_heading_color">Confirmed - </span> : ''}<span>Starts In:</span> <span className="color_d"> <Countdown date={moment() + this.state.diff} /></span> </h2>
                        </ReactPlaceholder>
                    </div>

                    <div className="col-lg-12 col-md-12"><div className="bor_T"></div></div>
                </div>

                <div className="row">
                    <div className="col-lg-2 col-md-0"></div>
                    <ul className="nav nav-tabs Category_tap col-lg-8 col-sm-12 P_20_TB" role="tablist">
                        <li role="presentation" className="col-lg-3 col-sm-3 col-xs-12 active">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(70)} ready={!this.state.loading}>
                                <a href="#Shift_Details" aria-controls="Shift_Details" role="tab" data-toggle="tab">Shift Details</a>
                            </ReactPlaceholder>
                        </li>
                        <li role="presentation" className="col-lg-3 col-sm-3 col-xs-12">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(70)} ready={!this.state.loading}>
                                <a href="#booking_details" aria-controls="booking_details" role="tab" data-toggle="tab">Booking Details</a>
                            </ReactPlaceholder>
                        </li>
                        <li role="presentation" className="col-lg-3 col-sm-3 col-xs-12">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(70)} ready={!this.state.loading}>
                                <a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Notes</a>
                            </ReactPlaceholder>
                        </li>
                        <li role="presentation" className="col-lg-3 col-sm-3 col-xs-12">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(70)} ready={!this.state.loading}>
                                <a href="#confirmation" aria-controls="confirmation" role="tab" data-toggle="tab">Confirmation Details</a>
                            </ReactPlaceholder>
                        </li>
                    </ul>

                    <div className="col-lg-3"></div>

                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                </div>


                <div className="row">
                    <div className="tab-content">
                        <ShiftDetais loading={this.state.loading} details={this.state} getShiftDetails={this.getShiftDetails} shiftId={this.props.props.match.params.id} />
                        <BookingDetais loading={this.state.loading} shift_caller={this.state.shift_caller} created={this.state.created} back_url={this.state.back_url} />
                        <Notes shiftId={this.props.props.match.params.id} back_url={this.state.back_url} />
                        <ConfirmationDetails shift_status={this.state.status} loading={this.state.loading} allocatedDeatils={this.state.allocated_member} getShiftDetails={this.getShiftDetails} confirmation_details={this.state.confirmation_details} shiftId={this.props.props.match.params.id} back_url={this.state.back_url} />
                    </div>
                </div>

            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.MemberReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setShiftDetails: (result) => dispach(setShiftDetails(result))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(ScheduleDetails)


class ShiftDetais extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shift_participant: [],
            location: [],
            preferred_member: [],
            shift_organiztion_site: [],
            modal_show: false,
            allocated_member: [],
            requirement: []
        }
    }


    componentWillReceiveProps(newProps) {
        this.setState(newProps.details);
    }

    closeModel = (type, mode) => {
        var state = {};
        state['modal_show_' + type] = false;
        this.setState(state);
        if (mode) {
            this.props.getShiftDetails(this.props.shiftId);
        }
    }

    render() {
        return (
            <div role="tabpanel" className="tab-pane active" id="Shift_Details">
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-md-12 text-center py-3"><ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(20, 'center')} ready={!this.state.loading}><h2>All Shift Details:</h2></ReactPlaceholder></div>
                        <div className="col-md-12"><div className="bor_T"></div></div>
                    </div>
                </div>
                <div className="col-lg-12">
                    <div className="row d-flex flex-wrap justify-content-center after_before_remove">
                        <div className="col-lg-9 col-md-9 pt-5">
                            <ReactPlaceholder showLoadingAnimation={true} type='textRow' customPlaceholder={custNumberLine(8)} ready={!this.props.loading}>

                                <div className="list_shift_AZ__">

                                    <div className="tr_list">
                                        <div className="td_list"><span className="color">Date: </span>{changeTimeZone(this.state.shift_date)}</div>
                                        {this.state.status != 7 ? <div className="td_list"> <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_date_time: true })}>
                                            <i className="icon icon-done-arrow"></i></button>
                                        </div> : ''}
                                    </div>
                                    {(this.state.booked_by == 1) ?
                                        <div className="tr_list">
                                            <div className="td_list"><span className="color">Site: </span> {this.state.shift_organiztion_site.length > 0 ? this.state.shift_organiztion_site[0].site_name : "N/A"}</div>
                                            <div className="td_list"></div>
                                        </div> :
                                        <div className="tr_list">
                                            <div className="td_list"><span className="color">Participant: </span> {this.state.shift_participant.map((participant, index) => (
                                                <span key={index + 1} >{participant.participantName + ((this.state.shift_participant.lenght < index) ? ', ' : '')}</span>
                                            ))}</div>
                                            <div></div>
                                        </div>
                                    }
                                    <div className="tr_list">
                                        <div className="td_list"><span className="color">Start: </span>{changeTimeZone(this.state.start_time, "LT")}</div>
                                        {this.state.status != 7 ? <div className="td_list">
                                            <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_date_time: true })} data-toggle="modal" data-target="#modal_1">
                                                <i className="icon icon-done-arrow"></i></button>
                                        </div> : ''}
                                    </div>
                                    <div className="tr_list">
                                        <div className="td_list"><span className="color">Duration: </span>{this.state.duration}</div>
                                        <div className="td_list"></div>
                                    </div>
                                    <div className="tr_list">
                                        <div className="td_list"><span className="color">End: </span>{changeTimeZone(this.state.end_time, "LT")}</div>
                                        {this.state.status != 7 ? <div className="td_list">
                                            <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_date_time: true })}>
                                                <i className="icon icon-done-arrow"></i></button>
                                        </div> : ''}
                                    </div>
                                    {this.state.location.map((address, index) => (
                                        <div className="tr_list" key={index + 1}>
                                            <div className="td_list"><span className="color">Location: </span> {address.site}</div>
                                            <div className="td_list">
                                                <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_address: true })}>
                                                    <i className="icon icon-done-arrow"></i></button>
                                            </div>
                                        </div>
                                    ))}
                                    <div className="tr_list">
                                        <div className="td_list"><span className="color">Requirements: </span>{this.state.shift_requirement}</div>
                                        <div className="td_list">
                                            <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_requirement: true })}>
                                                <i className="icon icon-done-arrow"></i></button>
                                        </div>
                                    </div>

                                    {this.state.preferred_member.map((member, index) => (
                                        <div className="tr_list" key={index + 1}>
                                            <div className="td_list"><span className="color">Prefered Member: </span>{member.memberName}</div>
                                            <div className="td_list">
                                                <button title={ParticiapntPageIconTitle.par_update_icon} className="default_but_remove" onClick={() => this.setState({ modal_show_preffered_member: true })}>
                                                    <i className="icon icon-done-arrow"></i></button>
                                            </div>
                                        </div>
                                    ))}

                                    {(this.state.allocated_member.length > 0 && this.state.status == 2) ?
                                        this.state.allocated_member.map((member, index) => (
                                            <div className="tr_list" key={index + 1}>
                                                <div className="td_list"><span className="color">Allocated To: </span>{member.memberName}  {(member.preferred == 1) ? '- Preferred' : ''}</div>
                                                <div className="td_list">
                                                    <button className="default_but_remove" onClick={() => this.setState({ modal_show_member_lookUp: true })}>
                                                        <i className="icon icon-circule color"></i></button>
                                                </div>
                                            </div>))
                                        : ''
                                    }

                                    {(this.state.status == 7) ?
                                        this.state.allocated_member.map((member, index) => (
                                            <div className="tr_list" key={index + 1}>
                                                <div className="td_list"><span className="color">Confirm By: </span>{member.memberName}  {(member.preferred == 1) ? '- Preferred' : ''}</div>
                                                <div className="td_list"></div>
                                            </div>)) : ''
                                    }

                                    {(this.state.status == 4) ?
                                        this.state.rejected_data.map((member, index) => (
                                            <div className="tr_list" key={index + 1}>
                                                <div className="td_list"><span className="color">Rejected By: </span>{member.memberName}</div>
                                                <div className="td_list"></div>
                                            </div>)) : ''
                                    }

                                    {(this.state.status == 5) ?
                                        this.state.cancel_data.map((val, index) => (

                                            <div ><div className="tr_list" key={index + 1}>
                                                <div className="td_list"><span className="color">Cancel By: </span>{val.cancel_by}</div>
                                                <div className="td_list"></div>
                                            </div>
                                                <div className="tr_list" key={index + 1}>
                                                    <div className="td_list"><span className="color">Cancel Reason: </span>{val.reason}</div>
                                                    <div className="td_list"></div>
                                                </div></div>)) : ''
                                    }


                                </div>

                                {/* 
                        <table className="w-100 radius_first_last tr_between shifts_deetails">
                            <tbody>

                                <tr>
                                    <td className="py-2 px-4"><span className="color">Date: </span>{changeTimeZone(this.state.shift_date)}</td>
                                    {this.state.status != 7 ? <td className="P_7_T px-3 text-right">
                                        <button onClick={() => this.setState({ modal_show_date_time: true })} className="default_but_remove">
                                            <i className="icon icon-done-arrow update_button"></i></button>
                                    </td> : ''}
                                </tr>
                                {(this.state.booked_by == 1) ?
                                    <tr>
                                        <td className="py-2 px-4"><span className="color">Site: </span> {this.state.shift_organiztion_site[0].site_name}</td>
                                        <td className="P_7_T px-3 text-right">
                                        </td>
                                    </tr> :
                                    <tr>
                                        <td className="py-2 px-4"><span className="color">Participant: </span> {this.state.shift_participant.map((participant, index) => (
                                            <span key={index + 1} >{participant.participantName + ((this.state.shift_participant.lenght < index) ? ', ' : '')}</span>
                                        ))}</td>
                                        <td className="P_7_T px-3 text-right">
                                        </td>
                                    </tr>
                                }
                                <tr>
                                    <td className="py-2 px-4"><span className="color">Start: </span>{changeTimeZone(this.state.start_time, "LT")}</td>
                                    {this.state.status != 7 ? <td className="P_7_T px-3 text-right">
                                        <button onClick={() => this.setState({ modal_show_date_time: true })} className="default_but_remove" data-toggle="modal" data-target="#modal_1">
                                            <i className="icon icon-done-arrow update_button"></i></button>
                                    </td> : ''}
                                </tr>
                                <tr>
                                    <td className="py-2 px-4"><span className="color">Duration: </span>{this.state.duration}</td>
                                    <td className="P_7_T px-3 text-right">
                                    </td>
                                </tr>
                                <tr>
                                    <td className="py-2 px-4"><span className="color">End: </span>{changeTimeZone(this.state.end_time, "LT")}</td>
                                    {this.state.status != 7 ? <td className="P_7_T px-3 text-right">
                                        <button onClick={() => this.setState({ modal_show_date_time: true })} className="default_but_remove">
                                            <i className="icon icon-done-arrow update_button"></i></button>
                                    </td> : ''}
                                </tr>
                                {this.state.location.map((address, index) => (
                                    <tr key={index + 1}>
                                        <td className="py-2 px-4"><span className="color">Location: </span> {address.site}</td>
                                        <td className="P_7_T px-3 text-right">
                                            <button onClick={() => this.setState({ modal_show_address: true })} className="default_but_remove">
                                                <i className="icon icon-done-arrow update_button"></i></button>
                                        </td>
                                    </tr>
                                ))}
                                <tr>
                                    <td className="py-2 px-4"><span className="color">Requirements: </span>{this.state.shift_requirement}</td>
                                    <td className="P_7_T px-3 text-right">
                                        <button onClick={() => this.setState({ modal_show_requirement: true })} className="default_but_remove" >
                                            <i className="icon icon-done-arrow update_button"></i></button>
                                    </td>
                                </tr>

                                {this.state.preferred_member.map((member, index) => (
                                    <tr key={index + 1}>
                                        <td className="py-2 px-4"><span className="color">Prefered Member: </span>{member.memberName}</td>
                                        <td className="P_7_T px-3 text-right">
                                            <button onClick={() => this.setState({ modal_show_preffered_member: true })} className="default_but_remove">
                                                <i className="icon icon-done-arrow update_button"></i></button>
                                        </td>
                                    </tr>
                                ))}

                                {(this.state.allocated_member.length > 0 && this.state.status == 2) ?
                                    this.state.allocated_member.map((member, index) => (
                                        <tr key={index + 1}>
                                            <td className="py-2 px-4"><span className="color">Allocated To: </span>{member.memberName}  {(member.preferred == 1) ? '- Preferred' : ''}</td>
                                            <td className="px-3 text-right">
                                                <button onClick={() => this.setState({ modal_show_member_lookUp: true })} className="default_but_remove cbtn_i"><i className="icon icon-circule color"></i></button>
                                            </td>
                                        </tr>))
                                    : ''
                                }

                                {(this.state.status == 7) ?
                                    this.state.allocated_member.map((member, index) => (
                                        <tr key={index + 1}>
                                            <td className="py-2 px-4"><span className="color">Confirm By: </span>{member.memberName}  {(member.preferred == 1) ? '- Preferred' : ''}</td>
                                            <td className="P_7_T px-3 text-right">
                                            </td>
                                        </tr>)) : ''
                                }

                                {(this.state.status == 4) ?
                                    this.state.rejected_data.map((member, index) => (
                                        <tr key={index + 1}>
                                            <td className="py-2 px-4"><span className="color">Rejected By: </span>{member.memberName}</td>
                                            <td className="P_7_T px-3 text-right">
                                            </td>
                                        </tr>)) : ''
                                }


                            </tbody>
                            {(this.state.status == 5) ?
                                this.state.cancel_data.map((val, index) => (
                                    <tfoot><tr key={index + 1}>
                                        <td className="py-2 px-4"><span className="color">Cancel By: </span>{val.cancel_by}</td>
                                        <td className="P_7_T px-3 text-right">
                                        </td>
                                    </tr>
                                        <tr key={index + 1}>
                                            <td className="py-2 px-4"><span className="color">Cancel Reason: </span>{val.reason}</td>
                                            <td className="P_7_T px-3 text-right">
                                            </td>
                                        </tr></tfoot>)) : ''
                            }</table> */}
                            </ReactPlaceholder>
                        </div>
                    </div>

                </div>
                <div className="col-lg-12">
                    <div className="row d-flex flex-wrap justify-content-center after_before_remove">
                        <div className="col-lg-9 col-md-9 mt-5">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(40)} ready={!this.props.loading}>
                                <div className="row">
                                    <div className="col-lg-3 col-md-3 ">{(this.state.status == 5) ? "" : <button className="but_submit but_cancel" onClick={() => this.setState({ modal_show_cancel_shift: true })}>Cancel Shift</button>}</div>

                                </div>
                            </ReactPlaceholder>
                        </div>
                    </div>
                </div>



                <MemberLookUpPopUp modal_show={this.state.modal_show_member_lookUp} closeModel={this.closeModel} shiftId={this.state.id}
                    shift_date={this.state.start_time} start_time={this.state.start_time}
                    shift_organiztion_site={this.state.shift_organiztion_site} participant_data={this.state.shift_participant}
                    booked_by={this.state.booked_by} LookUpttitle={this.state.LookUpttitle} manual_assign={this.state.manual_assign} />



                <UpdateShiftPrefferedMember modal_show={this.state.modal_show_preffered_member} closeModel={this.closeModel} shiftId={this.state.id} preferred_members={this.state.preferred_member} />
                <UpdateShiftRequirement modal_show={this.state.modal_show_requirement} closeModel={this.closeModel} requirement={this.state.requirement} shiftId={this.state.id} />
                <UpdateAddress modal_show={this.state.modal_show_address} closeModel={this.closeModel} location={this.state.location} shiftId={this.state.id} />
                <UpdateShiftDateTIme modal_show={this.state.modal_show_date_time} closeModel={this.closeModel} shiftId={this.state.id} shift_date={this.state.start_time} start_time={changeTimeZone(this.state.start_time, "YYYY-MM-DDTHH:mm:ss", true)} end_time={changeTimeZone(this.state.end_time, "YYYY-MM-DDTHH:mm:ss", true)} />
                <CancelShift modal_show={this.state.modal_show_cancel_shift} status={this.state.status} closeModel={this.closeModel} booked_by={this.state.booked_by} shiftId={this.state.id} participantName={(this.state.shift_participant.length > 0) ? this.state.shift_participant[0].firstname : ''} />
            </div>
        )
    }
}

class BookingDetais extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shift_caller: {},
            confirmation_details: {}
        }
    }
    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    bookedWay = (val) => {
        if (this.state.shift_caller.booking_method) {
            if (this.state.shift_caller.booking_method == 1) {
                return <span>- <u>{this.state.shift_caller.phone}</u><button className="default_but_remove ml-2"> <span className="icon icon-caller-icons call_icon"></span></button></span>;
            } else if (this.state.shift_caller.booking_method == 2) {
                return <span>- <u>{this.state.shift_caller.email}</u></span>;
            }
        }
    }
    render() {
        return (
            <div role="tabpanel" className="tab-pane" id="booking_details">
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-md-12 text-center py-3"><h2>Booking Details:</h2></div>
                        <div className="col-md-12"><div className="bor_T"></div></div>
                    </div>
                </div>

                <div className="col-lg-4 col-lg-offset-4 col-sm-4 col-sm-offset-4">
                    <div className="booking_details_parent pt-5">
                        <div className="py-3 dotted_line text_child"><span className="color">Booked By: </span>{(this.state.shift_caller.firstname) ? (this.state.shift_caller.firstname + ' ' + this.state.shift_caller.lastname) : 'N/A'}</div>
                        <div className="py-3 dotted_line text_child"><span className="color">Booked On: </span>{changeTimeZone(this.state.created)}</div>
                        <div className="py-3 text_child"><span className="color">Booking Method:</span> {this.state.shift_caller.booking_method ? confirmBy(this.state.shift_caller.booking_method) : 'N/A'} {this.bookedWay(this.state.shift_caller.booking_method)}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Notes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: []
        }
    }

    getShifNotes() {
        postData('schedule/ScheduleDashboard/get_shift_notes', { shiftId: this.props.shiftId }).then((result) => {
            if (result.status) {
                this.setState({ notes: result.data });
            }
        });
    }

    arhchiveNote = (id, index) => {
        var loges = { userId: this.props.shiftId, note: 'Archive notes: Shift Id ' + this.props.shiftId, module: 4 }
        archiveALL({ id: id }, '', 'schedule/Shift_Schedule/archive_shift_notes').then((result) => {
            if (result.status) {
                var state = {}
                state['notes'] = this.state.notes.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        jQuery('#create_notes').validate({});
        if (jQuery('#create_notes').valid()) {

            this.setState({ loading: true }, () => {
                var data = { id: this.state.id, title: this.state.title, notes: this.state.content, shiftId: this.props.shiftId };

                postData('schedule/ScheduleDashboard/create_shift_notes', data).then((result) => {
                    if (result.status) {
                        this.setState({ modal_show: false });
                        this.getShifNotes();
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }
    componentDidMount() {
        this.getShifNotes();
    }

    render() {
        return (
            <div role="tabpanel" className="tab-pane" id="notes">
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-lg-12 text-center py-3"><h2>Internal Shift Notes:</h2></div>
                        <div className="col-lg-12"><div className="bor_T"></div></div>
                    </div>
                </div>

                <div className="col-lg-12">
                    <div className="row d-flex flex-wrap justify-content-center after_before_remove">
                        <div className="col-lg-9 col-sm-12">

                            {(this.state.notes.length > 0) ? this.state.notes.map((note, id) => (
                                <div className="internal_shift_parent mt-4" key={id + 1}>
                                    <div className="row d-flex flex-wrap">
                                        <div className="col-lg-4 flex_wrap br-1">
                                            <div className="w-100 align-self-start">
                                                <h6 className="px-4 py-3"><span className="color">User:</span> {note.adminId}</h6>
                                            </div>
                                            <div className="w-100 align-self-center color">
                                                <div className="font_w_4 px-4 py-2">{note.title}</div>
                                            </div>
                                            <div className="w-100 align-self-end">
                                                <h6 className="px-4 py-3"><span className="color">Date:</span> {note.created}</h6>
                                            </div>
                                        </div>
                                        <div className="col-lg-8 flex_wrap">
                                            <div className="w-100 align-self-start">
                                                <div className="font_w_4 px-4 pt-3">{note.notes}</div>
                                            </div>
                                            <div className="w-100 align-self-end text-right px-4 pt-1 pb-2">
                                                <button onClick={() => this.arhchiveNote(note.id, id)} className="default_but_remove pr-2 mr-2"><i className="icon icon-email-pending archive_Icon"></i></button>
                                                <button onClick={() => this.setState({ id: note.id, title: note.title, content: note.notes, modal_show: true })} className="default_but_remove"><i className="icon icon-update upda_Icon"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )) : <div className="internal_shift_parent mt-4" >
                                    <div className=" py-3 px-3 text-center font_w_4">
                                        No Notes Found
                                </div>
                                </div>
                            }
                            <div className="w-100 pt-5 text-right">
                                <button title={SchedulePageIconTitle.internal_shift_notes} onClick={() => this.setState({ modal_show: true, id: '', title: '', content: '' })} className="button_plus__"><i className="icon icon-add-icons Add-2-1"></i></button>
                            </div>

                            <div className="row">
                                <div className="col-lg-3 col-lg-offset-9 col-sm-3 mt-5">
                                    <Link to={((this.props.back_url) ? this.props.back_url : '/admin/schedule/unfilled/unfilled')} className="but_submit w-100 d-block text-center">Return Previous</Link>
                                </div>
                            </div>

                            <Modal
                                className="modal fade Modal_A Modal_B"
                                show={this.state.modal_show}
                                onHide={this.handleHide}
                                container={this}
                                aria-labelledby="contained-modal-title"
                            >
                                <Modal.Body>
                                    <form onSubmit={this.onSubmit.bind(this)} id="create_notes">

                                        <div className="text text-left Popup_h_er_1">
                                            <span>Internal Shift Notes</span>
                                            <a type="button" className="close_i"><i onClick={() => this.setState({ modal_show: false })} className="icon icon-cross-icons"></i></a>
                                        </div>

                                        <div className="row P_15_T">
                                            <div className="col-md-5">
                                                <label>Title:</label>
                                                <span className="required">
                                                    <input type="text" name="title" onChange={(e) => this.setState({ title: e.target.value })} value={this.state['title'] || ''} data-rule-required="true" />
                                                </span>
                                            </div>
                                            <div className="col-md-5">
                                            </div>
                                        </div>

                                        <div className="row P_15_T">
                                            <div className="col-md-12">
                                                <label>Notes:</label>
                                                <span className="required">
                                                    <textarea className="col-md-12 min-h-120 textarea-max-size" onChange={(e) => this.setState({ content: e.target.value })} name="content" value={this.state['content'] || ''} data-rule-required="true" data-rule-maxlength="500" ></textarea>
                                                </span>

                                            </div>

                                        </div>

                                        <div className="row P_15_T">
                                            <div className="col-md-7"></div>
                                            <div className="col-md-5">
                                                <input disabled={this.state.loading} type="submit" className="but" value={'Save'} name="content" />
                                            </div>
                                        </div>

                                    </form>
                                </Modal.Body>
                            </Modal>
                        </div>
                    </div>
                </div>


            </div>
        )
    }
}

class ConfirmationDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allocatedDeatils: [{ memberName: 'N/A', allocate_on: '' }],
            confirmation_details: {}
        }
    }
    componentWillReceiveProps(newProps) {
        if (newProps.allocatedDeatils) {
            this.setState({ allocatedDeatils: newProps.allocatedDeatils });
        }
        this.setState({ confirmation_details: newProps.confirmation_details });
    }

    callBooker = () => {
        postData('schedule/ScheduleDashboard/call_booker', { shiftId: this.props.shiftId }).then((result) => {
            if (result.status) {
                this.props.getShiftDetails(this.props.shiftId);
            }
        });
    }

    callAllocated = () => {
        postData('schedule/ScheduleDashboard/call_allocated', { shiftId: this.props.shiftId }).then((result) => {
            if (result.status) {
                this.props.getShiftDetails(this.props.shiftId);
            }
        });
    }

    bookedWay = (val) => {
        if (val) {
            if (val == 1) {
                return <span>- <u>{this.state.confirmation_details.phone}</u><button className="default_but_remove ml-2"> <span className="icon icon-caller-icons call_icon"></span></button></span>;
            } else if (val == 2) {
                return <span>- <u>{this.state.confirmation_details.email}</u></span>;
            }
        }
    }

    render() {
        console.log(this.props, 'this.props');
        return (
            <div role="tabpanel" className="tab-pane" id="confirmation">
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-lg-12 text-center py-3"><h2>Confirmation Details</h2></div>
                        <div className="col-lg-12"><div className="bor_T"></div></div>
                    </div>
                </div>
                <div className="col-lg-12">
                    <div className="row d-flex flex-wrap justify-content-center after_before_remove">
                        <div className="col-lg-5 col-sm-6 ">
                            <div className="booking_details_parent pt-5">
                                {this.state.allocatedDeatils.map((allocate, index) => (<div key={index + 1}>
                                    <div className="py-3 dotted_line text_child"><span className="color">Allocated To:</span> {allocate.memberName}</div>
                                    <div className="py-3 dotted_line text_child"><span className="color">Allocated On:</span> {allocate.allocate_on ? allocate.allocate_on : 'N/A'}</div>
                                </div>
                                ))}
                                <div className="py-3 dotted_line text_child"><span className="color">Confirmed With Allocated:</span> {this.state.confirmation_details.confirmed_with_allocated || 'N/A'}</div>
                                <div className="py-3 dotted_line text_child"><span className="color">Confirmed With Booker:</span> {this.state.confirmation_details.firstname ? (this.state.confirmation_details.firstname + ' ' + this.state.confirmation_details.lastname) : 'N/A'}</div>
                                <div className="py-3 dotted_line text_child"><span className="color">Confirmed On:</span> {this.state.confirmation_details.confirmed_on ? changeTimeZone(this.state.confirmation_details.confirmed_on) : 'N/A'}</div>
                                <div className="py-3 text_child"><span className="color">Confirmed Method:</span> {(this.state.confirmation_details.confirm_by) ? (confirmBy(this.state.confirmation_details.confirm_by)) : 'N/A'} {this.bookedWay(this.state.confirmation_details.confirm_by)}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12">
                    <div className="row d-flex flex-wrap justify-content-center after_before_remove">
                        <div className="col-lg-5 col-sm-6 ">
                            <div className="row">
                                <div className="col-lg-6 col-sm-6 mt-4">
                                    <button disabled={this.state.shift_status == 7 ? false : true} onClick={this.callAllocated} className="but_submit">Contact Allocated</button>
                                </div>
                                <div className="col-lg-6 col-sm-6 mt-4">
                                    <button disabled={this.state.shift_status == 7 ? false : true} onClick={this.callBooker} className="but_submit">Contact Booker</button>
                                </div>
                                <div className="col-lg-6 col-sm-8 mt-4 col-lg-offset-3 col-sm-offset-2">
                                    <Link to={((this.props.back_url) ? this.props.back_url : '/admin/schedule/unfilled/unfilled')} className="but_submit w-100 d-block text-center">Download Confirmation as PDF</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class CancelShift extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cancel_method: 1,
        }

        this.cancel_reason = [{ label: 'Can’t Afford to Get to Shift', value: 'Can’t Afford to Get to Shift' }, { label: 'My cusion will handle', value: 'My cusion will handle' }];

    }

    selectChange = (key, value) => {
        var state = {}
        state[key] = value;
        state[key + '_error'] = false;
        if (key == 'cancel_type') {
            this.getCancelPerson(value);
            this.setState({ cancel_person: '', onCancelPerson: false });
        }
        this.setState(state);
    }

    getCancelPerson = (cancel_type) => {
        if ((cancel_type == 'kin' || cancel_type == 'booker') && (this.props.booked_by == 2 || this.props.booked_by == 3)) {
            var request = { cancel_type: cancel_type, shiftId: this.props.shiftId, booked_by: this.props.booked_by };
            postData('schedule/Shift_Schedule/get_canceler_list', request).then((result) => {
                if (result.status) {
                    var rst = result.data;
                    if (rst.length > 0) {
                        this.setState({ cancel_person: rst[0].value, cancelerList: rst });
                    }
                    this.setState({ onCancelPerson: true })
                }
            });
        }
    }

    onSubmit = (e) => {
        e.preventDefault()
        var status = true;
        if (!this.state.cancel_type) { this.setState({ cancel_type_error: true }); status = false }
        if (!this.state.reason) { this.setState({ reason_error: true }); status = false }
        if ((this.state.cancel_type == 'kin' || this.state.cancel_type == 'booker') && !this.state.cancel_person) { this.setState({ cancel_person_error: true }); status = false }
        jQuery('#cancel_shift').validate();

        if (jQuery('#cancel_shift').valid() && status) {

            this.setState({ loading: true }, () => {
                var request = { shiftId: this.props.shiftId, cancel_type: this.state.cancel_type, reason: this.state.reason, cancel_method: this.state.cancel_method, cancel_person: this.state.cancel_person }
                postData('schedule/ScheduleDashboard/cancel_shift', request).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={'Shift cancel successfully'} showType={'s'} />, {
                            // toast.success("Shift cancel successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.closeModel('cancel_shift', true);
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }

    errorShowInTooltip($key, msg) {
        return (this.state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((this.state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';
    }

    render() {
        return (
            <div>
                <Modal
                    className="Modal fade Modal_A Modal_B Schedule_Module"
                    show={this.props.modal_show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <form id="cancel_shift">

                            <div className="text text-left by-1 Popup_h_er_1"><span>Cancelling '{this.props.participantName}'s' Shift for: 12/12/2018</span>
                                <a onClick={() => this.props.closeModel('cancel_shift', false)} className="close_i"><i className="icon icon-cross-icons"></i></a>
                            </div>
                            <h4 className="P_20_T h4_edit__">Please give a reason for this cancellation</h4>
                            <div className="row P_15_T">
                                <div className="col-sm-6 mb-3">
                                    <div className="row">
                                        <div className="col-md-12 mb-4">
                                            <label>Who Cancelled?</label>
                                            <span className="required">
                                                <Select clearable={false} className="custom_select" simpleValue={true} required={true} searchable={false} value={this.state['cancel_type']} onChange={(e) => this.selectChange('cancel_type', e)}
                                                    options={cancelShiftWhoOption('', this.props.booked_by, this.props.status)} placeholder="Please Select" />
                                                {this.errorShowInTooltip('cancel_type', 'This field is required')}
                                            </span>
                                        </div>

                                        <div className="col-md-12">
                                            {this.state.onCancelPerson ? <div>
                                                <label>List of '{this.props.participantName}'s'  Bookers</label>
                                                <span className="required">
                                                    <Select clearable={false} className="custom_select" simpleValue={true} required={true} searchable={false} value={this.state['cancel_person']} onChange={(e) => this.selectChange('cancel_person', e)}
                                                        options={this.state.cancelerList} placeholder="Please Select" />
                                                    {this.errorShowInTooltip('cancel_person', 'This field is required')}</span></div> : ''}

                                        </div>
                                    </div>
                                </div>


                                <div className="col-sm-6 mb-3">
                                    <div className="row">
                                        <div className="col-sm-12 mb-4">
                                            <label>Cancel Method?</label>
                                            <span className="required">
                                                <Select clearable={false} className="custom_select" simpleValue={true} required={true} searchable={false} value={this.state['cancel_method']} onChange={(e) => this.selectChange('cancel_method', e)}
                                                    options={cancelShiftMethod()} placeholder="Please Select" />
                                            </span>
                                        </div>

                                        <div className="col-sm-12">
                                            <label>Cancellation Reason:</label>
                                            <span className="required">
                                                <Select clearable={false} className="custom_select" simpleValue={true} required={true} searchable={false} value={this.state['reason']} onChange={(e) => this.selectChange('reason', e)}
                                                    options={this.cancel_reason} placeholder="Please Select" />
                                                {this.errorShowInTooltip('reason', 'Please select reason')}
                                            </span>
                                        </div>
                                    </div>
                                </div>


                                <div className="col-sm-6 col-sm-offset-6 P_25_T">
                                    <button onClick={this.onSubmit} disabled={this.state.loading} className="but_submit but_cancel">Cancel Shift</button>
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

class UpdateShiftDateTIme extends Component {
    constructor(props) {

        super(props);
        this.state = {

        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }
    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        if (fieldname == 'start_time') {
            var difference = this.getTimeDifferInHours();

            if (difference > 24 || difference < 0) {
                state['end_time'] = '';
            }
        }
        this.setState(state);
    }

    getTimeDifferInHours = () => {
        var start_time = moment(this.state.start_time);
        var end_time = moment(this.state.end_time);

        const diff = end_time.diff(start_time);
        const diffDuration = moment.duration(diff);
        var dayHours = diffDuration.days() * 24
        return diffDuration.hours() + dayHours;
    }

    onSubmit = (e) => {
        e.preventDefault()
        jQuery('#updateDate').validate();
        if (jQuery('#updateDate').valid()) {
            var difference = this.getTimeDifferInHours();

            if (difference > 24) {
                toast.error(<ToastUndo message={"Shift duration can be maximum 24 hours."} showType={'e'} />, {
                    // toast.error("Shift duration can be maximum 24 hours.", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                return false;
            } else if (difference < 0) {
                toast.error(<ToastUndo message={"Please check start date time and end date time."} showType={'e'} />, {
                    // toast.error("Please check start date time and end date time.", {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                return false;
            }

            this.setState({ loading: true }, () => {
                postData('schedule/ScheduleDashboard/update_shift_date_time', this.state).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={'Update shift date time successfully'} showType={'s'} />, {
                            // toast.success("Update shift date time successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });

                        this.props.closeModel('date_time', true)
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }

    startDateTime = () => {
        return <DatePicker autoComplete={'off'} dateFormat="DD/MM/YYYY h:mm a" autoComplete="new-password" onChangeRaw={handleDateChangeRaw} placeholderText={"00/00/0000 00:00"} className="text-left"
            selected={this.state.start_time} name="start_time" onChange={(e) => this.selectChange(e, 'start_time')}
            timeIntervals={15} showTimeSelect timeCaption="Time" required={true} name="start_time"
            maxDate={(this.state.end_time) ? moment().add(60, 'days') : moment().add(60, 'days')}
            minDate={moment()}
            minTime={moment().hours(0).minutes(0)}
            maxTime={(this.state.end_time <= this.state.start_time) ? moment(this.state.end_time) : moment().hours(23).minutes(59)} minTime={moment().hours(0).minutes(0)} />
    }

    endDateTime = () => {
        return <DatePicker autoComplete={'off'} dateFormat="DD/MM/YYYY h:mm a" autoComplete="new-password" onChangeRaw={handleDateChangeRaw} placeholderText={"00/00/0000 00:00"} className="text-left"
            selected={this.state.end_time} name="end_time" onChange={(e) => this.selectChange(e, 'end_time')}
            showTimeSelect timeIntervals={15} timeCaption="Time" required={true} name="end_time"
            minDate={this.state.start_time ? moment(this.state.start_time) : moment()} maxDate={(this.state.start_time) ? moment(this.state.start_time).add(1, 'days') : moment().add(60, 'days')}
            minTime={(moment(this.state.start_time).format("DD-MM-YYYY") == moment(this.state.end_time).format("DD-MM-YYYY")) ? moment(this.state.start_time) : moment().hours(0).minutes(0)}
            maxTime={moment().hours(23).minutes(59)} />
    }



    render() {
        return (

            <Modal
                className="Modal fade Modal_A Modal_B Schedule_Module"
                show={this.props.modal_show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <form id="updateDate">
                        <div className="text text-left by-1 Popup_h_er_1"><span>Update shift Date</span>
                            <a onClick={() => this.props.closeModel('date_time', false)} className="close_i"><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div className="row P_25_T">
                            <div className="col-lg-6 col-sm-6">
                                <label>Start Date Time:</label>
                                <span className="required">
                                    {this.startDateTime()}
                                </span>
                            </div>
                            <div className="col-lg-6 col-sm-6">
                                <label>End Date Time:</label>
                                <span className="required">
                                    {this.endDateTime()}
                                </span>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-5 P_15_T pull-right">
                                <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit but_cancel">Update</button>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>

        )
    }
}

class UpdateAddress extends Component {
    constructor(props) {

        super(props);
        this.state = {
            location: []
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
        this.setState({ location: newProps.location });


    }
    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }



    handleShareholderNameChange = (obj, stateName, index, fieldName, value) => {
        var state = {};
        var tempField = {};
        var List = obj.state[stateName];
        List[index][fieldName] = value

        if (fieldName == 'state') {
            List[index]['suburb'] = {}
            List[index]['postal'] = ''
        }

        if (fieldName == 'suburb' && value) {
            List[index]['postal'] = value.postcode
        }


        state[stateName] = List;
        obj.setState(state);
    }

    componentDidMount() {
        postData('common/Common/get_state', {}).then((result) => {
            if (result.status) {
                var details = result.data
                this.setState({ stateList: details });
            }
        });
    }


    onSubmit = (e) => {
        e.preventDefault()
        jQuery('#updateAddress').validate({ ignore: [] });
        if (jQuery('#updateAddress').valid()) {

            this.setState({ loading: true }, () => {
                postData('schedule/ScheduleDashboard/update_shift_address', this.state).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={'Shift address update successfully'} showType={'s'} />, {
                            // toast.success("Shift address update successfully", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.closeModel('address', true)
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }

    render() {
        return (
            <Modal
                className="Modal fade Modal_A Modal_B Schedule_Module"
                show={this.props.modal_show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <form id="updateAddress">
                        <div className="dis_cell-1">
                            <div className="text text-left by-1 Popup_h_er_1"><span>Updating Shift Location:</span>
                                <a onClick={() => this.props.closeModel('address', false)} className="close_i"><i className="icon icon-cross-icons"></i></a>
                            </div>

                            {this.state.location.map((address, index) => (
                                <div className="row P_15_T" key={index + 1}>
                                    <div className="col-md-4">
                                        <label>Location: </label>
                                        <span className="required">
                                            <input type="text" data-rule-required="true" onChange={(e) => handleShareholderNameChange(this, 'location', index, 'address', e.target.value)} value={address.address} name="location" placeholder="Unit #/Street #, Street Name" />
                                        </span>
                                    </div>
                                    <div className="col-md-2">
                                        <label>State: </label>
                                        <Select clearable={false} className="default_validation " simpleValue={true} required={true}
                                            value={address.state} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'state', e)}
                                            options={this.state.stateList} placeholder="Please Select" />
                                    </div>
                                    <div className="col-md-4">
                                        <label>Suburb:</label>
                                        <span className="required">
                                            <Select.Async clearable={false} cache={false} className="default_validation" required={true}
                                                value={address.suburb} disabled={(address.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, address.state)} onChange={(e) => this.handleShareholderNameChange(this, 'location', index, 'suburb', e)}
                                                options={this.state.stateList} placeholder="Please Select" />
                                        </span>
                                    </div>
                                    <div className="col-md-2">
                                        <label>Postcode: </label>
                                        <span className="required">
                                            <input type="text" minLength="4" maxLength="4" value={address.postal} onChange={(e) => handleShareholderNameChange(this, 'location', index, 'postal', e.target.value)} data-rule-required="true" data-rule-postcodecheck="true" className="text-center" name="postcode" placeholder="0000" />
                                        </span>
                                    </div>

                                    {(this.props.location.length > 1) ?
                                        <div className="col-md-2">
                                            <label></label>
                                            <div className="col-md-2">
                                                {index > 0 ? <button className="button_unadd" onClick={(e) => handleRemoveShareholder(this, e, index, 'location')}>
                                                    <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                                </button> : (this.state.location.length == 3) ? '' :
                                                        <button onClick={(e) => handleAddShareholder(this, e, 'location', address)} className="add_i_icon default_but_remove"><i className="icon icon-add-icons"></i></button>}
                                            </div>o
                        </div>
                                        : ''}
                                </div>
                            ))}



                            <div className="row">
                                <div className="col-md-7"></div>
                                <div className="col-md-5 P_15_T">
                                    <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </Modal.Body>
            </Modal>

        )
    }
}

class UpdateShiftRequirement extends Component {
    constructor(props) {

        super(props);
        this.state = {
            requirement: []
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.setState({ loading: true }, () => {
            postData('schedule/ScheduleDashboard/update_shift_requirement', this.state).then((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Update shift requirement successfully'} showType={'s'} />, {
                        // toast.success("Update shift requirement successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.props.closeModel('requirement', true)
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false })
            });
        });
    }



    render() {
        return (

            <Modal
                className="Modal fade Modal_A Modal_B Schedule_Module"
                show={this.props.modal_show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left by-1 mb-3 Popup_h_er_1"><span>Updating Shift Requirement:</span>
                            <a onClick={() => this.props.closeModel('requirement', false)} className="close_i"><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div className="custom_scroll_set__">
                            <div className="cstmSCroll1 CrmScroll">
                                <ScrollArea
                                    speed={0.8}
                                    className="stats_update_list"
                                    contentClassName="content"
                                    horizontal={false}
                                    enableInfiniteScroll={true}
                                    style={{ paddingRight: "0px", height: 'auto', maxHeight: '120px' }}
                                >
                                    {this.state.requirement.map((value, key) => (
                                        <span key={key + 1} className="w-50 d-inline-block pb-2">
                                            {/*   <input type='checkbox' name="oc_service" className="checkbox1" checked={value.active || ''} onChange={(e) => this.setcheckbox(key, value.active, 'requirement')} name="shift_requirement" data-rule-required="true" />
                                        <label >
                                            <div className="d_table-cell">
                                                <span onClick={(e) => handleCheckboxValue(this, 'requirement', key, 'active')}></span>
                                            </div>
                                            <div className="d_table-cell" onClick={(e) => handleCheckboxValue(this, 'requirement', key, 'active')}>{value.name}</div>
                                        </label> */}

                                            <label class="c-custom-checkbox CH_010">
                                                <input type='checkbox' name="oc_service" className="checkbox1" checked={value.active || ''} onChange={(e) => handleCheckboxValue(this, 'requirement', key, 'active')} name="shift_requirement" data-rule-required="true" />
                                                <i class="c-custom-checkbox__img"></i><div>{value.name}</div>
                                            </label>

                                        </span>
                                    ))}
                                </ScrollArea>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-sm-5 col-sm-offset-7 P_15_T">
                                <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                            </div>
                        </div>

                    </div>
                </Modal.Body>
            </Modal>

        )
    }
}

class UpdateShiftPrefferedMember extends Component {
    constructor(props) {

        super(props);
        this.state = {
            preferred_members: []
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
    }

    onSubmit = (e) => {
        e.preventDefault();
        jQuery('#preferred_member_update').validate({ ignore: [] });
        if (jQuery('#preferred_member_update').valid()) {
            postData('schedule/ScheduleDashboard/update_preffered_member', this.state).then((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Update preffered member successfully'} showType={'s'} />, {
                        // toast.success("Update preffered member successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });

                    this.props.closeModel('preffered_member', true)
                }
            });
        }
    }

    render() {
        return (
            <Modal
                className="Modal fade Modal_A Modal_B Schedule_Module"
                show={this.props.modal_show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <div className="dis_cell">
                        <form id="preferred_member_update">
                            <div className="text text-left by-1 Popup_h_er_1"><span>Updating Shift Member:</span>
                                <a onClick={() => this.props.closeModel('preffered_member', false)} className="close_i"><i className="icon icon-cross-icons"></i></a>
                            </div>
                            {this.state.preferred_members.map((value, idx) => (
                                <div className="row P_25_T" key={idx}>
                                    <div className="col-lg-6">
                                        <div className="search_icons_right modify_select">
                                            <Select.Async
                                                className="default_validation" required={true}
                                                name={"form-field-name" + idx}
                                                value={value.select}
                                                loadOptions={getOptionsMember}
                                                placeholder='Preffered member'
                                                onChange={(e) => handleShareholderNameChange(this, 'preferred_members', idx, 'select', e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-2 col-md-2 mt-1">
                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'preferred_members')} className="button_unadd">
                                            <i className="icon icon-decrease-icon icon_cancel_1" ></i>
                                        </button> : (this.state.preferred_members.length == 1) ? '' : <button className="add_i_icon" onClick={(e) => handleAddShareholder(this, e, 'preferred_members', value)}>
                                            <i className="icon icon-add-icons" ></i>
                                        </button>
                                        }
                                    </div>
                                </div>
                            ))
                            }
                            <div className="row">
                                <div className="col-md-7"></div>
                                <div className="col-md-5 P_15_T">
                                    <button onClick={this.onSubmit} className="but_submit">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </Modal.Body>
            </Modal>

        )
    }
}