import React, { Component } from 'react';
import {  postData, handleDateChangeRaw,  handleShareholderNameChange,  getOptionsMember } from '../../../service/common.js';
import Select, { Async } from 'react-select-plus';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from 'react-bootstrap/lib/Modal';

import {  availableMemberLookup, availableMemberMatches} from '../../../dropdown/ScheduleDropdown.js';

class MemberLookUpPopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allocate_to: [],
            member_lookup: 1,
            memberLimit: 3
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps);
        if (newProps.modal_show) {
            this.getAllocatedMember();
        }
    }



    getAllocatedMember() {
        var Request = { shift_id: this.state.shiftId, shift_date: this.state.shift_date, start_time: this.state.start_time, shift_organiztion_site: this.props.shift_organiztion_site, 'memberLimit': this.state.memberLimit, member_lookup: this.state.member_lookup, participant_data: this.props.participant_data, booked_by: this.props.booked_by };
        postData('schedule/Shift_Schedule/get_nearest_shift_member', Request).then((result) => {
            this.setState({ allocate_to: result.data });
        });
    }

    onSubmit(e) {
        var checkSelect = false;
        this.state.allocate_to.map((val, id) => {
            if(val.access == true){
                checkSelect = true
            }
        });
        
        if (checkSelect) {
            var Request = this.state;
            postData('schedule/Shift_Schedule/allot_member_to_shift', Request).then((result) => {
                if (result.status) {
                    this.props.closeModel('member_lookUp', true);
                }
            });
        }
        else {
            toast.error("Please select atleast one Member to continue.", {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }

    render() {
        return (
            <Modal
                className="Modal fade Modal_A Modal_B"
                show={this.props.modal_show}
                onHide={this.handleHide}
                container={this}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Body>
                    <div className="lock_icon">
                        <i className="icon icon-circule fill_shifts_icons color"></i>
                    </div>

                    <div className="text text-center color by-1 Popup_h_er_1">
                       <span> Available Member Look-Up:</span>
                         <a onClick={() => this.props.closeModel('member_lookUp', false)} className="close_i"><i className="icon icon-cross-icons"></i></a></div>
                    
                    <div className="row mt-4">
                        <div className="col-sm-5  col-sm-offset-1">
                            <Select name="member_lookup" required={true} simpleValue={true} searchable={false} clearable={false} options={availableMemberLookup()} placeholder='Matched option' value={this.state.member_lookup} onChange={(e) => this.setState({ 'member_lookup': e }, () => { this.getAllocatedMember() })} />
                        </div>

                        <div className="col-sm-5">
                            <Select name="memberLimit" required={true} simpleValue={true} searchable={false} clearable={false} options={availableMemberMatches()} placeholder='Matched option' value={this.state.memberLimit} onChange={(e) => this.setState({ 'memberLimit': e }, () => { this.getAllocatedMember() })} />
                        </div>
                    </div>

                    <div className="row mt-3">
                        <div className="col-md-8 col-sm-offset-2">
                            <table className="available_member_table_parent Sae_row w-100">
                                <tbody>
                                    {(this.state.allocate_to.length > 0) ?
                                        this.state.allocate_to.map((value, idx) => (
                                            <tr key={idx + 1}>
                                                <td className="px-3 py-3">{value.memberName}</td>
                                                <td align="right" className="px-3 pt-2">

                                                    <label className="CU_input">
                                                        <input type="checkbox" checked={value.access || ''} onChange={(e) => handleShareholderNameChange(this, 'allocate_to', idx, 'access', e.target.checked)} disabled=""/>
                                                        <i className="CU_input__img"></i>
                                                    </label>

                                                </td>
                                            </tr>
                                        )) : <div className="no_record py-2">No record found.</div>
                                    }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-sm-4 col-sm-offset-4 mt-3">
                            {this.state.allocate_to.length > 0?<button className="but_submit" onClick={(e) => this.onSubmit(e)}>Finished</button>: ''}
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        )
    }
}

export default MemberLookUpPopUp;
