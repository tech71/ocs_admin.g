import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { postData, changeTimeZone } from '../../../service/common.js';
import 'react-select-plus/dist/react-select-plus.css';
import Countdown from 'react-countdown-now';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastUndo } from 'service/ToastUndo.js';
import { ROUTER_PATH } from 'config.js';


class ScheduleFillShift extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listShiftsIDs: this.props.props.location.state,
            shiftDetails: [],
            is_redirect: false
        }
    }

    getAutoFilledMemberAndShift = () => {
        postData('schedule/ScheduleDashboard/get_auto_fill_shift_member', this.state).then((result) => {
            if (result.status) {
                this.setState({ shiftDetails: result.data });
            }
        });
    }

    singleAccept = (index, value) => {
        var List = this.state.shiftDetails;
        List[index]['accepted'] = value
        this.setState({ shiftDetails: List });
    }

    acceptMatch = (event) => {
        event.preventDefault()
        var status = false
        this.state.shiftDetails.map((val, idx) => {
            if (val.accepted)
                status = true
        })

        toast.dismiss();
        if (status) {
            postData('schedule/ScheduleDashboard/assign_autofill_member', this.state.shiftDetails).then((result) => {
                if (result.status) {
                    toast.success(<ToastUndo message={'Assign successfully'} showType={'s'} />, {                    
                    // toast.success('Assign successfully', {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ is_redirect: true })
                }
            });
        } else {
            toast.error(<ToastUndo message={'Please assign member to at least one shift'} showType={'e'} />, {
            // toast.error('Please assign member to at least one shift', {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }

    componentDidMount() {
        if (this.state.listShiftsIDs) {
            this.getAutoFilledMemberAndShift(this.state.listShiftsIDs);
        } else {
            this.setState({ no_select: true })
        }
    }

    render() {
        return (
          <React.Fragment>
                {(this.state.is_redirect) ? <Redirect to={ROUTER_PATH+'admin/schedule/unconfirmed/unconfirmed'} /> : ''}
                {(this.state.no_select) ? <Redirect to={ROUTER_PATH+'admin/schedule/unfilled/unfilled'} /> : ''}
         

                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-sm-12">
                                <Link to={ROUTER_PATH+'admin/schedule/unfilled/unfilled'}><span className="icon icon-back-arrow back_arrow"></span></Link>
                            </div>
                        </div>
                        <div className="row"><div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div></div>


                        <div className="row">
                            <div className="col-lg-12 col-sm-12 P_25_TB">
                                <h1 className="text-center color"><span className="icon icon-circule fill_shifts_icons"></span></h1>
                                <h1 className="text-center color">Fill Shifts</h1>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-5 col-sm-5 Unfilled_div">
                                <h1 className="color">Unfilled Shifts</h1>
                            </div>

                            <div className="col-lg-2 col-sm-2 Mathed_div">
                                <label className="color">Matched With</label>
                                <div className="pt-3"></div>
                            </div>


                            <div className="col-lg-5 col-sm-5 Avai_Member_div">
                                <h1 className="color">Available Members:</h1>
                            </div>
                        </div>

                        {this.state.shiftDetails.map((val, index) => (
                            <div key={index + 1} className="row flex">
                                <div className="col-lg-5  col-sm-5 Unfilled_div">
                                    <div className="row">
                                        <div className="col-lg-12 col-sm-12 padding_right">
                                            <div className="panel-group accordion_me house_accordi  mb-1" id="accordion" role="tablist" aria-multiselectable="true">

                                                <div className="panel panel-default">
                                                    <div className="panel-heading" role="tab" id={'shift_head' + index}>
                                                        <h4 className="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent={'#accordion' + index} href={'#shift' + index} aria-expanded="false" aria-controls={'shift' + index}>
                                                                <table className="shift_table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td className="date_div"><span className="color">Date: </span>{val.shift_date}</td>
                                                                            <td><span className="color">For:</span>{(val.booked_by == 2 || val.booked_by == 3) ? 
                                                                            <React.Fragment>{val.shift_participant.length > 0? val.shift_participant[0].participantName: 'N/A'}</React.Fragment> 
                                                                              :<React.Fragment>{val.shift_organiztion_site.lenght > 0 ? val.shift_organiztion_site[0].site_name: 'N/A'}</React.Fragment>}</td>
                                                                            <td className="start_div"><span className="color">Start:</span> {changeTimeZone(val.start_time, 'LT')}</td>
                                                                            <td className="duration_div"><span className="color">Duration:</span> {val.duration}</td>
                                                                            <td className="duration_div"><span className="color">Suburb:</span> {val.location.length > 0? val.location[0].suburb: "N/A"}</td>
                                                                            <td className="up_down_arrow br-0"><i className="more-less glyphicon glyphicon-plus"></i></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id={'shift' + index} className="panel-collapse collapse" role="tabpanel" aria-labelledby={'shift_head' + index}>
                                                        <div className="panel-body px-0 pt-1 pb-2">
                                                            <table className="shift_table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td className="text-right br-0 py-0"><span className="color">Expenses: </span>{val.expenses ? '$' + val.expenses : 'N/A'}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="text-left br-0 py-0"><span className="color">End: </span>{changeTimeZone(val.end_time, "LT")}</td>
                                                                        <td className="text-right br-0 py-0"><span className="color">KMs: </span> {val.available_member ? val.available_member.distance_km : ''} km</td>
                                                                    </tr>
                                                                    <tr>
                                                                        {val.location.map((site, id) => (
                                                                            <td key={id + 1} className="text-left br-0 py-0"><span className="color">Location: </span>{site.site}</td>
                                                                        ))}
                                                                        <td className="text-right br-0 py-0"><span className="start_text">Starts in: </span>{<Countdown date={Date.now() + val.diff} />}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        {val.preferred_member.map((member, id) => (
                                                                            <td key={id + 1} colSpan="2" className="text-left br-0  py-0"><span>Prefferd Member:</span>{member.memberName}</td>
                                                                        ))}
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-2 col-sm-2 Mathed_div">
                                    <div className="col-lg-12">
                                        {(val.available_member.memberId) ?
                                            <div className="py-0  my-1 match_with">
                                                <div className="w-10"><span onClick={() => this.singleAccept(index, false)} className={'icon icon-cross-icons ' + ((val.accepted == true) ? 'rejected_shift' : '')}></span></div>
                                                <div className="approve_line w-100"><span className="line_me"></span></div>
                                                <div className="w-10"><span onClick={() => this.singleAccept(index, true)} className={"icon icon-input-type-check " + ((val.accepted == true) ? 'accepted_shift' : '')}></span></div>
                                            </div> : ''
                                        }
                                    </div>
                                </div>

                                <div className="col-lg-5 col-sm-5 Avai_Member_div">

                                    {(val.available_member.memberId) ?
                                        <div className="row">
                                            <div className="col-sm-12 padding_left">
                                                <div className="panel-group accordion_me house_accordi mb-1" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div className="panel panel-default">
                                                        <div className="panel-heading" role="tab" id={'headingOne' + index}>
                                                            <h4 className="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent={'#accordions' + index} href={'#test_1' + index} aria-expanded="false" aria-controls={'test_1' + index}>
                                                                    <table className="shift_table avail">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="Name_div"><span className="color">Name </span> {val.available_member.memberName}</td>
                                                                                <td><span className="color">Location: </span>{val.available_member.suburb}</td>
                                                                                <td className="counter_td"><span className="color">Contact Counter: </span> 0</td>
                                                                                <td className="up_down_arrow br-0"><i className="more-less glyphicon glyphicon-plus"></i></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id={'test_1' + index} className="panel-collapse collapse" role="tabpanel" aria-labelledby={'headingOne' + index}>
                                                            <div className="panel-body px-2 py-2">
                                                                <div className="heading_prefer">Shared Preferrences:</div>
                                                                <div className="row">
                                                                    <div className="col-sm-3">
                                                                        <p className="heading_prefer pb-2">Places:</p>
                                                                        {val.available_member.shared_place.map((plc, id) => (
                                                                            <p key={id + 1} className="place_text">{plc.name}</p>
                                                                        ))}
                                                                    </div>
                                                                    <div className="col-sm-3">
                                                                        <p className="heading_prefer pb-2">Activities:</p>
                                                                        {val.available_member.shared_activity.map((act, id) => (
                                                                            <p key={id + 1} className="place_text">{act.name}</p>
                                                                        ))}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> :
                                        <div className="row">
                                            <div className="col-sm-12 padding_left">
                                                <div className="panel-group accordion_me house_accordi mb-1" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div className="panel panel-default">
                                                        <div className="panel-heading" role="tab" id={'headingOne' + index}>
                                                            <h4 className="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent={'#accordions' + index} href={'#test_1' + index} aria-expanded="true" aria-controls={'test_1' + index}>
                                                                    <table className="shift_table avail">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td colSpan="4" className="Name_div text-center">No member available</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>}
                                </div>


                            </div>
                        ))}



                        <div className="row">
                      
                            <div className="col-lg-12 col-sm-12">
                                <div className="row">
                                    <div className="col-lg-3 col-sm-3 pull-right"><a onClick={this.acceptMatch} className="but">Accept Matches</a></div>
                                </div>
                            </div>
                          
                        </div>
                        </React.Fragment>
              
        );
    }
}

export default ScheduleFillShift
