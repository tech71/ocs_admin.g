const initaileState = {
    subMenuShow: false,
}

const sidebarData = (state = initaileState, action) => {

    switch (action.type) {
        case 'set_sidebar_subMenu_Show':
            return {...state, subMenuShow: action.subMenuShowData};
        default:
            return state;
    }
}

export default sidebarData;
    