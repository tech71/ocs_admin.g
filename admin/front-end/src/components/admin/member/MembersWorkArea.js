import React from 'react';
import jQuery from "jquery";
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import { checkItsNotLoggedIn, postData, archiveALL, postImageData } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { memberWorkArea, memberWorkAreaStatus, memberWorkAreaViewBy, memberSpecialAgreementViewBy, memberAward, memberPositionAward, memberPositionLevel } from '../../../dropdown/memberdropdown.js';
import Modal from 'react-bootstrap/lib/Modal';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile, customHeading } from '../../../service/CustomContentLoader.js';
import MemberProfile from './MemberProfile';
import { connect } from 'react-redux';
import {ParticiapntPageIconTitle, MemberPageIconTitle} from 'menujson/pagetitle_json';


import { ToastUndo } from 'service/ToastUndo.js'

class MembersWorkArea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            view_by_state: 'Current',
            agreementViewBy: 'Current',
            positionViewBy: 'Current',
            specialAgreement: [],
            workAreaData: [],
            workAwardData: []
        };
        checkItsNotLoggedIn(ROUTER_PATH);
        this.urlParam = this.props.props.match.params.id;
        this.memberWorkAreaViewBy = memberWorkAreaViewBy();
        this.memberSpecialAgreementViewBy = memberSpecialAgreementViewBy();
    }

    closeWorkAreaPopUp = () => {
        this.setState({ openModal: false });
    }

    closeAwardPopUp = () => {
        this.setState({ openAwardModal: false, OldData: [] });
    }

    closeDocsPopUp = () => {
        this.setState({ openDocsModal: false });
    }

    componentDidMount() {
        this.getMemberWorkArea();
        this.getMemberSpecialAgreement();
        this.getMemberPositionAward();
    }

    getMemberWorkArea = () => {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, status: this.state.view_by_state };
        postData('member/MemberDashboard/get_member_work_area', requestData).then((result) => {
            if (result.status) {
                this.setState({ workAreaData: result.data });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    getMemberPositionAward = () => {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, status: this.state.positionViewBy };
        postData('member/MemberDashboard/get_member_position_award', requestData).then((result) => {
            if (result.status) {
                this.setState({ workAwardData: result.data });
            } else {
                this.setState({ error: result.error });
            }
        });
    }

    getMemberSpecialAgreement = () => {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, status: this.state.agreementViewBy };
        this.setState({ loading: true });
        postData('member/MemberDashboard/get_member_special_agreement', requestData).then((result) => {
            if (result.status) {
                this.setState({ specialAgreement: result.data });
            } else {
                this.setState({ specialAgreement: result.data });
            }
            this.setState({ loading: false });
        });
    }

    archived(index, id, type) {
        var member_id = this.props.props.match.params.id;
        archiveALL({id: id,type:type,member_id:member_id},'', 'member/MemberDashboard/archive_member_workarea_awards').then((result) => {
            if (result.status) {
                var state = {}
                if (type == '2')
                    state['workAwardData'] = this.state.workAwardData.filter((s, sidx) => index !== sidx);
                else
                    state['workAreaData'] = this.state.workAreaData.filter((s, sidx) => index !== sidx);
                this.setState(state);
            }
        })
    }

    selectChange(selectedOption, fieldname, used_for) {
        if (selectedOption != null) {
            if (selectedOption == 'Current' && used_for == 'work_area') {
                this.setState({ view_by_state: 'Current' }, () => {
                    this.getMemberWorkArea();
                });
            }
            else if (selectedOption == 'Archive' && used_for == 'work_area') {
                this.setState({ view_by_state: 'Archive' }, () => {
                    this.getMemberWorkArea();
                });
            }

            else if (selectedOption == 'Current' && used_for == 'Special_agreement') {
                this.setState({ agreementViewBy: 'Current' }, () => {
                    this.getMemberSpecialAgreement();
                });
            }

            else if (selectedOption == 'Expire' && used_for == 'Special_agreement') {
                this.setState({ agreementViewBy: 'Expire' }, () => {
                    this.getMemberSpecialAgreement();
                });
            }

            else if (selectedOption == 'Current' && used_for == 'member_award') {
                this.setState({ positionViewBy: 'Current' }, () => {
                    this.getMemberPositionAward();
                });
            }

            else if (selectedOption == 'Archive' && used_for == 'member_award') {
                this.setState({ positionViewBy: 'Archive' }, () => {
                    this.getMemberPositionAward();
                });
            }
        }
    }

    clickToDownloadFile = (key) => {
        var activeDownload = this.state.specialAgreement;
        if (activeDownload[key]['is_active'] == true)
            activeDownload[key]['is_active'] = false;
        else
            activeDownload[key]['is_active'] = true;
        this.setState({ specialAgreement: activeDownload });
    }

    downloadSelectedFile = () => {
        var member_id = this.props.props.match.params.id;
        var requestData = { member_id: member_id, downloadData: this.state.specialAgreement };

        postData('member/MemberDashboard/download_selected_file', requestData).then((result) => {
            if (result.status) {
                window.location.href = BASE_URL + "archieve/" + result.zip_name;

                var removeDownload = this.state.specialAgreement;
                removeDownload.map((value, idx) => {
                    removeDownload[idx]['is_active'] = false;
                })
                this.setState({ specialAgreement: removeDownload });
            } else {
                toast.dismiss();
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

    render() {
        return (
            <div>

                <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} pageTypeParms={this.props.props.match.params.page} />

                <div className="row flex_height_equal">
                    <div className="col-lg-12 col-md-12">

                        <div className="tab-content">
                            <div role="tabpanel" className={this.props.showTypePage == 'current_work_area' ? "tab-pane active" : "tab-pane"} id="Barry_details">
                                <div className="row Roww_h_1 mt-3">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="row">
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                            <ReactPlaceholder style={{ marginTop: '3px' }} showLoadingAnimation={true} customPlaceholder={customHeading(60)} ready={!this.state.loading}>
                                                <div className="col-lg-12 P_7_TB"><h3><b>
                                                {/* {this.props.firstName} */}
                                                 Current Work Areas</b></h3></div>
                                            </ReactPlaceholder >
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                            <Select name="view_by_state" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.view_by_state} onChange={(e) => this.selectChange(e, 'view_by_state', 'work_area')}
                                                options={this.memberWorkAreaViewBy} placeholder="View By" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-9 col-sm-9">
                                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>

                                            <div className="quali_table___">
                                                {(this.state.workAreaData.length > 0) ?
                                                    this.state.workAreaData.map((value, idx) => (
                                                        <div className="quali_tr___" key={idx + 2}>
                                                            <div className="quali_td1___ bl-0"><span className="color">{memberWorkArea(value.work_area)}:</span> <b>{memberWorkAreaStatus(value.work_status)}</b></div>
                                                            <div className="quali_td4___"> {(value.archive) && value.archive == 0 ? <span title={ParticiapntPageIconTitle.par_archive_icon}> <i className="icon icon-email-pending archive_Icon" onClick={() => this.archived(idx, value.id, '1')}></i></span> : ''}</div>
                                                        </div>
                                                    )) : <div className="no_record py-2 w-100 mt-3">No record found.</div>
                                                }
                                            </div>
                                            {(this.state.view_by_state == 'Current') ?
                                                <div className='col-md-4 pull-right mt-5'>
                                                    <a className="but" onClick={() => this.setState({ openModal: true })} > Add new Work Area</a>
                                                </div>
                                                : ''}
                                        </ReactPlaceholder>
                                        <MembersWorkAreaModal isModalShow={this.state.openModal} closePopup={this.closeWorkAreaPopUp} firstName={this.props.firstName} full_name={this.props.full_name} memberId={this.props.props.match.params.id} getMemberWorkArea={this.getMemberWorkArea} />
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" className={this.props.showTypePage == 'award_level' ? "tab-pane active" : "tab-pane"} id="Barry_availabitity">
                                <div className="row Roww_h_1 mt-3">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="row">
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                            <div className="col-lg-12 P_7_TB"><h3><b>
                                            {/* {this.props.firstName} */}
                                             Position/Award Level</b></h3></div>
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                            <Select name="positionViewBy" required={true} simpleValue={true} searchable={false} clearable={false} placeholder="View By" value={this.state.positionViewBy} onChange={(e) => this.selectChange(e, 'view_by_state', 'member_award')} options={this.memberWorkAreaViewBy} />
                                        </div>
                                    </div>
                                </div>

                                <div className="row d-flex set_alignment">
                                    <div className="col-lg-9 col-md-9">

                                        <div className="quali_table___">
                                            {(this.state.workAwardData.length > 0) ?
                                                this.state.workAwardData.map((value, idx) => {
                                                    return <div className="quali_tr___" key={idx}>
                                                        <div className="quali_td1___ bl-0"><span className="color">{memberWorkArea(value.work_area)}</span></div>
                                                        <div className="quali_td2___"><b>{value.level_name}: {value.point_name}</b></div>
                                                        <div className="quali_td4___">
                                                            <span>
                                                                {(value.archive) && value.archive == 0 ? <a title={ParticiapntPageIconTitle.par_update_icon} className="mr-2" onClick={() => this.setState({ mode: 'edit', id: value.id, openAwardModal: true, OldData: value })}><i className="icon icon-update"></i></a> : ''}

                                                                {(value.archive) && value.archive == 0 ? <i title={ParticiapntPageIconTitle.par_archive_icon} className="icon icon-email-pending" onClick={() => this.archived(idx, value.id, '2')}></i> : ''}
                                                            </span>
                                                        </div>
                                                    </div>
                                                }) : <div className="no_record py-2 w-100 mt-3">No record found.</div>
                                            }
                                        </div>
                                        {this.state.positionViewBy == 'Current' ?

                                            <div className='col-md-3 pull-right mt-5'>
                                                 <a className="but" onClick={() => this.setState({ openAwardModal: true, mode: 'add' })}> Add New Position / Award</a>
                                            </div>
                                                    : ''}


                                    </div>
                                    {(this.state.openAwardModal)?<MembersPositionAwardModal isModalShow={this.state.openAwardModal} mode={this.state.mode} id={this.state.id} closePopup={this.closeAwardPopUp} firstName={this.props.firstName} full_name={this.props.full_name} memberId={this.props.props.match.params.id} getMemberPositionAward={this.getMemberPositionAward} OldData={this.state.OldData} />:''}
                                </div>
                            </div>

                            <div role="tabpanel" className={this.props.showTypePage == 'special_agreement' ? "tab-pane active" : "tab-pane"} id="Cancelled_Shifts">
                                <div className="row Roww_h_1 mt-3">
                                    <div className="col-lg-9 col-md-9">
                                        <div className="row">
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                            <div className="col-lg-12 P_7_TB"><h3><b>
                                            {/* {this.props.firstName}  */}
                                            Special Agreement(s)</b></h3></div>
                                            <div className="col-lg-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-md-3">
                                        <div className="box">
                                            <Select name="agreementViewBy" required={true} simpleValue={true} searchable={false} clearable={false} value={this.state.agreementViewBy} onChange={(e) => this.selectChange(e, 'agreementViewBy', 'Special_agreement')}
                                                options={this.memberSpecialAgreementViewBy} placeholder="View By" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row d-flex set_alignment">
                                    <div className="col-lg-9 col-md-9">
                                        <ul className="file_down quali_width P_15_TB">
                                            {(this.state.specialAgreement.length > 0) ?
                                                this.state.specialAgreement.map((value, idx) => (
                                                    <li key={idx + 1} className={(value.is_active) ? 'active' : ''} onClick={() => this.clickToDownloadFile(idx)}>
                                                        <div className="path_file mb-5 mt-0"><b>{value.title}</b></div>
                                                        <span className="icon icon-file-icons d-block"></span>
                                                        <div className="path_file">{value.filename}</div>
                                                    </li>
                                                )) : <div className="no_record py-2 w-100">No record found.</div>
                                            }
                                        </ul>
                                    </div>

                                    <div className="col-lg-3 col-md-3 align-self-end">
                                        <span>
                                            <p><a className="but" onClick={() => this.downloadSelectedFile()}>Download Selected</a></p>
                                            {(this.state.agreementViewBy == 'Current') ?
                                                <p><a className="but" onClick={() => this.setState({ openDocsModal: true })}>Upload Special Agreement</a></p>
                                                : ''}
                                        </span>
                                    </div>
                                    <MembersUploadSpecialAgreementModal isDocsModalShow={this.state.openDocsModal} closeUploadPopup={this.closeDocsPopUp} firstName={this.props.firstName} full_name={this.props.full_name} memberId={this.props.props.match.params.id} getMemberSpecialAgreement={this.getMemberSpecialAgreement} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.MemberReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(MembersWorkArea);

//
class MembersWorkAreaModal extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = {
            memberWorkArea: false,
            memberWorkAreaStatus: false,
            submit_form: true,
        }
        this.state = this.initialState;
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname + '_error'] = false;
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    onSubmit(e) {
        e.preventDefault();
        var isSubmit = 1;
        var state = {};
        if (!this.state.memberWorkArea) {
            isSubmit = 0;
            state['memberWorkArea' + '_error'] = true;
        }

        if (!this.state.memberWorkAreaStatus) {
            isSubmit = 0;
            state['memberWorkAreaStatus' + '_error'] = true;
        }
        this.setState(state);

        if (isSubmit) {
            toast.dismiss();
            this.setState({ submit_form: false }, () => {
                postData('member/MemberDashboard/save_member_work_area', { data: this.state, 'memberId': this.props.memberId, 'full_name': this.props.full_name }).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={'Work Area created successfully.'} showType={'s'} />, {
                        // toast.success("Work Area created successfully.", {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.getMemberWorkArea();
                        this.setState(this.initialState);
                        this.closeClear();
                        setTimeout(() => this.setState({ success: true }), 500);
                        this.setState({ submit_form: true });
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ submit_form: true });
                    }
                });
            });
        }
    }

    closeClear = () => {
        this.setState({ memberWorkArea: '', memberWorkAreaStatus: '' });
        this.props.closePopup();
    }

    callHtmlForValidation = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }
    //

    render() {
        return (
            <div>
                <Modal className="modal fade Modal_A  Modal_B" show={this.props.isModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                    <Modal.Body>
                        <div className="dis_cell">
                            <div className="text text-left Popup_h_er_1"><span>Adding New Work Area:</span>
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.closeClear()}><i className="icon icon-cross-icons"></i></a>
                            </div>

                            <h4 className="P_20_T h4_edit__">Please select the type of Work Area being added</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="memberWorkArea" required={true} simpleValue={true} searchable={false} clearable={false} options={memberWorkArea(0)} value={this.state.memberWorkArea} onChange={(e) => this.selectChange(e, 'memberWorkArea')} />
                                        {this.callHtmlForValidation('memberWorkArea')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>



                            <h4 className="P_20_T h4_edit__">Please select the 'status' for this Work Area</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="memberWorkAreaStatus" required={true} simpleValue={true} searchable={false} clearable={false} options={memberWorkAreaStatus(0)} value={this.state.memberWorkAreaStatus} onChange={(e) => this.selectChange(e, 'memberWorkAreaStatus')} />
                                        {this.callHtmlForValidation('memberWorkAreaStatus')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>


                            <div className="row">
                                <div className="col-md-7"></div>
                                <div className="col-md-5">
                                    <input type="submit" className="but" value={'Save'} name="content" disabled={(this.state.submit_form) ? false : true} onClick={(e) => this.onSubmit(e)} />
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

//
class MembersUploadSpecialAgreementModal extends React.Component {
    constructor(props) {
        super(props);
        this.uploadDocsInitialState = {
            selectedFile: null,
            startDate: null,
            submit_form: true,
        }
        this.state = this.uploadDocsInitialState;

    }
    fileChangedHandler = (event) => {
        this.setState({ selectedFile: event.target.files[0], filename: event.target.files[0].name })
    }

    uploadHandler = (e) => {
        e.preventDefault();
        jQuery("#special_agreement_form").validate({ /* */ });
        if (jQuery("#special_agreement_form").valid()) {
            this.setState({ submit_form: false });
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('memberId', this.props.memberId)
            formData.append('expiry', (this.state.startDate != null && this.state.startDate != '' ? this.state.startDate : ''))
            formData.append('docsTitle', (this.state.docsTitle != undefined ? this.state.docsTitle : ''))
            formData.append('memberName', this.props.full_name)

            postImageData('member/MemberDashboard/upload_member_special_agreement', formData).then((result) => {
                if (result.status) {
                    this.closeClear();
                    this.setState(this.uploadDocsInitialState);
                    this.props.getMemberSpecialAgreement();
                    toast.success(<ToastUndo message={'Uploaded successfully.'} showType={'s'} />, {
                    // toast.success("uploaded successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ submit_form: true });
            });
        }
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    closeClear = () => {
        this.setState({ startDate: '', filename: '', docsTitle: '' });
        this.props.closeUploadPopup();
    }

    render() {
        return (
            <div>
                <Modal className="modal fade Modal_A  Modal_B" show={this.props.isDocsModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                    <form id="special_agreement_form" method="post" autoComplete="off">
                        <Modal.Body>
                            <div className="dis_cell">
                                <div className="text text-left Popup_h_er_1"><span>Uploading Special Agreement:</span>
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.closeClear()}><i className="icon icon-cross-icons"></i></a>
                                </div>

                                <div className="row P_15_T">
                                    <div className="col-md-4">
                                        <div className="row P_15_T">
                                            <div className="col-md-12">
                                                <label style={{paddingBottom:'5px'}}>Title</label>
                                                <span className="required">
                                                    <input type="text" placeholder="Please Enter Your Title" onChange={(e) => this.setState({ 'docsTitle': e.target.value })} value={(this.state.docsTitle) ? this.state.docsTitle : ''} data-rule-required="true" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-8">
                                        <div className="row P_15_T">
                                            <div className="col-md-12"><label style={{paddingBottom:'5px'}}>Please set an expiry date for this special agreement</label></div>
                                            <div className="col-md-5">
                                                <span className="required"><DatePicker autoComplete={'off'} className="text-center" placeholderText={'00/00/0000'} selected={this.state.startDate} name="startDate" onChange={(e) => this.selectChange(e, 'startDate')} minDate={moment()} required /></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row P_15_T">
                                    <div className="col-md-12"> <label>Please select a file to upload</label></div>
                                    <div className="col-md-5">
                                        <span className="required upload_btn">
                                            <label className="btn btn-default btn-sm center-block btn-file">
                                                <i className="but" aria-hidden="true">Upload New Doc</i>
                                                <input className="p-hidden" type="file" name="special_agreement_file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
                                            </label>
                                        </span>
                                        {(this.state.filename) ? <p>File Name: <small>{this.state.filename}</small></p> : ''}

                                    </div>
                                    <div className="col-md-7"></div>
                                </div>

                                <div className="row">
                                    <div className="col-md-7"></div>
                                    <div className="col-md-5">
                                        <input type="submit" className="but" value={'Save'} name="content" onClick={(e) => this.uploadHandler(e)} disabled={(this.state.submit_form) ? false : true} />
                                    </div>
                                </div>

                            </div>
                        </Modal.Body>
                    </form>
                </Modal>
            </div>
        );
    }
}

//
class MembersPositionAwardModal extends React.Component {
    constructor(props) {
        super(props);
        this.initialState = {
            memberPositionAward: false,
            submit_form: true,
        }
        this.state = this.initialState;
    }

    componentDidMount() { 
        this.getPositionAndLevel();
        if(this.props.OldData)
        {
            this.setState(this.props.OldData);
        }
    }

    getPositionAndLevel = () => {
        postData('member/MemberDashboard/get_posistion_and_level', {}).then((result) => {
            if (result.status) {
                this.setState(result.data);
            }
        });
    }

    /*getSelectedWorkAreaOfMember = () => {
        postData('member/MemberDashboard/get_work_area_of_member', {member_id:this.props.memberId}).then((result) => {
            if (result.status) {
                this.setState(result.data);
            }
        });
    }*/

    componentWillReceiveProps(newProps) {
        this.setState(newProps.OldData);
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname + '_error'] = false;
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    onSubmit(e) {
        e.preventDefault();
        var isSubmit = 1;
        var state = {};
        if (!this.state.work_area) {
            isSubmit = 0;
            state['work_area' + '_error'] = true;
        }

        if (!this.state.award) {
            isSubmit = 0;
            state['award' + '_error'] = true;
        }

        if (!this.state.level) {
            isSubmit = 0;
            state['level' + '_error'] = true;
        }

         if (!this.state.pay_point) {
            isSubmit = 0;
            state['pay_point' + '_error'] = true;
        }
        this.setState(state);

        if (isSubmit) {
            toast.dismiss();
            this.setState({ submit_form: false }, () => {
                postData('member/MemberDashboard/save_member_position_award', { data: this.state, 'memberId': this.props.memberId, 'mode': this.props.mode, 'full_name': this.props.full_name, id: this.props.id }).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                        // toast.success(result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.getMemberPositionAward();
                        this.setState(this.initialState);
                        this.closeClear();
                        setTimeout(() => this.setState({ success: true }), 500);
                        this.setState({ submit_form: true });
                    } else {
                        toast.error(<ToastUndo message={result.msg} showType={'e'} />, {
                        // toast.error(result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ submit_form: true });
                    }
                });
            });
        }
    }

    closeClear = () => {
        this.setState({ work_area: '', award: '', level: '',pay_point:'' });
        this.props.closePopup();
    }

    callHtmlForValidation = (currentState) => {
        return (this.state[currentState + '_error']) ? <div className={'tooltip fade top in' + ((this.state[currentState + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">This field is required.</div></div> : '';
    }

    render() {
        return (
            <div>
                <Modal className="modal fade Modal_A  Modal_B" show={this.props.isModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                    <Modal.Body>
                        <div className="dis_cell">
                            <div className="text text-left Popup_h_er_1"><span>{(this.props.mode == 'add') ? 'Add' : 'Update'} New Position/Award:</span>
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.closeClear()}><i className="icon icon-cross-icons"></i></a>
                            </div>

                            <h4 className="P_25_T h4_edit__">Please select 'Work area' being added</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="work_area" required={true} simpleValue={true} searchable={false} clearable={false} options={memberWorkArea(0)} value={this.state.work_area} onChange={(e) => this.selectChange(e, 'work_area')} />
                                        {this.callHtmlForValidation('work_area')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>

                            <h4 className="P_25_T h4_edit__">Select the 'Awards' for this position</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="award" required={true} simpleValue={true} searchable={false} clearable={false} options={memberPositionAward(0)} value={this.state.award} onChange={(e) => this.selectChange(e, 'award')} />
                                        {this.callHtmlForValidation('award')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>

                            <h4 className="P_25_T h4_edit__">Select the 'Level' for this position</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="level" required={true} simpleValue={true} searchable={false} clearable={false} options={this.state.levels_list} value={this.state.level} onChange={(e) => this.selectChange(e, 'level')} />
                                        {this.callHtmlForValidation('level')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>

                             <h4 className="P_25_T h4_edit__">Please select 'Pay point' being added</h4>
                            <div className="row P_15_T">
                                <div className="col-md-5">
                                    <span className="required">
                                        <Select className='custom_select' name="pay_point" required={true} simpleValue={true} searchable={false} clearable={false} options={this.state.paypoint_level} value={this.state.pay_point} onChange={(e) => this.selectChange(e, 'pay_point')} />
                                        {this.callHtmlForValidation('pay_point')}
                                    </span>

                                </div>
                                <div className="col-md-7"></div>
                            </div>                         

                            <div className="row">
                                <div className="col-md-7"></div>
                                <div className="col-md-5">
                                    <input type="submit" className="but" value={'Save'} name="content" disabled={(this.state.submit_form) ? false : true} onClick={(e) => this.onSubmit(e)} />
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}