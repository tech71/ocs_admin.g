import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';

import MemberAbout from './MembersAbout';
import MemberQual from './MembersQuals';
import MemberFms from './MembersFms';
import MemberWorkArea from './MembersWorkArea';
import MemberOverView from './MemberShiftOverView';
import MemberShifts from './MemberShifts';
import MemberContactHistory from './MemberContactHistory';
import MemberDashboard from './MemberDashboard';
import PageNotFound from '../PageNotFound';



import { postData, getPermission, checkItsNotLoggedIn } from '../../../service/common.js';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { setFooterColor } from '../../admin/notification/actions/NotificationAction.js';
import { connect } from 'react-redux'
import { setMemberProfileData } from './actions/MemberAction.js';
import Sidebar from '../Sidebar';
import {memberJson} from 'menujson/member_menu_json';

const menuJson = () => {
    let menu = memberJson;
    return menu;
}


class AppMember extends React.Component {
    constructor(props) {
        
        super(props);
        this.permission = (getPermission() == undefined) ? [] : JSON.parse(getPermission());
        this.state = {
            loading: false,
            firstName: '',
            subMenuShowStatus:false,
            menus:menuJson(),
            replaceData:{':id':0}
        };
    }

    getMemberName = (firstName, full_name, dwes_confirm) => {
        this.setState({ firstName: firstName + '\'s', full_name: full_name, dwes_confirm: dwes_confirm, first_Name: firstName });
    }

    permissionRediect = () => {
        checkItsNotLoggedIn();
        return <Redirect to={ROUTER_PATH+'admin/no_access'} />;
    }

    componentDidMount() {
         this.props.setFooterColor('Orange');
    }
    
    componentWillUnmount(){
        this.props.setFooterColor('');
    }
    componentWillReceiveProps(nextProps){
        if(this.state.subMenuShowStatus!= nextProps.getSidebarMenuShow.subMenuShow ||  (nextProps.getSidebarMenuShow.subMenuShow== false && this.state.subMenuShowStatus== nextProps.getSidebarMenuShow.subMenuShow)) {
            this.setState({subMenuShowStatus:nextProps.getSidebarMenuShow.subMenuShow},()=>{
                let menuState = this.state.menus;
                let obj = menuState.find(x => x.id === 'member_name');
                let objIndex = menuState.indexOf(obj);

                let objmemberDetail = menuState.find(x => x.id === 'member_details');
                let objmemberDetailIndex = menuState.indexOf(objmemberDetail);

                let objMemberShift = menuState.find(x => x.id === 'member_shift_rosters');
                let objMemberShiftIndex = menuState.indexOf(objMemberShift);

                if(nextProps.getSidebarMenuShow.subMenuShow){
                    menuState[objIndex]['linkShow'] = true;
                    menuState[objmemberDetailIndex]['linkShow'] = true;
                    menuState[objMemberShiftIndex]['linkShow'] = true;
                }else{
                    //menuState[menuState.findIndex(x => x.id == 'member_details')]['linkShow'] = false;
                    //menuState[menuState.findIndex(x => x.id == 'member_shift_rosters')]['linkShow'] = false;
                    menuState[objIndex]['linkShow'] = false;
                    menuState[objmemberDetailIndex]['linkShow'] = false;
                    menuState[objMemberShiftIndex]['linkShow'] = false;
                }
                this.setState({menus:menuState});
            });
        }
        if(this.props.MemberProfile.ocs_id !=nextProps.MemberProfile.ocs_id){
            this.setState({replaceData:{':id':nextProps.MemberProfile.ocs_id}},()=>{
                let menuState = this.state.menus;
                let obj = menuState.find(x => x.id === 'member_name');
                let objIndex = menuState.indexOf(obj);
                if(nextProps.getSidebarMenuShow.subMenuShow){
                    menuState[objIndex]['name'] = nextProps.MemberProfile.full_name;
                }
                this.setState({menus:menuState});
            });
        }

    }



    render() {
        return (
                 <section className='asideSect__ manage_top Participant_Module Orange'>
                 <Sidebar
                    heading={'Members'}
                    menus={this.state.menus}
                    subMenuShowStatus={this.state.subMenuShowStatus}
                    replacePropsData={this.state.replaceData}
                />
                    <BlockUi tag="div" blocking={this.state.loading}>
                            <div className="container-fluid fixed_size">
                                <div className="row justify-content-center d-flex">
                                <div className="col-lg-11 col-md-12 col-sm-12 col-xs-12">
                                <Switch>
                                    <Route exact path={ROUTER_PATH + 'admin/member/dashboard'} render={(props) => this.permission.access_member ? <MemberDashboard props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/about/:id(\\d+)/:page?'} render={(props) => this.permission.access_member ? <MemberAbout firstName={this.props.MemberProfile.first_name+'\'s'} full_name={this.props.MemberProfile.full_name} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/quals/:id(\\d+)/:page?'} render={(props) => this.permission.access_member ? <MemberQual firstName={this.props.MemberProfile.first_name+'\'s'} full_name={this.props.MemberProfile.full_name} dwes_confirm={this.state.dwes_confirm} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/fms/:id(\\d+)'} render={(props) => this.permission.access_member ? <MemberFms firstName={this.props.MemberProfile.first_name+'\'s'} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/work_area/:id(\\d+)/:page?'} ProfileId={this.props.props.match.params.id} render={(props) => this.permission.access_member ? <MemberWorkArea firstName={this.props.MemberProfile.first_name+'\'s'} full_name={this.props.MemberProfile.full_name} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/overview/:id(\\d+)'} render={(props) => this.permission.access_member ? <MemberOverView first_Name={this.props.MemberProfile.first_Name} firstName={this.props.MemberProfile.first_name+'\'s'} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/shifts/:id(\\d+)/:page?'} render={(props) => this.permission.access_member ? <MemberShifts firstName={this.props.MemberProfile.first_name+'\'s'} props={props} /> : this.permissionRediect()} />

                                    <Route exact path={ROUTER_PATH + 'admin/member/contact_history/:id(\\d+)'} render={(props) => this.permission.access_member ? <MemberContactHistory firstName={this.props.MemberProfile.first_name+'\'s'} props={props} /> : this.permissionRediect()} />
                                    <Route path='*' component={PageNotFound}  />

                                </Switch>
                                </div>
                                </div>
                            </div>
                    </BlockUi>
                    </section>

        );
    }
}

const mapStateToProps = state => ({
    MemberProfile: state.MemberReducer.memberProfile,
    getSidebarMenuShow: state.sidebarData
})

const mapDispatchtoProps = (dispach) => {
    return {
        getMemberProfileData: (value) => dispach(setMemberProfileData(value)),
        setFooterColor: (result) => dispach(setFooterColor(result))
    }
}

const AppMemberData = connect(mapStateToProps, mapDispatchtoProps)(AppMember)
export { AppMemberData as AppMember };