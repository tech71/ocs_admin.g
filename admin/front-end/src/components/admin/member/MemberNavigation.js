import React from 'react';
import {Link,Redirect } from 'react-router-dom';
import { connect } from 'react-redux'              
import PinModal from '../PinModal';
import { checkPin} from '../../../service/common.js';
import { setMemberActiveClass } from './actions/MemberAction.js';
import { ROUTER_PATH } from 'config.js';              

class MemberNavigation extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
                        pinModalOpen: false,
                        redirectDone:false
                    };
    }

    closeModal=()=>
    {
        this.setState({pinModalOpen:false})
    }

    checkLoginModule= ()=>{
        this.setState({pinModalOpen:true,moduleHed:'FMS Module',color:'Red_fms',pinType:1})
    }

    fmsHtml= ()=>
    {  
        if(!checkPin('fms'))
           return <li><a onClick={()=>this.checkLoginModule()} className={(this.props.Active) && this.props.Active == 'fms' ? 'active' : ''}>FMS</a ></li>
        else
            return <li><Link to={ROUTER_PATH+'admin/member/fms/'+this.props.memberId} className={(this.props.Active) && this.props.Active == 'fms' ? 'active' : ''}>FMS</Link ></li>
    }

    render() {
        if (this.state.redirectDone) {
            return <Redirect to={ROUTER_PATH+'admin/member/fms/' + this.props.memberId } />;
        }
        return (
                <div>
                    <aside className="col-lg-2 col-sm-3 col-lg-offset-1">
                        {(this.props.InnerMenu) ?
                                    <ul className="side_menu">
                                        <li><Link to={ROUTER_PATH+'admin/member/about/' + this.props.memberId} className="major_button"><i className="icon icon-back-arrow"></i> About</Link></li>
                                        <li><Link className={( (this.props.ActiveClass && this.props.ActiveClass == 'memberShiftActive')?'active':'')} onClick={()=> this.props.changeMemberNavigationClass('memberShiftActive')} to={ROUTER_PATH+'admin/member/shifts/' + this.props.memberId}>Shifts</Link></li>

                                        <li><Link className={( (this.props.ActiveClass && this.props.ActiveClass == 'contact_history')?'active':'')} onClick={()=> this.props.changeMemberNavigationClass('contact_history')} to={ROUTER_PATH+'admin/member/contact_history/' + this.props.memberId}>Contact History</Link></li>

                                        <li><Link className={ ( (this.props.ActiveClass && this.props.ActiveClass == 'overview')?'active':'')} onClick={()=> this.props.changeMemberNavigationClass('overview')} to={"/admin/member/overview/"+this.props.memberId}>Overview</Link></li>
                                    </ul> :
                                    <ul className="side_menu">
                                        <li><Link to={ROUTER_PATH+'admin/member/about/' + this.props.memberId} className={(this.props.Active) && this.props.Active == 'about' ? 'active' : ''} >About</Link ></li>
                                        <li><Link to={ROUTER_PATH+'admin/member/quals/' + this.props.memberId} className={(this.props.Active) && this.props.Active == 'quals' ? 'active' : ''}>Qualifications</Link ></li>
                                        <li><Link to={ROUTER_PATH+'admin/member/work_area/' + this.props.memberId} className={(this.props.Active) && this.props.Active == 'work_area' ? 'active' : ''}>Work Areas</Link ></li>
                                        {/*<li><a onClick={()=>this.checkLoginModule()} className={(this.props.Active) && this.props.Active == 'fms' ? 'active' : ''}>FMS</a ></li>*/}
                                        {this.fmsHtml()}
                                    </ul>}
                    </aside>

                    <PinModal 
                    color={'Red_fms'}  
                    moduleHed={this.state.moduleHed} 
                    modal_show={this.state.pinModalOpen} 
                    pinType = {this.state.pinType}
                    returnUrl={'/admin/member/fms/' + this.props.memberId}
                    closeModal={this.closeModal}
                />

                </div>
                );
    }
}

//
const mapStateToProps = state => ({
    ActiveClass : state.MemberReducer.ActiveClassProfilePage,
})

 const mapDispatchtoProps = (dispach) => {
       return {
           changeMemberNavigationClass: (value) => dispach(setMemberActiveClass(value)),
      }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MemberNavigation)