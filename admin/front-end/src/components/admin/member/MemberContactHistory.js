import React from 'react';
import { Link } from 'react-router-dom';
import { contactHistoryViewBy } from '../../../dropdown/memberdropdown.js';
import { postData } from '../../../service/common.js';
import { BASE_URL } from '../../../config.js';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { connect } from 'react-redux'
import { setMemberProfileData } from './actions/MemberAction.js';
import { CSVLink } from 'react-csv';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MemberProfile from './MemberProfile';
import { ToastUndo } from 'service/ToastUndo.js'
import {ROUTER_PATH} from 'config.js';

class MemberContactHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            msg_record_msg: [],
            call_record_msg: [],
            userSelectedList: [],
            start_date: '',
            end_date: '',
            view_by: '',
            member_ph: []
        };
    }

    componentDidMount() {
        this.getImailAndCallList();
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state, () => {
            this.getImailAndCallList();
        });
    }

    getImailAndCallList() {
        var requestData = { memberId: this.props.props.match.params.id, start_date: this.state.start_date, end_date: this.state.end_date, view_by: this.state.view_by };
        this.setState({ loading: true });
        postData('member/MemberDashboard/get_imail_call_list', requestData).then((result) => {
            if (result.status) {
                this.setState({ msg_record_msg: result.data.msg_data, call_record_msg: result.data.call_data, userSelectedList: result.data.csv_array }, () => { });
            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            this.getImailAndCallList();
        });
    }

    csvDownload = () => {
        var requestData = { userSelectedList: this.state.userSelectedList };
        postData('member/MemberAction/contact_history_export', requestData).then((result) => {
            if (result.status) {
                window.location.href = BASE_URL + "archieve/" + result.csv_url;
                //this.setState({qual:removeDownload});
            } else {
                toast.dismiss();
                // toast.error(result.error, {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });

    }

    render() {
        /*const csvHeaders = [
            {label: 'Contact Type', key: 'contact_type'},
            {label: 'Title/URL', key: 'csv_title'},
            {label: 'Date', key: 'created_date'}
        ];*/

        return (
            <div>

                <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} Active={'contact_history'} pageTypeParms="contact_history"/> 
                <div className="row">
                   <div className="col-lg-12 col-sm-12">
                 
                        
                        <div className="row mt-3">
                            <div className="col-lg-9 col-sm-8">
                                <div className="row">
                                <div className="col-md-12"><div className="bor_T"></div></div>
                                    <div className="col-md-12 P_7_TB"><h3><b>Contact with {this.props.MemberProfile.first_name}</b></h3></div>
                                    <div className="col-md-12"><div className="bor_T"></div></div>
                                </div>
                            </div>
                            <div className="col-lg-3 col-sm-4">
                                <div className="box">
                                    <Select name="view_by" required={true} simpleValue={true} searchable={false} clearable={false} placeholder="View By" value={this.state.view_by} onChange={(e) => this.selectChange(e, 'view_by')} options={contactHistoryViewBy()} />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-9 col-sm-8">
                                <div className="row">
                                    <div className="CH_div col-sm-8">
                                        {
                                            this.state.msg_record_msg.map((value, idx) => (
                                                <div className="CH_row" key={idx + 1}>
                                                    <div className="CH_d1 color">Imail</div>
                                                    <div className="CH_d1">
                                                        <span className="CH_date">{value.created_date}</span>
                                                        <Link to={ROUTER_PATH+'admin/imail/external_imail/' + value.id} target="_blank"><i className="icon icon-notes CH_i1"></i></Link>
                                                    </div>
                                                </div>
                                            ))
                                        }

                                        {
                                            this.state.call_record_msg.map((value, idx) => (
                                                <div className="CH_row" key={idx + 1}>
                                                    <div className="CH_d1 color">Call</div>
                                                    <div className="CH_d1">
                                                        <span className="CH_date">{value.created_date}</span>
                                                        <i className="icon icon-caller-icons color CH_i2"></i>
                                                    </div>
                                                </div>
                                            ))
                                        }

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className=" no_record mt-5 py-2">{(this.state.msg_record_msg.length == 0 && this.state.call_record_msg.length == 0) ? 'No record found.' : ''}</div>
                                    </div>
                                </div>

                            </div>
                            <div className="col-lg-3  col-sm-4 ">
                                <div className="row">
                                    <div className="col-lg-6 col-md-6 P_15_TB">
                                        <label>From:</label>

                                        <DatePicker  autoComplete={'off'} isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center" placeholderText="00-00-0000" dateFormat='DD-MM-YYYY' />
                                    </div>
                                    <div className="col-lg-6 col-md-6 P_15_TB">
                                        <label>To:</label>
                                        <DatePicker autoComplete={'off'} sClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center" placeholderText="00-00-0000" minDate={moment(this.state.start_date)} dateFormat='DD-MM-YYYY' />
                                    </div>
                                </div>
                                <div>
                                <a className="but" onClick={() => this.csvDownload()} >Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-3 col-lg-offset-9 col-sm-3 col-sm-offset-9">
                        {/*<CSVLink data={this.state.userSelectedList} headers={csvHeaders} className="but" filename={ (this.state.view_by == 'phone_call') ? "member_ph_call.csv" :  ((this.state.view_by == 'iMail') ? "member_Imall.csv" : "memberImallCall.csv") }
                    onClick={(event) => {                                   
                        if(this.state.userSelectedList.length == 0)
                        {
                            toast.warning("No record to export.", {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                            }); 

                            return false;  
                        }
                    }} > 
            Export List
            </CSVLink>*/}
                       
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    MemberProfile: state.MemberReducer.memberProfile
})

const mapDispatchtoProps = (dispach) => {
    return {
        getMemberProfileData: (value) => dispach(setMemberProfileData(value)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(MemberContactHistory)