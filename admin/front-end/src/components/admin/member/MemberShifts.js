import React, { Component } from 'react';
import { postData, calendarColorCode } from '../../../service/common.js';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { BASE_URL } from 'config.js';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CSVLink } from 'react-csv';
import BigCalendar from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import Toolbar from 'react-big-calendar/lib/Toolbar';
import ShortShiftDetails from '../../admin/externl_component/ShortShiftDetails';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import MemberProfile from './MemberProfile';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';

import { connect } from 'react-redux'

const requestData = (pageSize, page, sorted, filtered, memberId) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, member_id: memberId });
        postData('member/MemberDashboard/get_member_shifts', Request).then((result) => {
            if(result.status){
                let filteredData = result.data;
                const res = {
                    rows: filteredData,
                    all_count: result.all_count,
                    pages: (result.count),
                    total_duration: result.total_duration
                };
                resolve(res);
            }
        });

    });
};


class MemberShifts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            shiftListing: [],
            counter: 0,
            selected: [],
            selectAll: 0,
            start_date: '',
            end_date: '',
            shift_status: 7, //7 upcoming shift
            userSelectedList: [],
            event_list: [],
            member_id: this.props.props.match.params.id,
            is_call_table: false,
        }
    }

    componentDidMount() {
        this.getMemberEvent(moment());
        if(this.props.showTypePage=='completed_shift'){
            this.changeTabPanel(6);
        }else if(this.props.showTypePage=='cancelled_shift'){
            this.changeTabPanel(5);
        }
    }

    getMemberEvent = (e) => {
        var member_id = this.state.member_id;
        var requestData = JSON.stringify({ member_id: member_id, default_date: e });
        postData('member/MemberDashboard/get_member_upcoming_shifts', requestData).then((result) => {
            if (result.status) {
                var tempAry = result.data;
                tempAry.map((value, idx) => {
                    tempAry[idx]['end'] = value.end
                    tempAry[idx]['start'] = value.start
                })
                this.setState({ event_list: tempAry, total_duration: result.total_duration });
            } else {
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    eventStyleGetter(event, start, end, isSelected) {
        var backgroundColor = calendarColorCode(event.status);
        var style = {
            backgroundColor: backgroundColor,
        };
        return {
            style: style
        };
    }

    closeModel = () => {
        this.setState({ openShit: false, shiftId: '' })
    }

    openSortShiftDetails = (event) => {
        this.setState({ selelctShiftId: event.id, openShit: true })
    }

    fetchData = (state, instance) => {
        if (this.state.is_call_table) {
            this.setState({ loading: true });
            requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.id
            ).then(res => {
                this.setState({
                    shiftListing: res.rows,
                    pages: res.pages,
                    all_count: res.all_count,
                    loading: false,
                    total_duration: res.total_duration,
                    selectAll: 0,
                    userSelectedList: [],
                    selected: [],
                });
            })
        }
    }

    changeTabPanel = (panel) => {
        this.setState({ shift_status: panel, is_call_table: true }, () => {
            var tempfilter = { search_box: '', shift_type: '', shift_date: '', start_date: '', end_date: '', shift_status: this.state.shift_status, userSelectedList: [], selected: [] }
            this.setState(tempfilter);
            this.setState({ filtered: tempfilter })
        })

    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        let selectedList = this.state.shiftListing;
        var userSelectedList = this.state.userSelectedList;

        var columnIndex = selectedList.findIndex(x => x.id == id);
        if (newSelected[id]) {
            this.setState({
                userSelectedList: userSelectedList.concat(selectedList[columnIndex]),
            });
        }
        else {
            var tempState = {};
            var selectedColumnIndex = userSelectedList.findIndex(x => x.id == id);
            tempState['userSelectedList'] = userSelectedList.filter((s, sidx) => selectedColumnIndex !== sidx);
            this.setState(tempState);
        }

        this.setState({
            selected: newSelected,
            selectAll: 2
        }, () => { });
    }

    toggleSelectAll = () => {
        let newSelected = {};
        let selectedList = this.state.shiftListing;
        var userSelectedList = this.state.userSelectedList;

        if (this.state.selectAll === 0) {
            this.state.shiftListing.forEach(x => {
                newSelected[x.id] = true;
            });
        }

        this.setState({
            userSelectedList: selectedList,
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = { search_box: this.state.search_box, shift_type: this.state.shift_type, shift_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date, shift_status: this.state.shift_status }
            this.setState({ filtered: filter });
        });
    }

    csvDownload = () => {
        var requestData = { userSelectedList: this.state.userSelectedList, active_tab: this.state.shift_status };
        postData('member/MemberAction/member_shift_export', requestData).then((result) => {
            if (result.status) {
                this.setState({ userSelectedList: [] });
                window.location.href = BASE_URL + "archieve/" + result.csv_url;
                //this.setState({qual:removeDownload});
            } else {
                toast.dismiss();
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

    componentWillReceiveProps(nextProps){
        let shfitType=['completed_shift','cancelled_shift'];
        if(this.props.showTypePage!=undefined && this.props.showTypePage!=nextProps.showTypePage && shfitType.indexOf(nextProps.showTypePage) > -1){
            if(nextProps.showTypePage=='completed_shift'){
                this.changeTabPanel(6);
            }else if(nextProps.showTypePage=='cancelled_shift'){
                this.changeTabPanel(5);
            }
        }
    }

    render() {
        moment.locale('ko', { week: { dow: 1, doy: 1, }, });
        const localizer = BigCalendar.momentLocalizer(moment) // or globalizeLocalizer 

        {/*const headers = [
                            {label: 'Shift Date', key: 'shift_date'},
                            {label: 'Participant', key: 'participantName'},
                            {label: 'Start', key: 'start_time'},
                            {label: 'Duration', key: 'duration'},
                            {label: 'Status', key: 'status'},
                        ];*/}

        const columns = [{
            id: "checkbox", accessor: "", maxWidth:55, textAlign:'center',
            headerClassName: 'Th_class_d1',
            className: (this.state.activeCol === 'checkbox') && this.state.resizing ? 'borderCellCls' : '_align_c__',
            Cell: ({ original }) => {
                return (
                 <span className="Set_Sec___ pl-2">
                 <label class="Cus_Check_1">
                     <input type="checkbox" checked={this.state.selected[original.id] === true} onChange={() => this.toggleRow(original.id)} />
                     <div class="chk_Labs_1"></div>
                 </label>
                 </span>
                );

            },
            Header: x => {
                return (
                  
                     <span className="Set_Sec___ pl-2">
                     <label class={"Cus_Check_1 "+(this.state.selectAll === 2?"minus_select__":'')}>
                     <input type='checkbox' checked={this.state.selectAll === 1} ref={input => {
                            if (input) { input.indeterminate = this.state.selectAll === 2 }
                        }}
                            onChange={() => this.toggleSelectAll()} />
                         <div class="chk_Labs_1"></div>
                     </label>
                     </span>
                );
            },
            sortable: false,

        },
        { Header: 'Shift Date', accessor: 'shift_date', filterable: false, maxWidth:125,
        headerClassName: 'Th_class_d1',
        className: (this.state.activeCol === 'Shift Date') && this.state.resizing ? 'borderCellCls' : '_align_l__',
        Cell: props => <span>
        {props.original.shift_date}</span>
        },
       
            { Header: 'Duration ', accessor: 'duration', filterable: false, maxWidth:135,
            headerClassName: 'Th_class_d1',
            className: (this.state.activeCol === 'Duration') && this.state.resizing ? 'borderCellCls' : '_align_l__',
            Cell: props => <span>
            {props.original.duration}</span>
         },
        { Header: 'Participant', accessor: 'shift_for', filterable: false,
        headerClassName: 'Th_class_d1',
        className: (this.state.activeCol === 'Participant') && this.state.resizing ? 'borderCellCls' : '_align_l__',
        Cell: props => <span>
        {props.original.shift_for}</span>
     },
       
        { Header: 'Status ', accessor: 'status', sortable: false , maxWidth:125,
        headerClassName: 'Th_class_d1',
        className: (this.state.activeCol === 'Status') && this.state.resizing ? 'borderCellCls' : '_align_l__',
        Cell: props => <span>
        {props.original.status}</span>
     },
        {
            Cell: (props) => <span></span>,
            Header: <TotalShowOnTable countData={this.state.all_count} />, maxWidth:155,
            style: {
                "textAlign": "right",
            }, headerStyle: { border: "0px solid #fff" }, sortable: false
        },
        ]

        return (
            <div>

                <MemberProfile MemberData={this.state} ProfileId={this.props.props.match.params.id} Active={'memberShiftActive'}  pageTypeParms={this.props.props.match.params.page}/>

                <div className="row">

                    <div className="col-lg-12 col-sm-12">
                        <div className="tab-content">
                            <div role="tabpanel" className={this.props.showTypePage=='upcoming_shift' ? "tab-pane active" :"tab-pane"} id="calendar">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 mb-3">
                                        <div className="row">
                                        <div className="col-md-12"><div className="bor_T"></div></div>
                                            <div className="col-md-12 P_7_TB"><h3><b>
                                            {this.props.firstName} 
                                            Upcoming Shifts</b></h3></div>
                                            <div className="col-md-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <div className="member_shift">

                                            <div id="example-component">
                                                <div>
                                                <BigCalendar
                                                    events={this.state.event_list}
                                                    localizer={localizer}
                                                    views={['month']}
                                                    eventPropGetter={(this.eventStyleGetter)}
                                                    onNavigate={this.getMemberEvent}
                                                    components={{ toolbar: CalendarToolbar }}
                                                    //defaultDate={this.state.default_date}
                                                    //formats={formats}
                                                    onSelectEvent={(event) => this.openSortShiftDetails(event)}
                                                />
                                                <ShortShiftDetails shiftId={this.state.selelctShiftId} openShit={this.state.openShit} closeModel={this.closeModel} />
                                                </div>

                                                 <div className="row px-5">
                                                    <div className="col-md-8 mt-5">
                                                        <ul className="status_new">
                                                            <li><span className="Confirmed"></span>Confirmed</li>
                                                            <li><span className="Unconfirmed"></span>Unconfirmed</li>
                                                            {/*<li><span className="Cancelled"></span>Cancelled</li>*/}
                                                        </ul>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-12 text-right total_div">
                                            <h3 className="P_7_b"><span className="Total_text">Total Hours:</span></h3>
                                            <h3><b>{(this.state.total_duration && this.state.total_duration > 1) ? this.state.total_duration : 0}</b></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" className={this.props.showTypePage=='completed_shift' || this.props.showTypePage=='cancelled_shift' ? "tab-pane active" :"tab-pane"} id="Barry_details">
                                <div className="row">
                                    <div className="col-lg-12 col-md-12 mb-3">
                                        <div className="row">
                                        <div className="col-md-12"><div className="bor_T"></div></div>
                                            <div className="col-md-12 P_7_TB"><h3><b> {(this.state.shift_status == 6) ? 'Shifts Completed by' : 'Cancelled Shifts by'} {this.props.firstName}</b></h3></div>
                                            <div className="col-md-12"><div className="bor_T"></div></div>
                                        </div>
                                    </div>
                                    <div className="col-lg-9 col-sm-7">
                                        <label>Search</label>
                                        <div className="table_search_new">
                                            <input type="text" onChange={(e) => this.searchBox('search_box', e.target.value)} value={this.state.search_box || ''} />
                                            <button type="submit">
                                                <span className="icon icon-search"></span>
                                            </button>
                                        </div>
                                    </div>

                                    <div className="col-lg-3 col-sm-5">
                                        <div className="row">

                                            <div className="col-lg-6 col-sm-6">
                                                <label>From</label>
                                                <DatePicker autoComplete={'off'} isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00-00-0000" />
                                            </div>

                                            <div className="col-lg-6 col-sm-6">
                                                <label>To</label>
                                                <DatePicker autoComplete={'off'} isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00-00-0000" minDate={moment(this.state.start_date)} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="schedule_listings th_padding-x px_text__ mt-2">
                                    <ReactTable
                                      PaginationComponent={Pagination}
                                        columns={columns}
                                        manual
                                        data={this.state.shiftListing}
                                        pages={this.state.pages}
                                        loading={this.state.loading}
                                        onFetchData={this.fetchData}
                                        filtered={this.state.filtered}
                                        defaultFiltered={{ shift_status: 7 }}
                                        defaultPageSize={10}
                                        className="-striped -highlight"
                                        noDataText="No Record Found"
                                        minRows={2}
                                        previousText={<span className="icon icon-arrow-left privious"></span>}
                                        nextText={<span className="icon icon-arrow-right next"></span>}
                                        showPagination={this.state.shiftListing.length > PAGINATION_SHOW ? true : false }
                                    />
                                </div>

                                <div className="row">
                                    <div className="col-md-12 text-right total_div">
                                        <h3 className="P_7_b"><span className="Total_text">Total Hours:</span></h3>
                                        <h3><b>{this.state.total_duration}</b></h3>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-2 col-lg-offset-10 col-md-3 col-md-offset-9">
                                        {/* <CSVLink data={this.state.userSelectedList} headers={headers} className="but" filename={ (this.state.shift_status == 7) ? "upcoming_shift.csv" :  ((this.state.shift_status == 8) ? "completed_shift.csv" : "cancelled_shift.csv") }
                                onClick={(event) => {                                   
                                    if(this.state.userSelectedList.length == 0)
                                    {
                                        toast.dismiss();
                                        toast.warning("Please select atleast one shift to export.", {
                                            position: toast.POSITION.TOP_CENTER,
                                            hideProgressBar: true
                                        }); 

                                        return false;  
                                    }
                                }} >
                                Export Selected
                            </CSVLink>*/}
                                        <a className="but mt-3" onClick={() => this.csvDownload()} > Export Selected</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.MemberReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(MemberShifts);

class CalendarToolbar extends Toolbar {
    componentDidMount() {
        const view = this.props.view;
    }

    render() {
        return (
            <div>
                <div className="rbc-btn-group">
                <span className="" onClick={() => this.navigate('TODAY')} >Today</span>
                    <span className="icon icon-arrow-left" onClick={() => this.navigate('PREV')}></span>
                    <span className="icon icon-arrow-right" onClick={() => this.navigate('NEXT')}></span>
                </div>
                <div className="rbc-toolbar-label">{this.props.label}</div>
            </div>
        );
    }
}
