import React, { Component } from 'react';

import MemberAbout from './MembersAbout';
import MemberProfile from './MemberProfile';
import 'react-table/react-table.css'
import BlockUi from 'react-block-ui';
import  'react-block-ui/style.css';

class Member extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            loading:true,
        };
       
    }

render() {	
	return (
		<React.Fragment>
		 
		   <BlockUi tag="div" blocking={this.state.loading}>

                <div className="row  _Common_back_a">
                    <div className="col-lg-12"><img className="back_icons" src={'/assets/images/Members_icons/back_arrow.svg '} /></div>
                </div>
                <div className="row"><div className="col-lg-12"><div className="bor_T"></div></div></div>
               {(!this.state.loading)?<MemberProfile MemberData ={this.state}/>:''}
               {(!this.state.loading)?<MemberAbout MemberData ={this.state}/>:''}
               
                
          
        </BlockUi>
		</React.Fragment>
		);
	}
}
export default Member;

