import React, { Component } from 'react';
import { orgSiteType } from '../../../dropdown/Orgdropdown.js';
import jQuery from "jquery";
import { BASE_URL } from '../../../config.js';
import { postData, getOptionsSuburb, handleChangeChkboxInput, handleChangeSelectDatepicker, handleShareholderNameChange, handleAddShareholder, postImageData, selectFilterOptions, getStateList, handleRemoveShareholder, queryOptionData } from '../../../service/common.js';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import Modal from 'react-bootstrap/lib/Modal'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import { setOrgBookingData } from './actions/OrganisationAction.js';
import {OrganisationPageIconTitle} from 'menujson/pagetitle_json';
import { ToastUndo } from 'service/ToastUndo.js'

const getOrganisation = (input) => {
    return queryOptionData(input,'common/Common/get_org_name',{query: input},4);
}

const site_detail_ary = { firstname: '', 'lastname': '', 'position': '', 'department': '', 'sitePh': [{ 'phone': '' }], 'siteMail': [{ 'email': '' }] };

class OrganisationUpdatePopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            SitePhone: [{ 'phone': '' }],
            SiteEmail: [{ 'email': '' }],
            siteDetails: [JSON.parse(JSON.stringify(site_detail_ary))],
            loading: false
        }
    }

    componentWillReceiveProps(newProps) {
        
    }

    selectChange = (selectedOption, fieldname) => {
        var state = {};
        state[fieldname] = selectedOption;
        if (fieldname == 'city' && selectedOption)
            state['postal'] = state.city.postcode;

        this.setState(state);
    }

    handleAddHtml = (e, tagType, object_array) => {
        e.preventDefault();
        var state = {};
        var list = this.state[tagType];
        var x = JSON.parse(JSON.stringify(site_detail_ary));
        state[tagType] = list.concat([x]);
        this.setState(state);
    }

    handleRemoveHtml = (e, idx, tagType) => {
        e.preventDefault();
        var state = {};
        var List = this.state[tagType];
        state[tagType] = List.filter((s, sidx) => idx !== sidx);
        this.setState(state);
    }

    componentDidMount() {
        this.setState(this.props.selectedData,()=>{ 
            let stateadd = {};

            if(this.props.hasOwnProperty('site_id') && this.props.site_id != null && this.props.site_id!=''){
                stateadd['site_id'] = this.props.site_id;
            }

            if(this.props.hasOwnProperty('mode') && this.props.mode != null && this.props.mode!=''){
                stateadd['mode'] = this.props.mode;
            }

            
            
            if(Object.keys(stateadd).length>0){
                 this.setState(stateadd);
            }
          });

        this.setState({orgId:this.props.orgId,callType:this.props.callType});
        getStateList().then((result) => {
            var stateList = result;
            this.setState({ stateList: stateList }, () => { });
        });
    }

    onSubmit = (e) => {
        e.preventDefault()
        jQuery('#updateAddress').validate({ ignore: [] });
        if (jQuery('#updateAddress').valid()) {
            this.setState({ loading: true }, () => {
                postData('organisation/OrgDashboard/save_site', JSON.stringify(this.state)).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                        // toast.success(result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.closeModal();
                    }
                    else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }

    orgOnChange = (e) => {
        var state = {};
        state['organisation_name'] = e;
        state['organisation_abn'] = e.abn;
        this.setState(state, () => { });
    }

    closeModal = () => {
        this.props.closeModal();
        /*this.setState({
            title: '', site_type: '', site_address: '', city: '', state: '', postal: '', abn: '', organisation_name: '',
            SitePhone: [{ 'phone': '' }],
            SiteEmail: [{ 'email': '' }],
            siteDetails: [{ 'firstname': '', 'lastname': '', 'position': '', 'department': '', 'sitePh': [{ 'phone': '' }], 'siteMail': [{ 'email': '' }] }]
        });*/
    }

    handleAddShareholderDepth(obj, e, stateName, object_array, main_obj, idxx, input_type) {
        e.preventDefault();
        var state = {};
        var list = obj.state[stateName];
        var list_temp = list[idxx][main_obj];

        if (input_type == 'phone')
            list_temp = list_temp.concat([{ 'phone': '' }]);
        else
            list_temp = list_temp.concat([{ 'email': '' }]);

        list[idxx][main_obj] = list_temp;
        state[stateName] = list;
        obj.setState(state);
    }

    handleRemoveShareholderDepth(obj, e, index, stateName, main_obj, idxx, input_type) {
        e.preventDefault();
        var state = {};
        var List = obj.state[stateName];
        var list_temp = List[idxx][main_obj];
        var removedata = list_temp.filter((s, sidx) => index !== sidx);
        List[idxx][main_obj] = removedata;
        state[stateName] = List;
        obj.setState(state);
    }

    handleShareholderNameChangeDepth(main_incr, incr, fieldName, value) {
        var state = {};
        var List = this.state.siteDetails;
        if (fieldName == 'phone')
            List[main_incr]['sitePh'][incr][fieldName] = value
        else
            List[main_incr]['siteMail'][incr][fieldName] = value

        state['siteDetails'] = List;
        this.setState(state);
    }

    render() {
        return (
            <Modal
                className="Modal fade Modal_A Modal_B Green O_Modal_size"
                show={this.props.modal_show}
                container={this}
                aria-labelledby="contained-modal-title"
            >

                <Modal.Body>
                    <form id="updateAddress">
                        <div className="dis_cell-1">
                            <div className="text text-left by-1 Popup_h_er_1">Add Existing Sites/Sub-Orgs:
                        <a onClick={() => this.closeModal()} className="close_i"><i className="icon icon-cross-icons"></i></a>
                            </div>

                            {(this.state.callType && this.state.callType == 1)?
                            <div className="row P_25_T">
                                <div className="col-lg-9 col-sm-9">
                                    <label>Organisation Name (Legal):</label>
                                    <div className="search_icons_right modify_select modify_select_big">
                                        <Select.Async
                                            name="organisation_name"
                                            loadOptions={(e) => getOrganisation(e)}
                                            clearable={false}
                                            placeholder='Search'
                                            value={this.state.organisation_name}
                                            onChange={(e) => this.orgOnChange(e)}
                                            className="default_validation search_icons_right modify_select"
                                            filterOptions={selectFilterOptions}
                                            cache={false}
                                        />
                                    </div>
                                </div>
                            </div>
                            :''}

                            <div className="row mt-4 mx-0">
                                <div className="col-sm-12 py-3 bb-1 px-0">
                                    <h3 className="color">New Site - Details:</h3>
                                </div>
                            </div>

                            <div className="row P_15_T" >

                                <div className="col-sm-2">
                                    <label>Type: </label>
                                    <span className="required default_validation">
                                        <Select name='site_type' required simpleValue={true} searchable={true} clearable={false} value={this.state.site_type} placeholder="Types" options={orgSiteType()} onChange={(e) => this.setState({ 'site_type': e })} />
                                    </span>
                                </div>

                                <div className="col-sm-3">
                                    <label>Title: </label>
                                    <span className="required">
                                        <input placeholder="Title" className="text-left" type="text" name={'title'} value={this.state.title} data-rule-required="true" autoComplete="new-password" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                    </span>
                                </div>

                                <div className="col-sm-3">
                                    {this.state.SitePhone.map((value, idx) => (
                                        <div className="mb-4" key={idx + 1}>
                                            <label>Phone ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                            <span className="required">
                                                <div className="input_plus__ ">
                                                    <input type="text" className="input_f distinctPh" placeholder="Can Include Area Code" value={value.phone} name={'org_phone_' + idx} required onChange={(e) => handleShareholderNameChange(this, 'SitePhone', idx, 'phone', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctPh"]' data-rule-phonenumber data-msg-notequaltogroup="Please enter a unique contact number" />
                                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'SitePhone')} >
                                                        <i className="icon icon-decrease-icon Add-2" ></i>
                                                    </button> : (this.state.SitePhone.length == 3) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'SitePhone', value)}>
                                                        <i className="icon icon-add-icons Add-1" ></i>
                                                    </button>
                                                    }
                                                </div>
                                            </span>
                                        </div>
                                    ))}
                                </div>

                                <div className="col-sm-3">
                                    {
                                        this.state.SiteEmail.map((value, idx) => (
                                            <div  className="mb-4" key={idx + 1}>
                                                <label>Email ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                                <span className="required">
                                                    <div className="input_plus__">
                                                        <input type="text" className="input_f distinctEmail" placeholder="example@example.com" value={value.email} name={'org_email_' + idx} required onChange={(e) => handleShareholderNameChange(this, 'SiteEmail', idx, 'email', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]' />
                                                        {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'SiteEmail')} >
                                                            <i className="icon icon-decrease-icon Add-2" ></i>
                                                        </button> : (this.state.SiteEmail.length == 3) ? '' : <button onClick={(e) => handleAddShareholder(this, e, 'SiteEmail', value)}>
                                                            <i className="icon icon-add-icons Add-1" ></i>
                                                        </button>
                                                        }
                                                    </div>
                                                </span>
                                            </div>
                                        ))}
                                </div>
                            </div>

                            <div className="row P_15_T mb-5" >
                                <div className="col-sm-4">
                                    <label>Site Address: </label>
                                    <span className="required">
                                        <input type="text" name="site_address" data-rule-required="true" onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state.site_address} placeholder="" />
                                    </span>
                                </div>
                                <div className="col-sm-2">
                                    <label>State: </label>
                                    <Select clearable={false} className="default_validation" simpleValue={true} required={true}
                                        value={this.state.state} onChange={(e) => handleChangeSelectDatepicker(this, e, 'state')}
                                        options={this.state.stateList} placeholder="Please Select" required />
                                </div>
                                <div className="col-sm-2 modify_select">
                                    <label>Suburb:</label>
                                    <span className="required">
                                        <Select.Async clearable={false} cache={false} className="default_validation"
                                            value={this.state.city} disabled={(this.state.state) ? false : true} loadOptions={(val) => getOptionsSuburb(val, this.state.state)} onChange={(e) => this.selectChange(e, 'city')}
                                            placeholder="Please Select" required />
                                    </span>
                                </div>

                                <div className="col-sm-2">
                                    <label>Postcode: </label>
                                    <span className="required">
                                        <input type="text" value={this.state.postal} onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-required="true" className="text-center" name="postal" placeholder="" data-rule-number="true" minLength="4" maxLength="4" data-rule-postcodecheck="true"/>
                                    </span>
                                </div>

                                <div className="col-sm-2">
                                    <label>ABN: </label>
                                    <span className="required">
                                        <input type="text" value={this.state.abn} onChange={(e) => handleChangeChkboxInput(this, e)}  className="text-left" name="abn" placeholder="" data-rule-required="true" data-rule-number minLength="11" maxLength="11" data-msg-number="Please enter 11 digit abn number" data-msg-minlength="Please enter 11 digit abn number" data-msg-maxlength="Please enter 11 digit abn number"/>
                                    </span>
                                </div>
                            </div>

                            {this.state.siteDetails.map((value, idxx) => (

                                <div key={idxx}>
                                    <div className="row mt-4 mx-0">
                                        <div className="col-sm-12 py-3 by-1 px-0">
                                            <h3 className="color">Site Key Contact ({idxx == 0 ? 'Primary' : 'Secondary'})  - Details:</h3>
                                        </div>
                                    </div>

                                    <div className="row P_15_T d-flex flex-wrap after_before_remove" >
                                        <div className="col-sm-3 mb-5 align-self-end">
                                            <label>Name: </label>
                                            <span className="required">
                                                <input type="text" value={value.firstname} name={'firstname' + idxx} onChange={(e) => handleShareholderNameChange(this, 'siteDetails', idxx, 'firstname', e.target.value)} data-rule-required="true" className="text-left" placeholder="first" autoComplete="new-password" />
                                            </span>
                                        </div>

                                        <div className="col-sm-3 mb-5 align-self-end">
                                            <label></label>
                                            <span className="required">
                                                <input type="text" value={value.lastname} name={'lastname' + idxx} onChange={(e) => handleShareholderNameChange(this, 'siteDetails', idxx, 'lastname', e.target.value)} data-rule-required="true" className="text-left" placeholder="last" autoComplete="new-password" />
                                            </span>
                                        </div>

                                        <div className="col-sm-3 mb-5 align-self-end">
                                            <label>Position: </label>
                                            <span className="required">
                                                <input type="text" value={value.position} name={'position' + idxx} onChange={(e) => handleShareholderNameChange(this, 'siteDetails', idxx, 'position', e.target.value)} data-rule-required="true" className="text-left" placeholder="EG, CEO" autoComplete="new-password" />
                                            </span>
                                        </div>

                                        <div className="col-sm-3 mb-5 align-self-end">
                                            <label>Department</label>
                                            <span className="required">
                                                <input type="text" value={value.department} name={'department' + idxx} onChange={(e) => handleShareholderNameChange(this, 'siteDetails', idxx, 'department', e.target.value)} data-rule-required="true" className="text-left" placeholder="EG, Accounts" autoComplete="new-password" />
                                            </span>
                                        </div>

                                        <div className="col-sm-3 mb-5 align-self-start" >
                                            {value.sitePh.map((val, idx) => (
                                                <div className="mb-3" key={idx + 1}>
                                                    <label>Phone ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                                    <span className="required">
                                                        <div className="input_plus__">
                                                            <input type="text" className="input_f distinctPh" placeholder="Can Include Area Code" value={val.phone} name={'org_phone' + idx} required onChange={(e) => this.handleShareholderNameChangeDepth(idxx, idx, 'phone', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctPh"]' data-rule-phonenumber data-msg-notequaltogroup="Please enter a unique contact number"/>
                                                            {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholderDepth(this, e, idx, 'siteDetails', 'sitePh', idxx, 'phone')} >
                                                                <i className="icon icon-decrease-icon Add-2" ></i>
                                                            </button> : (value.sitePh.length == 3) ? '' : <button onClick={(e) => this.handleAddShareholderDepth(this, e, 'siteDetails', value, 'sitePh', idxx, 'phone')}>
                                                                <i className="icon icon-add-icons Add-1" ></i>
                                                            </button>
                                                            }
                                                        </div>
                                                    </span>
                                                </div>
                                            ))}
                                        </div>

                                        <div className="col-sm-3 mb-5 align-self-start">
                                            {
                                                value.siteMail.map((val, idx) => (
                                                    <div className="mb-3" key={idx + 1}>
                                                        <label>Email ({(idx == 0) ? 'Primary' : 'Secondary'}):</label>
                                                        <span className="required">
                                                            <div className="input_plus__">
                                                                <input type="text" className="input_f distinctEmail" placeholder="example@example.com" value={val.email} name={'org_email' + idx} required onChange={(e) => this.handleShareholderNameChangeDepth(idxx, idx, 'email', e.target.value)} autoComplete="new-password" data-rule-notequaltogroup='[".distinctEmail"]' />
                                                                {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholderDepth(this, e, idx, 'siteDetails', 'siteMail', idxx, 'email')} >
                                                                    <i className="icon icon-decrease-icon Add-2" ></i>
                                                                </button> : (value.siteMail.length == 3) ? '' : <button onClick={(e) => this.handleAddShareholderDepth(this, e, 'siteDetails', value, 'siteMail', idxx, 'email')}>
                                                                    <i className="icon icon-add-icons Add-1" ></i>
                                                                </button>
                                                                }
                                                            </div>
                                                        </span>
                                                    </div>
                                                ))}
                                        </div>

                                        <div className="col-sm-3 mb-5  align-self-end">
                                            {idxx > 0 ? <button title={OrganisationPageIconTitle.org_remove_site_Primary_details} onClick={(e) => this.handleRemoveHtml(e, idxx, 'siteDetails')} className="button_plus__ mt-4 mb-3">
                                                <i className="icon icon-decrease-icon Add-2-2" ></i>
                                            </button> : (this.state.siteDetails.length == 3) ? '' : <button  title={OrganisationPageIconTitle.org_add_site_Primary_details} className="button_plus__ mt-4 mb-3" onClick={(e) => this.handleAddHtml(e, 'siteDetails', value)}>
                                                <i className="icon icon-add-icons Add-2-1" ></i>
                                            </button>}
                                        </div>

                                    </div>
                                </div>
                            ))}

                            <div className="row">
                                <div className="col-sm-8 col-lg-9"></div>
                                <div className="col-sm-4  col-lg-3 P_15_T">
                                    <button disabled={this.state.loading} onClick={this.onSubmit} className="but_submit">Save Changes</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </Modal.Body>
            </Modal>

        )
    }
}

const mapStateToProps = state => ({
    OrganisationProfile: state.OrganisationReducer.orgProfile
})

const mapDispatchtoProps = (dispach) => {
    return {
        org_update_booking_comp_fun: (value) => dispach(setOrgBookingData(value)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(OrganisationUpdatePopUp)
