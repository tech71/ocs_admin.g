import React from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../../config.js';
import { postData,checkItsNotLoggedIn,archiveALL,reFreashReactTable } from '../../../../service/common.js';
import OrganisationAddContactPopUp from '../OrganisationAddContactPopUp';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import {siteViewBy} from '../../../../dropdown/Orgdropdown.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile,customHeading } from '../../../../service/CustomContentLoader.js';
import 'react-toastify/dist/ReactToastify.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { connect } from 'react-redux';
import Pagination from "../../../../service/Pagination.js";
import { getHouseProfile } from '../actions/OrganisationAction.js';
import { setActiveSelectPage } from './../actions/OrganisationAction.js'

const requestData = (pageSize, page, sorted, filtered,orgId,houseId,type) => {
    return new Promise((resolve, reject) => {
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,orgId: orgId, houseId: houseId,type:type});
        postData('organisation/OrgDashboard/get_house_contact', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

class SubOrganisationContactBilling extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            sites_list:[],
            view_by_status:0,
             modal_show:false,
             pageTitle:null,
             active_panel:'site_contacts',
             active_panel_type:'3'
        };
        this.reactTable = React.createRef();

    }

    changeTabPanel = (panel) => {
        this.setState({ active_panel: panel }, () => {
            let requestData = {archive:'0'};
            let type = this.state.active_panel =='site_contacts' ? 3 : 4;
            this.setState({ view_by_status:0,active_panel_type:type,filtered:requestData },function(){
                reFreashReactTable (this,'fetchData');
            });
        })

    }

    fetchData = (state, instance) => {
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.props.match.params.orgId,
                this.props.props.match.params.houseId,
                this.state.active_panel_type
                ).then(res => {
            this.setState({
                sites_list: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    archived(id){ 
        var site_id = this.props.props.match.params.houseId;
        archiveALL({id:id,site_id:site_id},'','organisation/OrgDashboard/archive_organisation_site_key_contact').then((result) => {
            if(result.status){
                var requestData = {archive:'0'};
                this.setState({filtered:requestData},()=>{
                    if(this.state.active_panel_type=='3'){
                        this.props.getHouseProfile(this.props.props.match.params);
                    }
                    
                });
            }
        })
    }

    componentDidMount() { 
         this.props.setActiveSelectPage('suborg_house_overview');
    }

    viewByselectChange(selectedOption, fieldname) { 
       var requestData = {archive:selectedOption};
       this.setState({filtered:requestData,view_by_status:selectedOption});       
    }  

    closeEditPopUp=(param)=>{
        if(param)
        reFreashReactTable (this,'fetchData');

        this.setState({modal_show:false});
    } 


    render() {  
        const columns = [
            {Header: '', accessor: 'id', filterable: false,Cell: (props) => <span>{props.original.key_contact}</span>, Header: <span></span>} ,
            {Cell: (props) => <span></span>, Header: <span></span>, style: {
                    "textAlign": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, headerClassName: 'margin_right_side', sortable: false}, 
                   {expander : true, sortable: false,
                       Expander: ({ isExpanded, ...rest }) => 
                       <div>{isExpanded? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
                    headerStyle: {border:"0px solid #fff" },
            }
        ]

     return (
            <div>             
                   

                <div className="row">
                
                <div className="col-lg-12">
                <div className="row">
                <div className="col-sm-12"><div className="bor_T"></div></div>
                    <ul className="nav nav-tabs Category_tap col-lg-8 col-lg-offset-2 col-sm-8 col-sm-offset-2 P_20_TB" role="tablist">

                        <li role="presentation" className="col-lg-6 col-sm-6 active">
                        <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(60)} ready={!this.state.loading}>
                            <a  href="#site_contacts" aria-controls="site_contacts" role="tab" data-toggle="tab" onClick={() => this.changeTabPanel('site_contacts')}>Contacts</a>
                        </ReactPlaceholder>
                        </li>
                        
                        <li role="presentation" className="col-lg-6 col-sm-6 ">
                           <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customHeading(60)} ready={!this.state.loading}>
                           <a href="#site_billing" aria-controls="site_billing" role="tab" data-toggle="tab" onClick={() => this.changeTabPanel('site_billing')}>Billing</a>
                           </ReactPlaceholder>
                        </li>
                    </ul>
                    <div className="col-sm-12 P_25_b"><div className="bor_T"></div></div>
                </div>
                    <div className="tab-content">
                       
                        <div role="tabpanel" className="tab-pane active" id={this.state.active_panel}>
                            <div className="row">
                               <div className="col-lg-9 col-sm-8">
                                    <div className="P_7_TB"><h3>Contact at '{this.props.OrganisationProfile.name}':</h3></div>
                                    <div className="bor_T"></div>
                                </div>

                                <div className="col-lg-3 col-sm-4">
                                    <div className="box">
                                         <Select name="view_by_status" required={true} simpleValue={true} searchable={false} clearable={false} options={siteViewBy()} placeholder="View By" value={this.state.view_by_status} onChange={(e)=> this.viewByselectChange(e,'view_by_status')}/>
                                    </div>
                                </div>
                            </div>

                            <div className="row P_15_TB">
                                <div className="col-lg-12 col-sm-12">
                                   
                                    <div className="schedule_listings header_none_react_table PL_site">
                                        <ReactTable
                                         PaginationComponent={Pagination} 
                                            columns={columns}
                                            manual 
                                            data={this.state.sites_list}
                                            pages={this.state.pages}
                                            loading={this.state.loading} 
                                            onFetchData={this.fetchData} 
                                            filtered={this.state.filtered}
                                            defaultFiltered ={{archive: 0}}
                                            defaultPageSize={10}
                                            className="-striped -highlight"  
                                            noDataText="No Record Found"
                                            minRows={2}
                                            ref={this.reactTable}
                                            previousText={<span className="icon icon-arrow-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-right next"></span>}
                                            SubComponent={(props) => <div className="other_conter">
                                            <div className="col-sm-10">
                                                                                        <div className="my-0">
                                                <span className="color">Phone: </span>
                                                { props.original.hasOwnProperty('OrganisationPh') && props.original.OrganisationPh != null && props.original.OrganisationPh.length>0 ? props.original.OrganisationPh.map((value, idx) => (
                                                    <span key={idx+2}><span className="color">{value.primary_phone == '1' ? '(Primary) ' : '(Secondary) '}</span><span key={idx+2}>{value.phone} <i className="icon icon-pending-icons color"></i></span><br/></span>
                                                )) : <React.Fragment />}

                                            </div>
                                            <div className="my-0">
                                                <span className="color">Email: </span> 
                                                { props.original.hasOwnProperty('OrganisationEmail') && props.original.OrganisationEmail != null && props.original.OrganisationEmail.length>0 ? props.original.OrganisationEmail.map((value, idx) => (
                                                    <span key={idx+1}><span className="color">{value.primary_email == '1' ? '(Primary) ' : '(Secondary) '}</span> <span key={idx+5}>{value.email}</span><br/></span>
                                                )): <React.Fragment />}
                                            </div>
                                                
                                            </div>
                                            <div className="col-sm-2 text-right mt-5">
                                                {(this.state.view_by_status ==0)?
                                                    <span>
                                                        <a onClick={()=>this.setState({modal_show:true,selectedData:props.original,formUrl:'organisation/OrgDashboard/add_house_contact',pageTitle:'Update contact',edit_mode:1})}><i className="icon icon-update update_button mr-2"></i></a>
                                                        <a onClick={()=> this.archived(props.original.id)}><i className="icon icon-email-pending archive_Icon"></i></a>
                                                </span>
                                                :''}
                                            </div>
                                            </div>}
                                            showPagination={this.state.sites_list.length > PAGINATION_SHOW ? true : false }
                                        />
                                       {this.state.modal_show ? 
                                        <OrganisationAddContactPopUp 
                                            orgId={this.props.props.match.params.orgId} 
                                            houseId={this.props.props.match.params.houseId} 
                                            fromUrl={this.state.formUrl} 
                                            selectedData={this.state.selectedData} 
                                            orgName={this.props.orgName} 
                                            modal_show={this.state.modal_show} 
                                            closeEditPopUp={this.closeEditPopUp} 
                                            edit_mode={this.state.edit_mode}
                                            pageTitle={this.state.pageTitle}
                                       /> : <React.Fragment /> }
                                       {this.state.view_by_status ==1 ? <React.Fragment /> :
                                       <a className="pull-right P_30_T">
                                            <button className="button_plus__" onClick={()=>this.setState({modal_show:true,edit_mode:'',formUrl:'organisation/OrgDashboard/add_house_contact',pageTitle:'Adding New Contact to '+ this.props.OrganisationProfile.name,mode:'add',selectedData:{firstname:'',lastname:'',position:'',department:'',phone:'',position:'',email:'',contact_type:this.state.active_panel_type,OrganisationPh:[{'phone':''}],OrganisationEmail:[{'email':''}]}})}><i className="icon icon-add-icons Add-2-1"></i></button>
                                        </a> 
                                       }

                                    </div>
                                </div>
                                                     
                            </div>
                        </div>
                        
                        <div role="tabpanel" className="tab-pane" id="site_location">
                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="P_7_TB"><h3>Billing '{this.props.OrganisationProfile.name}':</h3></div>
                                </div>
                                <div className="col-sm-12"><div className="bor_T"></div></div>
                            </div>
                            <p>Coming Soon.</p>
                           {/*<div className="px-3">
                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Name: </div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.name}</div>
                            </div>

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Address: </div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.primary_address || 'N/A'}</div>
                            </div>

                            { (this.state.phone_no_ary)? <span>                             
                                {this.state.phone_no_ary.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-sm-3 f pt-3 ">Phone ({(value.primary_phone) && value.primary_phone == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-sm-9 f pt-3 pb-3"><u>{value.phone.indexOf('+') == -1 ? '+':''}{value.phone}</u></div>
                                    </div>
                                ))}  </span> : ''
                            }

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Office Phone: </div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.primary_phone || 'N/A'}</div>
                            </div>

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Office Email: </div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.primary_email || 'N/A'}</div>
                            </div>

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">ABN:</div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.abn}</div>
                            </div>

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Payroll Tax: </div>
                                <div className="col-sm-9 f dotted_line py-3">{this.state.payroll_tab || 'N/A'}</div>
                            </div>
                        
                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">GST: </div>
                                <div className="col-sm-9 f dotted_line py-3">{(this.state.gst && this.state.gst == 1)?'Yes':'No'}</div>
                            </div>

                            <div className="row f_color_size">
                                <div className="col-sm-3 f py-3">Booking Status: </div>
                                <div className="col-sm-9 f py-3">Do not allow future bookings from -00/00/0000</div>
                            </div>
                           
                        </div>
                            <div className="row P_15_T">
                                <div className="col-lg-3 col-sm-4 pull-right">
                                    <a className="but">
                                        Update Org Details
                                    </a>
                                </div>
                            </div>*/}
                            </ReactPlaceholder> 
                        </div>
                        
                    </div>
                </div>
                </div>            
            </div>
        );
    }
}


const mapStateToProps = state => ({
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
        getHouseProfile:(value) => dispach(getHouseProfile(value)),
         setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(SubOrganisationContactBilling)