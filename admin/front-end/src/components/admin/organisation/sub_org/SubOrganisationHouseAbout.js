import React from 'react';
import { ROUTER_PATH } from '../../../../config.js';
import { postData,checkItsNotLoggedIn } from '../../../../service/common.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile,customHeading } from '../../../../service/CustomContentLoader.js';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import OrganisationAddSitePopUp from '../OrganisationAddSitePopUp';  // dashboard pop to add site
import { setOrgProfileData, getHouseProfile } from '../actions/OrganisationAction.js';
import { setActiveSelectPage } from './../actions/OrganisationAction.js'

class Dotted_line extends React.Component {
    render(){
        return(
            <div className="row f_color_size py-3">
                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3"></div>
                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9 dotted_line col-xs-9"></div>
            </div>
            );
    }
}


class SubOrganisationHouse extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            loading:false,
            sub_org_list:[],
        };
    }

    closeModal=()=>{
        this.setState({modal_show:false},function(){
            this.getHouseDetails();
        });
    }
    
    componentDidMount() { 
         this.props.setActiveSelectPage('suborg_house_overview');
    }

    getHouseDetails(){
        //var postUrl = 'organisation/OrgDashboard/get_house_profile';
        this.setState({loading: true});
        this.props.getHouseProfile(this.props.props.match.params);
        
        setTimeout(()=>{
            this.setState({loading: false});
        },10); 
       
    }
    
    render() {  

     return (
            <div>           

                <div className="row">
               <div className="col-lg-12">
                <div className="row">
                <div className="col-sm-12"><div className="bor_T"></div></div>   
                </div>
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="orgOverView">
                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="P_7_TB"><h3>About '{this.props.OrganisationProfile.name}':</h3></div>
                                </div>
                                <div className="col-md-12"><div className="bor_T"></div></div>
                            </div>
                            <div className="px-3 mt-4">
                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Name: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.name}</div>
                            </div>
                            <Dotted_line/>

                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Address: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.primary_address || 'N/A'}</div>
                            </div>
                            <Dotted_line/>

                            { (this.props.OrganisationProfile.phone_no_ary)? <span>                             
                                {this.props.OrganisationProfile.phone_no_ary.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Phone ({(value.primary_phone) && value.primary_phone == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9"><u>{value.phone.indexOf('+') == -1 ? '+':''}{value.phone}</u></div>
                                    </div>
                                ))}  <Dotted_line/></span> : ''
                            }


                            { (this.props.OrganisationProfile.email_ary)? <span>                             
                                {this.props.OrganisationProfile.email_ary.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-md-3 f align_e_2 col-xs-3">Office Email ({(value.primary_email) && value.primary_email == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-md-9 f align_e_1 col-xs-9"><u>+{value.email}</u></div>
                                    </div>
                                ))}  <Dotted_line/></span> : ''
                            }
                           
                        </div>
                            <div className="row P_15_T">
                                <div className="col-lg-3 col-sm-4 pull-right">
                                    <a className="but" onClick={()=>this.setState({
                                        modal_show:true,
                                        formUrl:'organisation/OrgDashboard/add_org_site',
                                        pageTitle:'',mode:'add',
                                        selectedData:{
                                            title:this.props.OrganisationProfile.name,
                                            site_address:this.props.OrganisationProfile.street,
                                            state:this.props.OrganisationProfile.state,
                                            city:this.props.OrganisationProfile.city,
                                            postal:this.props.OrganisationProfile.postal,
                                            abn:this.props.OrganisationProfile.abn,
                                            SiteEmail:this.props.OrganisationProfile.email_ary!=null && this.props.OrganisationProfile.email_ary.length > 0 ? this.props.OrganisationProfile.email_ary : [{ 'email': '' }],                                            
                                            SitePhone:this.props.OrganisationProfile.phone_no_ary != null && this.props.OrganisationProfile.phone_no_ary.length > 0 ? this.props.OrganisationProfile.phone_no_ary : [{ 'phone': '' }],
                                            siteDetails: this.props.OrganisationProfile.siteDetails,
                                            site_type:this.props.OrganisationProfile.site_type
                                        }})}>Update House Details</a>
                                </div>
                            </div>
                            {(this.state.modal_show)?
                                        <OrganisationAddSitePopUp 
                                        modal_show={this.state.modal_show} 
                                        closeModal={this.closeModal} 
                                        site_id={this.props.props.match.params.houseId}
                                        mode='edit'
                                        orgId={this.props.props.match.params.orgId}
                                        selectedData={this.state.selectedData} 
                                        />  
                                        :''}
                            </ReactPlaceholder>
                        </div>
                     
                    </div>
                </div>
                </div>            
            </div>
        );
    }
}


const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 
const mapDispatchtoProps = (dispach) => {
    return {
        getOrgProfileData: (value) => dispach(setOrgProfileData(value)),
        getHouseProfile: (value) => dispach(getHouseProfile(value)),
        setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
   }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(SubOrganisationHouse)