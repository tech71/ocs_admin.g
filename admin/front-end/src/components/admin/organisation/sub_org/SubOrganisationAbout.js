import React from 'react';
import { ROUTER_PATH } from '../../../../config.js';
import { checkItsNotLoggedIn } from '../../../../service/common.js';
import moment from 'moment';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { customProfile,customHeading } from '../../../../service/CustomContentLoader.js';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux'
import OrganisationBookingDatePopup from '../OrganisationBookingDatePopup';
import OrganisationUpdatePopUp from '../OrganisationUpdatePopUp';
import { setActiveSelectPage } from './../actions/OrganisationAction.js'

class Dotted_line extends React.Component {
    render(){
        return(
            <div className="row f_color_size py-3">
                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3"></div>
                <div className="col-lg-9 col-md-9 f align_e_1 dotted_line col-xs-9"></div>
            </div>
            );
    }
}


class SubOrganisationAbout extends React.Component {
    constructor(props) {
    super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        var all=[]  ;
        this.state = {
            sub_org_list:[],
            org_update_modal:false,
        };
    }
    
    closeBookingPopUp=(param)=> {
        this.setState({modal_show:false});
    }

    componentDidMount(){
         this.props.setActiveSelectPage('suborg_overview');
    }
    
    closeOrgUpdatePopUp=(param)=>{
        this.setState({org_update_modal:false});
    }

    render() {  
        const columns = [
            {Header: '', accessor: 'id', filterable: false,Cell: (props) => <span><a href="f">{props.original.sub_org_detail.name}</a></span>, Header: <span></span>} ,
            {Cell: (props) => <span></span>, Header: <span></span>, style: {
                    "text-align": "right", 
                  }, headerStyle: {border:"0px solid #fff" }, headerClassName: 'margin_right_side', sortable: false}, 
                   {expander : true, sortable: false,
                       Expander: ({ isExpanded, ...rest }) => 
                       <div>{isExpanded? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
                     headerStyle: {border:"0px solid #fff" },
              }
        ]

     return (
            <div>           

                <div className="row">
                <div className="col-lg-12">
                 <div className="row">
                 <div className="col-sm-12"><div className="bor_T"></div></div>
                </div>
                    
        
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="orgOverView">
                        <ReactPlaceholder showLoadingAnimation type='textRow' customPlaceholder={customProfile} ready={!this.state.loading}>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="P_7_TB"><h3>About '{this.props.OrganisationProfile.name}':</h3></div>
                                </div>
                                <div className="col-sm-12"><div className="bor_T"></div></div>
                            </div>
                            <div className="px-3 mt-4">
                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Name: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.name}</div>
                            </div>
                            <Dotted_line/>

                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Address: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.primary_address || 'N/A'}</div>
                            </div>
                            <Dotted_line/>

                            { (this.props.OrganisationProfile.OrganisationPh)? <span>                             
                                {this.props.OrganisationProfile.OrganisationPh.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-md-3 f align_e_2 col-xs-3">Office Phone ({(value.primary_phone) && value.primary_phone == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-md-9 f align_e_1 col-xs-9"><u>{value.phone.indexOf('+') == -1 ? '+':''}{value.phone}</u></div>
                                    </div>
                                ))}  </span> : ''
                            }

                            {(this.props.OrganisationProfile.OrganisationPh)?<Dotted_line/>:''}

                            { (this.props.OrganisationProfile.OrganisationEmail)? <span>                             
                                {this.props.OrganisationProfile.OrganisationEmail.map((value, idx) => (
                                    <div className="row f_color_size" key={idx}>
                                        <div className="col-md-3 f align_e_2 col-xs-3">Office Email ({(value.primary_email) && value.primary_email == 1?'Primary':'Secondary'}):</div>
                                        <div className="col-md-9 f align_e_1 col-xs-9">{value.email}</div>
                                    </div>
                                ))}  </span> : ''
                            }

                           {(this.props.OrganisationProfile.OrganisationEmail)?<Dotted_line/>:''}

                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">ABN:</div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{this.props.OrganisationProfile.abn}</div>
                            </div>
                            <Dotted_line/>

                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Payroll Tax: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{ (this.props.OrganisationProfile.payroll_tax && this.props.OrganisationProfile.payroll_tax == '0')?'No':(this.props.OrganisationProfile.payroll_tax && this.props.OrganisationProfile.payroll_tax == '1')?'Yes':'N/A'}</div>
                            </div>
                            <Dotted_line/>
                        
                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">GST: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{(this.props.OrganisationProfile.gst && this.props.OrganisationProfile.gst == 1)?'Yes':'No'}</div>
                            </div>
                            
                            <Dotted_line/>

                            <div className="row f_color_size">
                                <div className="col-lg-3 col-md-3 f align_e_2 col-xs-3">Booking Status: </div>
                                <div className="col-lg-9 col-md-9 f align_e_1 col-xs-9">{(this.props.OrganisationProfile.booking_status)?'Do not allow future bookings from -':'Open'}  {(this.props.OrganisationProfile.booking_date && this.props.OrganisationProfile.booking_date!='00-00-0000')?moment(this.props.OrganisationProfile.booking_date).format('DD/MM/YYYY'):''}
                                </div>

                                <div className="row text-left mt-2">
                                    <a><i className="icon icon-update update_button" onClick={() => this.setState({modal_show:true})}></i></a>    
                                </div>
                            </div>
                           
                           {/*<OrganisationBookingDatePopup 
                                orgId={this.props.props.match.params.subOrgId} 
                                selectedData={{'booking_status':this.props.OrganisationProfile.booking_status,'booking_date':this.props.OrganisationProfile.booking_date}} 
                                modal_show={this.state.modal_show} 
                                closeBookingPopUp={this.closeBookingPopUp} 
                            />*/}
                           
                        </div>
                            <div className="row P_15_T">
                                <div className="col-lg-3  col-md-4 col-sm-3 pull-right">
                                   <a className="but" onClick={() => this.setState({org_update_modal:true})}>Update Sub-Org Details</a>
                                </div>
                            </div>

                            <OrganisationUpdatePopUp 
                             orgId={this.props.props.match.params.id}
                             org_update_modal={this.state.org_update_modal}
                             pageTitle= {'Update '+ "'"+ this.props.OrganisationProfile.name+"'"}
                             closeOrgUpdatePopUp={this.closeOrgUpdatePopUp}
                            />

                            </ReactPlaceholder>
                        </div>
                     
                    </div>
                </div>
                </div>            
            </div>
        );
    }
}

const mapStateToProps = state => ({
    //ActiveClass : state.OrganisationReducer.ActiveClassProfilePage,
    OrganisationProfile : state.OrganisationReducer.orgProfile
})

 const mapDispatchtoProps = (dispach) => {
       return {
            setActiveSelectPage: (value) => dispach(setActiveSelectPage(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(SubOrganisationAbout)