import React from 'react';
import { ROUTER_PATH, BASE_URL } from '../../../config.js';
import Modal from 'react-bootstrap/lib/Modal';
import Select from 'react-select-plus';
import { toast } from 'react-toastify';
import { handleChangeSelectDatepicker, postData, checkItsNotLoggedIn, queryOptionData } from '../../../service/common.js';
import { confirmAlert, createElementReconfirm } from 'react-confirm-alert';

class AttachSites extends React.Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.state = {
            org_details:props.orgDetails,
            site_org:[],
            isSiteModalShow: false,
            loading:false
        };
    }
    componentWillReceiveProps(newProps){
        if(newProps.hasOwnProperty('isSiteModalShow') && this.props.isSiteModalShow != newProps.isSiteModalShow){
            this.setState({isSiteModalShow:newProps.isSiteModalShow});
        }

        if(newProps.hasOwnProperty('orgDetails') && this.props.orgDetails != newProps.orgDetails){
            this.setState({org_details:newProps.orgDetails});
        }
        
    }

    getSiteOrg = (input) =>{
        return queryOptionData(input,'organisation/OrgDashboard/get_site_org',{query: input},3);
    } 

    addSiteData = () => {
        if (this.state.site_org.length > 0) {
            let siteSelected = this.state.site_org;
            confirmAlert({
                customUI: ({ onClose }) => {
                    return (
                            <div className='custom-ui'>
                                <div className="confi_header_div">
                                    <h3>Confirmation</h3>
                                    <span className="icon icon-cross-icons" onClick={() => {
                                    onClose();
                                    }}></span>
                                </div>
                                <p><span>Are you sure you want to Add this site to this Organisation?</span></p>
                                <div className="confi_but_div">
                                    <button className="Confirm_btn_Conf" onClick={
                                        () => {
                                            this.setState({loading:true});
                                            var Request = JSON.stringify(this.state);
                                             postData('organisation/OrgDashboard/add_site_org', Request).then((response) => {
                                                if(response.status){
                                                    toast.success(response.msg, {
                                                        position: toast.POSITION.TOP_CENTER,
                                                        hideProgressBar: true
                                                    });
                                                    this.setState({loading:false});
                                                    this.props.handleHide(true);
                                                    onClose();
                                                }else{
                                                    toast.error(response.error, {
                                                        position: toast.POSITION.TOP_CENTER,
                                                        hideProgressBar: true
                                                    });
                                                    this.setState({loading:false});
                                                    onClose();
                                                }
                                            }); 
                                                
                                            }} disabled={this.state.loading}>Confirm</button>
                                    <button className="Cancel_btn_Conf" onClick={
                                                () => {
                                                    onClose();
                                                }}> Cancel</button>
                                </div>
                            </div>
                                            )
                        }
                    })
            return true;
        } else {
            toast.error("Please select at least one site to add.", {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
    }
    render(){
        return (
            <div>
                <Modal className="modal fade-scale Modal_A  Modal_B Green" show={this.state.isSiteModalShow} onHide={this.props.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_2" tabIndex="-1" role="dialog" >
                    <Modal.Body>
                        <div className="dis_cell">
                            <div className="text text-left by-1 Popup_h_er_1"><span>Add Site:</span>
                    <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.setState({ site_org:[] },()=>{this.props.handleHide()})}><i className="icon icon-cross-icons"></i></a>
                            </div>
                            <div className="row P_15_T">
                                <div className="col-sm-5">
                                    <label>Name</label>
                                    <span className="required search_icons_right modify_select">
                                        <Select.Async
                                            cache={false}
                                            name="form-field-name"
                                            loadOptions={(e) => this.getSiteOrg(e)}
                                            clearable={false}
                                            closeOnSelect={false}
                                            placeholder='Search'
                                            multi={true}
                                            value={this.state.site_org}
                                            onChange={(e) => handleChangeSelectDatepicker(this, e, 'site_org')}
                                            className="default_validation search_icons_right modify_select"
                                        />
                                    </span>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-7"></div>
                                <div className="col-sm-5">
                                    <input type="submit" className="but" value={'Add'} name="content" onClick={(e) => this.addSiteData()} />
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}
AttachSites.defaultProps = {
isSiteModalShow : false,
handleHide: (e)=>{},
orgDetails:''
};
export default AttachSites;