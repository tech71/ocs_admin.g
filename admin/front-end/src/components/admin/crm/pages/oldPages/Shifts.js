import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {ProgressBar} from 'react-bootstrap';
import {ROUTER_PATH} from '../../../../../config.js';
import {PartShiftModal} from '../../ParticipantShiftModal';
import CrmPage from '../../CrmPage';
import {ShiftRequirement} from '../../../../../dropdown/CrmDropdown.js';
import { connect } from 'react-redux'
import { postData }  from '../../../../../service/common.js';
import { ToastContainer, toast } from 'react-toastify';
import { ToastUndo } from 'service/ToastUndo.js'
import jQuery from "jquery";

class Shifts extends Component {
  constructor(props, context) {
      super(props, context);
      this.participantDetailsRef = React.createRef();
      this.state = { participant_id:'',details:[],
      participantShift:{'shift_requirement':[]},
      PartShiftModal: false};

  }
  componentDidMount(){
    this.setState({participant_id:this.props.props.match.params.id});
    this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.props.props.match.params.id);
  }

    closePartShiftModal=()=>{
        this.setState({PartShiftModal:false});
    }

    showPartShiftModal=()=>{
        this.setState({PartShiftModal:true});
    }

    handleCheckboxValue=(e,array_name)=> {
      let details =this.state[array_name];
      let data = (details[e.target.name]!=null)?details[e.target.name]:[];
      if(data.includes(e.target.value)){
        var index = data.indexOf(e.target.value);
        if(index!=-1){
          data.splice(index, 1);
       }
      }else{
        data.push(e.target.value);
      }
       details[e.target.name + '_error'] = false;
      details[e.target.name] = data;
      this.setState({[array_name]:details});
     }
    componentWillReceiveProps(nextProps){
        if(  (nextProps.participaintDetails.id>0 || this.props.participaintDetails.id!=nextProps.participaintDetails.id) ){
            this.setState({details:nextProps.participaintDetails});
        }
    }

    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    submitParticipantShift = (e) => {
        e.preventDefault();
         var str = JSON.stringify(this.state);
         var custom_validate = this.custom_validation_shift({ errorClass: 'tooltip-default' });
         var validator = jQuery("#partcipant_shift").validate();
         if (!this.state.loading && jQuery("#partcipant_shift").valid() ) {
            var str = JSON.stringify(this.state);
            const formData = new FormData();

           this.props.crmParticipantSubmit(formData).then(json => {
             if(json.status){
                 this.setState({ success: true });
                  toast.success(<ToastUndo message={"Shift edited successfully"} showType={'s'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                  })
                }
              else{
                toast.error(<ToastUndo message={json.error}  />, {
                  position: toast.POSITION.TOP_CENTER,
                  hideProgressBar: true
                });
              }
            })
          } else {
              validator.focusInvalid();
          }
    }

    custom_validation_shift = () => {
        var return_var = true;
        var state = this.state['participantShift'];
        var List = [{ key: 'shift_requirement' }];
        List.map((object, sidx) => {
       if (state[object.key] == null || state[object.key] == '') {
                state[object.key + '_error'] = true;
                this.setState(state);
                return_var = false;
            }
        });
        return return_var;
      }

    getParticipantDetails = () => {
        this.setState({loading: true}, () => {
            postData('crm/CrmParticipant/get_prospective_participant_details', {id:this.state.participant_id}).then((result) => {

                if (result.status) {

                  //console.log(result.data);
                  this.setState({
                      details:result.data,

                    });
                }
                this.setState({loading: false});
            });
        });
    }
    render() {
      let shift_data = typeof(this.state.details.shift_data)!='undefined'?(this.state.details.shift_data):[];
      let shift_requirement = typeof(this.state.details.shift_requirement)!='undefined'?(this.state.details.shift_requirement):'';
      const now = 60;
        return (
            <div className="container-fluid">
            <CrmPage ref={this.participantDetailsRef} pageTypeParms={'participant_shift'} />
                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12">
                        <div className=" py-4 bb-1">
                        <span className="back_arrow">
                        <Link to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}><span className="icon icon-back1-ie"></span></Link>
                        </span>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1  col-md-12">
                        <div className="row d-flex py-4">
                            <div className="col-md-7 align-self-center br-1">
                                <div className="h-h1 ">
                                   {this.props.showPageTitle}
                            </div>
                            </div>
                            <div className="col-md-5">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-4 align-self-center text-right">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-8 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                            <div className="Lates_up_txt"><b>Stage 2:</b> Attachment added- Service Agreement Doc</div>
                                            {/* <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div> */}
                                            <div className="Lates_up_btn"><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Date: 01/01/01 - 11:32AM</div>
                                            {/* <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div> */}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-10 col-lg-offset-1 col-md-12 mt-5">
                        <div className="progress-b1">
                        <ProgressBar className="progress-b2" now={now} label={'Intake Progress: ' + `${now}%`+ 'Complete'} />
                        </div>
                    </div>
                </div>

                <div className="row">


                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Shifts</div>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 my-2"><strong>Shift Start Date:  </strong><span>{this.state.details.start_date}</span></div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 mt-4"><strong>Required Shifts:</strong></div>
                                <div className="shift_week_1">
                                {(shift_data).map((value, idx) => (
                                    <div className="shift_days_1">{/*console.log(value)*/}
                                        <div className="Shift_day_n">{value.day}</div>
                                          {(value.type).map((value2, idx) => (
                                            <div className={"Shift_day_blank "+value2}></div>
                                          ))}
                                    </div>
                                  ))}
                                </div>
                            </div>
                            <div className="col-md-12 d-flex Shift_times_div my-4">
                                <div><span className="am_shift"></span>AM (6am - 12pm)</div>
                                <div><span className="pm_shift"></span>PM (12pm - 10pm)</div>
                                <div><span className="so_shift"></span>S/O (10pm - 6am, non active)</div>
                                <div><span className="na_shift"></span>A/N (10pm - 6am)</div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 text-right">
                                <span className="add_btn1"><i className="icon icon-ios-plus-empty"></i></span>
                                <span className="add_btn1"><i className="icon icon-ios-minus-empty"></i></span>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-md-12">
                                <div className="shift_h1 py-2 by-1">Shift Requirements</div>
                            </div>
                            <div className="col-md-12">
                                <div className="Partt_d1_txt_3 py-4 bb-1">
                                    {(shift_requirement)?ShiftRequirement('',shift_requirement):'N/A'}</div>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-end">
                            <div className="col-md-3 mt-3"> <span onClick={()=>{this.showPartShiftModal()}} className="btn-3" >Edit Participants Shifts</span></div>
                        </div>

                    </div>


                </div>
                <PartShiftModal showModal={this.state.PartShiftModal} handleClose={()=>this.closePartShiftModal()} checkboxHandler={this.handleCheckboxValue} sts={this.state.participantShift} handleChanges={this.handleChange}/>
            </div>
        );
    }
}
const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
          participaintDetails :state.DepartmentReducer.participaint_details
      }
  };
export default connect(mapStateToProps)(Shifts);
