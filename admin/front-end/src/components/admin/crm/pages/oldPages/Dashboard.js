import React, { Component } from 'react';
import jQuery from "jquery";

import { ROUTER_PATH, BASE_URL } from '../../../../../config.js';
import { checkItsNotLoggedIn } from '../../../../../service/common.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';


class Dashboard extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);

    }

    render() {
        return (
            <div>


                <section className="manage_top">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_15_TB"><Link to={ROUTER_PATH+'admin/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link></div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                <div className="bor_T"></div>
                            </div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12 P_25_TB text-center">
                                <h1 className="my-0 color">Admin Portal</h1>
                            </div>
                            <div className="col-lg-10 col-lg-offset-1 col-md-12">
                                <div className="bor_T"></div>
                            </div>
                            <div className="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1 P_30_TB"><div className="text_app">Your App</div></div>
                        </div>


                        <div className="row">
                            <div className="col-lg-2 col-md-3 col-lg-offset-2 text-center"><Link className="d-inline-block" to={ROUTER_PATH + 'admin/crm/participantadmin'} ><span className="circle_but c-colr">P</span><p>Participants</p></Link></div>
                            <div className="col-lg-2 col-md-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH + 'admin/crm/reporting'} ><span className="circle_but c-colr">R</span><p>Reporting</p></Link></div>
                            <div className="col-lg-2 col-md-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH + 'admin/crm/schedules'}><span className="circle_but c-colr">S</span><p>Schedules</p></Link></div>
                            <div className="col-lg-2 col-md-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH + 'admin/crm/usermangement'}><span className="circle_but c-colr">U</span><p>User Management</p></Link></div>
                        </div>
                    </div>
                </section>


            </div>
        );
    }
}
export default Dashboard;
