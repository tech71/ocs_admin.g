import React, { Component } from 'react';
import { ROUTER_PATH, BASE_URL } from '../../../../../config.js';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactResponsiveSelect from 'react-responsive-select';
import { checkItsNotLoggedIn, getJwtToken, postData, archiveALL, reFreashReactTable } from '../../../../../service/common.js';
import { connect } from 'react-redux'
import AddDepartment from './AddDepartment';
import { setCrmDepartmentData } from '../../actions/DepartmentAction.js';
import {DepartmentAnalytics} from './DepartmentAnalytics';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CrmPage from '../../CrmPage';
import Pagination from "../../../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'



const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('crm/CrmDepartment/get_department', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

const getDepartmentList = () => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({});
        postData('crm/CrmDepartment/get_all_department', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            setTimeout(() => resolve(res), 10);
        });
    });
};

const multiSelectOptionMarkup = (text) => (
    <div>
        <span className="rrs_select"> {text}</span>
        <span className="checkbox">
            <i className="icon icon-star2-ie"></i>
        </span>

    </div>
);

// By default no caret icon is supplied - any valid jsx markup will do
const caretIcon = (
    <i className="icon icon-edit1-ie"></i>
);

class Departments extends Component {
    constructor(props, context) {
        super(props, context);
        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            key: 1, filterVal: '',
            showModal: false,
            departmentList:[],
            department_option:[],
            incomplete:'',
            inactive:'',
            DepartmentAnalyticsModal:false,
            export_for:'',
            depExportList:[],
        };
        this.reactTable = React.createRef();
    }

    componentWillMount(){
       getDepartmentList().then(res => {
           this.setState({
               department_option: res.rows,
               pages: res.pages,
               loading: false
           });
       });
    }



    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
          //console.log(res.rows);
            this.setState({
                departmentList: res.rows,
                pages: res.pages,
                loading: false
            });

        });
    }

    handleSelect(key) {
        this.setState({ key });
    }

    showModal = () => {
        this.setState({ showModal: true })
    }

      closeModal = (param) => {
          this.setState({ showModal: false,oldData:[] },()=>{

              if(param){
                reFreashReactTable(this, 'fetchData');
              }

          });
      }

    submitSearch = (e) => {
        e.preventDefault();
        var srch_ary = {search: this.state.search, inactive: this.state.inactive , incomplete: this.state.incomplete , filterVal: this.state.filterVal }
        this.setState({filtered: srch_ary});
    }

    searchData = (key, value) => {
        var srch_ary = {search: this.state.search,inactive: this.state.inactive, filterVal: this.state.filterVal };
        srch_ary[key] = value;
        this.setState(srch_ary);
        this.setState({filtered: srch_ary});
    }



    closeDepartmentAnalyticsModal(){
        this.setState({DepartmentAnalyticsModal:false})
    }
    showDepartmentAnalyticsModal(){
        this.setState({DepartmentAnalyticsModal:true})
    }

    csvDownload =(data)=>{
        var requestData = {depExportList:data,export_for:'department_record'};
        postData('crm/CrmDepartment/department_member_export', requestData).then((result) => {
        if (result.status) {
            this.setState({depExportList:[]});
            window.location.href=BASE_URL+"archieve/"+result.csv_url;
            //this.setState({qual:removeDownload});
        } else {
            toast.dismiss();
            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
            // toast.error(result.error, {
                position: toast.POSITION.TOP_CENTER,
                hideProgressBar: true
            });
        }
        });
    }





    render() {

      //console.log(this.state.department_option);

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];


        const options2 = [
            { text: 'Select Allocations:', optHeader: true },
            {
                value: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                text: 'Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo',
                markup: multiSelectOptionMarkup('Alfa Romeo Alfa RomeoAlfa RomeoAlfa RomeoAlfa RomeoAlfa Romeo'),
            },
            {
                value: 'fiat',
                text: 'Fiat',
                markup: multiSelectOptionMarkup('Fiat'),
            },
            {
                value: 'ferrari',
                text: 'Ferrari',
                markup: multiSelectOptionMarkup('Ferrari'),
            },
            {
                value: 'mercedes',
                text: 'Mercedes',
                markup: multiSelectOptionMarkup('Mercedes'),
            },
            {
                value: 'tesla',
                text: 'Tesla',
                markup: multiSelectOptionMarkup('Tesla'),
            },
            {
                value: 'volvo',
                text: 'Volvo',
                markup: multiSelectOptionMarkup('Volvo'),
            },
            {
                value: 'zonda',
                text: 'Zonda',
                markup: multiSelectOptionMarkup('Zonda'),
            },
        ]


        const subComponentDataMapper = row => {
          let data = row.row._original;

          //this.setState({depExportList:data});
          //console.log(row);
          return(
            <div className="col-md-12 task_table dep_table">

                <div className="row d-flex by-1">
                    <div className="col-md-12  col-lg-12 pb-3 my-3">

                        <ReactTable
                            columns={[
                                { Header: "HCMGR ID:", accessor: "hcmgr_id", },
                                { Header: "Name:", accessor: "FullName", },
                                { Header: "Department:", accessor: "department_name", },
                                { Header: "Date of Allocation:", accessor: "created", headerStyle: { border: "0px solid #fff" } },
                            ]}
                            data={row.row._original.members}
                            pageSize={row.row._original.members.length}
                            showPagination={false}
                            minRows={3}
                        />

                    </div>
                </div>

                <div className="row pb-4 mt-3">
                    <div className="col-md-12 task_table_footer pt-2">
                        <a onClick={()=>{this.showDepartmentAnalyticsModal()}}><u>Department Analytics</u></a>
                        <a href="#"><u>Archive Department</u></a>
                        <a onClick={()=>this.csvDownload(row.row._original.members)}  ><u>Export Department Record</u></a>
                    </div>
                </div>
            </div>
          );
        }
        return (
            <div className="container-fluid">
<CrmPage pageTypeParms={'user_departments'} />
                <div className="row">
                    <div className="col-lg-12 col-md-12">
                        <div className="py-4 bb-1">
                            <Link  className="back_arrow d-inline-block" to={ROUTER_PATH + 'admin/crm/participantadmin'}><span className="icon icon-back1-ie"></span></Link>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12  col-md-12">
                        <div className="row d-flex">
                            <div className="col-md-12 align-self-center py-4 bb-1">
                                <div className="h-h1 ">{this.props.showPageTitle}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row pt-4 pb-3">
                    <div className="col-lg-3 col-lg-offset-9 col-md-3  col-md-offset-9">
                        <button className="v-c-btn1 add_dept" onClick={() => this.setState({ showModal: true,pageTitle:'Create New Department' })}><span>Add Department</span><i className="icon icon-add3-ie"></i></button>
                    </div>
                </div>

                {(this.state.showModal && this.state.pageTitle=='Create New Department' || this.state.pageTitle=='Edit Department') ?
                    <AddDepartment showModal={this.state.showModal} closeModal={this.closeModal} pageTitle={this.state.pageTitle} oldData={this.state.oldData} />
                    : ''}


                <div className="row d-flex">


                    <div className="col-lg-12">
                    <form onSubmit={this.submitSearch}>
                        <div className="row d-flex">
                            <div className="col-md-8">
                                <div className="big-search l-search">
                                    <input type="text" name="search" value={this.state.search || ''} onChange={(e) => this.setState({'search': e.target.value})}  />
                                    <button  type="submit"><span className="icon icon-search1-ie"></span></button>
                                </div>
                            </div>
                            <div className="col-md-1 d-inline-flex align-self-center justify-content-end">Filter by:</div>
                            <div className="col-md-3 d-inline-flex align-self-center">
                                <div className="s-def1 w-100">
                                    <Select
                                        name="view_by_status"
                                        options={this.state.department_option}
                                        required={true}
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder="Filter by: Unread"
                                        onChange={(e) => this.searchData('filterVal', e)}
                                      //  onChange={(e) => this.setState({ filterVal: e })}
                                        value={this.state.filterVal}
                                        className={'custom_select'}
                                    />
                                </div>
                            </div>
                        </div>
                        </form>

                        <div className="row">
                            <div className="col-md-12 re-table re-table-collapse mt-4">
                                <ReactTable

                                    columns={[
                                        { Header: "Name:", accessor: "name", },
                                        { Header: "HCMGR ID:", accessor: "hcmgr_id", },
                                        { Header: "Start Date:", accessor: "created", },

                                        {Cell: (props) => <span className="booking_L_T1">
                                               <i onClick={() => this.setState({ showModal: true,pageTitle:'Edit Department',oldData:props.original })} className="icon icon-update update_button"></i>
                                            </span>, Header: <div className="">Action: </div> },
                                       {
                                            expander: true,
                                            Header: () => <strong></strong>,
                                            width: 55,
                                            headerStyle: { border: "0px solid #fff" },
                                            Expander: ({ isExpanded, ...rest }) =>
                                                <div className="rec-table-icon">
                                                    {isExpanded
                                                        ? <i className="icon icon-arrow-up"></i>
                                                        : <i className="icon icon-arrow-down"></i>}
                                                </div>,
                                            style: {
                                                cursor: "pointer",
                                                fontSize: 25,
                                                padding: "0",
                                                textAlign: "center",
                                                userSelect: "none"
                                            },

                                        }
                                    ]}
                                    PaginationComponent={Pagination}
                                    data={this.state.departmentList}
                                    onFetchData={this.fetchData}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    filtered={this.state.filtered}
                                    defaultPageSize={10}
                                    showPagination={true}
                                    className="-striped -highlight"
                                    previousText={<span className="icon icon-arrow-1-left privious"></span>}
                                    nextText={<span className="icon icon-arrow-1-right next"></span>}
                                    SubComponent={subComponentDataMapper}
                                    showPagination={this.state.departmentList.length > 0 ? true : false }
                                    defaultFiltered=""
                                    noDataText="No Record Found"
                                    minRows={3}
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
nextText={<span className="icon icon-arrow-right next"></span>}
                                    ref={this.reactTable}
                                />
                            </div>
                            <DepartmentAnalytics showModal={this.state.DepartmentAnalyticsModal} handleClose={()=>this.closeDepartmentAnalyticsModal()}/>
                        </div>


                        {// <div className={this.state.showModal ? 'customModal show' : 'customModal'}>
                        //     <div className="custom-modal-dialog Information_modal task_modal">
                        //
                        //         <div className="custom-modal-header by-1">
                        //             <div className="Modal_title">Assign Staff Member</div>
                        //             <i className="icon icon-close1-ie Modal_close_i" onClick={this.closeModal}></i>
                        //         </div>
                        //
                        //         <div className="custom-modal-body w-100 mx-auto">
                        //
                        //
                        //             <div className="row d-flex pt-5 pb-2">
                        //                 <div className="col-md-7">
                        //                     <div className="big-search r-search">
                        //                         <input />
                        //                         <button><span className="icon icon-search1-ie"></span></button>
                        //                     </div>
                        //                 </div>
                        //                 <div className="col-md-2 d-inline-flex align-self-center justify-content-end">Filter by:</div>
                        //                 <div className="col-md-3 d-inline-flex align-self-center">
                        //                     <div className="s-def1 w-100">
                        //                         <Select
                        //                             name="view_by_status"
                        //                             options={options}
                        //                             required={true}
                        //                             simpleValue={true}
                        //                             searchable={false}
                        //                             clearable={false}
                        //                             placeholder="Filter by: Unread"
                        //                             onChange={(e) => this.setState({ filterVal: e })}
                        //                             value={this.state.filterVal}
                        //                         />
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //
                        //             <div className="row">
                        //                 <div className="col-md-12 my-3">
                        //                     <h4 className="my-0"><b>Staff Members</b> </h4>
                        //                 </div>
                        //             </div>
                        //
                        //             <div className="row">
                        //                 <div className="col-md-12 task_table dep_table">
                        //                     <div className="row d-flex">
                        //                         <div className="col-md-12  col-lg-12 pb-3 my-3">
                        //                             <ReactTable
                        //                                 columns={[
                        //                                     { Header: "HCMGR ID:", accessor: "hcmgr_id", },
                        //                                     { Header: "Name:", accessor: "FullName", },
                        //                                     { Header: "Email:", accessor: "email", },
                        //                                     { Header: "Phone:", accessor: "phone", },
                        //                                     { Header: "Position:", accessor: "position", headerStyle: { border: "0px solid #fff" } },
                        //                                 ]}
                        //                                 data={this.state.departmentList}
                        //                                 pageSize={this.state.departmentList.length}
                        //                                 showPagination={false}
                        //                             />
                        //                         </div>
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //             <div className="row">
                        //                 <div class="col-md-12 pb-4"><div class="border-das_line"></div></div>
                        //             </div>
                        //
                        //             <div className="row mb-3">
                        //                 <div className="col-md-4 my-3">
                        //                     <h4 className="my-0">Allocated Departments:</h4>
                        //                     <div className="s-def1 w-100 mt-3">
                        //                         <Select
                        //                             name="view_by_status"
                        //                             options={options}
                        //                             required={true}
                        //                             simpleValue={true}
                        //                             searchable={false}
                        //                             clearable={false}
                        //                             placeholder="Filter by: Unread"
                        //                             onChange={(e) => this.setState({ filterVal: e })}
                        //                             value={this.state.filterVal}
                        //                         />
                        //                     </div>
                        //                 </div>
                        //                 <div className="col-md-4 col-md-offset-1 my-3">
                        //                     <h4 className="my-0">Permissions</h4>
                        //                     <div className="s-def1 w-100 mt-3">
                        //                         <Select
                        //                             name="view_by_status"
                        //                             options={options}
                        //                             required={true}
                        //                             simpleValue={true}
                        //                             searchable={false}
                        //                             clearable={false}
                        //                             placeholder="Filter by: Unread"
                        //                             onChange={(e) => this.setState({ filterVal: e })}
                        //                             value={this.state.filterVal}
                        //                         />
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //
                        //             <div className="row">
                        //                 <div className="col-md-4 my-3">
                        //                     <h4 className="my-0">Preferred Department: </h4>
                        //                     <div className="s-def1 w-100 mt-3">
                        //                         <Select
                        //                             name="view_by_status"
                        //                             options={options}
                        //                             required={true}
                        //                             simpleValue={true}
                        //                             searchable={false}
                        //                             clearable={false}
                        //                             placeholder="Filter by: Unread"
                        //                             onChange={(e) => this.setState({ filterVal: e })}
                        //                             value={this.state.filterVal}
                        //                         />
                        //                     </div>
                        //                 </div>
                        //             </div>
                        //
                        //         </div>
                        //
                        //         <div className="custom-modal-footer bt-1 mt-5 px-0 pb-4">
                        //             <div className="row d-flex justify-content-end">
                        //                 <div className="col-md-2"><a className="btn-1">Apply Changes</a></div>
                        //             </div>
                        //         </div>
                        //
                        //     </div>
                        // </div>
                        }



                    </div>
                </div>


            </div>


        );
    }
}


const mapStateToProps = state => ({
    showPageTitle: state.DepartmentReducer.activePage.pageTitle,
    showTypePage: state.DepartmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        crmDepartmentData: (value) => dispach(setCrmDepartmentData(value))
    }
}
export default connect(mapStateToProps,mapDispatchtoProps)(Departments)
