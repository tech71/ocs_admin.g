import React from 'react';
import Select from 'react-select-plus';
import jQuery from "jquery";
import '../../../../../../service/jquery.validate.js';
import { ROUTER_PATH, BASE_URL } from '../../../../../../config.js';
import { postData, getOptionsSuburb, handleAddShareholder } from '../../../../../../service/common.js';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { listViewSitesOption, relationDropdown, sitCategoryListDropdown, ocDepartmentDropdown, getAboriginalOrTSI, LivingSituationOption } from '../../../../../../dropdown/CrmDropdown.js';

export const RefererData = (props) => {
  return (

    <React.Fragment>
      <form id="referral_details">
        <div className="row">
          <div className="col-md-12 py-4 title_sub_modal">Referer Details</div>
        </div>
        <div className="row">
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">First name: </label>
            <div className="required"><input name="first_name" type="text" className="default-input" onChange={(e) => props.handleChanges(e, 'refererDetails')} maxLength="30" data-msg-required="Referer first name is required" data-rule-required="true" value={props.sts.first_name || ''} /></div>
          </div>
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">Last Name: </label>
            <div className="required"><input name="last_name" type="text" onChange={(e) => props.handleChanges(e, 'refererDetails')} className="default-input" data-rule-required="true" data-msg-required="Referer last name is required" value={props.sts.last_name || ''} /></div>
          </div>
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">Organisation: </label>
            <div className=""><input name="organisation" type="text" className="default-input" onChange={(e) => props.handleChanges(e, 'refererDetails')} value={props.sts.organisation || ''} /></div>
          </div>
        </div>

        <div className="row">
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">Email: </label>
            <div className="required"><input name="remail" type="text" className="default-input" data-rule-required="true" data-rule-email="true" onChange={(e) => props.handleChanges(e, 'refererDetails')} data-msg-required="Referer email is required" value={props.sts.remail || ''} /></div>
          </div>
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">Phone Number: </label>
            <div className="required"><input name="phone_number" type="text" className="default-input" data-rule-required="true" data-rule-phonenumber onChange={(e) => props.handleChanges(e, 'refererDetails')} data-msg-required="Referer phone is required" value={props.sts.phone_number || ''} /></div>
          </div>
          <div className="w-20-lg col-md-3 mb-4">
            <label className="title_input">Relationship to Participant: </label>
            <div className="required">

              <div className="s-def1">
                <Select
                  name="relation"
                  id="relation"
                  // options={relationDropdown(0)} 
                  options={props.optsState.relations_participant}
                  required={true} simpleValue={true}
                  searchable={false} clearable={false}
                  onChange={(e) => props.updateSelect(e, 'relation', 'refererDetails')}
                  value={props.sts.relation}
                  className={'custom_select default_validation'}
                  inputRenderer={
                    () => <input
                      type="text"
                      className="define_input"
                      id="relation"
                      name={"relation"}
                      defaultValue={props.sts.relation}
                      data-rule-required={true}
                      data-msg-required="Referer Relationship to Participant is required"
                    />
                  }
                />

              </div>
            </div>
          </div>

        </div>
        <div className="row d-flex justify-content-end">
          <div className="w-20-lg col-md-3"><a className="btn-1" onClick={props.submitReferDetail}>Save And Continue</a></div>
        </div>
      </form>
    </React.Fragment>

  );
}
