import React from 'react';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { Doughnut } from 'react-chartjs-2';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import CrmPage from '../../CrmPage';
import 'react-table/react-table.css'
import Countdown from 'react-countdown-now';
import DatePicker from 'react-datepicker';
import { ROUTER_PATH } from '../../../../../config.js';
import 'react-toastify/dist/ReactToastify.css';
import Pagination from "../../../../../service/Pagination.js";
import moment from 'moment';
import { checkItsNotLoggedIn, postData, getQueryStringValue, changeTimeZone, handleDateChangeRaw, reFreashReactTable } from '../../../../../service/common.js';
import { RosterDropdown, AnalysisDropdown, unfilledShiftTypeFilterOption } from '../../../../../dropdown/ScheduleDropdown.js';
import { TotalShowOnTable } from '../../../../../service/TotalShowOnTable';
import ManualMemberLookUp from './ManualMemberLookUp';
import ScheduleHistory from './ScheduleHistory';
import { connect } from 'react-redux';
import { AllUpdates } from './ParticipantAllUpdates';


const requestData = (pageSize, page, sorted, filtered,participantId) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = {pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,participantId:participantId};
        postData('crm/CrmParticipant/participant_shifts', Request).then((result) => {
          //console.log(result.total_count);
            let filteredData = result.data.data;

            const res = {
                rows: filteredData,
                pages: (result.data.count),
                total_count: (result.data.total_count),
            };
            resolve(res);
        });

    });
};
class Createshift extends React.Component {
  constructor(props, context) {
      super(props, context);
      this.participantDetailsRef = React.createRef();
      this.state = {
        sub_count: 0,
        all_shift: 0
      }
  }
    componentDidMount() { 
    this.setState({participant_id:this.props.props.match.params.id});
    this.participantDetailsRef.current.wrappedInstance.getParticipantDetails(this.props.props.match.params.id);
      this.getLatestSage();
      this.getAllStages();
  }
  showModal_PE = (id) => {
    if (id == "AllupdateModalShow") {
        var str = { participantId: this.state.participant_id, };
        this.notes_list(str);
        this.getAllStages();
    }

    let state = {};
    state[id] = true;

    this.setState(state)
}
closeModal_PE = (id) => {
    let state = {};
    state[id] = false;

    this.setState(state)
}
notes_list = (str) => {

    postData('crm/CrmParticipant/latest_updates', str).then((result) => {
        if (result.data.length>0) {

            this.setState({ allupdates: result.data });
        }
    })
}
getAllStages = () => {
    let stage_name = '';
    this.setState({ loading: true }, () => {
        var intakeStr = "{}";
        postData('crm/CrmStage/get_all_stage', intakeStr).then((result) => {

          if (result.status) {
              this.setState({ stage_info: result.data,stage_dropdown:result.stage });
          }
            this.setState({ loading: false });
        });
    });
}
getLatestSage = () => {
    let latestStage = '';

    var intakeStr = JSON.stringify({ crm_participant_id: this.props.props.match.params.id });
    postData('crm/CrmStage/get_latest_stage', intakeStr).then((result) => {

    if (result.status) {
         this.setState({ 
           latestDate: result.data.latest_date,
           latestStage: result.data[0].latest_stage_name, 
           latestStageStates: result.data[0].latest_stage,
           booking_status:result.data.booking_status,
           fund_lock:result.locked  });
     }
        this.setState({ loading: false });
    });

}
  fetchData = (state, instance) => {
      // function for fetch data from database
      this.setState({ loading: true });
      requestData(
          state.pageSize,
          state.page,
          state.sorted,
          state.filtered,
          this.props.props.match.params.id
      ).then(res => {
          this.setState({
              shiftListing: res.rows,
              pages: res.pages,
              total_count: res.total_count,
              loading: false,
              selectAll: 0,
              userSelectedList: [],
              selected: []
          });
      });
  }
  searchBox = (key, value) => {
      var state = {}
      state[key] = value;
      this.setState(state, () => {
          var filter = { search_box: this.state.search_box, shift_type: this.state.shift_type, shift_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date, push_to_app: this.state.active_panel }
          this.setState({ filtered: filter });
      });
  }
  render() {
    const data = {
      labels: ['Other Shift', 'New Shift'],
      datasets: [{
        data: [(this.state.all_shift - this.state.sub_count), this.state.sub_count],
        backgroundColor: ['#d04170', '#d04170'],
        hoverBackgroundColor: ['#d04170', '#d04170'],
        borderWidth: 0,
      }],
    };
    const columns = [
      
      (this.state.active_panel == 'unfilled') ? {
      id: "checkbox", accessor: "",
      Cell: ({ original }) => {
        return (<span className="w_50 w-100  mt-2">
          <input disabled={(original.push_to_app == 2) ? true : false} type='checkbox' className="checkbox1" checked={this.state.selected[original.id] === true} onChange={() => this.toggleRow(original.id)} />
          <label>
            <div className="d_table-cell"> <span onClick={() => original.push_to_app == 2 ? '' : this.toggleRow(original.id)}  ></span></div>
          </label>
        </span>);
      },
      Header: x => {
        return (
          <span className="w_50 w-100  mb-2">
            <input type='checkbox' className="checkbox1" checked={this.state.selectAll === 1} ref={input => {
              if (input) { input.indeterminate = this.state.selectAll === 2 }
            }}
              onChange={() => this.toggleSelectAll()} />
            <label>
              <div className="d_table-cell"> <span onClick={() => this.toggleSelectAll()}></span></div>
            </label>
          </span>
        );
      },
      sortable: false,
    } : { width: 0, headerStyle: { border: "0px solid #fff" } },

    { Header: 'ID',  accessor: 'id', filterable: false,},
    { Header: 'Date', accessor: 'shift_date', filterable: false, Cell: props => <span>{changeTimeZone(props.original.shift_date, "DD/MM/YYYY")}</span> },
    { Header: 'For', accessor: 'participantName', filterable: false, },
    { Header: 'Start', accessor: 'start_time', filterable: false, Cell: props => <span>{changeTimeZone(props.original.start_time, 'LT')}</span> },
    { Header: 'Duration', accessor: 'duration', filterable: false, },
    // {
    //   Header: 'Suburb', accessor: '', sortable: false, filterable: false,
    //   Cell: props => <span>
    //     {(props.original.address.length > 0) ? props.original.address[0].suburb : "N/A"}
    //   </span>
    // },
    {
      Cell: (props) => <span className="action_ix__">
        {/* <i onClick={() => this.setState({ manual_assign: true, selectShiftId: props.original.id })} className="fa icon-fillshift2-ie icon_h-1 mr-2 color"></i>
        <i onClick={() => this.setState({ historyId: props.original.id, open_history: true })} className="icon icon-pending-icons icon_h-1 mr-2"></i> */}
        <Link to={{ pathname: '/admin/crm/shiftdetails/' + props.original.id, state: this.props.props.location.pathname }}>
          <i className="icon icon-views"></i></Link></span>, Header: <div className="">Action</div>, style: {
            "textAlign": "right",
          }, headerStyle: { border: "0px solid #fff" },
      Header: <TotalShowOnTable countData={this.state.total_count} />,
      sortable: false
    },
    {
      expander: true, sortable: false,
      Expander: ({ isExpanded, ...rest }) =>
        <div>{isExpanded ? <i className="icon icon-arrow-up"></i> : <i className="icon icon-arrow-down"></i>}</div>,
      headerStyle: { border: "0px solid #fff" },
    }]

    return (
      <div>
      <CrmPage ref={this.participantDetailsRef} pageTypeParms={'participant_shift'} />
        <section className="manage_top">
          <div className="container-fluid">

            <div className="row  _Common_back_a">
              <div className="col-lg-12 col-md-12">
               <Link className="d-inline-flex" to={ROUTER_PATH + 'admin/crm/prospectiveparticipants'}><span className="icon icon-back-arrow back_arrow"></span></Link></div>
               </div>
            <div className="row"><div className="col-lg-12 col-md-12"><div className="bor_T"></div></div></div>


            <div className="row">
                <div className="col-lg-12  col-md-12">
                    <div className="row d-flex py-4">
                        <div className="col-md-6 align-self-center br-1">
                            <div className="h-h1 ">
                               {this.props.showPageTitle}
                        </div>
                        </div>
                        <div className="col-md-6">
                                <div className="Lates_up_1">
                                    <div className="Lates_up_a col-md-3 align-self-center">
                                        Latest
                                        Update:
                                    </div>
                                    <div className="col-md-9 justify-content-between pr-0">
                                        <div className="Lates_up_b">
                                            <div className="Lates_up_txt"><b>{(this.state.latestStage) ? this.state.latestStage : "Stage 1:NDIS Intake Participant Submission"} {(this.state.stageId != 6) ? "Information" : ""}</b></div>
                                            {/* <div className="Lates_up_btn br-1 bl-1"><i className="icon icon-view1-ie"></i><span>View Attachment</span></div> */}
                                            <div className="Lates_up_btn" onClick={() => this.showModal_PE("AllupdateModalShow")}><i className="icon icon-view1-ie"></i><span>View all Updates</span></div>
                                            <AllUpdates allupdates={this.state.allupdates} stages={(this.state.stage_info) ? this.state.stage_info : ''} onSelectDisp={(this.state.newSelectedValue) ? this.state.newSelectedValue : ''} selectedChange1={(e) => this.selectChange(e, 'view_by_status')} selectedChange={(e) => this.selectChange(e, 'assign_to')} showModal={this.state.AllupdateModalShow} handleClose={() => this.closeModal_PE("AllupdateModalShow")} />
                                        </div>
                                        <div className="Lates_up_2">
                                            <div className="Lates_up_txt2 btn-1">Date: {this.state.latestDate}</div>
                                            {/* <div className="Lates_up_time_date"> Date: 01/01/01 - 11:32AM</div> */}
                                        </div>
                                    </div>
                                </div>


                            </div>
                    </div>
                    <div className="row"><div className="col-md-12"><div className="bt-1"></div></div></div>
                </div>
            </div>

            {/* <div className="row"><div className="col-lg-12 col-md-12"><div className="bor_T"></div></div></div> */}

          {  // <div className="row">
            //   <div className="col-lg-1"></div>
            //   <div className="col-lg-10 col-md-12">
            //     <ul className="landing_graph my-5">
            //
            //       <li className="radi_2">
            //         <h2 className="text-center P_15_b cl_black">Shifts Filled  This Month:</h2>
            //         <div className="row pb-3 pt-4 w-100 mx-auto Graph_flex">
            //
            //           <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3 ">
            //             <CounterShowOnBox minNumber={2} counterTitle={this.state.count_fill_by_admin} />
            //           </div>
            //           <div className="W_M_Y_box col-md-4 col-xs-12 d-inline-flex align-self-center">
            //             <div className="vw_bx12 mx-auto">
            //               <h4>View By</h4>
            //               <span onClick={() => this.handleChange('view_type_by_admin', 'week')} className={(this.state.view_type_by_admin == 'week') ? 'color' : ''}>Week</span><br />
            //               <span onClick={() => this.handleChange('view_type_by_admin', 'month')} className={(this.state.view_type_by_admin == 'month') ? 'color' : ''}>Month </span>
            //             </div>
            //           </div>
            //         </div>
            //       </li>
            //
            //       <li className="radi_2">
            //         <h2 className="text-center P_15_b cl_black">Todays Total:</h2>
            //
            //         <div className="row pb-3 w-100 mx-auto Graph_flex">
            //
            //           <div className="text-center align-self-center col-md-5 col-xs-12">
            //             <div className="myChart12 mx-auto" >
            //               <Doughnut data={data} height={210} className="myDoughnut" legend={{ display: false }} />
            //             </div>
            //           </div>
            //           <div className="col-md-4 col-xs-12 text-center d-inline-flex align-self-center">
            //             <div className="myLegend mx-auto">
            //               <h4 className="clr1">New = {this.state.sub_count}</h4>
            //               <h4 className="clr2">Total = {this.state.all_shift}</h4>
            //             </div>
            //           </div>
            //           <div className="W_M_Y_box P_15_T col-md-3 col-xs-12 pb-3 d-inline-flex align-self-center">
            //             <div className="vw_bx12 mx-auto">
            //               <h5 className=""><b>View by</b></h5>
            //               <span onClick={() => this.handleChange('view_type_graph', 'week')} className={this.state.view_type_graph == 'week' ? 'color' : ''}>Week</span><br />
            //               <span onClick={() => this.handleChange('view_type_graph', 'month')} className={this.state.view_type_graph == 'month' ? 'color' : ''}>Month </span><br />
            //             </div>
            //           </div>
            //
            //         </div>
            //       </li>
            //
            //       <li className="radi_2">
            //         <h2 className="text-center P_15_b cl_black">Shifts Filled via App:</h2>
            //         <div className="row pb-3 pt-4 w-100 mx-auto Graph_flex">
            //           <div className="col-md-8 col-xs-12 d-inline-flex align-self-center justify-content-center mb-3 ">
            //             <CounterShowOnBox minNumber={2} counterTitle={this.state.count_fill_by_app} />
            //           </div>
            //           <div className="W_M_Y_box col-md-4 col-xs-12 d-inline-flex align-self-center">
            //             <div className="vw_bx12 mx-auto">
            //               <h4>View By</h4>
            //               <span onClick={() => this.handleChange('view_type_by_app', 'week')} className={(this.state.view_type_by_app == 'week') ? 'color' : ''}>Week</span><br />
            //               <span onClick={() => this.handleChange('view_type_by_app', 'month')} className={(this.state.view_type_by_app == 'month') ? 'color' : ''}>Month </span>
            //             </div>
            //           </div>
            //         </div>
            //       </li>
            //
            //     </ul>
            //   </div>
            //
            //
            // </div>
          }



            <div className="row">

              <div className="col-lg-12">
                <div className="tab-content">

                  <div role="tabpanel" className="tab-pane active" id={this.state.active_panel}>
                    <div className="row">
                      <div className="col-md-12">
                        <div className="row">
                          <div className="col-md-12 P_7_TB"><h3>Shifts:</h3></div>
                          <div className="col-md-12"><div className="bor_T"></div></div>
                        </div>
                      </div>
                    </div>


                    <div className="row P_25_T">
                      <div className="col-md-6 col-sm-8">
                        <label>Search</label>
                        <div className="table_search_new">
                          <input type="text" onChange={(e) => this.searchBox('search_box', e.target.value)} name="" value={this.state.search_box || ''} />
                          <button type="submit">
                            <span className="icon icon-search"></span>
                          </button>
                        </div>
                      </div>
                    {  // <div className="col-md-3 col-sm-4 mt-1">
                      //   <label></label>
                      //   <div className="box">
                      //     <Select clearable={false} name="shift_type" simpleValue={true} searchable={false} onChange={(e) => this.searchBox('shift_type', e)}
                      //       options={unfilledShiftTypeFilterOption(0)} value={this.state.shift_type} placeholder="Shift Type/Department" />
                      //
                      //   </div>
                      // </div>
}
                      <div className="col-md-6 col-sm-12">
                        <div className="row">
                        {  // <div className="col-sm-4">
                          //   <label>On</label>
                          //   <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw} minDate={moment()} utcOffset={0} isClearable={true} name="shift_date" onChange={(e) => this.searchBox('shift_date', e)} selected={this.state['shift_date'] ? moment(this.state['shift_date'], 'DD-MM-YYYY') : null} dateFormat="DD-MM-YYYY" className="text-center px-0" placeholderText="00/00/0000" />
                          // </div>
                        }
                          <div className="col-sm-6">
                            <label>From</label>
                            <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw} minDate={moment()} utcOffset={0} isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                          </div>
                          <div className="col-sm-6">
                            <label>To</label>
                            <DatePicker autoComplete="new-password" onChangeRaw={handleDateChangeRaw} minDate={moment()} utcOffset={0} isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />

                          </div>
                        </div>
                      </div>

                    </div>

                    <div className="row">
                      <div className="col-md-12 schedule_listings">
                        <ReactTable
                          PaginationComponent={Pagination}
                          ref={this.reactTable}
                          columns={columns}
                          data={this.state.shiftListing}
                          pages={this.state.pages}
                          loading={this.state.loading}
                          onFetchData={this.fetchData}
                          filtered={this.state.filtered}
                          defaultPageSize={10}
                          className="-striped -highlight"
                          noDataText="No Record Found"
                          minRows={2}

                          previousText={<span className="icon icon-arrow-left privious"></span>}
                          nextText={<span className="icon icon-arrow-right next"></span>}

                          SubComponent={(props) => <div className="other_conter"><div className="col-md-6">
                            <ul>

                              <li><span className="color">End: </span> {changeTimeZone(props.original.end_time, 'LT')}</li>



                            </ul>
                          </div>
                            <div className="col-md-6 text-right">
                              <ul>
                                <li><span className="start_in_color">Start In: </span> <Countdown date={Date.now() + props.original.diff} /></li>
                              </ul>
                            </div>
                          </div>}
                        />

                      </div>
                    </div>

                    <ScheduleHistory  open_history={false} />
                    <ManualMemberLookUp modal_show={false} />

                    {/* <ScheduleHistory open_history={this.state.open_history} shiftId={this.state.historyId} closeHistory={this.closeHistory} />
                    {(this.state.active_panel == 'unfilled') ? <div className="row text-right">
                      <button onClick={this.moveToAutoFill} className="default_but_remove">
                        <i className="icon icon-circule update_button_g mr-2"></i></button>
                      <button onClick={this.moveToUnfilledOnApp} className="default_but_remove" >
                        <i className="icon icon-mobile update_button_g"></i></button>
                    </div> : ''} */}
                    {/* <ManualMemberLookUp modal_show={this.state.manual_assign} closeModel={this.closeModel} shiftId={this.state.selectShiftId} /> */}
                  </div>
                </div>
              </div>
            </div>


          </div>
        </section>
      </div>
    );

  }
}
const mapStateToProps = state => {
      return {
        showPageTitle: state.DepartmentReducer.activePage.pageTitle,
        showTypePage: state.DepartmentReducer.activePage.pageType,
          participaintDetails :state.DepartmentReducer.participaint_details
      }
  };
export default connect(mapStateToProps)(Createshift);
