import React, { Component } from 'react';
import ReactTable from "react-table";
import Pagination from "service/Pagination.js";
import { ROUTER_PATH, PAGINATION_SHOW } from 'config.js';
import { checkItsNotLoggedIn, postData, reFreashReactTable, archiveALL } from 'service/common.js';
import { connect } from 'react-redux';
import { defaultSpaceInTable } from 'service/custom_value_data.js';

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {
        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('recruitment/RecruitmentUserManagement/get_round_robin_data_list', Request).then((result) => {
            if (result.status) {
                let filteredData = result.data;
                const res = {
                    rows: filteredData,
                    pages: (result.count),
                    //all_count:result.all_count,
                };
                resolve(res);
            }
        });
    });
};

class RoundRobinManagement extends Component {

    constructor() {
        super();
        checkItsNotLoggedIn(ROUTER_PATH);
        this.state = {
            recuiterAdminList: []
        }
        this.reactTable = React.createRef();
    }
    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                recuiterAdminList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    updateRoundRobinStatus(adminId, roundRobinStatus) {
        //alert(adminId+'--'+roundRobinStatus);
        let data = {};
        data['id'] = adminId;
        data['status'] = roundRobinStatus == 1 ? 0 : 1;
        let msg = 'Are you sure, you want to switch off this user in round robin management?';
        if (roundRobinStatus==1) {
            msg = 'Are you sure, you want to switch on this user in round robin management?';
        }
        archiveALL(data, msg, 'recruitment/RecruitmentUserManagement/update_round_robin_status').then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        });


    }


    render() {








        return (
            <React.Fragment>

                <div className="row pt-5">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1> {this.props.showPageTitle}


                        </h1>
                    </div>
                </div>
                {/* row ends */}


                <div className="row mt-5">


                    <div className="col-sm-12 no_pd_l">

                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">

                            <ReactTable
                                columns={[
                                    {
                                        // Header: "Name:",
                                        accessor: "name",
                                        minWidth: 80,
                                        headerClassName: '_align_c__ header_cnter_tabl',
                                        Header: () =>
                                            <div>
                                                <div className="ellipsis_line1__">Name</div>
                                            </div>
                                        ,
                                        className: '_align_c__',
                                        Cell: props => <span>{defaultSpaceInTable(props.value)}</span>

                                    },
                                    {
                                        // Header: "HCMGR-Id:",
                                        accessor: "id",
                                        headerClassName: '_align_c__ header_cnter_tabl',
                                        Header: () =>
                                            <div>
                                                <div className="ellipsis_line1__">HCMGR-Id</div>
                                            </div>
                                        ,
                                        className: '_align_c__',
                                        Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
                                    },
                                    {
                                        // Header: "Recruitment Area:",
                                        accessor: "area",
                                        sortable: false,
                                        filterable: false,
                                        headerClassName: '_align_c__ header_cnter_tabl',
                                        Header: () =>
                                            <div>
                                                <div className="ellipsis_line1__">Recruitment Area</div>
                                            </div>
                                        ,
                                        className: '_align_c__',
                                        //  minWidth:100,
                                        Cell: (props) => (
                                            <span>
                                                <div className="text-center text_ellip">
                                                    {props.original.hasOwnProperty('area') && props.original.area ? props.original.area : ''}
                                                </div>
                                            </span>

                                        )
                                    },

                                    {
                                        // Header: "Round Robin:",
                                        accessor: "round_robin_status",
                                        sortable: false,
                                        filterable: false,
                                        minWidth: 100,
                                        headerClassName: '_align_c__ header_cnter_tabl',
                                        Header: () =>
                                            <div>
                                                <div className="ellipsis_line1__">Round Robin</div>
                                            </div>
                                        ,
                                        className: '_align_c__',
                                        Cell: (props) => (
                                            <span>
                                                <div className="d-inline-flex align-items-center">
                                                    Round Robin &nbsp;
    
                                                    <span className="cmn_font_clr">
                                                        {props.original.hasOwnProperty('round_robin_status') && props.original.round_robin_status == 1 ? "ON" : "OFF"}
                                                    </span>
                                                    <label class="custm_switch ml_10p">
                                                        <input
                                                            type="checkbox"
                                                            id={"checkie_" + props.index}
                                                            checked={props.original.hasOwnProperty('round_robin_status') && props.original.round_robin_status == 1 ? true : false}
                                                            onClick={() => this.updateRoundRobinStatus((props.original.hasOwnProperty('id') && props.original.id ? props.original.id : 0), (props.original.hasOwnProperty('round_robin_status') && props.original.round_robin_status ? props.original.round_robin_status : 0))}

                                                        />
                                                        <span class="swtch_slider round"></span>
                                                    </label>
                                                </div>
                                            </span>
                                        )
                                    }
                                ]}
                                PaginationComponent={Pagination}
                                showPagination={this.state.recuiterAdminList.length > PAGINATION_SHOW ? true : false}
                                manual
                                ref={this.reactTable}
                                data={this.state.recuiterAdminList}
                                pages={this.state.pages}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                noDataText="No recruiters found"
                                defaultPageSize={10}
                                minRows={2}
                                className="-striped -highlight"

                                defaultSorted={[
                                    {
                                        id: "name",
                                        desc: true
                                    }
                                ]}

                            />

                        </div>


                    </div>



                </div>
                {/* row ends */}


            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(RoundRobinManagement);

