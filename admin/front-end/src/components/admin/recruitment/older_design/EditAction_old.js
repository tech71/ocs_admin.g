import React, { Component } from 'react';
import ReactTable from "react-table";
import Select from 'react-select-plus';


class EditAction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actionStatus: ''
        }
    };


    render() {

        const task = [
            {
                id: '00120812-12',
                name: 'Nonkosi Joy',
                email: 'marcelljohns@yahoo.com',
                phone: '040288999',
                status: 'Pending'
            },
            {
                id: '00120812-12',
                name: 'Nonkosi Joy',
                email: 'marcelljohns@yahoo.com',
                phone: '040288999',
                status: 'Pending'
            },
            {
                id: '00120812-12',
                name: 'Nonkosi Joy',
                email: 'marcelljohns@yahoo.com',
                phone: '040288999',
                status: 'Pending'
            },
            {
                id: '00120812-12',
                name: 'Nonkosi Joy',
                email: 'marcelljohns@yahoo.com',
                phone: '040288999',
                status: 'Pending'
            },
            {
                id: '00120812-12',
                name: 'Nonkosi Joy',
                email: 'marcelljohns@yahoo.com',
                phone: '040288999',
                status: 'Pending'
            },

        ];

        var options = [
            { value: 'one', label: 'In Progress' },
            { value: 'two', label: 'Completed', clearableValue: false }
        ];

        return (
            <div className={'customModal ' + (this.props.showModal ? ' show' : ' ')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        New Group Interview for Applicant
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className="row">

                        <div className="col-md-12 col-sm-12">

                            <div className="row">
                                <div className="col-lg-2 col-md-2 col-sm-4">
                                    <div className="csform-group">
                                        <label className="">Action Name:</label>
                                        <p className="mt-3 clr_grey">Group Interview</p>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-2 col-sm-4">
                                    <div className="csform-group">
                                        <label className="">Action Type:</label>
                                        <p className="mt-3 clr_grey">Group Interview</p>
                                    </div>
                                </div>
                                <div className="col-lg-2 col-md-2 col-sm-4">
                                    <div className="csform-group">
                                        <label className="">Date:</label>
                                        <p className="mt-3 clr_grey">02/09/2019</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-4">
                                    <div className="csform-group">
                                        <label className="">time/Duration:</label>
                                        <div className="row">
                                            <div className="col-md-6"> <p className="mt-3 clr_grey"><strong>From:&nbsp;</strong> 12:15</p></div>
                                            <div className="col-md-6"> <p className="mt-3 clr_grey"><strong>To:&nbsp;</strong> 16:15</p></div>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-3 col-sm-4">
                                    <div className="csform-group">
                                        <label className="">Location:</label>
                                        <p className="mt-3 clr_grey">
                                            ONCALL Training Facility
                                            <span className="d-block">-Training</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    {/* row ends */}
                    <div className="bor_line"></div>


                    <div className="row">

                        <div className="col-md-6 col-sm-6">

                            <div className="csform-group">
                                <label className="">Assigned To Recruiter/s : <span className="asgnd_num clr_grey">(12)</span></label>
                                <div><small className="extra_sm_fnt clr_grey">Primary recruiter on this task is:</small></div>

                                <ul className="pr_rec_list mt-3 mb-3 cmnDivScrollBar__ prList_sclBar ">
                                    <li className="active">Ignor antonovich</li>
                                    <li>Frank Boehm</li>
                                    <li>Klavdia Dedova</li>
                                    <li>Thenjiwe Msutu</li>
                                    <li>Maria Illescas</li>
                                    <li>Grigoriy kozhuhov</li>
                                    <li>Martin Ebasto</li>
                                    <li>Nandie Patroli</li>
                                    <li>Frank Boehm</li>
                                    <li>Klavdia Dedova</li>
                                    <li>Thenjiwe Msutu</li>
                                    <li>Frank Boehm</li>
                                    <li>Klavdia Dedova</li>
                                    <li>Thenjiwe Msutu</li>
                                </ul>


                            </div>

                        </div>

                        <div className="col-md-6 col-sm-6">

                            <div className="csform-group ">
                                <label className='bg_labs'>Action Status:</label>
                                <div className="cmn_select_dv wid-200 ">
                                    <Select name="view_by_status"
                                        required={true} simpleValue={true}
                                        searchable={false} Clearable={false}
                                        placeholder="Select"
                                        options={options}
                                        onChange={(e) => this.setState({ actionStatus: e })}
                                        value={this.state.actionStatus}
                                    />
                                </div>
                            </div>


                            <div className="csform-group ">
                                <label className='bg_labs'>Relevant Task Notes:</label>
                                <p className="clr_grey wid-80P">lorem ipsum is a free text made for all free printing and
                                 typesetting lorem ipsum is a free text made for all free printing and typesetting
                                  lorem ipsum is a free text made for all free printing and typesetting</p>
                            </div>


                        </div>

                    </div>
                    {/* row ends */}


                    <div className="row mt-5">

                        

                        <div className="col-md-12 col-sm-12">

                            <h3 className="mt-3 avl_ints mb-4"><b>Next Available Interviews</b></h3>
                            <div className="row">
                                <div className="col-md-10 col-sm-10">
                                    <div className="csform-group">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input
                                            type="text"
                                                className="srch-inp "
                                                placeholder="Search for Applicants"
                                            />
                                            <i className="icon icon-search2-ie"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="data_table_cmn tble_txt_center tbl_fnt_sm edit_TaskTble ">

                                <ReactTable
                                    columns={[

                                        { Header: "Applicant Id:", accessor: "id" },
                                        { Header: "Name:", accessor: "name" },
                                        { Header: "Email:", accessor: "email" },
                                        { Header: "Phone:", accessor: "phone" },
                                        { Header: "Status:", accessor: "status" },
                                        {
                                            Header: "Actions",
                                            accessor: "actions",
                                            minWidth: 140,
                                            Cell: props => (
                                                <div>
                                                    <button className={" cmn-btn1 no_bor int_actnBtn"}> Re-send</button>
                                                    <button className={" cmn-btn1 no_bor int_actnBtn"}> Remove</button>
                                                </div>
                                            )
                                        }




                                    ]}

                                    defaultPageSize={3}
                                    data={task}
                                    pageSize={task.length}
                                    showPagination={false}
                                    className=""

                                />

                            </div>
                            <div className="text-right">
                                <small className="extra_sm_fnt">*Clicking save will automatically send an email invite to the applicant</small>
                            </div>


                        </div>



                    </div>
                    <div className="bor_line mt-5"></div>
                    <div className="row  pb-5 pt-3">
                        <div className="col-sm-12 text-right">
                            <button type="submit" className="btn cmn-btn1 create_quesBtn">Save Changes</button>
                        </div>
                    </div>

                </div>

            </div>
        )
    }

}

export default EditAction;