import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link } from 'react-router-dom';

import ApplicantNotesModal from './ApplicantNotesModal';
import ApplicantAttachmentModal from './ApplicantAttachmentModal';
import EditApplicantInfo from './EditApplicantInfo';
import { Panel, Button, ProgressBar, PanelGroup } from 'react-bootstrap';
import ReactTable from "react-table";
import ScrollArea from 'react-scrollbar';
import { Tabs, Tab } from 'react-bootstrap';

import './recruit2.css';



class ApplicantInfo extends Component {

    constructor() {
        super();
        this.state = {
            filterVal: '',
            ReviewOnline: '',
            timeLineStatus: '',
            documentStatus: 'Inactive',
            InterviewStatus: '',
            grpInterviewStatus: '',
            refcheckStatus: '',
            hiredStatus: '',
            cabStatus: '',
            loading: true,
            ApplicantNotes: false,
            ApplicantAttachment: false,
            EditApplicant: false
        }
    }

    changeIntStatus = (key, value) => {
        var state = {};
        state[key] = value;
        state[key + 'Color'] = value;
        this.setState(state);
    }


    componentDidMount() {
        setTimeout(() => {
            this.setState({ loading: false })
        }, 700)
    }

    render() {

        const current_applis = [
            {
                position: 'lvl 1 qualified area',
                area: 'welfare',
                employment: 'Full time',
                channel: 'Website'
            },
            {
                position: 'lvl 1 qualified area',
                area: 'welfare',
                employment: 'Full time',
                channel: 'Website'
            },
            {
                position: 'lvl 1 qualified area',
                area: 'welfare',
                employment: 'Full time',
                channel: 'Website'
            }
        ]

        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];

        var optionsAP = [
            { value: '0', label: 'Pending' },
            { value: '1', label: 'Success' },
            { value: '2', label: 'UnSuccess' },
            { value: '3', label: 'In-Progress' },
            { value: '4', label: 'Parked' },
        ];
        const ReferencesData = [{
            firstName: 'Tanner Linsley',
            lastName: "Tanner",
        },
        {
            firstName: 'Tanner Linsley',
            lastName: "Tanner",
        },
        {
            firstName: 'Tanner Linsley',
            lastName: "Tanner",
        }]


        const now = 60;
        return (
            <React.Fragment>

                <div className="row">

                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        <Link to="../applicants">
                            <span className="icon icon-back1-ie"></span>
                        </Link>

                    </div>
                </div>
                {/* row ends */}

                <div className="row">

                    <div className="col-lg-12 col-md-12 main_heading_cmn- d_flex1">

                        <div className=""><h1>Applicants - Applicant Info</h1></div>
                        <div className="ltst_upsBox"></div>

                    </div>
                </div>
                {/* row ends */}

                <div className="row mt-1">
                    <ProgressBar className="cmn_progressBar_1" now={now} label={`${now}% ` + 'Complete'} />
                </div>

                <div className="row bg_w">
                    <div className="app_infoBox__">
                        <div className="row">

                            <div className="col-md-12 col-sm-12 d_flex1  align-items-center pb_15p bor_bot_b">
                                <h4 className="flex-1 pr_15p"><strong>Applicant Info</strong></h4>
                                <button className="btn cmn-btn1 eye-btn">Edit Applicant Info</button>
                            </div>
                            {/* col-sm-12 ends */}

                            <div className="col-md-12 col-sm-12 bor_bot_b pb_15p">
                                <div className="row mt-3 d_flex1 flex_wrap1 pb_15p ">

                                    <div className="col-lg-2 col-md-2">
                                        <h3 className="pb_15p"><strong>Susan Mcsmith</strong></h3>
                                        <h4>(Id: APP-10844)</h4>

                                        <button className=" green_btn1 hired_btn">HIRED</button>
                                        <label className="customChecks chck_clr1 mt-1">
                                            <input type="checkbox" />
                                            <div className="chkLabs fnt_sm">Flag Applicant as Inappropriate</div>
                                        </label>
                                    </div>

                                    <div className="col-lg-8 col-md-8 pd_lr_20p">

                                        <div className="row">

                                            <div className="col-lg-4 col-md-4">
                                                <h5><strong>Applicant Information</strong></h5>

                                                <div className="cstm_Tble mt-2 apli_infTble1 ">
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        <div>04214 79713</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        <div>ajohnston@gmail.com</div>
                                                    </div>
                                                </div>
                                                <h5 className="mt-2"><strong>Primary Address</strong></h5>
                                                <div className="fnt_14 mt-1 mb-1">Unit 13/972, John street South Melbourne, VIC 3000 -Personal Home</div>

                                            </div>

                                            <div className="col-lg-4 col-md-4">

                                                <h5><strong>Reference</strong></h5>

                                                <div className="cstm_Tble mt-2 apli_infTble1 bor_bot_b">
                                                    <div className="cstm_tr">
                                                        <div><strong>Name:</strong></div>
                                                        <div>Laquitta Elliotte</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        <div>04214 79713</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        <div>ajohnston@gmail.com</div>
                                                    </div>
                                                </div>

                                                <div className="cstm_Tble mt-1 apli_infTble1">
                                                    <div className="cstm_tr">
                                                        <div><strong>Name:</strong></div>
                                                        <div>Laquitta Elliotte</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        <div>04214 79713</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        <div>ajohnston@gmail.com</div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div className="col-lg-4 col-md-4">

                                                <h5><strong></strong></h5>

                                                <div className="cstm_Tble mt-3 apli_infTble1 bor_bot_b">
                                                    <div className="cstm_tr">
                                                        <div><strong>Name:</strong></div>
                                                        <div>Laquitta Elliotte</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        <div>04214 79713</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        <div>ajohnston@gmail.com</div>
                                                    </div>
                                                </div>

                                                <div className="cstm_Tble mt-1 apli_infTble1">
                                                    <div className="cstm_tr">
                                                        <div><strong>Name:</strong></div>
                                                        <div>Laquitta Elliotte</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        <div>04214 79713</div>
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        <div>ajohnston@gmail.com</div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>


                                    <div className="col-lg-2 col-md-2 d_flex1 bor_l_b pd_lr_20p" style={{ alignItems: 'flex-end' }}>
                                        <div className="pb_10p">
                                            <h5><strong>Assigned Recruiter:</strong></h5>
                                            <h3 className="mt-1">Jane Goldeberg</h3>
                                        </div>
                                    </div>




                                </div>

                            </div>
                            {/* col-sm-12 ends */}


                            <div className="col-sm-12">


                                <div className="row d_flex1 mt-2">


                                    <div className="col-md-8 pd_r_20p">
                                        <h3 className="mt-3"><strong>Current Applications</strong></h3>

                                        <div className="data_table_cmn dataTab_accrdn_cmn tbl_flx2 header_center tble_2_clr mt-3 currAPli_tble tbl_fnt_sm">

                                            <ReactTable
                                                columns={[
                                                    {
                                                        Header: "",
                                                        accessor: "serial",
                                                        Cell: (props) => (props.index + 1),
                                                        width: 50
                                                    },
                                                    { Header: "Position Applied for:", accessor: "position" },
                                                    { Header: "Recruitement Area:", accessor: "area" },
                                                    { Header: "Employment Type:", accessor: "employment" },
                                                    { Header: "Application Channel:", accessor: "channel" }
                                                ]}

                                                defaultPageSize={3}
                                                data={current_applis}
                                                pageSize={current_applis.length}
                                                showPagination={false}
                                                className={''}


                                            />

                                        </div>

                                    </div>

                                    <div className="col-md-4 bor_l_b pd_l_20p">
                                        <h3 className="mt-3"><strong>Communication Logs</strong></h3>
                                        <div className="mt-3 log_btnBoo">
                                            <div>
                                                <button className="btn cmn-btn1 eye-btn">SMS</button>
                                            </div>
                                            <div>
                                                <button className="btn cmn-btn1 eye-btn">Email</button>
                                            </div>
                                            <div>
                                                <button className="btn cmn-btn1 eye-btn">Phone</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                            {/* col-sm-12 ends */}

                        </div>
                    </div>
                    {/* app_infoBox__ ends */}
                </div>

                <div className="row bg_w mt-3">
                    <div className="app_infoBox__">
                        <div className="row">

                            <div className="col-md-12 col-sm-12 d_flex1  align-items-center pb_15p bor_bot_b">
                                <h4 className="flex-1 pr_15p"><strong>Applicant Attachments</strong></h4>
                                <button className="btn cmn-btn2 m_l_10p">Manage Attachment</button>
                                <button className="btn cmn-btn1 m_l_10p">Add Attachment</button>
                            </div>

                            <div className="col-md-12 col-lg-12">

                                <div className="row mt-3 d_flex1 flex_wrap1">

                                    <div className="col-md-7 pd_r_20p">

                                        <div className="row d_flex1 flex_wrap1 doc_Cat_row">
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                            <div className="col-md-4 mb_20p">
                                                <h5 className="pb_10p"><strong>Document Category Header</strong></h5>
                                                <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-md-5 bor_l_b pd_l_20p">
                                        <h3 className="mt-3"><strong>Other Attachments</strong></h3>

                                        <div className="other_Attch d_flex1 flex_wrap1 mt-3">
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Resume</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Other</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Cover letter</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Qualifications</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Other</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Qualifications2</button></div>
                                            <div><button className="btn eye-btn cmn-btn1 wid_100p">Other</button></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


                <div className="row mt-3">
                    <div className="col-md-12 col-lg-12 bor_bot_b pb_10p">
                        <h3><strong>Recruitment Progess</strong></h3>
                    </div>


                    <div className="col-md-12 text-center" >
                        {/* <PanelGroup accordion id="accordion-controlled-example" activeKey={this.state.activeKey} onSelect={this.handleSelect}> */}




                        <div className="Version_timeline_4 timeline_1 rec_applicatn_timeline" style={{
                            display: "inline-flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            margin: "0px auto",
                        }}>


                            {/* Start 1 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>1</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="1">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 1</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Review Online Application</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">
                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">1.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">

                                                                    <div class="Recruit_Time_header">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Review Answers</strong></h3>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div style={{ marginTop: "-25px" }}>
                                                                        <div className="Line_base_tabs">
                                                                            <Tabs defaultActiveKey="SeekAnswers" id="uncontrolled-tab-example">
                                                                                <Tab eventKey="SeekAnswers" title="Seek Answers">
                                                                                    <div className="Seek_Q_ul mt-1">
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-cross-icons"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>

                                                                                    </div>
                                                                                    <div className="text-left"><a className="under_l_tx">View all Answers</a></div>
                                                                                </Tab>
                                                                                <Tab eventKey="WebsiteAnswers" title="Website Answers">
                                                                                    <div className="Seek_Q_ul  mt-1">
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-cross-icons"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                        <div className="Seek_Q_li w-100"><span><i className="icon icon-accept-approve1-ie"></i></span>
                                                                                            <div>From Wikipedia, the free encyclopedia</div>
                                                                                        </div>
                                                                                    </div>

                                                                                </Tab>
                                                                            </Tabs>
                                                                        </div>
                                                                    </div>


                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            <div>Complete by: Smith Roy</div>
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 1 */}










                            {/* Start 2 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>2</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="2">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 2</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Phone Interview</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">
                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">2.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 2.1</strong></h3>
                                                                            <h2>Phone Interview</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">
                                                                                    <button class="btn cmn-btn1 phone-btn">Call Application</button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row d-flex justify-content-center">
                                                                        <div className="col-lg-12 Rec_center_s_1a mt-2 mb-1"><strong>Applicant Classification:</strong></div>
                                                                    </div>

                                                                    <div className="row d-flex justify-content-center">
                                                                        <div className="col-lg-4 px-0">
                                                                            <div className="s-def1 s1">
                                                                                <Select name="participant_assessment"
                                                                                    required={true}
                                                                                    simpleValue={true}
                                                                                    searchable={false}
                                                                                    clearable={false}
                                                                                    options={optionsAP}
                                                                                    value="1"
                                                                                    placeholder="Filter by: Unread"
                                                                                    className={'custom_select'} />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 2 */}









                            {/* Start 3 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>3</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="3">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 3</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Group Interview</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">3.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 3.1</strong></h3>
                                                                            <h2>Schedule Interview</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">
                                                                                    {/* <span class=" cmn-btn1 sp_btn_p"><span>Start Date </span><i class="icon icon-edit5-ie"></i></span> */}
                                                                                    <div className="cmn_select_dv mg_slct1  slct_des23 slct_s1">
                                                                                        <Select
                                                                                            className="custom_select"
                                                                                            name="department"
                                                                                            multi={true}
                                                                                            clearable={false}
                                                                                            searchable={false}
                                                                                            clearable={false}

                                                                                            onChange={(e) => { this.props.departmentAllotedTo(this.props.index, this.props.original, e, 1) }}
                                                                                            options={optionsAP} placeholder="Department" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="mt-2">
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Interview Details:</strong></div>
                                                                            <div className="Stage_Left_2"><a className="O_btn_1">Invitation Send- 01/01/2019</a></div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Date:</strong> 10/10/2018</div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Recruiter in Charge:</strong> 10/10/2018</div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Location:</strong> ONCALL Training Faacility - Training Room</div>
                                                                        </div>
                                                                    </div>


                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">3.2</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 3.2</strong></h3>
                                                                            <h2>Applicant Responses</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div className="limt_flex_set_0 mt-2">
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Invitation Sent:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1">01/01/2019</div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Applicant Response:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><a className="O_btn_1">Accepted - 01/01/2019</a></div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="mb-2"><a className="limt_btns w-z w1">Resend Invite</a></div>
                                                                            <div className="my-2"><a className="limt_btns">Reschedule</a></div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">3.3</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 3.3</strong></h3>
                                                                            <h2>Group Interview Results</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row d-flex justify-content-center">
                                                                        <div className="col-lg-12 Rec_center_s_1a mt-2 mb-1"><strong>Group Interview Results:</strong></div>
                                                                    </div>
                                                                    <div className="row d-flex justify-content-center">
                                                                        <div>
                                                                            <a className="O_btn_1 w-big my-1">Applicant Successful!</a><br />
                                                                            <div class="text-center"><a class="under_l_tx">View all Answers</a></div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 3 */}









                            {/* Start 4 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>4</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="4">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 4</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Mandatory Documentation</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">4.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Document Checklist</strong></h3>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">
                                                                                    <button className="btn cmn-btn1">Add Attachment</button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="cstmSCroll1">
                                                                        <ScrollArea
                                                                            speed={0.8}
                                                                            className="area"
                                                                            contentClassName="content"
                                                                            horizontal={false}
                                                                            style={{ maxHeight: '350px', paddingRight: '30px' }}
                                                                        >

                                                                            <div className="CheckList_Mand_Option">

                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>1. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_2" href="#">Mandatory</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>2. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_2" href="#">Mandatory</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>3. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_2" href="#">Mandatory</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>4. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_1" href="#">Optional</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>5. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_1" href="#">Optional</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Check_Mand_Option_li">
                                                                                    <div className="Ch-MO-1"><span>6. Resume</span></div>
                                                                                    <div className="Ch-MO-2">
                                                                                        <a className="Man_btn_1" href="#">Optional</a>
                                                                                    </div>
                                                                                    <div className="Ch-MO-3">
                                                                                        <span>
                                                                                            <i className="icon icon-accept-approve2-ie"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </ScrollArea>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">4.2</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Postion and <br />Award Levels</strong></h3>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="mt-1">
                                                                        <div className="Award_list_table_">
                                                                            <div className="Award_list_header_">
                                                                                <div className="Award_col_1">
                                                                                    <strong className="justify-content-start w-100 d-flex">Work Area:</strong>
                                                                                </div>
                                                                                <div className="Award_col_2">
                                                                                    <strong>Level:</strong>
                                                                                </div>
                                                                                <div className="Award_col_3">
                                                                                    <strong>Paypoint:</strong>
                                                                                </div>
                                                                            </div>

                                                                            <div className="Award_list_">
                                                                                <div className="Award_list_col_1">
                                                                                    <strong>1.</strong>
                                                                                </div>
                                                                                <div className="Award_list_col_2">
                                                                                    <div className="w-100">
                                                                                        <div className="s-def1 s1">
                                                                                            <Select name="participant_assessment"
                                                                                                required={true}
                                                                                                simpleValue={true}
                                                                                                searchable={false}
                                                                                                clearable={false}
                                                                                                options={optionsAP}
                                                                                                value="1"
                                                                                                placeholder="Filter by: Unread"
                                                                                                className={'custom_select'} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Award_list_col_3">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                                <div className="Award_list_col_4">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="Award_list_">
                                                                                <div className="Award_list_col_1">
                                                                                    <strong>2.</strong>
                                                                                </div>
                                                                                <div className="Award_list_col_2">
                                                                                    <div className="w-100">
                                                                                        <div className="s-def1 s1">
                                                                                            <Select name="participant_assessment"
                                                                                                required={true}
                                                                                                simpleValue={true}
                                                                                                searchable={false}
                                                                                                clearable={false}
                                                                                                options={optionsAP}
                                                                                                value="1"
                                                                                                placeholder="Filter by: Unread"
                                                                                                className={'custom_select'} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Award_list_col_3">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                                <div className="Award_list_col_4">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="Award_list_">
                                                                                <div className="Award_list_col_1">
                                                                                    <strong>3.</strong>
                                                                                </div>
                                                                                <div className="Award_list_col_2">
                                                                                    <div className="w-100">
                                                                                        <div className="s-def1 s1">
                                                                                            <Select name="participant_assessment"
                                                                                                required={true}
                                                                                                simpleValue={true}
                                                                                                searchable={false}
                                                                                                clearable={false}
                                                                                                options={optionsAP}
                                                                                                value="1"
                                                                                                placeholder="Filter by: Unread"
                                                                                                className={'custom_select'} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="Award_list_col_3">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                                <div className="Award_list_col_4">
                                                                                    <span className="">
                                                                                        <input type="text" className="border-black" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="Recruit_Time_header mt-3">
                                                                        <div className="Rec_Left_s_1">&nbsp;</div>
                                                                        <div className="Rec_Right_s_1">
                                                                            <button className="btn cmn-btn1 w-100">Submit</button>
                                                                            <span class="Time_line_error_msg">Awaiting Response - Sent 24-01-2020- 09:42am</span>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 4 */}




                            {/* Start 5 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>5</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="5">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 5</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Reference Checks</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">5.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Review References</strong></h3>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="">
                                                                        <ReactTable
                                                                            data={ReferencesData}
                                                                            columns={[

                                                                                {
                                                                                    Header: "First Name",
                                                                                    accessor: "firstName",
                                                                                    headerClassName: 'hdrCls',
                                                                                    className: (this.state.activeCol === 'firstName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_1 pl-0 pr-0',
                                                                                    Cell: (props) => (<div className="text_ellip_2line text-left">
                                                                                        <strong>{props.original.firstName}</strong>
                                                                                    </div>)
                                                                                },
                                                                                {
                                                                                    Header: "Last Name",
                                                                                    accessor: "lastName",
                                                                                    headerClassName: 'hdrCls',
                                                                                    className: (this.state.activeCol === 'lastName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_2  pl-0 pr-0 ',
                                                                                    Cell: (props) => (<div className="text_ellip_2line text-left">
                                                                                        {props.original.lastName}
                                                                                    </div>)
                                                                                },
                                                                                {
                                                                                    Header: "Last Name",
                                                                                    accessor: "lastName",
                                                                                    headerClassName: 'hdrCls',
                                                                                    className: (this.state.activeCol === 'lastName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_3  pl-0 pr-0',
                                                                                    Cell: (props) => (<div className="text_ellip_2line text-right">
                                                                                        <a className="Req_btn_out_1 R_bt_co_green">Approvel</a>
                                                                                    </div>)
                                                                                },
                                                                                {
                                                                                    Header: "Expand",
                                                                                    headerClassName: 'hdrCls',
                                                                                    className: (this.state.activeCol === 'lastName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_4  pl-0 pr-0 ',
                                                                                    expander: true,
                                                                                    Header: () => <strong>More</strong>,
                                                                                    width: 65,
                                                                                    Expander: ({ isExpanded, ...rest }) => (
                                                                                        <div className="text-right">
                                                                                            {isExpanded ? (
                                                                                                <i className="icon icon-arrow-down icn_ar1"></i>
                                                                                            ) : (
                                                                                                    <i className="icon icon-arrow-right icn_ar1"></i>
                                                                                                )}
                                                                                        </div>
                                                                                    ),
                                                                                }


                                                                            ]}

                                                                            defaultPageSize={5}
                                                                            showPagination={false}
                                                                            SubComponent={() => <div className="References_table_SubComponent">

                                                                                <div className="col-lg-offset-1 col-lg-10">

                                                                                    <div className="row mt-1">
                                                                                        <div className="col-lg-5 text-left">
                                                                                            <label className="My_Label_">Referentce Status:</label>
                                                                                            <div className="s-def1 s1">
                                                                                                <Select name="participant_assessment"
                                                                                                    required={true}
                                                                                                    simpleValue={true}
                                                                                                    searchable={false}
                                                                                                    clearable={false}
                                                                                                    options={optionsAP}
                                                                                                    value="1"
                                                                                                    placeholder="Filter by: Unread"
                                                                                                    className={'custom_select'} />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="row mt-1">
                                                                                        <div className="col-lg-12 col-sm-12 text-left">
                                                                                            <label className="My_Label_">Relevant Notes:</label>
                                                                                            <textarea className="w-100 border-black" rows="5"></textarea>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div className="row">
                                                                                        <div className="col-sm-12 text-right mb-3">
                                                                                            <button class="btn cmn-btn1">Save</button>
                                                                                        </div>
                                                                                    </div>



                                                                                </div>
                                                                            </div>}

                                                                            className="References_table hide_header_ReferencesTable"
                                                                        />
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 5 */}


                            {/* Start 6 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>6</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="6">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 6</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">CAB</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                                <div className="time_line_parent w-100">


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">6.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 6.1</strong></h3>
                                                                            <h2>Schedule CAB Day</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">
                                                                                    {/* <span class=" cmn-btn1 sp_btn_p"><span>Start Date </span><i class="icon icon-edit5-ie"></i></span> */}
                                                                                    <div className="cmn_select_dv mg_slct1  slct_des23 slct_s1">
                                                                                        <Select
                                                                                            className="custom_select"
                                                                                            name="department"
                                                                                            multi={true}
                                                                                            clearable={false}
                                                                                            searchable={false}
                                                                                            clearable={false}

                                                                                            onChange={(e) => { this.props.departmentAllotedTo(this.props.index, this.props.original, e, 1) }}
                                                                                            options={optionsAP} placeholder="Department" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="mt-2">
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>CAB Day Details:</strong></div>
                                                                            <div className="Stage_Left_2"><a className="O_btn_1">Invitation Send- 01/01/2019</a></div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Date:</strong> 10/10/2018</div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Recruiter in Charge:</strong> Mike Smith</div>
                                                                        </div>
                                                                        <div className="Stage_body_01">
                                                                            <div className="Stage_Left_1"><strong>Location:</strong> ONCALL Training Faacility - Training Room</div>
                                                                        </div>
                                                                    </div>


                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">6.2</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 6.2</strong></h3>
                                                                            <h2>Applicant Responses</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="limt_flex_set_0 mt-2">
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Invitation Sent:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1">01/01/2019</div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Applicant Response:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><a className="O_btn_1">Accepted - 01/01/2019</a></div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="mb-2"><a className="limt_btns w-z w1">Resend Invite</a></div>
                                                                            <div className="my-2"><a className="limt_btns">Reschedule</a></div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">6.3</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 6.3</strong></h3>
                                                                            <h2>CAB Day Results</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div className="row d-flex justify-content-center">
                                                                        <div className="col-lg-12 Rec_center_s_1a mt-2 mb-1"><strong>CAB Day Results:</strong></div>
                                                                    </div>
                                                                    <div className="row d-flex justify-content-center">
                                                                        <div>
                                                                            <a className="O_btn_1 w-big my-1">Applicant Successful!</a><br />
                                                                            <div class="text-center"><a class="under_l_tx">View Results</a></div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">6.4</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 6.4</strong></h3>
                                                                            <h2>Employment Contract</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                    <div className="limt_flex_set_0 mt-2">
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Contract Sent:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1">01/01/2019</div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>Docusign Response:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><a className="O_btn_1">Signed - 01/01/2019</a></div>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className="mb-2"><a className="limt_btns">Resend Document</a></div>
                                                                            <div className="my-2"><a className="limt_btns">Send Reminder SMS</a></div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">6.5</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1 min-height">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Stage 6.5</strong></h3>
                                                                            <h2>Members App Onboarding</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                {/* <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span> */}
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="limt_flex_set_0 mt-2">
                                                                        <div >
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1"><strong>One time PIN:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1">
                                                                                    <div className="one_time_pass__">
                                                                                    <input type="text"/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="flex-1 pl-3">
                                                                            <div className="Stage_body_01 ">
                                                                                <div className="Stage_Left_1"><strong>Onboarding Complete:</strong></div>
                                                                            </div>
                                                                            <div className="Stage_body_01">
                                                                                <div className="Stage_Left_1 w-100">

                                                                                    <div className="Time_Orient_div_">
                                                                                        <span>Successful Login:</span>
                                                                                        <span>
                                                                                            <a className="Onboard_btn y_colo">Yes</a>
                                                                                            <a className="Onboard_btn n_colo ml-3">No</a>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div className="Time_Orient_div_ pt-2">
                                                                                        <span>Orientation Completed:</span>
                                                                                        <span className="Time_Orient_span_">
                                                                                            <div>
                                                                                                <label class="radio_F1"><input type="radio" name="aboriginal_tsi" value="1" /><span class="checkround"></span></label>
                                                                                                <span>Yes</span>
                                                                                            </div>
                                                                                            <div>
                                                                                                <label class="radio_F1"><input type="radio" name="aboriginal_tsi" value="1" /><span class="checkround"></span></label>
                                                                                                <span>NO</span>
                                                                                            </div> 
                                                                                        </span>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                            {/* <div>Complete by: Smith Roy</div> */}
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 6 */}



                            {/* Start 5 */}
                            <div className="time_l_1">
                                <div className="time_no_div">
                                    <div className="time_no"><span>7</span></div>
                                    <div className="line_h"></div>
                                </div>

                                <div className="time_d_1" >
                                    <div className="time_d_2">
                                        <Panel eventKey="7">
                                            <div className="time_txt w-100">
                                                <div className="time_d_style v4-1_">
                                                    <Panel.Heading>
                                                        <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                            <div className="timeline_h tm_subHd"><span>Stage 7</span><i className="icon icon-arrow-down"></i></div>
                                                            <div className="timeline_h tm_subShrH">Recruitment Complete</div>
                                                        </Panel.Title>
                                                    </Panel.Heading>

                                                    <div className="task_table_v4-1__">
                                                        <div className="t_t_v4-1">
                                                            <div className={"t_t_v4-1a complete_msg"}>
                                                                <div className="ci_btn">Complete</div>
                                                                <div className="ci_date">24-8-2019 </div>
                                                            </div>
                                                            <div className="t_t_v4-1b">
                                                                <a href={''}>Attachments & Notes</a>&nbsp; | &nbsp;
                                                                    <a href={''}>Create Task</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Panel.Body collapsible className="px-1 py-3">
                                               {/*  <div className="time_line_parent w-100">


                                                    <div className=" time_l_1" >
                                                        <div className="time_no_div">
                                                            <div className="time_no">7.1</div>
                                                            <div className="line_h"></div>
                                                        </div>
                                                        <div className="time_d_1">
                                                            <div className="time_d_2">

                                                                <div className="time_d_style">
                                                                    <div class="Recruit_Time_header bb-1">
                                                                        <div class="Rec_Left_s_1">
                                                                            <h3><strong>Review Answers</strong></h3>
                                                                            <h2>Phone Interview</h2>

                                                                            <div className="row">
                                                                                <div className="col-lg-6">

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="Rec_Right_s_1">
                                                                            <div>
                                                                                <div className="s-def1 s1 req_s1">
                                                                                    <Select name="participant_assessment"
                                                                                        required={true}
                                                                                        simpleValue={true}
                                                                                        searchable={false}
                                                                                        clearable={false}
                                                                                        options={optionsAP}
                                                                                        value="1"
                                                                                        placeholder="Filter by: Unread"
                                                                                        className={'custom_select'} />
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <span className="Time_line_error_msg text_R_1">Un-Completed 01/01/2025 09:42am</span>
                                                                                <span className="Time_line_error_msg text_G_1">Completed 01/01/2025 09:42am</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="d-flex">
                                                                        <div className="time_txt w-100 Rerm_time_txt">
                                                                             <div>Complete by: Smith Roy</div> 
                                                                            <ul className="Time_subTasks_Action__ ">
                                                                                <li><span className="sbTsk_li">Attachments & Notes</span></li>
                                                                                <li><span className="sbTsk_li">Create Task</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>



                                                </div> */}
                                            </Panel.Body>

                                        </Panel>


                                    </div>
                                </div>
                            </div>
                            {/* End 5 */}



                        </div>
                        {/* </PanelGroup> */}
                    </div>


                </div >


                <div className="row mt-3">
                    <button className="btn cmn-btn1" onClick={() => this.setState({ seekAnserModal: true })}>Seek Answers Modal</button>
                </div>
                <ApplicantNotesModal show={this.state.ApplicantNotes} close={() => this.setState({ ApplicantNotes: false })} />
                <ApplicantAttachmentModal show={this.state.ApplicantAttachment} close={() => this.setState({ ApplicantAttachment: false })} />
                <EditApplicantInfo show={this.state.EditApplicant} close={() => { this.setState({ EditApplicant: false }) }} />
                <SeekAnsers show={this.state.seekAnserModal} close={() => { this.setState({ seekAnserModal: false }) }} />
            </React.Fragment >
        );
    }
}

export default ApplicantInfo;


class SeekAnsers extends Component {
    render() {
        return (
            <div className={'customModal ' + (this.props.show ? ' show' : '')}>
                <div className="cstomDialog widMedium">

                    <h3 className="cstmModal_hdng1--">
                        Seek Notes
                    <span className="closeModal icon icon-close1-ie" onClick={this.props.close}></span>
                    </h3>

                    <div className="modal_body_1_4__ pd_lr_20p">

                        <div><strong>Candidate answers to your questions</strong></div>

                        <div className="ques_ans_Seeks">
                            <div>
                                <div>
                                    <span className="ics_Q approve_Q"><i class="icon icon-accept-approve1-ie"></i></span>
                                </div>
                                <div className="q_area_14__">
                                    <h5 className="pb_10p"><strong>Preferred work rights in Australia</strong></h5>
                                    <div >Iam an Australian citizen</div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <span className="ics_Q approve_Q"><i class="icon icon-accept-approve1-ie"></i></span>
                                </div>
                                <div className="q_area_14__">
                                    <h5 className="pb_10p"><strong>Recent Disability or relevant experience</strong></h5>
                                    <div >
                                        lorem ipsum is a free <br />
                                        lorem ipsum is a free text made for<br />
                                        lreom ipsum
                                            </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <span className="ics_Q approve_Q"><i class="icon icon-accept-approve1-ie"></i></span>
                                </div>
                                <div className="q_area_14__">
                                    <h5 className="pb_10p"><strong>Current First Aid level 2</strong></h5>
                                    <div >Yes</div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <span className="ics_Q disApprove_Q"><i class="icon icon-cross-icons"></i></span>
                                </div>
                                <div className="q_area_14__">
                                    <h5 className="pb_10p"><strong>Expected Annual Salary</strong></h5>
                                    <div >$100,000</div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <span className="ics_Q approve_Q"><i class="icon icon-accept-approve1-ie"></i></span>
                                </div>
                                <div className="q_area_14__">
                                    <h5 className="pb_10p"><strong>Current Police and working with children checks</strong></h5>
                                    <div >Yes</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        );
    }
}