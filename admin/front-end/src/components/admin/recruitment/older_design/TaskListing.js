import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import moment from 'moment';
import { PanelGroup, Panel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import EditCabInfo from './../EditCabInfo';
import EditNewTask from './../EditNewTask';
import CreateNewTask from './CreateNewTask';
import { checkItsNotLoggedIn, postData, handleDateChangeRaw, changeTimeZone } from 'service/common.js';
import { PAGINATION_SHOW } from 'config';
import Pagination from "service/Pagination.js";

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('recruitment/RecruitmentTaskAction/get_recruitment_task_list', Request).then((result) => {
            let filteredData = result.data;

            if(result.status){
                const res = {
                    rows: filteredData,
                    pages: (result.count),
                    total_count: (result.total_count),
                };
                resolve(res);
            }
        });

    });
};

class TaskListing extends Component {

    constructor() {
        super();
        this.state = {
            taskListing: [],
            filterVal: 'in_progress',
        }
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                taskListing: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    showModal = () => {
        this.setState({ showModal: true })
    }

    closeModal = () => {
        this.setState({ showModal: false })
    }

    CreateTaskCloseModal = () => {
        this.setState({ CreateTaskShowModal: false })
    }


    render() {

        var options = [
            { value: 'all', label: 'All' },
            { value: 'in_progress', label: 'In Progress'},
            { value: 'completed', label: 'Completed'},
            { value: 'archive', label: 'Archive'},
        ];

        var columns = [
            {Header: "Task Name:",accessor: "task_name",minWidth: 80,},
            { Header: "Stage:", accessor: "stage" },
            { Header: "Date:", accessor: "start_datetime", minWidth: 60, Cell: (props) => <span>{moment(this.props.value).format('DD/MM/YYYY')}</span>},
            { Header: "Primary Recruiter:", accessor: "primary_recruiter" },
            {   Header: "Available Slots:",
                accessor: "slots",
                width: 320,
                sortable :false,
                Cell: (props) => (
                    <div className="text-center w-100 slots_flx">
                    {props.original.slot_applicable? <React.Fragment>
                        <div><span className="slots_sp clr_green">{props.original.max_applicant}/{props.original.filled}</span></div>
                        <div><span className="counties2 green">{props.original.accepted}</span> Accepted</div>
                        <div><span className="counties2 orange">{props.original.pending}</span> Pending</div>
                        </React.Fragment>: <div>Not Applicable</div>}
                        
                    </div>

                )
            },
            {
                Header: "Status:",
                accessor: "status",
                minWidth: 100,
                Cell: (props) => (
                    <div>{(
                            props.original.status == '2' ? <span className="slots_sp clr_green">Completed</span>:
                            props.original.status == '1' ?<span className="slots_sp clr_blue">In Progress</span>:
                            props.original.status == '4' ? <span className="slots_sp clr_purple">Archived</span>: <span className="slots_sp clr_red">Cancelled</span>
                        )}
                    </div>
                )
            },
            {   Header: "",
                sortable :false,
                accessor: "action",
                minWidth: 100,
                Cell: row => (<span className="tsk_actn_spn ">
                    <a>Complete</a>&nbsp;|&nbsp;
                    <a onClick={() => this.setState({ EditNewTaskModal: true })}>Edit/View</a>&nbsp;|&nbsp;
                    <a>Archive</a>
                </span>)
            }
        ];


        return (
            <React.Fragment>

                <div className="row pt-5">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1> Tasks

                            <Link to='#'>
                                <button className="btn hdng_btn cmn-btn1 icn_btn12" onClick={() => this.setState({ CreateTaskShowModal: true })}>Create New Task <i className="icon icon-add-icons hdng_btIc"></i></button>
                            </Link>
                        </h1>
                    </div>
                </div>
                {/* row ends */}

                <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-9 col-md-8 col-sm-8 no_pd_l">
                        <div className="search_bar right srchInp_sm actionSrch_st">
                            <input type="text" className="srch-inp" placeholder="Search.." />
                            <i className="icon icon-search2-ie"></i>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-sm-4 no_pd_r">
                        <div className="filter_flx">

                            <div className="filter_fields__ cmn_select_dv gr_slctB sl_center">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}

                                />
                            </div>

                        </div>
                    </div>

                </div>
                {/* row ends */}


                <div className="row mt-5">
                    <div className="col-sm-12 no_pd_l">
                        <div className="data_table_cmn dataTab_accrdn_cmn  taskTable">
                            <ReactTable
                                PaginationComponent={Pagination}
                                columns={columns}
                                manual
                                data={this.state.taskListing}
                                pages={this.state.pages}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                filtered={this.state.filtered}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                noDataText="No Record Found"
                                minRows={3}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={this.state.taskListing.length >= PAGINATION_SHOW ? true : false}
                            />

                        </div>
                    </div>
                 </div>
                {/* row ends */}





                <div className={this.state.showModal ? 'customModal show' : 'customModal'}>
                    <div className="cstomDialog widBig">

                        <h3 className="cstmModal_hdng1--">
                            New Group Interview for Applicant
                        <span className="closeModal icon icon-close1-ie" onClick={this.closeModal}></span>
                        </h3>

                        <form>

                            <div className='tskModRow1'>
                                <div className="row ">

                                    <div className="col-md-6 ">

                                        <div className="csform-group">
                                            <label className='bg_labs'>Applicant Name:</label>
                                            <input type="text" className="csForm_control" />
                                        </div>

                                        <div className="csform-group">
                                            <label className='bg_labs'>Interview Type:</label>
                                            <div className="cmn_select_dv">
                                                <Select name="view_by_status"
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={options}
                                                    onChange={(e) => this.setState({ InterviewType: e })}
                                                    value={this.state.InterviewType}
                                                />
                                            </div>
                                        </div>

                                        <div className="csform-group">
                                            <label className='bg_labs'>Search Date Range:</label>
                                            <div className="cmn_select_dv">
                                                <Select name="view_by_status"
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={options}
                                                    onChange={(e) => this.setState({ searchVal: e })}
                                                    value={this.state.searchVal}
                                                />
                                            </div>
                                        </div>

                                    </div>

                                    <div className='col-md-6 pad_l_50Ts'>

                                        <div className='tskRgHeads'>
                                            <h2><b>Next Available Interviews</b></h2>
                                            <h3>(from: 01/01/01 - to: 02/02/01)</h3>
                                        </div>
                                        <div className='avlblty_list__'>
                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>
                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>

                                            <div className='avlblty_box'>
                                                <ul>
                                                    <li>Date: <b>01/01/01</b></li>
                                                    <li>Attendees: <b>8 of 10 (2 Spots left to fill)</b></li>
                                                    <li>Recruiter in Charge: <b>Mike Smith</b></li>
                                                </ul>
                                            </div>


                                        </div>
                                        {/* avlblty_list__ ends */}

                                    </div>




                                </div>
                                {/* row ends */}
                            </div>

                            <div className='row bor_top inviRow'>
                                <div className='col-sm-12 text-center'>
                                    <input type='button' className='btn cmn-btn1 snd_iniBtn' value='Add Applicant and Send Invitation Email' />
                                </div>
                            </div>


                        </form>


                    </div>
                </div>
                {
                    (this.state.CreateTaskShowModal) ?
                        <CreateNewTask showModal={this.state.CreateTaskShowModal} closeModal={this.CreateTaskCloseModal} />
                        : ''
                }

                <EditNewTask showModal={this.state.EditNewTaskModal} closeModal={() => { this.setState({ EditNewTaskModal: false }) }} />

                <EditCabInfo showModal={this.state.EditCabModal} closeModal={() => { this.setState({ EditCabModal: false }) }} />

            </React.Fragment>
        );
    }
}

export default TaskListing;