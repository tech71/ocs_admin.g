import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW, DATA_CONSTANTS } from 'config.js';
import { recruitmentStatus, recruitmentActionType, recruitmentLocation, tastPriorityOptions } from 'dropdown/recruitmentdropdown.js';
import { postData, checkItsNotLoggedIn, Aux, handleChangeChkboxInput, getOptionsRecruitmentStaff, handleChangeSelectDatepicker } from 'service/common.js';
import ReactTable from "react-table";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import jQuery from "jquery";
import { ToastContainer, toast } from 'react-toastify';
import Pagination from "service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js';


const WAIT_INTERVAL = DATA_CONSTANTS.FILTER_WAIT_INTERVAL;
const ENTER_KEY = DATA_CONSTANTS.FILTER_ENTER_KEY;

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('recruitment/RecruitmentDashboard/get_applicant_list', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count: result.all_count,
            };
            resolve(res);
        });
    });
};

class CreateNewTask extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        this.timer = null;
        this.validator = '';
        this.state = {
            applicantList: [],
            attachedApplicant: [],
            assigned_user: [],
            create_sub_task: false,
            tempTitle: '',
            tempDescription: '',
            tempTaskAssignTo: '',
            tempDueDate: '',
            subTaskName: '',
            subTaskDescription: '',
            subTaskDue_date: '',
            subTaskAssigned: '',
            loading: false,
            view_sub_task: false,
            action_location: '',
            action_status: '',
            filterSearch: '',
            subsTasks_ul: [],
            activeItem: null,
            action_type: false
        }
    }

    handleItemClick = (i) => {
        let { subsTasks_ul } = this.state;
        this.setState({
            activeItem: i,
            subTaskName: subsTasks_ul[i].name,
            subTaskDescription: subsTasks_ul[i].decription,
            subTaskAssigned: subsTasks_ul[i].assigned,
            subTaskDue_date: subsTasks_ul[i].due_date,
            create_sub_task: false,
            view_sub_task: true,
        })
    }
    componentDidMount() {
      
    }

    addToAttachedApplicant = (e) => {
        var tempState = [];
        if (e.target.checked) {
            this.setState({ attachedApplicant: [...this.state.attachedApplicant, e.target.value] }, () => { });
        }
        else {
            var array = [...this.state.attachedApplicant]; // make a separate copy of the array
            var index = array.indexOf(e.target.value)
            if (index !== -1) {
                array.splice(index, 1);
                this.setState({ attachedApplicant: array }, () => { });
            }
        }

    }
    /*This function is responsible to handle load filtered common all input after user stop writing*/
    handleChangeSearch = (e) => {
        clearTimeout(this.timer)
        this.setState({ filterAllTyping: e.target.value })
        this.timer = setTimeout(this.triggerChange, WAIT_INTERVAL)
    }

    /* This function is responsible to call filter api   */
    triggerChange = (e) => {
        let value = this.state.filterAllTyping;
        var requestData = { srch_box: value };
        this.setState({ filtered: requestData }, () => { /* console.log(this.state.filtered) */ });
    }


    handleSaveAction = (e) => {
        e.preventDefault();
        if (this.validator)
            this.validator.destroy();

        this.selectValidationCheck();
        this.validator = jQuery("#task_form").validate({
            rules: {
                action_name: "required",
                time_duration: "required",
                assigned_user: "required",
            },
        });

        //if(jQuery("#task_form").valid())
        if (!this.state.loading && jQuery("#task_form").valid()) {
            if (this.state.attachedApplicant.length == 0) {
                toast.dismiss();
                toast.error('Please select atleast one applicant to continue', {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
            else {
                this.setState({ loading: true }, () => {
                    postData('recruitment/RecruitmentDashboard/create_action', this.state).then((result) => {
                        if (result.status) {
                            this.setState({ loading: false });
                            toast.dismiss();
                            toast.success(<ToastUndo message={'Action created successfully.'} showType={'s'} />, {
                                //    toast.success("Action created successfully.", {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                            });
                            this.props.closeModal();
                        } else {
                            toast.dismiss();
                            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                // toast.error(result.error, {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                            });
                            this.setState({ loading: false });
                        }
                    });
                });
            }
        }
        else {
            this.validator.focusInvalid();
        }
    }

    render() {
        return (
            <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
                <div className="cstomDialog widBig">
                    <h3 className="cstmModal_hdng1--">
                        Create Task
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>
                    <form id="task_form" method="post" autoComplete="off">
                        <div className="row ">
                            <div className="col-md-6">
                                <div className="csform-group">
                                    <label className="mb-4 fs_16">Search form Participant:</label>
                                            <Select.Async
                                                name='assigned_user'
                                                loadOptions={(e) => getOptionsRecruitmentStaff(e)}
                                                clearable={false}
                                                placeholder='Search'
                                                cache={false}
                                                value={this.state.assigned_user}
                                                onChange={(e) => handleChangeSelectDatepicker(this, e, 'assigned_user')}

                                            />
                                </div>
                            </div>
                            <div className="col-md-3 pl-5">
                                <div className="csform-group ">
                                    <label className="mb-4 fs_16">Task Type:</label>
                                    <div className="cmn_select_dv">
                                        <Select name="action_type" className="custom_select"
                                            simpleValue={true}
                                            searchable={false} clearable={false}
                                            placeholder="Action Name"
                                            options={recruitmentActionType()}
                                            onChange={(e) => this.setState({ action_type: e, action_type_error: false })}
                                            value={this.state.action_type}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row ">

                            <div className="col-lg-2 col-md-3 ">
                                <div className="csform-group">
                                    <label className="mb-4 ">Task Name:</label>
                                    <input type="text" name="tast_name" className="" value="" />
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-3 pl-3">
                                <div className="csform-group">
                                    <label className="mb-4 ">Date:</label>
                                    <DatePicker
                                        autoComplete={'off'}
                                        dateFormat="DD/MM/YYYY"
                                        required={true}
                                        data-placement={'bottom'}
                                        name="PostDate"
                                        onChange={(date) => this.setState({ PostDate: date })}
                                        selected={this.state.PostDate ? moment(this.state.PostDate, 'DD-MM-YYYY') : null}
                                        className="csForm_control text-center clr2"
                                        placeholderText="DD/MM/YYYY"
                                    />

                                </div>
                            </div>
                            
                            <div className="col-lg-2 col-md-2 pl-2">
                                <div className="csform-group ">
                                    <label className="mb-4 fs_16">Priority:</label>
                                    <div className="cmn_select_dv">
                                        <Select name="action_type" className="custom_select"
                                            simpleValue={true}
                                            searchable={false} clearable={false}
                                            placeholder="Action Name"
                                            options={tastPriorityOptions()}
                                            onChange={(e) => this.setState({ action_type: e})}
                                            value={this.state.action_type}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-4 col-md-3 ">
                                <div className="csform-group">
                                    <label className="mb-4">Assigned User:</label>
                                    <div className="cmn_select_dv">
                                        <Select.Async
                                                name='assigned_user'
                                                loadOptions={(e) => getOptionsRecruitmentStaff(e)}
                                                clearable={false}
                                                placeholder='Search'
                                                cache={false}
                                                value={this.state.assigned_user}
                                                onChange={(e) => handleChangeSelectDatepicker(this, e, 'assigned_user')}
                                            />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-5">
                            <div className="col-md-12">
                                <div className="csform-group">
                                    <label className="mb-3">Relevant Task Notes:</label>
                                    <textarea className="csForm_control notesArea clr2" placeholder="[Any relevant task notes here]"></textarea>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <div className="bor_line mt-5"></div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <button className="btn cmn-btn1 creat_task_btn__" onClick={(e) => this.handleSaveAction(e)} >Create Action</button>
                            </div>
                        </div>
                        {/* row ends */}
                    </form>
                </div>
            </div>
        );
    }
}
export default CreateNewTask;