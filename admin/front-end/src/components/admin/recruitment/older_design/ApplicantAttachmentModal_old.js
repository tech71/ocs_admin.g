import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

class ApplicantAttachmentModal extends Component {

    constructor() {
        super();
        this.state = {
            documentsSelected: '',

        }
    }


    render() {
        var disabeSelOptions = [
            { value: '1', label: 'Current Documents' },
            { value: '2', label: 'Archived Documents' }
        ];

        return (
            <div className={'customModal ' + (this.props.show ? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Attachments
                            <span className="closeModal icon icon-close1-ie" onClick={this.props.close}></span>
                    </h3>

                    <form>

                        <div className='row pd_lf_15 mr_tb_20'>
                            <div className='col-sm-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Applicant Documents</strong> </label>
                            </div>
                            <div className='col-md-10 col-md-offset-1'>

                                <div className='row '>

                                    <div className='col-sm-8'>
                                        <div className='row bor_bot1 mr_tb_20 '></div>

                                        <div className='row attch_row'>
                                            <div className='col-sm-3 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Resume</strong></h5>
                                                    <i className='icon icon-notes1-ie'></i>
                                                    <p>john_smith_resume_01-01-19.pdf</p>
                                                </div>
                                            </div>
                                            <div className='col-sm-3 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Cover letter</strong></h5>
                                                    <i className='icon icon-notes1-ie'></i>
                                                    <p>john_smith_Cover_letter_01-01-19.pdf</p>
                                                </div>
                                            </div>
                                            <div className='col-sm-3 col-xs-6'>
                                                <div className='attach_item'>
                                                    <h5><strong>Qualification</strong></h5>
                                                    <i className='icon icon-notes1-ie'></i>
                                                    <p>john_smith_Qualification_01-01-19.pdf</p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div className='col-sm-4'>
                                        <div className="csform-group">

                                            <div className="cmn_select_dv " >
                                                <Select name="view_by_status "
                                                    required={true} simpleValue={true}
                                                    searchable={false} Clearable={false}
                                                    placeholder="Select Type"
                                                    options={disabeSelOptions}
                                                    onChange={(e) => this.setState({ documentsSelected: e })}
                                                    value={this.state.documentsSelected}
                                                />
                                            </div>
                                        </div>
                                    </div>


                                </div>





                            </div>
                        </div>

                        <div className='row pd_lf_15'>

                            <div className='col-sm-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Communication</strong> </label>
                            </div>

                            <div className='col-md-10 col-md-offset-1'>

                                <div className='row'>
                                    <div className='col-md-8'>
                                        <div className='row bor_bot1 mr_tb_20 '></div>
                                        <p style={{ minHeight: '120px' }}>No Files Avaiable</p>

                                    </div>
                                </div>

                            </div>



                        </div>


                        <div className='row pd_lf_15'>


                            <div className='col-md-10 col-md-offset-1'>

                                <div className='row bor_bot1 mr_tb_20 '></div>
                                <div className='row'>
                                    <div className='col-md-6'>
                                        <button className='cmn-btn1 btn btn-block atchd_btn1__'>Save a New document to upload</button>
                                        <button className='cmn-btn1 btn btn-block atchd_btn1__'>Archive Selected Documents</button>
                                        <button className='cmn-btn1 btn btn-block atchd_btn1__'>Download Selected Documents</button>
                                    </div>

                                    <div className='col-md-6'>

                                       <label className='bg_labs2 mr_b_20'><strong>New Document Information</strong> </label>
                                      
                                            <div class="csform-group">
                                                <label>Doc Title:</label>
                                                <input type="text" class="csForm_control" placeholder="" name="title" />
                                            </div>

                                            <div class="csform-group">
                                                <label>Doc Category:</label>
                                                <input type="text" class="csForm_control" placeholder="" name="category" />
                                            </div>

                                            <div className='csform-group'>

                                                <div className='fileAtch_box__'>
                                                    <label>File Name:</label>
                                                    <div className=' d-flex align-items-center'>
                                                        <i className='icon icon-file-icons fle_ic'></i>
                                                        <div className='fle_nme__'>John_smith_phone_interview_01_01_01.mp3</div>
                                                        <i className='icon icon-close3-ie fle_close__'></i>
                                                    </div>
                                                </div>

                                            </div>
                                       
                                            <button className='cmn-btn1 btn btn-block atchd_btn1__'>Upload File</button>
                                    </div>
                                </div>


                            </div>



                        </div>






                        <div class="row trnMod_Foot__ disFoot1__">
                            <div class="col-sm-12 no-pad text-right">
                                <button type="submit" class="btn cmn-btn1 create_quesBtn">Apply Changes</button>
                            </div>
                        </div>

                    </form>


                </div>
            </div>

        );
    }
}

export default ApplicantAttachmentModal;

