import jQuery from "jquery";
import moment from 'moment';
import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import CKEditor from "react-ckeditor-component";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Link, Redirect } from 'react-router-dom';
import Select from 'react-select-plus';
import Slider from "react-slick";
import { toast } from 'react-toastify';
import { ToastUndo } from 'service/ToastUndo.js';
import { handleChangeChkboxInput, handleShareholderNameChange, postData, handleChangeSelectDatepicker,archiveALL,handleDateChangeRaw } from '../../../service/common.js';
import ReactGoogleAutocomplete from './../externl_component/ReactGoogleAutocomplete';
import AddDocuments from './AddDocuments';
import QuickPreviewModal from './QuickPreviewModal';
import { connect } from 'react-redux';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class CreateJob extends Component {

    constructor() {
        super();
        this.state = {

            Partnersettings: {
                dots: false,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 1,
            },
            additionQuesAr: [
                { id: 1, name: 'Ques1', inpValue: '' },
                { id: 2, name: 'Ques2', inpValue: '' },
                { id: 3, name: 'Ques3', inpValue: '' },
                { id: 4, name: 'Ques4', inpValue: '' }
            ],
            quickModal: false,
            AddDocumentsModal: false,
            magageDocModal: false,
            ac1: true,
            validate: false,
            job_type: '',
            job_template: [],
            job_category: '',
            job_sub_category: '',
            job_position: '',
            job_employment_type: '',
            job_salary_range: '',
            all_documents: [],
            //check_documents: false,
            allow_close_docs_form: false,
            publish_to: [],
            activeTemplate: 1,
            is_salary_publish: false,
            save_success: false,
            view: 'operate',   // used to identify pop is open in create page or in listing page
            job_operation: 'create', // this page is used in 3 ways, create/Edit and Duplicate
            document_name: '',
            is_editable: false,  //by deafaut false means all field are editable in add and in edit mode as per Jira
            editor_read_only:true,
            job_status:0,
        }
        this.page_title = 'Create Job';
        this.baseState = this.state

    }

    selectTemplate=(obj,i)=> {
        this.setState({
            activeTemplate: i
        })
    }

    AddQuesInput = () => {
        const { additionQuesAr } = this.state;
        const numRows = additionQuesAr.length;
        const inpQuesObject = { id: numRows + 1, name: 'Ques' + (numRows + 1), inpValue: '' };
        additionQuesAr.push(inpQuesObject);
        this.setState({
            additionQuesAr
        })
    }

    changeQuesText = (e) => {
        const { id, value } = e.target;
        let { additionQuesAr } = this.state;
        const targetIndex = additionQuesAr.findIndex(AdQues => {
            return AdQues.id == id;
        });

        if (targetIndex !== -1) {
            additionQuesAr[targetIndex].inpValue = value;
            this.setState({ additionQuesAr });
        }
    }

    submitjobs = (e) => {
        if (e) e.preventDefault()
        var validator = jQuery("#form_create_job").validate({
            ignore: [],
            focusInvalid: true,
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    var firstInvalidElement = jQuery(validator.errorList[0].element);
                    jQuery('html,body').scrollTop(firstInvalidElement.offset().top);
                }
            }
        });

        var is_validate = this.custom_validation();
        //if(jQuery("#form_create_job").valid() && is_validate)
        if (!this.state.loading && jQuery("#form_create_job").valid() && is_validate) {
            this.setState({ loading: true }, () => {
                postData('recruitment/recruitment_job/save_job', JSON.stringify(this.state)).then((result) => {
                    if (result.status) {
                        this.setState({ loading: false });
                        toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ save_success: true });
                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.setState({ loading: false });
                    }
                });
            });
        }
        else {
            this.setState({ validate: true });
            validator.focusInvalid();
        }
    }

    handleOnchangeAndvalidateSelect = (e, nameAttr) => {
        this.setState({ [nameAttr]: e }, () => {
            if (this.state.validate) {
                jQuery("#form_create_job").validate().element('input[name="' + nameAttr + '"]');
            }

            if (nameAttr == 'category') {
                this.getSeekSubCatgory();
                this.setState({sub_category:''})
            }
            else if (nameAttr == 'sub_category') {
                this.getSeekQuestionByCatgorySubcat();
            }
        });
    }

    custom_validation = () => {
        var return_var = true;
        var state = {};

        if (this.state['job_content'] == undefined || this.state['job_content'] == '') {
            state['job_content' + '_error'] = true;
            this.setState(state);
            return_var = false;
        }
        return return_var;
    }

    onChangeEditor = (evt) => {
        var newContent = evt.editor.getData();
        this.setState({
            job_content: newContent, job_content_error: false
        })
    }

    errorShowInTooltip = ($key, msg) => {
        return (this.state[$key + '_error']) ? <div className={'tooltip custom-tooltip fade top in' + ((this.state[$key + '_error']) ? ' select-validation-error' : '')} role="tooltip">
            <div className="tooltip-arrow"></div><div className="tooltip-inner">{msg}.</div></div> : '';
    }

    //
    componentDidMount() {
        this.getJobMasterList();
        this.getRequiredJobDocuments();
        this.getJobChannels();

        if (this.props.props.match.params.jobType == 'D')
            this.page_title = 'Duplicate Job';
        else if (this.props.props.match.params.jobType == 'E')
            this.page_title = 'Edit Job';

        document.title = this.page_title;

        if (this.props.props.match.params.jobId != '' && typeof this.props.props.match.params.jobId != 'undefined') {
            if (this.props.props.match.params.jobType == 'E' || this.props.props.match.params.jobType == 'D') {
                this.setState({ job_operation: this.props.props.match.params.jobType, job_id: this.props.props.match.params.jobId }, () => {
                    this.getJobDetail(this.props.props.match.params.jobId);
                })
            } else {
                this.setState({ save_success: true });
                //if request other then duplicate and edit page will move to job listing
            }
        }
    }

    getJobDetail = (jobId) => {
        postData('recruitment/recruitment_job/get_job_detail', { jobId: jobId }).then((result) => {
            if (result.status) {
                this.setState({
                    type: result.data.parentState.type,
                    category: result.data.parentState.category,
                    sub_category: result.data.parentState.sub_category,
                    position: result.data.parentState.position,
                    employment_type: result.data.parentState.employment_type,
                    salary_range: result.data.parentState.salary_range,
                    is_salary_publish: result.data.parentState.is_salary_publish,
                    phone: result.data.parentState.phone,
                    email: result.data.parentState.email,
                    website: result.data.parentState.website,
                    job_location: result.data.parentState.complete_address.formatted_address,
                    activeTemplate: result.data.parentState.activeTemplate,
                    job_content: result.data.parentState.job_content,
                    //check_documents: true,
                    all_documents: result.data.parentState.all_documents,
                    job_sub_category: result.data.parentState.job_sub_category,
                    publish_to: result.data.parentState.publish_to,
                    complete_address: result.data.parentState.google_response,
                    job_location_id: result.data.parentState.job_location_id,
                    from_date: result.data.parentState.from_date,
                    to_date: result.data.parentState.to_date,
                    is_recurring: result.data.parentState.is_recurring,
                    job_status: result.data.parentState.job_status,
                }, () => {
                    if (this.state.job_status == 3 || this.state.job_status == 5 || this.state.job_status == 2) {
                        this.setState({ is_editable: true });
                        if(this.state.job_status == 2)
                        this.setState({ editor_read_only: false });
                    }
                    if (this.props.props.match.params.jobType == 'D')
                        this.setState({ is_editable: false,editor_read_only:true });

                    this.getSeekSubCatgory();
                });
            } else {
                this.setState({ save_success: true });
                //if requested job id is not exist in DB then page will move to job listing
            }
        });
    }

    getJobMasterList = () => {
        postData('recruitment/recruitment_job/get_job_master_list', {}).then((result) => {
            if (result.status) {
                this.setState({
                    job_category: result.data.category,
                    job_position: result.data.position,
                    job_employment_type: result.data.employmentType,
                    job_salary_range: result.data.salaryRange,
                    job_type: result.data.jobType,
                    job_template: result.data.jobTemplate,
                }, () => { });
            }
        });
    }

    getRequiredJobDocuments = () => {
        postData('recruitment/recruitment_job/get_job_all_documents', {}).then((result) => {
            if (result.status) {
                this.setState({
                    all_documents: result.data,
                }, () => { });
            }
        });
    }

    getSeekSubCatgory = () => {
        postData('recruitment/recruitment_job/get_subcategory_from_seek', { category_id: this.state.category }).then((result) => {
            if (result.status) {
                this.setState({
                    job_sub_category: result.data,
                }, () => {

                });
            }
        });
    }

    getSeekQuestionByCatgorySubcat = () => {
        postData('recruitment/recruitment_job/get_seek_ques_by_cat_subcat', { category_id: this.state.category, sub_category: this.state.sub_category }).then((result) => {
            if (result.status) {
                this.setState({
                    questionByCatAndSubcat: result.data,
                }, () => {
                    var temp_state = {};
                    var previous_state = this.state.publish_to;
                    this.state.publish_to.map((value, idx) => {
                        previous_state[idx]['question'] = this.state.questionByCatAndSubcat;
                    })
                    this.setState(previous_state, () => { });
                });
            }
        });
    }

    getJobChannels = () => {
        postData('recruitment/recruitment_job/get_job_channel_details', {}).then((result) => {
            if (result.status) {
                this.setState({
                    publish_to: result.data,

                }, () => { });
            }
        });
    }

    enableAndSelectDocs = (value, index) => { 
        var temp_state = {};
        var old_state = this.state.all_documents;

        if (old_state[index]['clickable']){
            old_state[index]['clickable'] = false;
            old_state[index]['optional'] = false;
           old_state[index]['mandatory'] = false;
            
        }else{
            old_state[index]['mandatory'] = true;
            old_state[index]['clickable'] = true;
        }

        temp_state['all_documents'] = old_state;
        this.setState(temp_state, () => { });
    }

    chooseOptionalMandatory = (value, index, type) => { 
        var temp_state = {};
        var old_state = this.state.all_documents;
        if (type == 'optional') {
            old_state[index]['mandatory'] = false;
            old_state[index]['optional'] = true;
        }
        else {
            old_state[index]['mandatory'] = true;
            old_state[index]['optional'] = false;
        }

        temp_state['all_documents'] = old_state;
        this.setState(temp_state, () => { /* console.log(temp_state) */ });
    }

    selectDocument = (value, index, operation) => { 
        var temp_state = {};
        var old_state = this.state.all_documents;

        if (operation == 'add')
            old_state[index]['selected'] = true;
        else
            old_state[index]['selected'] = false;

        old_state[index]['clickable'] = false;

        temp_state['all_documents'] = old_state;
        //this.setState({check_documents:true});
        this.setState(temp_state, () => { });
    }

    closeAndSaveDocuments = () => {
        var temp_count = 0;
        var docs_length = this.state.all_documents.length;
        this.state.all_documents.map((value, idx) => {
            if (!value.selected)
                temp_count = temp_count + 1;
        })

        if (temp_count == docs_length) {
            this.setState({ check_documents: false, allow_close_docs_form: true });
        }
        else {
            this.setState({ check_documents: true, magageDocModal: false })
        }
    }

    editQuestion = (mainIndex, otherIndex, textVal) => {
        var temp_state = {};
        var old_pub_state = JSON.stringify(this.state.publish_to);
        old_pub_state = JSON.parse(old_pub_state);

        if (old_pub_state[mainIndex]['question'][otherIndex]['question_edit']) {
            old_pub_state[mainIndex]['question'][otherIndex]['question_edit'] = false;
            old_pub_state[mainIndex]['question'][otherIndex]['editable_class'] = '';
            old_pub_state[mainIndex]['question'][otherIndex]['btn_txt'] = 'Edit';
        }
        else {
            old_pub_state[mainIndex]['question'][otherIndex]['question_edit'] = true;
            old_pub_state[mainIndex]['question'][otherIndex]['editable_class'] = 'editable_class';
            old_pub_state[mainIndex]['question'][otherIndex]['btn_txt'] = 'Save';

            //if (textVal) { old_pub_state[mainIndex]['question'][otherIndex]['question'] = textVal.target.value; }
        }
        temp_state['publish_to'] = old_pub_state;
        this.setState(temp_state, () => { console.log(this.state.publish_to) });
    }

    handleOnChangeQuestion = (mainIndex, otherIndex, textVal) => {
        console.log(mainIndex, otherIndex, textVal);
        var temp_state = {};
        var old_pub_state = this.state.publish_to;

        if (old_pub_state[mainIndex]['question'][otherIndex]['question_edit']) { console.log('here');
            if (textVal) { old_pub_state[mainIndex]['question'][otherIndex]['question'] = textVal.target.value; }
        }
        temp_state['publish_to'] = old_pub_state;
        this.setState(temp_state, () => {  console.log(temp_state)  });
    }

    removeQuestion = (mainIndex, otherIndex) => {
        var temp_state = {};
        var old_pub_state = this.state.publish_to;

        if (old_pub_state[mainIndex]['question'][otherIndex]) {
            old_pub_state[mainIndex]['question'] = old_pub_state[mainIndex]['question'].filter((s, sidx) => otherIndex !== sidx);
        }

        temp_state['publish_to'] = old_pub_state;
        this.setState(temp_state, () => { /* console.log(temp_state) */ });
    }

    changeMyVariable = (myVariable) => {
        return 1;
    }

    resetAllFields = () => {
        this.setState({
            ...this.state,
            job_type: '',
            job_location: '',
            phone: '',
            email: '',
            website: '',
            all_documents: [],
            //check_documents: false,
            is_salary_publish: false,
            job_template: [],
            activeTemplate: 1,
            job_content: '',
            type: '',
            category: '',
            sub_category: '',
            position: '',
            employment_type: '',
            salary_range: '',
            publish_to: [],
            from_date:'',
            to_date:'',
            is_recurring:'',
        })
        this.getJobMasterList();
        this.getRequiredJobDocuments();
        this.getJobChannels();
    }

    setstateOfDraftAndPostJob = (draftState, postState) => {
        this.setState({ save_as_draft: draftState, post_job: postState, job_operation: this.state.job_operation },
            (e) => this.submitjobs(e))
    }

    addDocuments = (e) => {
        e.preventDefault();
        jQuery('#AddDocsForm').validate({});
        if (jQuery('#AddDocsForm').valid()) {
            this.setState({ loading: true }, () => {
                postData('recruitment/recruitment_job/save_job_required_documents', { document_name: this.state.document_name }).then((result) => {
                    if (result.status) {
                        toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.state.all_documents.push(result.data);
                    }
                    else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                    this.setState({ loading: false })
                });
            });
        }
    }

    handleDocumentOnChange = (e) => {
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    handleRemoveTodate = (e) => {

       
    var state = {};
    state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
    this.setState(state,()=>{
        if (!this.state.is_recurring)
            this.setState({ to_date: '' });
    });

        
    }

    updateJobStatus = (e, statusNeedToUpdate, msg) => {
        if (!this.state.loading) {
            this.setState({ loading: true }, () => {
                archiveALL({ status: statusNeedToUpdate, job_id: this.props.props.match.params.jobId }, 'Are you sure you want to ' + msg, 'recruitment/recruitment_job/update_job_status').then((result) => {
                    if (result.status) {
                        this.setState({ save_success: true });
                    }
                    this.setState({ loading: false });
                })
            })
        }
    }

    render() {
        const myVariable = this.changeMyVariable();
        return (
            <React.Fragment>
                {(this.state.save_success) ? <Redirect to={{ pathname: '/admin/recruitment/job_opening/jobs' }} /> : ''}
                <BlockUi tag="div" blocking={this.state.loading}>
                    <form id="form_create_job" method="post" autoComplete="off">
                        <div className="row">
                            <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                                <Link to='/admin/recruitment/job_opening/jobs'>
                                    <span className="icon icon-back1-ie"></span>
                                </Link>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12 col-md-12 main_heading_cmn-">
                                <h1>{this.props.showPageTitle}</h1>
                            </div>
                        </div>

                        <div className='row creaJobRow1__ bor_bot1 border-color-black'>
                            <div className="col-lg-12 px-0">
                                <h4 className="mt-3 pb-2"><strong>Job Details</strong></h4>
                                <div className="bb-1 border-color-black mb-2"></div>
                            </div>

                            <div className="col-lg-12 col-md-12 col-sm-12 no-pad">
                                <div className="row">

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Job Type:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Type"
                                                    options={this.state.job_type}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "type")}
                                                    value={this.state.type || ''}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"type"} required={true} value={this.state.type || ''} readOnly />}
                                                />
                                            </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Job Category:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Category"
                                                    options={this.state.job_category}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "category")}
                                                    value={this.state.category}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"category"} required={true} value={this.state.category || ''} readOnly />}
                                                />
                                            </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Job Sub Category:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Category"
                                                    options={this.state.job_sub_category}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "sub_category")}
                                                    value={this.state.sub_category}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"sub_category"} required={true} value={this.state.sub_category || ''} readOnly />}
                                                />
                                            </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Job Position:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Category"
                                                    options={this.state.job_position}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "position")}
                                                    value={this.state.position}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"position"} required={true} value={this.state.position || ''} readOnly />}
                                                />
                                            </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Employment Type:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Type"
                                                    options={this.state.job_employment_type}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "employment_type")}
                                                    value={this.state.employment_type}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"employment_type"} required={true} value={this.state.employment_type || ''} readOnly />}
                                                />
                                            </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-md-2">
                                        <div className="csform-group ">
                                            <label className="mb-m-4 fs_16">Salary Range:</label>
                                            <span className="required">
                                            <div className="cmn_select_dv">
                                                <Select className="custom_select default_validation"
                                                    simpleValue={true}
                                                    searchable={false} clearable={false}
                                                    placeholder="Select Range"
                                                    options={this.state.job_salary_range}
                                                    onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "salary_range")}
                                                    value={this.state.salary_range}
                                                    disabled={this.state.is_editable}
                                                    inputRenderer={() => <input type="text" className="define_input" name={"salary_range"} required={true} value={this.state.salary_range || ''} readOnly />
                                                    }
                                                />
                                            </div>
                                            </span>
                                            <label className="customChecks publ_sal">
                                                <input type="checkbox" name="is_salary_publish" onChange={(e) => this.setState({ is_salary_publish: e.target.checked })} checked={this.state.is_salary_publish} disabled={this.state.is_editable} />
                                                <div className="chkLabs fnt_sm">Publish Salary</div>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="col-lg-12 col-md-12 col-sm-12 no-pad">
                                <div className="row d-flex flex-wrap">

                                    <div className="col-lg-4 col-md-8">
                                        <div className="csform-group">
                                            <label className="mb-m-4">Job Advertisement Timeframe:</label>
                                            <div className="timeFrame_bx">

                                                <div className="dts_bxie">
                                                    <div className="csform-group frm_grp_flx datie_wid">
                                                        <label className="wid_50">From:</label>
                                                        <span className="required">
                                                        <DatePicker
                                                            showYearDropdown
                                                            scrollableYearDropdown
                                                            yearDropdownItemNumber={110}
                                                            dateFormat="DD/MM/YYYY"
                                                            required={true}
                                                            data-placement={'bottom'}
                                                            maxDate={(this.state.to_date) ? moment(this.state.to_date) : moment().add(60, 'days')}
                                                            minDate={moment()}
                                                            name={"from_date"}
                                                            onChange={(e) => handleChangeSelectDatepicker(this, e, 'from_date')}
                                                            selected={this.state.from_date && this.state.from_date != '0000/00/00' ? moment(this.state.from_date) : null}
                                                            className="csForm_control text-center bl_bor"
                                                            placeholderText="DD/MM/YYYY"
                                                            disabled={this.state.is_editable}
                                                            onChangeRaw={handleDateChangeRaw}
                                                            autoComplete={'off'}
                                                        />
                                                        </span>
                                                    </div>

                                                    <div className="csform-group frm_grp_flx datie_wid">
                                                        <label className="wid_50" ss={this.state.to_date}>To:</label>
                                                        <span className="required">
                                                        <DatePicker
                                                            showYearDropdown
                                                            scrollableYearDropdown
                                                            yearDropdownItemNumber={110}
                                                            dateFormat="DD/MM/YYYY"
                                                            required={true}
                                                            data-placement={'bottom'}
                                                            minDate={(this.state.from_date && this.state.from_date != '0000/00/00') ? moment(this.state.from_date) : moment()}
                                                            popperPlacement="bottom-end"
                                                            name={"to_date"}
                                                            onChange={(e) => handleChangeSelectDatepicker(this, e, "to_date")}
                                                            selected={this.state.to_date && this.state.to_date != '0000/00/00' ? moment(this.state.to_date) : null}
                                                            className="csForm_control text-center bl_bor"
                                                            placeholderText="DD/MM/YYYY"
                                                            disabled={((this.state.is_recurring) || (this.state.is_editable)) ? true : false}
                                                            onChangeRaw={handleDateChangeRaw}
                                                            autoComplete={'off'}
                                                        />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <label className="customChecks publ_sal mr_l_45">
                                                    <input type="checkbox" value={this.state.is_recurring || ''} disabled={this.state.is_editable} checked={this.state.is_recurring ? true : false} name="is_recurring" onChange={(e) => {
                                                        this.handleRemoveTodate(e) ;
                                                    }} />
                                                    <div className="chkLabs fnt_sm">Set as Recurring</div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-2 col-md-4">
                                        <div className=" cmn_select_dv srch_select12 loc_sel vldtn_slct">
                                            <div className="csform-group">
                                                <label>Job Location:</label>
                                                <span className="required">
                                                <div className="React_Google_auto">
                                                <ReactGoogleAutocomplete className="add_input mb-1"
                                                    required={true}
                                                    data-msg-required="Add address"
                                                    name={"job_location"}
                                                    onPlaceSelected={(place) => {
                                                        this.setState({ job_location: place.formatted_address, complete_address: place })
                                                    }}
                                                    types={['address']}
                                                    returntype={'array'}
                                                    className="csForm_control bl_bor"
                                                    value={this.state.job_location || ''}
                                                    onChange={(evt) => this.setState({ job_location: evt.target.value })}
                                                    componentRestrictions={{ country: "au" }}
                                                    disabled={this.state.is_editable}
                                                />
                                                </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-2 col-md-4">
                                        <div className="csform-group">
                                            <label>Phone:</label>
                                            <span className="required">
                                            <input type="text" className="csForm_control bl_bor" name="phone" value={this.state.phone || ''} required onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-phonenumber maxLength='18' readOnly={this.state.is_editable} />
                                                </span>
                                        </div>
                                    </div>

                                    <div className="col-lg-2 col-md-4">
                                        <div className="csform-group">
                                            <label>Email:</label>
                                            <span className="required">
                                            <input type="text" className="csForm_control bl_bor" name="email" value={this.state.email || ''} required onChange={(e) => handleChangeChkboxInput(this, e)} readOnly={this.state.is_editable} />
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-lg-2 col-md-4">
                                        <div className="csform-group">
                                            <label>Website:</label>
                                            <span className="required">
                                            <input type="text" className="csForm_control bl_bor" name="website" value={this.state.website || ''} required onChange={(e) => handleChangeChkboxInput(this, e)} data-rule-valid_website readOnly={this.state.is_editable} />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-lg-12 col-md-12 col-sm-12 no-pad mt-3">
                                
                                <div className="row">
                                    <div className="col-lg-12">
                                        <h4 className="mt-3 pb-2"> <strong>Select Required Documents:</strong>  </h4>
                               
                                        <span className="required"><label htmlFor="dcs[]" className="error CheckieError" style={{display:'block'}}></label></span>
                                        <div className="bb-1 border-color-black mb-2"></div>

                                    </div>
                                  

                                    {(!this.state.is_editable) ?
                                        <div className="col-lg-6 text-right">
                                            {/* <a onClick={() => this.setState({ magageDocModal: true })} className="btn cmn-btn2 mr-2">Manage Documents</a> */}
                                            <a onClick={() => this.setState({ AddDocumentsModal: true })} className="btn cmn-btn1" style={{ display: 'none' }}>Add Documents</a>
                                        </div> : ''}


                                    <div className="col-lg-12">
                                       
                                        <div className="Manage_ul_">
                                            {this.state.all_documents.map((value, idx) => (
                                                <div className="Manage_li_" key={idx}>
                                                    <div className="Manage_li_a1_"><label className="cUStom_check w-100"><input  onClick={(e) => !this.state.is_editable && this.enableAndSelectDocs(value, idx)} disabled={this.state.is_editable} type="checkbox" name={'dcs[]'} required checked={(value.clickable)?true:false} readOnly/><small></small>
                                                    <div className="Manage_li_a1_" ><span>{value.label}</span></div>
                                                    </label></div>
                                                   
                                                    <div className="Manage_li_a2_">

                                                        <div className="Manage_li_a2_">

                                                            <a className={(value.optional) ? 'Req_btn_out_1 R_bt_co_blue active_selected' : 'Req_btn_out_1 R_bt_co_blue'} onClick={(e) => !this.state.is_editable && value.clickable && this.chooseOptionalMandatory(value, idx, 'optional')}>Optional</a>

                                                            <a className={(value.mandatory) ? 'Req_btn_out_1 R_bt_co_ active_selected' : 'Req_btn_out_1 R_bt_co_'} onClick={(e) => !this.state.is_editable && value.clickable && this.chooseOptionalMandatory(value, idx, 'mandatory')}>Mandatory</a>
                                                            {/* <i className="icon icon-add2-ie Man_btn_3a" onClick={(e)=> this.selectDocument(value,idx,'add')}></i>  */}

                                                        </div>

                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-12">
                                        <div className="row">

                                            {this.state.all_documents.map((value, idx) => (
                                                (value.selected) ?
                                                    <div className="Req_list_1r col-lg-3" key={idx + 2}>
                                                        <div className="Req_list_1r_1a">{value.label}</div>
                                                        <div className="Req_list_1r_1b">
                                                            {(value.optional) ? <a className="Req_btn_out_1 R_bt_co_blue">Optional</a> : ''}
                                                            {(value.mandatory) ? <a className="Req_btn_out_1 R_bt_co_">Mandatory</a> : ''}
                                                        </div>
                                                    </div> : ''
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="clearfix"></div>
                        </div>

                        <div className='row creaJobRow1__ bor_bot1  border-color-black pt-5'>
                            <div className="col-lg-6 col-md-12 no_pd_l border-color-black">
                                <div className="pd-r-30">
                                    <div className="csform-group">
                                        <h4 ><strong>Job Style Template:</strong></h4>
                                        <div className='resume_slider mt-1'>
                                            <Slider {...this.state.Partnersettings}>

                                                {
                                                    this.state.job_template.map((template, i) => {
                                                        return (
                                                            <div key={i}>
                                                                <div className={this.state.activeTemplate == template.id ? 'template_slide selected' : 'template_slide'} onClick={()=>!this.state.is_editable && this.selectTemplate(this, template.id)}  >
                                                                    <div><img src={'/assets/images/' + template.thumb} alt={template.id + this.state.activeTemplate} /></div>
                                                                    <h5>{template.name}</h5>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }

                                            </Slider>
                                        </div>
                                    </div>
                                    <div className="csform-group">
                                        <h4 className="mt-3"><strong>Job Text Layout:</strong></h4>
                                        
                                        <div className="d-flex w-100 pb-4 justify-content-center">
                                            {this.errorShowInTooltip('job_content', 'This field is required.')}
                                        </div>
                                       
                                        <span className="required">
                                        <div className='cstmEditor bigHg mt-2 cmnEditorScrollBar__ createJobEditor Hide_tolles'> 
                                        {
                                            (this.state.editor_read_only)?
                                            (<CKEditor scriptUrl={'https://cdn.ckeditor.com/4.6.2/full/ckeditor.js'} activeClass="p10" content={this.state.job_content} events={{ "change": this.onChangeEditor }} required config={{}} />):
                                            (ReactHtmlParser(this.state.job_content))
                                        }
                                        </div>
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-12 no_pd_r bor-l border-color-black">
                                <div className="pd-l-30">
                                    <h4><strong>Publish To:</strong></h4>
                                    <div className="cmnDivScrollBar__ prList_sclBar  publish_ques">
                                        {this.state.publish_to.map((value, idx) => (
                                            <div className="ques_box" key={idx}>
                                                <div className="row mb-5">
                                                    <div className="col-md-12 d-flex align-items-center">
                                                        <span>{idx + 1}.</span>

                                                        <span className="row pl-2 w-100">
                                                            <div className="col-lg-3 col-md-3 pub_filCol">
                                                                <div className="csform-group">
                                                                    <label className="mb-m-4">Channel:</label>
                                                                    <Select className="default_validation"
                                                                        cache={false}
                                                                        searchable={false}
                                                                        name="form-field-name"
                                                                        clearable={false}
                                                                        value={value.drp_dwn_val}
                                                                        simpleValue={true}
                                                                        onChange={(e) => this.handleOnchangeAndvalidateSelect(e, "chanel")}
                                                                        required={true}
                                                                        options={[value.drp_dwn] || ''}
                                                                        inputRenderer={() => <input type="text" className="define_input" name={"chanel"} required={true} value={value.channel_name} onChange={() => { }} />}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className={"accordion_c acc_des1 " + ((value.question_tab) ? 'in' : '')} >

                                                    <div className="accor_hdng" onClick={(e) => handleShareholderNameChange(this, 'publish_to', idx, 'question_tab', !value.question_tab)}>
                                                        <div>Additional Questions</div>
                                                        <i className="icon icon-arrow-right"></i>
                                                    </div>

                                                    <div className="accor_body">
                                                        <p>Current question for application posted on {value.channel_name}:</p>

                                                        {
                                                            (value.question) ? value.question.map((val, index) => (
                                                                <div className="Addtional_li_1a" key={index}>
                                                                    <ul>
                                                                        <span>{index + 1}</span>
                                                                        {/*<li key={index + 2}>
                                                                            <div>
                                                                                <div>
                                                                                    <textarea className={(val.question_edit) ? val.editable_class : ''} onChange={(e) => this.handleOnChangeQuestion(idx, index, e)} disabled={(val.question_edit) && (val.question_edit) != '' ? false : true}>{val.question}</textarea></div>
                                                                                <div className="d-inline-flex align-items-center">
                                                                                    {(value.question.length > 1 && !this.state.is_editable) ? <a onClick={(e) => this.removeQuestion(idx, index)} className="under_l_tx pd-r-10 mr-3">Remove</a> : ''}

                                                                                    {(!this.state.is_editable) ?
                                                                                        <a onClick={(e) => this.editQuestion(idx, index)}><span className="btn cmn-btn1">{val.btn_txt}</span></a> : ''}
                                                                                </div>
                                                                            </div>
                                                                        </li>*/}

                                                                        

                                                                         <li key={index + 2}>
                                                                            <div>
                                                                                {(val.question_edit) && (val.question_edit) != '' ? <React.Fragment>
                                                                                    <div><textarea
                                                                                        className={(val.question_edit) ? val.editable_class : ''}
                                                                                        onChange={(e) => this.handleOnChangeQuestion(idx, index, e)}
                                                                                        disabled={(val.question_edit) && (val.question_edit) != '' ? false : true}>{val.question}</textarea>
                                                                                    </div>
                                                                                    <div className="d-inline-flex align-items-center">
                                                                                        {(value.question.length > 1 && !this.state.is_editable) ? <a onClick={(e) => this.removeQuestion(idx, index)} className="under_l_tx pd-r-10 mr-3">Remove</a> : ''}

                                                                                        {(!this.state.is_editable) ?
                                                                                            <a onClick={(e) => this.editQuestion(idx, index)}><span className="btn cmn-btn1">{val.btn_txt}</span></a> : ''}
                                                                                    </div></React.Fragment> :
                                                                                    <React.Fragment>
                                                                                        <div>{val.question}</div>
                                                                                        {(value.question.length > 1 && !this.state.is_editable) ? <a onClick={(e) => this.removeQuestion(idx, index)} className="under_l_tx pd-r-10 mr-3">Remove</a> : ''}
                                                                                        <a onClick={(e) => this.editQuestion(idx, index)}><span className="btn cmn-btn1">{val.btn_txt}</span></a></React.Fragment>}
                                                                            </div>
                                                                        </li> 

                                                                    </ul>
                                                                </div>
                                                            )) : ''}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>

                                <div className="csform-group" style={{ display: 'none' }}>
                                    <div className='additional_ques'>
                                        <label>Additional Questions:</label>

                                        {
                                            this.state.additionQuesAr.map((AdQues, i) => {
                                                return (
                                                    <div className="csform-group" key={i}>
                                                        <input
                                                            type="text"
                                                            className="csForm_control inp_blue"
                                                            placeholder='Type Ques..'
                                                            id={AdQues.id}
                                                            name={AdQues.name}
                                                            onChange={this.changeQuesText}
                                                            value={AdQues.inpValue}
                                                        />
                                                    </div>
                                                )
                                            })
                                        }


                                    </div>
                                    <div className='add_Ques'>
                                        <span className='add_ic icon icon-add3-ie' onClick={this.AddQuesInput}></span>
                                        <div className='clearfix'></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className='row'> 
                            <div className='col-md-6 no_pd_l'>
                             {(this.state.job_status == 0 || this.props.props.match.params.jobType == 'D') ?
                                <ul className='subTasks_Action__ left'>
                                    <li ><span className='sbTsk_li' onClick={(e) => this.resetAllFields()}>Reset All Fields</span></li>
                                </ul>
                            :''}
                            </div>

                            {(this.state.job_status != 2) ?
                                <div className='col-md-6 text-right no_pd_r crJoBtn_Col__'>
                                    {
                                        (this.state.job_status == 0 && this.props.props.match.params.jobType == 'E') ?
                                            (<input type='button' className='btn cmn-btn1 crte_svBtn' value='Delete job' onClick={(e) => this.updateJobStatus(e, 1, 'Delete this job?')} />)
                                            : ((this.state.job_status == 5) ?
                                                <input type='button' className='btn cmn-btn1 crte_svBtn' value='Cancel job' onClick={(e) => this.updateJobStatus(e, 0, 'Cancel this job?')} />
                                                : (this.state.job_status == 3) ?
                                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Close job' onClick={(e) => this.updateJobStatus(e, 2, 'Close this job?')} />
                                                    : '')}

                                    {(this.props.props.match.params.jobType == 'E') ?
                                        <input type='button' className='btn cmn-btn1 crte_svBtn' value='Save Changes' onClick={(e) => this.setstateOfDraftAndPostJob(true, false)} />
                                        :
                                        <input type='button' className='btn cmn-btn1 crte_svBtn' value='Save As Draft' onClick={(e) => this.setstateOfDraftAndPostJob(true, false)} />
                                    }
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Preview Job' onClick={() => this.setState({ quickModal: true }, () => {/* console.log(this.state) */ })} />
                                </div>
                                : 
                                <div className='col-md-6 text-right no_pd_r crJoBtn_Col__'>
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Save As Draft' onClick={(e) => this.setstateOfDraftAndPostJob(true, false)} />
                                    <input type='button' className='btn cmn-btn1 crte_svBtn' value='Preview Job' onClick={() => this.setState({ quickModal: true }, () => {/* console.log(this.state) */ })} />
                                </div>
                            }
                        </div>

                    </form>
                </BlockUi>
                {(this.state.quickModal) ? <QuickPreviewModal showModal={this.state.quickModal} closeModal={() => this.setState({ quickModal: false })} parentState={this.state} setstateOfDraftAndPostJob={this.setstateOfDraftAndPostJob} /> : ''}
                {(this.state.AddDocumentsModal) ? <AddDocuments showModal={this.state.AddDocumentsModal} closeModal={() => this.setState({ AddDocumentsModal: false })} all_documents={this.state.all_documents} addDocuments={this.addDocuments} handleDocumentOnChange={this.handleDocumentOnChange} /> : ''}

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CreateJob);