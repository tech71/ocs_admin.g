import React, { Component } from 'react';
import ApplicantAttachmentModal from './ApplicantAttachmentModal';
import ApplicantAddAttachmentModal from './ApplicantAddAttachmentModal';
import './recruit2.css';
import { connect } from 'react-redux';
import _ from 'lodash';
import { BASE_URL } from '../../../../config';
import ReactPlaceholder from 'react-placeholder';
import { custNumberLine, customHeading, closeIcons } from 'service/CustomContentLoader.js';
import { NottachmentAvailable } from 'service/custom_value_data.js';
import { postData, downloadFile } from 'service/common.js';
import ScrollArea from "react-scrollbar";
import jQuery from "jquery";
import axios from 'axios';

class ApplicantAttachment extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            ApplicantAttachment: false,
            ApplicantAddAttachment: false,
        }
    }

    componentDidMount() {
    }
    
    render() {
        let currentDocumet = this.props.applicant_attachment_list.filter((row) => row.archive == 0 && _.lowerCase(row.category_title) != 'other');
        let currentOtherDocumet = this.props.applicant_attachment_list.filter((row) => row.archive == 0 && _.lowerCase(row.category_title) == 'other');

        return (
            <React.Fragment>
                <div className="row bg_w mt-3">
                    <div className="app_infoBox__">
                        <div className="row">

                            <div className="col-md-12 col-sm-12 d_flex1  align-items-center pb_15p bor_bot_b col-xs-12">
                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(40, 'left')} ready={!this.props.info_loading}>
                                    <h4 className="flex-1 pr_15p"><strong>Applicant Attachments</strong></h4>
                                    <button className="btn cmn-btn1 m_l_10p" onClick={() => { this.setState({ ApplicantAttachment: true }) }}>Manage Attachment</button>
                                    <button className="btn cmn-btn1 m_l_10p" onClick={() => { this.setState({ ApplicantAddAttachment: true }) }}>Add Attachment</button>
                                </ReactPlaceholder>
                            </div>

                            <div className="col-md-12 col-lg-12 col-xs-12">
                                <div className="row mt-3 d-flex flex-wrap after_before_remove">
                                    <div className="col-md-7 pd_r_20p col-sm-7 col-xs-12">
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(4)} ready={!this.props.info_loading}>
                                            <div className="cstmSCroll1">
                                                <ScrollArea
                                                    speed={0.8}
                                                    className="stats_update_list"
                                                    contentClassName="content"
                                                    horizontal={false}
                                                    style={{ paddingRight: "15px", height: 'auto', maxHeight: '300px' }}
                                                >
                                                    <div className="row d-flex flex-wrap doc_Cat_row">
                                                        {currentDocumet.length > 0 ? currentDocumet.map((row, index) => {
                                                            return (<div className="col-md-4 mb_20p col-sm-4 col-xs-6" key={index}><h5 className="pb_10p"><strong>{row.category_title}</strong></h5><div>
                                                            <a onClick={() => downloadFile(BASE_URL + 'mediaShow/r/' + this.props.id + '/' + encodeURIComponent(btoa(row.attachment)),row.attachment)}  className="btn eye-btn cmn-btn1 wid_100p dotted_line_one">{row.attachment_title}</a></div></div>);

                                                        }) : <div className="mt-2 w-100"><NottachmentAvailable /></div>
                                                        }
                                                    </div>
                                                </ScrollArea>
                                            </div>
                                        </ReactPlaceholder>
                                    </div>

                                    <div className="col-md-5 col-sm-12 bor_l_b pd_l_20p bl-xs-0 bt-xs-1 pt-sm-3">
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(4)} ready={!this.props.info_loading}>
                                            <h5 className=""><strong>Other Attachments</strong></h5>

                                            <div className="other_Attch d_flex1 flex_wrap1 mt-1">
                                                {currentOtherDocumet.length > 0 ? currentOtherDocumet.map((row, index) => {
                                                    return (<div key={index}><a onClick={() => downloadFile(BASE_URL + 'mediaShow/r/' + this.props.id + '/' + encodeURIComponent(btoa(row.attachment)),row.attachment)} target="_blank" className="btn eye-btn cmn-btn1 wid_100p">{row.attachment_title}</a></div>);

                                                }) : <NottachmentAvailable />
                                                }
                                            </div>
                                        </ReactPlaceholder>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                {this.state.ApplicantAttachment ? <ApplicantAttachmentModal show={this.state.ApplicantAttachment} close={() => this.setState({ ApplicantAttachment: false })} /> : ''}
                {this.state.ApplicantAddAttachment ? <ApplicantAddAttachmentModal show={this.state.ApplicantAddAttachment} close={() => this.setState({ ApplicantAddAttachment: false })} /> : ''}

            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({
    ...state.RecruitmentApplicantReducer.details,
    applicant_attachment_list: state.RecruitmentApplicantReducer.attachment_list,
    info_loading: state.RecruitmentApplicantReducer.info_loading,
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(ApplicantAttachment)