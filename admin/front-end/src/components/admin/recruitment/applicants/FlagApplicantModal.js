import React, { Component } from 'react';
import {  postData, handleChangeChkboxInput } from 'service/common.js';
import jQuery from "jquery";
import { ToastUndo } from 'service/ToastUndo.js';
import {toast } from 'react-toastify';

class FlagApplicantModal extends Component {

    constructor() {
        super();
        this.state = {
            
        }
    }
    
    submitFlag = (e) => {
        e.preventDefault();
        
        jQuery("#flag_applicant").validate();
        
        if(jQuery("#flag_applicant").valid()){
             this.setState({loading: true});
             var request_data = {'reason_title': this.state.reason_title, 'reason_note': this.state.reason_note, 'applicant_id': this.props.applicant_id};
             toast.dismiss();
             postData('recruitment/RecruitmentApplicant/flage_applicant', request_data).then((result) => {
                        if (result.status) {
                            this.setState({ loading: false });
                     
                            toast.success(<ToastUndo message={'Request for the flag an applicant successfully.'} showType={'s'} />, {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                            });
                            this.props.closeModal(true);
                        } else {
                            toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                                position: toast.POSITION.TOP_CENTER,
                                hideProgressBar: true
                            });
                            this.setState({ loading: false });
                     }
              });
        }
        
    }

    render() {
        return (
            <div className={'customModal ' + (this.props.showModal ? ' show Flag_application_size' : '')}>
                <div className="cstomDialog widBig">
                <form id="flag_applicant" method="post">
                    <h3 className="cstmModal_hdng1--">
                         Flag Applicant
                        <span className="closeModal icon icon-close1-ie" onClick={() => this.props.closeModal(false)}></span>
                    </h3>

                    <div className="row  pd_b_20 ">
                        <div className="col-md-6">
                            <div className="csform-group">
                                <label>Reason for Flaging Applicant:</label>
                                <input type="text" name="reason_title" className="csForm_control bl_bor" data-rule-required="true" value={this.state.reason_title} onChange={(e) => handleChangeChkboxInput(this, e)} />
                            </div>
                        </div>
                    </div>

                    <div className="row  pd_b_20 ">
                        <div className="col-md-12">
                            <div className="csform-group">
                                <label>Add New Note:</label>
                                <span className="required">
                                <textarea className="csForm_control bl_bor txt_area brRad10 textarea-max-size" value={this.state.reason_note} name="reason_note" onChange={(e) => this.setState({reason_note: e.target.value})} data-rule-required="true"></textarea>
                                </span>
                            </div>
                        </div>
                    </div>
                  
                    <div className="row bt-1">
                        <div className="col-md-12 text-right">
                            <button onClick={this.submitFlag} disabled={this.state.loading} className="btn cmn-btn1 flag-btn mt-1">Flag Applicant</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default FlagApplicantModal;