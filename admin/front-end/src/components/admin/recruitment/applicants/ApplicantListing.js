import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import moment from "moment";
import 'react-table/react-table.css'
import { ProgressBar } from 'react-bootstrap';
import {  postData, reFreashReactTable, archiveALL, checkLoginWithReturnTrueFalse, getPermission } from 'service/common.js';
import { connect } from 'react-redux'
import RecruitmentPage from 'components/admin/recruitment/RecruitmentPage';
import FlagApplicantModal from './FlagApplicantModal';
import Pagination from "service/Pagination.js";
import {defaultSpaceInTable} from 'service/custom_value_data.js';

import { TrComponent, getTrProps } from 'service/ReactTableTrProgressBar'
import { ROUTER_PATH } from 'config.js';
import PropTypes from 'prop-types'
const queryString = require('query-string');


const requestData = (pageSize, page, sorted, filtered) => {
 
    return new Promise((resolve, reject) => {
        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('recruitment/RecruitmentApplicant/get_requirement_applicants', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

console.log(PropTypes);

class Applicants extends Component {

    constructor() {
        super();
        this.state = {
            searchVal: '',
            filterVal: '',
            applicantList: [],
            flageModel: false,
            filter_val: 1,
            job_position_option: [],
        }
        this.permission = (checkLoginWithReturnTrueFalse()) ? ((getPermission() == undefined) ? [] : JSON.parse(getPermission())) : [];
        this.reactTable = React.createRef();
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                applicantList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    get_job_position = () => {
        postData('recruitment/RecruitmentApplicant/get_job_postion_by_create_job', {}).then((result) => {
            if (result.status) {
                var res = [{ label: 'All', value: 'all' }];
                var data = result.data
                data = res.concat([...data]);
                this.setState({ job_position_option: data });
            }
        });
    }
  
    componentDidMount(){
        this.get_job_position();
    }

    filterChange = (key, value) => {
        var state = {};
        state[key] = value;
        this.setState(state, () => {
            this.setTableParams();
        });
    }

    submitSearch = (e) => {
        e.preventDefault();
        this.setTableParams();
    }

    closeFlagModal = (status) => {
        if (status) {
            reFreashReactTable(this, 'fetchData');
        }
        this.setState({ flageModel: false })
    }

    openFlagModal = (applicant_id) => {
        this.setState({ flageModel: true, applicant_id: applicant_id })
    }

    archiveApplicant = (applicant_id) => {
        archiveALL({ applicant_id: applicant_id }, 'Are you sure want to archive this applicant', "recruitment/RecruitmentApplicant/archive_applicant").then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        });
    }

    setTableParams = ()=>{
        var search_re = {search: this.state.search, filter_val: this.state.filter_val, job_pos: this.state.job_pos, ...this.defualtFilter};
        this.setState({filtered: search_re});
    }


    render() {
        this.defualtFilter = queryString.parse(window.location.search);
        
        var filter_option = [
            { value: '1', label: 'In-Progress' },
            { value: '3', label: 'Hired' },
            { value: '2', label: 'Rejected Applicants' },
        ];

        var columns = [
            {
                // Header: "Name:",
                accessor: "FullName",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line1__">Name</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
            },
            {
                // Header: "Job Position:",
                accessor: "job_position",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line1__">Job Position</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
            },
            {
                // Header: "Date Applied:",
                accessor: "date_applide",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line1__">Date Applied</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{moment(props.value).format('DD/MM/YYYY')}</span>
            },
            {
                // Header: "Recruitment Stage:",
                accessor: "stage",
                sortable: false,
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line1__">Recruitment Stage</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
            },
            {
                expander: true,
                Header: () => <strong></strong>,
                width: 45,
                accessor: "progress_count",
                headerStyle: { border: "0px solid #fff" },
                className: '_align_c__',
                Expander: ({ isExpanded, ...rest }) =>
                    <div className="expander_bind">
                        {isExpanded ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i> : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }
            }
        ];

        return (
            <React.Fragment>

                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        <Link to={ROUTER_PATH + 'admin/recruitment/dashboard'} className="icon icon-back1-ie"></Link>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1>{this.props.showPageTitle}</h1>
                    </div>
                </div>

                <div className="row action_cont_row">

                    <div className="col-lg-12 col-md-12">

                        <div className="tasks_comp">
                            <form onSubmit={(e) => this.submitSearch(e)} method="post">
                                <div className="row sort_row1-- after_before_remove">
                                    <div className="col-lg-6 col-md-6 col-sm-12 no_pd_l noPd_R_ipd">
                                        <div className="search_bar right  srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Applicant specific Search Bar.." onChange={(e) => this.setState({ search: e.target.value })} value={this.state.search} />
                                            <i className="icon icon-search2-ie" onClick={(e) => this.submitSearch(e)}></i>
                                        </div>
                                    </div>

                                    <div className="col-lg-6 col-md-6 col-sm-12 no_pd_r">
                                        <div className='row'>
                                            <div className='col-md-6 col-sm-6 pr-xs-0'>
                                                <div className="filter_flx lab_vrt">
                                                    <label>Filter by:</label>
                                                    <div className="filter_fields__ cmn_select_dv gr_slctB ">
                                                        <Select name="view_by_status"
                                                            simpleValue={true}
                                                            searchable={false}
                                                            placeholder="Filter by: Unread"
                                                            clearable={false}
                                                            options={filter_option}
                                                            onChange={(e) => this.filterChange('filter_val', e)}
                                                            value={this.state.filter_val}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-md-6 col-sm-6 pr-xs-0'>
                                                <div className="filter_flx lab_vrt">
                                                    <label>Show:</label>
                                                    <div className="filter_fields__ cmn_select_dv gr_slctB  ">
                                                        <Select name="view_by_status"
                                                            clearable={false}
                                                            simpleValue={true}
                                                            searchable={false}
                                                            placeholder="Show All"
                                                            options={this.state.job_position_option}
                                                            onChange={(e) => this.filterChange('job_pos', e)}
                                                            value={this.state.job_pos}
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                           



                            <div className="row Req-Applicant_tBL">

                                <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL table_progress">
                                    {/* <div className="data_table_cmn dataTab_accrdn_cmn aplcnt_table hdng_cmn2 table_progress"> */}

                                    <ReactTable
                                        TrComponent={TrComponent}
                                        getTrProps={getTrProps}
                                        PaginationComponent={Pagination}
                                        ref={this.reactTable}
                                        manual="true"
                                        loading={this.state.loading}
                                        pages={this.state.pages}
                                        onFetchData={this.fetchData}
                                        filtered={this.state.filtered}
                                        defaultFiltered={{...this.defualtFilter, filter_val: 1}}
                                        columns={columns}
                                        data={this.state.applicantList}
                                        defaultPageSize={10}
                                        minRows={1}
                                        onPageSizeChange={this.onPageSizeChange}
                                        noDataText="No Record Found"
                                        collapseOnDataChange={true}
                                        className="-striped -highlight"
                                        previousText={<span className="icon icon-arrow-left privious"></span>}
                                        nextText={<span className="icon icon-arrow-right next"></span>}
                                        SubComponent={(props) => <ApplicantExpander {...props.original} openFlagModal={this.openFlagModal} archiveApplicant={this.archiveApplicant} permission={this.permission} />}
                                    />

                                </div>

                            </div>


                            {/* row ends */}
                        </div>
                        {/* tasks_comp ends */}
                    </div>
                    {/* col-sm-10 ends */}
                </div>
                {/* row ends */}

                {this.state.flageModel ? <FlagApplicantModal applicant_id={this.state.applicant_id} showModal={this.state.flageModel} closeModal={this.closeFlagModal} /> : ''}
            </React.Fragment>
        );
    }
}

ReactTable.PropTypes ={
    defaultFiltered: PropTypes.object
}
const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Applicants);

class ApplicantExpander extends Component {

    render() {

        return (
            <div className='tBL_Sub applicant_info1'>
                <div className='row bor_l_cols bor_top pt-3 bor_bot_b1 pb-3'>
                    <div className='col-md-4 col-xs-12 br-xs-0'>
                        <p><b>{this.props.FullName}</b> ({this.props.appId})</p>
                        <div className='applis_dets1'>
                            <p>Phone: <b>{this.props.phone}</b></p>
                            <p>Email: <b>{this.props.email}</b></p>
                        </div>
                        <p>Assigned to: <b>{this.props.recruiter_name || 'Not Assigned'}</b></p>
                    </div>

                    <div className='col-md-3  col-xs-12 cntsDv br-xs-0 bt-xs-1 py-3'>
                        <p><b>Pending:</b></p>
                        <div>{this.props.sub_stage}</div>
                    </div>

                    {this.props.questions.seek.length > 0? 
                    <div className='col-md-5 col-xs-12  cntsDv bt-xs-1'>
                        <p><b>Seek Questions :</b></p>
                        <div className="Seek_Q_ul">
                            {this.props.questions.seek.map((val, index) => (
                                <div className="Seek_Q_li" key={index+1}>
                                    <span><i class={"icon " + ((val.answer_status == 1) ? "icon-accept-approve1-ie" : "icon-cross-icons")}></i></span>
                                    <div>{val.question}</div>
                                </div>
                            ))}
                        </div>
                    </div>: 
                    <div className='col-md-5 col-xs-12  cntsDv  bt-xs-1 pt-xs-3'>
                        <p><b>Website Questions :</b></p>
                        <div className="Seek_Q_ul">
                            {this.props.questions.website.map((val, index) => (
                                <div className="Seek_Q_li" key={index+1}>
                                    <span><i class={"icon " + ((val.answer_status == 1) ? "icon-accept-approve1-ie" : "icon-cross-icons")}></i></span>
                                    <div>{val.question}</div>
                                </div>
                            ))}
                        </div>
                    </div>}
                </div>


                <div className='row'>
                    <div className="col-md-12 ">
                        <div className="applicant_info_footer_1">
                            <div className='no_pd_l'>
                                <Link to={ROUTER_PATH + 'admin/recruitment/applicant/' + this.props.id}>
                                    <button className="btn cmn-btn1 apli_btn__ eye-btn">View Applicant Information</button>
                                </Link>

                            </div>
                            <div className='no_pd_r'>
                                <ul className="subTasks_Action__">
                                    {this.props.permission.access_recruitment_admin ? <li><span className="sbTsk_li" onClick={() => this.props.archiveApplicant(this.props.id)}>Archive Applicant</span></li> : ''}
                                    <li><span className="sbTsk_li" onClick={() => this.props.openFlagModal(this.props.id)}>Flag Applicant</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                {/* row ends */}

                <div className="progress-img"></div>
                <div className="progress-b1">
                    <div className="overlay_text_p0">Application Progress: {this.props.progress_count}% Complete</div>
                    <ProgressBar now={this.props.progress_count} />

                </div>
            </div>
        )
    }
}

