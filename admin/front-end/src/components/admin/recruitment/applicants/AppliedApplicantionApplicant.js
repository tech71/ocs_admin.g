import React, { Component } from 'react';
import Select from 'react-select-plus';
import {  postData,  handleShareholderNameChange } from 'service/common.js';
import ReactTable from "react-table";

import jQuery from "jquery";
import { getApplicantInfo } from './../actions/RecruitmentApplicantAction';
import { connect } from 'react-redux'
import ReactPlaceholder from 'react-placeholder';
import { custNumberLine} from 'service/CustomContentLoader.js';

class AppliedApplicantionApplicant extends Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            applications: [],
        }
    }

    componentWillReceiveProps(newProps) {
        if(newProps.applications){
            this.setState({applications: JSON.parse(JSON.stringify(newProps.applications)), discard_changes: false})
        }
    }
    
    discardChanges = () => {
        this.setState({applications: JSON.parse(JSON.stringify(this.props.applications)), discard_changes: false});
    }
    
    saveChanges = () => {
        if(this.state.applications.length > 0){
            this.setState({loading: true});
            postData('recruitment/RecruitmentApplicant/update_applied_application_of_application', {applications: this.state.applications, applicant_id: this.props.applicant_id}).then((result) => {
                if (result.status) {
                    this.props.getApplicantInfo(this.props.applicant_id);
                    this.setState({loading: false});
                }
            });
        }else{
            
        }
    }
    
    cloaseModel = (status, data, id) => {
        this.setState({showModel: false});

        var applications = this.state.applications;
        
        if(status){
            var state = {discard_changes: true};
            if(id){
                var index = applications.findIndex(x => x.id == id);
                applications[index] = data;
            }else{
                 applications = applications.concat([data]);
            }
           
            state['applications'] = applications;
            this.setState(state);
        }
    }

    render() {
        var applications = this.state.applications.filter((s, sidx) => s.remove !== true);
        return (
            <React.Fragment>
                <div className="col-md-8 pd_r_20p col-xs-12">
                     <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(4)} ready={!this.props.info_loading}>
                    <div className="d-flex mt-3 justify-content-between">
                   
                        <h3 className="align-self-center"><strong>Current Applications</strong></h3>
                        <button disabled={this.props.applicant_status == 1? false: true} className="btn cmn-btn1 flag-btn add-btn-z1" onClick={() => this.setState({showModel: true, editData: []})}>Add Application</button>
                    
                    </div>


                    {/* <div className="data_table_cmn dataTab_accrdn_cmn tbl_flx2 header_center tble_2_clr mt-3 currAPli_tble tbl_fnt_sm"> */}
                    <div className="Req-Applicant-info_tBL mt-3">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-2_tBL line_space_tBL H-Set_tBL">
                       
                        <ReactTable
                            columns={[
                                { 
                                    Header: "", accessor: "serial", 
                                    className: '_align_c__',
                                    Cell: (props) => (props.index + 1), width: 50 },
                                { 
                                    Header: "Position Applied for",
                                    className: '_align_c__',
                                     accessor: "position_applied",
                                     Cell: props => <span>{props.value}</span>
                                     },
                                { 
                                    Header: "Recruitment Area", 
                                    className: '_align_c__',
                                    accessor: "recruitment_area",
                                    Cell: props => <span>{props.value}</span>
                                 },
                                { 
                                    Header: "Employment Type", 
                                    className: '_align_c__',
                                    accessor: "employement_type",
                                    Cell: props => <span>{props.value}</span>
                                 },
                                {
                                    Header: "Application Channel",
                                     accessor: "channel",
                                     className: '_align_c__',
                                    Cell: (props) => (
                                        <span className="Current_apl_tr_td_last">
                                            <div>{props.value}</div>
                                            {this.props.applicant_status == 1? <i className="icon icon icon-views" onClick={() => this.setState({showModel: true, editData: props.original})}></i>:''}
                                            {applications.length > 1 && this.props.applicant_status == 1? <i className="icon icon-remove2-ie" onClick={(e) => {handleShareholderNameChange(this, 'applications', props.index, 'remove', true); this.setState({discard_changes: true})}}></i>: ''}
                                            
                                        </span>
                                    )
                                },
                            ]}
                            defaultPageSize={3}
                            data={applications}
                            pageSize={applications.length}
                            showPagination={false}
                            className="-striped -highlight"
                        />
                        
                    </div>
                    </div>
                    </ReactPlaceholder>
                    <div className="d-flex mt-3 justify-content-end">
                        {this.state.discard_changes? <button disabled={this.state.loading} onClick={this.discardChanges} className="btn cmn-btn1 mr-2" style={{background:"red"}}>Discard Changes</button> : ''}
                        {this.state.discard_changes? <button disabled={this.state.loading} className="btn cmn-btn1" onClick={this.saveChanges}>Save Changes</button>: ''}
                    </div>
                </div>
                
                {this.state.showModel?<AddEditAppliedApplicantion closeModel={this.cloaseModel} editData={this.state.editData} showModel={this.state.showModel}/>:''}

            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({
    applicant_id: state.RecruitmentApplicantReducer.details.id,
    applicant_status :state.RecruitmentApplicantReducer.details.status,
    applications: state.RecruitmentApplicantReducer.applications,
    info_loading: state.RecruitmentApplicantReducer.info_loading,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getApplicantInfo: (applicant_id) => dispach(getApplicantInfo(applicant_id)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(AppliedApplicantionApplicant);
    
class AddEditAppliedApplicantion extends Component {

    constructor() {
        super();
        this.state = {
            
        }
        this.validator = jQuery('#applicationId').validate();
    }
    
    getDropdownOption = () => {
        postData('recruitment/RecruitmentApplicant/get_applicant_applicant_option', {}).then((result) => {
                if (result.status) {
                    this.setState(result.data);
                }
        });
    }

    componentDidMount() {
        this.getDropdownOption();
        if(this.props.editData){
            this.setState(this.props.editData)
        }
    }
    
    handleChange = (value, fieldName) => {
        var state = {};
        state[fieldName] = value;
        this.setState(state, () => {
            if(this.validator){
                jQuery('#applicationId').valid();
            }
        });
    }
    
    onSubmit = (e) => {
        e.preventDefault();
        var mainState = {position_applied_id: this.state.position_applied_id, recruitment_area_id: this.state.recruitment_area_id, employement_type_id: this.state.employement_type_id, channel_id: this.state.channel_id};
        
        jQuery('#applicationId').validate();
        if(jQuery('#applicationId').valid()){
            
            var jobPositions =  this.state.jobPositions;
            var index = jobPositions.findIndex(x => x.value == this.state.position_applied_id);
            mainState['position_applied'] = jobPositions[index]['label'];

            var recruitmentAreas = this.state.recruitmentAreas; 
            var index = recruitmentAreas.findIndex(x => x.value == this.state.recruitment_area_id);
            mainState['recruitment_area'] = recruitmentAreas[index]['label'];

            var employmentTypes = this.state.employmentTypes;
            var index = employmentTypes.findIndex(x => x.value == this.state.employement_type_id);
            mainState['employement_type'] = employmentTypes[index]['label'];

            var index = this.state.channels.findIndex(x => x.value == this.state.channel_id);
            mainState['channel'] = this.state.channels[index]['label'];
            mainState['id'] = this.state.id;
            
            this.props.closeModel(true, mainState, this.state.id);
        }else{
            this.validator = true;
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className={'customModal ' + (this.props.showModel ? ' show' : '')}>
                <div className="cstomDialog widBig" style={{width:' 800px', minWidth: '800px'}}>

                    <h3 className="cstmModal_hdng1--">
                        Edit Application
                        <span className="closeModal icon icon-close1-ie" onClick={() => this.props.closeModel(false)}></span>
                    </h3>

                    <form method="post" id="applicationId">
                        <div className='row pd_lf_15 mr_tb_20 d-flex justify-content-center flexWrap' >
                            <div className='col-md-12 col-xs-12'>
                                <div className='row '>
                                    <div className="col-sm-5">
                                        <div className="csform-group">
                                            <label className="pd_l_15">Position Applied:</label>
                                            <span className="required">
                                            <Select name="participant_assessment"
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={this.state.jobPositions}
                                                    value={this.state.position_applied_id}
                                                    onChange={(e) => this.handleChange( e, 'position_applied_id')}
                                                    className={'custom_select default_validation'}
                                                    inputRenderer={() => <input type="text"  className="define_input" name={"position_applied_id"} required={true} value={this.state.position_applied_id || ''} />}
                                                />
                                                </span>
                                        </div>
                                    </div>

                                    <div className="col-sm-5">
                                        <div className="csform-group">
                                            <label className="pd_l_15">Recruitment Area:</label>
                                            <span className="required">
                                            <Select name="participant_assessment"
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={this.state.recruitmentAreas}
                                                    value={this.state.recruitment_area_id}
                                                    onChange={(e) => this.handleChange( e, 'recruitment_area_id')}
                                                    className={'custom_select default_validation'} 
                                                    inputRenderer={() => <input type="text" className="define_input" name={"recruitment_area_id"} required={true} value={this.state.recruitment_area_id || ''} />}
                                                />
                                            </span>
                                        </div>
                                    </div>

                                    <div className="col-sm-5">
                                        <div className="csform-group">
                                            <label className="pd_l_15">Employment Type:</label>
                                            <span className="required">
                                            <Select name="participant_assessment"
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={this.state.employmentTypes}
                                                    value={this.state.employement_type_id}
                                                    onChange={(e) => this.handleChange( e, 'employement_type_id')}
                                                    className={'custom_select default_validation'} 
                                                    inputRenderer={() => <input type="text" className="define_input" name={"employement_type_id"} required={true} value={this.state.employement_type_id || ''} />}
                                                />
                                                </span>
                                        </div>
                                    </div>

                                    <div className="col-sm-5">
                                        <div className="csform-group">
                                            <label className="pd_l_15">Application Channel:</label>
                                            <span className="required">
                                            <Select name="participant_assessment"
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={this.state.channels}
                                                    value={this.state.channel_id}
                                                    onChange={(e) => this.handleChange( e, 'channel_id')}
                                                    className={'custom_select default_validation'} 
                                                    inputRenderer={() => <input type="text" className="define_input" name={"channel_id"} required={true} value={this.state.channel_id || ''} />}
                                                />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="row trnMod_Foot__ disFoot1__">
                            <div className="col-sm-12 no-pad text-right">
                                <button type="submit" onClick={this.onSubmit} className="btn cmn-btn1 create_quesBtn">Save Changes</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
            </React.Fragment >
        );
    }
}    
    
    