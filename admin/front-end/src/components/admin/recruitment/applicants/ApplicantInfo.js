import React, { Component } from 'react';
import Select from 'react-select-plus';
import { postData,   checkLoginWithReturnTrueFalse, getPermission } from 'service/common.js';
import ApplicantNotesModal from './ApplicantNotesModal';
import ApplicantAttachment from './ApplicantAttachment';

import EditApplicantInfo from './EditApplicantInfo';
import FlagApplicantModal from './FlagApplicantModal';
import CommunicationLog from './CommunicationLog';
import ApplicantLogs from './ApplicantLogs';
import { Panel,  ProgressBar, PanelGroup } from 'react-bootstrap';
import AppliedApplicantionApplicant from './AppliedApplicantionApplicant';
import { DynamicComponentMapping } from './applicant_stages/DynamicComponentMapping';
import { applicantStageStatus } from 'dropdown/recruitmentdropdown.js';
import moment from "moment";
import './recruit2.css';
import { getOptionsRecruitmentStaff } from './../action_task/CommonMethod.js';
import { connect } from 'react-redux'
import { getApplicantInfo, getApplicantMainStageDetails, getApplicantStageWiseDetails,getApplicantAttachmentDetails, getApplicantLastUpdateDetails } from './../actions/RecruitmentApplicantAction';
import ApplicantAttachmentsAndNotesModal from './ApplicantAttachmentsAndNotesModal';
import CreateNewTask from './../action_task/CreateNewTask.js';
import { getTaskStageDetails } from './../actions/RecruitmentAction.js';
import _ from 'lodash';
import  {AttachmentAndNotesAndCreateTask}  from './applicant_stages/AttachmentAndNotesAndCreateTask';

import ReactPlaceholder from 'react-placeholder';
import { custNumberLine, customHeading, recruitmentStages, recruitmentSubStage } from 'service/CustomContentLoader.js';
import { ROUTER_PATH } from 'config.js';
import { Link } from 'react-router-dom';
import { toastMessageShow } from '../../../../service/common';

class ApplicantInfo extends Component {

    constructor() {
        super();
        this.state = {
            loading: true,
            ApplicantNotes: false,
            EditApplicant: false,
            editModeAssignRecruiter: false,
            ApplicantAttachmentsAndNotesModal: false,
            modal_type_stage_number:'',
            is_main_stage:false,
            current_stage_overwrite:null,
            CreateTaskShowModal:false
        }
        this.permission = (checkLoginWithReturnTrueFalse())?((getPermission() == undefined)? [] : JSON.parse(getPermission())):[];
        this.refTask = React.createRef();
    }

    updateAssignRecruiter = () => {
        if (this.state.updatedRecruiter) {
            var req = { applicant_id: this.props.props.match.params.id, recruiter: this.state.updatedRecruiter.value }
            postData('recruitment/RecruitmentApplicant/update_assign_recruiter', req).then((result) => {
                if (result.status) {
                    this.props.getApplicantInfo(this.props.props.match.params.id);
                    this.setState({ editModeAssignRecruiter: false });
                }
            });
        }
    }
    
    closeFlageModel = (status) => {
        this.setState({flageModelOpen:false})
        if(status){
            this.props.getApplicantInfo(this.props.props.match.params.id);
        }
    }

    componentWillMount(){
        this.props.getApplicantAttachmentDetails(this.props.props.match.params.id);
    }
    componentDidMount() {
        this.props.getApplicantInfo(this.props.props.match.params.id, true);
        this.props.getApplicantMainStageDetails(this.props.props.match.params.id);
        if(this.props.task_stage_list.length==0){
            this.props.getTaskStageDetails();
        }
    }
    
    openStagePanel = (its_open, stage_number) => {
        this.props.getApplicantStageWiseDetails(its_open, stage_number, this.props.id);
    }
    openAttachmentModel=(type,is_main_stage,current_stage_overwrite)=>{
        this.setState({ApplicantAddAttachmentsAndNotes:true,modal_type_stage_number:type,is_main_stage:is_main_stage,current_stage_overwrite:current_stage_overwrite});
    }
    
    CreateTaskCloseModal = (status) => {
        if(status){
            this.props.getApplicantStageWiseDetails(true, this.state.current_open_stage_number, this.props.id);
        }
         this.setState({ CreateTaskShowModal: false });
    }
    CreateTaskShowModal = (stage) => {
        this.setState({ CreateTaskShowModal: true, current_open_stage_number: stage },()=>{
            if (this.refTask.hasOwnProperty('current')) {
                if(!this.permission.access_recruitment_admin && (stage =='3' ||stage =='6')){
                    stage = '0';
                }
                let stageValue = _.find(this.props.task_stage_list,{stage_number:stage});
                let stageVal = stageValue!=undefined && stageValue.hasOwnProperty('value') ? stageValue.value : '';
                if(stageVal!=''){
                    this.refTask.current.wrappedInstance.setStageAndApplicatById(stageVal,this.props.props.match.params.id);
                }
            }
        })
    }

    skipStage = (stageNumber) => {
        if (stageNumber==3 || stageNumber==6) {
            var req = { applicant_id: this.props.props.match.params.id, stageNumber: stageNumber }
            postData('recruitment/RecruitmentCabDayInterview/call_bypass', req).then((result) => {
                if (result.status) {
                    toastMessageShow(result.msg,'s');
                    this.props.getApplicantMainStageDetails(this.props.props.match.params.id);
                    this.props.getApplicantInfo(this.props.props.match.params.id, true);
                }else if(!result.status && result.hasOwnProperty('error')){
                    toastMessageShow(result.error,'e');
                }
            });
        }else{
            toastMessageShow('Invalid Request.','e');
        }
    }
    




    render() {
        const applicant_status_color = { Successful: 'clr_green', Parked: 'clr_yellow', Rejected: 'clr_red' };
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        <a onClick={() => this.props.props.history.goBack()}>
                            <span className="icon icon-back1-ie"></span>
                        </a>

                    </div>
                </div>
                {/* row ends */}

                <div className="row">

                    <div className="col-lg-12 col-md-12 main_heading_cmn- d_flex1">
                        <div className="col-md-7 align-self-center">
                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(70, 'left')} ready={!this.props.info_loading}>
                            <h1>{this.props.showPageTitle}</h1>
                        </ReactPlaceholder>
                        </div>
                        

                        <div className="Lates_up_1 col-md-5 bl-1 align-self-center bl-xs-0 pt-xs-3">
                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(3)} ready={!this.props.info_loading}>
                            <div className="Lates_up_a col-md-4 align-self-center text-right">Latest Update:</div>
                            <div className="col-md-8 justify-content-between pr-0">
                                <div className="Lates_up_b">
                                    {/*<div className="Lates_up_txt align-self-center"><b>Stage 2:</b> Phone Interview Completed</div>*/}
                                    <div className="Lates_up_txt align-self-center">{this.props.last_update.specific_title || 'N/A'}</div>
                                    <div className="Lates_up_btn">
                                        <i className="icon icon-view1-ie" onClick={() => this.setState({logsModelOpen: true})}></i><span>View all</span>
                                    </div>
                                </div>
                                <div className="Lates_up_2">
                                    <div className="Lates_up_txt2 btn-1"><span className="w-50"><b>Date:</b> {this.props.last_update.created? moment(this.props.last_update.created).format("DD/MM/YYYY"): 'N/A'}</span> <span  className="w-50"> <b>Time:</b> {this.props.last_update.created? moment(this.props.last_update.created).format("LT"): 'N/A'}</span></div>
                                </div>
                            </div>
                            </ReactPlaceholder>
                        </div>

                    </div>
                </div>
                {/* row ends */}

              
                <div className="row mt-1">
                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(0, 'center')} ready={!this.props.info_loading}>
                <div className="progress-b1">
                                <div className="overlay_text_p01"> {this.props.applicant_progress}% Complete</div>
                                <ProgressBar className="progress-b2"  now={this.props.applicant_progress}  >
                                </ProgressBar>
                            </div>
                            </ReactPlaceholder>
                        </div>

                <div className="row bg_w"> 
                    <div className="app_infoBox__">
                        <div className="row">

                            <div className="col-md-12 col-sm-12 d_flex1  align-items-center pb_15p bor_bot_b">
                            
                                <h4 className="flex-1 pr_15p"><ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(40, 'left')} ready={!this.props.info_loading}>
                                <strong>Applicant Info</strong></ReactPlaceholder></h4>
                                <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(0, 'center')} ready={!this.props.info_loading}>
                                    <button disabled={(this.props.status == 1 || this.props.status == 2)  && this.props.flagged_status == 0 &&  this.props.duplicated_status == 0? false: true} onClick={() => this.setState({EditApplicant:true})} className="btn cmn-btn1 eye-btn">Edit Applicant Info</button>
                                </ReactPlaceholder>   
                            </div>
                            {/* col-sm-12 ends */}

                            <div className="col-md-12 col-sm-12 bor_bot_b pb_15p">
                                <div className="row mt-3 d_flex1 flex_wrap1 pb_15p after_before_remove">

                                    <div className="col-lg-2 col-md-5 col-sm-6 col-xs-12">
                                    <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(5)} ready={!this.props.info_loading}>
                                        <h3 className="pb_15p"><strong>{this.props.fullname}</strong></h3>
                                        <h4>(Id: APP-{this.props.appId})</h4>

                                        {/* <div className={"Def_btn_01 my-3"}>{this.props.applicant_status}</div> */}

                                    <span className={"Def_btn_01 my-3 " + (applicant_status_color.hasOwnProperty(this.props.applicant_status) ? applicant_status_color[this.props.applicant_status] : 'Def_btn_01')}>{this.props.applicant_status}</span>

                                        <label className="customChecks chck_clr1 mt-1 pl-sm-2">
                                            <input type="checkbox" onChange={() => this.setState({flageModelOpen: true})} disabled={ this.props.flagged_status == 0? false: true} checked={this.props.flagged_status == 2? true: false}/>
                                            <div className="chkLabs fnt_sm">{this.props.flagged_status==1 ? 'Flag Applicant pending':'Flag Applicant as Inappropriate'}</div>
                                        </label>
                                        </ReactPlaceholder>
                                    </div>

                                    <div className="col-lg-8 col-md-7 pd_lr_20p col-sm-6 col-xs-12">

                                        <div className="row">

                                            <div className="col-lg-4 col-md-6">
                                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(6)} ready={!this.props.info_loading}>
                                                <h5><strong>Applicant Information:</strong></h5>

                                                <div className="cstm_Tble mt-2 apli_infTble1 ">
                                               
                                                    <div className="cstm_tr">
                                                        <div><strong>Phone:</strong></div>
                                                        {this.props.phones.map((val, index) => (
                                                            <div key={index + 1}>{val.phone}</div>
                                                        ))}
                                                    </div>
                                                    <div className="cstm_tr">
                                                        <div><strong>Email:</strong></div>
                                                        {this.props.emails.map((val, index) => (
                                                            <div key={index + 1}>{val.email}</div>
                                                        ))}
                                                    </div>
                                                </div>
                                                <h5 className="mt-2"><strong>Primary Address:</strong></h5>
                                                {this.props.addresses.map((val, index) => (
                                                    <div className="fnt_14 mt-1 mb-1" key={index + 1}>{val.street + ' ' + val.city + ', ' + val.stateName + ', ' + val.postal}</div>
                                                ))}
                                            </ReactPlaceholder> 
                                            </div>


                                            <div className="col-lg-8 col-md-6">
                                            <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(4)} ready={!this.props.info_loading}>
                                            <h5><strong>References:</strong></h5>
                                            <div className="row d-flex flex-wrap appLi_listBor__ mt-1">
                                            {this.props.references.map((val, index) => (
                                                <div className="col-lg-6 col-md-12" key={index + 1}>
                                                    {/* console.log((index % 2)) */}
                                                    <div className={"cstm_Tble w-90 apli_infTble1"}>
                                                        <div className="cstm_tr">
                                                            <div><strong>Name:</strong></div>
                                                            <div>{val.name}</div>
                                                        </div>
                                                        <div className="cstm_tr">
                                                            <div><strong>Phone:</strong></div>
                                                            <div>{val.phone}</div>
                                                        </div>
                                                        <div className="cstm_tr">
                                                            <div><strong>Email:</strong></div>
                                                            <div>{val.email}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                            </div>
                                            </ReactPlaceholder>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-lg-2 col-md-12 d_flex1 bor_l_b pd_lr_20p bl-sm-0 bt-sm-1 pt-sm-3 mt-sm-4 col-sm-12 col-xs-12" style={{ alignItems: 'flex-end' }}>
                                        <div className="pb_10p">
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(100, 'center')} ready={!this.props.info_loading}>
                                            <h5><strong>Assigned Recruiter:</strong></h5>
                                            </ReactPlaceholder>
                                           
                                            {(this.state.editModeAssignRecruiter && this.permission.access_recruitment_admin && this.props.status == 1)?
                                                <React.Fragment>
                                                     <div className="s-def1 s1 mt-1">
                                                    <Select.Async
                                                        name='assigned_user'
                                                        loadOptions={(e) => getOptionsRecruitmentStaff(e, [])}
                                                        clearable={false}
                                                        placeholder='Search'
                                                        cache={false}
                                                        value={this.state.updatedRecruiter}
                                                        onChange={(e) => this.setState({ updatedRecruiter: e })}
                                                    />
                                                     </div>

                                                     <div className="mt-1">
                                                         <span onClick={this.updateAssignRecruiter} class="short_buttons_01 btn_color_avaiable mr-2">Yes</span>
                                                         <span onClick={() => this.setState({ editModeAssignRecruiter: false })} class="short_buttons_01 btn_color_archive">No</span>
                                                     </div>
                                                     <div className="Assi_Rec_Yep">
                                                    </div>
                                                </React.Fragment>
                                                : <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(100, 'center')} ready={!this.props.info_loading}>
                                                        <h3 className="mt-1 cursor-pointer word_wrap_break_" onClick={() => this.setState({ editModeAssignRecruiter: true, updatedRecruiter: '' })}>
                                                    {this.props.assign_recruiter || "Not Assign"}
                                                </h3>
                                                </ReactPlaceholder>}
                                       
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* col-sm-12 ends */}


                            <div className="col-sm-12">
                                <div className="row  d-flex flex-wrap mt-2">
                                
                                    <AppliedApplicantionApplicant />
                                   

                                    <div className="col-md-4 bor_l_b pd_l_20p bl-xs-0 bt-xs-1 col-sm-12">
                                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={custNumberLine(4)} ready={!this.props.info_loading}>
                                        <h3 className="mt-3"><strong>Communication Logs</strong></h3>
                                        <div className="mt-3 log_btnBoo">
                                            <div>
                                                <Link to={ROUTER_PATH+'admin/recruitment/communications_logs?a='+this.props.id+'&t=1'} className="btn cmn-btn1 eye-btn">SMS</Link>
                                            </div>
                                            <div>
                                                <Link to={ROUTER_PATH+'admin/recruitment/communications_logs?a='+this.props.id+'&t=2'} className="btn cmn-btn1 eye-btn">Email</Link>
                                            </div>
                                            <div>
                                                <Link to={ROUTER_PATH+'admin/recruitment/communications_logs?a='+this.props.id+'&t=3'} className="btn cmn-btn1 eye-btn">Phone</Link>
                                            </div>
                                        </div>
                                         </ReactPlaceholder>
                                    </div>

                                </div>


                            </div>
                            {/* col-sm-12 ends */}

                        </div>
                    </div>
                    {/* app_infoBox__ ends */}
                </div>

                <ApplicantAttachment />

                <div className="row mt-3">
                    <div className="col-md-12 col-lg-12 bor_bot_b pb_10p">
                        <h3><ReactPlaceholder showLoadingAnimation={true} customPlaceholder={customHeading(40, 'left')} ready={!this.props.info_loading}><strong>Recruitment progress</strong></ReactPlaceholder></h3>
                    </div>


                    <div className="col-md-12 text-center" >
                    
                        <div className="Version_timeline_4 timeline_1 rec_applicatn_timeline" style={{
                            display: "inline-flex",
                            flexDirection: "column",
                            justifyContent: "center",
                            margin: "0px auto"
                        }}>
                        <ReactPlaceholder showLoadingAnimation={true} customPlaceholder={recruitmentStages()} ready={!this.props.info_loading}>
                            {this.props.stage_details.map((stage, index) => { 
                                return(
                                <div className="time_l_1" key={index + 1}>
                                    <div className="time_no_div">
                                        <div className="time_no"><span>{stage.stage_number}</span></div>
                                        <div className="line_h"></div>
                                    </div>

                                    <div className="time_d_1" >
                                        <div className="time_d_2">
                                            <PanelGroup
                                            id={"panel_"+stage.stage_number}
                                            accordion
                                            onSelect={(e) => { this.openStagePanel(!stage.its_open, stage.stage_number)}}
                                            >
                                            <Panel eventKey={stage.stage_number} >
                                                <div className="time_txt w-100">
                                                    <div className="time_d_style v4-1_">
                                                        <Panel.Heading >
                                                            <Panel.Title toggle className="v4_panel_title_ v4-2_ mb-0">
                                                                <div className="timeline_h tm_subHd"><span>Stage {stage.stage_number}</span><i  className="icon icon-arrow-down"></i></div>
                                                                <div className="timeline_h tm_subShrH">{stage.title}</div>
                                                            </Panel.Title>
                                                            
                                                        </Panel.Heading>

                                                        <div className="task_table_v4-1__">
                                                            <div className="t_t_v4-1">
                                                                <div className={"t_t_v4-1a complete_msg"}>
                                                                    <div className="ci_btn">{applicantStageStatus(stage.stage_status)}</div> 
                                                                    <div className="ci_date">
                                                                        {(stage.stage_status == 3 || stage.stage_status == 4) ? <React.Fragment>{moment(stage.action_at).format("DD/MM/YYYY")} - {stage.action_username} </React.Fragment> : ''}
                                                                    </div>
                                                                  
                                                                </div>
                                                                <div className="t_t_v4-1b">
                                                                <AttachmentAndNotesAndCreateTask CreateTaskShowModal={this.CreateTaskShowModal} openAttachmentModel={this.openAttachmentModel} stageDetailsAttachmentANdNotes={{task_stage_number:stage.stage_number,stageId:stage.id,stage_number:stage.stage_number,overwrite_stage:true,is_main_stage:true,stage_status:stage.stage_status}} />
                                                                </div>
                                                            </div>
                                                            <div className="text-right">
                                                            {(stage.stage_number==3 && stage.stage_status==2) || (stage.stage_number==6 && stage.stage_status==2)   ?<button className="ci_btn" style={{marginRight:'0'}} onClick={()=>this.skipStage(stage.stage_number)}>Skip Stage</button>:<React.Fragment/>}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <Panel.Body collapsible className="px-1 py-3">
                                                    <div className="time_line_parent w-100">
                                                        {stage.sub_stage.map((sub_stage, sub_index) => {
                                                            var MyComponent = sub_stage.component_name ? DynamicComponentMapping[sub_stage.component_name] : 'React.Fragment';
                                                            return (<ReactPlaceholder showLoadingAnimation={true} customPlaceholder={recruitmentSubStage()} ready={!stage.its_loading}>
                                                                    <MyComponent key={sub_index + 1} {...sub_stage} stage_number={stage.stage_number} stageDetailsAttachmentANdNotes={{task_stage_number:stage.stage_number,stageId:sub_stage.id,stage_number:sub_stage.stage,overwrite_stage:true,is_main_stage:false,stage_status:sub_stage.stage_status}} CreateTaskShowModal={this.CreateTaskShowModal} openAttachmentModel={this.openAttachmentModel}/>
                                                                    </ReactPlaceholder>)
                                                        }
                                                        )}
                                                    </div>
                                                </Panel.Body>

                                            </Panel>
                                            </PanelGroup>
                                        </div>
                                    </div>
                                </div>
                            )})}
                             </ReactPlaceholder>
                        </div>
                       
                    </div>
                </div >


                {this.state.flageModelOpen?<FlagApplicantModal showModal={this.state.flageModelOpen} closeModal={this.closeFlageModel} applicant_id={this.props.id} />:''}
                {this.state.ApplicantNotes? <ApplicantNotesModal show={this.state.ApplicantNotes} close={() => this.setState({ ApplicantNotes: false })} />:''}
                {this.state.EditApplicant ? <EditApplicantInfo show={this.state.EditApplicant} close={() => this.setState({ EditApplicant: false })} />:''}
                {this.state.logsModelOpen? <ApplicantLogs show={this.state.logsModelOpen} closeModal={() => { this.setState({ logsModelOpen: false }) }} applicant_id={this.props.id} />:''}
                {this.state.ApplicantAddAttachmentsAndNotes ? <ApplicantAttachmentsAndNotesModal show={this.state.ApplicantAddAttachmentsAndNotes} close={() => this.setState({ ApplicantAddAttachmentsAndNotes: false })}  stage_title={this.state.modal_type_stage_number} is_main_stage={this.state.is_main_stage} current_stage_overwrite={this.state.current_stage_overwrite} overwrite_stage={true} />:''}
                {(this.state.CreateTaskShowModal) ? <CreateNewTask showModal={this.state.CreateTaskShowModal} closeModal={this.CreateTaskCloseModal} ref={this.refTask} /> : ''}
                {(this.state.viewCommunicationLog) ? <CommunicationLog showModal={this.state.viewCommunicationLog} applicant_id={this.props.id} log_type={this.state.viewLogType} closeModal={() => this.setState({viewCommunicationLog: false})}  /> : ''}

            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({
    ...state.RecruitmentApplicantReducer.details,
    phones: state.RecruitmentApplicantReducer.phones,
    addresses: state.RecruitmentApplicantReducer.addresses,
    emails: state.RecruitmentApplicantReducer.emails,
    references: state.RecruitmentApplicantReducer.references,
    applications: state.RecruitmentApplicantReducer.applications,
    stage_details: state.RecruitmentApplicantReducer.stage_details,
    applicant_progress: state.RecruitmentApplicantReducer.applicant_progress,
    last_update: state.RecruitmentApplicantReducer.last_update,
    info_loading: state.RecruitmentApplicantReducer.info_loading,
    task_stage_list: state.RecruitmentReducer.task_stage_option,
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        getApplicantInfo: (applicant_id, loading_status) => dispach(getApplicantInfo(applicant_id, loading_status)),
        getApplicantMainStageDetails: (applicant_id) => dispach(getApplicantMainStageDetails(applicant_id)),
        getApplicantAttachmentDetails: (applicant_id) => dispach(getApplicantAttachmentDetails(applicant_id)),
        getApplicantStageWiseDetails: (its_open, stage_number, applicant_id) => dispach(getApplicantStageWiseDetails(its_open, stage_number, applicant_id)),
        getTaskStageDetails: () => dispach(getTaskStageDetails()),
        getApplicantLastUpdateDetails: (applicant_id) => dispach(getApplicantLastUpdateDetails(applicant_id)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(ApplicantInfo)