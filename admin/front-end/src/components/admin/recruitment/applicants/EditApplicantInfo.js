import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { connect } from 'react-redux'
import { getApplicantInfo } from './../actions/RecruitmentApplicantAction';
import { handleChange, handleShareholderNameChange, handleAddShareholder, handleRemoveShareholder, googleAddressFill, postData, getOptionsSuburb, onKeyPressPrevent } from 'service/common.js';
import ReactGoogleAutocomplete from './../../externl_component/ReactGoogleAutocomplete';
import jQuery from "jquery";
import { toast } from 'react-toastify';
import { ToastUndo } from 'service/ToastUndo.js'

class EditApplicantInfo extends Component {

    constructor() {
        super();
        this.state = {
            phones: [],
            emails: [],
            addresses: [],
            references: [],
        }
    }

    componentDidMount() {
        this.setState(JSON.parse(JSON.stringify(this.props)));
        if (this.props.references.length === 0) {
            this.setState({ references: [{ name: '', email: '', phone: '' }] })
        }
        if (this.props.addresses.length === 0) {
            this.setState({ addresses: [{ street: '', state: '', city: '', postal: '' }] })
        }
        this.getStateList();
    }

    getStateList = () => {
        postData('participant/ParticipantDashboard/get_state', []).then((result) => {
            if (result.status) {
                this.setState({ stateList: result.data });
            }
            this.setState({ loading: false });
        });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.validator = jQuery("#edit_applicant").validate({ ignore: [] });

        if (jQuery("#edit_applicant").valid()) {
            this.setState({ loading: true });
            postData('recruitment/RecruitmentApplicant/update_applicant_details', this.state).then((result) => {
                if (result.status) {
                    this.props.getApplicantInfo(this.props.id);
                    this.props.close();
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });
            });
        }
    }

    handleShareholderNameChange = (index, stateKey, fieldtkey, fieldValue) => {
        var state = {};
        var tempField = {};
        var List = this.state[stateKey];
        List[index][fieldtkey] = fieldValue


        if (fieldtkey == 'state') {
            List[index]['city'] = {}
            List[index]['postal'] = ''
            List[index]['state_error'] = false;
        }

        if (fieldtkey == 'city' && fieldValue) {
            List[index][fieldtkey] = fieldValue.value
            List[index]['postal'] = fieldValue.postcode
        }

        state[stateKey] = List;
        this.setState(state);
    }

    render() {
        var references = this.state.references.filter((s, sidx) => s.its_delete !== true);

        return (
            <div className={'customModal ' + (this.props.show ? ' show' : '')}>
                <div className="cstomDialog widBig" >

                    <h3 className="cstmModal_hdng1--">
                        Edit Application Info
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.close}></span>
                    </h3>

                    <form id="edit_applicant" onSubmit={(e) =>  e.preventDefault()} onKeyPress = {onKeyPressPrevent}>

                        <div className='row pd_lf_15 mr_tb_20 d-flex justify-content-center flexWrap' >
                            <div className='col-sm-12 col-xs-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Applicants Information</strong> </label>
                            </div>
                            <div className='col-md-12 col-xs-12'>

                                <div className='row '>
                                    <div className="col-sm-3">
                                        <div className="csform-group">
                                            <label>First Name:</label>
                                            <span className="required">
                                            <input type="text" maxlength="30" name="firstname" required className="csForm_control bl_bor" onChange={(e) => handleChange(this, e)} value={this.state.firstname || ''} />
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="csform-group">
                                            <label>Last Name:</label>
                                            <span className="required">
                                            <input type="text" maxlength="30" name="lastname" required className="csForm_control bl_bor" onChange={(e) => handleChange(this, e)} value={this.state.lastname || ''} />
                                            </span>
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="csform-group">
                                            <label>ID:</label>
                                            <p className="clr_grey">{this.state.appId || ''}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className='col-sm-12 col-xs-12 mt-5'>
                                <label className='bg_labs2 mr_b_20'><strong>Contact Information</strong> </label>
                            </div>
                            <div className='col-md-12 col-xs-12'>
                                <div className='row '>

                                    <div className="col-sm-3">
                                        {this.state.phones.map((val, index) => (
                                            <div className="csform-group" key={index + 1}>
                                                <label>Phone:</label>
                                                <span className="required">
                                                <input type="text" name="phone" maxlength="20" required className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'phones', index, 'phone', e.target.value)} value={val.phone || ''} />
                                                </span>
                                            </div>
                                        ))}
                                    </div>
                                    <div className="col-sm-3">
                                        {this.state.emails.map((val, index) => (
                                            <div className="csform-group" key={index + 1}>
                                                <label>Email:</label>
                                                <span className="required">
                                                <input type="text" maxlength="100" name="email" required className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'emails', index, 'email', e.target.value)} value={val.email || ''} />
                                                </span>
                                            </div>
                                        ))}
                                    </div>

                                </div>

                                {this.state.addresses.map((val, index) => (
                                    <div className='row ' key={index + 1}>
                                        <div className="col-sm-3">
                                            <div className="csform-group">
                                                <label>Primary Address:</label>
                                                <span className="required">
                                                <ReactGoogleAutocomplete className="add_input mb-1" key={index + 1} maxlength="100"
                                                    required={true}
                                                    data-msg-required="Add address"
                                                    name={"address_primary" + index}
                                                    onPlaceSelected={(place) => googleAddressFill(this, index, 'addresses', 'street', place)}
                                                    types={['address']}
                                                    returnType={'array'}
                                                    value={val.street || ''}
                                                    onChange={(evt) => this.handleShareholderNameChange(index, 'addresses', 'street', evt.target.value)}
                                                    onKeyDown={(evt) => this.handleShareholderNameChange(index, 'addresses', 'street', evt.target.value)}
                                                    componentRestrictions={{ country: "au" }}
                                                />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-sm-9">
                                            <div className="row">
                                                <div className="col-md-4">
                                                    <div className="csform-group">
                                                        <label>State:</label>
                                                        <div className="sLT_gray left left-aRRow">
                                                            <Select
                                                                simpleValue={true}
                                                                name="form-field-name"
                                                                value="one"
                                                                options={this.state.stateList}
                                                                clearable={false}
                                                                searchable={false}
                                                                onChange={(e) => handleShareholderNameChange(this, 'addresses', index, 'state', e)}
                                                                value={val.state || ''}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="csform-group">
                                                        <label>Suburb:</label>
                                                        <div className="sLT_gray left left-aRRow">
                                                            <Select.Async clearable={false}
                                                                className="default_validation" required={true}
                                                                value={{ label: val.city, value: val.city }}
                                                                cache={false}
                                                                disabled={(val.state) ? false : true}
                                                                loadOptions={(e) => getOptionsSuburb(e, val.state)}
                                                                onChange={(evt) => this.handleShareholderNameChange(index, 'addresses', 'city', evt)}
                                                                placeholder="Please Select" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="csform-group">
                                                        <label>PostCode:</label>
                                                        <span className="required">
                                                        <input type="text" maxlength="6" name="postal" required className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'addresses', index, 'postal', e.target.value)} value={val.postal || ''} />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <div className="bt-1 mt-3"></div>
                            </div>
                        </div>

                        <div className='row pd_lf_15 mr_tb_20 d-flex justify-content-center flexWrap' >
                            <div className='col-sm-12 col-xs-12'>
                                <label className='bg_labs2 mr_b_20'><strong>Reference:</strong> </label>
                            </div>
                            {this.state.references.map((val, index) => (
                                !val.its_delete ?
                                    <div className='col-md-12 col-xs-12 mt-5' key={index + 1}>
                                        <div className="row d-flex flex-wrap">
                                            <div className="col-md-12 pr-12">
                                                <div className="row">
                                                    <div className='col-sm-12 col-xs-12'>
                                                        <label className='bg_labs2 mr_b_20'><strong>Reference {index + 1}:</strong> </label>
                                                    </div>
                                                </div>
                                                <div className="row d-flex flex-wrap">
                                                <div className="col-md-3">
                                                        <div className="csform-group">
                                                            <label>Full Name:</label>
                                                            <span className="required">
                                                            <input type="text" maxlength="30" required name={"references_name" + index} className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'references', index, 'name', e.target.value)} value={val.name || ''} />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="csform-group">
                                                            <label>Phone:</label>
                                                            <span className="required">
                                                            <input type="text" maxlength="20" required data-rule-phonenumber="true" name={"references_phone" + index} className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'references', index, 'phone', e.target.value)} value={val.phone || ''} />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="csform-group">
                                                            <label>Email:</label>
                                                            <span className="required">
                                                            <input type="text" maxlength="100" required name={"references_email" + index} className="csForm_control bl_bor" onChange={(e) => handleShareholderNameChange(this, 'references', index, 'email', e.target.value)} value={val.email || ''} />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-3 align-self-end">
                                                        <div className="csform-group">
                                                            {index > 0 ? <a onClick={(e) => { val.id > 0 ? handleShareholderNameChange(this, 'references', index, 'its_delete', true, e) : handleRemoveShareholder(this, e, index, 'references') }} className="button_plus__"><i className="icon icon-decrease-icon Add-2-2"></i></a>
                                                                : (references.length == 4) ? '' :
                                                                    <a className="button_plus__" onClick={(e) => handleAddShareholder(this, e, "references", val)}><i className="icon icon-add-icons Add-2-1"></i></a>}
                                                        </div>
                                                    </div>
                                                    </div>
                                            </div>


                                        </div>
                                    </div> : ''
                            ))}
                        </div>

                        <div className="row trnMod_Foot__ disFoot1__">
                            <div className="col-sm-12 no-pad text-right">
                                <button type="submit" onClick={this.onSubmit} className="btn cmn-btn1 create_quesBtn">Save Changes</button>
                            </div>
                        </div>

                    </form>


                </div>
            </div>

        );
    }
}

const mapStateToProps = state => ({
    ...state.RecruitmentApplicantReducer.details,
    phones: state.RecruitmentApplicantReducer.phones,
    addresses: state.RecruitmentApplicantReducer.addresses,
    emails: state.RecruitmentApplicantReducer.emails,
    references: state.RecruitmentApplicantReducer.references,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getApplicantInfo: (applicant_id) => dispach(getApplicantInfo(applicant_id)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(EditApplicantInfo);

