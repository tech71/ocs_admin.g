import React, { Component } from "react";
import Select from "react-select-plus";
import "react-select-plus/dist/react-select-plus.css";
import { handleChangeSelectDatepicker, postImageData, toastMessageShow } from "service/common.js";
import jQuery from "jquery";
import { connect } from 'react-redux';
import { getApplicantAttachmentCategoryDetails, getApplicantAttachmentDetails } from './../actions/RecruitmentApplicantAction.js';
import { UPLOAD_MAX_SIZE_IN_MB, UPLOAD_MAX_SIZE_ERROR } from 'config.js';

class ApplicantAddAttachmentFrom extends Component {
  constructor() {
    super();
    this.uploadDocsInitialState = {
      selectedFile: null,
      docsTitle: "",
      docsCategory: "",
      filename: null,
      fileValue: '',
      loading: false
    }
    this.state = this.uploadDocsInitialState;

  }
  componentDidMount() {
    if (this.props.attachment_category_list.length <= 0) {
      this.props.getApplicantAttachmentCategoryDetails();
    }

  }
  fileChangedHandler = event => {
    let fileData = event.target.files.length > 0 ? event.target.files[0] : '';
    let fileSize = fileData != '' ? Math.round((fileData.size / 1024)) : '';
    let fileUpload = fileSize != '' && fileSize > (UPLOAD_MAX_SIZE_IN_MB * 1024) ? false : true;
    if (fileUpload) {
      this.setState({
        selectedFile: fileData != '' ? fileData : null,
        filename: fileData != '' ? fileData.name : null,
        fileValue: event.target.files.length > 0 ? event.target.value : ''
      });
    } else {
      this.setState({
        selectedFile: null,
        filename: null,
        fileValue: ''
      }, () => {
        toastMessageShow(UPLOAD_MAX_SIZE_ERROR, 'e');
      });

    }
    /* this.setState({
      selectedFile:  fileData!='' &&  >0 ?fileData:null,
      filename: fileData!='' ?  fileData.name:null,
      fileValue: event.target.files.length>0 ?event.target.value:''
    },()=>{
      let fSize =  event.target.files.length>0 ? event.target.files[0].size:0;
      console.log('sdd',fSize);
      let  fileSize = Math.round((fSize / 1024)); 
      if (fileSize > 5120) { 
        this.setState({
          selectedFile:  null,
          filename: null,
          fileValue: ''
        },()=>{toastMessageShow('The file you are attempting to upload is larger than the permitted size(5MB).','e')});
      }
    }); */
  };

  uploadHandler = e => {
    e.preventDefault();
    jQuery("#applicant_attachment_form").validate({ ignore: [] });
    if (jQuery("#applicant_attachment_form").valid()) {
      this.setState({ loading: true });
      const formData = new FormData();
      let currentStageData = this.props.overwrite_stage && this.props.current_stage_overwrite > 0 ? this.props.current_stage_overwrite : this.props.applicantDetailsDocPage.current_stage;
      formData.append("docsFile", this.state.selectedFile, this.state.filename);
      formData.append("applicantId", this.props.applicantDetailsDocPage.id);
      formData.append("currentStage", currentStageData);
      formData.append("stageMain", this.props.is_main_stage);
      formData.append("docsTitle", this.state.docsTitle);
      formData.append("docsCategory", this.state.docsCategory);

      postImageData(
        "recruitment/RecruitmentApplicant/upload_attachment_docs",
        formData
      )
        .then(responseData => {
          var x = responseData;
          if (responseData.status) {
            this.props.closeModel(true);
            this.setState(this.uploadDocsInitialState);
            this.props.getApplicantAttachmentDetails(this.props.applicantDetailsDocPage.id);
            let msg = (
              <span>
                File uploaded successfully. <br />
                {responseData.warn ? responseData.warn : ""}
              </span>
            );
            toastMessageShow(msg, 's');
          } else {
            toastMessageShow(x.error, 'e');
          }
          this.setState({ loading: false });
        })
        .catch(error => {
          toastMessageShow("Api Error", 'e');
          this.setState({ loading: false });
        });
    }
  };


  render() {
    return (
      <React.Fragment>
        <form
          id="applicant_attachment_form"
          method="post"
          autoComplete="off"
        >

          <div className={this.props.isModelPage ? "col-md-12 pr-5" : "col-md-6 pr-5"}>
            <label className={"bg_labs2 mr_b_20 " + (this.props.isModelPage ? "mt-3" : "")}>
              <strong>New Document Information</strong>{" "}
            </label>
            <div className="csform-group">
              <label>Document Category:</label>
                <div className="sLT_gray left left-aRRow w-70">
              <span className="required">
                  <Select
                    className="custom_select default_validation "
                    name="docsCategory"
                    simpleValue={true}
                    searchable={false}
                    clearable={false}
                    value={this.state.docsCategory}
                    onChange={e =>
                      handleChangeSelectDatepicker(this, e, "docsCategory")
                    }
                    placeholder={'Select a Category'}
                    options={this.props.attachment_category_list}
                    inputRenderer={() => (
                      <input
                        type="text"
                        className="define_input"
                        name={"docsCategory"}
                        required={"true"}
                        value={this.state.docsCategory}
                      />
                    )}
                  />
              </span>
                </div>
            </div>

            <div className="csform-group">
              <label>Doc Title:</label>
              <span className="required">
                <input
                  type="text"
                  className="csForm_control"
                  placeholder="Document Title"
                  name="docsTitle156464"
                  maxLength="60"
                  required={true}
                  onChange={e => this.setState({ docsTitle: e.target.value })}
                  value={this.state.docsTitle || ""}
                />
              </span>
            </div>

            <div className="csform-group mt-3">
              {this.state.filename != null && this.state.filename != '' ? <div className="fileAtch_box__">
                {/* <label>File Name:</label> */}
                <div className=" d-flex align-items-center">
                  <div className="w-100 text-center py-2">
                    <i className="icon icon-document3-ie fle_ic"></i>
                    <div className="fle_nme__">{this.state.filename}</div>
                  </div>
                  <i className="icon icon-close2-ie fle_close__" onClick={() => this.setState({ fileValue: '', selectedFile: null, filename: null })}></i>
                </div>
              </div> : <React.Fragment />}
            </div>

            <span className="required">
              <label className="cmn-btn1 btn btn-block atchd_btn1__">
                Upload File
                      <input className="p-hidden" multiple="false" type="file" name="special_agreement_file" value={this.state.fileValue} onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
              </label>
            </span>
          </div>

          <div className="col-sm-12 no-pad text-right bt-1 pt-3">

            <button
              type="submit"
              className="btn cmn-btn1 create_quesBtn"
              onClick={(e) => this.uploadHandler(e)}
              disabled={this.state.loading}
            >
              Apply Changes
                    </button>

            {!this.props.isModelPage ? <button
              type="button"
              className="btn cmn-btn1 create_quesBtn ml-2"
              onClick={() => this.props.closeModel(false)}
            >
              Cancel
          </button> : <React.Fragment />}

          </div>
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  applicantDetailsDocPage: { ...state.RecruitmentApplicantReducer.details },
  attachment_category_list: state.RecruitmentApplicantReducer.attachment_category_list,

})

const mapDispatchtoProps = (dispatch) => {
  return {
    getApplicantAttachmentCategoryDetails: () => dispatch(getApplicantAttachmentCategoryDetails()),
    getApplicantAttachmentDetails: (applicant_id) => dispatch(getApplicantAttachmentDetails(applicant_id))
  }
};

ApplicantAddAttachmentFrom.defaultProps = {
  closeModel: (e) => {
    console.log('default function call');
  },
  isModelPage: false,
  overwrite_stage: false,
  is_main_stage: 0,
  current_stage_overwrite: 0,
};


export default connect(mapStateToProps, mapDispatchtoProps)(ApplicantAddAttachmentFrom)

