import React, { Component } from 'react';
import Select from 'react-select-plus';

import {  postData, handleShareholderNameChange, toastMessageShow } from 'service/common.js';
import { applicantStageStatus, applicantReferenceStatusApproveDeny } from 'dropdown/recruitmentdropdown.js';
import ReactTable from "react-table";
import moment from "moment";
import './../recruit2.css';
import { connect } from 'react-redux'
import { updateApplicantStage, setApplicantInfo } from './../../actions/RecruitmentApplicantAction';
import  {AttachmentAndNotesAndCreateTask}  from './AttachmentAndNotesAndCreateTask';


class ReferenceChecksStage extends Component {

    constructor() {
        super();
        this.state = {
            references: []
        }
    }

    componentDidMount() {
        this.setState({ references: this.props.references })
    }

    componentWillReceiveProps(newProps) {
        this.setState({ references: newProps.references })
    }
    
    saveChanges = (e, detials) => {
        var req = {applicant_id: this.props.applicant_id, reference_id : detials.id, status : detials.status, relevant_note: detials.relevant_note};
        
        postData('recruitment/RecruitmentApplicant/update_applicant_reference_status_note', req).then((result) => {
            if (result.status) {
                toastMessageShow('Updated Successfully','s');
                this.props.setApplicantReferenceInfo({references: this.state.references});
            }
        });
    }
    
    

    render() {
        var columns = [
            {
                headerClassName: 'hdrCls',
                className: (this.state.activeCol === 'firstName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_1 pl-0 pr-0',
                Cell: (props) => (<div className="text_ellip_2line text-left"><strong>Reference {props.index + 1}</strong></div>)
            },
            {
                accessor: "name",
                headerClassName: 'hdrCls',
                className: (this.state.activeCol === 'name') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_2  pl-0 pr-0 ',
                Cell: (props) => (<div className="text_ellip_2line text-left">{props.value}</div>)
            },
            {
                accessor: "status",
                headerClassName: 'hdrCls',
                className: (this.state.activeCol === 'lastName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_3  pl-0 pr-0',
                Cell: (props) => (<div className="text_ellip_2line text-right">{props.value == 1 ?<spna className="Req_btn_out_1 R_bt_co_green"> Approved</spna> : <spna className="Req_btn_out_1 R_bt_co_">Rejected</spna>}</div>)
            },
            {
                Header: "Expand",
                headerClassName: 'hdrCls',
                className: (this.state.activeCol === 'lastName') && this.state.resizing ? 'defaultCellCls' : 'Refer_colum_4  pl-0 pr-0 ',
                expander: true,
                Header: () => <strong>More</strong>,
                width: 65,
                Expander: ({ isExpanded, ...rest }) => (
                    <div className="text-right">
                        {isExpanded ? (
                            <i className="icon icon-arrow-down icn_ar1"></i>
                        ) : (
                                <i className="icon icon-arrow-right icn_ar1"></i>
                            )}
                    </div>
                ),
            }
        ];

        return (
            <div className=" time_l_1" >
                <div className="time_no_div">
                    <div className="time_no">{this.props.stage}</div>
                    <div className="line_h"></div>
                </div>
                <div className="time_d_1">
                    <div className="time_d_2">

                        <div className="time_d_style">
                            <div className="Recruit_Time_header bb-1">
                                <div className="Rec_Left_s_1">
                                    <h3><strong>{this.props.title}</strong></h3>
                                    <div className="row">
                                        <div className="col-lg-6">
                                        </div>
                                    </div>
                                </div>
                                <div className="Rec_Right_s_1">
                                    <div>
                                    <div className="set_select_small req_s1">
                                            <Select name="participant_assessment"
                                                required={true}
                                                simpleValue={true}
                                                searchable={false}
                                                clearable={false}
                                                options={applicantStageStatus()}
                                                onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id, this.props.id, status)}
                                                value={this.props.stage_status}
                                                placeholder="Filter by: Unread"
                                                className={'custom_select'}
                                                disabled={this.props.stage_status == 2 ? false : true}
                                            />
                                        </div>
                                    </div>
                                    <div>
                                        <span className="Time_line_error_msg text_G_1">
                                            {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4) ? moment(this.props.action_at).format("DD/MM/YYYY LT") : ''}</span>
                                    </div>
                                </div>
                            </div>

                            <div className="">
                                <ReactTable
                                    data={this.state.references}
                                    columns={columns}
                                    defaultPageSize={5}
                                    showPagination={false}
                                    collapseOnDataChange={false}
                                    SubComponent={(props) => <div className="References_table_SubComponent">
                                        <div className="col-lg-offset-1 col-lg-10">
                                            <div className="row mt-1">
                                                <div className="col-lg-5 text-left">
                                                    <label className="My_Label_">Reference Status:</label>
                                                    <div className="s-def1 s1">
                                                        <Select name="participant_assessment"
                                                            required={true}
                                                            simpleValue={true}
                                                            searchable={false}
                                                            clearable={false}
                                                            options={applicantReferenceStatusApproveDeny()}
                                                            value={props.original.status}
                                                            className={'custom_select'}
                                                            onChange={(e) => handleShareholderNameChange(this, 'references', props.index, 'status', e)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-1">
                                                <div className="col-lg-12 col-sm-12 text-left">
                                                    <label className="My_Label_">Relevant Notes:</label>
                                                    <textarea className="w-100 border-black" rows="5" maxlenght={500} onChange={(e) => handleShareholderNameChange(this, 'references', props.index, 'relevant_note', e.target.value)}
                                                        value={props.original.relevant_note}></textarea>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-sm-12 text-right mb-3">
                                                    <button className="btn cmn-btn1" onClick={(e) => this.saveChanges(e,props.original)}>Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>}
                                    className="References_table hide_header_ReferencesTable"
                                />
                            </div>

                            <div className="d-flex flex-wrap">
                                <div className="time_txt w-100 Rerm_time_txt">
                                    {/* <div>Complete by: Smith Roy</div> */}
                                    <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    applicant_id: state.RecruitmentApplicantReducer.details.id,
    references: state.RecruitmentApplicantReducer.references,
})

const mapDispatchtoProps = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
        setApplicantReferenceInfo: (data) => dispach(setApplicantInfo(data)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(ReferenceChecksStage)