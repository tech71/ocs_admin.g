import React, { Component } from 'react';
import Select from 'react-select-plus';
import { Link } from 'react-router-dom';
import {  postData} from 'service/common.js';
import { applicantStageStatus } from 'dropdown/recruitmentdropdown.js';
import { toast } from 'react-toastify';
import { ToastUndo } from 'service/ToastUndo.js';
import { Panel,  PanelGroup } from 'react-bootstrap';
import moment from "moment";
import './../recruit2.css';
import { updateApplicantStage, getApplicantMainStageDetails, getApplicantStageWiseDetails } from './../../actions/RecruitmentApplicantAction';
import RescheduleGroupOrCabInterview from './../../action_task/RescheduleGroupOrCabInterview';
import { connect } from 'react-redux'
import { AttachmentAndNotesAndCreateTask } from './AttachmentAndNotesAndCreateTask';

class ScheduleInterviewStage extends Component {

    constructor() {
        super();
        this.state = {
            stage_details: []
        }
    }

    componentDidMount() {
    }

    closeRescheduleModel = (status) => {
        this.setState({ openReschedule: false });
        if (status) {
            this.props.getApplicantStageWiseDetails(true, this.props.stage_number, this.props.applicant_id);
        }
    }

    render() {
        //console.log(this.props);
        return (<React.Fragment><div className="time_l_1" >
            <div className="time_no_div">
                <div className="time_no">3.1</div>
                <div className="line_h"></div>
            </div>
            <div className="time_d_1">
                <div className="time_d_2">

                    <div className="time_d_style">
                        <div className="Recruit_Time_header bb-1 min-height">
                            <div className="Rec_Left_s_1">
                                <h3><strong>Stage {this.props.stage}</strong></h3>
                                <h2>{this.props.title}</h2>

                                <div className="row">
                                    <div className="col-lg-6">

                                        <div className="cmn_select_dv mg_slct1  slct_des23 slct_s1">
                                            <button disabled={(typeof(this.props.applicant_email_status) === 'undefined' && this.props.applicant_status == 1) ? false : true} onClick={() => this.setState({ openReschedule: true })} className="limt_btns">Select Interivew  <i className="icon icon-edit5-ie ml-2"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="Rec_Right_s_1">
                                <div>
                                <div className="set_select_small req_s1">
                                        <Select name="participant_assessment"
                                            onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id, this.props.id, status)}
                                            simpleValue={true}
                                            searchable={false}
                                            clearable={false}
                                            options={applicantStageStatus()}
                                            value={this.props.stage_status}
                                            className={'custom_select'}
                                            disabled={this.props.stage_status == 2 && (this.props.applicant_email_status == 0 || this.props.applicant_email_status == 1) ? false : true}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <span className="Time_line_error_msg text_G_1">
                                        {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4) ? moment(this.props.action_at).format("DD/MM/YYYY LT") : ''}</span>
                                </div>
                            </div>
                        </div>


                        <PanelGroup
                            accordion
                            id="accordion-controlled-example"
                            activeKey={this.state.activeKey}
                            onSelect={this.handleSelect}
                        >
                            <Panel className="Time_line_panel_Main_0" activeKey={true}>
                                <Panel.Heading className="px-0 py-0 Time_line_panel_heading_0">
                                    <Panel.Title toggle >
                                        <div className="Stage_body_01">
                                            <div className="Stage_Left_1"><strong>Interview Details:</strong></div>
                                            <div className="Stage_Left_2"><a className="O_btn_1 txt_success">Invitation Send - {(this.props.invitation_send_at) ? <React.Fragment> {moment(this.props.invitation_send_at).format('DD/MM/YYYY LT')}</React.Fragment> : 'N/A'}</a></div>
                                            <i className="icon icon-arrow-left d-none"></i>
                                            <i className="icon icon-arrow-down  d-none"></i>
                                        </div>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible className="Time_line_panel_body_0">
                                    <div className="Stage_body_01">
                                        <div className="Stage_Left_1"><strong>Date:</strong> {this.props.start_datetime ? moment(this.props.start_datetime).format('DD/MM/YYYY') : 'N/A'}</div>
                                    </div>
                                    <div className="Stage_body_01">
                                        <div className="Stage_Left_1"><strong>Recruiter in Charge:</strong> {this.props.recruiter_in_charge || "N/A"}</div>
                                    </div>
                                    <div className="Stage_body_01">
                                        <div className="Stage_Left_1"><strong>Location:</strong> {this.props.training_location ? this.props.training_location : 'N/A'}</div>
                                    </div>
                                </Panel.Body>
                            </Panel>

                            {this.props.history_group_interview.map((val, index) => (
                                <Panel eventKey={index} key={index} className="Time_line_panel_Main_0">
                                    <Panel.Heading className="px-0 py-0 Time_line_panel_heading_0">
                                        <Panel.Title toggle>
                                            <div className="Stage_body_01">
                                                <div className="Stage_Left_1 w-40"><strong>Interview Details:</strong></div>
                                                <div className="Stage_Left_2"><span className="O_btn_1 txt_infor">{val.mark_as_no_show == 1? "No show": "Declined"} - {(val.manual_order) ? <React.Fragment> {moment(val.manual_order).format('DD/MM/YYYY')}</React.Fragment> : 'N/A'}</span></div>
                                                <i className="icon icon-arrow-left d-none"></i>
                                                <i className="icon icon-arrow-down  d-none"></i>
                                            </div>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible className="Time_line_panel_body_0">
                                        <div className="Stage_body_01">
                                            <div className="Stage_Left_1"><strong>Date:</strong> {val.start_datetime ? moment(val.start_datetime).format('DD/MM/YYYY') : 'N/A'}</div>
                                        </div>
                                        <div className="Stage_body_01">
                                            <div className="Stage_Left_1"><strong>Recruiter in Charge:</strong> {val.recruiter_in_charge || "N/A"}</div>
                                        </div>
                                        <div className="Stage_body_01">
                                            <div className="Stage_Left_1"><strong>Location:</strong> {val.training_location ? val.training_location : 'N/A'}</div>
                                        </div>
                                        <div className="d-flex mt-3">
                                        <div className="w-40">
                                            <div className="Stage_body_01">
                                                <div className="Stage_Left_1"><strong>Invitation Sent:</strong></div>
                                            </div>
                                            <div className="Stage_body_01">
                                                <div className="Stage_Left_1">{(val.invitation_send_at) ? <React.Fragment> {moment(val.invitation_send_at).format('DD/MM/YYYY')}</React.Fragment> : 'N/A'}</div>
                                            </div>
                                        </div>
                                        <div className="w-40">
                                            <div className="Stage_body_01">
                                                <div className="Stage_Left_1"><strong>Applicant Response:</strong></div>
                                            </div>
                                            <div className="Stage_body_01">
                                                <div className="Stage_Left_1">
                                                    {(val.applicant_email_status == 1 || val.applicant_email_status == 2) ?
                                                        <React.Fragment>{val.applicant_email_status == 1 ?
                                                           <React.Fragment>  <span className="O_btn_1 txt_success">Accepted - {moment(val.invitation_accepted_at).format('DD/MM/YYYY')}</span></React.Fragment> :
                                                            <React.Fragment><span className="O_btn_1 txt_infor">Declined - {moment(val.invitation_cancel_at).format('DD/MM/YYYY')}</span> </React.Fragment>}</React.Fragment>
                                                        : ((val.applicant_email_status == 0) ? 'Pending' : 'N/A')}</div>
                                            </div>
                                        </div>
                                        </div>
                                    </Panel.Body>
                                </Panel>
                            ))}
                        </PanelGroup>

                        <div className="d-flex flex-wrap">
                            <div className="time_txt w-100 Rerm_time_txt">
                            <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            {this.state.openReschedule ? <RescheduleGroupOrCabInterview task_stage={3} showModal={this.state.openReschedule} closeModal={this.closeRescheduleModel} applicant_id={this.props.applicant_id} /> : ''}

        </React.Fragment>);

    }
}

const mapStateToProps = state => ({
    applicant_id: state.RecruitmentApplicantReducer.details.id,
    history_group_interview: state.RecruitmentApplicantReducer.history_group_interview,
    ...state.RecruitmentApplicantReducer.group_interview,
    applicant_status: state.RecruitmentApplicantReducer.details.status,
})

const mapDispatchtoProps = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
        getApplicantMainStageDetails: (applicant_id) => dispach(getApplicantMainStageDetails(applicant_id)),
        getApplicantStageWiseDetails: (its_open, stage_number, applicant_id) => dispach(getApplicantStageWiseDetails(its_open, stage_number, applicant_id)),
    }
};


ScheduleInterviewStage = connect(mapStateToProps, mapDispatchtoProps)(ScheduleInterviewStage)
export { ScheduleInterviewStage };

class ApplicantResponseGroupStage extends Component {

    constructor() {
        super();
        this.state = {

        }
    }

    closeRescheduleModel = (status) => {
        this.setState({ openReschedule: false });
        if (status) {
            this.props.getApplicantStageWiseDetails(true, this.props.stage_number, this.props.applicant_id);
        }
    }

    reSendMail = (e, taskId) => {
        e.preventDefault();
        var state = {};
        state['email_loading'] = true;
        state['email_sended'] = false;
        this.setState(state);

        postData('recruitment/RecruitmentTaskAction/resend_task_mail_to_applicant', { taskId: taskId, applicant_id: this.props.applicant_id }).then((result) => {
            if (result.status) {
                state['email_sended'] = true;
            }else{
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
            state['email_loading'] = false;
            this.setState(state);
        });
    }

    componentDidMount() {
    }

    render() {
        var i = 0;
        return (<React.Fragment><div className=" time_l_1" >
            <div className="time_no_div">
                <div className="time_no">{this.props.stage}</div>
                <div className="line_h"></div>
            </div>
            <div className="time_d_1">
                <div className="time_d_2">

                    <div className="time_d_style">
                        <div className="Recruit_Time_header bb-1 min-height">
                            <div className="Rec_Left_s_1">
                                <h3><strong>Stage {this.props.stage}</strong></h3>
                                <h2>{this.props.title}</h2>

                                <div className="row">
                                    <div className="col-lg-6">

                                    </div>
                                </div>
                            </div>
                            <div className="Rec_Right_s_1">
                                <div>
                                <div className="set_select_small req_s1">
                                        <Select name="participant_assessment"
                                            onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id, this.props.id, status)}
                                            simpleValue={true}
                                            searchable={false}
                                            clearable={false}
                                            options={applicantStageStatus()}
                                            value={this.props.stage_status}
                                            className={'custom_select'}
                                            disabled={this.props.stage_status == 2 && this.props.applicant_email_status == 1 ? false : true}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <span className="Time_line_error_msg text_G_1">
                                        {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4) ? moment(this.props.action_at).format("DD/MM/YYYY LT") : ''}</span>
                                </div>
                            </div>
                        </div>


                        <div className="limt_flex_set_0 mt-2">
                            <div>
                                <div className="Stage_body_01">
                                    <div className="Stage_Left_1"><strong>Invitation Sent:</strong></div>
                                </div>
                                <div className="Stage_body_01">
                                    <div className="Stage_Left_1">{(this.props.invitation_send_at) ? <React.Fragment> {moment(this.props.invitation_send_at).format('DD/MM/YYYY')}</React.Fragment> : 'N/A'}</div>
                                </div>
                            </div>
                            <div>
                                <div className="Stage_body_01">
                                    <div className="Stage_Left_1"><strong>Applicant Response:</strong></div>
                                </div>
                                <div className="Stage_body_01">
                                    <div className="Stage_Left_1"><a className="O_btn_1">
                                        {(this.props.applicant_email_status == 1 || this.props.applicant_email_status == 2) ?
                                            <React.Fragment>{this.props.applicant_email_status == 1 ?
                                                <React.Fragment> Accepted - {moment(this.props.invitation_accepted_at).format('DD/MM/YYYY')}</React.Fragment> :
                                                <React.Fragment>Declined - {moment(this.props.invitation_cancel_at).format('DD/MM/YYYY')} </React.Fragment>}</React.Fragment>
                                            : ((this.props.applicant_email_status == 0) ? 'Pending' : 'N/A')}</a></div>
                                </div>
                            </div>
                            <div>
                                <div className="mb-2 resend_color "><button disabled={(this.props.applicant_email_status == 0) ? false : true} onClick={(e) => this.reSendMail(e, this.props.taskId)} className="limt_btns w-z w1 ">Resend Invite</button>
                                    {this.state.email_loading ? <i className="ie ie-loading my_Spin"></i> : ''}
                                    {this.state.email_sended ? <i className="icon icon-approved2-ie"></i> : ''}
                                </div>
                                <div className="my-2"><button disabled={(this.props.applicant_email_status == 2 && this.props.applicant_status == 1) ? false : true} onClick={() => this.setState({ openReschedule: true })} className="limt_btns">Reschedule</button></div>
                            </div>
                        </div>


                        <div className="d-flex flex-wrap">
                            <div className="time_txt w-100 Rerm_time_txt">
                                {/* <div>Complete by: Smith Roy</div> */}
                                <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

            {this.state.openReschedule ? <RescheduleGroupOrCabInterview task_stage={3} showModal={this.state.openReschedule} closeModal={this.closeRescheduleModel} applicant_id={this.props.applicant_id} /> : ''}

        </React.Fragment>);
    }
}

const mapStateToProps1 = state => ({
    applicant_id: state.RecruitmentApplicantReducer.details.id,
    ...state.RecruitmentApplicantReducer.group_interview,
    applicant_status: state.RecruitmentApplicantReducer.details.status,
})

const mapDispatchtoProps1 = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
        getApplicantMainStageDetails: (applicant_id) => dispach(getApplicantMainStageDetails(applicant_id)),
        getApplicantStageWiseDetails: (its_open, stage_number, applicant_id) => dispach(getApplicantStageWiseDetails(its_open, stage_number, applicant_id)),
    }
};


ApplicantResponseGroupStage = connect(mapStateToProps1, mapDispatchtoProps1)(ApplicantResponseGroupStage)
export { ApplicantResponseGroupStage };

class GroupInterviewResultStage extends Component {

    constructor() {
        super();
        this.state = {
            stage_details: []
        }
    }
    
    closeRescheduleModel = (status) => {
        this.setState({ openReschedule: false });
        if (status) {
            this.props.getApplicantStageWiseDetails(true, this.props.stage_number, this.props.applicant_id);
        }
    }

    componentDidMount() {
    }

    render() {
    
        return (<div className=" time_l_1" >
            <div className="time_no_div">
                <div className="time_no">{this.props.stage}</div>
                <div className="line_h"></div>
            </div>
            <div className="time_d_1">
                <div className="time_d_2">

                    <div className="time_d_style">
                        <div className="Recruit_Time_header bb-1 min-height">
                            <div className="Rec_Left_s_1">
                                <h3><strong>Stage {this.props.stage}</strong></h3>
                                <h2>{this.props.title}</h2>

                                <div className="row">
                                    <div className="col-lg-6">
                                    </div>
                                </div>

                            </div>
                            <div className="Rec_Right_s_1">
                                <div>
                                <div className="set_select_small req_s1">
                                        <Select name="participant_assessment"
                                            onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id, this.props.id, status)}
                                            simpleValue={true}
                                            searchable={false}
                                            clearable={false}
                                            options={applicantStageStatus('', this.props.applicant_result, true)}
                                            value={this.props.stage_status}
                                            className={'custom_select'}
                                            disabled={(this.props.stage_status == 2 && (this.props.applicant_result == 1 || this.props.applicant_result == 2))? false : true}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <span className="Time_line_error_msg text_G_1">
                                        {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4) ? moment(this.props.action_at).format("DD/MM/YYYY LT") : ''}</span>
                                </div>
                            </div>
                        </div>

                        <div className="row d-flex justify-content-center">
                            <div className="col-lg-12 Rec_center_s_1a mt-2 mb-1"><strong>Group Interview Results:</strong></div>
                        </div>
                        <div className="row d-flex justify-content-center">
                            <div>
                                
                                {(this.props.applicant_result == 0 || !this.props.taskId)? <span className="O_btn_1 w-big my-1 txt_infor">Pending</span>: 
                                        ((this.props.applicant_result == 1)? <span className="O_btn_1 w-big my-1 txt_success">Applicant Successfull</span>:  
                                        <React.Fragment> <span className="O_btn_1 w-big my-1 txt_infor">Applicant Unsuccessful {(this.props.mark_as_no_show == 1)? <React.Fragment><br/>(Marked as No show)</React.Fragment>: ''}</span></React.Fragment>)}<br />
                                
                                <div className="text-center">
                                {this.props.mark_as_no_show == 1? <button disabled={(this.props.status == 1) ? false : true} onClick={() => this.setState({ openReschedule: true })} className="limt_btns">Reschedule</button>
                                    : <Link disabled={this.props.applicant_result? false: true} to={'/admin/recruitment/group_interview_result/'+this.props.applicant_id+'/'+this.props.taskId} className="under_l_tx">View Results</Link>}
                                </div>
                            </div>
                        </div>

                        <div className="d-flex flex-wrap">
                            <div className="time_txt w-100 Rerm_time_txt">
                                <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                            </div>
                        </div>

                    </div>

                    {this.state.openReschedule ? <RescheduleGroupOrCabInterview task_stage={3} showModal={this.state.openReschedule} closeModal={this.closeRescheduleModel} applicant_id={this.props.applicant_id} /> : ''}
                            
                </div>
            </div>
        </div>);
    }
}

const mapStateToProps2 = state => ({
    applicant_id: state.RecruitmentApplicantReducer.details.id,
    status: state.RecruitmentApplicantReducer.details.status,
            ...state.RecruitmentApplicantReducer.group_interview,
})

const mapDispatchtoProps2 = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
        getApplicantMainStageDetails: (applicant_id) => dispach(getApplicantMainStageDetails(applicant_id)),
        getApplicantStageWiseDetails: (its_open, stage_number, applicant_id) => dispach(getApplicantStageWiseDetails(its_open, stage_number, applicant_id)),
    }
};


GroupInterviewResultStage = connect(mapStateToProps2, mapDispatchtoProps2)(GroupInterviewResultStage)
export { GroupInterviewResultStage };