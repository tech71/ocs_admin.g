import React, { Component } from 'react';
import Select from 'react-select-plus';
import {  postData, toastMessageShow } from 'service/common.js';
import { applicantStageStatus, phoneInterviwApplicantClassification } from 'dropdown/recruitmentdropdown.js';
import { connect } from 'react-redux'
import moment from "moment";
import './../recruit2.css';
import { updateApplicantStage, getApplicantMainStageDetails, getApplicantStageWiseDetails } from './../../actions/RecruitmentApplicantAction';
import  {AttachmentAndNotesAndCreateTask}  from './AttachmentAndNotesAndCreateTask';



class PhoneInterviewStage extends Component {

    constructor() {
        super();
        this.state = {
        }
    }

    updateClassfication = (classificaiton) => {
        var req = {classificaiton: classificaiton, applicant_id: this.props.applicant_id};
        postData('recruitment/RecruitmentApplicant/update_applicant_phone_interview_classification', req).then((result) => {
            if (result.status) {
               // this.props.getApplicantMainStageDetails(this.props.applicant_id);
                this.props.getApplicantStageWiseDetails(true, this.props.stage_number, this.props.applicant_id);
            }else{
                toastMessageShow(result.error,'e')
            }
        });
    }

    render() {
        
        return (
                <div className=" time_l_1" >
                    <div className="time_no_div">
                        <div className="time_no">{this.props.stage}</div>
                        <div className="line_h"></div>
                    </div>
                    <div className="time_d_1">
                        <div className="time_d_2">

                            <div className="time_d_style">
                                <div className="Recruit_Time_header bb-1">
                                    <div className="Rec_Left_s_1">
                                        <h3><strong>Stage {this.props.stage}</strong></h3>
                                        <h2>{this.props.title}</h2>

                                        <div className="row">
                                            <div className="col-lg-6">
                                                <button phone={this.props.phone} className="btn cmn-btn1 phone-btn">Call Application</button>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="Rec_Right_s_1">
                                        <div>
                                        <div className="set_select_small req_s1">
                                                <Select name="participant_assessment"
                                                    required={true}
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={applicantStageStatus()}
                                                    onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id,this.props.id,status)}
                                                    value={this.props.stage_status}
                                                    placeholder="Filter by: Unread"
                                                    className={'custom_select'} 
                                                    disabled={this.props.stage_status == 2 && this.props.phone_interview_classification > 0? false: true}
                                                    />
                                            </div>
                                        </div>
                                        <div>
                                            <span className="Time_line_error_msg text_G_1">
                                            {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4)? moment(this.props.action_at).format("DD/MM/YYYY LT"): ''}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="row d-flex justify-content-center">
                                    <div className="col-lg-12 Rec_center_s_1a mt-2 mb-1"><strong>Applicant Classification:</strong></div>
                                </div>

                                <div className="row d-flex justify-content-center">
                                    <div className="col-lg-4 col-sm-4 px-0">
                                        <div className="s-def1 s1">
                                            <Select name="participant_assessment"
                                                simpleValue={true}
                                                searchable={false}
                                                clearable={false}
                                                onChange={(status) => this.updateClassfication(status)}
                                                options={phoneInterviwApplicantClassification()}
                                                value={this.props.phone_interview_classification}
                                                placeholder="Applicant Classification"
                                                className={'custom_select'}
                                                disabled={this.props.stage_status == 2 && !this.props.phone_interview_classification? false : true}
                                                />
                                        </div>
                                    </div>
                                </div>

                                <div className="d-flex flex-wrap">
                                    <div className="time_txt w-100 Rerm_time_txt">
                                        {/* <div>Complete by: Smith Roy</div> */}
                                       {/*  <ul className="Time_subTasks_Action__ ">
                                            <li><span className="sbTsk_li" {...attachmentsAttributes}>Attachments & Notes</span></li>
                                            <li><span className="sbTsk_li" {...createTaskAttributes}>Create Task</span></li>
                                        </ul> */}
                                        <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
                );
    }
}

const mapStateToProps = state => ({
    applicant_id :state.RecruitmentApplicantReducer.details.id,
    phone_interview_classification :state.RecruitmentApplicantReducer.phone_interview_classification,
})

const mapDispatchtoProps = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
        getApplicantMainStageDetails: (applicant_id) => dispach(getApplicantMainStageDetails(applicant_id)),
        getApplicantStageWiseDetails: (its_open, stage_number, applicant_id) => dispach(getApplicantStageWiseDetails(its_open, stage_number, applicant_id)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(PhoneInterviewStage)