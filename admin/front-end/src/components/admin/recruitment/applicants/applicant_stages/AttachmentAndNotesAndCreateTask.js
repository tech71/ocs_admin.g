import React, { Component } from 'react';
   
export const AttachmentAndNotesAndCreateTask = (props)=>{
    let attachmentsAttributes = {};
    if(props.stageDetailsAttachmentANdNotes.stage_status !=1){
        attachmentsAttributes['onClick'] = ()=>props.openAttachmentModel(props.stageDetailsAttachmentANdNotes.stage_number,props.stageDetailsAttachmentANdNotes.is_main_stage,props.stageDetailsAttachmentANdNotes.stageId);
    }
    let createTaskAttributes = {};
    if(props.stageDetailsAttachmentANdNotes.stage_status ==2){
        createTaskAttributes['onClick'] = () => props.CreateTaskShowModal(props.stageDetailsAttachmentANdNotes.task_stage_number);
    }

    return (
        <React.Fragment>
        
                <ul className="Time_subTasks_Action__ ">
                    <li><span className="sbTsk_li" {...attachmentsAttributes}>Attachments & Notes </span></li>
                    <li><span className="sbTsk_li" {...createTaskAttributes}>Create Task </span></li>
                </ul>
    

    </React.Fragment>
    );
}

AttachmentAndNotesAndCreateTask.defaultProps ={
    
    stageDetailsAttachmentANdNotes:{
        overwrite_stage:false,
        stageId:0,
        is_main_stage:false,
        stage_number:0,
        task_stage_number:0,
        stage_status:0
    }
};