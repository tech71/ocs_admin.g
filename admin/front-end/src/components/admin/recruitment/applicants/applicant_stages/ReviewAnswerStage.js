import React, { Component } from 'react';
import Select from 'react-select-plus';
import { applicantStageStatus } from 'dropdown/recruitmentdropdown.js';
import SeekWebsiteAnswerModel from './../SeekWebsiteAnswerModel'
import { connect } from 'react-redux'
import { Tabs, Tab } from 'react-bootstrap';
import moment from "moment";
import './../recruit2.css';
import { updateApplicantStage } from './../../actions/RecruitmentApplicantAction';
import  {AttachmentAndNotesAndCreateTask}  from './AttachmentAndNotesAndCreateTask';

class StageReviewAnswer extends Component {

    constructor() {
        super();
        this.state = {
            seek_answer_model: false,
            answerType: "",
        }
    }
    
    closeSeekAnswerModel = () => {
        this.setState({seek_answer_model: false, answerType: ''})
    }

    render() {
        return (<React.Fragment>
                <div className=" time_l_1" >
                    <div className="time_no_div">
                        <div className="time_no">{this.props.stage}</div>
                        <div className="line_h"></div>
                    </div>
                    <div className="time_d_1">
                        <div className="time_d_2">
                            <div className="time_d_style">
                                <div className="Recruit_Time_header">
                                    <div className="Rec_Left_s_1">
                                        <h3><strong>{this.props.title}</strong></h3>

                                        <div className="row">
                                            <div className="col-lg-6">
                                            </div>
                                        </div>

                                    </div>
                                    <div className="Rec_Right_s_1">
                                        <div>
                                            <div className="set_select_small req_s1">
                                                <Select name="participant_assessment"
                                                    simpleValue={true}
                                                    searchable={false}
                                                    clearable={false}
                                                    options={applicantStageStatus()}
                                                    value={this.props.stage_status}
                                                    onChange={(status) => this.props.updateApplicantStage(this.props.applicant_id,this.props.id,status)}
                                                    className={'custom_select'} 
                                                    disabled={this.props.stage_status == 2 && this.props.applicant_status == 1? false: true}
                                                    />
                                            </div>
                                        </div>
                                        <div>
                                            <span className="Time_line_error_msg text_G_1">
                                            {applicantStageStatus(this.props.stage_status)} {(this.props.stage_status == 3 || this.props.stage_status == 4)? 
                                            <React.Fragment>{moment(this.props.action_at).format("DD/MM/YYYY LT")} </React.Fragment>: ''}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div style={{ marginTop: "-25px" }}>
                                    <div className="Line_base_tabs">
                                        <Tabs defaultActiveKey={this.props.seek.length > 0? "SeekAnswers": "WebsiteAnswers"} id="uncontrolled-tab-example">
                                            {this.props.seek.length > 0?
                                            <Tab eventKey="SeekAnswers" title="Seek Answers">
                                                <div className="Seek_Q_ul mt-1">
                                                    {this.props.seek.map((val, index) => (   
                                                    <div className="Seek_Q_li w-100" key={index + 1}><span><i className={"icon "+ ((val.answer_status == 1)? "icon-accept-approve1-ie" : "icon-cross-icons")}></i></span>
                                                        <div>{val.question}</div>
                                                    </div>
                                                    ))}
                                                </div>
                                                <div className="text-left"><a className="under_l_tx" onClick={() => this.setState({seek_answer_model: true, answerType: "SEEK"})}>View all Answers</a></div>
                                            </Tab>: ''}
                                            
                                            {this.props.website.length > 0?
                                            <Tab eventKey="WebsiteAnswers" title="Website Answers">
                                                <div className="Seek_Q_ul  mt-1">
                                                    {this.props.website.map((val, index) => (   
                                                    <div key={index + 1} className="Seek_Q_li w-100"><span><i className={"icon "+ ((val.answer_status == 1)? "icon-accept-approve1-ie" : "icon-cross-icons")}></i></span>
                                                        <div>{val.question}</div>
                                                    </div>
                                                    ))}
                                                </div>
                                                <div className="text-left"><a className="under_l_tx" onClick={() => this.setState({seek_answer_model: true, answerType: "Website"})}>View all Answers</a></div>
                                            </Tab>: ''}
                                        </Tabs>
                                    </div>
                                </div>


                                <div className="d-flex flex-wrap">
                                    <div className="time_txt w-100 Rerm_time_txt">
                                        <div>Complete by: {this.props.action_username || 'N/A'}</div>
                                       {/*  <ul className="Time_subTasks_Action__ ">
                                            <li><span className="sbTsk_li" {...attachmentsAttributes}>Attachments & Notes</span></li>
                                            <li><span className="sbTsk_li" {...createTaskAttributes}>Create Task</span></li>
                                        </ul> */}
                                        <AttachmentAndNotesAndCreateTask stageDetailsAttachmentANdNotes={this.props.stageDetailsAttachmentANdNotes} CreateTaskShowModal={this.props.CreateTaskShowModal} openAttachmentModel={this.props.openAttachmentModel} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.seek_answer_model ? <SeekWebsiteAnswerModel answerType={this.state.answerType} closeModel={this.closeSeekAnswerModel} openModel={this.state.seek_answer_model}/>: ''}
                </div>
                
           
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    ...state.RecruitmentApplicantReducer.question_answer,
    applicant_id :state.RecruitmentApplicantReducer.details.id,
    applicant_status :state.RecruitmentApplicantReducer.details.status,
})

const mapDispatchtoProps = (dispach) => {
    return {
        updateApplicantStage: (applicant_id, stageId, status) => dispach(updateApplicantStage(applicant_id, stageId, status)),
    }
};


export default connect(mapStateToProps, mapDispatchtoProps)(StageReviewAnswer)