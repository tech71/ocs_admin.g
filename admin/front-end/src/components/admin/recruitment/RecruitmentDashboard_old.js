import React, { Component } from 'react';
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import { checkLoginWithReturnTrueFalse,getPermission } from 'service/common.js';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from '../../../config.js';
import Pagination from "../../../service/Pagination.js";
import { connect } from 'react-redux'
import RecruitmentPage from 'components/admin/recruitment/RecruitmentPage';
import ScrollArea from 'react-scrollbar';

import DashboardAdminView from './dashboard/DashboardAdminView';
import DashboardPersonalView from './dashboard/DashboardPersonalView';

class Dashboard extends Component {

    constructor() {
        super();
        this.state = {
            searchVal: '',
            filterVal: '',
        }
        this.permission = (checkLoginWithReturnTrueFalse())?((getPermission() == undefined)? [] : JSON.parse(getPermission())):[];
    }

    handleChangeState=(givenState,otherState)=>{
        var temp = {};
        temp[givenState] = true;
        temp[otherState] = false;
        this.setState(temp,()=>{
        });
    }

    componentDidMount() {
        if(this.permission.access_recruitment_admin == 1)
            this.handleChangeState('admin_view','personal_view');
        else
            this.handleChangeState('personal_view','admin_view');
    }

    render() {
         var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];
const dashboardData = [
            {
                Action: 'Task',
                DueDate: '01/02/03',
                Assigned: 'Jimmy',
                LastAction: '01/02/03 - by Jimmy',
                Status: 'Pending'
            },
            {
                Action: 'To-Do',
                DueDate: '01/02/03',
                Assigned: 'Tony',
                LastAction: '01/02/03 - by Eric ',
                Status: 'Actioned'
            },
            {
                Action: 'Schedule',
                DueDate: '01/02/03',
                Assigned: 'Jessica',
                LastAction: '01/02/03 - by Jessica',
                Status: 'Pending'
            },
        ]

        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#2082ac', '#39a4d1', '#63bae9'],

            }],
        };
        return (
            <React.Fragment>

                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        {/* <Link to={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></Link> */}
                    </div>
                </div>
              
        
                <div className="row pt-5">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1> {this.props.showPageTitle}
                           
                            <Link to='./user_management'>
                                <button className="btn hdng_btn cmn-btn1 icn_btn12">Create New Job <i className="icon icon-add-icons hdng_btIc"></i></button>
                            </Link>
                        </h1>
                    </div>
                </div>
               
                <div className="row status_row-- pt-3 after_before_remove">


                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Prospective Applicants:</h4>
                                
                                <div className="col-lg-12 col-md-12 col-sm-12 colJ-1">
                                
                                    <div className="num_stats1">
                                        <span>258</span>
                                    </div>

                                    <div className="duly_vw">
                                        <div className="viewBy_dc text-center">
                                            <h5>View By:</h5>
                                            <ul>
                                                <li>Week</li>
                                                <li>Month</li>
                                                <li>Year</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Applicants Hired:</h4>
                                
                                <div className="col-lg-12 col-md-12 col-sm-12 colJ-1">
                                    <div className="num_stats1">
                                        <span>151</span>
                                    </div>

                                    <div className="duly_vw">
                                        <div className="viewBy_dc text-center">
                                            <h5>View By:</h5>
                                            <ul>
                                                <li>Week</li>
                                                <li>Month</li>
                                                <li>Year</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Recruitment Status:</h4>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <div className='grph_dv'>
                                        <Bar data={Recruitmentdata}  style={{width:'100%'}} height={180} legend={null}
                                            options={{
                                                maintainAspectRatio: false
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <ul className="status_det_list">
                                        <li className="drk-color4">New = 250</li>
                                        <li className="drk-color2">In Progress = 333</li>
                                        <li className="drk-color3">Unsuccessful = 250</li>
                                    </ul>
                                    {/* <div className="duly_vw"> */}
                                    <div className="viewBy_dc text-center">
                                        <h5>View By:</h5>
                                        <ul>
                                            <li>Week</li>
                                            <li>Month</li>
                                            <li>Year</li>
                                        </ul>
                                    </div>
                                    {/* </div> */}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row sort_row1-- after_before_remove" style={{ display: 'none' }}>
                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_l srchCol_r">
                        <div className="search_bar left">
                            <input type="text" className="srch-inp" placeholder="Search.." />
                            <i className="icon icon-search2-ie"></i>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_r filCol_l">
                        <div className="filter_flx">
                            <div className="filter_fields__ cmn_select_dv">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Search by: All"
                                    options={options}
                                    onChange={(e) => this.setState({ searchVal: e })}
                                    value={this.state.searchVal}
                                />

                            </div>

                            <div className="filter_fields__ cmn_select_dv pd_r_0_De">
                                <Select name="view_by_status "
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}
                                />
                            </div>
                        </div>

                    </div>

                </div>
                

                <div className="row data_table_row1" style={{ display: 'none' }}>
                    <div className="col-lg-12 no-pad">
                        <div className="data_table_cmn dashboard_Table ">
                            <ReactTable
                                PaginationComponent={Pagination}
                                data={dashboardData}
                                defaultPageSize={dashboardData.length}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                columns={[
                                    { Header: "Action Type", accessor: 'Action' },
                                    { Header: "Due Date", accessor: 'DueDate' },
                                    { Header: "Assigned to", accessor: 'Assigned' },
                                    { Header: "Last Actioned", accessor: 'LastAction' },
                                    { Header: "Status", accessor: 'Status' }
                                ]}
                            />
                        </div>
                    </div>
                </div>


                <div className="row">
                    <ul className="nav nav-tabs Category_tap Nav_ui__ col-lg-10  col-sm-12 P_20_TB">
                        
                        <li className={(this.state.personal_view)?'active col-lg-4 col-sm-4':'col-lg-4 col-sm-4'}>
                            <a href="#1a" data-toggle="tab" onClick={(e)=>this.handleChangeState('personal_view','admin_view')}>Personal View</a>
                        </li>

                        <li className={(this.state.admin_view)?'active col-lg-4 col-sm-4':'col-lg-4 col-sm-4'}>
                        <a onClick={(e)=>this.handleChangeState('admin_view','personal_view')}>Admin View</a>
                        </li>
                    </ul>
                </div>

                {(this.state.admin_view)?<DashboardAdminView />:'' }
                     
                {(this.state.personal_view)?<DashboardPersonalView />:'' }
                          
                
            </React.Fragment>

        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);