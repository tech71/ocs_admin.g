import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";
import Select from 'react-select-plus';
import { postData,handleShareholderNameChange } from 'service/common.js';
import DatePicker from "react-datepicker";
import { recruitmentLocation,phoneInterviwApplicantClassification,applicantQuizResultStatus } from 'dropdown/recruitmentdropdown.js';
import moment from 'moment';
import FlagApplicantModal from '../../applicants/FlagApplicantModal';
import { ROUTER_PATH } from 'config.js';
import {defaultSpaceInTable} from 'service/custom_value_data.js';

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('recruitment/RecruitmentGroupInterview/get_group_interview_list', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count: result.all_count,
            };
            resolve(res);
        });
    });
};

class ManageGroupInterview extends Component {
    constructor() {
        super();
        this.state = {
            interViewList:[],
            flagallicantpopup: false,
        }
    }

    componentDidMount() { 
        document.title = "Manage Group Interview"
    }

    closeFlagApplicantPopUp =()=>{
        this.setState({ flagallicantpopup: false })
    }

    fetchGroupInterview = (state, instance) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
        ).then(res => {
            this.setState({
                interViewList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    handleChangeCurrentState = ()=> {
        this.setState({flagallicantpopup:true});
    }

    render() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];

        var options2 = [
            { value: 'successful', label: 'Successful' },
            { value: 'pending', label: 'Pending' }
        ];

        
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        <Link to={ROUTER_PATH+'admin/recruitment/dashboard'} className="icon icon-back1-ie"></Link>
                    </div>
                </div>
               
                <div className="row">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1>Manage Group Interview</h1>
                    </div>
                </div>
                
                <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-7 col-sm-7 no_pd_l">
                        <div className=" cmn_select_dv srch_select12  vldtn_slct">

                            <Select
                                cache={false}
                                name="form-field-name"
                                clearable={false}
                                value={this.state.job_location}
                                placeholder='Search'
                                onChange={(e) => this.setState({ job_location: e })}
                                required={true}
                            />

                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-sm-4 no_pd_r">
                        <div className="filter_flx">

                            <div className="filter_fields__ cmn_select_dv gr_slctB sl_center">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}
                                />
                            </div>

                        </div>
                    </div>
                </div>
                
                <div className="row">
                    <div className="col-sm-12">
                        <div className="data_table_cmn dataTab_accrdn_cmn tbl_flx2 ">
                            <ReactTable
                                columns={[
                                    {
                                        Header: "task_name:",
                                        accessor: "task_name",
                                        // width: 200,
                                        Cell: (props) => (
                                            <div className="d_flex1 align-items-center">
                                                <div><h3><strong>{props.original.task_name}</strong>&nbsp;</h3></div>
                                                <div><h3><span>{props.original.date}</span>&nbsp;</h3></div>
                                                {
                                                    (
                                                        props.original.task_status == '1' ?
                                                            <span className="slots_sp clr_yellow">In Progress</span>
                                                            :
                                                            props.original.task_status == '3' ?
                                                                <span className="slots_sp clr_red">Canceled</span>
                                                                :
                                                                props.original.task_status == '2' ?
                                                                <span className="slots_sp clr_green">Completed</span>
                                                                :
                                                                <span className="">N/A</span>
                                                    )
                                                }

                                            </div>
                                        )

                                    },
                                    {
                                        expander: true,
                                        Header: () => <strong></strong>,
                                        width: 55,
                                        headerStyle: { border: "0px solid #fff" },
                                        Expander: ({ isExpanded, ...rest }) =>
                                            <div >
                                                {isExpanded
                                                    ? <i className="icon icon-arrow-down icn_ar1"></i>
                                                    : <i className="icon icon-arrow-right icn_ar1"></i>}
                                            </div>,
                                        style: {
                                            cursor: "pointer",
                                            fontSize: 25,
                                            padding: "0",
                                            textAlign: "center",
                                            userSelect: "none"
                                        }
                                    }
                                ]}

                                defaultPageSize={3}
                                data={this.state.interViewList}
                                //pageSize={this.state.pages}
                                collapseOnDataChange={false}
                                showPagination={false}
                                minRows={1}                              
                                className="dble_tble_Mg mnge_grp_Tble"
                                onFetchData={this.fetchGroupInterview}
                                SubComponent={(props) =>
                                    <GroupInterviewDetail {...props} options2={options2} handleChangeCurrentState={this.handleChangeCurrentState} flagallicantpopup={this.state.flagallicantpopup} closeFlagApplicantPopUp={this.closeFlagApplicantPopUp}/>
                                }
                            />

                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default ManageGroupInterview;

class GroupInterviewDetail extends Component {
    render() {
        return (
        
            <div className="mngGrp_subComp">
                {/* {this.props.original.name} */}
                <div className="row mt_15p">
                    <div className="col-lg-3 col-md-4">
                        <h5><strong>Time/Duration</strong></h5>
                        <div className="row mt_15p">
                            <div className="col-md-6">
                                <div className="d-flex align-items-center">
                                    <small className="smallwid_50"><strong>From:</strong></small>

                                    <DatePicker
                                        autoComplete={'off'}
                                        dateFormat="DD/MM/YYYY h:mm A"
                                        selected={(this.props.original.start_datetime)?moment(this.props.original.start_datetime):'N/A'}
                                    />
                                </div>

                            </div>
                            <div className="col-md-6">
                                <div className="d-flex align-items-center">
                                    <small className="smallwid_50"><strong>To:</strong></small>
                                    <DatePicker
                                        autoComplete={'off'}
                                        dateFormat="DD/MM/YYYY h:mm A"
                                        className="csForm_control clr2"
                                        selected={(this.props.original.end_datetime)?moment(this.props.original.end_datetime):'N/A'}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4  pd_l_20p">
                        <h5><b>Location:</b></h5>
                        <div className="d_flex1">
                            <div>
                                <h6 className="fnt_15p">
                                    <div className="lh-17 mt-2 clr_grey">
                                        {(this.props.original.training_location)?recruitmentLocation(this.props.original.training_location):''}
                                        
                                    </div>
                                </h6>
                            </div>
                            <div className="align-items-end d_flex1 pd_l_20p">
                                <h6 className="pd_l_20p fnt_15p">Total Applicants <strong>{this.props.original.applicant_cnt}</strong></h6>
                            </div>

                        </div>

                    </div>
                    <div className="col-md-4 col-lg-5">
                        <div className="st_dvice_bx disabled_1__">
                            <button className="btn cmn-btn1 set_up_btn">Set up device</button>
                            <div><small className="cmplte_sp txtclr_green">completed DD/MM/YYYY - 00:00 AM</small></div>
                        </div>
                    </div>
                </div>

                <div className="row mt_30p">
                    <div className="col-sm-12 Req-Management-Group-Interview_tBL">

                        <ApplicantListing mainIndex={this.props.index} {...this.props} handleChangeCurrentState={this.props.handleChangeCurrentState} flagallicantpopup={this.props.flagallicantpopup} closeFlagApplicantPopUp={this.props.closeFlagApplicantPopUp}/>
                    </div>
                </div>

            </div>
        );
    }
}
//
class ApplicantListing extends Component {
    render() {

        return (
            <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                <ReactTable
                    columns={[
                        { 
                        // Header: "Applicant Name:", 
                        accessor: "applicant_name",
                        headerClassName:"_align_c__ header_cnter_tabl",
                        className:"_align_c__",
                        Header: () =>
                        <div>
                            <div className="ellipsis_line1__">Applicant Name</div>
                        </div>
                    ,
                        Cell:props =><span>{defaultSpaceInTable(props.value)}</span>
                       },
                        { 
                            // Header: "Device Allocation:", 
                            accessor: "device_alloc",
                            headerClassName:"_align_c__ header_cnter_tabl",
                            className:"_align_c__",
                            Header: () =>
                            <div>
                                <div className="ellipsis_line1__">Device Allocation</div>
                            </div>
                        ,
                            Cell:props =><span>{defaultSpaceInTable(props.value)}</span>
                         },
                        { 
                            // Header: "D.O.B:", 
                            accessor: "dob" ,
                            headerClassName:"_align_c__ header_cnter_tabl",
                            className:"_align_c__",
                            Header: () =>
                            <div>
                                <div className="ellipsis_line1__">D.O.B</div>
                            </div>
                        ,
                            Cell:props =><span>{defaultSpaceInTable(props.value)}</span>
                        },
                        {
                            accessor: "additional_ques",
                            headerClassName:"_align_c__ header_cnter_tabl",
                            className:"_align_c__",
                            Header: () =>
                            <div>
                                <div className="ellipsis_line1__">Additional Questions</div>
                            </div>
                        ,
                            Cell: (props) => (
                                <div className="">
                                    {
                                        (
                                            props.original.que_cnt > 0  ?
                                                <span className="slots_sp clr_green">Successful</span>
                                                :
                                                <span className="slots_sp clr_yellow">Pending</span>                                                    
                                        )
                                    }

                                </div>
                            )
                        },
                        {
                            accessor: "quiz",
                            headerClassName:"_align_c__ header_cnter_tabl",
                            className:"_align_c__",
                            Header: () =>
                            <div>
                                <div className="ellipsis_line1__">Quiz</div>
                            </div>
                        ,
                            Cell: (props) => (
                                <div className="">
                                    {
                                        (
                                            props.original.quiz == 'successful' ?
                                                <span className="slots_sp clr_green">Successful</span>
                                                :
                                                props.original.quiz == 'progress' ?
                                                    <span className="slots_sp clr_blue">Progress</span>
                                                    :
                                                    <span className="slots_sp clr_red">Flagged</span>
                                        )
                                    }

                                </div>
                            )
                        },
                        {
                            accessor: "draft",
                            headerClassName:"_align_c__ header_cnter_tabl",
                            className:"_align_c__",
                            Header: () =>
                            <div>
                                <div className="ellipsis_line1__">Draft Contract</div>
                            </div>
                        ,
                            Cell: (props) => (
                                <div className="">
                                    {
                                        (
                                            props.original.draft == 'successful' ?
                                                <span className="slots_sp clr_green">Successful</span>
                                                :
                                                props.original.draft == 'progress' ?
                                                    <span className="slots_sp clr_blue">Progress</span>
                                                    :
                                                    <span className="slots_sp clr_red">Flagged</span>
                                        )
                                    }

                                </div>
                            )
                        },
                        {
                            // Header: "Applicant Status",
                            accessor: "apli_Status",
                            headerClassName:"_align_c__ header_cnter_tabl",
                            Header: () =>
                                            <div>
                                                <div className="ellipsis_line1__">Applicant Status</div>
                                            </div>
                                        ,
                            className:"_align_c__",
                            Cell: (props) => (
                                <div className="">
                                    {
                                        (
                                            props.original.applicant_status == '3' ?
                                                <span className="slots_sp clr_green">Completed</span>
                                                :
                                                props.original.applicant_status == '1' ?
                                                    <span className="slots_sp clr_blue">In Progress</span>
                                                    :
                                                    props.original.applicant_status == '1' ?
                                                    <span className="slots_sp clr_red">Rejected</span>:

                                                    <span className="">N/A</span>
                                        )
                                    }

                                </div>
                            )
                        },
                        {
                            expander: true,
                            Header: () => <strong></strong>,
                            width: 55,
                            headerStyle: { border: "0px solid #fff" },
                            Expander: ({ isExpanded, ...rest }) =>
                                <div className="expander_bind" >
                                    {isExpanded
                                        ? <i className="icon icon-arrow-down icn_ar1"></i>
                                        : <i className="icon icon-arrow-right icn_ar1"></i>}
                                </div>,
                            style: {
                                cursor: "pointer",
                                fontSize: 25,
                                padding: "0",
                                textAlign: "center",
                                userSelect: "none"
                            }

                        }

                    ]}

                    defaultPageSize={10}
                    data={this.props.original.aplicantDetail}
                    pageSize={this.props.original.aplicantDetail.length}
                    showPagination={false}
                    collapseOnDataChange={false}
                    className="-striped -highlight"
                    SubComponent={(props) => {
                        return (
                           
                                                <div className="tBL_Sub pd_lr_20p">
                                                    <div className="ap_dts_bx2__k____">

                                                        <div className="row ap_dts_padding">
                                                            <div className="col-lg-3 br-1 border-black">
                                                                <div className="">
                                                                    <div className="mb-m-1 header_CABDay">
                                                                        <span className="MG_inter_label"><strong>1. Review Quiz Results</strong></span>
                                                                        <div className="my_wigh">
                                                                            <div className="s-def1 s1 req_s1">
                                                                                <Select name="participant_assessment" required={true} simpleValue={true}
                                                                                    searchable={false} clearable={false} options={applicantQuizResultStatus()} value="1"
                                                                                    placeholder="Filter by: Unread" className={'custom_select'}
                                                                                />
                                                                            </div>
                                                                        </div>
                                                                        {/* <span className="slots_sp clr_green">Successful</span> */}
                                                                    </div>

                                                                    <div className="main_row_NA_Quiz mb-m-1  progress_color_01_ success_color_01_">
                                                                        <div className="heading_Na_Quiz">
                                                                            Applicant Results:
                                                                        </div>
                                                                        <div className="row_NA_Quiz">
                                                                            <a className="NA_btn active_NA_Quiz">N/A</a>
                                                                            {(props.original.test_complete == 0)?
                                                                            <a className="NA_btn" onClick={(e)=> alert('Test is not completed yet.')} >Mark Quiz</a>
                                                                            :
                                                                            <a className="NA_btn" href={'/admin/recruitment/group_interview_result/'+(props.original.applicant_id)}>Mark Quiz</a>
                                                                        }
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div className="col-lg-9 col-md-8 bor_l_b ">

                                                                <div className="col-lg-9">
                                                                    <div className="appli_row_1 mb-m-1">
                                                                        <div className="appli_row_a">
                                                                            <div>
                                                                                <label>Applicant Classification:</label>
                                                                                <a className="appli_btn_ ">{(props.original.classfication)?phoneInterviwApplicantClassification(props.original.classfication):'N/A'}</a>
                                                                            </div>
                                                                        </div>
                                                                        <div className="appli_row_a">
                                                                            <div>
                                                                                <label>Recruitment Area:</label>
                                                                                <a className="appli_btn_ ">{props.original.recruitment_area}</a>
                                                                            </div>
                                                                        </div>
                                                                        <div className="appli_row_a">
                                                                            <div>
                                                                                <label>Job Position:</label>
                                                                                <a className="appli_btn_ ">{props.original.job_position}</a>
                                                                            </div>
                                                                        </div>
                                                                        <div className="appli_row_a">
                                                                            <div>
                                                                                <label></label> 
                                                                                <a className="appli_btn_ active_1" href={'/admin/recruitment/group_interview/GroupInterviewQuestionsForApplicant/'+(props.original.applicant_id)}>Manage Questions</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        
                                                            <FlagApplicantModal showModal={this.props.flagallicantpopup} closeModal={ this.props.closeFlagApplicantPopUp } applicant_id={props.original.applicant_id}/>

                                                        </div>
                                                        <div className="CAB_table_footer_">

                                                <div className="Cab_table_footer_div1_ mr-5">
                                                    <label className="customChecks publ_sal mr-3">
                                                        <input type="checkbox" name="is_salary_publish" />
                                                        <div className="chkLabs fnt_sm">No Show</div>
                                                    </label>
                                                   
                                                    <label className="customChecks publ_sal ml-3">
                                                        <input type="checkbox" checked={(props.original.flagged_status && props.original.flagged_status == '2')?'checked':''} onChange={ (e) => this.props.handleChangeCurrentState()} />
                                                        <div className="chkLabs fnt_sm">Flag Applicant</div>
                                                    </label>
                                                </div>
                                                <div className="CAB_table_footer_div2_">
                                                    <a className="CAB-btn">Stop</a>
                                                    <a className="CAB-btn ml-3" disabled>Start</a>
                                                </div>

                                                </div>
                                                </div>

                                                </div>

                                          
                                 
                        );
                    }


                    }

                />
            </div>);
    }
}