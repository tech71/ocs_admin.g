import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ScrollArea from 'react-scrollbar';
import { postData } from '../../../service/common.js';

class ManageDocuments extends Component {

    constructor() {
        super();
        this.state = {
            
        }
    }    

    render() {
        return (
            <div className={'customModal ' + (this.props.showModal? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                    Manage Required Documents
                            <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className="row Manage_div_1">
                        <div className="col-md-6">
                            <div className="cstmSCroll1">
                                <ScrollArea
                                    speed={0.8}
                                    className="Options_scroll"
                                    contentClassName="content"
                                    horizontal={false}
                                    style={{paddingRight:'15px'}}
                                >

                            <div className="Manage_ul_">
                            {this.props.all_documents.map((value, idx) => (
                                <div className= {(value.clickable)?'Manage_li_ step_a':(value.selected && !value.clickable)?'Manage_li_ step_b':'Manage_li_'} key={idx}>
                                    <div className="Manage_li_a1_" onClick={(e) => this.props.enableAndSelectDocs(value,idx)}><span>{value.label}</span></div>
                                    <div  className="Manage_li_a2_">
                                        {(value.clickable)?
                                            <div className="Manage_li_a2_">

                                                <a className={(value.optional)?'Req_btn_out_1 R_bt_co_blue mr-2 active_selected':'Req_btn_out_1 R_bt_co_blue mr-2'} onClick={(e) => this.props.chooseOptionalMandatory(value,idx,'optional')}>Optional</a>
                                                <a className={(value.mandatory)?'Req_btn_out_1 R_bt_co_ active_selected':'Req_btn_out_1 R_bt_co_'} onClick={(e) => this.props.chooseOptionalMandatory(value,idx,'mandatory')}>Mandatory</a>
                                                <i className="icon icon-add2-ie Man_btn_3a" onClick={(e)=> this.props.selectDocument(value,idx,'add')}></i> 

                                        </div>
                                        :''}
                                    </div>
                                </div>
                            ) )}
                            </div>
                            </ScrollArea>
                            </div>
                        </div>

                        <div className="col-md-6">
                        <div className="cstmSCroll1">
                        <ScrollArea
                            speed={0.8}
                            className="Options_scroll"
                            contentClassName="content"
                            horizontal={false}
                            style={{paddingRight:'15px'}}
                        >
                            <div className="Manage_ul_">
                             {this.props.all_documents.map((value, idx) => (
                                (value.selected)?
                                <div className= "Manage_li_" key={idx+2}>
                                    <div className="Manage_li_a1_"><span>{value.label}</span></div>
                                    <div  className="Manage_li_a2_">
                                        {(value.optional)?<a className="Man_btn_1">Optional</a>:''}
                                        {(value.mandatory)?<a className="Man_btn_2">Mandatory</a>:''}
                                        <i className="icon icon-remove2-ie Man_btn_3b"  onClick={(e)=> this.props.selectDocument(value,idx,'remove')}></i> 
                                    </div>
                                </div>:''
                            ) )}
                            </div>
                            </ScrollArea>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-md-12">
                            <div className="bt-1"></div>
                        </div>
                    </div>

                    {(!this.props.check_documents && this.props.allow_close_docs_form)?'Please select atleast one Documents to continue.':''}

                    <div className="row">
                        <div className="col-md-12">
                            <button className="btn cmn-btn1 creat_task_btn__" onClick={(e)=>this.props.closeAndSaveDocuments()}>Save Changes</button>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
export default ManageDocuments;

