import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import CKEditor from "react-ckeditor-component";
import moment from 'moment';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class QuickPreviewModal extends Component {

    constructor(props) {
        super();
        this.state = {  
            enable_job_post_btn:false
        } 
    }

    getSelectedValue=(ary,givenVal)=>{
        var returnVal = 'N/A';
        if(ary)
        {
            ary.map((term, idx) => {
                if(term.value == givenVal)
                {
                    returnVal = term.label;
                }               
            })
        }
        return returnVal;
    }

    job_post_btn =()=>{ 
        var enable_job_post_btn = false;

        if(this.props.parentState.from_date)
        {
            enable_job_post_btn = true;    
        }
        
        //in job status 3 or live job can not see post button
        if(enable_job_post_btn && this.props.parentState.view == 'operate' && (this.props.parentState.job_status!=3 && this.props.parentState.job_status!=5)) { 
            return <div className='col-md-6 text-right no_pd_r crJoBtn_Col__'>
                <input type='button' className='btn cmn-btn1 crte_svBtn' value='Post Job' onClick={ (e) => this.props.setstateOfDraftAndPostJob(false,true) } />
            </div>
        }
    }

    render() {
        return (
            <div className={'customModal ' + (this.props.showModal? ' show' : '')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Quick Preview Ad{/*  - Disability Support Worker (Casual) */}
                            <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className='resume_frame'>
                    <div className='template1'>

                        <table className='content_mn_tble'>
                            <tr>
                                <td style={{background: 'rgb(56, 41, 110)',  width: '150px'}}></td>
                                <td>
                                <div className='right_part_wh'>
                            <div className='content_main'>

                                <img src='/assets/images/admin/ocs_logo.svg' className='logo_img'/>
                                <h2 className='hdng_1'><strong>Job description</strong></h2>
                                {(this.props.parentState.job_content)?ReactHtmlParser(this.props.parentState.job_content):'N/A'}
                                <br/>
                                <br/>
                                <h3 className="sub_title_02"><strong> Job Details</strong></h3>
                                <p><strong>Job Type:</strong> {this.getSelectedValue(this.props.parentState.job_type,this.props.parentState.type)} </p>
                                <p><strong>Job Category:</strong> {this.getSelectedValue(this.props.parentState.job_category,this.props.parentState.category)} </p>
                                <p><strong>Job Sub-Category:</strong> {this.getSelectedValue(this.props.parentState.job_sub_category,this.props.parentState.sub_category)} </p>
                                <p><strong>Job Position:</strong> {this.getSelectedValue(this.props.parentState.job_position,this.props.parentState.position)} </p>
                                <p><strong>Employment Type:</strong> {this.getSelectedValue(this.props.parentState.job_employment_type,this.props.parentState.employment_type)} </p>
                                <p><strong>Salary Range:</strong> {this.getSelectedValue(this.props.parentState.job_salary_range,this.props.parentState.salary_range)} </p>
                                {(this.props.parentState.is_salary_publish)?<p><strong>Publish salary:</strong> Yes </p>:''}
                                
                                <br/>
                                <br/>
                                <h3 className="sub_title_02"><strong> Job Advertisement Timeframe</strong></h3>
                                
                                <div><strong>From date: </strong>{(this.props.parentState.from_date) && this.props.parentState.from_date!='0000-00-00 00:00:00'?moment(this.props.parentState.from_date).format('DD-MM-YYYY'):'N/A'}</div>
                                {(!this.props.parentState.is_recurring || this.props.parentState.is_recurring ==0)?<div><strong>To date:</strong> {(this.props.parentState.to_date) && this.props.parentState.to_date!='0000-00-00 00:00:00'?moment(this.props.parentState.to_date).format('DD-MM-YYYY'):'N/A'}</div>:''}
                                <div className="pb-4"><strong>Is Recurring:</strong> {(this.props.parentState.is_recurring) && this.props.parentState.is_recurring == 1?'Yes':'No'}</div>

                                <h3 className="sub_title_02"><strong> Job Location</strong></h3>
                                <p><strong>Location:</strong> {(this.props.parentState.complete_address)?this.props.parentState.complete_address.formatted_address:'N/A'} </p>
                                <p><strong>Phone:</strong> {(this.props.parentState.phone)?this.props.parentState.phone:'N/A'} </p>
                                <p><strong>Email:</strong> {(this.props.parentState.email)?this.props.parentState.email:'N/A'} </p>
                                <p><strong>Website:</strong> {(this.props.parentState.website)?this.props.parentState.website:'N/A'} </p>
                                <div className="clearfix"></div>
                                
                                <div className="row">
                                {console.log(this.props.parentState.all_documents)}
                                {(this.props.parentState.all_documents.length >0)?<h3 className="sub_title_02"><strong> Required Documents</strong></h3>:''}
                                {this.props.parentState.all_documents.map((value, idx) => (
                                    (value.clickable)?                                  
                                        <div key={idx}>                                       
                                        <div className="Req_list_1r col-lg-4" key={idx+2}>
                                        <div className="Req_list_1r_1a">{value.label}</div>
                                        <div className="Req_list_1r_1b"> 
                                            {(value.optional)?<a className="Req_btn_out_1 R_bt_co_blue">Optional</a>:''}
                                            {(value.mandatory)?<a className="Req_btn_out_1 R_bt_co_">Mandatory</a>:''}
                                        </div>
                                        </div>
                                       
                                         </div>:''
                                ) )}
                                 </div>
                                <div className="clearfix"></div>
                               
                                <div className="d-block w-100">
                                <h3 className="sub_title_02"><strong> Published To</strong></h3>
                                </div>

                                {this.props.parentState.publish_to.map((val, idx) => (
                                    <div key={idx+1}>
                                        <div><strong>Channel:</strong> {val.channel_name}</div>
                                    </div>
                                ))}
                            </div>

                            {this.job_post_btn()}

                            <div className='footer_gr'>
                                    <table className='mn_tble'>
                                        <tr>
                                            <td width='50%'>
                                                <ul className='socio_link'>
                                                    <li><a href=""><img src='https://www.freeiconspng.com/uploads/fb-logo-icon-facebook-26.png' /></a></li>
                                                    <li><a href=""><img src='https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-512.png' /></a></li>
                                                    <li><a href=""><img src='https://www.iosicongallery.com/icons/instagram-2016-05-12/256.png' /></a></li>
                                                </ul>
                                            </td>
                                            <td width='50%'>
                                                <div className='foot_logo'><img  src='/assets/images/admin/ocs_logo.svg' className=''/></div>
                                                <div className='site_lnk'><a href="">hcm.com.au</a></div>
                                            </td>
                                        </tr>
                                    </table>
                            </div>

                        </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    </div>                
                </div>
            </div>
        );
    }
}

export default QuickPreviewModal;

