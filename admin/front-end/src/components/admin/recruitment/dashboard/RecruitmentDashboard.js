import React, { Component } from 'react';
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import { checkLoginWithReturnTrueFalse,getPermission,postData } from 'service/common.js';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from '../../../../config.js';
import Pagination from "../../../../service/Pagination.js";
import { connect } from 'react-redux'
import RecruitmentPage from 'components/admin/recruitment/RecruitmentPage';
import ScrollArea from 'react-scrollbar';
import EditTask from '../action_task/EditTask';
import _ from 'lodash';
import DashboardAdminView from '../dashboard/DashboardAdminView';
import DashboardPersonalView from '../dashboard/DashboardPersonalView';
import { taskRecruitmentDashboard, graphViewType } from 'service/custom_value_data.js';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';

const defaultGraphTypeShow='week';
const defultLegendColor = _.map(taskRecruitmentDashboard, 'color');

class Dashboard extends Component {

    constructor() {
        super();
        
        this.state = {
            searchVal: '',
            filterVal: '',
            due_task:[],
            recruitment_status:[],
            latest_action:[],
            new_assigned_applicant:[],
            EditNewTaskModal:false,
            editTaskId:0,
            graphType:graphViewType,
            prospective_app_count_type:defaultGraphTypeShow,
            prospective_app_count_data:0,
            hired_app_count_type:defaultGraphTypeShow,
            hired_app_count_data:0,
            recruitment_app_count_type:defaultGraphTypeShow,
            recruitment_app_count_data:{"1": "0", "2": "0", "3": "0"},
            recruitment_app_count_data_1:{"1": "30", "2": "40", "3": "70"},
        }
        this.permission = (checkLoginWithReturnTrueFalse())?((getPermission() == undefined)? [] : JSON.parse(getPermission())):[];
    }

    handleChangeState=(givenState,otherState)=>{
        var temp = {};
        temp[givenState] = true;
        temp[otherState] = false;
        temp['due_task'] = [];
        this.setState(temp,()=>{
            this.getDueTask();
            if(!this.state.personal_view)
            {
                this.getLatestAction();
            }
            else
            {
                this.getNewAssignedApplicant();
            }            
        });
    }

    componentDidMount() {
        if(this.permission.access_recruitment_admin == 1){
            this.handleChangeState('admin_view','personal_view');
        }
        else{
            this.handleChangeState('personal_view','admin_view');
        }
        this.getGraphResult('all','1','1');
    }

    getGraphData=(request)=>{

        postData('recruitment/RecruitmentDashboard/get_applicant_recruitment_dashboard_graph',request).then((result) => {
            
            if (result.status) {
                this.setState({ 
                    recruitment_status:result.data.rows,
                    recruitment_graph:result.data.graph,
                }, () => { });
                
            }

        });

    }

    getGraphResult = (actionType, graphType, mode) => {
        this.setState({ [actionType + '_count_type']: graphType }, () => {
            this.fetchGraphData(actionType, mode);
        })
    }

    fetchGraphData = (actionType, mode) => {
        let actionTypeView = mode==2 ? actionType:'all' ;
        let type = mode==2?  this.state[actionType + '_count_type']:defaultGraphTypeShow;
        postData('recruitment/RecruitmentDashboard/get_applicant_recruitment_dashboard_graph', { mode: actionTypeView, type: type }).then((result) => {
            if (result.status && actionTypeView!='all') {
                this.setState({ [actionType + '_count_data']: result.data[actionType] });
            }else if(result.status && actionTypeView=='all'){
                this.setState({ ['hired_app_count_data']: result.data['hired_app'],['prospective_app_count_data']: result.data['prospective_app'],['recruitment_app_count_data']: result.data['recruitment_app'] });
            }
        });
    }

    getDueTask = ()=>
    { 
        postData('recruitment/RecruitmentDashboard/get_task_list_dashboard', {currentState:this.state,acess_type : this.permission.access_recruitment_admin}).then((result) => {
            if (result.status) {
                this.setState({ 
                    due_task:result.data,
                }, () => { });
            }
        });
    }

    getLatestAction = ()=>
    { 
        postData('recruitment/RecruitmentDashboard/get_latest_action', {currentState:this.state,acess_type : this.permission.access_recruitment_admin}).then((result) => {
            if (result.status) {
                this.setState({ 
                    latest_action:result.data,
                }, () => { });
            }
        });
    }

    getNewAssignedApplicant = ()=>
    { 
        postData('recruitment/RecruitmentDashboard/get_new_assigned_applicant', {currentState:this.state,acess_type : this.permission.access_recruitment_admin}).then((result) => {
            if (result.status) {
                this.setState({ 
                    new_assigned_applicant:result.data,
                }, () => { });
            }
        });
    }
    editShowTask = (taskId)=>{
        this.setState({EditNewTaskModal:true,editTaskId:taskId})
    }

    refreshPageData = ()=>{
        this.getDueTask();
        if(!this.state.personal_view)
        {
            this.getLatestAction();
        }else{
            this.getNewAssignedApplicant();
        }
    }

    render() {
       
        

         var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two', clearableValue: false }
        ];

const dashboardData = [
            {
                Action: 'Task',
                DueDate: '01/02/03',
                Assigned: 'Jimmy',
                LastAction: '01/02/03 - by Jimmy',
                Status: 'Pending'
            },
            {
                Action: 'To-Do',
                DueDate: '01/02/03',
                Assigned: 'Tony',
                LastAction: '01/02/03 - by Eric ',
                Status: 'Actioned'
            },
            {
                Action: 'Schedule',
                DueDate: '01/02/03',
                Assigned: 'Jessica',
                LastAction: '01/02/03 - by Jessica',
                Status: 'Pending'
            },
        ]
        const Recruitmentdata = {
            labels: ['New', 'In-Progress', 'Unsucessful'],
            datasets: [{
                data:_.values(this.state.recruitment_app_count_data),
                //fill:false,
                backgroundColor: defultLegendColor,

            }],
        };
 console.log(Recruitmentdata);
        return (
           
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12 col-md-12 no-pad back_col_cmn-">
                        {/* <Link to={ROUTER_PATH + 'admin/dashboard'}><span className="icon icon-back1-ie"></span></Link> */}
                    </div>
                </div>
              
            
                
                <div className="row pt-5">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1> {(this.permission.access_recruitment_admin == 1)?this.props.showPageTitle+ ' (Admin)':'Recruitment'}

                        </h1>
                    </div>
                </div>
               
                <div className="row status_row-- pt-3 after_before_remove">

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Prospective Applicants:</h4>
                                <div className="col-lg-12 col-md-12 col-sm-12 colJ-1">
                                
                                    
                                    <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="recruitment" />
                                    

                                    <div className="duly_vw">
                                        <div className="viewBy_dc text-center">
                                            <h5>View By:</h5>
                                            <ul>
                                            {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                                    return (<li key={index} className={val.name == this.state.prospective_app_count_type ? 'active' : ''} onClick={() => this.getGraphResult('prospective_app', val.name, 2)}>{_.capitalize(val.name)}</li>);
                                                                }) : <React.Fragment />}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Applicants Hired:</h4>
                                
                                <div className="col-lg-12 col-md-12 col-sm-12 colJ-1">
                                   {/*  <div className="num_stats1">
                                        <span>151</span>
                                    </div> */}
                                    <CounterShowOnBox counterTitle={this.state.hired_app_count_data} classNameAdd="" mode="recruitment" />

                                    <div className="duly_vw">
                                        <div className="viewBy_dc text-center">
                                            <h5>View By:</h5>
                                            <ul>
                                            {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                                    return (<li key={index} className={val.name == this.state.hired_app_count_type ? 'active' : ''} onClick={() => this.getGraphResult('hired_app', val.name, 2)}>{_.capitalize(val.name)}</li>);
                                                                }) : <React.Fragment />}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div className="status_box1">
                            <div className="row">
                                <h4 className="hdng">Recruitment Status:</h4>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <div className='grpRecruitmentdatah_dv'>
                                        <Bar data={Recruitmentdata}  style={{width:'100%'}} height={180}
                                            options={{
                                                maintainAspectRatio: false,scales: {
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true
                                                        },
                                                        gridLines: {
                                                            display: false,
                                                            drawBorder: true
                                                        }
                                                    }],
                                                    xAxes: [{
                                                        ticks: { display: false },
                                                        gridLines: {
                                                        display: false,
                                                        drawBorder: true
                                                    }
                                                }]
                                                },
                                                legend: {display:false}
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-12 col-sm-12 colJ-1">
                                    <ul className="status_det_list">
                                    {Object.keys(this.state.recruitment_app_count_data).map((valD,index) => {
                                            return (<li className={taskRecruitmentDashboard[valD].className} key={index}>{ taskRecruitmentDashboard[valD].type + "=" + this.state.recruitment_app_count_data[valD] }</li> );
                                        }) 
                                    }

                                    </ul>
                                    {/* <div className="duly_vw"> */}
                                    <div className="viewBy_dc text-center">
                                        <h5>View By:</h5>
                                        <ul>
                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                                    return (<li key={index} className={val.name == this.state.recruitment_app_count_type ? 'active' : ''} onClick={() => this.getGraphResult('recruitment_app', val.name, 2)}>{_.capitalize(val.name)}</li>);
                                                                }) : <React.Fragment />}
                                        </ul>
                                    </div>
                                    {/* </div> */}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row sort_row1-- after_before_remove" style={{ display: 'none' }}>
                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_l srchCol_r">
                        <div className="search_bar left">
                            <input type="text" className="srch-inp" placeholder="Search.." />
                            <i className="icon icon-search2-ie"></i>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-6 no_pd_r filCol_l">
                        <div className="filter_flx">
                            <div className="filter_fields__ cmn_select_dv">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Search by: All"
                                    options={options}
                                    onChange={(e) => this.setState({ searchVal: e })}
                                    value={this.state.searchVal}
                                />

                            </div>

                            <div className="filter_fields__ cmn_select_dv pd_r_0_De">
                                <Select name="view_by_status "
                                    required={true} simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Filter by: Unread"
                                    options={options}
                                    onChange={(e) => this.setState({ filterVal: e })}
                                    value={this.state.filterVal}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                

                <div className="row data_table_row1" style={{ display: 'none' }}>
                    <div className="col-lg-12 no-pad">
                        <div className="data_table_cmn dashboard_Table ">
                            <ReactTable
                                PaginationComponent={Pagination}
                                data={dashboardData}
                                defaultPageSize={dashboardData.length}
                                className="-striped -highlight"
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                columns={[
                                    { Header: "Action Type", accessor: 'Action' },
                                    { Header: "Due Date", accessor: 'DueDate' },
                                    { Header: "Assigned to", accessor: 'Assigned' },
                                    { Header: "Last Actioned", accessor: 'LastAction' },
                                    { Header: "Status", accessor: 'Status' }
                                ]}
                            />
                        </div>
                    </div>
                </div>


                <div className="row">
                    <ul className="nav nav-tabs Category_tap Nav_ui__ col-lg-10  col-sm-12 P_20_TB">
                        <li className={(this.state.personal_view)?'active col-lg-4 col-sm-4':'col-lg-4 col-sm-4'}>
                            <a href="#1a" data-toggle="tab" onClick={(e)=>this.handleChangeState('personal_view','admin_view')}>Personal View</a>
                        </li>

                        {(this.permission.access_recruitment_admin == 1)?
                        <li className={(this.state.admin_view)?'active col-lg-4 col-sm-4':'col-lg-4 col-sm-4'}>
                        <a onClick={(e)=>this.handleChangeState('admin_view','personal_view')}>Admin View</a>
                        </li>:''}
                    </ul>
                </div>

                {(this.state.admin_view)?<DashboardAdminView editShowTask={this.editShowTask} due_task={this.state.due_task} latest_action={this.state.latest_action}/>:'' }
                {(this.state.personal_view)?<DashboardPersonalView editShowTask={this.editShowTask} due_task={this.state.due_task} new_assigned_applicant={this.state.new_assigned_applicant} />:'' }
                {this.state.EditNewTaskModal? <EditTask taskId={this.state.editTaskId} showModal={this.state.EditNewTaskModal} closeModal={(res) => { this.setState({ EditNewTaskModal: false },()=>{if(res==true){this.refreshPageData();}}) }} /> : ''}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
       
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);