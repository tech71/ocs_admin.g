import React, { Component } from 'react';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'




class AddDevices extends Component {

    constructor() {
        super();
        this.state = {

        }
    }

    

    render() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
          ];

          const data = [
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00001',
                DeviceLocation:'Traning Room 1',               
            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00002',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00003',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00004',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00005',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00006',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00007',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00008',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '00009',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '000010',
                DeviceLocation:'Traning Room 1',               

            },
            {
                DeviceNo:'DLXX61G2HBD8',
                DeviceName: '000011',
                DeviceLocation:'Traning Room 1',               

            }

        ];
           
        return (
          <div  className={'customModal ' + (this.props.showModal? ' show' : '')}>
                <div className="cstomDialog widBig" style={{width:" 800px", minWidth:" 800px"}}>

                    <h3 className="cstmModal_hdng1--">
                        Add Devices
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className="row mt-5">
                        <div className="col-md-12">
                        <div>Update the details for <b>'OCS 0015'</b></div>
                        </div>
                    </div>

                    <div className="row  pd_b_20 mt-5 mb-5 d-flex align-content-end">
                        <div className="col-md-4">
                            <div className="csform-group">
                                <label>Devices Name:</label>
                                <input type="text" name="answer0" className="csForm_control bl_bor" data-rule-required="true" value="" />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="csform-group">
                                <label>Devices Name:</label>
                                <input type="text" name="answer0" className="csForm_control bl_bor" data-rule-required="true" value="" />
                            </div>
                        </div>
                        <div className="col-md-4">
                        <div className="csform-group">
                                <label>Devices Location:</label>
                        <Select
                            name="form-field-name"
                            value="one"
                            options={options}
                            searchable = {false}
                            //   onChange={logChange}
                            />
                            </div>
                        </div>
                    </div>



                    <div className="row mt-5">
                        <div className="col-lg-12">
                            <div className="Search_for_device_heading">
                            <div><b>Devices currently available on your network</b></div>
                             <a class="btn cmn-btn1">Search for Device</a>

                            </div>
                        </div>
                    </div>

               

                    <div className="row mt-5 mb-5">
                    <div className="col-lg-12">
                
                                    <div className="data_table_cmn  header_center  tbl_flx2 tble_2_clr AddionalQ_table add_devices_react_table">
                                        <ReactTable
                                            columns={[
                                                {Header: " Device Name:", accessor: "DeviceName",
                                                Cell: (props) => (
                                                    <div className="Question_id_div_">
                                                        <label className="customChecks publ_sal"><input type="checkbox" /><div className="chkLabs fnt_sm">&nbsp;</div></label>
                                                        <div>{props.original.DeviceName}</div>
                                                       
                                                    </div>

                                                )
                                            },
                                                {Header: "   Device No:", accessor: "DeviceNo",
                                               Cell: (props) => (<div className="text_ellip_2line text-center">{props.original.DeviceNo} </div>)},
                                             
                                                {Header: " Status:", accessor: "QuestionID",
                                               Cell: (props) => ( 
                                                <div>
                                               {/* <span className="short_buttons_01 btn_color_assigned">Assigned</span> */}
                                               <span className="short_buttons_01 btn_color_avaiable">Avaiable</span>
                                              {/*  <span className="short_buttons_01 btn_color_offline">Offline</span> */}
                                               </div>
                                               )},

                                                {Header: "     Actions:", accessor: "QuestionID",
                                                Cell: (props) => ( <div>
                                                    <a className="short_buttons_01">Add Device</a>
                                                </div>)},
                                            ]}
                                            defaultPageSize={3}
                                            data={data}
                                            pageSize={data.length}
                                            showPagination={false}
                                            className=""

                                        />
                                    </div>
                              
                        </div>

                    </div>

                
                    <div className="row bt-1" style={{paddingTop:"15px"}}>
                        <div className="col-md-12 text-right">
                            <button class="btn cmn-btn1 new_task_btn">Add Devices</button>
                        </div>
                    </div>

                </div>
            </div> 
        );
    }
}

export default AddDevices;