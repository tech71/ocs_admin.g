import { postData,toastMessageShow,archiveALL} from 'service/common.js';
import { BASE_URL } from 'config.js';

/*
 *  applicant info start
 *  set applicant info setter, getter and middleware 
 */

// action for set applicant info
export const setApplicantInfo = (applicantInfo) => ({
        type: 'set_applicant_info',
        applicantInfo
    })
    
export const setApplicantInfoKeyValue = (obj) => ({
        type: 'set_applicant_info_key_value',
        obj
    })


// call middleware from class
export function getApplicantInfo(applicant_id, loading_status) {
    return (dispatch, getState) => {
        return dispatch(applicantInfoQuery(applicant_id, loading_status))
    }
}

// ascronus middleware for fetch data 
function applicantInfoQuery(applicant_id, loading_status) {
    return dispatch => {
        if(loading_status)
        dispatch(setApplicantInfoKeyValue({info_loading: true}));
        
        return postData('recruitment/RecruitmentApplicant/get_applicant_info', {applicant_id: applicant_id}).then((result) => {
            if (result.status) {
                dispatch(setApplicantInfo(result.data))
                if(loading_status)
                dispatch(setApplicantInfoKeyValue({info_loading: false}));
            }else{
                window.location.href = "/admin/recruitment/applicants";
            }
        });
    }
}

/*
 *  applicant info end 
 */


/*
 *  applicant stage details start
 *  set applicant stage details setter, getter and middleware 
 */

// action for set applicant stage details
export const setApplicantMainStageDetails = (stageDetails) => ({
        type: 'set_applicant_main_stage_details',
        stageDetails
    })


// call middleware from class
export function getApplicantMainStageDetails(applicant_id) {
    return (dispatch, getState) => {
        return dispatch(applicantMainStageDetailsQuery(applicant_id))
    }
}

// ascronus middleware for fetch data 
function applicantMainStageDetailsQuery(applicant_id) {
    return dispatch => {

        return postData('recruitment/RecruitmentApplicant/get_applicant_main_stage_details', {applicant_id: applicant_id}).then((result) => {
            if (result.status) {
                dispatch(setApplicantMainStageDetails(result.data))
            }
        });
    }
}

/*
 *  applicant mnastage details end 
 */


/*
 * update applicant stage
 */
export function updateApplicantStage(applicant_id, stageId, status) {
    return (dispatch, getState) => {
        return dispatch(updateApplicantStageQuery(applicant_id, stageId, status))
    }
}

// ascronus middleware for fetch data 
function updateApplicantStageQuery(applicant_id, stageId, status) {
    return dispatch => {
        var request = {applicant_id: applicant_id, stageId: stageId, status: status};
        return postData('recruitment/RecruitmentApplicant/update_applicant_stage_status', request).then((result) => {
            if (result.status) {
               dispatch(getApplicantMainStageDetails(applicant_id));
               if(status == 4 || status == 5){
                   dispatch(applicantInfoQuery(applicant_id));
               }
               if(status == 4 ){
                   dispatch(applicantAttachmentDetailsQuery(applicant_id));
               }
            }else{
                toastMessageShow(result.error,'e')
            }
        });
    }
}

/*
 * end  update applicant stage
 */

/*
 *  get stage wise detials on open accordian
 */

// action for set applicant stage wise details
export const setApplicantStageWiseDetails = (stageDetails, its_open, stage_number) => ({
        type: 'set_applicant_stage_wise_details',
        stageDetails,
        its_open,
        stage_number
    })

export const setApplicantStageWiseDetailsLoadingStatus = (stage_number, its_loading) => ({
        type: 'set_applicant_stage_wise_details_loading_status',
        stage_number,
        its_loading,
    })

// call middleware from class
export function getApplicantStageWiseDetails(its_open, stage_number, applicant_id) {
    return (dispatch, getState) => {
        return dispatch(applicantStageWiseDetailsQuery(its_open, stage_number, applicant_id))
    }
}

// ascronus middleware for fetch data 
function applicantStageWiseDetailsQuery(its_open, stage_number, applicant_id) {
    return dispatch => {
        if(its_open){
        dispatch(setApplicantStageWiseDetailsLoadingStatus(stage_number, true))    
            
        return postData('recruitment/RecruitmentApplicant/get_applicant_stage_wise_details', {applicant_id: applicant_id, stage_number: stage_number}).then((result) => {
            if (result.status) {
                dispatch(setApplicantStageWiseDetails(result.data, its_open, stage_number))
                dispatch(setApplicantStageStatusOPen(its_open, stage_number))
            }
            dispatch(setApplicantStageWiseDetailsLoadingStatus(stage_number, false))
        });
        }else{
            dispatch(setApplicantStageStatusOPen(its_open, stage_number));
        }
    }
}
/*
 * end stage wise details section of action
 */

/*
 * set open close status
 */
export const setApplicantStageStatusOPen = ( its_open, stage_number) => ({
        type: 'set_applicant_stage_wise_open_status',
        its_open,
        stage_number
})

/*
 * applicant attachment category start
 *  set applicant attachment categorydetails setter, getter and middleware 
 */

// action for set applicant stage details
export const setApplicantAttachmentCategoryDetails = (data) => ({
    type: 'set_applicant_attachment_category',
    attachmentCategory:data
})


// call middleware from class
export function getApplicantAttachmentCategoryDetails() {
return (dispatch, getState) => {
    return dispatch(applicantAttachmentCategoryDetailsQuery())
}
}

// ascronus middleware for fetch data 
function applicantAttachmentCategoryDetailsQuery() {
return dispatch => {

    return postData('recruitment/RecruitmentApplicant/get_attachment_category_details').then((result) => {
        if (result.status) {
            dispatch(setApplicantAttachmentCategoryDetails(result.data))
        }
    });
}
}

/*
*  applicant mnastage details end 
*/

/*
 * applicant attachment start
 *  set applicant attachment categorydetails setter, getter and middleware 
 */

// action for set applicant stage details
export const setApplicantAttachmentDetails = (data) => ({
    type: 'set_applicant_attachment',
    attachment:data
})


// call middleware from class
export function getApplicantAttachmentDetails(applicantId) {
return (dispatch, getState) => {
    return dispatch(applicantAttachmentDetailsQuery(applicantId))
}
}

// ascronus middleware for fetch data 
function applicantAttachmentDetailsQuery(applicantId) {
return dispatch => {

    return postData('recruitment/RecruitmentApplicant/get_all_attachments_by_applicant_id',{id:applicantId}).then((result) => {
        if (result.status) {
            dispatch(setApplicantAttachmentDetails(result.data))
        }
    });
}
}

/*
*  applicant mnastage details end 
*/

/*
*common action
*/

export function downloadSelectedRecuritmentAttachment(obj) {
    if(Object.keys(obj.state.activeDownload).length>0){
        let requestData = {applicantId: obj.props.applicantDetails.id, downloadData: obj.state.activeDownload};
        postData('recruitment/RecruitmentApplicant/download_selected_file', requestData).then((result) => {
            if (result.status) {
                window.location.href = BASE_URL + "mediaDownload/"+result.zip_name;    
                obj.setState({activeDownload : {}},()=>{
                    obj.scrollTopData();
                });
            } else {
            toastMessageShow(result.error,'e');
            }
        });   
    }else{
    toastMessageShow('Please select at least one document for download.','e');
    }
}
export function archiveSelectedRecuritmentAttachment(obj) {
    if(Object.keys(obj.state.activeDownload).length>0){
        let requestData = {applicantId: obj.props.applicantDetails.id, archiveData: obj.state.activeDownload};
        archiveALL(requestData, 'Are you sure want to archive selected attachment', 'recruitment/RecruitmentApplicant/archive_selected_file').then((result) => {
            if (result.status) {
                obj.attachmentListRefresh();
                obj.setState({activeDownload : {}},()=>{
                    obj.scrollTopData();
                });
            } else if (!result.status) {
                if (result.hasOwnProperty('error') || result.hasOwnProperty('msg')) {
                    let msg = (result.hasOwnProperty('error') ? result.error : (result.hasOwnProperty('msg') ? result.msg : ''));
                    toastMessageShow(msg,'e');
                }
            } else {
                toastMessageShow('Something went wrong please try again.','e');
            }
        });
    }else{
    toastMessageShow('Please select at least one document for Archive.','e');
    }
}
export function selecteRecuritmentAttachment(obj,id) {
    let activeDownloadData =obj.state.activeDownload;
    if(activeDownloadData[id]){
        delete activeDownloadData[id];
    }else{
    activeDownloadData[id]=1;
    }
    obj.setState({activeDownload:activeDownloadData});
}

/*
 * applicant stage label notes start
 *  set applicant notes setter, getter and middleware 
 */

// action for set applicant stage details
export const setApplicantAttachementStageNotesDetails = (data) => ({
    type: 'set_applicant_attachment_notes',
    notes:data
})


// call middleware from class
export function getApplicantAttachementStageNotesDetails(applicantId) {
return (dispatch, getState) => {
    return dispatch(applicantAttachementStageNotesDetailsQuery(applicantId))
}
}

// ascronus middleware for fetch data 
function applicantAttachementStageNotesDetailsQuery(applicantId) {
return dispatch => {

    return postData('recruitment/RecruitmentApplicant/get_attachment_stage_notes_by_applicant_id',{id:applicantId}).then((result) => {
        if (result.status) {
            dispatch(setApplicantAttachementStageNotesDetails(result.data))
        }
    });
}
}

/*
*  applicant stage label notes end 
*/

/*
 * applicant stage label notes start
 *  set applicant notes setter, getter and middleware 
 */


// call middleware from class
export function getApplicantLastUpdateDetails(applicantId) {
return (dispatch, getState) => {
    return dispatch(applicantLastUpdateDetailsQuery(applicantId))
}
}

// ascronus middleware for fetch data 
function applicantLastUpdateDetailsQuery(applicantId) {
return dispatch => {

    return postData('recruitment/RecruitmentApplicant/get_lastupdate_by_applicant_id',{id:applicantId}).then((result) => {
        if (result.status) {
            dispatch(setApplicantInfoKeyValue({last_update:result.data}))
        }
    });
}
}

/*
*  applicant stage label notes end 
*/