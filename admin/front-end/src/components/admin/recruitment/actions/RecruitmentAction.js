import {recruitmentActiveTitle} from 'menujson/recruitment_menu_json';
import { postData} from '../../../../service/common.js';
import { ROUTER_PATH} from 'config.js';

export const setRecruitmentArea = (area) => ({
        type: 'set_recruitment_area',
        area
})


export function getRecruitmentArea(){
     
     return (dispatch, getState) => {
        return dispatch(recruitmentQuery())
    }  
}

// ascronus middleware for fetch data 
function recruitmentQuery(request) {
    return dispatch => {
    
        return postData('recruitment/RecruitmentUserManagement/get_recruitment_area', request).then((result) => {
            if (result.status) {
                dispatch(setRecruitmentArea(result.data))
            }
        });
    }
}

export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_recruitment',
        value
}}
    
export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =recruitmentActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}


export const setRecruiterAdminActionNotification = (data) => ({
    type: 'set_recruiter_admin_actionable_notification_data',
    data
})
export const getRecruiterAdminActionNotificationRequest = () => ({
    type: 'get_recruiter_admin_actionable_notification_request',
    res:true
})


export function getRecruiterAdminActionNotification(){
     
    return (dispatch, getState) => {
        dispatch(getRecruiterAdminActionNotificationRequest());
       return dispatch(recruiterAdminActionNotificationQuery());
   }  
}

// ascronus middleware for fetch data 
function recruiterAdminActionNotificationQuery(request) {
    return dispatch => {
    
        return postData('recruitment/RecruitmentDashboard/get_latest_status_updates_notification', request).then((result) => {
            if (result.status) {
                dispatch(setRecruiterAdminActionNotification(result.data));
            }
        });
    }
}

export function recruiterAdminActionNotificationDismissAndView(request) {
    return dispatch => {
        return postData('recruitment/RecruitmentDashboard/action_updates_notification', request).then((result) => {
            if (result.status) {
                dispatch(getRecruiterAdminActionNotification());
                if(request.type==1){
                    window.location =request.url;
                }
            }
        });
    }
}

/*
 * applicant stage label notes start
 *  set applicant notes setter, getter and middleware 
 */

// action for set applicant stage details
export const setTaskStageDetails = (data) => ({
    type: 'set_task_stage',
    data:data
})


// call middleware from class
export function getTaskStageDetails() {
return (dispatch, getState) => {
    return dispatch(taskStageDetailsQuery())
}
}

// ascronus middleware for fetch data 
function taskStageDetailsQuery() {
return dispatch => {

    return postData('recruitment/RecruitmentTaskAction/get_recruiter_stage').then((result) => {
        if (result.status) {
            dispatch(setTaskStageDetails(result.data))
        }
    });
}
}

/*
*  applicant stage label notes end 
*/


