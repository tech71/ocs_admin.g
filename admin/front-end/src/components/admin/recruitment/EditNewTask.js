import React, { Component } from 'react';
import ReactTable from "react-table";

class EditNewTask extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    };


    render() {

        const task = [
            {
                date:'01/01/2019',
                location:'ONCALL Training',
                recruiter:'Mike Smith',
                accepted:'4',
                pending:'4',
                available:'2'
            },
            {
                date:'01/01/2019',
                location:'ONCALL Training',
                recruiter:'Mike Smith',
                accepted:'2',
                pending:'3',
                available:'2'
            },
            {
                date:'01/01/2019',
                location:'ONCALL Training',
                recruiter:'Mike Smith',
                accepted:'2',
                pending:'3',
                available:'2'
            },
            {
                date:'01/01/2019',
                location:'ONCALL Training',
                recruiter:'Mike Smith',
                accepted:'2',
                pending:'3',
                available:'2'
            },
            {
                date:'01/01/2019',
                location:'ONCALL Training',
                recruiter:'Mike Smith',
                accepted:'2',
                pending:'3',
                available:'2'
            }

        ];
        return (
            <div className={'customModal ' + (this.props.showModal ? ' show' : ' ')}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        New Group Interview for Applicant
                        <span className="closeModal icon icon-close1-ie" onClick={this.props.closeModal}></span>
                    </h3>

                    <div className="row">

                        <div className="col-md-6 col-sm-12">

                            <div className="row">
                                <div className="col-lg-4 col-md-6 col-sm-6">
                                    <div className="csform-group">
                                        <label className="fs_16">Applicant Name:</label>
                                        <p className="mt-3 clr_grey">Charles Davis</p>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-6 col-sm-6">
                                    <div className="csform-group">
                                        <label className="fs_16">Interview Type:</label>
                                        <p className="mt-3 clr_grey">Group Interview</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="col-md-6 col-sm-12">

                            <div className="row">

                                <div className="col-md-12 col-sm-12">
                                    <div className="csform-group">
                                        <label className="fs_16">Interview Selected:</label>
                                    </div>
                                </div>

                                <div className="col-md-6 col-sm-6">
                                    <h6 className="mb-4"><b>Date: &nbsp;</b><span className="clr_grey">02/09/2019</span></h6>
                                    <h6>
                                        <b>Location:</b><br />
                                        <div className="lh-17 mt-2 clr_grey">
                                            ONCALL Training facility
                                                        <div>- Training Room</div>
                                        </div>
                                    </h6>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                    <h6 className="mb-4"><b>Confirmed Attendees: &nbsp;</b><span className="clr_grey">4</span></h6>
                                    <h6 className="mb-4"><b>Pending Attendees: &nbsp;</b><span className="clr_grey">4</span></h6>
                                    <h6 className="mb-5"><b>Available Spots: &nbsp;</b><span className="clr_grey">2</span></h6>

                                    <h6 className="mb-4"><b>Recruiter in charge: &nbsp;</b><span className="clr_grey">Mike Smith</span></h6>
                                </div>


                            </div>

                        </div>



                    </div>
                    <div className="bor_line"></div>

                    <div className="row">

                        <div className="col-md-12 col-sm-12">

                            <h3 className="mt-3 avl_ints mb-4"><b>Next Available Interviews</b></h3>

                            <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">

                                <ReactTable
                                    columns={[
                                        
                                        { Header: "Date:", accessor: "date" },
                                        { Header: "Location:", accessor: "location" },
                                        { Header: "Recruiter:", accessor: "recruiter" },
                                        { Header: "Accepted:", accessor: "accepted" },
                                        { Header: "Pending:", accessor: "pending" },
                                        { Header: "Available:", accessor: "available" },
                                        {
                                            Header: "Select Interview",
                                            accessor: "select",
                                            Cell: props => (
                                                <button 
                                                    className={"selectTsk_btn cmn-btn1 " + ((props.index % 2 === 0) ? 'active': ' ')}
                                                    onClick={this.selectHandler}
                                                >
                                                        Select
                                                </button>
                                            )
                                        }
                                        


                                    ]}

                                    defaultPageSize={3}
                                    minRows={3}
                                    data={task}
                                    pageSize={task.length}
                                    showPagination={false}
                                    className="-striped -highlight"

                                />

                            </div>
                            <div className="text-right">
                                <small className="extra_sm_fnt">*Clicking save will automatically send an email invite to the applicant</small>
                            </div>


                        </div>
                        


                    </div>
                    <div className="bor_line mt-5"></div>
                    <div className="row  pb-5 pt-3">
                            <div className="col-sm-12 text-right">
                                <button type="submit" className="btn cmn-btn1 create_quesBtn">Save Changes</button>
                            </div>
                        </div>

                </div>

            </div>
        )
    }

}

export default EditNewTask;