const initialState = {
    recruitment_area: [],  
    recruiter_admin_actionable_notification:{fetchRecord:false,data:[]},
    activePage:{pageTitle:'',pageType:''},
    recruitment_location: [], 
    task_stage_option: [], 
    its_recruiter_admin: false, 
    recruiter_data: [], 
}

const RecruitmentReducer = (state = initialState, action) => {

    switch (action.type) {
        case 'set_recruitment_area':
            return { ...state, recruitment_area : action.area };
        
        case 'set_active_page_recruitment':
            return {...state, activePage: action.value};

        case 'set_recruiter_admin_actionable_notification_data':
            return {...state, recruiter_admin_actionable_notification: {fetchRecord:false,data:action.data}};
        case 'get_recruiter_admin_actionable_notification_request':
            return {...state, recruiter_admin_actionable_notification: {...state.recruiter_admin_actionable_notification,fetchRecord:action.res}};
        case 'set_task_stage':
            return {...state, ...action.data};
        
        default:
            return state
    }
}

export default RecruitmentReducer
    