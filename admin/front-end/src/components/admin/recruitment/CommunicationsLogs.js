import React from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import { postData, reFreashReactTable,handleChangeChkboxInput,toastMessageShow } from 'service/common.js';
import { BASE_URL } from 'config.js';
import 'react-table/react-table.css'
import { PAGINATION_SHOW } from 'config.js';
import Pagination from "service/Pagination.js";
import Select from 'react-select-plus';
import CommunicationLogTemplateEmail from './CommunicationLogTemplateEmail.js';
import CommunicationLogTemplatePhone from './CommunicationLogTemplatePhone.js';
import CommunicationLogTemplateSms from './CommunicationLogTemplateSms.js';
import { logType } from '../../../dropdown/recruitmentdropdown.js';
import queryString from 'query-string';
import { urlNotification,  defaultSpaceInTable} from 'service/custom_value_data.js';
import { connect } from 'react-redux'

const requestData = (pageSize, page, sorted, filtered, log_type, applicant_id) => {
    return new Promise((resolve, reject) => {
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered, log_type: log_type, applicant_id: applicant_id };

        postData('recruitment/RecruitmentDashboard/get_communication_log', Request).then((result) => {
            let filteredData = result.data;
            if(result.status){
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        }else{
            const res = {
                rows: [],
                pages: 0
            };
            resolve(res);
        }
        });

    });
};

class CommunicationsLogs extends React.Component {
    constructor() {
        super();
        this.state = {
            communicationLog: [],
            selectAll:0,
            filterVal:0,
            selected:[],
            srch_box:''
        }
    }

    componentWillMount() {
        const parsed = queryString.parse(window.location.search);
        if (typeof (parsed) === 'object' && parsed.a != '') {
            let stateData = {};
            stateData['applicantId'] = parsed.a;
            if (parsed['t'] && parsed.t != '') {
                stateData['filterVal'] = parsed.t;
            }
            this.setState(stateData);
        }
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            this.props.log_type,
            this.props.applicant_id,
        ).then(res => {
            this.setState({
                communicationLog: res.rows,
                pages: res.pages,
                loading: false,
                userSelectedList:[],
                selectAll:0
            });
        });
    }

    toggleSelectAll_ = () => {
        let newSelected = {};
        
        if (this.state.selectAll === 0) {
            this.state.communicationLog.forEach(x => {
                newSelected[x.id] = true;                
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        },()=>{
           
        });
    }

    toggleSelectAll = () => {
        let newSelected = {};
        let selectedList = this.state.communicationLog;
        var userSelectedList = this.state.userSelectedList;

        if (this.state.selectAll === 0) {
            this.state.communicationLog.forEach(x => {
                newSelected[x.id] = true;
            });
        }

        this.setState({
            userSelectedList: selectedList,
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    toggleRow_1 = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        let selectedList = this.state.communicationLog;
        var userSelectedList = this.state.userSelectedList;

        var columnIndex = selectedList.findIndex(x => x.id == id);
        if (newSelected[id]) {
            this.setState({
                userSelectedList: userSelectedList.concat(selectedList[columnIndex]),
            });
        }
        else {
            var tempState = {};
            var selectedColumnIndex = userSelectedList.findIndex(x => x.id == id);
            tempState['userSelectedList'] = userSelectedList.filter((s, sidx) => selectedColumnIndex !== sidx);
            this.setState(tempState);
        }

        this.setState({
            selected: newSelected,
            selectAll: 2
        }, () => { });
    }

    searchLog = (e) => {
        if(e!=undefined){
            e.preventDefault();
        }
        let extratstate = {};
        if (this.state.hasOwnProperty('applicantId')) {
            extratstate['applicantId'] = this.state.applicantId;
        }
        var requestData = {...this.state.filtered,...extratstate, srch_box: this.state.srch_box, filterBy: this.state.filterVal };
        this.setState({ filtered: requestData });
    }

    defaultFilteredData = () => {
        let data = {};
        data['applicantId'] = this.state.hasOwnProperty('applicantId') ? this.state.applicantId : '';
        data['filterBy'] = this.state.hasOwnProperty('filterVal') ? this.state.filterVal : 0;
        return data;
    }

    filterChange = (value) => {
        let extratstate = {};
        if (this.state.hasOwnProperty('applicantId')) {
            extratstate['applicantId'] = this.state.applicantId;
        }
        this.setState({ filterVal: value, filtered: { ...this.state.filtered, ...extratstate, srch_box: this.state.srch_box, filterBy: value } });
    }

    csvDownload = () => {
        var requestData = { communicationLog: this.state.userSelectedList };
        console.log(requestData);
        postData('recruitment/RecruitmentDashboard/export_communication_log', requestData).then((result) => {
            if (result.status) {
                this.setState({ userSelectedList: [] });
                window.location.href = BASE_URL + "archieve/" + result.csv_url;
            } else {
               toastMessageShow(result.error,'s');
            }
        });
    }

    render() {
        
        const columns = [
            {
                id: "from",
                accessor: "from_email",
                headerClassName: 'Th_class_d1 header_cnter_tabl checkbox_header',
                className: 'Tb_class_d1 Tb_class_d2',
                Cell: props => {
                    return (
                        <span>
                            <label className="Cus_Check_1">
                              <input type='checkbox' checked={this.state.selected[props.original.id] === true} onChange={() => this.toggleRow(props.original.id)} />
                              <div className="chk_Labs_1"></div>
                            </label>
                            <div> {defaultSpaceInTable(props.value)}<span onClick={() =>  this.toggleRow(props.original.id)}  ></span></div>
                        </span>

                    );
                },
                Header: x => {
                    return (
                        <div className="Tb_class_d1 Tb_class_d2 w-100">
                            <span >
                                <label className="Cus_Check_1">
                                     <input type='checkbox'  checked={this.state.selectAll === 1} ref={input => {
                            if (input) { input.indeterminate = this.state.selectAll === 2 }
                        }}
                            onChange={() => this.toggleSelectAll()} />
                                    <div className="chk_Labs_1" ></div>
                                </label>
                                <div>From:</div>
                            </span>
                        </div>
                    );
                },
                resizable: false,
                sortable:false,
            },
            {
                id: "To",
                accessor: "to_email",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">To:</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span> {defaultSpaceInTable(props.value)}</span>
            },
            {
                id: "type",
                accessor: "log_type",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Type:</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{(props.value == 1)?'SMS':(props.value == 2)?'Email':'Phone'}</span>,
                width: 230
            },

            {
                id: "sent",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                width: 180,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                accessor: "sent",
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Sent:</div>
                    </div>,
                Expander: (props) =>
                    <div className="expander_bind"> 
                        <span>{props.original.sent}</span>
                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }
            }
        ]

        return (

            <React.Fragment>
                <div className="row pt-5">
                    <div className="col-lg-12 col-md-12 main_heading_cmn-">
                        <h1>{this.props.showPageTitle}</h1>
                    </div>
                </div>

                <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-8 col-sm-8 no_pd_l">
                         <form id="srch_log" autoComplete="off" onSubmit={this.searchLog} method="post">
                            <div className="search_bar right srchInp_sm actionSrch_st">
                                <input type="text" className="srch-inp" placeholder="Search.." name="srch_box" onChange={(e) => {handleChangeChkboxInput(this, e)}}/>
                                <i className="icon icon-search2-ie"  onClick={(e) => this.searchLog(e)}></i>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 no_pd_r">
                        <div className="filter_flx">
                            <div className="filter_fields__ cmn_select_dv gr_slctB sl_center">
                                <Select name="view_by_status"
                                    required={true} simpleValue={true}
                                    searchable={false} clearable={false}
                                    placeholder="Contact Type"
                                    options={logType()}
                                    onChange={this.filterChange}
                                    value={this.state.filterVal}
                                    readOnly={true}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 d-flex align-self-center">
                        <a className="C_NeW_BtN w-100" onClick={() => this.csvDownload()}>
                            <span>Download Select Logs</span><i className="icon icon icon-download2-ie"></i>
                        </a>
                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col-lg-12 Req-Communications-logs_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                            <ReactTable
                                PaginationComponent={Pagination}
                                columns={columns}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                className="-striped -highlight"

                                manual
                                ref={this.reactTable}
                                data={this.state.communicationLog}
                                pages={this.state.pages}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                filtered={this.state.filtered}
                                defaultFiltered={this.defaultFilteredData()}
                                showPagination={this.state.communicationLog.length > PAGINATION_SHOW ? true : false}
                                
                                SubComponent={(props) =>
                                    (props.original.log_type == 2)?
                                 <CommunicationLogTemplateEmail emailTemplateVersion={props.original.email_template_version} log_data={props.original.log_data}/>
                                 :(props.original.log_type == 3)?
                                 <CommunicationLogTemplatePhone log_data={props.original.log_data}/>
                                 :
                                 <CommunicationLogTemplateSms log_data={props.original.log_data}/>
                                }
                            />
                        </div>

                    </div>
                </div>
            </React.Fragment>

        );
    }
}


const mapStateToProps = state => ({
    showPageTitle: state.RecruitmentReducer.activePage.pageTitle,
    showTypePage: state.RecruitmentReducer.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(CommunicationsLogs);

// export default CommunicationsLogs;