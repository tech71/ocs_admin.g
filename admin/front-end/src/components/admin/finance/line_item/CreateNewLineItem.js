import React, { Component } from 'react';

import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';

import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom';
import { postData, handleChangeSelectDatepicker, handleChange, handleDateChangeRaw, handleShareholderNameChange, toastMessageShow, onlyNumberAllow } from 'service/common.js';
import moment from 'moment';
import jQuery from "jquery";


class CreateNewLineItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            funding_type_option: [],
            outcome_domain_option: [],
            registration_group_option: [],
            support_category_option: [],
            time_of_the_days: [],
            week_days: [],
            state: [],
            quote_required: false,
            price_control: false,
            schedule_constraint: false,
            travel_required: false,
            cancellation_fees: false,
            ndis_reporting: false,
            non_f2f: false,
            public_holiday: false,
        }
    }

    getCreateLineItemOption = (lineItemId) => {
        postData('finance/FinanceLineItem/get_create_line_item_option_and_details', {lineItemId: lineItemId}).then((result) => {
            if (result.status) {
                this.setState(result.data)
            }
        });
    }

    componentDidMount = () => {
        this.getCreateLineItemOption(this.props.match.params.id);
        if(this.props.match.params.id){
            this.setState({lineItemId: this.props.match.params.id});
        }
    }

    onSubmit = (e) => {
        e.preventDefault();

        var validator = jQuery("#add_line_item").validate({ ignore: [],});

        if (jQuery("#add_line_item").valid()) {
            this.setState({ loading: true});
            postData('finance/FinanceLineItem/add_update_line_item', this.state).then((result) => {
                if (result.status) {
                    toastMessageShow('Line item created successfully', 's');
                    this.setState({ redirectTrue: true });
                } else {
                    toastMessageShow(result.error, 'e');
                }
                this.setState({ loading: false });
            });
        }else{
             validator.focusInvalid();
        }

    }
    
    viewStatus = () => {
        var status = '';
        if(this.props.match.params.id){
            if(this.state.status == 1){
                var status = "Active";
            }else if(this.state.status == 2){
                var status = "Inactive";
            }else{
                var status = "Archived";
            }    
        }
      return status;
    }     

    render() {
        const line_itemId = this.props.match.params.id;
        return (
            <React.Fragment>
                {this.state.redirectTrue ? <Redirect to='/admin/finance/line_item_listing' /> : ''}
                <div className="row">

                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <Link to="/admin/finance/line_item_listing"><span className="icon icon-back1-ie"></span></Link>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.match.params.id? 'Edit Line Item '+this.state.line_item_number : this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center justify-content-end">
                                     {this.viewStatus()?<span className="Small_btn_az green_colr">{this.viewStatus()}</span>: ''}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div className="Finance__panel_1 mt-5">
                    <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F" >
                        <form id="add_line_item" method="post">
                            <Panel defaultExpanded>
                                <Panel.Heading>
                                    <Panel.Title toggle>
                                        <div>
                                            <p>Line Item Categories</p>
                                            <span className="icon icon-arrow-right"></span>
                                            <span className="icon icon-arrow-down"></span>
                                        </div>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    <div className="row">
                                        <div className="col-lg-8 col-lg-offset-2">

                                            <div className="row mt-5">
                                                <div className="col-lg-6 col-md-6 col-sm-6">
                                                    <span className='required'>
                                                    <div className="sLT_gray left left-aRRow">
                                                        <Select className="custom_select default_validation"
                                                            simpleValue={true}
                                                            name="form-field-name"
                                                            value={this.state.funding_type || ''}
                                                            //valueComponent={<FundingAreaRender />}
                                                            options={this.state.funding_type_option}
                                                            onChange={(value) => handleChangeSelectDatepicker(this, value, 'funding_type')}
                                                            clearable={false}
                                                            searchable={false}
                                                            placeholder="Support Funding Type"
                                                            inputRenderer={() => <input type="text" className="define_input" name={"funding_type"} required={true} readOnly={true} value={this.state.funding_type || ''} />}
                                                        />
                                                    </div>
                                                    </span>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-6">
                                                    <div className="sLT_gray left left-aRRow">
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-12">
                                                    <label className="label_2_1_1">Support Registration Group</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}
                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.registration_group_option.map((val, index) => (
                                                                        <li className={"w-" + (((index % 2) == 0) ? "60" : "40")} key={index + 1}>
                                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="radio" name="support_registration_group" onChange={(e) => handleChangeSelectDatepicker(this, e.target.value, 'support_registration_group')} checked={(val.id == this.state.support_registration_group) ? true : false} value={val.id || ''} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="row mt-5">
                                                <div className="col-lg-12">
                                                    <label className="label_2_1_1">Support Category</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}

                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.support_category_option.map((val, index) => (
                                                                        <li className={"w-" + (((index % 2) == 0) ? "60" : "40")} key={index + 1}>
                                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="radio" name="support_category" onChange={(e) => handleChangeSelectDatepicker(this, e.target.value, 'support_category')} checked={(val.id == this.state.support_category) ? true : false} value={val.id || ''} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="row mt-5">
                                                <div className="col-lg-12">
                                                    <label className="label_2_1_1">Support Outcome Domain</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}
                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.outcome_domain_option.map((val, index) => (
                                                                        <li className={"w-" + (((index % 2) == 0) ? "60" : "40")} key={index + 1}>
                                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="radio" name="support_outcome_domain" onChange={(e) => handleChangeSelectDatepicker(this, e.target.value, 'support_outcome_domain')} checked={(val.id == this.state.support_outcome_domain) ? true : false} value={val.id || ''} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Panel.Body>
                            </Panel>

                            <Panel defaultExpanded>
                                <Panel.Heading>
                                    <Panel.Title toggle>
                                        <div>
                                            <p>Line Item Details </p>
                                            <span className="icon icon-arrow-right"></span>
                                            <span className="icon icon-arrow-down"></span>
                                        </div>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    <div className="row">
                                        <div className="col-lg-8 col-lg-offset-2">
                                            <div className="row mt-5">
                                                <div className="col-lg-4 col-sm-4">
                                                    <label className="label_2_1_1">Reference Number</label>
                                                    <span className='required'>
                                                    <input type="text" placeholder="001" name="line_item_number" onChange={(e) => handleChange(this, e)} disabled={line_itemId? true: false} value={this.state.line_item_number || ''} data-rule-required="true" maxLength="40" />
                                                    </span>
                                                </div>
                                                <div className="col-lg-8 col-sm-8">
                                                    <label className="label_2_1_1">Line Item Name</label>
                                                    <span className='required'>
                                                    <input type="text" name="line_item_name"  placeholder="Name" onChange={(e) => handleChange(this, e)} value={this.state.line_item_name || ''} maxLength="50" />
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="row mt-5">
                                                <div className="col-lg-4 col-sm-6">
                                                    <label className="label_2_1_1">Start Date</label>
                                                    <span className='required'>
                                                    <DatePicker dateFormat="DD/MM/YYYY" autoComplete={'off'} onChangeRaw={handleDateChangeRaw} placeholderText={"DD/MM/YYYY"} className="text-left"
                                                        selected={this.state.start_date? moment(this.state.start_date): null} name="start_date" onChange={(e) => handleChangeSelectDatepicker(this, e, 'start_date')} required={true}
                                                        maxDate={(this.state.end_date) ? moment(this.state.end_date) : moment().add(60, 'days')}
                                                        minDate={moment()}
                                                    />
                                                    </span>
                                                </div>
                                                <div className="col-lg-4 col-sm-6">
                                                    <label className="label_2_1_1">End Date</label>
                                                    <span className='required'>
                                                    <DatePicker dateFormat="DD/MM/YYYY" autoComplete={'off'} onChangeRaw={handleDateChangeRaw} placeholderText={"DD/MM/YYYY"} className="text-left"
                                                        selected={this.state.end_date? moment(this.state.end_date): null} name="end_date" onChange={(e) => handleChangeSelectDatepicker(this, e, 'end_date')} required={true}
                                                        minDate={this.state.start_date ? moment(this.state.start_date) : moment()}
                                                    />
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="row mt-5">
                                                <div className="col-lg-8 col-sm-12">
                                                    <label className="label_2_1_1">Line Item Description</label>
                                                   
                                                    <textarea className="w-100" maxLength="500" placeholder="Maximum 500 Characters" name="description" onChange={(e) => handleChange(this, e)} value={this.state.description || ''} />
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Panel.Body>
                            </Panel>

                            <Panel defaultExpanded>
                                <Panel.Heading>
                                    <Panel.Title toggle>
                                        <div>
                                            <p>Line Item Pricing</p>
                                            <span className="icon icon-arrow-right"></span>
                                            <span className="icon icon-arrow-down"></span>
                                        </div>
                                    </Panel.Title>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    <div className="row">
                                        <div className="col-lg-8 col-lg-offset-2">
                                            <div className="row mt-5">
                                                <div className="col-lg-5 col-md-5 col-sm-5">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Quote Required?</label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="quote_required" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.quote_required == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="quote_required" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.quote_required == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-lg-7 col-md-7 col-sm-7">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Does this Item have a Price Control? </label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="price_control" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.price_control == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="price_control" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.price_control == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-5 col-md-5 col-sm-5">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Travel Required?</label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="travel_required" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.travel_required == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="travel_required" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.travel_required == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-lg-7 col-md-7 col-sm-7">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Cancellation Fees? </label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="cancellation_fees" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.cancellation_fees == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="cancellation_fees" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.cancellation_fees == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-5 col-md-5 col-sm-5">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">NDIA Reporting?</label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="ndis_reporting" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.ndis_reporting == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="ndis_reporting" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.ndis_reporting == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col-lg-7 col-md-7 col-sm-7">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Non-F2F? </label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="non_f2f" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.non_f2f == 1) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="non_f2f" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.non_f2f == 0) ? true : false} />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                    <label className="label_2_1_1">Member to Participant Ratio</label>
                                                    <br></br>
                                                    <div className="ratio_input mt-2">
                                                        <span className="required"><input type="text" name="member_ratio" placeholder='1'  onChange={(e) => onlyNumberAllow(this, e)}  value={this.state.member_ratio || ''} min="1" data-rule-required="true" /></span>
                                                        <span>:</span>
                                                        <span className="required"><input type="text" name="participant_ratio" placeholder='1'  onChange={(e) => onlyNumberAllow(this, e)}  value={this.state.participant_ratio || ''} min="1" data-rule-required="true" /></span>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                    <label className="label_2_1_1">Upper Price Limit (National Non Remote)</label>
                                                    <span className="required mt-2">
                                                    <input type="text" name="upper_price_limit" placeholder="$00.00" onChange={(e) => onlyNumberAllow(this, e)} value={parseInt(this.state.upper_price_limit) > 0? this.state.upper_price_limit: ''} data-rule-required="true" min="1" maxLength="14" />
                                                    </span>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                    <label className="label_2_1_1">National Remote Price</label>
                                                    <input type="text" name="national_price_limit"  placeholder="$00.00"   onChange={(e) => onlyNumberAllow(this, e)} value={parseInt(this.state.national_price_limit) > 0? this.state.national_price_limit: ''} min="1" maxLength="14" />
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                    <label className="label_2_1_1">National Very Remote Price</label>
                                                    <input type="text" name="national_very_price_limit"  placeholder="$00.00"   onChange={(e) => onlyNumberAllow(this, e)} value={parseInt(this.state.national_very_price_limit) > 0? this.state.national_very_price_limit: ''} min="1" maxLength="14" />
                                                </div>
                                            </div>
                                            
                                            <div className="row mt-5">
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                <span className="required">
                                                    <div className="sLT_gray left left-aRRow">
                                                      <Select className="custom_select default_validation"
                                                            simpleValue={true}
                                                            name="form-field-name"
                                                            value={this.state.levelId || ''}
                                                            options={this.state.level_option}
                                                            onChange={(value) => handleChangeSelectDatepicker(this, value, 'levelId')}
                                                            clearable={false}
                                                            searchable={false}
                                                            placeholder="Minimum Level Required"
                                                            inputRenderer={() => <input type="text" className="define_input" name={"levelId"} required={true} readOnly={true} value={this.state.levelId || ''} />}
                                                        />
                                                      </div>
                                                      </span>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6">
                                                <span className="required">
                                                    <div className="sLT_gray left left-aRRow">
                                                       <Select className="custom_select default_validation"
                                                            simpleValue={true}
                                                            name="form-field-name"
                                                            value={this.state.pay_pointId || ''}
                                                            options={this.state.point_name_option}
                                                            onChange={(value) => handleChangeSelectDatepicker(this, value, 'pay_pointId')}
                                                            clearable={false}
                                                            searchable={false}
                                                            placeholder="Minimum Pay Point Required"
                                                            inputRenderer={() => <input type="text" className="define_input" name={"pay_pointId"} required={true} readOnly={true} value={this.state.pay_pointId || ''} />}
                                                        />
                                                       </div>
                                                       </span>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-12">
                                                    <div className="Time_line_lables">
                                                        <label className="label_2_1_1">Does this item have a schedule constraint?</label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="schedule_constraint" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.schedule_constraint == 1) ? true : false} data-rule-required="true" />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="schedule_constraint" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.schedule_constraint == 0) ? true : false} data-rule-required="true" />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-6 col-md-6 col-sm-6">
                                                    <label className="label_2_1_1">Select Days Applied to</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}
                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.week_days.map((val, index) => (
                                                                        <li className="w-50" key={index + 1}>
                                                                            <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="checkbox" name="week_days" value={val.id || ''} checked={val.selected || ''} onChange={(e) => handleShareholderNameChange(this, 'week_days', index, 'selected', e.target.checked)} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-lg-6 col-md-6 col-sm-6 pt-5">
                                                    <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Is this a Public Holiday?</label>
                                                        <label className="label_2_1_2">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="public_holiday" value="1" onChange={(e) => handleChange(this, e)} checked={(this.state.public_holiday == 1) ? true : false} data-rule-required="true" />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>Yes</span>
                                                        </label>
                                                        <label className="label_2_1_3">
                                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                <input type="radio" name="public_holiday" value="0" onChange={(e) => handleChange(this, e)} checked={(this.state.public_holiday == 0) ? true : false} data-rule-required="true" />
                                                                <span className="checkround"></span>
                                                            </label>
                                                            <span>No</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row mt-5">
                                                <div className="col-lg-6 ">
                                                    <label className="label_2_1_1">Select State Applied to</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}
                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.state.map((val, index) => (
                                                                        <li className="w-50" key={index + 1}>
                                                                            <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="checkbox" name="week_days" value={val.id || ''} checked={val.selected || ''} onChange={(e) => handleShareholderNameChange(this, 'state', index, 'selected', e.target.checked)} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.long_name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-lg-6">
                                                    <label className="label_2_1_1">Select Time of Day Applied to</label>
                                                    <div className="Parent-List_2_ul">
                                                        <div className="cstmSCroll1 FScroll">
                                                            <ScrollArea
                                                                speed={0.8}
                                                                contentClassName="content"
                                                                horizontal={false}

                                                                style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                            >
                                                                <ul className="List_2_ul">
                                                                    {this.state.time_of_the_days.map((val, index) => (
                                                                        <li className="w-50" key={index + 1}>
                                                                            <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                                <input type="checkbox" name="time_of_the_days" value={val.id || ''} checked={val.selected || ''} onChange={(e) => handleShareholderNameChange(this, 'time_of_the_days', index, 'selected', e.target.checked)} />
                                                                                <span className="checkround"></span>
                                                                            </label>
                                                                            <span className="text_2_0_1">{val.name}</span>
                                                                        </li>
                                                                    ))}
                                                                </ul>
                                                            </ScrollArea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </Panel.Body>
                            </Panel>
                            <div>
                            <div className="row d-flex justify-content-end none-after none-before">
                                <div className="col-lg-3"><button className="btn-1 w-100" onClick={this.onSubmit}>{this.props.match.params.id? 'Update Line Item' : 'Save Line Item'}</button></div> 
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </React.Fragment >
        );
    }

}


const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateNewLineItem);

const FundingAreaRender = (props) => {
    console.log(props.value)
    return (
        <React.Fragement >df{props.value}</React.Fragement>
    )
}    