import React, { Component } from "react";
import ReactTable from "react-table";
import Select from "react-select-plus";
import DatePicker from "react-datepicker";
import Pagination from "service/Pagination.js";
import ScrollArea from "react-scrollbar";
import { PanelGroup, Panel } from "react-bootstrap";
import ProfilePage from "./../common/ProfilePage";
import { connect } from "react-redux";
import _ from "lodash";
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW,CURRENCY_SYMBOL,REGULAR_EXPRESSION_FOR_AMOUNT } from "config.js";
import {
  getOrganisationDetails,
  getParticipantDetails,
  getSiteDetails,
  requestInvoiceData as requestData
} from "./../action/FinanceAction.js";
import { postData, toastMessageShow,currencyFormatUpdate,handleShareholderNameChange } from "service/common.js";
import {
  colorCodeInvoiceStatus,
  iconFinanceShift,
  defaultSpaceInTable
} from "service/custom_value_data.js";

const urlRedirect = ROUTER_PATH + "admin/finance/CreditNotes";
class CreateCreditNote extends React.Component {
  constructor(props) {
    super(props);
    let invoice_for =
      props.location.state != undefined &&
      props.location.state != null &&
      props.location.state.hasOwnProperty("booked_by") &&
      props.location.state.inf > 0
        ? props.location.state.inf
        : 0;
    let ptype =
      props.location.state != undefined &&
      props.location.state != null &&
      props.location.state.hasOwnProperty("ptype") &&
      props.location.state.ptype != ""
        ? _.lowerCase(props.location.state.ptype)
        : "";
    let booked_by =
      props.location.state != undefined &&
      props.location.state != null &&
      props.location.state.hasOwnProperty("booked_by") &&
      props.location.state.booked_by != ""
        ? props.location.state.booked_by
        : 0;

    this.state = {
      activeCol: "",
      invoice_for: invoice_for,
      ptype: ptype,
      booked_by: booked_by,
      modeType: "",
      filtered: { invoice_for: invoice_for, booked_by: booked_by },
      invoiceList: [],
      filter_by: "invoice_number",
      callDefault: false,
      expanded: null,
      selectedInvoiceId:{},
      totalCreditAvailabelAmount:0,
      totalCreditAmountRemaining:0,
      amountAppliedInvoiceId:{},
    };
    this.reactTable = React.createRef();
  }

  expand_row(rowIndexId,invoice_id) {
    var expanded = {...this.state.expanded};
    var selectedInvoiceId = {...this.state.selectedInvoiceId};
    if (expanded[rowIndexId]) {
      expanded[rowIndexId] = !expanded[rowIndexId];
      delete(selectedInvoiceId[invoice_id]);
    } else {
      expanded[rowIndexId] = true;
      let tempData ={};
      tempData['status'] = true;
      tempData['amount'] ='';
      tempData['desc'] = '';
      tempData['indexId'] = rowIndexId;
      selectedInvoiceId[invoice_id] = tempData;
    }


    this.setState({
      expanded: expanded,
      selectedInvoiceId: selectedInvoiceId,
    },()=>{
      this.calculateCreditAmountAvailableAndRemaining()
    });
  }

  fetchInvoiceList = (state, instance) => {
    this.setState({ loading: true });
    requestData(
      state.pageSize,
      state.page,
      state.sorted,
      state.filtered,
      2
    ).then(res => {
      this.setState({
        invoiceList: res.rows,
        all_count: res.all_count,
        pages: res.pages,
        loading: false
      },()=>this.calculateCreditAmountAvailableAndRemaining());
    });
  };

  submitSearch = (e, key, value, checkTextBoxValue) => {
    if (e) e.preventDefault();

    var state = {};
    state[key] = value;

    this.setState(state, () => {
      if (checkTextBoxValue != undefined && checkTextBoxValue) {
        if (
          (this.state.search && this.state.search.length > 0) ||
          (value == this.state.filter_by &&
            this.state.filter_by == "all" &&
            key == "filter_by")
        ) {
          this.filterListing();
        }
      } else {
        this.filterListing();
      }
    });
  };

  filterListing = () => {
    let req = {
      search: this.state.search,
      filter_by: this.state.filter_by,
      start_date: this.state.start_date,
      end_date: this.state.end_date,
      invoice_for: this.state.invoice_for,
      booked_by: this.state.booked_by,
      
    };
    this.setState({ filtered: req,expanded:null,selectedInvoiceId:{} });
  };

  componentDidMount() {
    if (this.state.invoice_for > 0 && this.state.ptype != "") {
      if (this.state.booked_by == "2" || this.state.booked_by == "3") {
        this.setState({ modeType: "participant", callDefault: true }, () => {
          this.props.getParticipantDetails(this.state.invoice_for);
        });
      } else if (this.state.booked_by == "1") {
        this.setState({ modeType: "site", callDefault: true }, () => {
          this.props.getSiteDetails(this.state.invoice_for);
        });
      } else if (this.state.booked_by == "4" || this.state.booked_by == "5") {
        this.setState({ modeType: "finace", callDefault: true }, () => {
          this.props.getOrganisationDetails(this.state.invoice_for);
        });
      }
    } else {
      this.setState({ pageDisplay: false }, () => {
        toastMessageShow("Required parameter is missing.", "e", {
          close: () => {
            window.location = urlRedirect;
          }
        });
      });
    }
  }

  calculateCreditAmountAvailableAndRemaining=()=>{
  let availableAmountData = Object.values(this.state.selectedInvoiceId);
  let totalAmount = _.sumBy(availableAmountData, function (row) {
    return row!=undefined && row!=null && row.hasOwnProperty('amount') ? _.round(parseFloat(row.amount), 2) :0;
  });
  
  let applyAmountData = Object.values(this.state.amountAppliedInvoiceId);
  let totalApplyAmount = _.sumBy(applyAmountData, function (row) {
    return row!=undefined && row!=null && row.hasOwnProperty('amount') ? _.round(parseFloat(row.amount), 2) :0;
  });
  let remaingAmount = _.subtract(totalAmount,applyAmountData);
      this.setState({totalCreditAvailabelAmount:totalAmount});
  }


  onchangedData =(e,invoice_id,type)=>{
    let selectedInvoiceIdData = Object.assign({},{...this.state.selectedInvoiceId});
    let invoicListData = this.state.invoiceList[selectedInvoiceIdData[invoice_id]['indexId']] ? this.state.invoiceList[selectedInvoiceIdData[invoice_id]['indexId']]:{};
    let amountAvailable = invoicListData!= undefined && invoicListData!=null && invoicListData.hasOwnProperty('amount') && invoicListData.hasOwnProperty('credit_note_from_used') ? invoicListData.amount - invoicListData.credit_note_from_used:0;
    if(selectedInvoiceIdData[invoice_id]['status'] && type=='amount' &&  e.target.value<=amountAvailable){
      selectedInvoiceIdData[invoice_id][type] = e.target.value;
      handleShareholderNameChange(this,'selectedInvoiceId',invoice_id,type,e.target.value,e).then((res)=>{
        if(res.status){
          this.calculateCreditAmountAvailableAndRemaining();
        }
      });
    }else if(selectedInvoiceIdData[invoice_id]['status'] && type!='amount'){
      selectedInvoiceIdData[invoice_id][type] = e.target.value;
      this.setState({selectedInvoiceId:selectedInvoiceIdData},()=>{
     
      });
    }
   

  }
  render() {
    const modeTypePropsRequest = {
      participant: "participantDetails",
      site: "siteDetails",
      finace: "organisationDetails"
    };
    const dataTable = [
      { task: "Task 1", duedate: "10/10/2025", assign: "Tanner Linsley" },
      { task: "Task 1", duedate: "10/10/2025", assign: "Tanner Linsley" },
      {
        task: "Task 1",
        duedate: "10/10/2025",
        assign: "Tanner Linsley"
      }
    ];

    const ndiserror = [
      {
        invoicenumber: "X_00001",
        description: "Shift",
        addressedto: "Booker 1",
        amount: "$100",
        dateofinvoice: "01/03/2019"
      },
      {
        invoicenumber: "X_00002",
        description: "Shift",
        addressedto: "Booker 2",
        amount: "$110",
        dateofinvoice: "01/03/2019"
      },
      {
        invoicenumber: "X_00003",
        description: "Shift",
        addressedto: "Booker 3",
        amount: "$140",
        dateofinvoice: "01/03/2019"
      }
    ];

    const columns = [
      {
        headerClassName: '_align_c__ header_cnter_tabl',
        className:'_align_c__',
        accessor:'invoice_status',
        id:'invoice_status',
        width: 70,
        resizable: false,
        headerStyle: { border: "0px solid #fff" },
        //expander: true,
        Header: () =>
            <div>
                <div className="ellipsis_line__">Status</div>
            </div>,
        Cell: (props) =>
            <div className="">
                <span>
                  <label className="Cus_Check_1">
                    <input type="checkbox" onChange={() => this.expand_row(props.viewIndex,props.original.id)}  checked={props.isExpanded ? true:false}/>
                    <div className="chk_Labs_1" style={{paddingRight:0}}></div>
                  </label>
                </span>
            </div>,
        style: {
            cursor: "pointer",
            fontSize: 25,
            padding: "0",
            textAlign: "center",
            userSelect: "none"
        }

    },
        /* {
            accessor: "invoice_id",
            id: "invoice_id",
            headerClassName: "_align_c__ header_cnter_tabl",
            className: "Tb_class_d1 Tb_class_d2",
          
            Expander: props => {
              return (
                <span>
                  <label className="Cus_Check_1">
                    <input type="checkbox" onChange={() => this.expand_row(props.viewIndex)}  checked={props.isExpanded? true :false}/>
                    <div className="chk_Labs_1" style={{paddingRight:0}}></div>
                  </label>
                </span>
              );
            },
            Header: x => {
              return (
                <div className="Tb_class_d1 Tb_class_d2">
                  <span>
                    <label className="Cus_Check_1">
                      <input type="checkbox" />
                      <div className="chk_Labs_1"></div>
                    </label>
                  </span>
                </div>
              );
            },
            style: {
              cursor: "pointer",
              fontSize: 25,
              padding: "0",
              textAlign: "center",
              userSelect: "none"
            },
            resizable: false,
            width: 70
          }, */
      {
        accessor: "invoice_number",
        id: "invoice_number",
        headerClassName: "_align_c__ header_cnter_tabl",
        className: "Tb_class_d1 Tb_class_d2",
        Cell: props => {
          return (
            <span>
              <div>{defaultSpaceInTable(props.value)}</div>
            </span>
          );
        },
        Header: x => {
          return (
            <div className="Tb_class_d1 Tb_class_d2">
              <span>
                <div className="ellipsis_line__">Invoice Number</div>
              </span>
            </div>
          );
        },
        className: "_align_c__",
        resizable: false,
        width: 180
      },
      {
        id: "description",
        accessor: "description",
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Description</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
      },

      {
        // Header: "Fund Type",
        id: "amount",
        accessor: "amount",
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Amount</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{defaultSpaceInTable(currencyFormatUpdate(props.value,CURRENCY_SYMBOL))}</span>
      },
      {
        id: "fund_type",
        accessor: "fund_type",
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Fund Type</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
      },
      {
        // Header: "Date Of Last Issue",
        id: "invoice_date",
        accessor: "invoice_date",
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Date Of Invoice</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{defaultSpaceInTable(props.value)}</span>
      },

      {
        accessor: "invoice_status",
        id: "invoice_status",
        headerClassName: "_align_c__ header_cnter_tabl",
        className: "_align_c__",
        width: 160,
        resizable: false,
        headerStyle: { border: "0px solid #fff" },
        Header: () => (
          <div>
            <div className="ellipsis_line__">Status</div>
          </div>
        ),
        Cell: props => (
          <div className="expander_bind">
            <div className="d-flex w-100 justify-content-center align-item-center">
              <span
                className={
                  "short_buttons_01 " +
                  (colorCodeInvoiceStatus[
                    _.lowerCase(props.original.invoice_status)
                  ]
                    ? colorCodeInvoiceStatus[
                        _.lowerCase(props.original.invoice_status)
                      ]
                    : "")
                }
              >
                {props.original.invoice_status}
              </span>
              {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
                                    <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                                    <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                                    <span className="short_buttons_01 btn_color_offline">Assigned</span>
                                
                                    <span className="short_buttons_01 btn_color_unavailable">Assigned</span>
                                <span className="short_buttons_01">Assigned</span> */}
            </div>
            {/* {props.isExpanded ? (
              <i
                className="icon icon-arrow-down icn_ar1"
                style={{ fontSize: "13px" }}
              ></i>
            ) : (
              <i
                className="icon icon-arrow-right icn_ar1"
                style={{ fontSize: "13px" }}
              ></i>
            )} */}
          </div>
        ),
        style: {
          cursor: "pointer",
          fontSize: 25,
          padding: "0",
          textAlign: "center",
          userSelect: "none"
        }
      }
    ];

    var selectOpt = [
      { value: "one", label: "One" },
      { value: "two", label: "Two" }
    ];

    function logChange(val) {
      //console.log("Selected: " + val);
    }

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <div className=" py-4">
              <span className="back_arrow">
                <a href="/admin/crm/participantadmin">
                  <span className="icon icon-back1-ie"></span>
                </a>
              </span>
            </div>

            <div className="by-1">
              <div className="row d-flex  py-4">
                <div className="col-lg-8">
                  <div className="h-h1 color">
                    {this.props.showPageTitle} {this.props[modeTypePropsRequest[this.state.modeType]] ? this.props[modeTypePropsRequest[this.state.modeType]]['name'] : ''}
                  </div>
                </div>
                <div className="col-lg-4 d-flex align-self-center">
                  {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                </div>
              </div>
            </div>
          </div>
        </div>

        <ProfilePage
          fundingShow={
            this.state.booked_by == "2" || this.state.booked_by == "3"
              ? true
              : false
          }
          pageType="creditnotes"
          details={
            modeTypePropsRequest.hasOwnProperty(this.state.modeType)
              ? this.props[modeTypePropsRequest[this.state.modeType]]
              : []
          }
          mode={
            this.state.modeType != "" ? this.state.modeType : "participaint"
          }
        />

        <div className="Finance__panel_1 mt-5">
          <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F">
            {/* <PanelGroup accordion id="accordion-example"> */}
            <Panel eventKey="1" defaultExpanded>
              <Panel.Heading>
                <Panel.Title toggle>
                  <div>
                    <p>Select for invoice Credit Notes</p>
                    <span className="icon icon-arrow-right"></span>
                    <span className="icon icon-arrow-down"></span>
                  </div>
                </Panel.Title>
              </Panel.Heading>
              <Panel.Body collapsible>
                <div className="row d-flex justify-content-center">
                  <div className="col-lg-8 col-sm-12 py-5">
                    <div className="row d-flex">
                      <form
                        method="post"
                        className="col-lg-7"
                        onSubmit={this.submitSearch}
                      >
                        <div className="search_bar right srchInp_sm actionSrch_st">
                          <input
                            type="text"
                            className="srch-inp"
                            placeholder="Search.."
                            onChange={e =>
                              this.setState({ search: e.target.value })
                            }
                            value={this.state.search}
                          />
                          <i
                            className="icon icon-search2-ie"
                            onClick={this.filterListing}
                          ></i>
                        </div>
                      </form>
                      {/* <form method="post" className="col-lg-7">
                                                <div className="search_bar right srchInp_sm actionSrch_st">
                                                    <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                                </div>
                                            </form> */}
                      <div className="col-lg-5">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="Fil_ter_ToDo">
                              <label>From</label>
                              <span>
                                <DatePicker
                                  selected={this.state.start_date}
                                  isClearable={true}
                                  onChange={value =>
                                    this.submitSearch("", "start_date", value)
                                  }
                                  placeholderText="00/00/0000"
                                  dateFormat="DD/MM/YYYY"
                                  autoComplete={"off"}
                                />
                              </span>
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="Fil_ter_ToDo">
                              <label>To</label>
                              <span>
                                <DatePicker
                                  selected={this.state.end_date}
                                  onChange={value =>
                                    this.submitSearch("", "end_date", value)
                                  }
                                  placeholderText="00/00/0000"
                                  isClearable={true}
                                  dateFormat="DD/MM/YYYY"
                                  autoComplete={"off"}
                                />
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row py-5">
                      <div className="col-lg-12">
                        <div className="bt-1"></div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-12 NDIS-Billing_tBL Credit-notes_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                          <ReactTable
                            expanded={this.state.expanded}
                            filtered={this.state.filtered}
                            TheadComponent={_ => null}
                            manual
                            onFetchData={this.fetchInvoiceList}
                            data={this.state.invoiceList}
                            columns={columns}
                            PaginationComponent={Pagination}
                            noDataText="No Record Found"
                            minRows={2}
                            defaultPageSize={this.state.invoiceList.length}
                            pages={this.state.pages}
                            loading={this.state.loading}
                            previousText={
                              <span className="icon icon-arrow-left privious"></span>
                            }
                            nextText={
                              <span className="icon icon-arrow-right next"></span>
                            }
                            showPagination={false}
                            className="-striped -highlight"
                            noDataText="No record found"
                            collapseOnDataChange={false}
                            ref={this.reactTable}
                            SubComponent={props => (
                              <div className="tBL_Sub">
                                <div className="tBL_des">
                                  <div className="row  d-flex">
                                    <div className="col-lg-4 col-sm-4">
                                      <label className="label_2_1_1">
                                        Amount
                                      </label>
                                      <input type="text" pattern={REGULAR_EXPRESSION_FOR_AMOUNT} value={this.state.selectedInvoiceId[props.original.id]['amount'] || ''} name={"amount_"+props.original.id} onChange={(e)=>this.onchangedData(e,props.original.id,'amount')} />
                                      {props.original.credit_note_from_used>0 ? <div>Credit limit Available {(props.original.amount-props.original.credit_note_from_used)} only</div> :<React.Fragment/>}
                                    </div>
                                    <div className="col-lg-8 col-sm-8">
                                      <label className="label_2_1_1">
                                        Item Description
                                      </label>
                                      <input type="text" />
                                    </div>
                                  </div>
                                </div>
                                {/* <a className="short_buttons_01 pull-right">
                                  View Task
                                </a>  */}
                              </div>
                            )
                          }
                          />
                        </div>
                      </div>
                    </div>
                   {this.state.totalCreditAvailabelAmount>0 ? <div className="row mt-5  d-flex">
                      <div className="col-lg-12 text-right">
                       
                        <span className="short_buttons_01 btn_color_avaiable F__bTN ">
                        Credit Amount Available {this.state.totalCreditAvailabelAmount}
                        </span>
                        
                      </div>
                        </div> :<React.Fragment/>}

                  </div>
                </div>
              </Panel.Body>
            </Panel>

            <Panel eventKey="2" defaultExpanded>
              <Panel.Heading>
                <Panel.Title toggle>
                  <div>
                    <p>Apply Credit Notes</p>
                    <span className="icon icon-arrow-right"></span>
                    <span className="icon icon-arrow-down"></span>
                  </div>
                </Panel.Title>
              </Panel.Heading>
              <Panel.Body collapsible>
                <div className="row d-flex justify-content-center">
                  <div className="col-lg-8 col-sm-12 py-5">
                    <div className="row d-flex mb-5">
                      <div className="col-lg-6 col-sm-6 align-self-end">
                        <label className="label_2_1_1">Apply To</label>
                        <div className="sLT_gray left left-aRRow">
                          <Select
                            name="form-field-name"
                            value="one"
                            options={selectOpt}
                            onChange={logChange}
                            clearable={false}
                            searchable={false}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-sm-4">
                        <label className="label_2_1_1">Amount</label>
                        <input type="text" />
                      </div>
                      <div className="col-lg-1 col-sm-1 align-self-center">
                        <label className="label_2_1_1">&nbsp;</label>
                        <div>
                          <button className="icon icon-add2-ie aDDitional_bTN_F0"></button>
                        </div>
                      </div>
                    </div>

                    <div className="row d-flex mb-5">
                      <div className="col-lg-6 col-sm-6 align-self-end">
                        <label className="label_2_1_1">Apply To</label>
                        <div className="sLT_gray left left-aRRow">
                          <Select
                            name="form-field-name"
                            value="one"
                            options={selectOpt}
                            onChange={logChange}
                            clearable={false}
                            searchable={false}
                          />
                        </div>
                      </div>
                      <div className="col-lg-4 col-sm-4">
                        <label className="label_2_1_1">Amount</label>
                        <input type="text" />
                      </div>
                      <div className="col-lg-1 col-sm-1 align-self-center">
                        <label className="label_2_1_1">&nbsp;</label>
                        <div>
                          <button className="icon icon-remove2-ie aDDitional_bTN_F1"></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Panel.Body>
            </Panel>

            {/* <PanelGroup/> */}
          </div>
        </div>

        <div className="row d-flex justify-content-end">
          <div className="col-lg-3">
            <a className="btn-1">Apply Credit Note</a>
          </div>
        </div>

        {/* <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8 col-sm-12">

                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Original Invoice Number</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Original Issue Date</label>
                                            <input type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Original Invoice Number</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Original Issue Date</label>
                                            <input type="text"/>
                                        </div>
                                    </div>


                                    <React.Fragment>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Credit Note Issued For</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Description</label>
                                                <input type="text"/>
                                            </div>
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Amount</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-4 col-sm-4 align-self-end">
                                                <div className="sLT_gray left left-aRRow">
                                                    <Select
                                                        name="form-field-name"
                                                        value="one"
                                                        options={selectOpt}
                                                        onChange={logChange}
                                                        clearable={false}
                                                        searchable={false}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-1 col-sm-1 align-self-center">
                                                <label className="label_2_1_1">&nbsp;</label>
                                                <div>
                                                    <button className="icon icon-add2-ie aDDitional_bTN_F0"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                    <React.Fragment>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Credit Note Issued For</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Description</label>
                                                <input type="text"/>
                                            </div>
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Amount</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-4 col-sm-4 align-self-end">
                                                <div className="sLT_gray left left-aRRow">
                                                    <Select
                                                        name="form-field-name"
                                                        value="one"
                                                        options={selectOpt}
                                                        onChange={logChange}
                                                        clearable={false}
                                                        searchable={false}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Invoice No (If Available)</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-1 col-sm-1 align-self-center">
                                                <label className="label_2_1_1">&nbsp;</label>
                                                <div>
                                                    <button className="icon icon-remove2-ie aDDitional_bTN_F1" disabled></button>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>

                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-8 col-sm-12">
                                            <label className="label_2_1_1">Add Notes To Quote</label>
                                            <textarea className="w-100"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> */}
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  showPageTitle: state.FinanceReducer.activePage.pageTitle,
  showTypePage: state.FinanceReducer.activePage.pageType,
  organisationDetails: state.FinanceReducer.organisationDetails,
  participantDetails: state.FinanceReducer.participantDetails,
  siteDetails: state.FinanceReducer.siteDetails
});
const mapDispatchtoProps = dispach => {
  return {
    getOrganisationDetails: (orgId, extraParms) =>
      dispach(getOrganisationDetails(orgId, extraParms)),
    getParticipantDetails: patId => dispach(getParticipantDetails(patId)),
    getSiteDetails: siteId => dispach(getSiteDetails(siteId))
  };
};

export default connect(mapStateToProps, mapDispatchtoProps)(CreateCreditNote);
