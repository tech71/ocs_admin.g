import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "service/Pagination.js";
import ProfilePage from '../common/ProfilePage';
import { connect } from 'react-redux';
import {toastMessageShow, postData,reFreashReactTable,archiveALL} from 'service/common.js';
import { getOrganisationDetails } from "./../action/FinanceAction.js";
import { ROUTER_PATH,BASE_URL,PAGINATION_SHOW } from "config.js";
import PDFViewerMY from 'service/PDFViewerMY.js';
import { Link } from "react-router-dom";


const urlRedirect = ROUTER_PATH+'admin/finance/payrollexemption';
const pdfBaseUrl = BASE_URL + 'mediaShowDocument/fe/';
const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {
        
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('finance/FinanceShiftAndPayRoll/get_payroll_exemption_history_list', Request).then((result) => {
            if (result.status) {
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }
        });
    });
};
class PayrollExemptionHistory extends React.Component {
    constructor(props) {
        super(props);
        let orgId = props.location.state!=undefined && props.location.state!=null && props.location.state.hasOwnProperty('orgId') && props.location.state.orgId>0 ? props.location.state.orgId : 0;
        
        this.state = {
            orgId:orgId,
            activeCol: '',
            payrollExemptionList: [],
            loading:false,
            filtered:{orgId:orgId},
            editNotes:{},
            expanded:{},
            loaderData:{},
            pdfLoad:false,
            pdfChange:false,

        }
        this.pdfLoaderData = {
            src:'',
            extraclassName:''
        };
        this.reactTable = React.createRef();
    }

 
    requestSendEmail = (requsetData) => {
        this.setState({loading:true},()=>{
            postData('finance/FinanceShiftAndPayRoll/send_renewal_email_payroll',requsetData).then((result) => {
                if (result.status) {
                    let msg = result.hasOwnProperty('msg') ? result.msg:'';
                    toastMessageShow(msg,'s');
                }else{
                    let msg = result.hasOwnProperty('error') ? result.error:(result.hasOwnProperty('msg')?result.msg:'');
                    let callBack={};
                    if(result.hasOwnProperty('refresh_data') && result.refresh_data){
                        callBack['close'] = ()=>{this.props.getOrganisationDetails(this.state.orgId,{type:'payrollExemption'});}
                    }
                    toastMessageShow(msg,'e',callBack);
                }
                this.setState({loading:false});
            });
        });
    }
    
    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                payrollExemptionList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
                editNotes:{}
            });
        })
    }

    takeEditNotes = (id,type,relN) =>{
        let statusType = type==1 ? true:false;
        let steteData = this.state.editNotes;

        steteData[id] = {status:statusType,text:relN,editContent:relN};
        this.setState({...this.state,editNotes:steteData});
    }

    changeNotes = (e,id) =>{
        let steteData = this.state.editNotes;
        let data = steteData[id];
        data['editContent'] = e.target.value;
        this.setState({...this.state,editNotes:{...this.state.editNotes,[id]:data}});
    }

    updateNotes = (id,noteId) =>{
        var postData= {};
        let steteData = this.state.editNotes;
        let data = steteData.hasOwnProperty(id) ?steteData[id]:'';
        let noteData = data!='' && typeof(data)=='object' && data!=null && data.hasOwnProperty('editContent') ? data.editContent : ''; 
        postData['exemption_id'] = id;
        postData['note'] = noteData ;
        postData['orgId'] = this.state.orgId;
        postData['note_id'] = noteId ;
        let msg = 'Are you sure, you want to save this note?';
        archiveALL(postData,msg,'finance/FinanceShiftAndPayRoll/update_payroll_exemption_note').then((result) => {
            if(result.status){
                reFreashReactTable(this, 'fetchData');
            }else{
                if(result.hasOwnProperty('error') || result.hasOwnProperty('msg')){
                    let msg = (result.hasOwnProperty('error') ? result.error : (result.hasOwnProperty('msg') ? result.msg :'')); 
                    toastMessageShow(msg,'e');
                 }
            }
        });
    }

    handleRowExpanded(newExpanded, index, event) {
        this.setState({pdfLoad:false},()=>{
           // console.log('loadData4',this.state);
            if(newExpanded[index]!==false){
                this.setState({
                    // we override newExpanded, keeping only current selected row expanded
                     expanded: { [index]: true },
                     pdfChange:true 
            
                    },()=>{
                        
                    })
            }else{
               // console.log('loadData6',this.state);
                this.setState({
                    // we override newExpanded, keeping only current selected row expanded
                     expanded: {},
                     pdfChange:false 
            
                    })
            }
            
        });
    }
    documentLoadUpdate =(filename)=>{
       // console.log('loadData7',this.state);
        let loadData = Object.assign({}, this.pdfLoaderData,{src:pdfBaseUrl+encodeURIComponent(btoa(this.state.orgId))+'/'+encodeURIComponent(btoa(filename))+'/Payroll Exempt Forms History'});
        this.setState({loaderData:loadData},()=>{
            this.setState({pdfLoad:true,pdfChange:false},()=>{
               // console.log('loadData5',this.state);
            });
        })
    }

    componentWillMount() {
        if(this.state.orgId>0){
          this.props.getOrganisationDetails(this.state.orgId,{type:'payrollExemption'});
        }else{
          this.setState({pageDisplay:false},()=>{
            toastMessageShow('Required parameter is missing.','e',{'close':()=>{window.location=urlRedirect;}});
          });
        }
      }


    render() {
       
        const columns = [
            {
                // Header: "Date Of Last Issue",
                id: "fin_file_title",
                accessor: "fin_file_title",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">File Name</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "fin_valid_from",
                accessor: "fin_valid_from",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Valid From</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "fin_valid_until",
                accessor: "fin_valid_until",
                headerClassName: '_align_c__ header_cnter_tabl sort_expander_custom',
                className:'_align_c__',
                width: 160,
                resizable: false,
                sortable:true,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () =>
                    <div className="sort_expander_custom_icons">
                        <div className="ellipsis_line__">Expiry Date</div>
                    </div>,
                Expander: (props) =>{
                   if(props.isExpanded && this.state.pdfChange){
                    this.documentLoadUpdate(props.original.fin_file);
                   }
                    return ( <div className="expander_bind">

                    <span className="w-100 text-center">{props.original.fin_valid_until}</span>

                    {props.isExpanded
                        ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                        : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}

    
                </div>);
                }
                   ,
                style: {
                    cursor: "pointer",
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }

            }

        ];
     
let exemption_id = this.props.organisationDetails!=undefined && this.props.organisationDetails!=null && typeof(this.props.organisationDetails)=='object' &&  this.props.organisationDetails.hasOwnProperty('finance_payroll_details') &&  this.props.organisationDetails.finance_payroll_details.hasOwnProperty('id') ? this.props.organisationDetails.finance_payroll_details.id : 0; 

        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href={urlRedirect}><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.organisationDetails.org_name +" " +this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <Link  to={{
                        state: {
                          orgId: this.state.orgId,
                          redirect_to:ROUTER_PATH+'admin/finance/payrollexemptionhistory'
                        },
                        pathname:
                          ROUTER_PATH +
                          "admin/finance/payrollexemption/certificate"
                      }} className="C_NeW_BtN w-100"><span>Upload New Certificate</span><i className="icon icon icon-add-icons"></i></Link>
                                </div>
                            </div>
                        </div>

                        <ProfilePage details={this.props.organisationDetails} payrollShow={true} mode="finance" sendReminderOnCall={()=>this.requestSendEmail({orgId:this.state.orgId,exemption_id:exemption_id})} />

                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-5 Finance-Exisiting-Payroll_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  odd_even_marge-2_tBL line_space_tBL H-Set_tBL">
                            {/* <ReactTable
                                data={payrollExempt}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">
                                        <div className="tBL_des">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled it to make a</div>
                                    <div className="text-right">
                                        <a className="short_buttons_01 mr-2">Edit Note</a>
                                        <a className="short_buttons_01 ">Add Note</a>
                                    </div>
                                    </div>}
                            /> */}
                            <ReactTable
                            PaginationComponent={Pagination}
                            showPagination={this.state.payrollExemptionList.length > PAGINATION_SHOW ? true : false}
                            ref={this.reactTable}
                            columns={columns}
                            manual
                            data={this.state.payrollExemptionList}
                            filtered={this.state.filtered}
                            defaultFilter={this.state.filtered}
                            pages={this.state.pages}
                            previousText={<span className="icon icon-arrow-left privious" ></span>}
                            nextText={<span className="icon icon-arrow-right next"></span>}
                            loading={this.state.loading}
                            onFetchData={this.fetchData}
                            noDataText="No payroll exemption found"
                            defaultPageSize={10}
                            className="-striped -highlight"
                            minRows={2}
                            collapseOnDataChange={false}
                            expanded={this.state.expanded}
                            onExpandedChange={(newExpanded, index, event) => this.handleRowExpanded(newExpanded, index, event)}
                            SubComponent={(props) =>{
                                let idData = props.original.hasOwnProperty('fin_id') &&  props.original.fin_id!='' && props.original.fin_id!=undefined ?props.original.fin_id:0;
                                return(
                                <div className="tBL_Sub">
                                    { 
                                        (this.state.editNotes[idData] && this.state.editNotes[idData].hasOwnProperty('status') &&this.state.editNotes[idData].status) ? (<div>
                                        <div className="mt-m-1"><textarea rows="3" className="w-100" value={(this.state.editNotes[idData] && this.state.editNotes[idData].hasOwnProperty('status') && this.state.editNotes[idData].status && this.state.editNotes[idData].hasOwnProperty('editContent')) ? this.state.editNotes[idData].editContent:'' } onChange={(e)=>this.changeNotes(e,idData)}></textarea></div>
                                        <div className="mt-m-1">
                                        <a className="under_l_tx pd-r-10" onClick={(e)=>this.takeEditNotes(idData,0,props.original.fin_note)}>Close</a>
                                        <a ><span className="btn cmn-btn1" onClick={(e)=>this.updateNotes(idData,props.original.fin_note_id)}>Save Notes</span></a>
                                        </div>
                                        </div>):(<React.Fragment><div className="tBL_des">{props.original.fin_note_id>0? props.original.fin_note:''}</div> <div className="text-right">
                                        {props.original.fin_note_id>0? <a className="short_buttons_01 mr-2" onClick={(e)=>this.takeEditNotes(idData,1,props.original.fin_note)}>Edit Note</a>: <a className="short_buttons_01" onClick={(e)=>this.takeEditNotes(idData,1,props.original.fin_note)}>Add Note</a>}
                                        </div></React.Fragment>)
                                    }
                                   
                                </div>
                                );

                            }
                                }
                        />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>
                    <div className="col-md-7">{this.state.pdfLoad?<PDFViewerMY loadData={this.state.loaderData}/>:<React.Fragment />}</div>
                    

                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType,
    organisationDetails: state.FinanceReducer.organisationDetails
  });
  const mapDispatchtoProps = dispach => {
    return {
      getOrganisationDetails: (orgId,extraParms) => dispach(getOrganisationDetails(orgId,extraParms))
    };
  };


export default connect(mapStateToProps, mapDispatchtoProps)(PayrollExemptionHistory);