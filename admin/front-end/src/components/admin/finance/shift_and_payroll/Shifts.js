import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "service/Pagination.js";
import { connect } from 'react-redux';
import moment from 'moment'
import { postData, archiveALL, reFreashReactTable } from 'service/common.js';
import { colorCodeFinanceShift, iconFinanceShift } from 'service/custom_value_data.js';
import { ROUTER_PATH, PAGINATION_SHOW } from 'config.js';
import _ from 'lodash';

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {

        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('finance/FinanceShiftAndPayRoll/get_shift_list', Request).then((result) => {
            if (result.status) {
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }
        });
    });
};




class Shifts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            staffList: [],
            filter_by: 'all',
        }
        this.reactTable = React.createRef();
    }

    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                staffList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    submitSearch = (e, key, value,checkTextBoxValue) => {
        if(e)
        e.preventDefault();

        

        
        var state = {};
        state[key] = value;
        
        this.setState(state, () => {
            if(checkTextBoxValue!=undefined && checkTextBoxValue){
                if((this.state.search && this.state.search.length>0) || (value== this.state.filter_by && this.state.filter_by=='all' && key=='filter_by') ){
                    this.filterListing();
                }
            }else{
                this.filterListing();
            }
        })
    }

    filterListing =()=>{
        let req = {search: this.state.search, filter_by: this.state.filter_by, start_date:this.state.start_date, end_date: this.state.end_date};
            this.setState({filtered: req})
    }


    render() {
             
               const columns = [
                {
                    id: "shift_id",
                    accessor: "shift_id",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Shift ID</div>
                        </div>
                    ,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    id: "shift_for",
                    accessor: "shift_for",
                    headerClassName: 'Th_class_d1 header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Shift For</div>
                        </div>
                    ,
                    className:'Tb_class_d1 Tb_class_d2 ',
                    Cell: props => <span className={(props.original.booked_from !='' && iconFinanceShift.hasOwnProperty(_.lowerCase(props.original.booked_from)) ? iconFinanceShift[_.lowerCase(props.original.booked_from)] :'')+' '+ ((props.original.booked_gender !='' ? props.original.booked_gender :''))}>
                        <i className="icon"></i>
                        <div>{props.value}</div>
                    </span>
    
                },
                {
                    id: "completed_by",
                    accessor: "completed_by",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Shift Completed By</div>
                        </div>
                    ,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    id: "shfit_date_format",
                    accessor: "shfit_date_format",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Date</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    // Header: "Fund Type",
                    id: "total_hours",
                    accessor: "total_hours",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Hours</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    // Header: "Date Of Last Issue",
                    id: "amount",
                    accessor: "amount",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Amount</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    // Header: "Timeframe",
                    id: "variance",
                    accessor: "variance",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Variance</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    // Header: "Status",
                    id: "work_area_type",
                    accessor: "work_area_type",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Work Area type</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    id: "shfit_status",
                    accessor: "shfit_status",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    className:'_align_c__',
                    width: 160,
                    resizable: false,
                    headerStyle: { border: "0px solid #fff" },
                    expander: true,
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Status</div>
                        </div>,
                    Expander: (props) =>{
                        let classStatus = colorCodeFinanceShift[props.original.shfit_status] ? colorCodeFinanceShift[props.original.shfit_status] :'';
                        return (
                        <div className="expander_bind justify-content-center">
    
                            <span className={"short_buttons_01 "+classStatus} >{_.capitalize(props.original.shfit_status)}</span>
                               {/*  <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                                <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                                <span className="short_buttons_01 btn_color_offline">Assigned</span>
                                <span className="short_buttons_01 btn_color_archive">Assigned</span>
                                <span className="short_buttons_01 btn_color_unavailable">Assigned</span>
                            <span className="short_buttons_01">Assigned</span> */}
    
                            {/*props.isExpanded
                                ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                                : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>*/} 
                        </div>);
                    },
                    style: {
                        cursor:'default',
                        fontSize: 25,
                        padding: "0",
                        textAlign: "center",
                        userSelect: "none"
                    }
    
                }
    
            ]

        var selectOpt = [
            { value: 'all', label: 'All' },
            { value: 'shift_id', label: 'Shift ID' },
            { value: 'shift_for', label: 'Shift For' },
            { value: 'completed_by', label: 'Shift Completed By' },
            { value: 'shfit_date_format', label: 'Date' },
            { value: 'total_hours', label: 'Hours' },
            { value: 'amount', label: 'Amount' },
            { value: 'variance', label: 'Variance' },
            { value: 'work_area_type', label: 'Work Area Type' },
            { value: 'shfit_status', label: 'Status' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href={ROUTER_PATH+"admin/finance/dashboard"}><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                   {/*  <a className="C_NeW_BtN w-100"><span>Create New Statements</span><i className="icon icon icon-add-icons"></i></a> */}
                                </div>
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                        <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-8 col-sm-8 ">
                        <form method="post" onSubmit={this.submitSearch}>
                            <div className="search_bar right srchInp_sm actionSrch_st">
                                <input type="text" className="srch-inp" placeholder="Search.." onChange={(e) => this.setState({search: e.target.value})} value={this.state.search} />
                                <i className="icon icon-search2-ie" onClick={this.filterListing}></i>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                    <div className="sLT_gray left left-aRRow">

                                <Select
                                    simpleValue={true}
                                    name="form-field-name"
                                    value={this.state.filter_by}
                                    options={selectOpt}
                                    onChange={(value) => this.submitSearch('', 'filter_by', value,true)}
                                    clearable={false}
                                    searchable={false}
                                />
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>From</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.start_date}
                                            isClearable={true}
                                            onChange={(value) => this.submitSearch('', 'start_date', value)}
                                            placeholderText="00/00/0000"
                                            dateFormat="DD/MM/YYYY"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>To</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.end_date}
                                            onChange={(value) => this.submitSearch('', 'end_date', value)}
                                            placeholderText="00/00/0000"
                                            isClearable={true}
                                            dateFormat="DD/MM/YYYY"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>


                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-12 Shift_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                            <ReactTable
                                /* data={ShiftQueries}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found" */

                                PaginationComponent={Pagination}
                                showPagination={this.state.staffList.length > PAGINATION_SHOW ? true : false}
                                ref={this.reactTable}
                                columns={columns}
                                manual
                                data={this.state.staffList}
                                filtered={this.state.filtered}
                                defaultFilter={this.state.filtered}
                                pages={this.state.pages}
                                previousText={<span className="icon icon-arrow-left privious" ></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                noDataText="No shift found"
                                defaultPageSize={10}
                                className="-striped -highlight"
                                minRows={2}
                    
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(Shifts);