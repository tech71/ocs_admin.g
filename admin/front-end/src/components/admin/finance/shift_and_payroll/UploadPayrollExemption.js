import React, { Component } from "react";
import { PanelGroup, Panel } from "react-bootstrap";
import ProfilePage from "./../common/ProfilePage";
import { connect } from "react-redux";
import { ROUTER_PATH, UPLOAD_MAX_SIZE, UPLOAD_MAX_SIZE_IN_MB,BASE_URL } from "config.js";
import { handleChangeSelectDatepicker, handleDateChangeRaw, postImageData, toastMessageShow} from "service/common.js";
import { getOrganisationDetails } from "./../action/FinanceAction.js";
import moment from "moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Dropzone from "react-dropzone";
import _ from "lodash";
import jQuery from "jquery";
import { Redirect } from "react-router-dom";


const allowedFileTypes = ["image/png", "application/pdf"];
const urlRedirect = ROUTER_PATH+'admin/finance/payrollexemption';
class UploadPayrollExemption extends React.Component {
  constructor(props) {
    super(props);
    let orgId = props.location.state!=undefined && props.location.state!=null && props.location.state.hasOwnProperty('orgId') && props.location.state.orgId>0 ? props.location.state.orgId : 0;
    let redirectTo = props.location.state!=undefined && props.location.state!=null && props.location.state.hasOwnProperty('redirect_to') && props.location.state.redirect_to!='' ? props.location.state.redirect_to : null;
    this.state = {
      orgId:orgId,
      activeCol: "",
      from_date: null,
      to_date: null,
      is_disabled: false,
      selected_file:null,
      file_title:'',
      selected_file_name:false,
      rejected_msg: false,
      pageDisplay:true,
      loading:false,
      redirectPage:false,
      redirect_to:redirectTo
    };
  }

  setRedirect = () => {
    this.setState({redirectPage:true});
  }


  renderRedirect = () => {

    let objectRedirectData ={pathname:this.state.redirect_to !=null &&  this.state.redirect_to!='' ?  this.state.redirect_to :urlRedirect};
    if(this.state.redirect_to !=null &&  this.state.redirect_to!='' && this.state.redirect_to.indexOf('payrollexemptionhistory')>-1){
      objectRedirectData['state'] = {
        orgId: this.state.orgId,
        
      };
    }
    
    if(this.state.redirectPage){
       return(<Redirect to={{...objectRedirectData}} />);
    }
    
  }
  componentWillMount() {
    if(this.state.orgId>0){
      this.props.getOrganisationDetails(this.state.orgId);
    }else{
      this.setState({pageDisplay:false},()=>{
        toastMessageShow('Required parameter is missing.','e',{'close':()=>{window.location=urlRedirect;}});
      });
    }
  }
  onDropAddFiles = (acceptedFiles, rejectedFiles) => {
    if (rejectedFiles.length > 0) {
      
      let errormsg = false;
      if (
        rejectedFiles.length > 0 &&
        !_.includes(allowedFileTypes, rejectedFiles[0].type)
      ) {
        errormsg = "The filetype you are attempting to upload is not allowed.";
      } else if (
        rejectedFiles.length > 0 &&
        rejectedFiles[0].size > UPLOAD_MAX_SIZE
      ) {
        errormsg =
          "The file you are attempting to upload is larger than the permitted size (" +
          UPLOAD_MAX_SIZE_IN_MB +
          " MB).";
      } else {
        errormsg = "The filetype you are attempting to upload is not allowed.";
      }
      this.setState({ rejected_msg: errormsg, selected_file:null,selected_file_name:false});
    }else if(acceptedFiles.length>0){
      this.setState({rejected_msg:false,selected_file:acceptedFiles[0],selected_file_name:acceptedFiles[0].name});
    }
  };

  
 uploadHandler = e => {
    e.preventDefault();
    jQuery("#payroll_exemption_add_from").validate({ ignore: [] });
    if (jQuery("#payroll_exemption_add_from").valid()) {
     this.setState({ loading: true });
      const formData = new FormData();
      formData.append("docsFile", this.state.selected_file);
      formData.append("orgId", this.state.orgId);
      formData.append("to_date", this.state.to_date);
      formData.append("from_date", this.state.from_date);
      formData.append("file_title", this.state.file_title);


     postImageData(
        "finance/FinanceShiftAndPayRoll/upload_attachment_docs",
        formData
      )
        .then(responseData => {
      
          var x = responseData;
          if (responseData.status) {
            
            let msg = responseData.hasOwnProperty('msg') ? responseData.msg :'Document uploaded successfully';
            toastMessageShow(msg,'s',{'close':()=>{this.setState({ loading: false,redirectPage:true })}});
          } else {
            toastMessageShow(x.error,'e');
            this.setState({ loading: false });
          }
        })
        .catch(error => {
          toastMessageShow("Api Error",'e');
          this.setState({ loading: false });
        }); 
    }
  };

  render() {
   
    let configStartDate = this.state.to_date && this.state.to_date != "0000/00/00" ? {maxDate:moment(this.state.to_date)} :{};
    return (
      <React.Fragment>
        {this.state.redirectPage?this.renderRedirect():null}
      {!this.state.pageDisplay ? null :
        (<React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <div className=" py-4">
            <span className="back_arrow">
               <a onClick={()=>this.setRedirect()}>
              
                <span className="icon icon-back1-ie"></span>
               
               </a> 
               </span>
            </div>

            <div className="by-1">
              <div className="row d-flex  py-4">
                <div className="col-lg-12">
                  <div className="h-h1 color">
                    {"Add " +
                      this.props.organisationDetails.org_name +
                      " " +
                      this.props.showPageTitle}
                  </div>
                </div>
            
              </div>
            </div>
          </div>
        </div>

        <ProfilePage details={this.props.organisationDetails} mode="finance" />
        <form id="payroll_exemption_add_from" method="post" autoComplete="off">
          <div className="row d-flex justify-content-center mt-5">
            <div className="col-lg-12">
              <div className="Bg_F_moule">
                <div className="row d-flex justify-content-center">
                  <div className="col-lg-6 col-ms-6 col-xs-12">
                    <div className="row mt-5 pb-2">
                      <div className="col-lg-6 col-sm-6">
                        <label className="label_2_1_1">Valid From</label>
                        <span  className="required">
                        <div className="datepicker_my">
                        <DatePicker
                          showYearDropdown
                          scrollableYearDropdown
                          yearDropdownItemNumber={18}
                          dateFormat="DD/MM/YYYY"
                          required={true}
                          data-placement={"bottom"}
                          {...configStartDate}
                          minDate={moment()}
                          name={"from_date"}
                          autoComplete={'off'}
                          onChange={e =>
                            handleChangeSelectDatepicker(this, e, "from_date")
                          }
                          selected={
                            this.state.from_date &&
                            this.state.from_date != "0000/00/00"
                              ? moment(this.state.from_date)
                              : null
                          }
                          className="csForm_control text-center bl_bor"
                          placeholderText="DD/MM/YYYY"
                          disabled={this.state.is_disabled}
                          onChangeRaw={handleDateChangeRaw}
                        />
                        </div>
                        </span>
                      </div>
                      <div className="col-lg-6 col-sm-6 ">
                        <label className="label_2_1_1">Valid To</label>
                        <span  className="required">
                        <div className="datepicker_my">
                        <DatePicker
                          showYearDropdown
                          scrollableYearDropdown
                          yearDropdownItemNumber={18}
                          dateFormat="DD/MM/YYYY"
                          required={true}
                          data-placement={"bottom"}
                          autoComplete={'off'}
                          minDate={
                            this.state.from_date &&
                            this.state.from_date != "0000/00/00"
                              ? moment(this.state.from_date)
                              : moment()
                          }
                          popperPlacement="bottom-end"
                          name={"to_date"}
                          onChange={e =>
                            handleChangeSelectDatepicker(this, e, "to_date")
                          }
                          selected={
                            this.state.to_date &&
                            this.state.to_date != "0000/00/00"
                              ? moment(this.state.to_date)
                              : null
                          }
                          className="csForm_control text-center bl_bor"
                          placeholderText="DD/MM/YYYY"
                          disabled={this.state.is_disabled}
                          onChangeRaw={handleDateChangeRaw}
                        />
                        </div>
                        </span>
                      </div>
                    </div>

                    <div className="row mt-5">
                      <div className="col-lg-12 col-sm-12 file_drag_validation__">
                      <input type="text " readOnly className="hide" value={(!this.state.selected_file_name? '' : this.state.selected_file_name)} name={"file_upload"} id={"file_upload"} data-rule-required={true} />
                      <span  className="required">
                        <div className="File-Drag-and-Drop_bOX position-relative" >
                         <Dropzone
                            onDrop={(files, rejected) =>
                              this.onDropAddFiles(files, rejected)
                            }
                            multiple={false}
                            accept={allowedFileTypes}
                            minSize={0}
                            maxSize={UPLOAD_MAX_SIZE}
                            autoDiscover={false}
                          >
                            {props => {
                              return (
                                <div
                                  className="custom-file-upload"
                                  {...props.getRootProps()}
                                >
                                  <input  type="text"  {...props.getInputProps()}/>
                                  {!this.state.selected_file_name ?  (<React.Fragment>
                                    <div className="Drop_bOX_content_1">
                                      <i className="icon icon-if-ic-file-upload-48px-352345"></i>
                                      <span>
                                        <h3>Drag &amp; Drop</h3>
                                        <div>
                                          or <span>Browse</span> for your files
                                        </div>
                                      </span>
                                    </div>
                                  </React.Fragment>):

                                  (<React.Fragment>
                                    <div className="Drop_bOX_content_2">
                                      <i className="icon icon-document3-ie"></i>
                                      <span>
                                        <div>{this.state.selected_file_name}</div>
                                      </span>
                                    </div>
                                  </React.Fragment>)}
                                </div>
                              );
                            }}
                          </Dropzone>
                          {this.state.rejected_msg ? (
                            <div className="row mt-5 d-flex justify-content-center">
                              <div className="col-lg-9 col-sm-12 text-center">
                                <div className="cUS_error_class error_cLass">
                                  <i className="icon icon-alert"></i>
                                  <span>Error, {this.state.rejected_msg}</span>
                                </div>
                              </div>
                            </div>
                          ) : (
                            <React.Fragment />
                          )}
                        </div>
                       </span>
                      </div>
                    </div>

                    <div className="row mt-5 d-none">
                      <div className="col-lg-12 col-sm-12">
                        <div className="Progress-bar_mYcustom">
                          <div className="_mYcustom_1">
                            <div
                              className="_mYcustom_Bar"
                              style={{ width: "10%" }}
                            >
                              <span>40 % Upload File</span>
                            </div>
                          </div>
                        </div>
                        <div className="Progress-bar_mYcustom cNTer_mYcustom">
                          <div className="_mYcustom_1">
                            <div
                              className="_mYcustom_Bar"
                              style={{ width: "10%" }}
                            >
                              <span>40 % Upload File</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div className="row mt-5 d-flex justify-content-center">
                      <div className="col-lg-12 col-sm-12">
                        <label className="label_2_1_1">File Title</label>
                        
                        <span className="required">
                            <input
                              type="text"
                              className="csForm_control"
                              placeholder="File Title"
                              name="file_title"
                              maxLength="60"
                              required={true}
                              onChange={e => this.setState({ file_title: e.target.value })}
                              value={this.state.file_title || ""}
                            />
                          </span>
                      </div>
                    </div>

                    <div className="row mt-5 d-flex justify-content-end">
                      <div className="col-lg-5">
                        <button type="submit" className="but" onClick={(e)=>this.uploadHandler(e)} disabled={this.state.loading}>Save & Upload</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form></React.Fragment>)}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  showPageTitle: state.FinanceReducer.activePage.pageTitle,
  showTypePage: state.FinanceReducer.activePage.pageType,
  organisationDetails: state.FinanceReducer.organisationDetails
});
const mapDispatchtoProps = dispach => {
  return {
    getOrganisationDetails: orgId => dispach(getOrganisationDetails(orgId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchtoProps
)(UploadPayrollExemption);
