import React, { Component, Fragment } from "react";
import "react-select-plus/dist/react-select-plus.css";
import "react-datepicker/dist/react-datepicker.css";
import { queryOptionData,} from "service/common.js";
import Select from "react-select-plus";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ROUTER_PATH } from "config.js";

const getOptionNewOrganisation = e => {
  /*     if (!e) {
        return Promise.resolve({ options: [] });
    }

    return postData('finance/FinanceUserManagement/get_new_finance_user_name', { search: e }).then((json) => {
        return { options: json };
    }); */
  return queryOptionData(
    e,
    "finance/FinanceCommon/get_organisation_name_payroll_exemption",
    { search: e },
    2,
    1
  );
};

class SelectOrganisation extends Component {
  constructor() {
    super();
    this.state = {
      organisation: []
    };
  }

  selectChange = e => {
    let valData =
      e != undefined &&
      e != null &&
      typeof e == "object" &&
      e.hasOwnProperty("value")
        ? e
        : [];
    this.setState({ organisation: valData });
  };

  render() {
    return (
      <React.Fragment>
        
        {this.state.organisation.hasOwnProperty("value") &&
                  this.state.organisation.value > 0 ? (
                    <Redirect
                      to={{
                        state: {
                          orgId: this.state.organisation.value
                        },
                        pathname:
                          ROUTER_PATH +
                          "admin/finance/payrollexemption/certificate"
                      }}
                      disabled={this.state.loading}
                      onClick={this.onSubmit}
                      className="btn cmn-btn1 creat_task_btn__"
                   />
                  ) : (
                    ""
                  )}

      <div
      className={
        "cmn_select_dv srch_select12 vldtn_slct " +
        (this.state.organisation.hasOwnProperty("value")
          ? "remove-search"
          : "")
      }
    >
      <Select.Async
        cache={false}
        name="form-field-name"
        clearable={
          this.state.organisation.hasOwnProperty("value")
            ? true
            : false
        }
        searchable={
          this.state.organisation.hasOwnProperty("value")
            ? false
            : true
        }
        value={this.state.organisation}
        loadOptions={e => getOptionNewOrganisation(e)}
        placeholder="Search"
        onChange={e => this.selectChange(e)}
        required={true}
      />
    </div>
    </React.Fragment>
                   
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(SelectOrganisation);
