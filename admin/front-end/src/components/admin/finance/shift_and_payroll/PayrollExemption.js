import React, { Component, createRef } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "service/Pagination.js";
import { connect } from 'react-redux'
import SelectOrganisation from './SelectOrganisation.js';
import { postData, archiveALL, reFreashReactTable,toastMessageShow } from 'service/common.js';
import { ROUTER_PATH, PAGINATION_SHOW } from 'config.js';
import _ from 'lodash';
import { colorCodeFinancePayroll,checkBoxFinancePayrollExemptionAllowStatus,warningIconColorCodeFinancePayroll, warningIconColorFinancePayrollExemptionShowAllowStatus } from 'service/custom_value_data.js';
import { Link } from "react-router-dom";

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {

        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('finance/FinanceShiftAndPayRoll/get_payroll_exemption_list', Request).then((result) => {
            if (result.status) {
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }
        });
    });
};

const selectOpt = [
    { value: 'all', label: 'All' },
    { value: 'org_name', label: 'Org name' },
    { value: 'fin_valid_from', label: 'Valid From' },
    { value: 'fin_valid_until', label: 'Valid Until' },
    { value: 'days_until_expire', label: 'Days Until Expiry' },
    { value: 'fin_status', label: 'Status' }
];

class PayrollExemption extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            payrollExemptionList: [],
            filter_by: 'all',
            openOrganisationModal:false,
            selected: {},
            selectAll: 0,
            loading:false
        }
        this.reactTable = React.createRef();
    }

    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                payrollExemptionList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    submitSearch = (e, key, value,checkTextBoxValue) => {
        if(e){
            e.preventDefault();
        }
        var state = {};
        state[key] = value;
        
        this.setState(state, () => {
            if(checkTextBoxValue!=undefined && checkTextBoxValue){
                if((this.state.search && this.state.search.length>0) || (value== this.state.filter_by && this.state.filter_by=='all' && key=='filter_by') ){
                    this.filterListing();
                }
            }else{
                this.filterListing();
            }
        })
    }

    filterListing =()=>{
        let req = {search: this.state.search, filter_by: this.state.filter_by, start_date:this.state.start_date, end_date: this.state.end_date};
            this.setState({filtered: req})
    }
    
    closeOrganisationModal = (status) => {
        this.setState({ openOrganisationModal: false },()=>{
            if(status){

            }
        });
    }

    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        if(this.state.selected[id]){
            delete newSelected[id];
        }else{
            newSelected[id] = true;  
        }

        this.setState({
            selected: newSelected,
            selectAll: 2
        },()=>{console.log('dsadasd',this.state)});
    }

    toggleSelectAll = () => {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.payrollExemptionList.filter((row) => _.includes(checkBoxFinancePayrollExemptionAllowStatus,row.fin_status)).forEach(x => {
                newSelected[x.id] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    inactivateSelected=()=>{
        if(Object.keys(this.state.selected).length<=0){
            toastMessageShow('Please select atleast one row for inactive.','e');
        }else if(Object.keys(this.state.selected).length>0){
            this.setState({ loading: true },()=>{
          
            archiveALL({selectedData:Object.keys(this.state.selected)},'Are you sure, you want update Payroll Exemption status to Inactive?','finance/FinanceShiftAndPayRoll/set_inactive_payroll_exemption').then((result) => {
                if (result.status) {
                    this.setState({
                        selected: {},
                        selectAll: 0,
                        loading: false
                    },()=>{
                        reFreashReactTable(this, 'fetchData'); 
                    });
                }else{
                    let msg = result.hasOwnProperty('error') ? result.error:(result.hasOwnProperty('msg')?result.msg:'');
                    toastMessageShow(msg,'e');
                    this.setState({ loading: false });
                }
            });
        });
        }
    }

    render() {
        const columns = [
            {
                // id: "org_name",
                // accessor: "org_name",
                headerClassName: 'Th_class_d1 header_cnter_tabl checkbox_header',
                className:'Tb_class_d1 Tb_class_d2 br-0',
                Cell: (props) => {
                    let classData = warningIconColorCodeFinancePayroll[props.original.fin_status] ? warningIconColorCodeFinancePayroll[props.original.fin_status] :'';
                    return (
                        <span>
                            <label className="Cus_Check_1" >
                                {_.includes(checkBoxFinancePayrollExemptionAllowStatus,props.original.fin_status)?<input type='checkbox'  checked={this.state.selected[props.original.id] === true} onChange={() => this.toggleRow(props.original.id)} /> :<React.Fragment/>}
                                <div className="chk_Labs_1"  style={{paddingRight:'0px'}}></div>
                            </label>
                            {_.includes(warningIconColorFinancePayrollExemptionShowAllowStatus,props.original.fin_status) ? <i className={"icon icon-alert ml-2 "+classData}></i>:<React.Fragment/>}
                            
                        </span>
                        
                    );
    
                },
                Header: x => {
                    return (
                        <div className="Tb_class_d1 Tb_class_d2">
                            <span >
                                <label className="Cus_Check_1">
                                <input type='checkbox' className="checkbox1" checked={this.state.selectAll === 1} ref={input => {
                                    if(input){input.indeterminate = this.state.selectAll === 2}
                                }}
                                onChange={() => this.toggleSelectAll()} /><div className="chk_Labs_1" style={{paddingRight:'0px'}}></div>
                                </label>
                                {/* <div>Org Name</div> */}
                            </span>
                        </div>
                    );

                },
                resizable: false,
                sortable:false,
                width: 55
            },
            {
                id: "org_name",
                accessor: "org_name",
                headerClassName: '_align_c__ header_cnter_tabl _align_l__',
                Header: () =>
                    <div className="text">
                        <div className="ellipsis_line__">Org Name</div>
                    </div>
                ,
                className:'_align_c__ _align_l__',
                Cell: props => <span><Link to={{
                    state: {
                      orgId: props.original.organisation_id
                    },
                    pathname:
                      ROUTER_PATH +
                      "admin/finance/payrollexemptionhistory"
                  }}>
                   {/* {props.value} */}
                   {props.value}</Link></span>
            },
            {
                id: "fin_valid_from",
                accessor: "fin_valid_from",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Valid From</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "fin_valid_until",
                accessor: "fin_valid_until",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Valid Until</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "days_until_expire",
                accessor: "days_until_expire",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Days Until Expiry</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "fin_status",
                accessor: "fin_status",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Status</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: (props) => { 
                    let classData = colorCodeFinancePayroll[props.original.fin_status] ? colorCodeFinancePayroll[props.original.fin_status] :'';
                    return (<span>
                            <span className={"short_buttons_01 "+classData}>{_.capitalize(props.original.fin_status)}</span>
                        </span>);
                    }
            },
           

        ];

        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href={ROUTER_PATH+"admin/finance/dashboard"}><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>

                                <div className="col-lg-3 d-flex align-self-center">
                                    {!this.state.openOrganisationModal ?
                                        <a  onClick={() => this.setState({ openOrganisationModal: true })} className="C_NeW_BtN w-100">
                                            <span>Add New Org</span><i className="icon icon icon-add-icons"></i>
                                        </a> :
                                        <div className="modify_select align-self-center w-100">
                                           {this.state.openOrganisationModal ? <SelectOrganisation showModal={this.state.openOrganisationModal} closeModal={this.closeOrganisationModal} /> :<React.Fragment />}
                                          </div>}
                                </div>

                            </div>

                        </div>

                        <div className="bb-1 mb-4">
                        <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-8 col-sm-8 ">
                    <form method="post" onSubmit={this.submitSearch}>
                            <div className="search_bar right srchInp_sm actionSrch_st">
                                <input type="text" className="srch-inp" placeholder="Search.." onChange={(e) => this.setState({search: e.target.value})} value={this.state.search} />
                                <i className="icon icon-search2-ie" onClick={this.filterListing}></i>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                    <div className="sLT_gray left left-aRRow">

                            <Select
                                    simpleValue={true}
                                    name="form-field-name"
                                    value={this.state.filter_by}
                                    options={selectOpt}
                                    onChange={(value) => this.submitSearch('', 'filter_by', value,true)}
                                    clearable={false}
                                    searchable={false}
                                />
                            </div>
                        </div>

                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>From</label>
                                    <span>
                                    <DatePicker
                                            selected={this.state.start_date}
                                            isClearable={true}
                                            onChange={(value) => this.submitSearch('', 'start_date', value)}
                                            placeholderText="00/00/0000"
                                            dateFormat="DD/MM/YYYY"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>To</label>
                                    <span>
                                    <DatePicker
                                            selected={this.state.end_date}
                                            onChange={(value) => this.submitSearch('', 'end_date', value)}
                                            placeholderText="00/00/0000"
                                            isClearable={true}
                                            dateFormat="DD/MM/YYYY"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-12 Pay-Exemption_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                        <ReactTable
                            PaginationComponent={Pagination}
                            showPagination={this.state.payrollExemptionList.length > PAGINATION_SHOW ? true : false}
                            ref={this.reactTable}
                            columns={columns}
                            manual
                            data={this.state.payrollExemptionList}
                            filtered={this.state.filtered}
                            defaultFilter={this.state.filtered}
                            pages={this.state.pages}
                            previousText={<span className="icon icon-arrow-left privious" ></span>}
                            nextText={<span className="icon icon-arrow-right next"></span>}
                            loading={this.state.loading}
                            onFetchData={this.fetchData}
                            noDataText="No payroll exemption found"
                            defaultPageSize={10}
                            className="-striped -highlight"
                            minRows={2}
                        />
                        </div>
                    </div>

                </div>

                <div className="row d-flex justify-content-end">

                <div className="col-lg-3 d-flex align-self-center">
                    <button className="C_NeW_BtN w-100" disabled={(Object.keys(this.state.selected).length<=0 || this.state.loading)? true:false} onClick={this.inactivateSelected}><span>Inactivate Selected</span><i className="icon icon icon-archive-im"></i></button>
                    </div>
                </div>
            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(PayrollExemption);