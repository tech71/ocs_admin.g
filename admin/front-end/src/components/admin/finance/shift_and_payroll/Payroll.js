import React, { Component } from "react";
import ReactTable from "react-table";
import Select from "react-select-plus";
import DatePicker from "react-datepicker";
import Pagination from "service/Pagination.js";
import { Chart } from "react-google-charts";
import MonthChart from "./../common/MonthChart";
import CustomChartBar from "./../common/CommonBarChart";
import ScrollArea from "react-scrollbar";
import classNames from "classnames";
import { connect } from "react-redux";
import { handleChangeSelectDatepicker,handleDateChangeRaw,postData} from 'service/common.js';
import { NottachmentAvailable } from 'service/custom_value_data.js';
import moment from "moment";
import _ from "lodash";

const requestData = (filtered) => {
    return new Promise((resolve) => {

        var Request = { filtered: filtered };
        postData('finance/FinanceShiftAndPayRoll/get_payroll_fetch', Request).then((result) => {
            if (result.status) {
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }else{
                const res = { rows: [], pages: 0 };
                resolve(res);
            }
        });
    });
};
const dummyData=[
  ["Qty.", "This Year", "Last Year", "Two Years Ago"],
  ['',0,0,0]
];

const CustomTbodyComponent = props => (
  <div {...props} className={classNames("rt-tbody", props.className || [])}>
    <div className="cstmSCroll1 FScroll">
      <ScrollArea
        speed={0.8}
        contentClassName="content"
        horizontal={false}
        style={{ paddingRight: "15px", maxHeight: "400px" }}
      >
        {props.children}
      </ScrollArea>
    </div>
  </div>
);

class Payroll extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeCol: "",
      filter_by: "year",
      dateShow: false,
      loading: false,
      loadingGraph: false,
      from_date: null,
      to_date: null,
      payrollList:[],
      chartData:dummyData
    };
  }
  componentWillMount(){
    this.setFilterListing(this.state.filter_by,true);
    this.setState({loadingGraph:true},()=>{
      this.requestGraphData();
    })
  }

  requestGraphData = () => {
    postData('finance/FinanceShiftAndPayRoll/get_payroll_graph_fetch').then((result) => {
        if (result.status) {
          this.setState({chartData:result.data});
        }else{
          this.setState({chartData:dummyData});
        }
        this.setState({loadingGraph:false});
    });
  }

  

  fetchData = (state) => {
    this.setState({ loading: true });
    requestData(
        state.filtered
    ).then(res => {
        this.setState({
            payrollList: res.rows,
            //all_count: res.all_count,
            pages: res.pages,
            loading: false,
        });
    })
}
  submitSearch = (e, key, value, checkTextBoxValue) => {
    if (e) {
      e.preventDefault();
    }
    var state = {};
    state[key] = value;

    this.setState(state, () => {
      if (checkTextBoxValue != undefined && checkTextBoxValue) {
        if (
          value == this.state.filter_by &&
          this.state.filter_by != "custom" &&
          key == "filter_by"
        ) {
          //this.filterListing();
          this.setState({ dateShow: false },()=>{
              this.setFilterListing(value);
          });
        } else if (
          value == this.state.filter_by &&
          this.state.filter_by == "custom" &&
          key == "filter_by"
        ) {
          this.setState({ dateShow: true,to_date:null, from_date:null});
        }
      } 
    });
  };
  setFilterListing =(type,stopFiltreing)=>{
    let from_date,to_date =null;
      switch(type){
            case 'year':
              if(moment().month()>=6){
                from_date = moment().month(6).startOf('month');
                to_date = moment().month(5).endOf('month').add('years',1);
              }else{
                from_date = moment().month(6).startOf('month').subtract('years',1);
                to_date = moment().month(5).endOf('month');
              }
            break;
            case 'this month':
                from_date = moment().startOf('month');
                to_date = moment().endOf('month');
            break;
            case 'last month':
                from_date = moment().subtract(1, 'months').startOf('month');
                to_date = moment().subtract(1, 'months').endOf('month');
            break;
            case 'last three months':
                from_date = moment().subtract(3, 'months').startOf('month');
                to_date = moment().endOf('month');
            break;
      }
      if(from_date!=null && to_date!=null){
        this.setState({from_date:from_date,to_date:to_date},()=>{
          if(stopFiltreing==undefined|| (stopFiltreing!=undefined && !stopFiltreing)){
            this.filterListing();
          }
        })
      }
  }
  filterListing =()=>{
        let req = {filter_by: this.state.filter_by, from_date:this.state.from_date, to_date: this.state.to_date};
        this.setState({filtered: req})
 }

 defaultFilteredData = () => {
  let data = {};
  data['filter_by'] = 'year';
  data['from_date'] = moment().month(6).startOf('month');
  data['to_date'] =  moment().month(5).endOf('month').add('years',1);

  return data;
}

  render() {
    const selectOpt = [
        { value: "year", label: "Year To Date" },
        { value: "this month", label: "This Month" },
        { value: "last month", label: "Last Month" },
        { value: "last three months", label: "Last Three Months" },
        { value: "custom", label: "Custom Range" }
      ];
    const columns = [
      {
        id: "number",
        accessor: "number",
        sortable:false,
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Number</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{_.padStart(props.value,2,0)}</span>
      },
      {
        id: "des",
        accessor: "des",
        sortable:false,
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Description</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => (
          <span>
            <div>
              <div className="ellipsis_line__">{props.value}</div>
            </div>
          </span>
        )
      },
      {
        id: "amount",
        sortable:false,
        accessor: "amount",
        headerClassName: "_align_c__ header_cnter_tabl",
        Header: () => (
          <div>
            <div className="ellipsis_line__">Amonut</div>
          </div>
        ),
        className: "_align_c__",
        Cell: props => <span>{props.value}</span>
      }
    ];

  

    function logChange(val) {
      //console.log("Selected: " + val);
    }

    const chartData = [
      ["Qty.", "This Year", "Last Year", "Two Years Ago"],
      ["Jan", 2500, 5000, 2000],
      ["Feb", 2500, 5000, 2000],
      ["Mar", 2500, 5000, 2000],
      ["Apr", 2500, 5000, 2000],
      ["May", 2500, 5000, 2000],
      ["Jun", 2500, 5000, 2000],
      ["Jul", 2500, 5000, 2000],
      ["Aug", 2500, 5000, 2000],
      ["Sep", 2500, 5000, 2000],
      ["Oct", 2500, 5000, 2000],
      ["Nov", 2500, 5000, 2000],
      ["Dec", 2500, 5000, 2000]
    ];

    const setChartOptions = {
      chartArea: { width: "80%" },
      isStacked: false,
      legend: { position: "bottom", alignment: "center" },
      backgroundColor: "#ececec",
      colors: ["#43c148", "#5291d6", "#9855d5"],
      vAxis: {
        gridlines: {
          color: "none"
        },
        title: 'Some of Payroll',
      },
      hAxis: {
        title: 'Over time',
      }
    };

    let configStartDate = this.state.to_date && this.state.to_date != "0000/00/00" ? {maxDate:moment(this.state.to_date)} :{};
    let applyButtonShow = (this.state.filter_by=='custom' && this.state.to_date && this.state.to_date != "0000/00/00" && this.state.to_date!=null && this.state.from_date && this.state.from_date != "0000/00/00" && this.state.from_date!=null) ? true : false;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-lg-12">
            <div className=" py-4 invisible">
            <span className="back_arrow">
              <a href="/admin/crm/participantadmin">
                <span className="icon icon-back1-ie"></span>
              </a>
              </span>
            </div>

            <div className="by-1">
              <div className="row d-flex  py-4">
                <div className="col-lg-6">
                  <div className="h-h1 color">{this.props.showPageTitle}</div>
                </div>
                {/* <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Import CSV File</span><i className="icon icon icon-download2-ie"></i></a>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Add New Line Item</span><i className="icon icon icon-add-icons"></i></a>
                                </div> */}
              </div>
            </div>

            <div className="bb-1 mb-4">
              <div className="row sort_row1-- after_before_remove">
                {/* <div className="col-lg-6 col-md-8 col-sm-8 ">
                                    <form method="post">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div> */}
                <div className="col-lg-3 col-md-4 col-sm-4 ">
                <div className="sLT_gray left left-aRRow">

                      <Select
                        name="form-field-name"
                        value={this.state.filter_by}
                        options={selectOpt}
                        onChange={logChange}
                        onChange={value =>
                          this.submitSearch("", "filter_by", value, true)
                        }
                        simpleValue={true}
                        clearable={false}
                        searchable={false}
                      />
                    </div>
                 
                </div>
                {this.state.dateShow ? (
                  <div className="col-lg-4 col-md-4 col-sm-4 ">
                    <div className="row">
                      <div className="col-md-5">
                        <div className="Fil_ter_ToDo">
                          <label>From</label>
                          <span>
                          <DatePicker
                          showYearDropdown
                          scrollableYearDropdown
                          yearDropdownItemNumber={18}
                          dateFormat="DD/MM/YYYY"
                          required={true}
                          data-placement={"bottom"}
                          autoComplete={'off'}
                          {...configStartDate}
                          
                          name={"from_date"}
                          onChange={e =>
                            handleChangeSelectDatepicker(this, e, "from_date")
                        }
                          selected={
                              this.state.from_date &&
                            this.state.from_date != "0000/00/00"
                            ? moment(this.state.from_date)
                              : null
                            }
                          className="csForm_control text-center bl_bor"
                          placeholderText="DD/MM/YYYY"
                          disabled={this.state.is_disabled}
                          onChangeRaw={handleDateChangeRaw}
                        />
                          </span>
                        </div>
                      </div>
                      <div className="col-md-5">
                        <div className="Fil_ter_ToDo">
                          <label>To</label>
                          <span>
                            <DatePicker
                              showYearDropdown
                              scrollableYearDropdown
                              yearDropdownItemNumber={18}
                              autoComplete={'off'}
                              dateFormat="DD/MM/YYYY"
                              required={true}
                              data-placement={"bottom"}
                              minDate={
                                this.state.from_date &&
                                this.state.from_date != "0000/00/00"
                                  ? moment(this.state.from_date)
                                  : moment()
                              }
                              popperPlacement="bottom-end"
                              name={"to_date"}
                              onChange={e =>
                                handleChangeSelectDatepicker(this, e, "to_date")
                              }
                              selected={
                                this.state.to_date &&
                                this.state.to_date != "0000/00/00"
                                  ? moment(this.state.to_date)
                                  : null
                              }
                              className="csForm_control text-center bl_bor"
                              placeholderText="DD/MM/YYYY"
                              disabled={this.state.is_disabled}
                              onChangeRaw={handleDateChangeRaw}
                            />
                          </span>
                        </div>
                      </div>
                    {applyButtonShow? <div className="col-lg-2 "><button className="btn-1 w-100" disabled={this.state.loading} onClick={()=>this.filterListing()}>Apply</button></div>:<React.Fragment/>}
                    </div>
                  </div>
                ) : (
                  <React.Fragment />
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className=""></div>
        </div>

        <div className="row">
          <div className="col-lg-12 F-Payroll_tBL">
            <div className="listing_table PL_site th_txt_center__ odd_even_tBL odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
             {/*  <ReactTable
                data={PricingTable}
                columns={columns}
                PaginationComponent={Pagination}
                noDataText="No Record Found"
                // onPageSizeChange={this.onPageSizeChange}
                minRows={2}
                previousText={
                  <span className="icon icon-arrow-left privious"></span>
                }
                nextText={<span className="icon icon-arrow-right next"></span>}
                showPagination={false}
                className="-striped -highlight"
                noDataText="No duplicate applicant found"
                //TbodyComponent={CustomTbodyComponent}
              /> */}
              <ReactTable
                //PaginationComponent={Pagination}
                //showPagination={this.state.payrollExemptionList.length > PAGINATION_SHOW ? true : false}
                showPagination={false}
                ref={this.reactTable}
                columns={columns}
                manual
                data={this.state.payrollList}
                filtered={this.state.filtered}
                defaultFiltered={this.defaultFilteredData()}
                //pages={this.state.pages}
                //previousText={<span className="icon icon-arrow-left privious" ></span>}
                //nextText={<span className="icon icon-arrow-right next"></span>}
                loading={this.state.loading}
                onFetchData={this.fetchData}
                noDataText="No payroll found"
                defaultPageSize={10}
                className="-striped -highlight"
                minRows={2}
                />
            </div>
          </div>
          <div className="col-lg-12">
          {this.state.loadingGraph? <NottachmentAvailable msg="Graph Loading..." extraClass="mt-5"/> :<MonthChart chartData={this.state.chartData} setOption={setChartOptions} setLegendToggle={true} height={500} />}
          </div>
        </div>

        <div className="row mt-5"></div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  showPageTitle: state.FinanceReducer.activePage.pageTitle,
  showTypePage: state.FinanceReducer.activePage.pageType
});
const mapDispatchtoProps = dispach => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchtoProps
)(Payroll);
