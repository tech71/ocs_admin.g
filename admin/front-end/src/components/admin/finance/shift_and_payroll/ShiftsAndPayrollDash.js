import React, { Component } from 'react';
import ReactTable from 'react-table';
import Chart from "react-google-charts";
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { graphViewType } from 'service/custom_value_data.js';
import _ from 'lodash';
import CustomChartBar from './../common/CommonBarChart';
import MonthChart from './../common/MonthChart';
import ProfilePage from './../common/ProfilePage';
import { connect } from 'react-redux'
import { toastMessageShow, postData } from 'service/common';

import { groupChating } from 'service/CustomContentLoader.js';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { CURRENCY_SYMBOL } from 'config.js'




const graphModeTypeData = ['shift_by_work_area','shift_queries_amount','payroll_tax_ydt','payroll_exemption_expire'];

class ShiftsAndPayrollDash extends React.Component {
    constructor(props) {
        super(props);
        let intData={};
        _.forEach(graphModeTypeData,function(el,index){
            intData[el+'_loading']=false;
            intData[el+'_data']=[];
            intData[el+'_type']='week';
        });
        this.state = {
            activeCol: '',
            value: null,
            graphType: graphViewType,
            ...intData

        }
    }

    getGraphResult = (graphModeType, graphType) => {
        this.setState({ [graphModeType + '_type']: graphType }, () => {
            this.fetchGraphData(graphModeType, graphType);
        })
    }

    fetchGraphData = (graphModeType,graphType) => {
        let modeType = _.includes(graphModeTypeData,graphModeType)? graphModeType:'';
        let viewType = _.includes(_.map(this.state.graphType,'name'),graphType)? graphType:'week';
        if(modeType==''){
            toastMessageShow('Invalid Request.','e');
        }else{
            this.setState({[modeType+'_loading']:true},()=>{
                let requestData ={
                    mode:modeType,
                    type:viewType
                };
                postData('finance/FinanceShiftAndPayRoll/get_payroll_graph_dashboard_fetch',requestData).then((result) => {
                    if (result.status) {
                     this.setState({[modeType+'_data']:result.data});
                    }else{
                    //this.setState({chartData:dummyData});
                    }
                    this.setState({[modeType+'_loading']:false});
                });
            });
        }
      }

      componentWillMount(){
        this.fetchGraphData(graphModeTypeData[3],this.state[graphModeTypeData[3]+'_type']);
        this.fetchGraphData(graphModeTypeData[2],this.state[graphModeTypeData[2]+'_type']);
        this.fetchGraphData(graphModeTypeData[1],this.state[graphModeTypeData[1]+'_type']);
        this.fetchGraphData(graphModeTypeData[0],this.state[graphModeTypeData[0]+'_type']);
      }

   

    render() {
        console.log('this.state',this.state,this.state[graphModeTypeData[3]+'_type']);

        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: this.state[graphModeTypeData[0]+'_data'],
                backgroundColor: ['#464765 ', '#707188', '#c1c1c9'],

            }],
        };

        const Queries = {
            labels: ['', '', ''],
            datasets: [{
                data: this.state[graphModeTypeData[1]+'_data'],
                backgroundColor: ['#f01', '#00be44', '#474866'],

            }],
        };
        const Querieslablechart = [
            {label:'Pay Out',extarClass:'drk-color4', extraStyle:{color:'#f01'}},
            {label:'Pay In',extarClass:'drk-color2', extraStyle:{color:'#00be44'}},
            {label:'Total',extarClass:'drk-color3', extraStyle:{color:'#474866'}},
        ];

        const lablechart = [
            {label:'NDIS',extarClass:'drk-color4', extraStyle:{color:'#464765'}},
            {label:'Welfare',extarClass:'drk-color2', extraStyle:{color:'#707188'}},
            {label:'Private',extarClass:'drk-color3', extraStyle:{color:'#c1c1c9'}},
        ];

        /* const NavLinkchart = [
            {label:'Week', clickEvent:()=>{}},
            {label:'Month', },
            {label:'Year', },
        ]; */

        const NavLinkchartQueries= graphViewType.length>0? graphViewType.map((row,index)=>{
            return {label:_.capitalize(row.name),clickEvent:()=>this.getGraphResult(graphModeTypeData[1],_.lowerCase(row.name)),class:this.state[graphModeTypeData[1]+'_type'] == _.lowerCase(row.name) ? 'active' : ''};
        }):[];

        const NavLinkchartWorkArea= graphViewType.length>0? graphViewType.map((row,index)=>{
            return {label:_.capitalize(row.name),clickEvent:()=>this.getGraphResult(graphModeTypeData[0],_.lowerCase(row.name)),class:this.state[graphModeTypeData[0]+'_type'] == _.lowerCase(row.name) ? 'active' : ''};
        }):[];

        


 
    


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>
                        <div className="d-flex by-1 py-4">
                            <div className="h-h1 color">{this.props.showPageTitle}</div>
                            <i className="icon icon-"></i>
                        </div>
                    </div>
                </div>

                <div className="row d-flex justify-content-center " style={{marginTop:"15px"}}>
                    <div className="col-lg-8 d-none">
                        <div className="FD_ul_" style={{margin:"0px 0px 15px"}}>
                            <div className="FD_li_">
                                <i className="icon ie-usd"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-dollarl-cancel"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-ie-profit"></i>
                                <span>There is a $XXXXXXXXX profit this month</span>
                            </div>
                        </div>
                    </div>
                </div>

                
                

                <div className="row d-flex justify-content-center">
                    
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            {/* <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ groupChating()} ready={this.state[graphModeTypeData[0]+'_loading']}> */}
                                 <CustomChartBar barTitle={'Amount Spent on shifts by Work Area'} barData={Recruitmentdata} labelShowData={lablechart} 
                                 navbarShowData={NavLinkchartWorkArea} selectedActiveNavBar={this.state[graphModeTypeData[0]+'_type']}
                                 placeHoldeShow={this.state[graphModeTypeData[0]+'_loading']}
                                />
                            {/* </ReactPlaceholder> */}
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <CustomChartBar barTitle={'Shift Queries Amounts'} barData={Queries} labelShowData={Querieslablechart} navbarShowData={NavLinkchartQueries} selectedActiveNavBar={this.state[graphModeTypeData[1]+'_type']} placeHoldeShow={this.state[graphModeTypeData[1]+'_loading']}/>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row d-flex  justify-content-center ">
                    <div className="col-lg-8">


                        <div className="row">

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Total Payroll YDT</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state[graphModeTypeData[2]+'_data']} classNameAdd="" mode="other" 
                                            currencyFormat={true} currencySymbol={CURRENCY_SYMBOL} 
                                            placeHoldeShow={this.state[graphModeTypeData[2]+'_loading']}
                                            />
                                        </div>
                                        {/* <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={this.state[graphModeTypeData[2]+'_type']==val.name ? 'active':''} onClick={() => this.getGraphResult(graphModeTypeData[3], val.name)}>{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> */}

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Orgs With Payroll Exempt  Tax Expiring within a Month</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state[graphModeTypeData[3]+'_data']} classNameAdd="" mode="other"  placeHoldeShow={this.state[graphModeTypeData[3]+'_loading']}/>
                                        </div>
                                        {/* <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={this.state[graphModeTypeData[3]+'_type']==val.name ? 'active':''} onClick={() => this.getGraphResult(graphModeTypeData[3], val.name)}>{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div> */}

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(ShiftsAndPayrollDash);