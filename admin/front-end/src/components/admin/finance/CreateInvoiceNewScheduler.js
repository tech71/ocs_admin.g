import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'






class CreateInvoiceNewScheduler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage />
              


                <div className="Finance__panel_1">
                    <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F" >
                        {/* <PanelGroup accordion id="accordion-example"> */}
                        <Panel eventKey="1">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Schedule </p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>
                                <React.Fragment>
                                    <div className="row d-flex justify-content-center">
                                        <div className="col-lg-8 col-sm-12">

                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-9">
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Start Date</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">End Date</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Frequency</label>
                                                            <div className="sLT_gray left left-aRRow">
                                                                <Select
                                                                    name="form-field-name"
                                                                    value="one"
                                                                    options={selectOpt}
                                                                    onChange={logChange}
                                                                    clearable={false}
                                                                    searchable={false}
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Create Invoice - This Many Days Before Due</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </React.Fragment>
                            </Panel.Body>
                        </Panel>
                        <Panel eventKey="1">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Payment </p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>

                                <React.Fragment>
                                    <div className="row d-flex justify-content-center">
                                        <div className="col-lg-8 col-sm-12">
                                            <div className="row mt-5  d-flex">
                                                <div className="col-lg-12 col-sm-12">
                                                    <div className="pAY_heading_01 by-1">
                                                        <div className="tXT_01">Breakdown</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-9">
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Rent</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Activities</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Food</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Residential Fee</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Pension</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">% of Pension</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div className="row mt-5  d-flex">
                                                <div className="col-lg-12 col-sm-12">
                                                    <div className="pAY_heading_01 by-1">
                                                        <div className="tXT_01">Payment Method</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-9">
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Funds Paid By:</label>
                                                            <div className="sLT_gray left left-aRRow">
                                                                <Select
                                                                    name="form-field-name"
                                                                    value="one"
                                                                    options={selectOpt}
                                                                    onChange={logChange}
                                                                    clearable={false}
                                                                    searchable={false}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-9">
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-12">
                                                            <div className="Time_line_lables">

                                                                <label className="label_2_1_2 mr-5">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="radio" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span>SAMs Not Entered</span>
                                                                </label>
                                                                <label className="label_2_1_3 mr-5">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="radio" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span>Change to SAMs Agreement</span>
                                                                </label>
                                                                <label className="label_2_1_3">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="radio" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span>Not Applicable to SAMs Agreement</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Fee (Per Frequency)</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6 align-self-end">
                                                            <div className="Time_line_lables">
                                                                <label className="label_2_1_2 mr-5">
                                                                    <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span>Apply GST</span>
                                                                </label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div className="row mt-5  d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">GST Amount</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                </React.Fragment>

                            </Panel.Body>
                        </Panel>
                        {/* </PanelGroup> */}
                    </div>
                </div>

            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateInvoiceNewScheduler);