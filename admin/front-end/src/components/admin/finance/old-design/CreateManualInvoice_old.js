import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'





class CreateManualInvoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle} Lorna Marsh</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage />


                <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8 col-sm-12">

                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-12 col-sm-12">
                                            <label className="label_2_1_1 color"><strong>Quote Number: </strong>5465645</label>
                                        </div>
                                    </div>

                                    <div className="row mt-5">
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Quote Date</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Valid Until</label>
                                            <input type="text"/>
                                        </div>
                                    </div>

                                    {/* looping start  */}
                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-3 col-sm-3">
                                            <label className="label_2_1_1">Funding Type</label>
                                            <div className="sLT_gray left left-aRRow">
                                                <Select
                                                    name="form-field-name"
                                                    value="one"
                                                    options={selectOpt}
                                                    onChange={logChange}
                                                    clearable={false}
                                                    searchable={false}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-lg-5 col-sm-5 align-self-end">
                                        <label className="label_2_1_1">Line Item</label>
                                            <div className="modify_select">
                                                <Select
                                                    name="form-field-name"
                                                    value="one"
                                                    options={selectOpt}
                                                    onChange={logChange}
                                                    clearable={false}
                                                    searchable={true}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-sm-2">
                                            <label className="label_2_1_1">Quantity</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-2 col-sm-2 align-self-center">
                                            <div className="label_2_1_1">&nbsp;</div>
                                            <label className="label_2_1_1">Cost: $00.00</label>
                                        </div>
                                        <div className="col-lg-1 col-sm-1 align-self-center">
                                            <label className="label_2_1_1">&nbsp;</label>
                                            <div>
                                                <button className="icon icon-add2-ie aDDitional_bTN_F0"></button>
                                                <button className="icon icon-remove2-ie aDDitional_bTN_F1" disabled></button>
                                            </div>
                                        </div>
                                    </div>
                                    {/* looping edn  */}

                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-4">
                                            <a className="C_NeW_BtN w-100"><span>Add Manual Quote Item</span><i className="icon icon icon-add-icons"></i></a>
                                        </div>
                                    </div>
                                    <div className="row mt-5  d-flex">
                                        <div className="col-lg-12 col-sm-12">
                                            <div className="pAY_heading_01 by-1">
                                                <div className="tXT_01">Manual Quote Item</div>
                                                <i className="icon icon-close2-ie"></i>
                                            </div>
                                        </div>
                                    </div>

                                    {/* looping start  */}
                                    <React.Fragment>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Name</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Item Description</label>
                                                <input type="text"/>
                                            </div>
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Cost</label>
                                                <input type="text"/>
                                            </div>
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1"></label>
                                                <div className="sLT_gray left left-aRRow">
                                                    <Select
                                                        name="form-field-name"
                                                        value="one"
                                                        options={selectOpt}
                                                        onChange={logChange}
                                                        clearable={false}
                                                        searchable={false}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-2 col-sm-2 align-self-center">
                                                <label className="label_2_1_1">&nbsp;</label>
                                                <div>
                                                    <button className="icon icon-add2-ie aDDitional_bTN_F0"></button>
                                                    <button className="icon icon-remove2-ie aDDitional_bTN_F1" disabled></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-8 col-sm-12">
                                                <label className="label_2_1_1">Add Notes To Quote</label>
                                                <textarea className="w-100"></textarea>
                                            </div>
                                        </div>

                                        <div className="row mt-5 d-flex justify-content-end">
                                            <div className="col-lg-4"><a className="btn-1 out_line" href="/admin/finance/CreateManualInvoice">Save and Draft</a></div>
                                            <div className="col-lg-4"><a className="btn-1" href="/admin/finance/CreateManualInvoice">Create & View Quote</a>
                                            </div>
                                    </div>
                                    </React.Fragment>
                                    {/* looping End  */}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateManualInvoice);