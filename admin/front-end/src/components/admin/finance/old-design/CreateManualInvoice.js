import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import CustomChartBar from './common/CommonBarChart';
import { Link } from 'react-router-dom';
import { postData, toastMessageShow, handleChangeSelectDatepicker, handleRemoveShareholder, handleAddShareholder, handleChangeChkboxInput, handleShareholderNameChange, onlyNumberAllow } from 'service/common.js';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux';

class CreateManualInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            payPoints: [{ 'fundingType': '', 'lineItem': '', 'quantity': '' }]
        }
    }
    render() {
        const invoicescheduler = [
            { invshift: 'Org 1', invoiceschedule: 'Weekly', cost: '$500.00', outstanding: 'outstanding' },
            { invshift: 'Org 2', invoiceschedule: 'Dortnightly', cost: '$500.00', outstanding: 'outstanding' },
            { invshift: 'Org 3', invoiceschedule: 'Monthly', cost: '$500.00', outstanding: 'outstanding' },
        ]

        const columns = [
            {
                id: "invshift",
                accessor: "invshift",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },

            {
                id: "invoiceschedule",
                accessor: "invoiceschedule",
                headerClassName: 'Th_class_d1 header_cnter_tabl',
                className: 'Tb_class_d1 Tb_class_d2 ',
                Cell: props => <span>
                    {/* <i className="icon icon-userm1-ie"></i>
                    <i className="icon icon-userf1-ie"></i> */}
                    <div>{props.value}</div>
                </span>

            },
            {
                id: "cost",
                accessor: "cost",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "outstanding",
                accessor: "outstanding",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                Cell: props => <span className="short_buttons_01 btn_color_archive">{props.value}</span>
            },
        ]



        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/finance/InvoicesDashboard"><span className="icon icon-back1-ie"></span></a>
                           </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle} For Lorna Marsh</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage />



                <div className="row d-flex justify-content-center mt-5">
                    <div className="col-lg-12 ">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8">
                                    <p className="color m-0"><strong>Invoice Number: HC_00009</strong></p>
                                    <div className="row mt-5 d-flex">
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Quote Date</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">Pay By</label>
                                            <input type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="Finance__panel_1 mt-5">
                            <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F" >
                                <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <div>
                                                <p>Invoice Shifts </p>
                                                <span className="icon icon-arrow-right"></span>
                                                <span className="icon icon-arrow-down"></span>
                                            </div>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <React.Fragment>
                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-8 col-sm-12">
                                                    <div className="row mt-5  d-flex flex-wrap">
                                                        <div className="col-lg-4 col-sm-4">
                                                            <div className="">
                                                                <label className="label_2_1_1">Invoice Date</label>
                                                                <span>
                                                                    <DatePicker
                                                                        selected={this.state.startDate}
                                                                        onChange={this.handleChange}
                                                                        autoComplete={'off'}
                                                                    />
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 col-sm-4">
                                                            <div className="">
                                                                <label className="label_2_1_1">Pay By</label>
                                                                <span>
                                                                    <DatePicker
                                                                        selected={this.state.startDate}
                                                                        onChange={this.handleChange}
                                                                        autoComplete={'off'}
                                                                    />
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="manual_invoice_tbl col-lg-12 col-sm-12">
                                                            <div className="listing_table PL_site odd_even_tBL  odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                                                                <ReactTable
                                                                    TheadComponent={_ => null}
                                                                    data={invoicescheduler}
                                                                    columns={columns}
                                                                    PaginationComponent={Pagination}
                                                                    noDataText="No Record Found"
                                                                    minRows={2}
                                                                    showPagination={false}
                                                                    className="-striped -highlight"
                                                                    noDataText="No duplicate applicant found"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-12 mt-5">
                                                            <label className="label_2_1_1">Add Notes To Invoice</label>
                                                            <textarea className="w-100" name="comment" ></textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    </Panel.Body>
                                </Panel>

                                <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <div>
                                                <p>Line Item Invoice</p>
                                                <span className="icon icon-arrow-right"></span>
                                                <span className="icon icon-arrow-down"></span>
                                            </div>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <React.Fragment>
                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-8 col-sm-12">
                                                    {this.state.payPoints.map((value, idx) => (
                                                        <div className="row mt-5  d-flex flex-wrap" key={idx}>
                                                            <div className="col-lg-3 col-sm-3 align-self-end">
                                                                <div className="sLT_gray left left-aRRow">
                                                                    <label className="label_2_1_1">Funding Type</label>
                                                                    <span>
                                                                        <Select
                                                                            required={true} simpleValue={true}
                                                                            className="custom_select default_validation"
                                                                            options={this.state.payrateCategory}
                                                                            onChange={(e) => this.handleChangeCategory(e, 'category')}
                                                                            value={this.state.category}
                                                                            clearable={false}
                                                                            searchable={false}
                                                                            placeholder={'Select'}
                                                                            inputRenderer={(props) => <input type="text" {...props} readOnly name={'fundingType'} />}
                                                                        />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-4 col-sm-4 align-self-end">
                                                                <div className="sLT_gray left left-aRRow">
                                                                    <label className="label_2_1_1">Line Item</label>
                                                                    <span className="modify_select">
                                                                        <Select
                                                                            required={true} simpleValue={true}
                                                                            className="custom_select default_validation"
                                                                            // options={this.state.payrateCategory}
                                                                            onChange={(e) => this.handleChangeCategory(e, 'category')}
                                                                            // value={this.state.category}
                                                                            clearable={false}
                                                                            searchable={true}
                                                                            placeholder={' '}
                                                                            inputRenderer={(props) => <input type="text" {...props} name={'lineItem'} />}
                                                                        />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-2 col-sm-2">
                                                                <label className="label_2_1_1">Quantity</label>
                                                                <input type="text" name={'quantity'} />
                                                            </div>
                                                            <div class="col-lg-2 col-sm-2 align-self-center">
                                                                <div class="label_2_1_1">&nbsp;</div>
                                                                <label class="label_2_1_1">Cost: $0</label>
                                                            </div>
                                                            <div className="col-lg-1 col-sm-1 align-self-center">
                                                                <label className="label_2_1_1">&nbsp;</label>
                                                                <div>
                                                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'payPoints')} className="icon icon-remove2-ie aDDitional_bTN_F1">
                                                                    </button> : (this.state.payPoints.length == 3) ? '' : <button className="icon icon-add2-ie aDDitional_bTN_F0" onClick={(e) => handleAddShareholder(this, e, 'payPoints', value)}>
                                                                    </button>}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ))}
                                                    <div className="row d-flex justify-content-center">
                                                        <div className="col-lg-12 mt-5">
                                                            <label className="label_2_1_1">Add Notes To Invoice</label>
                                                            <textarea className="w-100" name="comment" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    </Panel.Body>
                                </Panel>

                                <Panel eventKey="1">
                                    <Panel.Heading>
                                        <Panel.Title toggle>
                                            <div>
                                                <p>Manual Invoice</p>
                                                <span className="icon icon-arrow-right"></span>
                                                <span className="icon icon-arrow-down"></span>
                                            </div>
                                        </Panel.Title>
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <React.Fragment>
                                            <div className="row d-flex justify-content-center">
                                                <div className="col-lg-8 col-sm-12">
                                                    <div className="row mt-5 d-flex">
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Item Name</label>
                                                            <input type="text"/>
                                                        </div>
                                                        <div className="col-lg-6 col-sm-6">
                                                            <label className="label_2_1_1">Item Description</label>
                                                            <input type="text"/>
                                                        </div>
                                                    </div>

                                                    {this.state.payPoints.map((value, idx) => (
                                                        <div className="row mt-5  d-flex flex-wrap" key={idx}>
                                                            <div className="col-lg-3 col-sm-2">
                                                                <label className="label_2_1_1">Cost</label>
                                                                <input type="text"/>
                                                            </div>
                                                            <div className="col-lg-3 col-sm-3 align-self-end">
                                                                <div className="sLT_gray left left-aRRow">
                                                                    <label className="label_2_1_1">Funding Type</label>
                                                                    <span>
                                                                        <Select
                                                                            required={true} simpleValue={true}
                                                                            className="custom_select default_validation"
                                                                            options={this.state.payrateCategory}
                                                                            onChange={(e) => this.handleChangeCategory(e, 'category')}
                                                                            value={this.state.category}
                                                                            clearable={false}
                                                                            searchable={false}
                                                                            placeholder={'Charge By: Fixed Price'}
                                                                            inputRenderer={(props) => <input type="text" {...props} readOnly />}
                                                                        />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="col-lg-1 col-sm-1 align-self-center">
                                                                <label className="label_2_1_1">&nbsp;</label>
                                                                <div>
                                                                    {idx > 0 ? <button onClick={(e) => handleRemoveShareholder(this, e, idx, 'payPoints')} className="icon icon-remove2-ie aDDitional_bTN_F1">
                                                                    </button> : (this.state.payPoints.length == 3) ? '' : <button className="icon icon-add2-ie aDDitional_bTN_F0" onClick={(e) => handleAddShareholder(this, e, 'payPoints', value)}>
                                                                    </button>}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    ))}
                                                    <div className="row d-flex justify-content-center">
                                                        <div className="col-lg-12 mt-5">
                                                            <label className="label_2_1_1">Add Notes To Invoice</label>
                                                            <textarea className="w-100" name="comment" ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    </Panel.Body>
                                </Panel>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-3 pull-right">
                                <button className="btn-1 w-100">Generate Invoice</button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CreateManualInvoice);