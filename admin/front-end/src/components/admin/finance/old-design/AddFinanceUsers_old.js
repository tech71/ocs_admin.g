import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ProfilePage from './common/ProfilePage';
import ScrollArea from 'react-scrollbar';
import { connect } from 'react-redux'




class AddFinanceUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-6">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                            </div>
                        </div>

                        {/* <div className="bb-1 mb-4">
                            <div className="row sort_row1--">
                                <div className="col-lg-6 col-md-8 col-sm-8 ">
                                    <form method="post">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                    <div className="filter_flx">
                                        <div className="filter_fields__ cmn_select_dv gr_slctB sl_center" style={{ padding: 0 }}>
                                            <Select
                                                name="form-field-name"
                                                value="one"
                                                options={selectOpt}
                                                onChange={logChange}
                                                clearable={false}
                                                searchable={false}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>From</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>To</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"

                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}


                    </div>
                </div>

                <ProfilePage />


                <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8">

                                    <div className="row">
                                        <div className="col-md-12 mb-5">
                                            <label className="label_2_1_1 color"><strong>Finance User ID: </strong>5465645</label>
                                        </div>

                                        <div className="col-md-12">
                                            <label className="label_2_1_1">Access Permissions</label>
                                            <div className="Parent-List_2_ul">
                                                <div className="cstmSCroll1 FScroll">
                                                    <ScrollArea
                                                        speed={0.8}
                                                        contentClassName="content"
                                                        horizontal={false}

                                                        style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                    >
                                                        <ul className="List_2_ul">
                                                            <li className="w-60">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-40">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-60">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-40">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-60">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-40">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-60">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                            <li className="w-40">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Lorem Ipsum is simply dummy text</span>
                                                            </li>
                                                        </ul>
                                                    </ScrollArea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    
                                    <div className="row mt-5 d-flex justify-content-end">
                                        <div className="col-lg-4">
                                            <a className="btn-1">
                                                Add To Finance Module
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(AddFinanceUser);