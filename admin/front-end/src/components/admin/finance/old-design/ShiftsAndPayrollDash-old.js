import React, { Component } from 'react';
import ReactTable from 'react-table';
import Chart from "react-google-charts";
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import CustomChartBar from './common/CommonBarChart';
import MonthChart from './common/MonthChart';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { graphViewType } from 'service/custom_value_data.js';
import _ from 'lodash';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'





class ShiftsAndPayrollDash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            value: null,
            graphType: graphViewType,

        }
    }

   

    render() {

        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#464765 ', '#707188', '#c1c1c9'],

            }],
        };

        const Queries = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#f01 ', '#00be44', '#474866'],

            }],
        };
        const Querieslablechart = [
            {label:'Pay Out',extarClass:'drk-color4'},
            {label:'Pay In',extarClass:'drk-color2'},
            {label:'Total',extarClass:'drk-color3'},
        ];

        const lablechart = [
            {label:'NDIS',extarClass:'drk-color4'},
            {label:'Welfare',extarClass:'drk-color2'},
            {label:'Private',extarClass:'drk-color3'},
        ];

        const NavLinkchart = [
            {label:'Week', },
            {label:'Month', },
            {label:'Year', },
        ];

        


 
    


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>
                        <div className="d-flex by-1 py-4">
                            <div className="h-h1 color">{this.props.showPageTitle}</div>
                            <i className="icon icon-"></i>
                        </div>
                    </div>
                </div>

                <div className="row d-flex justify-content-center">
                    <div className="col-lg-8">
                        <div className="FD_ul_" style={{marginBottom:"15px"}}>
                            <div className="FD_li_">
                                <i className="icon ie-usd"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-dollarl-cancel"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-ie-profit"></i>
                                <span>There is a $XXXXXXXXX profit this month</span>
                            </div>
                        </div>
                    </div>
                </div>

                
                

                <div className="row d-flex justify-content-center">
                    
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <CustomChartBar barTitle={'Amount Spend on shift by Work Area'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart}/>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <CustomChartBar barTitle={'Shift Queries Amounts'} barData={Queries} labelShowData={Querieslablechart} navbarShowData={NavLinkchart}/>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row d-flex  justify-content-center ">
                    <div className="col-lg-8">


                        <div className="row">

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Total Payroll Tax YDT</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Orgs With Payroll Exempt  Tax Expiring With in Month</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(ShiftsAndPayrollDash);