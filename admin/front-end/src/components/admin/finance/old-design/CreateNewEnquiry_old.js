import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'
import { postData } from 'service/common.js';




class CreateNewEnquiry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }
    componentDidMount() {
        this.getStateList();
        this.getUserType();
    }

    getStateList = ()=> {
        this.setState({ loading: true });
            postData('finance/FinanceQuoteManagement/get_state').then((result) => {           
            if (result.status) {
               this.setState({ enquiryState: result.data });
            } else {                
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    getUserType=()=>{
        
        this.setState({ loading: true });
            postData('finance/FinanceQuoteManagement/get_finance_user_type_category').then((result) => {           
            if (result.status) {
               this.setState({ userType: result.data });
            } else {                
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }

    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {/* <ProfilePage /> */}

                <div className="row d-flex justify-content-center mt-5">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8 col-sm-12">

                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Service for</label>
                                            <input  type="text"/>
                                        </div>
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Category</label>
                                            <div className="sLT_gray left left-aRRow">
                                                <Select
                                                    name="form-field-name"
                                                    value="one"
                                                    options={this.state.userType}
                                                    onChange={logChange}
                                                    clearable={false}
                                                    searchable={false}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Contact Name</label>
                                            <input  type="text"/>
                                        </div>
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Company Name</label>
                                            <input  type="text"/>
                                        </div>
                                    </div>


                                    <div className="row mt-5">
                                        <div className="col-lg-8 col-sm-8">
                                            <label className="label_2_1_1">Email Address</label>
                                            <input  type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Primary Contact Number</label>
                                            <input  type="text"/>
                                        </div>
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Additional Contact Number</label>
                                            <input  type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-8 col-sm-8">
                                            <label className="label_2_1_1">Address</label>
                                            <input  type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Suburb</label>
                                            <input  type="text"/>
                                        </div>
                                        <div className="col-lg-2 col-sm-2">
                                            <label className="label_2_1_1">Postcode</label>
                                            <input  type="text"/>
                                        </div>
                                        <div className="col-lg-4 col-sm-4">
                                            <label className="label_2_1_1">State</label>
                                            <div className="sLT_gray left left-aRRow">
                                                <Select
                                                    name="form-field-name"
                                                    value="one"
                                                    options={this.state.enquiryState}
                                                    onChange={logChange}
                                                    clearable={false}
                                                    searchable={false}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row mt-5 d-flex justify-content-end">
                                        <div className="col-lg-4">
                                            <Link className="btn-1" to='./CreateManualInvoice'>Next</Link>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateNewEnquiry);