import React, { Component } from 'react';
import ReactTable from 'react-table';
import Chart from "react-google-charts";
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import CustomChartBar from './common/CommonBarChart';
import MonthChart from './common/MonthChart';
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { connect } from 'react-redux'
import { graphViewType } from 'service/custom_value_data.js';
import _ from 'lodash';





class NdisInvoices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            value: null,
            graphType: graphViewType,
        }
    }

    renderSquare(i) {
        return <Square />;
    }


    render() {

        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#464765 ', '#707188', '#c1c1c9'],

            }],
        };

        const lablechart = [
            { label: 'Total', extarClass: 'drk-color4' },
            { label: 'Participant', extarClass: 'drk-color2' },
            { label: 'Org', extarClass: 'drk-color3' },
        ];

        const NavLinkchart = [
            { label: 'Week', },
            { label: 'Month', },
            { label: 'Year', },
        ];


        const chartData = [
            [
                "Qty.",
                "Quotes Created",
                "Converted into Invoice",
            ],
            ["Jan", 2500, 5000],
            ["Feb", 1500, 2500],
            ["Mar", 1300, 1500],
            ["Apr", 5000, 1200],
            ["May", 1526, 1517],
            ["Jun", 5000, 4445],
            ["Jul", 3500, 4500],
            ["Aug", 1526, 1517],
            ["Sep", 1526, 1517],
            ["Oct", 1526, 1517],
            ["Nov", 1526, 1517],
            ["Dec", 1526, 1517]
        ];

        const setChartOptions = {
            chartArea: { width: "80%" },
            isStacked: true,
            legend: { position: "bottom", alignment: "center" },
            backgroundColor: '#ececec',
            colors: ['#464765', '#6f7087'],
            vAxis: {
                gridlines: {
                    color: 'transparent'
                }
            }
        }




        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>
                        <div className="d-flex by-1 py-4">
                            <div className="h-h1 color">{this.props.showPageTitle}</div>
                            <i className="icon icon-"></i>
                        </div>
                    </div>
                </div>

                {/* <div className="row">
                    <div className="col-md-12">
                        <div className="FD_ul_">
                            <div className="FD_li_">
                                <i className="icon ie-usd"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-dollarl-cancel"></i>
                                <span>XXXXXXXXXXXX has come in this month</span>
                            </div>
                            <div className="FD_li_">
                                <i className="icon ie-ie-profit"></i>
                                <span>There is a $XXXXXXXXX profit this month</span>
                            </div>
                        </div>
                    </div>
                </div> */}

                <div className="row d-flex  justify-content-center mt-5">
                    <div className="col-lg-8">


                        <div className="row">

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Money In:</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">NDIS Invoice Errors(Total):</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Outstanding Errors:</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Last NDIS Billing:</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.prospective_app_count_data} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                        {this.state.graphType.length > 0 ? this.state.graphType.map((val, index) => {
                                                            return (<li key={index} className={''} >{_.capitalize(val.name)}</li>);
                                                        }) : <React.Fragment />}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <div className="row">
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <CustomChartBar barTitle={'Amount of Quotes Generated'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart} />
                            </div>
                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <CustomChartBar barTitle={'Income From Invoices'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart} />
                            </div>
                        </div> */}


                    </div>
                </div>




                <div className="row">
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="Out_title by-1">Quote To Invoice Conversion</div>
                            </div>
                            <div className="col-md-12">
                                <MonthChart chartData={chartData} setOption={setChartOptions} height={'450px'} />
                            </div>
                        </div>
                    </div>

                </div>

                {/* <div onClick={function(){alert('hi atif')}}>fdsfasd</div>
                <div onClick={()=>alert('hi atif')}>testing</div>

                <button className="square" onClick={() => this.setState({value: 'Hi Atif'})}>Button</button>
                {this.state.value} */}


                {/* <Square /> */}
                <div>

                    <div className="board-row">
                        {this.renderSquare(0)}
                        {this.renderSquare(1)}
                        {this.renderSquare(2)}
                    </div>
                    <div className="board-row">
                        {this.renderSquare(3)}
                        {this.renderSquare(4)}
                        {this.renderSquare(5)}
                    </div>
                    <div className="board-row">
                        {this.renderSquare(6)}
                        {this.renderSquare(7)}
                        {this.renderSquare(8)}
                    </div>
                </div>

            </React.Fragment >
        );
    }

}


class Square extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    render() {
        return (
            <button
                className="square"
                onClick={() => this.setState({ value: 'X' })}
            >
                {this.state.value}
            </button>
        );
    }
}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(NdisInvoices);