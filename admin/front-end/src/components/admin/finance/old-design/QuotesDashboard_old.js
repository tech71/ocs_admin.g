import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { Doughnut, Bar, Line } from 'react-chartjs-2';
import CustomChartBar from './common/CommonBarChart';
import createClass from 'create-react-class';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'




const GravatarOption = createClass({
    // propTypes: {
    //     children: PropTypes.node,
    //     className: PropTypes.string,
    //     isDisabled: PropTypes.bool,
    //     isFocused: PropTypes.bool,
    //     isSelected: PropTypes.bool,
    //     onFocus: PropTypes.func,
    //     onSelect: PropTypes.func,
    //     option: PropTypes.object.isRequired,
    // },
    // handleMouseDown(event) {
    //     event.preventDefault();
    //     event.stopPropagation();
    //     this.props.onSelect(this.props.option, event);
    // },
    // handleMouseEnter(event) {
    //     this.props.onFocus(this.props.option, event);
    // },
    // handleMouseMove(event) {
    //     if (this.props.isFocused) return;
    //     this.props.onFocus(this.props.option, event);
    // },
    render() {
        return (
            <div className={this.props.className}
                onMouseDown={this.handleMouseDown}
                onMouseEnter={this.handleMouseEnter}
                onMouseMove={this.handleMouseMove}
                title={this.props.option.title}>

                {/* <div className="h_ser_div"><a><span class={"add_access " + this.props.option.type + "-colr"}>{this.props.option.type}</span></a></div>
                <div className="h_ser_div_2">{this.props.option.value} - ID:<br />
                    {this.props.children}</div>
                <div className="h_ser_div_3"><span className={"icon icon-arrow-r " + this.props.option.type + '-arrow'}></span></div> */}
                <div className="Select_Search_Type_">
                    <div className="text_set">{this.props.children}</div>
                    <span className="Default_icon Participant_icon Site_icon Sub_org_icon Org_icon Member_icon House_icon"><i className="icon icon icon-userm1-ie"></i></span>
                </div>

                <div className="Select_Search_Type_ mt-5">
                    <div className="text_set">Create New Enquiry</div>
                    <span className="Default_icon"><i className="icon icon icon-add2-ie"></i></span>
                </div>
            </div>
        );
    }
});

class QuotesDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '', 
        }
    }


    render() {

        const quoteshome = [
            { quoteNum: '00001', quotefor: 'Particpant 1', addressto: 'Booker 1', amount: '$0,100', fundingtype: 'NDIS', quotedate: '01/02/2019', quotedby: 'Ben Fleming' },
            { quoteNum: '00002', quotefor: 'Org 1', addressto: 'Booker 1', amount: '$0,100', fundingtype: 'NDIS', quotedate: '01/02/2019', quotedby: 'Ben Fleming' },
            { quoteNum: '00002', quotefor: 'Org 2', addressto: 'Booker 1', amount: '$0,100', fundingtype: 'NDIS', quotedate: '01/02/2019', quotedby: 'Ben Fleming' },
        ]


        const columns = [
            {
                id: "quoteNum",
                accessor: "quoteNum",
                headerClassName: 'Th_class_d1 header_cnter_tabl checkbox_header',
                className:'Tb_class_d1 Tb_class_d2',
                Cell: (props) => {
                    return (
                        <span>
                            <label className="Cus_Check_1">
                                <input type="checkbox" /><div className="chk_Labs_1"></div>
                            </label>
                            <div>{props.value}</div>
                        </span>
                    );
                },
                Header: x => {
                    return (
                        <div className="Tb_class_d1 Tb_class_d2">
                            <span >
                                <label className="Cus_Check_1">
                                    <input type="checkbox" /><div className="chk_Labs_1"></div>
                                </label>
                                <div>Quote Number</div>
                            </span>
                        </div>
                    );

                },
                resizable: false,
                width: 230
            },
            {
                id: "quotefor",
                accessor: "quotefor",
                headerClassName: 'Th_class_d1 header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Quote For</div>
                    </div>
                ,
                className:'Tb_class_d1 Tb_class_d2 ',
                Cell: props => <span>
                    <i className="icon icon-userm1-ie"></i>
                    <i className="icon icon-userf1-ie"></i>
                    <div>{props.value}</div>
                </span>

            },
            {
                id: "addressto",
                accessor: "addressto",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Addressed To</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "amount",
                accessor: "amount",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Amount</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "fundingtype",
                accessor: "fundingtype",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Funding Type</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "quotedate",
                accessor: "quotedate",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Quote Date</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "quotedby",
                accessor: "quotedby",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Quoted By</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "status",
                // accessor: "status",
                headerClassName: '_align_c__ header_cnter_tabl',
                className:'_align_c__',
                width: 160,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Status</div>
                    </div>,
                Expander: (props) =>
                    <div className="expander_bind">

                        {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
                            <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                            <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                            <span className="short_buttons_01 btn_color_offline">Assigned</span>
                            <span className="short_buttons_01 btn_color_archive">Assigned</span>
                            <span className="short_buttons_01 btn_color_unavailable">Assigned</span> */}
                        <span className="short_buttons_01">Assigned</span>

                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }

            }

        ]

        
        const Recruitmentdata = {
            labels: ['', '', ''],
            datasets: [{
                data: ['714', '528', '465'],
                backgroundColor: ['#464765 ', '#707188', '#c1c1c9'],

            }],
        };

        const lablechart = [
            {label:'Total',extarClass:'drk-color4'},
            {label:'Participant',extarClass:'drk-color2'},
            {label:'Org',extarClass:'drk-color3'},
        ];

        const NavLinkchart = [
            {label:'Week', },
            {label:'Month', },
            {label:'Year', },
        ];

        var selectOpt = [
            { value: 'one', label: 'One' },
            // { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">

                                <div className="col-lg-6 align-self-center">
                                    <div className="h-h1 color ">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <Link to='./CreateNewEnquiry' className="C_NeW_BtN w-100"><span>Create New Quote</span><i className="icon icon icon-add-icons"></i></Link>
                                </div>
                                <div className="col-lg-3 modify_select align-self-center">
                                            <Select
                                                name="form-field-name"
                                                value="one"
                                                options={selectOpt}
                                                onChange={logChange}
                                                clearable={false}
                                                searchable={true}
                                                optionComponent={GravatarOption}
                                            />
                                        </div>

                            </div>
                        </div>
                    </div>
                </div>



                <div className="row" style={{ paddingTop: '15px' }}>
                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <CustomChartBar barTitle={'Amount of Quotes Generated'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart}/> 
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <CustomChartBar barTitle={'Quote Acceptance'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart}/> 
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <CustomChartBar barTitle={'Average Time to Accept Quote'} barData={Recruitmentdata} labelShowData={lablechart} navbarShowData={NavLinkchart}/> 
                    </div>
                </div>


                <div className="by-1 mb-4">
                    <div className="row sort_row1-- after_before_remove">
                        <div className="col-lg-6 col-md-8 col-sm-8 ">
                            <form method="post">
                                <div className="search_bar right srchInp_sm actionSrch_st">
                                    <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-4 ">
                        <div className="sLT_gray left left-aRRow">

                                    <Select
                                        name="form-field-name"
                                        value="one"
                                        options={selectOpt}
                                        onChange={logChange}
                                        clearable={false}
                                        searchable={false}
                                    />

                            </div>
                        </div>
                        <div className="col-lg-3 col-md-4 col-sm-4 ">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="Fil_ter_ToDo">
                                        <label>From</label>
                                        <span>
                                            <DatePicker
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                placeholderText="00/00/0000"
                                            />
                                        </span>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="Fil_ter_ToDo">
                                        <label>To</label>
                                        <span>
                                            <DatePicker
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                placeholderText="00/00/0000"

                                            />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-12 Quot-Home_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={quoteshome}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">
                                        <div className="tBL_des">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled it to make a</div>
                                        <a className="short_buttons_01 pull-right">View Task</a>
                                    </div>}
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>

                <div className="row">
                    <div className="col-lg-3 pull-right">
                    <a class="C_NeW_BtN w-100"><span>Approved Selected</span><i class="icon icon icon-check"></i></a>
                    </div>
                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(QuotesDashboard);