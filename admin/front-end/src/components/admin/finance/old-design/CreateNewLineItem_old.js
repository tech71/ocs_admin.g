import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import { connect } from 'react-redux'





class CreateNewLineItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">

                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    <a className="but">Retrive Payroll  Tax Information Via MYOB</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div className="Finance__panel_1 mt-5">
                    <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F" >
                        {/* <PanelGroup accordion id="accordion-example"> */}
                        <Panel eventKey="1">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Line Item Categories</p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2">

                                        <div className="row mt-5">
                                            <div className="col-lg-6 col-md-6 col-sm-6">
                                                <div className="sLT_gray left left-aRRow">
                                                    <Select
                                                        name="form-field-name"
                                                        value="one"
                                                        options={selectOpt}
                                                        onChange={logChange}
                                                        clearable={false}
                                                        searchable={false}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-md-6 col-sm-6">
                                                <div className="sLT_gray left left-aRRow">
                                                    <Select
                                                        name="form-field-name"
                                                        value="one"
                                                        options={selectOpt}
                                                        onChange={logChange}
                                                        clearable={false}
                                                        searchable={false}
                                                    />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5">
                                            <div className="col-lg-12">
                                                <label className="label_2_1_1">Support Registation Group</label>
                                                <div className="Parent-List_2_ul">
                                                    <div className="cstmSCroll1 FScroll">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                        >
                                                            <ul className="List_2_ul">
                                                                <li className="w-60">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                                <li className="w-40">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                            </ul>
                                                        </ScrollArea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row mt-5">
                                            <div className="col-lg-12">
                                                <label className="label_2_1_1">Support Category</label>
                                                <div className="Parent-List_2_ul">
                                                    <div className="cstmSCroll1 FScroll">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                        >
                                                            <ul className="List_2_ul">
                                                                <li className="w-60">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                                <li className="w-40">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                            </ul>
                                                        </ScrollArea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row mt-5">
                                            <div className="col-lg-12">
                                                <label className="label_2_1_1">Support Outcome Domain</label>
                                                <div className="Parent-List_2_ul">
                                                    <div className="cstmSCroll1 FScroll">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                        >
                                                            <ul className="List_2_ul">
                                                                <li className="w-60">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                                <li className="w-40">
                                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">
                                                                        Lorem Ipsum is simply dummy text
                                            </span>
                                                                </li>
                                                            </ul>
                                                        </ScrollArea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </Panel.Body>
                        </Panel>

                        <Panel eventKey="2">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Line Item Details </p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2">
                                        <div className="row mt-5">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Reference Number</label>
                                                <input  type="text"/>
                                            </div>
                                            <div className="col-lg-8 col-sm-8">
                                                <label className="label_2_1_1">Line Item Name</label>
                                                <input  type="text"/>
                                            </div>
                                        </div>
                                        <div className="row mt-5">
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">Start Date</label>
                                                <input  type="text"/>
                                            </div>
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">End Date</label>
                                                <input  type="text"/>
                                            </div>
                                        </div>
                                        <div className="row mt-5">
                                            <div className="col-lg-8 col-sm-12">
                                                <label className="label_2_1_1">Start Date</label>
                                                <textarea className="w-100" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Panel.Body>
                        </Panel>

                        <Panel eventKey="3">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Line Item Pricing</p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2">
                                        <div className="row mt-5">
                                            <div className="col-lg-4">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Quote Required?</label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Does this Item have a Price Control? </label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5">
                                            <div className="col-lg-4">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Travel Required?</label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Cancellation Fees? </label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5">
                                            <div className="col-lg-4">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">NDIS Reporting?</label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-lg-8">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Non-F2F? </label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5">
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">Member to Participant Ratio</label>
                                                <div className="ratio_input">
                                                    <span><input type="text"/></span>
                                                    <span>:</span>
                                                    <span><input type="text"/></span>
                                                </div>
                                            </div>
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">Upper Price Limit (National Non Remote)</label>
                                                <input  type="text"/>
                                            </div>
                                        </div>

                                        <div className="row mt-5">
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">National Remote Price</label>
                                                <input  type="text"/>
                                            </div>
                                            <div className="col-lg-4 col-sm-6">
                                                <label className="label_2_1_1">National Very Remote Price</label>
                                                <input  type="text"/>
                                            </div>
                                        </div>


                                        <div className="row mt-5">
                                            <div className="col-lg-12">
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_1">Does this item is have a schedule constraint ?</label>
                                                    <label className="label_2_1_2">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Yes</span>
                                                    </label>
                                                    <label className="label_2_1_3">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>No</span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>



                                        <div className="row mt-5">
                                            <div className="col-lg-6">
                                                <label className="label_2_1_1">Type Of Day</label>
                                                <div className="Parent-List_2_ul">
                                                    <div className="cstmSCroll1 FScroll">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                        >
                                                            <ul className="List_2_ul">
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1"> Weekend</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Saturday</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Sunday</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Public Holiday</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Standard</span>
                                                                </li>
                                                            </ul>
                                                        </ScrollArea>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-lg-6">
                                                <label className="label_2_1_1">Time of Day</label>
                                                <div className="Parent-List_2_ul">
                                                    <div className="cstmSCroll1 FScroll">
                                                        <ScrollArea
                                                            speed={0.8}
                                                            contentClassName="content"
                                                            horizontal={false}

                                                            style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                        >
                                                            <ul className="List_2_ul">
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Daytime</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Evening</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Overnight</span>
                                                                </li>
                                                                <li className="w-100">
                                                                    <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                        <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                        <span className="checkround"></span>
                                                                    </label>
                                                                    <span className="text_2_0_1">Active Overnight</span>
                                                                </li>
                                                            </ul>
                                                        </ScrollArea>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </Panel.Body>
                        </Panel>
                        {/* </PanelGroup> */}
                    </div>
                </div>
            </React.Fragment >
        );
    }

}


const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateNewLineItem);