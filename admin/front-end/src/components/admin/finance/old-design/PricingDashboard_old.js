import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { connect } from 'react-redux'



class PricingDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const StatementsTable = [
            { LineItemNumber: 'Participant 1', LineItemName: 'Booker 1', StartDate: '$1000', EndDate: 'NDIS', UpperPriceLimit: '19/12/2205', FundingType: 'Monthly' },
            { LineItemNumber: 'Participant 1', LineItemName: 'Booker 1', StartDate: '$1000', EndDate: 'NDIS', UpperPriceLimit: '19/12/2205', FundingType: 'Monthly' },
            { LineItemNumber: 'Participant 1', LineItemName: 'Booker 1', StartDate: '$1000', EndDate: 'NDIS', UpperPriceLimit: '19/12/2205', FundingType: 'Monthly' },
        ]


        const PricingTable = [
            { itemnumber: '01_015_0107_1_1', itemname: 'Assitance With Self-Care Activities', startdate: '01/07/2019', enddate: '00/01/2019', limit: '$54.31', type: 'NDIS' },
            { itemnumber: '01_015_0107_1_1', itemname: 'Assitance With Self-Care Activities', startdate: '01/07/2019', enddate: '00/01/2019', limit: '$54.31', type: 'NDIS' },
            { itemnumber: '01_015_0107_1_1', itemname: 'Assitance With Self-Care Activities', startdate: '01/07/2019', enddate: '00/01/2019', limit: '$54.31', type: 'NDIS' },
            { itemnumber: '01_015_0107_1_1', itemname: 'Assitance With Self-Care Activities', startdate: '01/07/2019', enddate: '00/01/2019', limit: '$54.31', type: 'NDIS' }

        ]











        const columns = [
            {
                id: "itemnumber",
                accessor: "itemnumber",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Line Item Number</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "itemname",
                accessor: "itemname",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Line Item Name</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>
                    <div>
                        <div className="ellipsis_line__">{props.value}</div>
                    </div>
                </span>
            },
            {
                id: "startdate",
                accessor: "startdate",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Start Date</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "enddate",
                accessor: "enddate",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">End Date</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "limit",
                accessor: "limit",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Upper Price Limit (National Non Remote)</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "type",
                accessor: "type",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Funding Type</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "status",
                // accessor: "status",
                headerClassName: '_align_c__ header_cnter_tabl',
                className:'_align_c__',
                width: 160,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Status</div>
                    </div>,
                Expander: (props) =>
                    <div className="expander_bind">

                        {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
                            <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                            <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                            <span className="short_buttons_01 btn_color_offline">Assigned</span>
                            <span className="short_buttons_01 btn_color_archive">Assigned</span>
                            <span className="short_buttons_01 btn_color_unavailable">Assigned</span> */}
                        <span className="short_buttons_01">Assigned</span>

                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }

            }

        ]


        // const columns = [

        //     {
        //         id: "checkbox",
        //         accessor: "",
        //         headerClassName: 'Th_class_d1',
        //         className:'Tb_class_d1',
        //         Cell: (props) => {
        //             return (
        //                 <span>
        //                     <label className="Cus_Check_1">
        //                         <input type="checkbox" /><div className="chk_Labs_1"></div>
        //                     </label>
        //                 </span>
        //             );
        //         },
        //         Header: x => {
        //             return (
        //                 <span>
        //                     <label className="Cus_Check_1">
        //                         <input type="checkbox" /><div className="chk_Labs_1"></div>
        //                     </label>
        //                 </span>
        //             );

        //         },
        //         width: 50,
        //         resizable: false,
        //     },
        //     {
        //         id: "checkbox",
        //         accessor: "",
        //         headerClassName: 'Th_class_d1',
        //         className:'Tb_class_d1 Tb_class_d2',
        //         Cell: (props) => {
        //             return (
        //                 <span>
        //                     <label className="Cus_Check_1">
        //                         <input type="checkbox" /><div className="chk_Labs_1"></div>
        //                     </label>
        //                     <i className="icon icon-warning-im"></i>
        //                     <div>dofnasdfoasdifn</div>
        //                 </span>
        //             );
        //         },
        //         Header: x => {
        //             return (
        //                 <div className="Tb_class_d1 Tb_class_d2">
        //                     <span >
        //                         <label className="Cus_Check_1">
        //                             <input type="checkbox" /><div className="chk_Labs_1"></div>
        //                         </label>
        //                         <div>dofnasdfoasdifn</div>
        //                     </span>
        //                 </div>
        //             );

        //         },
        //         // width: 50,
        //         resizable: false,
        //     },
        //     {
        //         Header: "Task",
        //         id: "Task",
        //         accessor: "task",
        //         headerClassName: 'Th_class_d1',
        //         className:'Tb_class_d1 Tb_class_d2 ',
        //         Cell: props => <span>
        //             <i className="icon icon-userm1-ie"></i>
        //             <i className="icon icon-userf1-ie"></i>
        //             <div>{props.value}</div>
        //         </span>

        //     },
        //     {
        //         Header: "Due Date",
        //         id: "DueDate",
        //         accessor: "duedate",
        //         headerClassName: '_align_c__',
        //         className:'_align_c__',
        //         Cell: props => <span>{props.value}</span>
        //     },
        //     {
        //         Header: "Due Date",
        //         id: "DueDate",
        //         accessor: "duedate",
        //         headerClassName: '_align_c__',
        //         className:'_align_c__',
        //         Cell: props => <div>
        //             <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
        //             {/* <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_offline">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_archive">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_unavailable">Assigned</span>
        //                 <span className="short_buttons_01">Assigned</span> */}
        //         </div>
        //     },
        //     {
        //         id: "AssignedBy",
        //         accessor: "assign",
        //         Header: () => <div className="text-center">Status</div>,
        //         headerClassName: '_align_c__',
        //         className:'_align_c__',
        //         width: 160,
        //         resizable: false,
        //         headerStyle: { border: "0px solid #fff" },
        //         expander: true,
        //         Expander: (props) =>
        //             <div className="expander_bind">

        //                 {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
        //                 <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_offline">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_archive">Assigned</span>
        //                 <span className="short_buttons_01 btn_color_unavailable">Assigned</span> */}
        //                 <span className="short_buttons_01">Assigned</span>

        //                 {props.isExpanded
        //                     ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
        //                     : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
        //             </div>,
        //         style: {
        //             cursor: "pointer",
        //             fontSize: 25,
        //             padding: "0",
        //             textAlign: "center",
        //             userSelect: "none"
        //         }

        //     }

        // ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-6">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Import CSV File</span><i className="icon icon icon-download2-ie"></i></a>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Add New Line Item</span><i className="icon icon icon-add-icons"></i></a>
                                </div>
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                            <div className="row sort_row1-- after_before_remove">
                                <div className="col-lg-6 col-md-8 col-sm-8 ">
                                    <form method="post">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                <div className="sLT_gray left left-aRRow">

                                            <Select
                                                name="form-field-name"
                                                value="one"
                                                options={selectOpt}
                                                onChange={logChange}
                                                clearable={false}
                                                searchable={false}
                                            />
                                        </div>
                                    </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>From</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>To</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div className="row">
                    <div className=""></div>
                </div>


                <div className="row">

                    <div className="col-lg-12 L-I-P_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={PricingTable}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">

                                        <div className="tBL_des">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled it to make a</div>
                                        <div className="row">
                                            <div className="col-lg-4">
                                                <ul className="L-I-P_list">
                                                    <li className="w-100"><span className="text-right"><b>Rotio: </b>            </span>  <span className="align-self-center">1:1</span></li>
                                                    <li className="w-100"><span className="text-right"><b>Price Controlled: </b> </span>  <span className="align-self-center">Yes</span></li>
                                                    <li className="w-100"><span className="text-right"><b>Measured by: </b>      </span>  <span className="align-self-center">Hourly</span></li>
                                                </ul>
                                            </div>
                                            <div className="col-lg-4">
                                                <ul className="L-I-P_list">
                                                    <li className="w-100"><span className="text-right"><b>Schedule Contraints: </b>            </span>  <span className="align-self-center">Evening</span></li>
                                                    <li className="w-100"><span className="text-right"><b>National Remote: </b> </span>  <span className="align-self-center">$81.63</span></li>
                                                    <li className="w-100"><span className="text-right"><b>National Very Remote: </b>      </span>  <span className="align-self-center">$87.47</span></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12 text-right">
                                                 <a className="short_buttons_01 mr-3">Edit</a> 
                                                 <a className="short_buttons_01">Archive</a> </div>
                                        </div>

                                    </div>}
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(PricingDashboard);