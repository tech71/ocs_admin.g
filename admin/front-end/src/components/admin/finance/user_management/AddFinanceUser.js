import React, { Component } from 'react';
import ProfilePage from './ProfilePage';
import ScrollArea from 'react-scrollbar';
import { connect } from 'react-redux'
import { postData, handleShareholderNameChange,toastMessageShow } from 'service/common.js';
import jQuery from "jquery";
import { Redirect } from 'react-router-dom';

class AddFinanceUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            finance_permissions: [],
            select_all: false,
            redirectListing: false,
        }
    }
    
    componentDidMount(){
        this.getUserDetails(this.props.match.params.id);
    }
    
    getUserDetails = (adminId) => {
        postData('finance/FinanceUserManagement/get_finance_user_details_for_add', { adminId: adminId }).then((result) => {
            if(result.status){
                this.setState(result.data);
            }
        });
    }
    
    SelectAll = (e) => {
        var value = e.target.checked;
        this.setState({select_all : value})
        
        var finance_permissions = this.state.finance_permissions 
        var finance_permissions = finance_permissions.map((val,index) => {
            val.access = value;
            return val;
        })
        
        this.setState({select_all : value, finance_permissions: finance_permissions})
    }
    
    onSubmit = (e) => {
        e.preventDefault();
        var validate = jQuery("#add_staff").validate({ /* */ });

        if (jQuery("#add_staff").valid()) {
            this.setState({ loading: true });
            
            postData('finance/FinanceUserManagement/add_new_finance_staff', this.state).then((result) => {
                if (result.status) {
                    toastMessageShow('New user addedd successfully', 's');
                    this.setState({redirectListing: true});
                } else {
                    toastMessageShow(result.error, 'e');
                }
                 this.setState({ loading: false });
            });
        }
    }


    render() {
       
        return (
            <React.Fragment>
                {this.state.redirectListing? <Redirect to='/admin/finance/user_management' />: ''}
                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-6">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage {...this.state} />


                <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-8">
                                
                                    <form id="add_staff" method="post">
                                    <div className="row">
                                        <div className="col-md-12 mb-5">
                                            <label className="label_2_1_1 color"><strong>Finance User ID: </strong>{this.state.userId}</label>
                                        </div>
                                        
                                        <div className="col-md-12">
                                            <label className="label_2_1_1">Access Permissions</label>
                                            <div className="Parent-List_2_ul">
                                                <div className="cstmSCroll1 FScroll">
                                                    <ScrollArea
                                                        speed={0.8}
                                                        contentClassName="content"
                                                        horizontal={false}

                                                        style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                    >
                                                        <ul className="List_2_ul">
                                                            <li className="w-60">
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input onChange={this.SelectAll} type="checkbox" checked={this.state.select_all || ''} name="aboriginal_tsi" value="1" /><span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">ALL</span>
                                                            </li>
                                                            
                                                            {this.state.finance_permissions.map((val, index) => (
                                                            <li key={index+1} className={"w-"+ (((index%2) == 0)? "40":"60")}>
                                                                <label className="radio_F1 check_F1  mb-0" style={{ width: 'auto' }}>
                                                                    <input onChange={(e) => {handleShareholderNameChange(this, 'finance_permissions', index, 'access', e.target.checked); if(!e.target.checked){this.setState({select_all: false})} }} checked={val.access || ''}  type="checkbox" name="permission" value={val.permissionId}  /><span className="checkround"></span>
                                                                </label>
                                                                <span onClick={(e) => {handleShareholderNameChange(this, 'finance_permissions', index, 'access', !val.access, e); if(!e.target.checked){this.setState({select_all: false})} }} className="text_2_0_1">{val.title}</span>
                                                            </li>
                                                            ))}
                                                        </ul>
                                                    </ScrollArea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row mt-5 d-flex justify-content-end">
                                        <div className="col-lg-4">
                                            <button className="btn-1 w-100" onClick={this.onSubmit} disabled={this.state.loading}>
                                                Add To Finance Module
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(AddFinanceUser);