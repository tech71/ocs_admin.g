import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "service/Pagination.js";
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import SelectFinanceUser from './SelectFinanceUser'
import moment from 'moment'
import { postData, archiveALL, reFreashReactTable } from 'service/common.js';
import {  PAGINATION_SHOW } from 'config.js';



const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {

        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('finance/FinanceUserManagement/get_finance_user_list', Request).then((result) => {
            if (result.status) {
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }
        });
    });
};

const getOptionNewFinanceUser = (e) => {
    if (!e && e.length > 1) {
        return Promise.resolve({ options: [] });
    }

    return postData('finance/FinanceUserManagement/get_new_finance_user_name', { search: e }).then((json) => {
        return { options: json };
    });
}


class FinanceUserListing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            staffList: [],
            filter_by: 'active',
            finance: {},
        }

        this.reactTable = React.createRef();
    }

    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                staffList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }

    enableDisableUser = (userDetails) => {
        var extraParams = [];
        extraParams['confirm_button_position'] = 'right';
        if (userDetails.status == 1) {
            extraParams['confirm'] = 'Yes, Disable Account';
            extraParams['cancel'] = 'Cancel';
            extraParams['heading_title'] = 'Disable Finance User';

            var status = 0;
            var msg = 'Are you sure you want to disable the user account?'
        } else {
            extraParams['confirm'] = 'Yes, Enable Account';
            extraParams['cancel'] = 'Cancel';
            extraParams['heading_title'] = 'Enable Finance User';

            var status = 1;
            var msg = 'Are you sure you want to enable the user account?';
        }

        var req = { financeId: userDetails.id, status: status, adminId: userDetails.adminId };
        archiveALL(req, msg, 'finance/FinanceUserManagement/active_disable_finance_user', extraParams).then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            }
        });

    }

    closeSlectUser = (status) => {
        this.setState({ openSelectUser: false });
    }

    submitSearch = (e, key, value) => {
        if (e)
            e.preventDefault();

        var state = {};
        state[key] = value;

        this.setState(state, () => {
            var req = { search: this.state.search, filter_by: this.state.filter_by, start_date: this.state.start_date, end_date: this.state.end_date };
            this.setState({ filtered: req })
        })
    }

    selectChange = (e, selectedOption) => {
        if(e)
        this.setState({ finance: e });
    }


    render() {
        const columns = [
            {
                accessor: "FullName",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () => <div><div className="ellipsis_line__">Name</div></div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                accessor: "userId",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () => <div><div className="ellipsis_line__">Finance Id</div></div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                accessor: "service_area",
                sortable: false,
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () => <div><div className="ellipsis_line__">Service Area</div></div>,
                className: '_align_c__',
                Cell: props => <span>{props.original.finance_permissions}</span>
            },
            {
                accessor: "start_date",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () => <div><div className="ellipsis_line__">Start Date</div></div>,
                className: '_align_c__',
                Cell: props => <span>{moment(props.value).format('DD/MM/YYYY')}</span>
            },
            {
                accessor: "status",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                width: 160,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () => <div><div className="ellipsis_line__">Status</div></div>,
                Expander: (props) =>
                    <div className="expander_bind">
                       <div className="d-flex w-100 justify-content-center align-item-center">
                        {props.original.status == 1 ? <span className="short_buttons_01 btn_color_avaiable">Active</span> : <span className="short_buttons_01 btn_color_ndis">Disabled</span>}
                        </div>
                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: { cursor: "pointer", fontSize: 25, padding: "0", textAlign: "center", userSelect: "none" }
            }
        ];

        var options = [
            { value: 'all', label: 'All' },
            { value: 'disabled', label: 'Disabled' },
            { value: 'active', label: 'Active' },
        ];


        return (
            <React.Fragment>
                {this.state.finance.hasOwnProperty("value") &&
                    this.state.finance.value > 0 ? (
                        <Redirect to={'/admin/finance/add_finance_user/' + this.state.finance.value} />) : null}

                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                               
                                <div className="col-lg-3 d-flex align-self-center">
                                    {!this.state.createQuoteEnable ?
                                        <a onClick={() => this.setState({ createQuoteEnable: true })} className="C_NeW_BtN w-100">
                                            <span>Add Finance User</span><i className="icon icon icon-add-icons"></i>
                                        </a> :
                                        <div className="modify_select align-self-center w-100">
                                            <Select.Async
                                                cache={false}
                                                name="form-field-name"
                                                clearable={false}
                                                value={this.state.finance}
                                                loadOptions={(e) => getOptionNewFinanceUser(e)}
                                                placeholder='Search'
                                                onChange={(e) => this.selectChange(e, 'recruiter')}
                                                required={true}
                                            /></div>}
                                </div>

                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                            <div className="row sort_row1-- after_before_remove">
                                <div className="col-lg-6 col-md-4 col-sm-4">
                                    <form method="post" onSubmit={this.submitSearch}>
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" name="search" onChange={(e) => this.setState({ search: e.target.value })} className="srch-inp" placeholder="Search.." value={this.state.search} /><i onClick={this.submitSearch} className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div>

                                <div className="col-lg-2 col-md-4 col-sm-3">
                                <div className="sLT_gray left left-aRRow">
                                            <Select name="form-field-name"
                                                simpleValue={true}
                                                value={this.state.filter_by}
                                                options={options}
                                                onChange={(value) => this.submitSearch('', 'filter_by', value)}
                                                clearable={false}
                                                searchable={false}
                                            />
                                        </div>
                                </div>
                                <div className="col-lg-4 col-md-4 col-sm-5">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className="Fil_ter_ToDo justify-content-end">
                                                <label>From</label>
                                                <span>
                                                    <DatePicker
                                                        isClearable={true}
                                                        selected={this.state.start_date}
                                                        onChange={(value) => this.submitSearch('', 'start_date', value)}
                                                        placeholderText="00/00/0000"
                                                        dateFormat="DD/MM/YYYY"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="Fil_ter_ToDo justify-content-end">
                                                <label>To</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.end_date}
                                                        onChange={(value) => this.submitSearch('', 'end_date', value)}
                                                        placeholderText="00/00/0000"
                                                        isClearable={true}
                                                        dateFormat="DD/MM/YYYY"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className=""></div>
                </div>

                <div className="row">

                    <div className="col-lg-12 View-Finance-User_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                            <ReactTable
                                PaginationComponent={Pagination}
                                showPagination={this.state.staffList.length > PAGINATION_SHOW ? true : false}
                                ref={this.reactTable}
                                columns={columns}
                                manual
                                data={this.state.staffList}
                                filtered={this.state.filtered}
                                defaultFiltered={{filter_by: 'active'}}
                                pages={this.state.pages}
                                previousText={<span className="icon icon-arrow-left privious" ></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                loading={this.state.loading}
                                onFetchData={this.fetchData}
                                noDataText="No users found"
                                defaultPageSize={10}
                                className="-striped -highlight"

                                minRows={2}
                                collapseOnDataChange={false}
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">

                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="pRO_div  py-5 pl-5">
                                                    <div className="uSer_pRO">
                                                        <span>
                                                            <img src={props.original.profile_image} />
                                                        </span>
                                                    </div>
                                                    <div className="dEtail_pRO pLace_left_side">
                                                        <div>
                                                            <ul>
                                                                <li>
                                                                    <span className="sUB_heding_tBL">Contact </span>
                                                                </li>
                                                                <li>
                                                                    <span className="dEtail_a">Phone: </span>
                                                                    <span className="dEtail_b">{props.original.phone}</span>
                                                                </li>
                                                                <li>
                                                                    <span className="dEtail_a">Email: </span>
                                                                    <span className="dEtail_b">{props.original.email}</span>
                                                                </li>

                                                                <li className="mt-4">
                                                                    <span className="dEtail_a">Access Permissions: </span>
                                                                    <span className="dEtail_b">{props.original.finance_permissions}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-12 text-right">
                                                <Link to='./AddFinanceUsers' disabled={props.original.status == 1 ? false : true} className="short_buttons_01 F__bTN mr-3">Finance User Details</Link>
                                                {props.original.status == 1 ?
                                                    <a onClick={() => this.enableDisableUser(props.original)} className="short_buttons_01 btn_color_archive F__bTN">Disable Finance user</a> :
                                                    <a onClick={() => this.enableDisableUser(props.original)} className="short_buttons_01 btn_color_archive F__bTN">Enable Finance user</a>}
                                            </div>
                                        </div>
                                    </div>}
                            />
                        </div>
                    </div>

                </div>

                <SelectFinanceUser showModal={this.state.openSelectUser} closeModal={this.closeSlectUser} />
            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(FinanceUserListing);