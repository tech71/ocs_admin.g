import React, { Component } from 'react';
import 'react-select-plus/dist/react-select-plus.css';
import "react-datepicker/dist/react-datepicker.css";
import { postData } from 'service/common.js';
import 'react-toastify/dist/ReactToastify.css';
import 'service/jquery.validate.js';
import "service/custom_script.js";
import Select from 'react-select-plus';
import { Link} from 'react-router-dom';

import { connect } from 'react-redux'

const getOptionNewFinanceUser = (e) => {
    if (!e) {
        return Promise.resolve({ options: [] });
    }

    return postData('finance/FinanceUserManagement/get_new_finance_user_name', { search: e }).then((json) => {
        return { options: json };
    });
}


class SelectFinanceUser extends Component {
    constructor() {
        super();
        this.state = {
            finance:[],
        }
    }

    selectChange = (e, selectedOption) => {
        this.setState({ finance: e });
    }

    render() {
    return (
            <div className={this.props.showModal ? 'customModal show' : 'customModal'}>
                <div className="cstomDialog widBig">

                    <h3 className="cstmModal_hdng1--">
                        Select User
                        <span className="closeModal  icon icon-close1-ie" onClick={() => this.props.closeModal(false)}></span>
                    </h3>
                    <form id="add_staff" autoComplete="off">
                        <div className="row">
                            <div className="col-lg-8 col-md-8 col-sm-8">
                                <div className="csform-group">
                                <h3 className="mb-2"><strong>Search for a finance:</strong></h3>
                                    <div className=" cmn_select_dv srch_select12 vldtn_slct">
                                        <Select.Async
                                            cache={false}
                                            name="form-field-name"
                                            clearable={false}
                                            value={this.state.finance}
                                            loadOptions={(e) => getOptionNewFinanceUser(e)}
                                            placeholder='Search'
                                            onChange={(e) => this.selectChange(e, 'recruiter')}
                                            required={true}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                           
                        <div className="row">
                                <div className="col-md-12">
                                    <span className="">
                                        {this.state.finance.value?
                                        <Link to={'/admin/finance/add_finance_user/'+this.state.finance.value} disabled={this.state.loading} onClick={this.onSubmit} className="btn cmn-btn1 creat_task_btn__">Select User</Link>:''}
                                    </span>
                                </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    recruitment_area: state.RecruitmentReducer.recruitment_area
})

const mapDispatchtoProps = (dispach) => {
    return {
        //departmentData: (value) => dispach(setDepartmentData(value)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(SelectFinanceUser)