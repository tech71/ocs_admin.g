import React, { Component } from 'react';


class ProfilePage extends React.Component {
    render() {
        return (
            <div className="row mb-5">
                <div className="col-lg-12">
                    <div className="pRO_div bb-1 py-5">
                        <div className="uSer_pRO">
                            <span>
                                <img src={this.props.profile_image} />
                            </span>
                        </div>
                        <div className="dEtail_pRO">
                            <div>
                                <ul>
                                    <li>
                                        <span className="dEtail_a">Name: </span>
                                        <span className="dEtail_b">{this.props.fullname}</span>
                                    </li>
                                    <li>
                                        <span className="dEtail_a">Phone: </span>
                                        <span className="dEtail_b">{this.props.phone}</span>
                                    </li>
                                    <li>
                                        <span className="dEtail_a">Email: </span>
                                        <span className="dEtail_b">{this.props.email}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
            ;
    }
}

export default ProfilePage;
