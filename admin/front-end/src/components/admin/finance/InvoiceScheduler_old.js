import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'



class InvoiceScheduler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]

        const invoiceschrduler = [
            { orgname: 'Org 1', orgtype: 'House', invoiceschedule: 'Weekly', nexinvoicedue: '01/02/2019', lastinvoicesent: '01/02/2019' },
            { orgname: 'Org 2', orgtype: 'Site', invoiceschedule: 'Dortnightly', nexinvoicedue: '01/02/2019', lastinvoicesent: '01/02/2019' },
            { orgname: 'Org 3', orgtype: 'Sub Org', invoiceschedule: 'Monthly', nexinvoicedue: '01/02/2019', lastinvoicesent: '01/02/2019' },
        ]





        const columns = [

            {
                id: "orgtype",
                accessor: "orgtype",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Org Type</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },

            {
                id: "statementfor",
                accessor: "statementfor",
                headerClassName: 'Th_class_d1 header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Statement For</div>
                    </div>
                ,
                className: 'Tb_class_d1 Tb_class_d2 ',
                Cell: props => <span>
                    <i className="icon icon-userm1-ie"></i>
                    <i className="icon icon-userf1-ie"></i>
                    <div>{props.value}</div>
                </span>

            },
            {
                id: "invoiceschedule",
                accessor: "invoiceschedule",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Invoice Schedule</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "nexinvoicedue",
                accessor: "nexinvoicedue",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Next Invoice Due</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "lastinvoicesent",
                accessor: "lastinvoicesent",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Last Invoice Sent</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },

        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                            <span className="back_arrow">
                                <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <Link to='./CreateInvoiceNewScheduler' className="C_NeW_BtN w-100"><span>Create New Scheduler</span><i className="icon icon icon-add-icons"></i></Link>
                                </div>
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                            <div className="row sort_row1-- after_before_remove">
                                <div className="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                                    <form method="post">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12 mt-xs-3">
                                    <div className="sLT_gray left left-aRRow">
                                        <Select
                                            name="form-field-name"
                                            value="one"
                                            options={selectOpt}
                                            onChange={logChange}
                                            clearable={false}
                                            searchable={false}
                                        />
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 col-xs-12 my-xs-3">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>From</label>
                                                <span>
                                                    <DatePicker
                                                    autoComplete={'off'}
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-xs-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>To</label>
                                                <span>
                                                    <DatePicker
                                                    autoComplete={'off'}
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"

                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-12 Invoic-Scheduler_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={invoiceschrduler}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                            // SubComponent={(props) =>
                            //     <div className="tBL_Sub">

                            //     </div>}
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>

             


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(InvoiceScheduler);