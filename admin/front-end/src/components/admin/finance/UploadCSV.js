import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'





class UploadCSV extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="back_arrow py-4">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage />
            

                
                <div className="Finance__panel_1">
                    <div className="F_module_Panel E_ven_and_O_dd-color E_ven_color_ O_dd_color_ Border_0_F" >
                        {/* <PanelGroup accordion id="accordion-example"> */}
                        <Panel eventKey="1">
                            <Panel.Heading>
                                <Panel.Title toggle>
                                    <div>
                                        <p>Upload.CSV for import</p>
                                        <span className="icon icon-arrow-right"></span>
                                        <span className="icon icon-arrow-down"></span>
                                    </div>
                                </Panel.Title>
                            </Panel.Heading>
                            <Panel.Body collapsible>
                                <div className="row d-flex justify-content-center">
                                    <div className="col-lg-6 col-ms-6 col-xs-12">
                                        <div className="row mt-5">
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">Start Date</label>
                                                <input  type="text" />
                                            </div>
                                            <div className="col-lg-6 col-sm-6">
                                                <label className="label_2_1_1">End Date</label>
                                                <input  type="text" />
                                            </div>
                                        </div>



                                        <div className="row mt-5">
                                            <div className="col-lg-12 col-sm-12">
                                                <div className="File-Drag-and-Drop_bOX">
                                                    <div>
                                                        <input type="file" />
                                                        <div className="Drop_bOX_content_1">
                                                            <i className="icon icon-if-ic-file-upload-48px-352345"></i>
                                                            <span>
                                                                <h3>Drag & Drop</h3>
                                                                <div>or <span>Browser</span> for your files</div>
                                                            </span>
                                                        </div>
                                                        <div className="Drop_bOX_content_2">
                                                            <i className="icon icon-document3-ie"></i>
                                                            <span>
                                                                <div>for your files</div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div className="row mt-5">
                                            <div className="col-lg-12 col-sm-12">
                                                {/* <div className="Progress-bar_mYcustom">
                                    <div className="_mYcustom_1">
                                        <div className="_mYcustom_Bar" style={{ width: '10%' }}>
                                            <span>40 % Upload File</span>
                                        </div>
                                    </div>
                                </div> */}
                                                <div className="Progress-bar_mYcustom cNTer_mYcustom">
                                                    <div className="_mYcustom_1">
                                                        <div className="_mYcustom_Bar" style={{ width: '10%' }}>
                                                            <span>40 % Upload File</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5 d-flex justify-content-center">
                                            <div className="col-lg-9 col-sm-12 text-center">
                                                <div className="cUS_error_class error_cLass">
                                                    <i className="icon icon-alert"></i>
                                                    <span>Error, this file has already been uploaded. Please select another</span>
                                                </div>
                                                <div className="cUS_error_class success_cLass">
                                                    <i className="icon icon-input-type-check"></i>
                                                    <span>This CSV file has successfully been uploaded into the system. Once Confirmed, it will become available for use within the system</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row mt-5 d-flex justify-content-center">
                                            <div className="col-lg-5">
                                                <a className="but" disabled >Confirm import</a>
                                            </div>
                                        </div>

                                        <div className="row mt-5 d-flex justify-content-center">
                                            <div className="col-lg-5">
                                                <a className="but" >Refresh Upload</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </Panel.Body>
                        </Panel>
                        {/* </PanelGroup> */}
                    </div>
                </div>

               
            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(UploadCSV);