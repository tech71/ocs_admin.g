import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import ScrollArea from 'react-scrollbar';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'





class CreateNewLineItemGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "month",
                accessor: "month",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "financialyear",
                accessor: "financialyear",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "ydtpayroll",
                accessor: "ydtpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">YDT Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "monthpayroll",
                accessor: "monthpayroll",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month Payroll</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "autualpayment",
                accessor: "autualpayment",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Autual Payment</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <ProfilePage />
            
                
                <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="Bg_F_moule">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-6">
                                    <div className="row">
                                        <div className="col-lg-12 col-sm-12">
                                            <label className="label_2_1_1">Name of Line Item Group</label>
                                            <input type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Start Date</label>
                                            <input type="text"/>
                                        </div>
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">End Date</label>
                                            <input type="text"/>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            {/* <label className="label_2_1_1">Start Date</label> */}
                                            <div className="sLT_gray left left-aRRow">
                                                <Select
                                                    name="form-field-name"
                                                    value="one"
                                                    options={selectOpt}
                                                    onChange={logChange}
                                                    clearable={false}
                                                    searchable={false}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-6 col-sm-6">
                                            <label className="label_2_1_1">Select States Group Applies To </label>
                                            <div className="Parent-List_2_ul">
                                                <div className="cstmSCroll1 FScroll">
                                                    <ScrollArea
                                                        speed={0.8}
                                                        contentClassName="content"
                                                        horizontal={false}

                                                        style={{ paddingRight: '15px', maxHeight: "140px" }}
                                                    >
                                                        <ul className="List_2_ul">
                                                            <li className="w-50">
                                                                <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                    <span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Daytime</span>
                                                            </li>
                                                            <li className="w-50">
                                                                <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                    <span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Evening</span>
                                                            </li>
                                                            <li className="w-50">
                                                                <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                    <span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Overnight</span>
                                                            </li>
                                                            <li className="w-50">
                                                                <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                                    <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                                    <span className="checkround"></span>
                                                                </label>
                                                                <span className="text_2_0_1">Active Overnight</span>
                                                            </li>
                                                        </ul>
                                                    </ScrollArea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mt-5">
                                        <div className="col-lg-12">
                                            <div className="Time_line_lables">
                                                <label className="label_2_1_1">Invoice to NDIS?</label>
                                                <label className="label_2_1_2">
                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                        <input type="radio" name="aboriginal_tsi" value="1" />
                                                        <span className="checkround"></span>
                                                    </label>
                                                    <span>Yes</span>
                                                </label>
                                                <label className="label_2_1_3">
                                                    <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                        <input type="radio" name="aboriginal_tsi" value="1" />
                                                        <span className="checkround"></span>
                                                    </label>
                                                    <span>No</span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               
            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateNewLineItemGroup);