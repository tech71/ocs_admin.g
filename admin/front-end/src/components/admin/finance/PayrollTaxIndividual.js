import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { connect } from 'react-redux'



class PayrollTaxIndividual extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]

            const parrolltax=[
                {number:'01', des:'Total MYOB Wages (Excluding Tax, Super and Deducations)', amount:'$24,852,870,51'},
                {number:'02', des:'Salery Sacrifice', amount:'$0.00'},
                {number:'03', des:'Terminations', amount:'-$44.300.03'}
            ]

            const columns = [
                {
                    id: "number",
                    accessor: "number",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Number</div>
                        </div>
                    ,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    id: "des",
                    accessor: "des",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">Description</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
                {
                    // Header: "Fund Type",
                    id: "amount",
                    accessor: "amount",
                    headerClassName: '_align_c__ header_cnter_tabl',
                    Header: () =>
                        <div>
                            <div className="ellipsis_line__">YDT amount</div>
                        </div>,
                    className:'_align_c__',
                    Cell: props => <span>{props.value}</span>
                },
               
              
    
            ]
    


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle} March 2018/2019</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Create New Statements</span><i className="icon icon icon-add-icons"></i></a>
                                </div>
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                        <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-8 col-sm-8 ">
                        <form method="post">
                            <div className="search_bar right srchInp_sm actionSrch_st">
                                <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                    <div className="sLT_gray left left-aRRow">

                                <Select
                                    name="form-field-name"
                                    value="one"
                                    options={selectOpt}
                                    onChange={logChange}
                                    clearable={false}
                                    searchable={false}
                                />
                            </div>
                        </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>From</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="00/00/0000"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>To</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="00/00/0000"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>


                    </div>
                </div>

                <div className="row">

                  
                <div className="col-lg-3">
                        <div className="Left_info_Content_box">
                            <div className="body_bOX_left">


                                <h5>Payment Breakdown:</h5>
                                <div className="body_bOX_left_text_1 bb-1 pb-3">
                                    <div>[Line Item Number 1]: $300</div>
                                    <div>[Line Item Number 2]: $300</div>
                                    <div>[Line Item Number 3]: $300</div>
                                    <div>[Line Item Number 4]: $300</div>
                                    <div>Pension: 4000</div>
                                    <div>% of Pension: 100%</div>
                                    <div className="mt-3">Total: $1,125</div>
                                </div>

                                <div className="body_bOX_left_text_1 bb-1 py-3">
                                    <div>Schedule Start Date: 00/00/0000</div>
                                    <div>Schedule End Date: 00/00/0000</div>
                                    <div>Create Invoice - This</div>
                                    <div> Many Days Before Due:7</div>
                                </div>

                                <div className="body_bOX_left_text_1 pt-3">
                                    <div>Weekly Fee: $1,150</div>
                                    <div>
                                        <div className="Time_line_lables">
                                            <label className="label_2_1_2 w-100 pb-2">
                                                <label className="radio_F1 check_F1 mb-0" style={{ width: 'auto' }}>
                                                    <input type="checkbox" name="aboriginal_tsi" value="1" />
                                                    <span className="checkround"></span>
                                                </label>
                                                <span>Apply GST: $1,125</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div>GST Amount</div>
                                    <div className="mt-3 mb-3">Total Amount: $1,125</div>
                                </div>

                                <a className="short_buttons_01">Update</a>
                            </div>

                            {/* <div className="">
                                    <div className="Time_line_lables">
                                        <label className="label_2_1_2 w-100 pb-2">
                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                <input type="radio" name="aboriginal_tsi" value="1" />
                                                <span className="checkround"></span>
                                            </label>
                                            <span>SAMs Not Entered</span>
                                        </label>
                                        <label className="label_2_1_3 w-100  pb-2">
                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                <input type="radio" name="aboriginal_tsi" value="1" />
                                                <span className="checkround"></span>
                                            </label>
                                            <span>Change to SAMs Agreement</span>
                                        </label>
                                        <label className="label_2_1_3 w-100">
                                            <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                <input type="radio" name="aboriginal_tsi" value="1" />
                                                <span className="checkround"></span>
                                            </label>
                                            <span>Not Applicable to SAMs Agreement</span>
                                        </label>
                                    </div>
                                </div> */}


                        </div>
                    </div>

                    <div className="col-lg-9 Pay-Tax-Indivi_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={parrolltax}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(PayrollTaxIndividual);