import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { Redirect, Link } from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import {ROUTER_PATH} from 'config.js';
import { PanelGroup, Panel } from 'react-bootstrap';
import ProfilePage from './common/ProfilePage';
import { connect } from 'react-redux'
import { getOrganisationDetails, getParticipantDetails } from "./action/FinanceAction.js";
import { postData, toastMessageShow, handleChangeSelectDatepicker, handleRemoveShareholder, handleAddShareholder, handleChangeChkboxInput, handleShareholderNameChange, onlyNumberAllow, handleDateChangeRaw, selectFilterOptions, onKeyPressPrevent } from 'service/common.js';
import moment from 'moment-timezone';
import jQuery from "jquery";


const requestData = (pageSize, page, sorted, filtered) => {

    return new Promise((resolve, reject) => {
        if (filtered.length != 0) {
            var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
            postData('finance/FinanceInvoice/srch_statement_bydate', Request).then((result) => {
                let filteredData = result.data;
                console.log(filteredData);
                const res = {
                    rows: filteredData,
                    pages: (result.count),
                    all_count: result.all_count,
                };
                resolve(res);
            });
        }
    });
};

class CreateNewStatement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            srchShiftList: [],
            bookerCheck:true,
        }
    }

    componentWillMount() {
        this.setAndGetProfileData();
    }

    getStatementByDate = (state, instance) => {
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
        ).then(res => {
            this.setState({
                srchShiftList: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }
    searchStatementByDate = (e) => {
        this.setState({ statement_to_date: e }, () => {
            var requestData = { statement_from_date: this.state.statement_from_date, statement_to_date: this.state.statement_to_date, UserType: this.props.match.params.UserType, UserId: this.props.match.params.UserId };
            this.setState({ filtered: requestData });
        })
    }
    onChange_booker=(e)=>{
       this.setState({bookerCheck:e.target.checked})
    }
    handleSaveStatements = (e) => {

        var validator = jQuery("#create_new_statement").validate({ ignore: [], });

        e.preventDefault();


        if (jQuery("#create_new_statement").valid()) {

            this.setState({ loading: true }, () => {
                let requestData = this.state;

                if (this.props.match.params.UserType == 2) {
                    requestData['email'] = this.props.participantDetails.email;
                } else {
                    requestData['email'] = this.props.organisationDetails.email;
                }

                postData('finance/FinanceInvoice/save_statemenets', requestData).then((result) => {

                    if (result.status) {
                        this.setState({ loading: false });
                        toastMessageShow(result.msg, 's');                        
                        this.setState({ is_save: true }, () => { });
                        this.setState({ success: true })
                    } else {
                        toastMessageShow(result.msg, 'e');
                        this.setState({ loading: false });
                    }
                });
            });
        } else {
            validator.focusInvalid();
        }

    }
    setAndGetProfileData = () => {
        //console.log(this.props)
        if (this.props.match.params.UserType == 2)
            this.setState({ mode: 'participant', UserTypeInt: this.props.match.params.UserType, UserId: this.props.match.params.UserId }, () => { this.props.getParticipantDetails(this.props.match.params.UserId); })
        else
            this.setState({ mode: 'finance', UserTypeInt: this.props.match.params.UserType, UserId: this.props.match.params.UserId }, () => { this.props.getOrganisationDetails(this.props.match.params.UserId); })

    }
    render() {
       
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


        const payrolltaxtable = [
            { month: 'March', financialyear: '2019/2020', ydtpayroll: '$9,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'February', financialyear: '2019/2020', ydtpayroll: '$8,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
            { month: 'January', financialyear: '2019/2020', ydtpayroll: '$7,000,000', monthpayroll: '$1,000,000', autualpayment: '$500,000' },
        ]



        const columns = [
            {
                id: "id",
                accessor: "id",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Month</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>,
                show: false
            },
            {
                id: "invoice_id",
                accessor: "invoice_id",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Financial Year</div>
                    </div>,
                className: '_align_l__ color',
                Cell: props => (<Link target="_blank" to={{ pathname: `/admin/finance/invoice/view/${props.original.id}` }}><u>I_{props.value}_{props.original.invoice_type}_due date_({props.original.shift_due})</u> </Link>)
            },



        ]



        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                 {(this.state.success) ? <Redirect to='/admin/finance/StatementsDashboard' /> : ''}
                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4">
                            <span className="back_arrow">
                            <Link to={ROUTER_PATH+"admin/finance/StatementsDashboard"}><span className="icon icon-back1-ie"></span></Link>
                            </span>

                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-8">
                                    <div className="h-h1 color">Create New Statement for {(this.state.mode == 'finance') ? this.props.organisationDetails.contact_name : this.props.participantDetails.name}</div>
                                    <div className="h-h1 color">{/*this.props.showPageTitle*/}</div>
                                </div>
                                <div className="col-lg-4 d-flex align-self-center">
                                    {/* <a className="but">Retrive Payroll  Tax Information Via MYOB</a> */}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <ProfilePage details={(this.state.mode == 'finance') ? this.props.organisationDetails : this.props.participantDetails} mode={this.state.mode} fundingShow={true} bookertype={true} onChange_booker={this.onChange_booker} bookerCheck={this.state.bookerCheck} />
                <form id="create_new_statement" method="post">
                    <div className="row d-flex justify-content-center mt-5">
                        <div className="col-lg-12">
                            <div className="Bg_F_moule">


                                <div className="row d-flex justify-content-center">
                                    <div className="col-lg-8 col-sm-12">

                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-12 col-sm-12">
                                                <label className="label_2_1_1 color"><strong>Statement Number: </strong>dfasdfasd</label>
                                            </div>
                                        </div>

                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Date Range - From</label>
                                                <span className="required">
                                                <DatePicker
                                                    selected={this.state.statement_from_date}
                                                    required={true}
                                                    name={"from_date"}
                                                    dateFormat="DD/MM/YYYY"
                                                    placeholderText={"DD/MM/YYYY"}
                                                    onChange={(e) => handleChangeSelectDatepicker(this, e, 'statement_from_date')}
                                                    onChangeRaw={handleDateChangeRaw}
                                                    autoComplete={'off'}
                                                />
                                                </span>
                                            </div>
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Date Range - To</label>
                                                <span className="required">
                                                <DatePicker
                                                    selected={this.state.statement_to_date}
                                                    dateFormat="DD/MM/YYYY"
                                                    autoComplete={'off'}
                                                    onChangeRaw={handleDateChangeRaw}
                                                    placeholderText={"DD/MM/YYYY"}
                                                    className="text-left"
                                                    name={"to_date"}
                                                    required={true}
                                                    onChange={(e) => this.searchStatementByDate(e)}
                                                />
                                                </span>

                                            </div>
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-12 finance-statement_Table">
                                                <div className={((((this.state.srchShiftList).length) > 0) ? '' : 'd-none') + " listing_table PL_site odd_even_tBL  odd_even_marge-2_tBL line_space_tBL H-Set_tBL"}>
                                                    <ReactTable
                                                        TheadComponent={_ => null}
                                                        data={this.state.srchShiftList}
                                                        columns={columns}
                                                        PaginationComponent={Pagination}
                                                        noDataText="No Record Found"
                                                        minRows={1}
                                                        className="-striped -highlight"
                                                        onFetchData={this.getStatementByDate}
                                                        defaultPageSize={10}
                                                        pages={this.state.pages}
                                                        showPagination={false}
                                                        loading={((this.state.srchShiftList).length) > 0 ? this.state.loading : ''}
                                                        filtered={this.state.filtered}
                                                        previousText={<span className="icon icon-arrow-left privious"></span>}
                                                        nextText={<span className="icon icon-arrow-right next"></span>}
                                                    />
                                                </div>
                                            </div>


                                        </div>
                                        <div className="row mt-5  d-flex">
                                            {/*<div className="col-lg-12" >
                                                <div className="Time_line_lables">
                                                    <label className="label_2_1_2 mr-5">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Itemised</span>
                                                    </label>
                                                    <label className="label_2_1_3 mr-5">
                                                        <label className="radio_F1 mb-0" style={{ width: 'auto' }}>
                                                            <input type="radio" name="aboriginal_tsi" value="1" />
                                                            <span className="checkround"></span>
                                                        </label>
                                                        <span>Full Statement</span>
                                                    </label>
                                                </div>
                                            </div>*/ }
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Issue Date</label>
                                                <span className="required">
                                                <DatePicker
                                                    autoComplete={'off'}
                                                    selected={this.state.statement_issue_date}
                                                    required={true}
                                                    name={"issue_date"}
                                                    dateFormat="DD/MM/YYYY"
                                                    placeholderText={"DD/MM/YYYY"}
                                                    onChange={(e) => handleChangeSelectDatepicker(this, e, 'statement_issue_date')}
                                                    onChangeRaw={handleDateChangeRaw}
                                                />
                                                </span>

                                            </div>
                                            <div className="col-lg-4 col-sm-4">
                                                <label className="label_2_1_1">Due Date</label>
                                                <span className="required">
                                                <DatePicker
                                                    autoComplete={'off'}
                                                    selected={this.state.statement_due_date}
                                                    required={true}
                                                    name={"due_date"}
                                                    dateFormat="DD/MM/YYYY"
                                                    placeholderText={"DD/MM/YYYY"}
                                                    onChange={(e) => handleChangeSelectDatepicker(this, e, 'statement_due_date')}
                                                    onChangeRaw={handleDateChangeRaw}
                                                />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="row mt-5  d-flex">
                                            <div className="col-lg-8 col-sm-12">
                                                <label className="label_2_1_1">Add Notes To Quote</label>
                                                <span className="required">
                                                <textarea className="w-100" data-rule-required="true" name="statement_shift_notes" onChange={(e) => handleChangeChkboxInput(this, e)}></textarea>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-3 pull-right">
                                                <a onClick={(e) => this.handleSaveStatements(e)} className="btn-1 w-100">Generate Statement</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType,
    organisationDetails: state.FinanceReducer.organisationDetails,
    participantDetails: state.FinanceReducer.participantDetails
})
const mapDispatchtoProps = (dispach) => {
    return {
        getOrganisationDetails: (orgId) => dispach(getOrganisationDetails(orgId)),
        getParticipantDetails: (PartId) => dispach(getParticipantDetails(PartId))
    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(CreateNewStatement);