import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { CounterShowOnBox } from 'service/CounterShowOnBox.js';
import { Link } from 'react-router-dom';
import { graphViewType } from 'service/custom_value_data.js';
import _ from 'lodash';
import PropTypes from 'prop-types';
import createClass from 'create-react-class';
import { postData,selectFilterOptions } from 'service/common.js';
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route,  Redirect } from "react-router-dom";

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve) => {

        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('finance/FinanceInvoice/dashboard_finance_invoice_statement_list', Request).then((result) => {
            if (result.status) {
                console.log(result);
                const res = { rows: result.data, pages: (result.count) };
                resolve(res);
            }
        });
    });
};

class StatementsDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
            graphType: graphViewType,
            prospective_app_count_data:20,
            view_type_org:'week',
            view_type_prt:'week'

        }
    }


    fetchData = (state) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                lineItems: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
            });
        })
    }
    
    componentDidMount() {
        this.participantAjax();
        this.organizationAjax();
    }

    participantCountBy = (type) => {
        this.setState({ view_type_prt: type }, function () {
            this.participantAjax();
        });
    }

    participantAjax = () => {
        let requestData={view_type:this.state.view_type_prt}
        postData('finance/FinanceInvoice/get_outstanding_statements_participants_count', requestData).then((result) => {
            if (result.status) {
                this.setState({ participantCount: result.data.unpaid_participant});                
            } else {
                this.setState({ error: result.error });
            }
        });
    }
    organizationCountBy = (type) => {
        this.setState({ view_type_org: type }, function () {
            this.organizationAjax();
        });
    }
    organizationAjax = () => {
        let requestData={view_type:this.state.view_type_org}
        postData('finance/FinanceInvoice/get_outstanding_statements_organization_count', requestData).then((result) => {            
            if (result.status) {
                this.setState({ organizationCount: result.data.unpaid_org});                
            } else {
                this.setState({ error: result.error });
            }
        });
    }
    getStatmentStatus = (status) => {
        //0 -Not Send/1 - Send/ 2 - Send n read / 3- error	
        switch (status) {            
            case '1':
                return <span className="short_buttons_01 btn_color_assigned" >Sent</span>
                break;
            case '2':
                return <span className="short_buttons_01 btn_color_avaiable" >Sent & Read</span>
                break;
            case '3':
                return <span className="short_buttons_01 btn_color_ihcyf" >Error Sending</span>
                break;
            default:
                return <span className="short_buttons_01 btn_color_unavailable" >Not Sent</span>
        }
    }





    render() {
        const columns = [
            {
                id: "id",
                accessor: "id",
                headerClassName: 'Th_class_d1 header_cnter_tabl checkbox_header',
                className: 'Tb_class_d1 Tb_class_d2',
                Cell: (props) => {
                    return (
                        <span>
                            <label className="Cus_Check_1">
                                <input type="checkbox" /><div className="chk_Labs_1"></div>
                            </label>
                            <div>{props.original.statement_number}</div>
                        </span>
                    );
                },
                Header: x => {
                    return (
                        <div className="Tb_class_d1 Tb_class_d2">
                            <span >
                                <label className="Cus_Check_1">
                                    <input type="checkbox" /><div className="chk_Labs_1"></div>
                                </label>
                                <div>Statement Number</div>
                            </span>
                        </div>
                    );

                },
                resizable: false,
                width: 230
            },
            {
                id: "id",
                accessor: "statement_for",
                headerClassName: 'Th_class_d1 header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Statement For</div>
                    </div>
                ,
                className: 'Tb_class_d1 Tb_class_d2 ',
                Cell: props => <span>{props.value}</span>,

            },
            {
                id: "AddressedTo",
                accessor: "addressedto",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Addressed To</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "total",
                accessor: "total",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Amount</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "FundType",
                accessor: "fundtype",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Fund Type</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },            
            {
                // Header: "Status",
                id: "statement_type",
                accessor: "statement_type",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Statement Type</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "status",
                accessor: "status",
                headerClassName: '_align_c__ header_cnter_tabl',
                className: '_align_c__',
                width: 160,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Status</div>
                    </div>,
                Expander: (props) =>
                    <div className="expander_bind">
                          <div className="d-flex w-100 justify-content-center align-item-center">
                        {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
                            <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                            <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                            <span className="short_buttons_01 btn_color_offline">Assigned</span>
                            <span className="short_buttons_01 btn_color_archive">Assigned</span>
                            <span className="short_buttons_01 btn_color_unavailable">Assigned</span> */}
                           {this.getStatmentStatus(props.original.status)}
                        </div>

                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }

            }

        ]
        const getOptionParticipantOrganization = (e) => {
            if (!e) {
                return Promise.resolve({ options: [] });
            }
            return postData('finance/FinanceInvoice/get_participant_organization_name', { search: e }).then((res) => {
                if(res.status){
                    return { options: res.data };
                }
            });
        }

        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }
        const searchOnChange = (e) => { 
            console.log(e);
            this.setState({selectedUserType: e.type, selectedUserId: e.value});
        }
        const GravatarOption = createClass({
            propTypes: {
                children: PropTypes.node,
                className: PropTypes.string,
                isDisabled: PropTypes.bool,
                isFocused: PropTypes.bool,
                isSelected: PropTypes.bool,
                onFocus: PropTypes.func,
                onSelect: PropTypes.func,
                option: PropTypes.object.isRequired,
            },
            handleMouseDown(event) {
                event.preventDefault();
                event.stopPropagation();
                this.props.onSelect(this.props.option, event);
            },
            handleMouseEnter(event) {
                this.props.onFocus(this.props.option, event);
            },
            handleMouseMove(event) {
                if (this.props.isFocused) return;
                this.props.onFocus(this.props.option, event);
            },
            render() {
                
                return (
                    <div className={this.props.className}
                        onMouseDown={this.handleMouseDown}
                        onMouseEnter={this.handleMouseEnter}
                        onMouseMove={this.handleMouseMove}
                        title={this.props.option.title}>
                        <div className="Select_Search_Type_">
                            <div className="text_set">{this.props.children}</div>
                            <span className={"Default_icon "+ ((this.props.option.type == 2)?"Participant_icon" : "Org_icon")}><i className="icon icon icon-userm1-ie"></i></span>
                        </div>
                    </div>
                );
            }
        });
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                    {this.state.selectedUserId ? 
                        <Redirect to={'/admin/finance/createNewStatement/'+this.state.selectedUserType+'/'+this.state.selectedUserId} />: ''}

                        <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                
                                <div className="col-lg-3">
                                <div className="modify_select align-self-center ">
                                      {!this.state.createQuoteEnable? 
                                            <a onClick={() => this.setState({createQuoteEnable: true})} className="C_NeW_BtN w-100">
                                                <span>Create New Statements</span><i className="icon icon icon-add-icons"></i>
                                            </a>: 
                                            <div className="modify_select align-self-center w-100">
                                            <Select.Async
                                                cache={false}
                                                newOptionCreator={({ label: '', labelKey: '', valueKey: '' })}
                                                name="form-field-name"
                                                ignoreCase={true}
                                                matchProp={'any'}
                                                clearable={false}
                                                value={this.state.finance}
                                                filterOptions={selectFilterOptions}
                                                loadOptions={(e) => getOptionParticipantOrganization(e)}
                                                placeholder='Search'
                                                onChange={(e) => searchOnChange(e)}
                                                required={true}
                                                optionComponent={GravatarOption}
                                            /></div>}       
                                </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>

                <div className="row d-flex  justify-content-center mt-5">
                    <div className="col-lg-8">


                        <div className="row">

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Outstanding Statements - Participants</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.participantCount} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                    <li onClick={() => this.participantCountBy('week')} className={this.state.view_type_prt == 'week' ? 'color' : ''}>Week</li>
                                                    <li onClick={() => this.participantCountBy('month')} className={this.state.view_type_prt == 'month' ? 'color' : ''}>Month </li>
                                                    <li onClick={() => this.participantCountBy('year')} className={this.state.view_type_prt == 'year' ? 'color' : ''}> Year</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div className="status_box1 d-flex flex-wrap">
                                    <div className="d-flex flex-wrap w-100">
                                        <h4 className="hdng w-100">Outstanding Statements - Orgs</h4>
                                        <div className="w-100">
                                            <CounterShowOnBox counterTitle={this.state.organizationCount} classNameAdd="" mode="other" />
                                        </div>
                                        <div className="colJ-1 w-100">
                                            <div className="duly_vw">
                                                <div className="viewBy_dc text-center">
                                                    <h5>View By:</h5>
                                                    <ul>
                                                    <li onClick={() => this.organizationCountBy('week')} className={this.state.view_type_org == 'week' ? 'color' : ''}>Week</li>
                                                    <li onClick={() => this.organizationCountBy('month')} className={this.state.view_type_org == 'month' ? 'color' : ''}>Month </li>
                                                    <li onClick={() => this.organizationCountBy('year')} className={this.state.view_type_org == 'year' ? 'color' : ''}> Year</li>                                                       
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

                <div className="by-1 mb-4">
                    <div className="row sort_row1-- after_before_remove">
                        <div className="col-lg-4 col-md-4 col-sm-4 ">
                            <form method="post">
                                <div className="search_bar right srchInp_sm actionSrch_st">
                                    <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-3 ">
                        <div className="sLT_gray left left-aRRow">

                                    <Select
                                        name="form-field-name"
                                        value="one"
                                        options={selectOpt}
                                        onChange={logChange}
                                        clearable={false}
                                        searchable={false}
                                    />
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-5 ">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="Fil_ter_ToDo">
                                        <label>From</label>
                                        <span>
                                            <DatePicker
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                placeholderText="00/00/0000"
                                                autoComplete={'off'}
                                            />
                                        </span>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="Fil_ter_ToDo">
                                        <label>To</label>
                                        <span>
                                            <DatePicker
                                                selected={this.state.startDate}
                                                onChange={this.handleChange}
                                                placeholderText="00/00/0000"
                                                autoComplete={'off'}
                                            />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12 Finance-Statement_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                            <ReactTable
                                //data={StatementsTable}
                                data={this.state.lineItems}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                onFetchData={this.fetchData}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">
                                        <div className="tBL_des">
                                            {(props.original.statement_notes)?<span><p>Statement Notes:</p>{props.original.statement_notes}</span>:''}                                            
                                            </div>
                                        <Link to={"/admin/finance/statement/view/"+props.original.id} className="short_buttons_01 pull-right ml-2">View</Link>
                                        {/*<span onClick={()=>reSendInvoiceEmail(this,props.original.id)} className="short_buttons_01 pull-right ml-2">Resend Email</span> */}
                                    </div>}
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>


            </React.Fragment >
        );
    }

}
const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(StatementsDashboard);