import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { connect } from 'react-redux'



class LineItemPricingGroups extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {
        const dataTable = [
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            { task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley', },
            {
                task: 'Task 1', duedate: '10/10/2025', assign: 'Tanner Linsley',
            },]


            const PricingGroupTable=[
                {groupname:'Group 1', creationdate:'01/01/1920', locality:'Regional', States:'ACT,NSW, QLD, VIC', invoicendis:'input 5'},
                {groupname:'Group 2', creationdate:'01/01/1920', locality:'Remote', States:'WA', invoicendis:'input 5'},
                {groupname:'Group 3', creationdate:'01/01/1920', locality:'VeryRemote', States:'NT, SA', invoicendis:'input 5'},
                ]



        const columns = [
            {
                id: "groupname",
                accessor: "groupname",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Group Name</div>
                    </div>
                ,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "creationdate",
                accessor: "creationdate",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Creation Date</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Fund Type",
                id: "locality",
                accessor: "locality",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">locality</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Date Of Last Issue",
                id: "States",
                accessor: "States",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">States</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                // Header: "Timeframe",
                id: "invoicendis",
                accessor: "invoicendis",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Invoice to NDIS</div>
                    </div>,
                className:'_align_c__',
                Cell: props => <span>{props.value}</span>
            },
          
            {
                id: "status",
                // accessor: "status",
                headerClassName: '_align_c__ header_cnter_tabl',
                className:'_align_c__',
                width: 180,
                resizable: false,
                headerStyle: { border: "0px solid #fff" },
                expander: true,
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Action</div>
                    </div>,
                Expander: (props) =>
                    <div className="expander_bind">

                        {/* <span className="short_buttons_01 btn_color_assigned" >Assigned</span>
                            <span className="short_buttons_01 btn_color_avaiable">Assigned</span>
                            <span className="short_buttons_01 btn_color_ihcyf">Assigned</span>
                            <span className="short_buttons_01 btn_color_offline">Assigned</span>
                            <span className="short_buttons_01 btn_color_archive">Assigned</span>
                            <span className="short_buttons_01 btn_color_unavailable">Assigned</span> */}
                        <span className="short_buttons_01 mr-1">Update</span>
                        <span className="short_buttons_01">Archive</span>

                        {props.isExpanded
                            ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                            : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                    </div>,
                style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                }

            }

        ]


        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">


                    <div className=" py-4">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>


                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-9">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Create New Group</span><i className="icon icon icon-add-icons"></i></a>
                                </div>
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                        <div className="row sort_row1-- after_before_remove">
                    <div className="col-lg-6 col-md-8 col-sm-8 ">
                        <form method="post">
                            <div className="search_bar right srchInp_sm actionSrch_st">
                                <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                    <div className="sLT_gray left left-aRRow">
                                <Select
                                    name="form-field-name"
                                    value="one"
                                    options={selectOpt}
                                    onChange={logChange}
                                    clearable={false}
                                    searchable={false}
                                />
                         
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-4 ">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>From</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="00/00/0000"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="Fil_ter_ToDo">
                                    <label>To</label>
                                    <span>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleChange}
                                            placeholderText="00/00/0000"
                                            autoComplete={'off'}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>


                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-12  L-I-P-G_Table">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL   odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={PricingGroupTable}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={true}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                SubComponent={(props) =>
                                    <div className="tBL_Sub">
                                        <div className="tBL_des">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                     when an unknown printer took a galley of type and scrambled it to make a</div>
                                        <a className="short_buttons_01 pull-right">View Task</a>
                                    </div>}
                            />
                        </div>
                        {/* <div className="d-flex justify-content-between mt-3">
                            <a className="btn B_tn" href="#">Mark Selected As Complete</a>
                            <a className="btn B_tn" href="#">View all Taks</a>
                        </div> */}
                    </div>

                </div>


            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(LineItemPricingGroups);