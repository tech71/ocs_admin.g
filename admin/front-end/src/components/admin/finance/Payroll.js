import React, { Component } from 'react';
import ReactTable from 'react-table';
import Select from 'react-select-plus';
import DatePicker from 'react-datepicker';
import Pagination from "../../../service/Pagination.js";
import { Chart } from "react-google-charts";
import MonthChart from './common/MonthChart';
import ScrollArea from "react-scrollbar";
import classNames from "classnames";
import { connect } from 'react-redux'




const CustomTbodyComponent = props => (
    <div {...props} className={classNames("rt-tbody", props.className || [])}>
        <div className="cstmSCroll1 FScroll">
            <ScrollArea
                speed={0.8}
                contentClassName="content"
                horizontal={false}

                style={{ paddingRight: '15px', maxHeight: "400px" }}
            >
                {props.children}
            </ScrollArea>
        </div>
    </div>
);


class Payroll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeCol: '',
        }
    }


    render() {

        const PricingTable = [
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', },
            { number: '1', des: 'Assitance With Self-Care Activities', amount: '$54.31', }

        ]


        const columns = [
            {
                id: "number",
                accessor: "number",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Number</div>
                    </div>
                ,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },
            {
                id: "des",
                accessor: "des",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Description</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>
                    <div>
                        <div className="ellipsis_line__">{props.value}</div>
                    </div>
                </span>
            },
            {
                id: "amount",
                accessor: "amount",
                headerClassName: '_align_c__ header_cnter_tabl',
                Header: () =>
                    <div>
                        <div className="ellipsis_line__">Amonut</div>
                    </div>,
                className: '_align_c__',
                Cell: props => <span>{props.value}</span>
            },



        ]

        var selectOpt = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        function logChange(val) {
            //console.log("Selected: " + val);
        }

        const chartData = [
            [
                "Qty.",
                "This Year",
                "Last Year",
                "Two Years Ago",
            ],
            ["Jan", 2500, 5000, 2000],
            ["Feb", 1500, 2500, 5000],
            ["Mar", 1300, 1500, 9000],
            ["Apr", 5000, 1200, 1400],
            ["May", 1526, 1517, 1200],
            ["Jun", 5000, 4445, 1700],
            ["Jul", 3500, 4500, 1000],
            ["Aug", 1526, 1517, 1500],
            ["Sep", 1526, 1517, 1400],
            ["Oct", 1526, 1517, 1200],
            ["Nov", 1526, 1517, 3000],
            ["Dec", 1526, 1517, 1000]
        ];

        const setChartOptions = {
            chartArea: { width: "80%" },
            isStacked: false,
            legend: { position: "bottom", alignment: "center" },
            backgroundColor: '#ececec',
            colors: ['#43c148', '#5291d6', '#9855d5'],
            vAxis: {
                gridlines: {
                    color: 'transparent'
                }
            }
        }


        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <div className=" py-4 invisible">
                        <span className="back_arrow">
                            <a href="/admin/crm/participantadmin"><span className="icon icon-back1-ie"></span></a>
                            </span>
                        </div>

                        <div className="by-1">
                            <div className="row d-flex  py-4">
                                <div className="col-lg-6">
                                    <div className="h-h1 color">{this.props.showPageTitle}</div>
                                </div>
                                {/* <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Import CSV File</span><i className="icon icon icon-download2-ie"></i></a>
                                </div>
                                <div className="col-lg-3 d-flex align-self-center">
                                    <a className="C_NeW_BtN w-100"><span>Add New Line Item</span><i className="icon icon icon-add-icons"></i></a>
                                </div> */}
                            </div>
                        </div>

                        <div className="bb-1 mb-4">
                            <div className="row sort_row1-- after_before_remove">
                                {/* <div className="col-lg-6 col-md-8 col-sm-8 ">
                                    <form method="post">
                                        <div className="search_bar right srchInp_sm actionSrch_st">
                                            <input type="text" className="srch-inp" placeholder="Search.." value="" /><i className="icon icon-search2-ie"></i>
                                        </div>
                                    </form>
                                </div> */}
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                <div className="sLT_gray left left-aRRow">
                                            <Select
                                                name="form-field-name"
                                                value="one"
                                                options={selectOpt}
                                                onChange={logChange}
                                                clearable={false}
                                                searchable={false}
                                            />
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-4 col-sm-4 ">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>From</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="Fil_ter_ToDo">
                                                <label>To</label>
                                                <span>
                                                    <DatePicker
                                                        selected={this.state.startDate}
                                                        onChange={this.handleChange}
                                                        placeholderText="00/00/0000"
                                                        autoComplete={'off'}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div className="row">
                    <div className=""></div>
                </div>


                <div className="row">

                    <div className="col-lg-12 F-Payroll_tBL">
                        <div className="listing_table PL_site th_txt_center__ odd_even_tBL odd_even_marge-1_tBL line_space_tBL H-Set_tBL">
                            <ReactTable
                                data={PricingTable}
                                columns={columns}
                                PaginationComponent={Pagination}
                                noDataText="No Record Found"
                                // onPageSizeChange={this.onPageSizeChange}
                                minRows={2}
                                previousText={<span className="icon icon-arrow-left privious"></span>}
                                nextText={<span className="icon icon-arrow-right next"></span>}
                                showPagination={false}
                                className="-striped -highlight"
                                noDataText="No duplicate applicant found"
                                TbodyComponent={CustomTbodyComponent}
                            />
                        </div>

                    </div>

                </div>


                <div className="row mt-5">

                    <div className="col-lg-12">
                        <MonthChart chartData={chartData} setOption={setChartOptions} setLegendToggle={true} height={500} />
                    </div>
                </div>



            </React.Fragment >
        );
    }

}

const mapStateToProps = state => ({
    showPageTitle: state.FinanceReducer.activePage.pageTitle,
    showTypePage: state.FinanceReducer.activePage.pageType
})
const mapDispatchtoProps = (dispach) => {
    return {

    }
}


export default connect(mapStateToProps, mapDispatchtoProps)(Payroll);