import React, { Component } from 'react';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import { checkItsNotLoggedIn, postData, handleChangeChkboxInput, getPermission, checkPin, checkLoginModule, checkPinVerified } from '../../../service/common.js';
import { Link, Redirect } from 'react-router-dom';
import PinModal from '../../admin/PinModal';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import { connect } from 'react-redux';
import {setActiveSelectPage,setCaseDetails} from 'components/admin/fms/actions/FmsAction';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ParticiapntPageIconTitle } from '../../../menujson/pagetitle_json.js';
import Pagination from "service/Pagination.js";

const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {
        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData('fms/fmsDashboard/get_fms_cases', Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                all_count: result.all_count,
                total_duration: result.total_duration
            };
            resolve(res);
        });
    });
};

class FmsDashboard extends Component {
    constructor(props) {
        checkItsNotLoggedIn(ROUTER_PATH);
        super(props);
        var uri_props = this.props.props.match.params.type;
        if (uri_props == 'incidents') {
//            checkPinVerified('incident');
        } else {
//            checkPinVerified('fms');
        }

        this.state = {
            activeTab: '',
            srch_box: '',
            caseListing: [],
            current_uri: this.props.props.match.params.type,
            //default_status:(uri_props && uri_props == 'new')?0:(uri_props == 'onGoing')?0:(uri_props == 'completed')?1:2,
            caseType: (uri_props && uri_props == 'new') ? '0' : '1',
            permissions: (getPermission() == undefined) ? [] : JSON.parse(getPermission())
        }
    }

    componentDidMount() {
        this.funSetState(this.props, '1');
        this.props.setSubmenuShow(1);
        this.getPageSelect();
        this.manage_incident();
    }

    manage_incident() {
        var AES = require("crypto-js/aes");
        var SHA256 = require("crypto-js/sha256");
        var CryptoJS = require("crypto-js");

        var f_status = true;
        var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(f_status), 'secret key 123');
        localStorage.setItem("f_status", ciphertext);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.props.match.params.type !== this.props.props.match.params.type) {
            this.funSetState(newProps, '0');
        }

        if(this.props.showPageType !=newProps.showPageType){
            this.props.setCaseDetails({id:''});
            if(this.props.props.match.params.page=='case_ongoing' || this.props.props.match.params.page=='incident_ongoing'){
                this.changeCaseStatus('onGoing');
            }else{
                this.changeCaseStatus('completed');
            }
        }    
    }

    checkLoginModule = () => {
        var CryptoJS = require("crypto-js");
        var ciphertext = localStorage.getItem('f_status');
        var bytes = CryptoJS.AES.decrypt(ciphertext.toString(), 'secret key 123');
        var plaintext = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        if (!plaintext) {
            this.setState({ pinModalOpen: true, moduleHed: 'FMS Module', color: 'Red_fms', pinType: 1 })
        }
        else {
            this.setState({ redirectDone: true, })
        }
    }

    funSetState(newProps, type) {
        var uri_props = newProps.props.match.params.type;
        //if(uri_props!='new' && uri_props!='onGoing' && uri_props!='completed' &&  uri_props!='incidents')
        if (uri_props != 'new' && uri_props != 'incidents') {
            window.location = '/admin/fms/dashboard/new';
        }

        //var filter = (uri_props && uri_props == 'new')?-1:(uri_props == 'onGoing')?0:(uri_props == 'completed')?1:2;
        //var heading = (uri_props && uri_props == 'new')?'Most Recent Feedback':(uri_props == 'onGoing')?'Ongoing Feedback':(uri_props == 'completed')?'Completed Feedback':'Incidents';
        var filter = (uri_props && uri_props == 'new') ? '0' : '1';
        //var heading = (uri_props && uri_props == 'new')?'Most Recent Feedback':'Incidents';

        if (type == '0') {
            //this.setState({current_uri: uri_props,filtered:{caseType:filter},heading:heading});
            this.setState({ current_uri: uri_props, filtered: { status: '0,1', caseType: filter, srch_box: this.state.srch_box }, activeTab: '' }, () => { });
        }
        else {
            // this.setState({default_filter: filter,heading:heading});
            this.setState({ default_filter: filter, status: '0,1', caseType: this.state.caseType, activeTab: '', srch_box: this.state.srch_box }, () => { });
        }
    }

    fetchData = (state, instance) => {
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                caseListing: res.rows,
                all_count: res.all_count,
                pages: res.pages,
                loading: false,
                total_duration: res.total_duration
            });
        })
    }

    searchData = (e) => {
        var uri_props = this.state.current_uri;
        //var FeedbackType = (uri_props && uri_props == 'new')?0:(uri_props == 'onGoing')?1:(uri_props == 'completed')?2:4;
        var FeedbackType = (uri_props && uri_props == 'new') ? '0' : '1';
        e.preventDefault();
        //jQuery("#srch_case").validate({ });
        // if(jQuery("#srch_case").valid())
        {
            //if(this.state.srch_box!='')
            {
                var caseType = (uri_props && uri_props == 'new') ? '0' : '1';
                var requestData = { srch_box: this.state.srch_box, status: FeedbackType, caseType: caseType };
                this.setState({ filtered: requestData });
            }
        }
    }

    changeCaseStatus = (activeTab) => {
        var tempStatus = (activeTab == 'onGoing') ? '0' : '1';
        var uri_props = this.props.props.match.params.type;
        var caseType = (uri_props && uri_props == 'new') ? '0' : '1';
        var requestData = { status: tempStatus, caseType: caseType, srch_box: this.state.srch_box };
        this.setState({ filtered: requestData, activeTab: activeTab });
    }

    closeModal = () => {
        this.setState({ pinModalOpen: false })
    }

    fmsHtml = () => {
        if (!checkPin('incident'))
            return this.state.permissions.incident_fms ? <li><a onClick={() => checkLoginModule(this, 'incident', 'incidents')}>Incidents</a></li> : ''
        else
            return this.state.permissions.incident_fms ? <li><Link to="/admin/fms/dashboard/incidents" className={(this.state.current_uri == 'incidents') ? 'active' : ''}> Incidents</Link></li> : ''
    }

    getPageSelect(){
        this.props.setActivePage(this.props.props.match.params.page);
      }

    componentDidUpdate(){
        if(this.props.props.match.params.page!=undefined && this.props.showTypePage!=this.props.props.match.params.page){
              this.getPageSelect();
              
        }
    }

    render() {
        if (this.state.redirectDone) {

            //return <Redirect from="admin/fms/dashboard/:new" to="/admin/fms/dashboard/incidents" />
            return <Redirect to={{ pathname: 'incidents' }} />

        }

        const columns = [
            { Header: 'HCM-ID', accessor: 'id', filterable: false, },
            { Header: 'Timer', accessor: 'timelapse', filterable: false, sortable: false },
            { Header: 'Category', accessor: 'category', filterable: false, sortable: false },
            { Header: 'Date of Event ', accessor: 'event_date', filterable: false, },
            { Header: 'Initiated By ', accessor: 'initiated_by', sortable: false },
            { Header: 'Against To ', accessor: 'against_for', sortable: false },
            {
                Cell: (props) => <span className="action_ix__"><Link title={ParticiapntPageIconTitle.par_view_icon} to={{ pathname: '/admin/fms/case/' + props.original.id+'/case_details', state: this.props.props.location.pathname }}><i className="icon icon-views"></i></Link></span>, Header: <div className="">Action</div>,
                Header: <TotalShowOnTable color="no" countData={this.state.all_count} />,
                style: {
                    "textAlign": "right",
                }, headerStyle: { border: "0px solid #fff" }, sortable: false
            },
        ]

        return (
            <React.Fragment>
                  <div className="row  _Common_back_a">
                            <div className="col-lg-12  col-md-12">
                                <Link className="d-inline-flex" to={ROUTER_PATH+'admin/dashboard'}>
                                    <span className="icon icon-back-arrow back_arrow"></span>
                                </Link>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12  col-sm-12">
                                <div className="bor_T"></div>
                            </div>
                        </div>

                        <div className="row _Common_He_a">
                            <div className="col-lg-9  col-md-9 col-sm-8">
                                <h1 className="color">FMS Cases</h1>
                            </div>
                            <div className="col-lg-3 col-md-3 col-sm-4"> <Link to={ROUTER_PATH+'admin/fms/create_case'} className="Plus_button"><i className="icon icon-add-icons create_add_but"></i><span>Initiate FMS Case</span></Link> </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-sm-12">
                                <div className="bor_T"></div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-6  col-md-6 ">
                                <div className="search_bar">
                                <form id="srch_feedback" autoComplete="off" onSubmit={this.searchData} method="post">
                                    <div className="input_search P_25_TB">
                                        <input type="text" className="form-control" placeholder="Search" name="srch_box" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                        <button type="submit"><span className="icon icon-search"></span></button>
                                    </div>
                                </form>
                                </div>
                            </div>


                            <div className="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3">
                                <div className="box P_30_TB font_big">
                                    <Select name="view_by_status" required={true} simpleValue={true} searchable={false} Clearable={false} placeholder="Filter by Type" />
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <article className="col-lg-12 col-sm-12">


                                <div className="row">
                                    <div className="col-sm-12 P_15_b">
                                        <div className="bor_T"></div>
                                    </div>
                                    <div className="col-sm-12 listing_table hand_effect PL_site">
                                        <ReactTable
                                         PaginationComponent={Pagination}
                                            columns={columns}
                                            manual
                                            data={this.state.caseListing}
                                            pages={this.state.pages}
                                            loading={this.state.loading}
                                            onFetchData={this.fetchData}
                                            filtered={this.state.filtered}
                                            defaultFiltered={{ caseType: this.state.caseType, status: '0,1' }}
                                            defaultPageSize={10}
                                            className="-striped -highlight"
                                            noDataText="No Record Found"
                                            minRows={2}
                                            previousText={<span className="icon icon-arrow-1-left privious"></span>}
                                            nextText={<span className="icon icon-arrow-1-right next"></span>}
                                            showPagination={this.state.caseListing.length > PAGINATION_SHOW ? true : false}
                                        />

                                    </div>
                                </div>

                            </article>
                        </div>
                    

                <PinModal
                    color={this.state.color}
                    pinType={this.state.pinType}
                    moduleHed={this.state.moduleHed}
                    modal_show={this.state.pinModalOpen}
                    returnUrl={this.state.returnUrl}
                    closeModal={this.closeModal}
                />

            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
       showPageTitle: state.FmsCaseDetailsData.activePage.pageTitle,
       showPageType: state.FmsCaseDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
        setActivePage:(result) => dispach(setActiveSelectPage(result)),
        setCaseDetails:(result) => dispach(setCaseDetails(result))
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(FmsDashboard);
