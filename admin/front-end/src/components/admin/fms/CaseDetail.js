import React, { Component } from 'react';
import jQuery from "jquery";
import { Redirect, Link } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW } from '../../../config.js';
import { checkItsNotLoggedIn, postData, handleChangeChkboxInput, archiveALL, handleChangeSelectDatepicker, reFreashReactTable, checkPinVerified, getFmscasePrimaryCategory, postImageData } from '../../../service/common.js';
import { statusOptionFms } from '../../../dropdown/Fmsdropdown.js';
import CaseMonitor from './CaseMonitor';
import CaseRespond from './CaseRespond';
import Panel from 'react-bootstrap/lib/Panel';
import moment from 'moment';
import Modal from 'react-bootstrap/lib/Modal';
import { toast } from 'react-toastify';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import { TotalShowOnTable } from '../../../service/TotalShowOnTable';
import { connect } from 'react-redux';
import {setCaseDetails, setActiveSelectPage} from 'components/admin/fms/actions/FmsAction';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ToastUndo } from 'service/ToastUndo.js'
import { ParticiapntPageIconTitle } from '../../../menujson/pagetitle_json.js';


class CaseDetail extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified();
        this.caseId = this.props.props.match.params.id;
        this.state = {
            loading: false,
            caseReason: [],
            case_detail: [],
            activeMenu: 'case_detail_menu',
            caseId: this.props.props.match.params.id
        };
    }

    componentDidMount() {
        this.get_case_detail();
        this.getPageSelect();
        this.props.setSubmenuShow(1);
    }

    componentWillReceiveProps(newProps) {
        if(this.state.caseId!= newProps.props.match.params.id){
   
            this.setState({ caseId: newProps.props.match.params.id }, () => {
                this.get_case_detail()
            })
        }

        if(this.props.showPageType !=newProps.showPageType){
            let activeMenuData=''
            if(newProps.showPageType=='case_details'){
                activeMenuData='case_detail_menu';
            }else if(newProps.showPageType=='case_respond'){
                activeMenuData='respond_menu';

            }else if(newProps.showPageType=='case_monitor'){
                activeMenuData='monitor_menu';
            }
             
            if(activeMenuData!=''){
                this.setState({activeMenu:activeMenuData});
            }

        }
    }

    get_case_detail() {
        var requestData = { case_id: this.state.caseId };
        this.setState({ loading: true });
        postData('fms/FmsDashboard/get_case_detail', requestData).then((result) => {
            if (result.status) {
                this.props.setCaseDetails(result.data);
                this.setState({ case_detail: result.data }, () => { });
            } else {
                window.location = '/admin/fms/dashboard/new';
                this.setState({ error: result.error });
            }
            this.setState({ loading: false });
        });
    }


    getPageSelect(){
        this.props.setActivePage(this.props.props.match.params.page);
      }

    componentDidUpdate(){
        if(this.props.props.match.params.page!=undefined && this.props.showTypePage!=this.props.props.match.params.page){
              this.getPageSelect();
        }
    }

    render() {
        return (
            <div>
                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-md-12">
                                <a onClick={this.props.props.history.goBack}>
                                    <span className="icon icon-back-arrow back_arrow"></span>
                                </a>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12 col-sm-12">
                                <div className="bor_T"></div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12 col-sm-12 P_15_TB text-left">
                                <h1 className="color">Case ID<span> - {this.state.case_detail.id}</span></h1>
                               
                            </div>
                            <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                        </div>
                       
                        <div className="row">
                        <div className="py-5">
                        <ul className="user_info P_15_TB">
                                    {this.state.case_detail.against?<li>Against : <span>{this.state.case_detail.against} </span></li>:''}
                                    <li>HCM-ID: <span> {this.state.case_detail.id} </span></li>
                                    {this.state.case_detail.initiated_by?<li>Initiator Name: <span> {this.state.case_detail.initiated_by}</span></li>:''}
                                    <li>Created Date: <span>{moment(this.state.case_detail.created).format('D/M/Y')} </span></li>
                                </ul>
                                </div>
                        <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>

                        </div>

                        {
                            (this.state.activeMenu && this.state.activeMenu == 'monitor_menu') ?
                                <CaseMonitor activeMenu={this.state.activeMonitorMenu} caseId={this.state.caseId} />
                                :
                                (this.state.activeMenu && this.state.activeMenu == 'case_detail_menu') ?
                                    <div>
                                        <div className="row d-flex justify-content-center">
                                            <ul className="nav nav-tabs Category_tap col-lg-8 col-sm-10 P_20_TB" role="tablist">
                                                <li role="presentation" className="col-lg-4 col-sm-4 active">
                                                    <a href="#CategoryReason" aria-controls="CategoryReason" role="tab" data-toggle="tab">Category/Reason</a>
                                                </li>
                                                <li role="presentation" className="col-lg-4 col-sm-4">
                                                    <a href="#Docs_Notes" aria-controls="Docs_Notes" role="tab" data-toggle="tab">Docs/Notes</a>
                                                </li>
                                                <li role="presentation" className="col-lg-4 col-sm-4">
                                                    <a href="#Link_Case" aria-controls="Link_Case" role="tab" data-toggle="tab">Link Case</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                                        </div>

                                        <div className="tab-content">
                                            <CaseCategory caseId={this.state.caseId} casetype={this.state.case_detail.fms_type} caseStatus={this.state.case_detail.caseStatus} caseCategory={this.state.case_detail.categoryId} Active={'category'} />
                                            <CaseDocsNotes caseId={this.state.caseId} Active={'docs'} />
                                            <CaseLink caseId={this.state.caseId} Active={'link'} />
                                        </div>
                                    </div>
                                    :
                                    <CaseRespond activeMenu={this.state.activeMonitorMenu} caseId={this.state.caseId} />
                        }
                    </div>
              
        );
    }
}
const mapStateToProps = state => ({
    //getSidebarMenuShow: state.sidebarData
    showPageType: state.FmsCaseDetailsData.activePage.pageType,
})

const mapDispatchtoProps = (dispach) => {
    return {
        setCaseDetails: (result) => dispach(setCaseDetails(result)),
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
        setActivePage:(result) => dispach(setActiveSelectPage(result))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CaseDetail);

//case categroy
class CaseCategory extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            caseReason: []
        }
    }

    componentDidMount() {
        this.setState({
            caseId: this.props.caseId,
            caseStatus: this.props.caseStatus,
        }, () => { this.getReason(this.state.caseId) });

        getFmscasePrimaryCategory().then((result) => {
            this.setState({ caseDetailPrimaryCategory: result }, () => { });
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.caseStatus) { this.setState({ caseStatus: nextProps.caseStatus }) }
        if (nextProps.casetype) { this.setState({ casetype: nextProps.casetype }) }
        if (nextProps.caseCategory) { this.setState({ CaseCategory: nextProps.caseCategory.categoryId }) }
    }

    getReason = (caseId) => {
        var requestData = { caseId: caseId, actionTbl: 'fms_case_reason' };
        postData('fms/fmsDashboard/get_result_array', requestData).then((result) => {
            if (result.status) {
                this.setState({ caseReason: result.data }, () => { });
            }
        });
    }

    updateCaseStatus = (selectedOption, fieldname, update_type) => {
        var caseId = this.state.caseId;
        var state = {};
        state[fieldname] = selectedOption;
        var requestData = { status: selectedOption, caseId: caseId, update_type: update_type };
        postData('fms/FmsDashboard/update_case_status', requestData).then((result) => {
            this.setState({ loading: false });
            if (result.status) {
                this.setState(state);
            } else {
                this.setState({ error: result.error });
            }
        });

    }

    closeCategoryPopUp = () => {
        this.setState({ categoryModalShow: false });
    }

    moveToEscalate(caseStatus) {
        if (caseStatus != 4) { 
            archiveALL({id: this.state.caseId}, 'Are you sure you want to move this Case to Incident.', 'fms/fmsDashboard/move_case_to_incident').then((result) => {
                if (result.status) {
                    this.setState({ casetype: result.casetype }, () => { });
                }
            })
        }
    }

    render() {
        return (

            <div role="tabpanel" className="tab-pane active" id="CategoryReason">
                <div className="row">
                    <div className="col-lg-6  col-sm-6"><h2 className="P_15_TB">Category:</h2></div>
                    <div className="col-lg-6 col-sm-6"><h2 className="P_15_TB">Case Status:</h2></div>
                </div>
                <div className="row">
                    <div className="col-lg-12  col-sm-12"><div className="bor_T"></div></div>
                </div>
                <div className="row d-flex">
                    <div className="col-lg-3 col-sm-3 P_25_TB">
                        <span className="required">
                            <Select name="CaseCategory" required={true} simpleValue={true} searchable={false} clearable={false}
                                placeholder="Primary Category" options={this.state.caseDetailPrimaryCategory} value={this.state.CaseCategory} onChange={(e) => this.updateCaseStatus(e, 'CaseCategory', 'category')} />
                        </span>
                    </div>
                    <div className="col-lg-3 col-sm-1 P_25_TB bcolor br-1"></div>
                    <div className="col-lg-3 col-sm-3 P_25_TB">
                        <span className="required">
                            <Select name="caseStatus" required={true} simpleValue={true} searchable={false} clearable={false}
                                placeholder="Please select" options={statusOptionFms()} value={this.state.caseStatus} onChange={(e) => this.updateCaseStatus(e, 'caseStatus', 'status')} />
                        </span>
                    </div>

                    <div className="col-lg-3 col-sm-3 P_25_TB">
                        {
                            (this.state.casetype && this.state.casetype == 0) ?
                                <a className={'but'} onClick={() => this.moveToEscalate(this.state.caseStatus)}>Escalate to Incident</a>
                                :
                                <a className='but remove_hand'>Incident</a>
                        }
                    </div>

                </div>
                <div className="row">
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    <div className="col-lg-12 col-sm-12"><h2 className="P_15_TB">Reason:</h2></div>
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                </div>
                <div className="row">
                    <div className="col-lg-9 col-sm-12 P_25_T">
                        <div className="panel-group accordion_me accordion_member font_w_1 fms_accor" id="accordion" role="tablist" aria-multiselectable="true">

                            {(this.state.caseReason.length > 0) ? <span>
                                {this.state.caseReason.map((value, index) => (
                                    <Panel key={index + 1} eventKey={index + 1}>
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                <span>
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <ul>
                                                        <li className="li_fms_1"><span className="color">{moment(value.created).format('D/M/Y')}:</span> {value.title}</li>
                                                        <li className="li_fms_2"><span className="color">By:</span> {value.created_by}</li>
                                                    </ul>
                                                </span>
                                            </Panel.Title>
                                        </Panel.Heading>

                                        <Panel.Body collapsible className="px-1 py-0">
                                            <div className="panel-body px-3 py-2">
                                                <div className="px-3 py-3">{value.description}</div>
                                            </div>
                                            <p className="text-right P_15_LR">
                                                <a className="d-inline-flex pr-3"><i title={ParticiapntPageIconTitle.par_update_icon} className="icon icon-update update_button" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Edit', updateData: value })}></i></a>
                                                <a className="d-inline-flex"><i title={ParticiapntPageIconTitle.par_history_icon} className="icon icon-pending-icons history_button"></i></a>
                                            </p>
                                        </Panel.Body>
                                    </Panel>
                                ))}  </span> : 'Currently no reason found for this case.'
                            }
                            <div className="P_15_T">
                                <div className="pull-left">
                                    <a className="button_plus__">
                                        <span className="icon icon-add-icons Add-2-1" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Add', updateData: '' })}></span>
                                    </a>
                                </div>
                            </div>
                            <AddCategoryNotesPopup isModalShow={this.state.categoryModalShow} getDataListFun={this.getReason} mode={this.state.categoryMode} closeCategoryPopUp={this.closeCategoryPopUp} caseId={this.props.caseId} actionTbl="case_reason" dataAry={this.state.updateData} popName={'Reason'} />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

//case docs/notes
class CaseDocsNotes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            caseNotes: [],
            caseDocs: [],
            selectAll: false,
        }
    }

    componentDidMount() {
        this.setState({
            caseId: this.props.caseId,
        }, () => {
            this.getCaseNotes(this.state.caseId)
            this.getCaseDocs(this.state.caseId)
        });
    }

    getCaseNotes = (caseId) => {
        var requestData = { caseId: caseId, actionTbl: 'fms_case_notes' };
        postData('fms/fmsDashboard/get_result_array', requestData).then((result) => {
            if (result.status) {
                this.setState({ caseNotes: result.data }, () => { });
            }
        });
    }

    getCaseDocs = (caseId) => {
        var requestData = { caseId: caseId, actionTbl: 'fms_case_docs' };
        postData('fms/fmsDashboard/get_result_array', requestData).then((result) => {
            if (result.status) {
                this.setState({ caseDocs: result.data }, () => { });
            }
        });
    }

    closeCategoryPopUp = () => {
        this.setState({ categoryModalShow: false });
    }

    closeDocsPopUp = () => {
        this.setState({ openDocsModal: false });
    }

    clickToDownloadFile = (key) => {
        var activeDownload = this.state.caseDocs;
        if (activeDownload[key]['is_active'] == true) {
            activeDownload[key]['is_active'] = false;
            this.setState({ selectAll: false });
        }
        else {
            activeDownload[key]['is_active'] = true;
            this.setState({ caseDocs: activeDownload });
        }
    }

    selectAllFile = (e) => {
        this.setState({ selectAll: e.target.checked });
        var activeDownload = this.state.caseDocs;
        activeDownload.map((value, index) => {
            if (e.target.checked)
                activeDownload[index]['is_active'] = true;
            else
                activeDownload[index]['is_active'] = false;
        })
        this.setState({ caseDocs: activeDownload });
    }

    downloadSelectedFile = () => {
        toast.dismiss();
        var caseId = this.state.caseId;
        var requestData = { caseId: caseId, downloadData: this.state.caseDocs };

        postData('fms/FmsDashboard/download_selected_file', requestData).then((result) => {
            if (result.status) {
                window.location.href = BASE_URL + "archieve/" + result.zip_name;

                var removeDownload = this.state.caseDocs;
                removeDownload.map((value, idx) => {
                    removeDownload[idx]['is_active'] = false;
                })
                this.setState({ caseDocs: removeDownload });
            } else {
                toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

    render() {
        return (

            <div role="tabpanel" className="tab-pane" id="Docs_Notes">
                <div className="row">
                    <div className="col-lg-12 col-sm-12"><h2 className="P_15_TB">Case Docs:</h2></div>
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                </div>
                <div className="row">
                    <div className="col-lg-9 col-sm-9">
                        <ul className="file_down P_15_TB">

                            {(this.state.caseDocs.length > 0) ? <span className="fms_file">
                                {this.state.caseDocs.map((value, index) => (

                                    <li key={index + 1} onClick={() => this.clickToDownloadFile(index)} className={(value.is_active) ? 'active' : ''}>
                                        <div className="path_file"><b>{value.title}</b></div>
                                        <span className="icon icon-file-icons d-block"></span>
                                        <div className="path_file">{value.filename}</div>
                                    </li>
                                ))}  </span> : 'Currently no Docs found for this case.'
                            }

                        </ul>
                    </div>
                    <div className="col-lg-3 col-sm-3 P_15_TB">
                        <p className="select_all"><span>Select All</span>
                            <input type='checkbox' className="input_c" name='thing' checked={this.state.selectAll} id="thing" onChange={(e) => this.selectAllFile(e)} /><label htmlFor="thing"></label> </p>
                        <p><a className="but" onClick={() => this.downloadSelectedFile()}>Download Selected</a></p>
                        <label className="btn btn-default btn-sm center-block btn-file">
                            <i className="but" aria-hidden="true" onClick={() => this.setState({ openDocsModal: true })}>Upload Doc(s)</i>
                        </label>

                        {(this.state.openDocsModal) ?
                            <UploadCaseDocsModal isDocsModalShow={this.state.openDocsModal} closeUploadPopup={this.closeDocsPopUp} caseId={this.state.caseId} getCaseDocs={this.getCaseDocs} />
                            : ''}
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                    <div className="col-lg-12 col-sm-12"><h2 className="P_15_TB">Case Notes:</h2></div>
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div><div className="col-lg-1"></div>
                </div>
                <div className="row">
                    <div className="col-lg-9 col-sm-9 P_25_T">

                        <div className="panel-group accordion_me accordion_member font_w_1 fms_accor" id="accordion" role="tablist" aria-multiselectable="true">

                            {(this.state.caseNotes.length > 0) ? <span>
                                {this.state.caseNotes.map((value, index) => (
                                    <Panel key={index + 1} eventKey={index + 1}>
                                        <Panel.Heading>
                                            <Panel.Title toggle>
                                                <span>
                                                    <i className="more-less glyphicon glyphicon-plus"></i>
                                                    <ul>
                                                        <li className="li_fms_1"><span className="color">{moment(value.created).format('D/M/Y')}: </span>{value.title}</li>
                                                        <li className="li_fms_2"><span className="color">By:</span> {value.created_by}</li>
                                                    </ul>
                                                </span>
                                            </Panel.Title>
                                        </Panel.Heading>

                                        <Panel.Body collapsible className="px-1 py-0">
                                            <div className="panel-body px-3 py-2">
                                               <div className="px-3 py-3">{value.description}</div> 
                                            </div>
                                            <p className="text-right P_15_LR">
                                                <a  title={ParticiapntPageIconTitle.par_update_icon} className="d-inline-flex mr-3"><i className="icon icon-update update_button" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Edit', updateData: value })}></i></a>
                                                <a  title={ParticiapntPageIconTitle.par_history_icon}  className="d-inline-flex"><i className="icon icon-pending-icons history_button"></i></a>
                                            </p>
                                        </Panel.Body>
                                    </Panel>
                                ))}  </span> : 'Currently no reason found for this case.'
                            }
                            <div className="P_15_T">
                                <div className="pull-left">
                                    <a className="button_plus__">
                                        <span className="icon icon-add-icons Add-2-1" onClick={() => this.setState({ categoryModalShow: true, categoryMode: 'Add', updateData: '' })}></span>
                                    </a>
                                </div>
                            </div>
                            <AddCategoryNotesPopup isModalShow={this.state.categoryModalShow} getDataListFun={this.getCaseNotes} mode={this.state.categoryMode} closeCategoryPopUp={this.closeCategoryPopUp} caseId={this.props.caseId} actionTbl="case_notes" dataAry={this.state.updateData} popName={'Case Notes'} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

//case Link 
const requestDataLinkCase = (pageSize, page, sorted, filtered, listMode) => {
    return new Promise((resolve, reject) => {
        if (listMode == 'linkCase')
            var url = 'fms/fmsDashboard/get_link_fms_cases';
        else
            var url = 'fms/fmsDashboard/get_srch_fms_cases';

        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });
        postData(url, Request).then((result) => {
            let filteredData = result.data;

            const res = {
                rows: filteredData,
                pages: (result.count),
                total_duration: result.total_duration,
                all_count: result.all_count,
            };
            setTimeout(() => resolve(res), 10);
        });

    });
};

class CaseLink extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            linkCaseListing: [],
            srchCaseListing: [],
            showSrchTbl: false,
        }
        this.reactTable = React.createRef();
    }

    componentDidMount() {
        this.setState({
            caseId: this.props.caseId,
        }, () => {

        });
    }

    fetchData = (state, instance) => {
        this.setState({ loading: true });
        requestDataLinkCase(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            'linkCase'
        ).then(res => {
            this.setState({
                linkCaseListing: res.rows,
                pages: res.pages,
                all_count: res.all_count,
                loading: false,
            });
        })
    }

    srchFetchData = (state, instance) => {
        this.setState({ loading: true });
        requestDataLinkCase(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered,
            'srchCase'
        ).then(res => {
            this.setState({
                srchCaseListing: res.rows,
                pages: res.pages,
                all_count_srch: res.all_count,
                loading: false,
            });
        })
    }

    linkUnlink = (action, caseId) => {
        var requestData = { unLinkcaseId: caseId, action: action, mainCaseId: this.state.caseId };

        archiveALL(requestData, 'Are you sure you want to ' + action + ' this Case?', 'fms/fmsDashboard/link_unlink_case').then((result) => {
            if (result.status) {
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                // toast.success(result.msg, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                reFreashReactTable(this, 'fetchData');
                reFreashReactTable(this, 'srchFetchData');
            }
        })
    }

    searchData = (e) => {
        e.preventDefault();
        jQuery("#srch_case").validate({});

        if (jQuery("#srch_case").valid()) {
            if (this.state.srch_box != '') {
                var requestData = { srch_box: this.state.srch_box, from_date: this.state.from_date, to_date: this.state.to_date, caseId: this.props.caseId };
                this.setState({ showSrchTbl: true, filteredData: requestData });
            }
        }
    }

    render() {

        const linkedCaseColumns = [
            { Header: 'ID', accessor: 'linkCaseId', filterable: false, },
            { Header: 'Timer', accessor: 'timelapse', filterable: false, sortable: false },
            { Header: 'Category', accessor: 'case_category', filterable: false, sortable: false },
            { Header: 'Date of Event ', accessor: 'event_date', filterable: false, },
            { Header: 'Initiated By ', accessor: 'initiated_by', sortable: false },
            { Header: 'Against To ', accessor: 'against_for', sortable: false },
            {
                Cell: (props) => <span className="Linkcase_icon"><a className="icon-un_attach_btn" onClick={() => this.linkUnlink('Unlink', props.original.linkCaseId)}><i className="icon icon-un-attach-icons"></i></a><Link to={ROUTER_PATH+'admin/fms/case/' + props.original.linkCaseId} ><i className="icon icon-notes CH_i1"></i></Link></span>,
                Header: <div className="">Action</div>,
                Footer: <TotalShowOnTable countData={this.state.all_count} />,
                style: {
                    "textAlign": "right",
                }, headerStyle: { border: "0px solid #fff" }, sortable: false
            },


        ]

        const srchCaseColumns = [
            { Header: 'Case ID', accessor: 'linkCaseId', filterable: false, },
            { Header: 'Timer', accessor: 'timelapse', filterable: false, sortable: false },
            { Header: 'Category', accessor: 'case_category', filterable: false, sortable: false },
            { Header: 'Date of Event ', accessor: 'event_date', filterable: false, },
            { Header: 'Initiated By ', accessor: 'initiated_by', sortable: false },
            { Header: 'Against To ', accessor: 'against_for_srch', sortable: false },
            {
                Cell: (props) => <span><a className="icon-attach_btn" onClick={() => this.linkUnlink('Link', props.original.linkCaseId)}><i className="icon icon-attach"></i></a></span>,
                
                Header: <TotalShowOnTable countData={this.state.all_count_srch} />,
                style: {
                    "textAlign": "right",
                }, headerStyle: { border: "0px solid #fff" }, sortable: false
            },
        ]

        return (
            <div role="tabpanel" className="tab-pane" id="Link_Case">

                <div className="col-lg-12">
                    <h2 className="P_15_T">Linked to:</h2>
                    <div className="schedule_listings p_left_table">
                        <ReactTable
                            columns={linkedCaseColumns}
                            manual
                            data={this.state.linkCaseListing}
                            pages={this.state.pages}
                            loading={this.state.loadingLinkcase}
                            onFetchData={this.fetchData}
                            filtered={this.state.filtered}
                            defaultFiltered={{ caseId: this.props.caseId }}
                            defaultPageSize={10}
                            className="-striped -highlight"
                            noDataText="No Record Found"
                            minRows={2}
                            previousText={<span className="icon icon-arrow-1-left privious"></span>}
                            nextText={<span className="icon icon-arrow-1-right next"></span>}
                            ref={this.reactTable}
                            showPagination={this.state.linkCaseListing.length > PAGINATION_SHOW ? true : false}
                        />
                    </div>
                </div>

                <div className="col-lg-12 mt-5">

                    <form id="srch_case" autoComplete="off" onSubmit={this.searchData} method="post">
                        <h2 className="P_15_TB by-1">Link Lookup:</h2>
                        <div className="d-flex flex-wrap">
                        <div className="col-sm-6 P_15_TB align-self-end">
                            <label></label>

                            <div className="event_loca search_i_right">
                                <input name="srch_box" type="text" className="form-control" onChange={(e) => handleChangeChkboxInput(this, e)} />
                                <button type="submit">
                                    <span className="icon icon-search color"></span>
                                </button>
                            </div>
                        </div>
                        <div className="col-sm-2 P_15_TB w_100_date align-self-end">
                            <label>From:</label>
                            <DatePicker autoComplete={'off'} className="text-center" selected={this.state.from_date ? moment(this.state.from_date, 'DD-MM-YYYY') : null} name="from_date" onChange={(e) => handleChangeSelectDatepicker(this, e, 'from_date')} minDate={moment()} dateFormat="DD-MM-YYYY" autoComplete="off" />
                        </div>
                        <div className="col-sm-2 P_15_TB w_100_date align-self-end">
                            <label className="w-100">To:</label>
                            <DatePicker autoComplete={'off'} className="text-center" selected={this.state.to_date ? moment(this.state.to_date, 'DD-MM-YYYY') : null} name="to_date" onChange={(e) => handleChangeSelectDatepicker(this, e, 'to_date')} minDate={moment()} dateFormat="DD-MM-YYYY" autoComplete="off" />
                        </div>
                        </div>
                    </form>
                    <div className="clearfix"></div>

                    {(this.state.showSrchTbl) ?
                        <div className="schedule_listings p_left_table">
                            <ReactTable
                                columns={srchCaseColumns}
                                manual
                                data={this.state.srchCaseListing}
                                pages={this.state.pages}
                                loading={this.state.loadingSrchCase}
                                onFetchData={this.srchFetchData}
                                filtered={this.state.filteredData}
                                //defaultFiltered ={{caseId: this.props.caseId}}
                                defaultPageSize={10}
                                className="-striped -highlight"
                                noDataText="No Record Found"
                                minRows={2}
                                previousText={<span className="icon icon-arrow-1-left privious"></span>}
                                nextText={<span className="icon icon-arrow-1-right next"></span>}
                                showPagination={this.state.srchCaseListing.length > PAGINATION_SHOW ? true : false}
                            /></div> : ''}

                </div>
            </div>
        )
    }
}

//category Add/Update
class AddCategoryNotesPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            caseId: this.props.caseId,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dataAry) { this.setState(nextProps.dataAry) }
        if (nextProps.actionTbl) { this.setState({ actionTbl: nextProps.actionTbl }) }
        if (nextProps.popName) { this.setState({ popName: nextProps.popName }) }
    }

    handleSaveBox = (e) => {
        e.preventDefault();
        var isSubmit = 1;
        var validator = jQuery("#categoryBox").validate({ ignore: [] });
        //if(jQuery("#categoryBox").valid() && isSubmit)
        if (!this.state.loading && jQuery("#categoryBox").valid()) {
            this.setState({ loading: true, mode: this.props.mode }, () => {
                postData('fms/FmsDashboard/update_reason_notes', this.state).then((result) => {
                    if (result.status) {
                        this.setState({ loading: false });
                        toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                        // toast.success(result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.closeClear();
                        //if(result.actionTbl == 'tbl_fms_case_notes')
                        this.props.getDataListFun(this.props.caseId);
                        //else
                        //this.props.getReason(this.props.caseId);

                    } else {
                        toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        // toast.error(result.error, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                });
            });
        }
        else {
            validator.focusInvalid();
        }
    }

    closeClear = () => {
        this.setState({ title: '', description: '', mode: '' });
        this.props.closeCategoryPopUp();
    }

    render() {
        return (
            <div>
                <Modal
                    className="modal fade Modal_A Modal_B"
                    show={this.props.isModalShow}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Body>
                        <form id="categoryBox" autoComplete="off" method="post">

                            <div className="text text-left by-1 Popup_h_er_1">
                            <span>{this.props.mode && this.props.mode == 'Edit' ? 'Update ' + this.props.popName : 'Add New ' + this.props.popName}</span>
                                <a type="button" className="close_i"><i onClick={() => this.closeClear()} className="icon icon-cross-icons"></i></a>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-sm-5">
                                    <label>Please Type a Title Here:</label>
                                    <span className="required">
                                        <input type="text" name="title" onChange={(e) => handleChangeChkboxInput(this, e)} value={this.state['title'] || ''} data-rule-required="true" />
                                    </span>
                                </div>
                                <div className="col-sm-5">
                                </div>
                            </div>

                            <div className="row P_15_T">
                                <div className="col-sm-12">
                                    <label>Short description:</label>
                                    <span className="required">
                                        <textarea className="col-sm-12 min-h-120 textarea-max-size" onChange={(e) => handleChangeChkboxInput(this, e)} name="description" value={this.state['description'] || ''} data-rule-required="true" data-rule-maxlength="500" ></textarea>
                                    </span>

                                </div>

                            </div>

                            <div className="row P_15_T">
                                <div className="col-sm-7"></div>
                                <div className="col-sm-5">
                                    <input disabled={this.state.loading} type="submit" className="but" value={'Save'} onClick={(e) => this.handleSaveBox(e)} />
                                </div>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

// upload case docs
class UploadCaseDocsModal extends React.Component {
    constructor(props) {
        super(props);
        this.uploadDocsInitialState = {
            selectedFile: null,
            submit_form: true,
        }
        this.state = this.uploadDocsInitialState;

    }
    fileChangedHandler = (event) => {
        this.setState({ selectedFile: event.target.files[0], filename: event.target.files[0].name })
    }

    uploadHandler = (e) => {
        e.preventDefault();
        jQuery("#upload_case_form").validate({ /* */ });
        if (jQuery("#upload_case_form").valid()) {
            this.setState({ submit_form: false });
            const formData = new FormData()
            formData.append('myFile', this.state.selectedFile, this.state.selectedFile.name)
            formData.append('caseId', this.props.caseId)
            formData.append('docsTitle', this.state.docsTitle)

            postImageData('fms/FmsDashboard/upload_case_docs', formData).then((result) => {
                if (result.status) {
                    this.props.getCaseDocs(this.props.caseId);
                    this.props.closeUploadPopup();
                    this.setState(this.uploadDocsInitialState);
                    toast.success(<ToastUndo message={'Uploaded successfully.'} showType={'s'} />, {
                    // toast.success("uploaded successfully.", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.closeClear();
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ submit_form: true });
            });
        }
    }

    selectChange(selectedOption, fieldname) {
        var state = {};
        state[fieldname] = selectedOption;
        this.setState(state);
    }

    closeClear = () => {
        this.setState({ filename: '' });
        this.props.closeUploadPopup();
    }

    render() {
        return (
            <div>
                <Modal className="modal fade-scale Modal_A  Modal_B" show={this.props.isDocsModalShow} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
                    <form id="upload_case_form" method="post" autoComplete="off">
                        <Modal.Body>
                            <div className="dis_cell">
                                <div className="text text-left by-1 Popup_h_er_1"><span>Upload Case Docs(s):</span>
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.closeClear()}><i className="icon icon-cross-icons"></i></a>
                                </div>


                                <div className="row P_15_T mb-4">

                                    <div className="col-sm-4">
                                        <div className="row P_15_T">
                                            <div className="col-sm-12">
                                                <label>Title</label>
                                                <span className="required">
                                                    <input type="text" placeholder="Please Enter Your Title" onChange={(e) => this.setState({ 'docsTitle': e.target.value })} value={(this.state.docsTitle) ? this.state.docsTitle : ''} data-rule-required="true" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-sm-8 P_15_T">
                                        <label>Please select a file to upload</label>
                                    </div>
                                    <div className="col-sm-4">
                                        <span className="required upload_btn">
                                            <label className="btn btn-default btn-sm center-block btn-file">
                                                <i className="but" aria-hidden="true">Upload New Doc(s)</i>
                                                <input className="p-hidden" type="file" name="special_agreement_file" onChange={this.fileChangedHandler} data-rule-required="true" date-rule-extension="jpg|jpeg|png|xlx|xls|doc|docx|pdf" />
                                            </label>
                                        </span>

                                    </div>
                                    {(this.state.filename) ? <p className="col-sm-8 col-sm-offset-4" style={{ lineHeight: 'normal' }}>File Name: <small>{this.state.filename}</small></p> : ''}
                                </div>

                                <div className="row">
                                    <div className="col-sm-7"></div>
                                    <div className="col-sm-5">
                                        <input type="submit" className="but" value={'Save'} name="content" disabled={(this.state.submit_form) ? false : true} onClick={(e) => this.uploadHandler(e)} />
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                    </form>
                </Modal>
            </div>
        );
    }
}
