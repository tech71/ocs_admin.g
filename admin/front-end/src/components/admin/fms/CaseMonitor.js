import React, { Component } from 'react';
import { ROUTER_PATH, BASE_URL, PAGINATION_SHOW } from '../../../config.js';
import { checkItsNotLoggedIn,postData,checkPinVerified } from '../../../service/common.js';
import moment from 'moment';
import { toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
import 'react-select-plus/dist/react-select-plus.css';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import Pagination from "service/Pagination.js";
import {CSVLink} from 'react-csv';

class CaseMonitor extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified();
        this.caseId = this.props.caseId;
        this.state = {  
              loading: false,  
              caseLog:[],
              case_detail:[],
        };
    }

    componentDidMount() {  
    }

    get_case_detail()
    {    
        var requestData = {case_id: this.props.caseId};
        this.setState({loading: true});
        postData('fms/FmsDashboard/get_case_detail', requestData).then((result) => {
            if (result.status) {
                this.setState({case_detail:result.data},()=>{ });
            } else {
                this.setState({error: result.error});
            }
             this.setState({loading: false});
        });
    }

    render() {
        return (
            <div>
                <div className="row d-flex justify-content-center">
                      <ul className="nav nav-tabs Category_tap col-lg-5 col-sm-6 P_20_TB" role="tablist">

                            <li role="presentation" className='col-lg-6 col-sm-6 active'>
                                  <a  href="#Logs" aria-controls="Logs" role="tab" data-toggle="tab">Logs</a>
                            </li>
                            <li role="presentation" className="col-lg-6 col-sm-6" className={(this.props.activeMenu == 'Analysis')?'col-lg-6 col-sm-6 active':'col-lg-6 col-sm-6'}>
                                  <a  href="#Analysis" aria-controls="Analysis" role="tab" data-toggle="tab">Analysis</a>
                            </li>
                      </ul>
                    </div>
                    <div className="row">
                      <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                </div>

                <div className="tab-content">
                    <CaseLog caseId = {this.caseId}  />
                    <CaseAnalysis caseId = {this.caseId} activeMenu={this.props.activeMenu} />
                </div>
            </div>
        );
    }
}
export default CaseMonitor;

//
const requestData = (pageSize, page, sorted, filtered,caseId) => {
    return new Promise((resolve, reject) => {
        var url = 'fms/fmsDashboard/get_fms_log';
        var Request = JSON.stringify({pageSize: pageSize, page: page, sorted: sorted, filtered: filtered,caseId:caseId});
        postData(url, Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count),
                total_duration: result.total_duration
            };
            resolve(res);
        });

    });
};

//case Log
class CaseLog extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            caseLog:[],
        }
    }

    componentDidMount() { 
        this.setState({
        caseId:this.props.caseId,
        },()=>{ 
            
        });        
    } 

    fetchData = (state, instance) => {
        this.setState({loading: true});
        requestData(
                state.pageSize,
                state.page,
                state.sorted,
                state.filtered,
                this.props.caseId
                ).then(res => {
            this.setState({
                caseLog: res.rows,
                pages: res.pages,
                loading: false,
            });
        })
    }

    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = {on: this.state.on, from_date: this.state.from_date, to_date: this.state.to_date,caseId:this.props.caseId}
            this.setState({filtered: filter});
        });
    }

    render() {  
        const logColumns = [ 
            {Header: 'Title:', accessor: 'title', filterable: false,  },
            {Header: 'Date:', accessor: 'created', filterable: false,sortable:false },
            {Header: 'By:', accessor: 'created_by', filterable: false,sortable:false },
        ]

        const csvHeaders = [
            {label: 'Title', key: 'title'},
            {label: 'Date', key: 'created'},
            {label: 'By', key: 'created_by'},
        ];

            return (
               <div role="tabpanel" className='tab-pane active' id="Logs">
                        <div className="row">
                              <div className="col-lg-4 col-sm-4"><h2 className="P_20_TB">Logs for Case ID - {this.state.caseId}</h2></div>
                              <div className="col-lg-5 P_15_TB col-sm-7">
                                    <ul className="on_f_to_ul">
                                          <li><label>On: </label></li>
                                          <li>
                                               <DatePicker  autoComplete={'off'} isClearable={true} name="on" onChange={(e) => this.searchBox('on',e)} selected={this.state['on'] ? moment(this.state['on'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                          <li><label>From: </label></li>
                                          <li>
                                                <DatePicker autoComplete={'off'}  isClearable={true} name="from_date" onChange={(e) => this.searchBox('from_date',e)} selected={this.state['from_date'] ? moment(this.state['from_date'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                          <li><label>to: </label></li>
                                          <li>
                                                <DatePicker autoComplete={'off'}  isClearable={true} name="to_date" onChange={(e) => this.searchBox('to_date',e)} selected={this.state['to_date'] ? moment(this.state['to_date'], 'DD-MM-YYYY') : null}   className="text-center px-0" dateFormat="DD-MM-YYYY" autoComplete="off" />
                                          </li>
                                    </ul>
                              </div>
                              <div className="col-lg-1"></div>
                        </div>      
                        <div className="row">
                              <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                        </div>
                        <div className="row mt-3">
                              <div className="col-lg-12 col-sm-12 listing_table PL_site">
                                    <ReactTable
                                PaginationComponent={Pagination}
                                    columns={logColumns}
                                    manual 
                                    data={this.state.caseLog}
                                    pages={this.state.pages}
                                    loading={this.state.loading} 
                                    onFetchData={this.fetchData} 
                                    filtered={this.state.filtered}
                                    defaultFiltered ={{caseId: this.props.caseId}}
                                    defaultPageSize={10}
                                    className="-striped -highlight"  
                                    noDataText="No Record Found"
                                    minRows={2}
                                    previousText = {<span className="icon icon-arrow-1-left privious"></span>}
                                    nextText = {<span className="icon icon-arrow-1-right next"></span>}
                                    getTheadThProps={ (state, rowInfo, column) => {
                                                        return {
                                                            style: { display: 'none' }
                                                        }
                                                    }}
                                    showPagination={this.state.caseLog.length > PAGINATION_SHOW ? true : false }                          
                                    />
                              </div>
                        </div>
                        <div className="row">
                              <div className="col-lg-2 col-lg-offset-9 col-sm-3 col-sm-offset-9">
                                  <CSVLink data={this.state.caseLog} headers={csvHeaders} className="but" filename="case_log.csv"
                                    onClick={(event) => {                                   
                                        if(this.state.caseLog.length == 0)
                                        {
                                            toast.warning("No log to export.", {
                                                position: toast.POSITION.TOP_CENTER,
                                                hideProgressBar: true
                                            }); 
                                            return false;  
                                        }
                                    }} >
                                    Export
                                </CSVLink>
                              </div>
                        </div>
                  </div>
            )
        }
}

//case Analysis
class CaseAnalysis extends React.Component {
    constructor(props) {
        super(props);       
            this.state = {

            }
    }
                
    render() {  
        return (
            <div role="tabpanel" className={(this.props.activeMenu == 'Analysis')?'tab-pane active':'tab-pane'} id="Analysis">
                <div className="row">
                    <div className="col-lg-4  col-sm-4"><h2 className="P_20_TB">Analysis for Case ID - {this.props.caseId}</h2></div>
                    <div className="col-lg-5  P_15_TB col-sm-7"></div>
                </div>  
                
                <div className="row">
                    <div className="col-lg-12 col-sm-12"><div className="bor_T"></div></div>
                   <div className="col-sm-12 text-center"> <h3 className="py-3">Coming Soon.</h3></div>
                </div>  
            </div>
        )
    }
}
    
