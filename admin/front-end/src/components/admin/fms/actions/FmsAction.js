import {fmsActiveTitle} from 'menujson/fms_menu_json';
export const setCaseDetails = (detailsData) => ({
        type: 'set_fms_case_details_data',
        detailsData
    });
export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_fms',
        value
}}

    
export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =fmsActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}