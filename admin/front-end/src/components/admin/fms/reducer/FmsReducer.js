const initialState = {
  case_details: {
   id:''
},
activePage:{pageTitle:'',pageType:''}
}


const FmsCaseDetailsData = (state = initialState, action) => {
  switch (action.type) {
    case 'set_fms_case_details_data':
    return { ...state, case_details: action.detailsData };

    case 'set_active_page_fms':
    return {...state, activePage: action.value};

    default:
            return state
        }
    }
export default FmsCaseDetailsData