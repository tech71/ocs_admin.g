import React, { Component } from 'react';

import {  postData,  handleShareholderNameChange,
    handleAddShareholder, handleRemoveShareholder,  archiveALL} from '../../../../service/common.js';
import {colorPickerStyle} from '../../../../dropdown/imailDropdown.js';
import Select from 'react-select-plus';

import 'react-select-plus/dist/react-select-plus.css';
import jQuery from "jquery";
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Modal from 'react-bootstrap/lib/Modal';
import { SketchPicker } from 'react-color';

import { connect } from 'react-redux'
import "react-placeholder/lib/reactPlaceholder.css";
import { ToastUndo } from 'service/ToastUndo.js'

const getOptionsGroupAdmin = (e, previousSelected, existingAdmin) =>  {
     if (!e) {
        return Promise.resolve({ options: [] });
    }
    return postData('imail/Internal_imail/get_admin_name',{search: e, previous_selected: previousSelected, exist: existingAdmin})
    .then((response) => {
      return { options: response };
    });
}


class CreateUpdateTeam extends Component {
    constructor(props) {
        super(props);
        this.HeImg = '/assets/images/admin/chats_2.png'
        this.SheImg = '/assets/images/admin/chats.png'
        
        this.state = {
            displayColorPicker: false,
            team_color: '#F11313',
            team_member : [{member : ''}],
            existingAdmin: []
        }
    }
    
    componentWillReceiveProps(newProps) {
        if (newProps.mode == 'update') {
            this.setState(newProps.updateData)
            this.setState({mode: newProps.mode})
            this.getPreviousMember(newProps.updateData.id)
        }else{
            this.setState({team_name: '', team_color: '#F11313', id:'', mode: newProps.mode})
        }
        this.setState({team_member : [{member : ''}]})
    }
    
    getPreviousMember = (teamId) => {
        postData('imail/Internal_imail/get_team_member', {teamId: teamId}).then((result) => {
               if (result.status) {
                    this.setState({existingAdmin: result.data})
               }
        });
    }
    
    removeTeamMember = (index, adminId) => {
        archiveALL({teamId: this.props.updateData.id, adminMemberId: adminId}, '', 'imail/Internal_imail/remove_team_member').then((result) => {
              if(result.status){
                  var state = {}
                  state['existingAdmin'] = this.state.existingAdmin.filter((s, sidx) => index !== sidx);
                  this.setState(state);
              }
        });
    }
    
    selectChange = (key, value) => {
            var state = {}
            state[key] = value;
            this.setState(state)
      };
      
    submitForm = (e) => {
         this.setState({loading: true});
         var formError = jQuery('#create_team').validate({ignore: []});
         if(jQuery('#create_team').valid()){
            postData('imail/Internal_imail/create_update_team', this.state).then((result) => {
               if (result.status) {
                   this.props.stateValueChange('openModalTeam', false)
                   this.props.getTeamDetails();
               }else{
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                //    toast.error(result.error, {
                         position: toast.POSITION.TOP_CENTER,
                         hideProgressBar: true
                   });
               }
               this.setState({loading: false});
           });
        }
     }
    
    render() {
        return (
                <Modal className="modal fade-scale Modal_A Gold  Modal_B bg_grey" show={this.props.openModalTeam} onHide={this.handleHide} container={this} aria-labelledby="myModalLabel" id="modal_1" tabIndex="-1" role="dialog" >
               <form id="create_team">
                <Modal.Body>
                    <div className="dis_cell">
                        <div className="text text-left by-1 Popup_h_er_1">
                        <span>{(this.props.mode == 'update')? 'Editing Team: ' + this.props.updateData.team_name: 'Creating New Team'}</span>
                            <a data-dismiss="modal" aria-label="Close" className="close_i" onClick={() => this.props.stateValueChange('openModalTeam', false)}><i className="icon icon-cross-icons"></i></a>
                        </div>
                        <div className="row P_15_TB">
                            <div className="col-md-5">
                            <label>Team Name:</label>
                            <span className="required">
                                 <input type="text" name="team_name" onChange={(e)=>this.setState({'team_name':e.target.value})} value={this.state.team_name} data-rule-required="true"/>
                             </span>
                            </div>
                            <div className="col-md-3">
                            <label>Team Colour:</label>
                                  <span className="required">
                                        <div className="colorPicker" onClick={() => this.selectChange('displayColorPicker', true) }>
                                             <div style={{backgroundColor: this.state.team_color}} />
                                        </div>
                                   </span>
                                  {this.state.displayColorPicker ? <div style={colorPickerStyle.popover }>
                                    <div style={ colorPickerStyle.cover }  onClick={() => this.selectChange('displayColorPicker', false) }/>
                                    <SketchPicker color={ this.state.team_color } onChange={(color) => this.selectChange('team_color', color.hex)} />
                                  </div> : null }
                                 
                            </div>
                        </div>
                         
                        {(this.props.mode == 'update')? 
                        <div className="row P_15_TB by-1">
                            <div className="col-md-5">
                            <label>Team Members:</label>
                                {this.state.existingAdmin.map((val, index) => (
                                    <div key={index+1} className="team_members_div">
                                        <span className="t_m_d_1"><img src={(val.adminProfile? val.adminProfile : ((val.adminGender == 2)? this.SheImg: this.HeImg))} /></span>
                                        <span  className="t_m_d_2">{val.adminName}</span>
                                        <span  className="t_m_d_3"><i onClick={() => this.removeTeamMember(index, val.adminId)} className="icon icon-email-pending hand_cursor"></i></span>
                                    </div>
                                ))}
                            </div>
                        </div>:''}
                        
                        <div className="row P_15_T">
                            <div className="col-md-12">
                            <label>Team Member Look-Up:</label>
                            </div>
                           
                            {this.state.team_member.map((member, index) => (
                             <span key={index+1} className="upload_btn">
                                 <div className="col-md-4 mb-3 required modify_select">
                                 <label className="btn btn-default btn-sm center-block btn-file">
				   <Select.Async
                                    name="memberIds"
                                    clreable={false}
                                    cache={false}
                                    value={member.member || ''}
                                    loadOptions={(e) => getOptionsGroupAdmin(e, this.state.team_member, this.state.existingAdmin)}
                                    placeholder=''
                                    onChange={(e)=> handleShareholderNameChange(this, 'team_member', index, 'member', e)}
                                    className="default_validation"
                                    required={(index > 0 || this.state.existingAdmin.length == 0) ? true : false}
                                    />
		                </label>
                                </div>

                               <div className="col-md-2 mb-3">
                                {index > 0 ? <button className="button_plus__ " onClick={(e) => handleRemoveShareholder(this, e, index, 'team_member')}>
                                            <i  className="icon icon-decrease-icon Add-2-2" ></i></button> 
                                            : 
                                     <button  className="button_plus__" onClick={(e) => handleAddShareholder(this, e, 'team_member', member)}><i  className="icon icon-add-icons Add-2-1" ></i></button>}
                               </div>
                              </span>
                             ))}
                        </div>

                        <div className="row">
                            <div className="col-md-7"></div>
                            <div className="col-md-5">
                                <button type="button" className="but" value={'Save'} name="content" onClick={(e)=>this.submitForm(e)}>Save Team</button>
                            </div>
                        </div>
                    </div>  
                </Modal.Body>
               </form>
            </Modal>
          );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchtoProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CreateUpdateTeam) 