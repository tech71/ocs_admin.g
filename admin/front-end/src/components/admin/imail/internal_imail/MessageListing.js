import React from 'react';
import {  Link } from 'react-router-dom';
import { connect } from 'react-redux'
import moment from 'moment';
import {getInternalMailContent} from '../actions/InternalImailAction';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";

import { customSingleChatingLoader } from '../../../../service/CustomContentLoader.js';

import { OverlayTrigger,  Tooltip } from 'react-bootstrap';

class MessageListing extends React.Component {
    constructor(props) {
        super(props);
        this.active = this.props.active;
    }
    
    setAllContentData = (messageId) => {
         this.props.getInternalMailContent({messageId: messageId, type: this.props.mail_type});
    }

    render() {
        return (
            <div className="Internal_M_D">
             
                    <ReactPlaceholder showLoadingAnimation ={true}  customPlaceholder={ customSingleChatingLoader()} ready={!this.props.is_fetching}>
                    <div className="custom_scolling">
                      

                        {this.props.mail_listing.length > 0 ?
                            this.props.mail_listing.map((val, index) => (
                            <div key={index + 1} className={"mess_V1_1 " + ((val.is_block == 1) ? 'block_MSG' : '')}>

                                <div className="mess_v1">

                                    <div className="mess_vn_1">
                                        <Link onClick={() => this.setAllContentData(val.id)} to={"/admin/imail/internal/" + this.props.mail_type + '/' + val.id}>
                                            <div className="mess_vn_in_1">
                                                <div className="mess_V_1">

                                                    <div className="nav_apps text-left px-0 py-0">
                                                        <span><img src={val.user_img} /></span>
                                                    </div>

                                                </div>
                                                <div className="mess_V_2">
                                                    <div className="mess_V_a">
                                                        <div className="mess_V_a1">From: {val.user_name} </div>
                                                        <div className="mess_V_a2">{val.title}</div>
                                                    </div>
                                                    <div className="mess_V_b">{moment(val.mail_date).format('DD/MM/YYYY LT')}</div>
                                                    <div className="mess_vn_2">
                                                        {(val.is_block == 1) ? <OverlayTrigger  placement="bottom" overlay={<Tooltip id={'bl'+index} className="MSG_Tooltip"  placement={'top'}>Blocked</Tooltip>}><i className="icon icon-block-im attach_im color"></i></OverlayTrigger>: <React.Fragment />}
                                                        {(val.is_flage == 1) ? <OverlayTrigger  placement="bottom" overlay={<Tooltip id={'fl'+index}  className="MSG_Tooltip"  placement={'top'}>Flaged</Tooltip>}><i className="icon icon-flag-im flag-im color"></i></OverlayTrigger> : <React.Fragment />}
                                                        {(val.have_attachment == 1) ? <OverlayTrigger  placement="bottom" overlay={<Tooltip id={'at'+index}  className="MSG_Tooltip"  placement={'top'}>Attached file</Tooltip>}><i className="icon icon-attach-im attach_im color"></i></OverlayTrigger> : <React.Fragment />}
                                                        {(val.is_priority == 1) ? <OverlayTrigger  placement="bottom" overlay={<Tooltip id={'pr'+index}  className="MSG_Tooltip"  placement={'top'}>High Priority</Tooltip>}><i className="icon icon-warning-im warning-im color"></i></OverlayTrigger> : <React.Fragment />}
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>

                                        <div className="mess_vn_in_2">
                                            <p className="mb-0 mt-2"></p>
                                            <p className="my-0 ML_show_fixed">{val.content}</p>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        )): <h1 className="not_msg_v1">No Result</h1>}

                    </div> </ReactPlaceholder>
                   
            </div>
        );
    }
}

const mapStateToProps = state => ({
    mail_listing : state.InternalImailReducer.mail_listing,
    mail_type : state.InternalImailReducer.mail_type,
    is_fetching : state.InternalImailReducer.is_fetching,
})

 const mapDispatchtoProps = (dispach) => {
      return {
           getInternalMailContent : (value) => dispach(getInternalMailContent(value)),
      }
}
                
export default connect(mapStateToProps, mapDispatchtoProps)(MessageListing)   