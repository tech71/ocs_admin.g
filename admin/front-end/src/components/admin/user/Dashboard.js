import React, { Component } from 'react';

import { ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn } from '../../../service/common.js';
import { Link } from 'react-router-dom';

import PinModal from '../../admin/PinModal';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);

    }

    render() {
        return (
            <div>


                <section className="manage_top">
                    <div className="container-fluid Blue">

                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-sm-12">
                                <Link to={ROUTER_PATH+'admin/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-sm-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-12  col-sm-12 text-center">
                                <h1 className="color">Admin Portal</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-sm-12"><div className="bt-1"></div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-6 col-lg-offset-3 col-sm-10 col-sm-offset-1 P_30_TB"><div className="text_app">Your App</div></div>
                        </div>


                        <div className="row">
                            <div className="col-lg-2 col-sm-3 col-lg-offset-2 text-center"><Link className="d-inline-block" to={ROUTER_PATH+'admin/user/approval'} ><span className="circle_but">A</span><p>Approval</p></Link></div>
                          {/*   <div className="col-lg-2 col-sm-3 text-center"><a href="javascript:void(0)" className="d-inline-block" href=""><span className="circle_but">R</span><p>Report</p></a></div> */}
                            <div className="col-lg-2 col-sm-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH+'admin/user/reports'}><span className="circle_but">R</span><p>Reports</p></Link></div>
                            <div className="col-lg-2 col-sm-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH+'admin/user/logs'}><span className="circle_but">L</span><p>Logs</p></Link></div>
                            <div className="col-lg-2 col-sm-3 text-center"><Link className="d-inline-block" to={ROUTER_PATH+'admin/user/list'}><span className="circle_but">U</span><p>User Management</p></Link></div>
                        </div>
                    </div>
                </section>

                <PinModal color={'Blue'} moduleHed="Admin Module" />
            </div>
        );
    }
}
export default Dashboard;
