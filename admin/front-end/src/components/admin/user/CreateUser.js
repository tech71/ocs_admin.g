import React, { Component } from 'react';
import jQuery from "jquery";

import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { BASE_URL,ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { Link, Redirect } from 'react-router-dom';

import 'react-block-ui/style.css';
import BlockUi from 'react-block-ui';

import '../../../service/jquery.validate.js';
import "../../../service/custom_script.js";
import { connect } from 'react-redux';
import {setUserDetails, setActiveSelectPage} from 'components/admin/user/actions/UserAction';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import { ToastUndo } from 'service/ToastUndo.js'


class CreateUser extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn();

        this.state = {
            department_error: false,
            position: '',
            password: '',
            email: [],
            PhoneInput: [{ name: '' }],
            EmailInput: [{ name: '' }],
            department_option: [],
            rolesList: [],
            access_error: false,
            loading: true,
            success: false,
            edit_mode: false,
            its_super_admin: false,
        }

    }


    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    selectChange = (selectedOption) => {
        this.setState({ department: selectedOption, department_error: false });
    }

    setRoleToUser = (e, idx, roleId) => {
        var List = this.state.rolesList;

        if (List[idx].id == 7 && List[idx].id) {
            var key = this.state.rolesList.findIndex(x => x.id == 8)
            List[key].access = false;
        }
        if (List[idx].id == 11 && List[idx].id) {
            var key = this.state.rolesList.findIndex(x => x.id == 12)
            List[key].access = false;
        }
        if (List[idx].id == 10 && List[idx].id) {
            var key = this.state.rolesList.findIndex(x => x.id == 14)
            List[key].access = false;
        }
        
        if (!List[idx].access) {
            this.setState({ access_error: false });
            List[idx].access = true;
        } else {
            List[idx].access = false;
        }

        this.setState({ rolesList: List }, function () { this.setState({ loading: false }); });
    }

    handleShareholderNameChange = (idx, evt, tagType) => {
        let list = this.state[tagType]
        var state = {}

        if (tagType == 'EmailInput') {
            list[idx].name = evt.target.value.replace(/\s/g, '');
        } else {
            list[idx].name = evt.target.value;
        }

        state[tagType] = list
        this.setState(state);
    }

    handleAddShareholder = (e, tagType) => {
        e.preventDefault();
        if (tagType === 'PhoneInput') {
            this.setState({ PhoneInput: this.state.PhoneInput.concat([{ name: '' }]) });
        } else {
            this.setState({ EmailInput: this.state.EmailInput.concat([{ name: '' }]) });
        }
    }

    handleRemoveShareholder = (e, idx, tagType) => {
        e.preventDefault();
        if (tagType === 'PhoneInput') {
            this.setState({ PhoneInput: this.state.PhoneInput.filter((s, sidx) => idx !== sidx) });
        } else {
            this.setState({ EmailInput: this.state.EmailInput.filter((s, sidx) => idx !== sidx) });
        }
    }

    cutomValidation = () => {
        if (!this.state.department) {
            this.setState({ department_error: true });
        }

        var access = false
        this.state.rolesList.map((roleAccess, sidx) => {
            if (roleAccess.access) {
                access = true;
            }
        });

        if (!access)
            this.setState({ access_error: true });
 
        
        if (this.state.department && access)
            return true;

        return false;
    }

    onSubmit = (e) => {
        e.preventDefault();
        jQuery("#create_user").validate({ /* */ });
        var customValid = this.cutomValidation();


        if (!this.state.loading && jQuery("#create_user").valid() && customValid) {
            this.setState({ loading: true });
            postData('admin/Dashboard/create_edit_user', this.state).then((result) => {
                if (result.status) {
                    toast.success((this.state.edit_mode)?<ToastUndo message={'User updated successfully'} showType={'s'} />: <ToastUndo message={'User created successfully'} showType={'s'} />, {
                    // toast.success((this.state.edit_mode) ? "User updated successfully" : "User created successfully", {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });

                    setTimeout(() => this.setState({ success: true }), 2000);
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
                this.setState({ loading: false });
            });
        }
    }

    componentDidMount() {
        
        this.getPageSelect();
        this.setState({ loading: true });
        postData('common/Common/get_department', {}).then((result) => {
            if (result.status) {
                this.setState({ department_option: result.data });
            }
            this.setState({ loading: false });
        });
        postData('admin/Dashboard/get_listing_roles', this.props.props.match.params).then((result) => {
            if (result.status) {
                this.setState({ rolesList: result.data });
            }
            this.setState({ loading: false });
        });

        if (this.props.props.match.params.AdminId) {
            this.props.setSubmenuShow(1);
            postData('admin/Dashboard/get_user_details', this.props.props.match.params).then((result) => {
                if (result.status) {
                    this.setState(result.data);
                    this.setState({ edit_mode: true });
                    this.props.setUserDetails(result.data);
                } else {
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                    this.setState({ success: true })
                }
                this.setState({ loading: false });
            });
        }else{
            this.props.setSubmenuShow(0);
        }
    }

    getPageSelect(){
        this.props.setActivePage('single_user_management');
      }

    componentDidUpdate(){
        if(this.props.showTypePage!='single_user_management'){
              this.getPageSelect();
        }
    }

    changeIconColor=(role)=>{
        console.log(role);
        let a='';
        if(role.role_key.toLowerCase()=='finance' || role.role_key.toLowerCase()=='marketing'|| role.role_key.toLowerCase()=='finance_planner'){
            a=role.name.toLowerCase()
        }else{
            a= role.name.charAt(0).toLowerCase();
        }
        
        return a;

    }

    render() {
        return (
            <div>
                {(this.state.success) ? <Redirect to='/admin/user/list' /> : ''}
                <BlockUi tag="div" blocking={this.state.loading}>

                    <section className="manage_top">
                        <div className="container-fluid Blue">

                            <div className="row  _Common_back_a">
                                <div className="col-lg-12 col-md-12">
                                    <Link className="d-inline-flex" to={ROUTER_PATH+'admin/user/list'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                                </div>
                            </div>
                            <div className="row _Common_He_a">
                                <div className="col-lg-8  col-md-12">
                                    <h1 className="color">User Management - <span>{this.state.edit_mode ? 'Update' : 'Create'}</span></h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                                </div>
                            </div>


                                <form id="create_user" autoComplete="off">
                                    <div className="row py-4">
                                        <div className="w-40-lg col-lg-4  col-sm-6 py-3">
                                            <div className="row">
                                                <div className="col-lg-12 col-sm-12"><label className="label_font">Name:</label></div>
                                                <div className="col-lg-6 col-sm-6"><input className="input_f" type="text" name="firstname" placeholder="First" onChange={this.handleChange} value={this.state.firstname || ''} data-rule-required="true" maxLength="30" data-msg-required="Add First Name" /></div>
                                                <div className="col-lg-6 col-sm-6"><input className="input_f" type="text" name="lastname" placeholder="Last" onChange={this.handleChange} value={this.state.lastname || ''} data-rule-required="true" maxLength="30" data-msg-required="Add Last Name" /></div>
                                            </div>
                                        </div>
                                        <div className="w-20-lg col-lg-2 col-sm-3 py-3">
                                            <label className="label_font">Position:</label>
                                            <input className="input_f" type="text" name="position" placeholder="Enter Position" onChange={this.handleChange} value={this.state.position || ''} data-rule-required="true" maxLength="30" data-msg-required="Enter Position" />
                                        </div>
                                        <div className="w-20-lg col-lg-2 col-sm-3 py-3 select-parant-validation">
                                            <label className="label_font">Department:</label>

                                            <Select name="department" required={true} simpleValue={true}  searchable={false} clearable={false} value={this.state.department || ''} onChange={this.selectChange} options={this.state.department_option} placeholder="Department" />
                                            <div className={'tooltip fade top in' + ((this.state.department_error) ? ' select-validation-error' : '')} role="tooltip"><div className="tooltip-arrow"></div><div className="tooltip-inner">Select Department.</div></div>
                                        </div>
                                    </div>


                                    <div className="row ">
                                        <div className="w-20-lg col-lg-2  col-sm-3 pb-3">
                                            <label className="label_font">Phone:</label>

                                            {this.state.PhoneInput.map((PhoneInput, idx) => (
                                                <div className="input_plus__ " key={idx + 1}>
                                                    <input className="input_f distinctPhone" type="text"
                                                        value={PhoneInput.name || ''}
                                                        name={'phone_' + idx}
                                                        placeholder="Can include area code"
                                                        onChange={(evt) => this.handleShareholderNameChange(idx, evt, 'PhoneInput')}
                                                        data-rule-required="true"
                                                        maxLength="18"
                                                        data-rule-notequaltogroup='[".distinctPhone"]'
                                                        data-msg-notequaltogroup='Please enter uniqe phone number'
                                                        data-msg-required="Add Phone Number"
                                                    />

                                                    {idx > 0 ? <button  onClick={(e) => this.handleRemoveShareholder(e, idx, 'PhoneInput')}>
                                                        <i className="icon icon-decrease-icon Add-2" ></i>
                                                    </button> : (this.state.PhoneInput.length == 2) ? '' : <button onClick={(e) => this.handleAddShareholder(e, 'PhoneInput')}>
                                                        <i className="icon icon-add-icons Add-1" ></i>
                                                    </button>}

                                                </div>
                                            ))}
                                        </div>

                                        <div className="w-20-lg col-lg-2 col-sm-3 pb-3">
                                            <label className="label_font">Email:</label>
                                            {this.state.EmailInput.map((EmailInput, idx) => (
                                                <div className="input_plus__ mb-1 " key={idx + 1}>
                                                    <input className="input_f distinctEmail" type="text"
                                                        value={EmailInput.name || ''}
                                                        name={'email_' + idx}
                                                        placeholder={idx > 0 ? 'Secondary email' : 'Primary Email'}
                                                        onChange={(evt) => this.handleShareholderNameChange(idx, evt, 'EmailInput')}
                                                        data-rule-required="true"
                                                        data-rule-email="true"
                                                        data-rule-remote={BASE_URL + 'admin/Dashboard/check_user_emailaddress_already_exist' + ((this.state.edit_mode) ? '?adminId=' + this.state.id : '')}
                                                        data-msg-remote="This email already exist"
                                                        data-rule-notequaltogroup='[".distinctEmail"]'
                                                        maxLength="70"
                                                        data-msg-required="Add Email Address"
                                                    />
                                                    {idx > 0 ? <button onClick={(e) => this.handleRemoveShareholder(e, idx, 'EmailInput')}>
                                                        <i className="icon icon-decrease-icon Add-2" ></i>
                                                    </button> : (this.state.EmailInput.length == 2) ? '' : <button onClick={(e) => this.handleAddShareholder(e, 'EmailInput')}>
                                                        <i className="icon icon-add-icons Add-1" ></i>
                                                    </button>}
                                                </div>
                                            ))}
                                        </div>

                                        <div className="w-20-lg col-lg-2 col-sm-3 pb-3">
                                            <label className="label_font">Username:</label>
                                            <div className="add_input ">
                                                <input className="input_f mb-1" type="text"
                                                    value={this.state.username || ''}
                                                    name="username"
                                                    placeholder="Username"
                                                    onChange={(e) => this.setState({ username: e.target.value.replace(/\s/g, '') })}
                                                    data-rule-required="true"
                                                    data-rule-minlength="6"
                                                    maxLength="25"
                                                    readOnly={(this.state.edit_mode)}
                                                    data-rule-remote={BASE_URL + 'admin/Dashboard/check_username_already_exist/' + ((this.state.edit_mode) ? '?adminId=' + this.state.id : '')}
                                                    data-msg-remote="This username already exist"
                                                />
                                            </div>
                                        </div>
                                        <div className="w-20-lg col-lg-2 col-sm-3 pb-3">
                                            <label className="label_font">Password:</label>
                                            <div className="add_input ">
                                                <input className="input_f mb-1" type="password"
                                                    value={this.state.password || ''}
                                                    name="password"
                                                    maxLength="20"
                                                    data-rule-strongPassword="true"
                                                    placeholder="Password"
                                                    onChange={this.handleChange}
                                                    data-rule-required={this.state.edit_mode ? false : true}
                                                    data-rule-minlength="6" autoComplete="new-password"
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    {!this.state.its_super_admin?
                                        <div>
                                            <div className="row mt-4" >
                                                <div className="col-lg-12 col-sm-12 text-center">
                                                    <h2 className="py-4 by-1 bold-600 color">Access Permissions</h2>
                                                </div>
                                            </div>

                                            <div className="row text-center row-access nEW_access_list pt-4">

                                                <div className="col-lg-12 col-sm-12">
                                                    <ul className="but_around">
                                                        {this.state.rolesList.map((role, idx) => (

                                                            <li key={role.id} className={(role.role_key == 'fms_incentive' || role.role_key == 'crm_admin' ||  role.role_key == 'recruitment_admin' || role.role_key == 'finance_admin') ? 'd-none' : ''} >
                                                               <div>
                                                                {(role.id == 8) ? '' :
                                                                
                                                                   <a><span title={role.name} key={role.id} className={'add_access ' + this.changeIconColor(role) + '-colr' + ((role.access) ? ' access' : '')}  onClick={(e) => this.setRoleToUser(e, idx)} >{role.name.charAt(0)}</span></a>
                                                                //  <React.Fragment><a><span title={role.name} key={role.id} className={'add_access ' + this.changeIconColor(role) + '-colr' + ((role.access) ? ' access' : '')}  onClick={(e) => this.setRoleToUser(e, idx)} >{role.name.charAt(0)}</span></a><label className="checkbox_icons mt-3"><small>{role.name}</small></label></React.Fragment>
                                                                }

                                                                {(role.role_key == 'fms') ?
                                                                    <label className="checkbox_icons add_new_s0 mt-3">
                                                                        <input type='checkbox' onChange={(e) => this.setRoleToUser(e, this.state.rolesList.findIndex(x => x.role_key == 'fms_incentive'))} checked={(this.state.rolesList.find(x => x.role_key == 'fms_incentive').access) ? true : false || ''} disabled={(role.access) ? false : true} />
                                                                        <span></span>
                                                                        <small> Incidents</small>
                                                                    </label> : ''}
                                                                    
                                                                {(role.role_key == 'crm') ?
                                                                    <label className="checkbox_icons add_new_s0 mt-3">
                                                                        <input type='checkbox' onChange={(e) => this.setRoleToUser(e, this.state.rolesList.findIndex(x => x.role_key == 'crm_admin'))} checked={(this.state.rolesList.find(x => x.role_key == 'crm_admin').access) ? true : false || ''} disabled={(role.access) ? false : true} />
                                                                        <span></span>
                                                                        <small> Crm Admin</small>
                                                                </label> : ''}
                                                                        
                                                                {(role.role_key == 'recruitment') ?
                                                                    <label className="checkbox_icons add_new_s0 mt-3">
                                                                        <input type='checkbox' onChange={(e) => this.setRoleToUser(e, this.state.rolesList.findIndex(x => x.role_key == 'recruitment_admin'))} checked={(this.state.rolesList.find(x => x.role_key == 'recruitment_admin').access) ? true : false || ''} disabled={(role.access) ? false : true} />
                                                                        <span></span>
                                                                        <small> Recruitment Admin</small>
                                                                </label> : ''}
                                                                
                                                                {(role.role_key == 'finance') ?
                                                                    <label className="checkbox_icons add_new_s0 mt-3">
                                                                        <input type='checkbox' onChange={(e) => this.setRoleToUser(e, this.state.rolesList.findIndex(x => x.role_key == 'finance_admin'))} checked={(this.state.rolesList.find(x => x.role_key == 'finance_admin').access) ? true : false || ''} disabled={(role.access) ? false : true} />
                                                                        <span></span>
                                                                        <small> Finance Admin</small>
                                                                </label> : ''}
                                                                </div>
                                                            </li>
                                                        ))}
                                                    </ul>


                                                    {(this.state.access_error) ? <span className="access-error">Please select at least one access</span> : ''}
                                                </div>
                                            </div>
                                        </div> : ''}

                                    <div className="row">
                                        <div className="col-lg-12 col-sm-12 text-center pt-4">
                                            <ToastContainer />
                                        </div>
                                    </div>
                                    <div className="row d-flex justify-content-end">
                                        <div className="w-20-lg col-lg-2 col-sm-3  mt-3"><button disabled={this.state.loading} onClick={this.onSubmit} className="but">Save User</button></div>
                                    </div>

                                </form>


                            </div>
                    </section>
                 
                 </BlockUi>
                </div>
                );
    }
}

const mapStateToProps = state => ({
    showTypePage: state.UserDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result)),
        setActivePage:(result) => dispach(setActiveSelectPage(result)),
        setUserDetails:(result) => dispach(setUserDetails(result)),
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(CreateUser);
