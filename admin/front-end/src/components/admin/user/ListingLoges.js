import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import { PAGINATION_SHOW, ROUTER_PATH } from '../../../config.js';
import { checkItsNotLoggedIn, postData } from '../../../service/common.js';
import { Link } from 'react-router-dom';
import { CSVLink } from 'react-csv';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { setSubmenuShow } from 'components/admin/actions/SidebarAction';
import Pagination from "../../../service/Pagination.js";



const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = { pageSize: pageSize, page: page, sorted: sorted, filtered: filtered };
        postData('admin/Dashboard/get_loges', Request).then((result) => {
            if (result.status) {
                let filteredData = result.data;
                const res = {
                    rows: filteredData,
                    pages: (result.count)
                };
                resolve(res);
            } else {
//                window.location.href = "/admin/user/logs";
            }
        });
    });
};

class ListingLoges extends Component {
    constructor(props) {
        super(props);
        this.logTItle = { participant: 'Participants', organisation: 'Organisation', member: 'Members', fms: 'FMS', schedule: 'Schedule', imail: "Imail", '': 'All' }
        checkItsNotLoggedIn();
        ////checkPinVerified();
        this.state = {
            logesListing: [],
            selected: [],
            selectAll: 0,
            module: (this.props.props.match.params.module) ? this.props.props.match.params.module : '',
            userSelectedList: []
        }
    }


    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                logesListing: res.rows,
                pages: res.pages,
                loading: false,
                selectAll: 0,
                userSelectedList: [],
                selected: []
            });
        });
    }

    downloadAll = () => {
        //console.log(this.state.selected);
    }


    toggleRow = (id) => {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        let selectedList = this.state.logesListing;
        var userSelectedList = this.state.userSelectedList;


        var columnIndex = selectedList.findIndex(x => x.id == id);
        if (newSelected[id]) {
            this.setState({
                userSelectedList: userSelectedList.concat(selectedList[columnIndex]),
            });
        } else {
            var tempState = {};
            var selectedColumnIndex = userSelectedList.findIndex(x => x.id == id);
            tempState['userSelectedList'] = userSelectedList.filter((s, sidx) => selectedColumnIndex !== sidx);
            this.setState(tempState);
        }

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll = () => {
        let newSelected = {};
        let selectedList = this.state.logesListing;

        if (this.state.selectAll === 0) {
            this.state.logesListing.forEach(x => {
                newSelected[x.id] = true;
            });
        } else {
            selectedList = [];
        }

        this.setState({
            userSelectedList: selectedList,
            selected: newSelected,
            selectAll: (this.state.selectAll === 0) ? 1 : 0
        });
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.props.match.params, () => {
            var filter = { search_box: this.state.search_box, module: this.state.module, on_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date }
            this.setState({ filtered: filter });
        });
    }


    searchBox = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var filter = { search_box: this.state.search_box, module: this.state.module, on_date: this.state.shift_date, start_date: this.state.start_date, end_date: this.state.end_date }
            this.setState({ filtered: filter });
        });
    }

    componentDidMount() {
        this.props.setSubmenuShow(0);
    }

    render() {
        const headers = [
            { label: 'ID', key: 'created_by' },
            { label: 'Date', key: 'created' },
            { label: 'Time', key: 'time' },
            { label: 'Description', key: 'title' },
        ];
        const columns = [{
            id: "checkbox", accessor: "",
            Cell: ({ original }) => {
                return (

                    <span className="Set_Sec___">
                        <label class={"Cus_Check_1 "}>
                            <input type='checkbox' checked={this.state.selected[original.id] === true} onChange={() => this.toggleRow(original.id)} />
                            <div class="chk_Labs_1"></div>
                        </label>
                    </span>

                );
            },
            Header: x => {
                return (

                    <span className="Set_Sec___ pl-2">
                        <label class={"Cus_Check_1 " + (this.state.selectAll === 2 ? "minus_select__" : '')}>
                            <input type='checkbox' checked={this.state.selectAll === 1} ref={input => {
                                if (input) { input.indeterminate = this.state.selectAll === 2 }
                            }}
                                onChange={() => this.toggleSelectAll()} />
                            <div class="chk_Labs_1"></div>
                        </label>
                    </span>


                );
            },
            sortable: false,
            width: 50,

        },
        { Header: 'User ID', accessor: 'created_by', filterable: false, className: "_align_c__" },
        { Header: 'Date', accessor: 'created', filterable: false, className: "_align_c__" },
        { Header: 'Time', accessor: 'time', filterable: false, className: "_align_c__" },
        {
            Header: 'Description', accessor: 'title', filterable: false, className: "_align_c__",  headerClassName: '_align_c__ header_cnter_tabl',

            expander: true,
            className: "_align_c__",
            width: 400,
            Expander: (props) =>
                <div className="expander_bind">
                    <div className="d-flex w-100 justify-content-start align-item-center">
                        <span className=""><div className="ellipsis_line__">{props.original.title}</div></span>
                    </div>
                    {props.isExpanded
                        ? <i className="icon icon-arrow-down icn_ar1" style={{ fontSize: '13px' }}></i>
                        : <i className="icon icon-arrow-right icn_ar1" style={{ fontSize: '13px' }}></i>}
                </div>
        }

        ]

        return (
            <div>
                <section className="manage_top">
                    <div className="container-fluid Blue">

                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-md-12">
                                <Link to={ROUTER_PATH + 'admin/user/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-8  col-md-12">
                                <h1 className="color">Logs - <span>{this.logTItle[this.state.module]}</span></h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>


                        <div className="row _Common_He_a justify-content-center">
                            <div className="col-lg-5 col-sm-6">
                                <div className="input_search change_b">
                                    <input type="text" className="form-control" onChange={(e) => this.searchBox('search_box', e.target.value)} name="" value={this.state.search_box || ''} placeholder="Search" />
                                    <button type="submit">
                                        <span className="icon icon-search"></span>
                                    </button>
                                </div>
                            </div>
                            <div className="col-lg-5 col-sm-6">
                                <ul className="on_form_to_search">
                                    <li>
                                        <span>On:</span>
                                        <span><DatePicker  autoComplete={'off'} dateFormat="DD-MM-YYYY" isClearable={true} name="shift_date" onChange={(e) => this.searchBox('shift_date', e)} selected={this.state['shift_date'] ? moment(this.state['shift_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />
                                        </span>
                                    </li>
                                    <li>
                                        <span>Form:</span>
                                        <span><DatePicker  autoComplete={'off'} dateFormat="DD-MM-YYYY" isClearable={true} name="start_date" onChange={(e) => this.searchBox('start_date', e)} selected={this.state['start_date'] ? moment(this.state['start_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />
                                        </span>
                                    </li>
                                    <li>
                                        <span>To:</span>
                                        <span><DatePicker  autoComplete={'off'} dateFormat="DD-MM-YYYY" isClearable={true} name="end_date" onChange={(e) => this.searchBox('end_date', e)} selected={this.state['end_date'] ? moment(this.state['end_date'], 'DD-MM-YYYY') : null} className="text-center px-0" placeholderText="00/00/0000" />
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="row"><div className="col-lg-12 col-sm-12 bor_T"></div></div>

                        <div className="row text-center row-access pt-4">
                            <div className="col-lg-12 col-sm-12 py-4">
                                <ul className="but_around but_around_threee">
                                    <li><Link to={{ pathname: '/admin/user/logs/participant', state: 1 }}><span className={"add_access " + ((this.props.props.match.params.module == 'participant') ? "access" : "") + " p-colr"}>P</span></Link></li>
                                    <li><Link to={{ pathname: '/admin/user/logs/organisation', state: 2 }}><span className={"add_access " + ((this.props.props.match.params.module == "organisation") ? "access" : "") + " o-colr"}>O</span></Link></li>
                                    <li><Link to={{ pathname: '/admin/user/logs/fms', state: 3 }} ><span className={"add_access " + ((this.props.props.match.params.module == 'fms') ? "access" : "") + " f-colr"}>F</span></Link></li>
                                    <li><Link to={{ pathname: '/admin/user/logs/imail', state: 4 }}><span className={"add_access " + ((this.props.props.match.params.module == 'imail') ? "access" : "") + " i-colr"}>I</span></Link></li>
                                    <li><Link to={{ pathname: '/admin/user/logs/member', state: 5 }} ><span className={"add_access " + ((this.props.props.match.params.module == 'member') ? "access" : "") + " m-colr"}>M</span></Link></li>
                                    <li><Link to={{ pathname: '/admin/user/logs/schedule', state: 6 }}><span className={"add_access " + ((this.props.props.match.params.module == 'schedule') ? "access" : "") + " s-colr"}>S</span></Link></li>
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-lg-12 col-sm-12 L-I-P_Table">
                                <div className="listing_table PL_site th_txt_center__ odd_even_tBL  line_space_tBL H-Set_tBL">
                                <ReactTable
                                    PaginationComponent={Pagination}
                                    columns={columns}
                                    manual
                                    data={this.state.logesListing}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    defaultFiltered={{ module: this.state.module }}
                                    defaultPageSize={10}
                                    className="-striped -highlight"
                                    noDataText="No Record Found"
                                    minRows={2}

                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                    showPagination={this.state.logesListing.length >= PAGINATION_SHOW ? true : false}
                                    SubComponent={(props) =>
                                        <div className="tBL_Sub">
                                            <div className="tBL_des d-flex ">
                                                <b>Description:</b>
                                                <p className="mb-0 pt-1 pl-1"> {props.original.title}</p>
                                            </div>
                                        </div>
                                    }
                                />
                                </div>

                                <div className="col-sm-3"><div onClick={this.downloadAll} className="but">Download All</div></div>

                                <div className="col-sm-3 pull-right">
                                    <CSVLink data={this.state.userSelectedList} headers={headers} className="but" filename={"loges.csv"}
                                        onClick={(event) => {

                                            if (this.state.userSelectedList.length == 0) {
                                                toast.warning("Please select atleast one log to export.", {
                                                    position: toast.POSITION.TOP_CENTER,
                                                    hideProgressBar: true
                                                });
                                                return false;
                                            }
                                        }} >
                                        Export Selected
                            </CSVLink></div>

                            </div>
                        </div>



                    </div>
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    showTypePage: state.UserDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
}

export default connect(mapStateToProps, mapDispatchtoProps)(ListingLoges);
