import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL } from 'config.js';


class Reports extends Component {
    constructor(props) {
        super(props);
        this.state = {          
           
        };
    }

    render() {
        return (

            <div>


                <section className="manage_top">
                    <div className="container-fluid Blue">

                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-sm-12">
                                <Link to={ROUTER_PATH+'admin/user/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-sm-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-12  col-sm-12 text-center">
                                <h1 className="color">Reports</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-sm-12"><div className="bt-1"></div>
                            </div>
                        </div>

                        <div className="row">
                        <div className="col-lg-12  col-sm-12 text-center pt-5">
                        <div style={{fontSize:'24px', color:'var(--b-color)'}}>Report module is currently under construction. For minor report, check individual module. </div>
                        </div>
                        </div>
                    </div>
                </section>

                
            </div>
               
                );
    }
}
export default Reports
