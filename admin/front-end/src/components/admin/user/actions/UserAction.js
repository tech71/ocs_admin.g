import {adminActiveTitle} from 'menujson/admin_menu_json';
export const setApprovalDetails = (detailsData) => ({
        type: 'set_user_approval_data',
        detailsData
    });
export const setUserDetails = (detailsData) => ({
        type: 'set_user_managment_data',
        detailsData
    });
export const setActiveSelectPageData= (value) => {
    return {
        type: 'set_active_page_user',
        value
}}

    
export function setActiveSelectPage(request) {
    return (dispatch, getState) => {
        let pageData =adminActiveTitle;
        let pageType = pageData.hasOwnProperty(request) ? request: 'details';
        let pageTypeTitle = pageData[pageType];
        return dispatch(setActiveSelectPageData({pageType:pageType,pageTitle:pageTypeTitle}))
    }
}