import React, { Component } from 'react';


import ReactTable from "react-table";

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { checkItsNotLoggedIn, postData, archiveALL, reFreashReactTable } from '../../../service/common.js';
import { ROUTER_PATH, PAGINATION_SHOW } from '../../../config.js';
import { Link } from 'react-router-dom';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';
import { connect } from 'react-redux';
import {setSubmenuShow} from 'components/admin/actions/SidebarAction';
import Pagination from "../../../service/Pagination.js";
import { ToastUndo } from 'service/ToastUndo.js'
import { ParticiapntPageIconTitle, AdminPageIconTitle } from '../../../menujson/pagetitle_json.js';


const requestData = (pageSize, page, sorted, filtered) => {
    return new Promise((resolve, reject) => {

        // request json
        var Request = JSON.stringify({ pageSize: pageSize, page: page, sorted: sorted, filtered: filtered });

        postData('admin/Dashboard/list_admins', Request).then((result) => {
            let filteredData = result.data;
            const res = {
                rows: filteredData,
                pages: (result.count)
            };
            resolve(res);
        });

    });
};

/*
 * class ListUsers 
 */
class ListUsers extends Component {
    constructor(props) {
        super(props);
        checkItsNotLoggedIn(ROUTER_PATH);
        ////checkPinVerified(); 

        this.filterType = [{ label: 'Select', value: '' }, { label: 'Archive only', value: 'archive_only' }, { label: 'Active only', value: 'active_only' }, { label: 'Inactive only', value: 'inactive_only' }]
        this.state = {
            loading: false,
            userList: [],
            counter: 0,
            startDate: new Date(),
            search: '',
            search_by: ''
        };

        this.reactTable = React.createRef();

    }

    searchTable = (search_by) => {
        var searchData = { search: this.state.search, search_by: search_by };
        this.setState({ filtered: searchData, search_by: search_by });
    }

    submitSearch = (e) => {
        e.preventDefault();
        // reflec value of search box
        this.searchTable(this.state.search, this.state.search_by);
    };

    archiveHandle = (id) => {
        archiveALL({id: id}, 'Are you sure, you want to Archive this user?', 'admin/Dashboard/delete_user').then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            } else {
                if(result.error){
                    toast.dismiss();
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                    // toast.error(result.error, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
            }
        })
    }

    activeDeactiveHandle = (id, status) => {
        this.setState({ loading: true });
        postData('admin/Dashboard/active_inactive_user', { 'adminID': id, 'status': status }).then((result) => {
            if (result.status) {
                reFreashReactTable(this, 'fetchData');
            } else {
                this.setState({ loading: false });
                toast.dismiss();
                toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                // toast.error(result.error, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

    componentDidMount() { 
        this.props.setSubmenuShow(0);
    }

    fetchData = (state, instance) => {
        // function for fetch data from database
        this.setState({ loading: true });
        requestData(
            state.pageSize,
            state.page,
            state.sorted,
            state.filtered
        ).then(res => {
            this.setState({
                userList: res.rows,
                pages: res.pages,
                loading: false
            });
        });
    }

    render() {

        const { data, pages, loading } = this.state;
        const columns = [{ Header: 'ID', accessor: 'id', filterable: false, Cell: props => <span className={((props.original.archive == 1 || props.original.status == 0) ? 'Unot_A' : '')}>{props.value}</span> },
        { Header: 'First Name', accessor: 'firstname', filterable: false, },
        { Header: 'Last Name', accessor: 'lastname', filterable: false, },
        { Header: 'Email', accessor: 'email', filterable: false, },
        { Header: 'Phone', accessor: 'phone', filterable: false, },
        { Header: 'Username', accessor: 'username', filterable: false, },
        {
            Header: 'Action',
            accessor: 'id',
            sortable: false,
            filterable: false,
            Cell: props => <span className="booking_L_T2">
                <Link title={ParticiapntPageIconTitle.par_update_icon} to={ROUTER_PATH+'admin/user/update/' + props.value} ><i className="icon icon-edit3-ie Edit_i1"></i></Link>
                {props.original.archive == 0 ? <i title={ParticiapntPageIconTitle.par_archive_icon} onClick={() => this.archiveHandle(props.value)} className="icon icon-archive2-ie  Arch_i1"></i> : ""}
                {props.original.status == 1 ? <button title={AdminPageIconTitle.adm_inactive_icon} className="btn inactive_btn" onClick={() => this.activeDeactiveHandle(props.value, 0)} ><i className="icon icon-close1-ie"></i></button> :
                    <button title={AdminPageIconTitle.adm_active_icon} className="btn active_btn" onClick={() => this.activeDeactiveHandle(props.value, 1)} ><i className="icon icon-accept-approve1-ie"></i></button>}
            </span>
        },
        ]


        return (
            <div>

                <section className="manage_top"> 
                    <div className="container-fluid Blue">

                        <div className="row  _Common_back_a">
                            <div className="col-lg-12 col-md-12">
                                <Link className="d-inline-flex" to={ROUTER_PATH+'admin/user/dashboard'}><div className="icon icon-back-arrow back_arrow"></div></Link>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>
                        <div className="row _Common_He_a">
                            <div className="col-lg-12  col-md-12">
                                <h1 className="color">User Management</h1>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-lg-12 col-md-12"><div className="bt-1"></div>
                            </div>
                        </div>


                        <div className="w-100">
                            <form onSubmit={this.submitSearch}>
                            <div className="row _Common_He_a">
                                <div className="col-lg-6 col-sm-6 search_bar">
                                    <div className="input_search change_b">
                                        <input type="text" id="searchRoles" onChange={(e) => this.setState({ search: e.target.value })} className="form-control" placeholder="Search" />
                                        <button type="submit">
                                            <span className="icon icon-search"></span>
                                        </button>
                                    </div>
                                </div>
                                <div className="col-lg-3   col-sm-3  clearable-show">
                                    <Select className="wide" clearable={false} simpleValue={true} searchable={false} options={this.filterType} value={this.state.search_by || ''} placeholder="Filter by type" onChange={(e) => this.searchTable(e)} />
                                </div>
                                <div className="col-lg-3  col-sm-3 clearable-show">
                                    <Link class="Plus_button" to="/admin/user/create"><span>Create New User</span><i class="icon icon-add-icons create_add_but"></i></Link>
                                </div>
                            </div>
                            </form>
                            <div className="row"> <div className="col-lg-12 col-sm-12 px-0 "><div className="bb-1"></div></div></div>
                        </div>

                        <div className="row P_15_T">
                            <div className="col-lg-12 col-sm-12 px-0 listing_table PL_site Unot_A_table">
                                <ReactTable
                                PaginationComponent={Pagination}
                                    columns={columns}
                                    manual
                                    data={this.state.userList}
                                    pages={this.state.pages}
                                    loading={this.state.loading}
                                    onFetchData={this.fetchData}
                                    filtered={this.state.filtered}
                                    defaultPageSize={10}
                                    noDataText="No user"
                                    className="-striped -highlight"
                                    ref={this.reactTable}
                                    minRows={2}
                                    previousText={<span className="icon icon-arrow-left privious"></span>}
                                    nextText={<span className="icon icon-arrow-right next"></span>}
                                    showPagination={this.state.userList.length >= PAGINATION_SHOW ? true : false}
                                />
                            </div>
                        </div>

                    </div>

                </section>

            </div>
        );
    }
}
const mapStateToProps = state => ({
    showTypePage: state.UserDetailsData.activePage.pageType
})

const mapDispatchtoProps = (dispach) => {
    return {
        setSubmenuShow: (result) => dispach(setSubmenuShow(result))
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(ListUsers);
