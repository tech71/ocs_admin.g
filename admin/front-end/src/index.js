import React from 'react';
import ReactDOM from 'react-dom';
import dotenv from 'dotenv';
import './index.css';
import App from './App';
import { unregister } from './registerServiceWorker';
import { Provider } from 'react-redux'
import rootReducer from './reducers';



import { createStore, applyMiddleware, compose } from 'redux'


import thunkMiddleware from 'redux-thunk'
dotenv.config()

//const store = createStore(rootReducer)

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    composeEnhancer(applyMiddleware(thunkMiddleware)),
);

ReactDOM.render(<Provider store={store}>
    <App />
</Provider>, document.getElementById('root'));

unregister();
