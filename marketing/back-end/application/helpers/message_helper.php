<?php

function system_msgs($msg_key) {
    $global_ary = array(
        'something_went_wrong' => 'Something went wrong.',
        'wrong_username_password' => 'Invalid username or password.',
        'account_not_active' => 'Your account is not active please contact to HCM.',
        'success_login' => 'Login successfully.',
        'This_email_not_exist_oversystem' => 'Email address does not exist in our system.',
        'forgot_password_send_mail_succefully' => 'Please visit your inbox to reset your password.',
        'verfiy_token_error' => 'Invalid request.',
        'password_reset_successfully' => 'Password reset successfully.',
        'password_pin_update_success' => 'Save successfully.',
        'verfiy_password_error' => 'Invalid request.',
        'encorrect_pin' => 'Incorrect pin.',
        'token_verfied' => 'Pin verfied.',
        'link_exprire' => 'Sorry your link is expired.',
        'permission_error' => 'Sorry you have no permission to excess.',
        'server_error' => 'Server error.',
        'ip_address_error' => 'Ip address changed.',
        'no_staff' => 'Sorry, No staff assigned to this department.',
        'ndis_exist'=>'Sorry, This Ndis number is already exist.',
        'profile_saved'=>'Profile updated successfully.',
        'profile_not_saved'=>'Sorry, some error occur in profile saving.',
        'verification_code_sent'=>'Code sent successfully.',
        'verification_code_error'=>'Some Error Happened.',
        'verification_code_verified'=>'Profile verified successfully.',
        'verification_code_wrong'=>'Wrong code entered.'
    );
    return $global_ary[$msg_key];
}

?>
