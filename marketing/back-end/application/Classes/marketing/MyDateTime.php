<?php

/*
 * Filename: Admin.php
 * Desc: Deatils of Admin
 * @author YDT <yourdevelopmentteam.com.au>
 */

namespace MyDateTime;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

use DateTime;

class MyDateTime extends DateTime
{
    /**
    * Calculates start and end date of fiscal year
    * @param DateTime $dateToCheck A date withn the year to check
    * @return array('start' => timestamp of start date ,'end' => timestamp of end date)
    */
    public function fiscalYear()
    {
      $date_range_arr = array();
      $fn_month =date("m");
      $cur_year = date("Y");
      if($fn_month > 06){
        //Current year
        $month_sel = 06;
        $year = date('Y', strtotime('+1 year'));
        $end_date = new DateTime($year . '-' . $month_sel . '-01');
        $start_date = clone $end_date;
        $start_date->modify('-1 year');
        $end_date->modify('-2 day');


        $c_year = date('Y', strtotime($start_date->format('Y-m-d')));
        $start_month = date($c_year.'-m-01');
        $end_month =  date($c_year.'-m-t');

        $date_range_arr['c_f']  = array(
         'start_date' => $start_date->format('Y-m-d'),
         'end_date' => $end_date->format('Y-m-d'),
         'start_day_month' => $start_month,
         'last_day_month' => $end_month
       );
        // lAST year
        $year2 = date('Y');
        $end_date2 = new DateTime($year2 . '-' . $month_sel . '-01');
        $start_date2 = clone $end_date2;
        $start_date2->modify('-1 year');
        $end_date2->modify('-2 day');

        $c_year2 = date('Y', strtotime($start_date2->format('Y-m-d')));
        $start_month2 = date($c_year2.'-m-01');
        $end_month2 =  date($c_year2.'-m-t');

        $date_range_arr['l_f']  = array(
         'start_date' => $start_date2->format('Y-m-d'),
         'end_date' => $end_date2->format('Y-m-d'),
         'start_day_month' => $start_month2,
         'last_day_month' => $end_month2
       );

      }
       var_dump($date_range_arr); exit; 
      return $date_range_arr;
    }
  }
