<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_dashboard_count($session) {
        $participant_res = $this->get_onboard_or_participant_count($session);
        $member_res = $this->get_onboard_or_active_member_count($session);
        $org_res = $this->get_active_or_onboard_org_count($session);

        $fms_count = $this->get_active_or_previous_fms_count($session);
        $filled_shift_count = $this->get_filled_shift_hours_or_previous($session);

        // active specific count
        $result['total_active_participant_count'] = $participant_res['total_active_participant'];
        $result['total_active_member_count'] = $member_res['total_active_member'];
        $result['total_filled_shift_hours'] = number_format((float) $filled_shift_count['total_filled_shift_hours'], 2, '.', '');
        $result['total_fms_count'] = number_format((float) $fms_count['total_fms_count'], 2, '.', '');
        $result['total_active_org'] = $org_res['total_active_org'];

        // count title for soecific count according to month or year
        $result['dashboard_count_title'] = $session['dashboard_count_title'];

        // count of member who onboard according month and year
        $result['onboard_member_count_sp'] = $this->make_calculate_arr($member_res['onboard_member'], $member_res['onboard_member_previous']);

        // count of fms according month and year
        $result['fms_count_sp'] = $this->make_calculate_arr($fms_count['fms_count'], $fms_count['fms_count_previous']);

        // hours of filled shift according month and year
        $result['filled_shift_hours_sp'] = $this->make_calculate_arr($filled_shift_count['filled_shift_hours'], $filled_shift_count['filled_shift_hours_previous']);

        // count of participant who onboareded according month and year
        $result['onboarded_participant_count_sp'] = $this->make_calculate_arr($participant_res['onboard_participant'], $participant_res['onboard_participant_previous']);

        // count of member who onboard according month and year
        $result['onboard_org_count_sp'] = $this->make_calculate_arr($org_res['onboard_org'], $org_res['onboard_org_previous']);

        // count of member who onboard according month and year
        $result['inactive_participant_count_sp'] = $this->make_calculate_arr($participant_res['inactive_participant'], $participant_res['inactive_participant_previous']);

        return $result;
    }

    function get_session_according_to_request($request) {
        $view_type = isset($request->view_type) ? $request->view_type : 'year';

        $return = [];
        $previous_year = date('Y', strtotime('-1 year'));
        if ($view_type == 'year') {
            $start_date = $finicial_start_date = date('Y') . '-07-01';
            $end_date = $finicial_end_date = date('Y', strtotime('+1 year')) . '-06-30';

            $previous_start_date = $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_end_date = $previous_finicial_end_date = date('Y') . '-06-30';

            $curr_maxdays = $this->dateDiffInDays($start_date, date('Y-m-d'));
            $prev_maxdays = $this->dateDiffInDays($previous_start_date, $previous_end_date);

            $return['dashboard_count_title'] = [
                'current' => DateFormate($start_date, "Y") . '-' . DateFormate($end_date, "Y"),
                'previous' => DateFormate($previous_start_date, "Y") . '-' . DateFormate($previous_end_date, "Y"),
                'percentage' => 'previous year'
            ];
        } elseif ($view_type == 'current_month') {

            $previous_start_date = date("$previous_year-m-01");
            $previous_end_date = date("$previous_year-m-t");

            $curr_maxdays = date('d');
            $prev_maxdays = date("j", strtotime("last day of previous month"));

            $start_date = $first_day_this_month = date('Y-m-01');
            $end_date = $last_day_this_month = date('Y-m-t');
            $return['dashboard_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
        } elseif ($view_type == 'selected_month' && !empty($request->specific_month)) {
            $x = explode('-', $request->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];


            $start_date = date("$year-$month-01");
            $end_date = date("$year-$month-t");

            $previous_start_date = date("$previous_year-$month-01");
            $previous_end_date = date("$previous_year-$month-t");

            $curr_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $prev_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $previous_year);
            $return['dashboard_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
        }

        $return['start_date'] = $start_date;
        $return['end_date'] = $end_date;
        $return['previous_start_date'] = $previous_start_date;
        $return['previous_end_date'] = $previous_end_date;

        $return['curr_max_days'] = (int) $curr_maxdays;
        $return['prev_max_days'] = (int) $prev_maxdays;

        return $return;
    }

    function get_active_or_previous_fms_count($session) {
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN 1  ELSE '0' END) as fms_count", false);
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN 1  ELSE '0' END) as fms_count_previous", false);
        $this->db->select("count(id) as total_fms_count", false);

        $this->db->from('tbl_fms_case');
        $res = $this->db->get()->row_array();

        $res['fms_count'] = $res['fms_count'] ?? 0;
        $res['fms_count_previous'] = $res['fms_count_previous'] ?? 0;
        $res['total_fms_count'] = $res['total_fms_count'] ?? 0;

        return $res;
    }

    function get_filled_shift_hours_or_previous($session) {
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN (TIME_TO_SEC(TIMEDIFF(end_time, start_time)))/(60*60) else '0' END) as filled_shift_hours", false);
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN (TIME_TO_SEC(TIMEDIFF(end_time, start_time)))/(60*60) else '0'  END) as filled_shift_hours_previous", false);
        $this->db->select("sum(TIME_TO_SEC(TIMEDIFF(end_time, start_time)))/(60*60) as total_filled_shift_hours", false);

        $this->db->from('tbl_shift');
        $this->db->where('status', 7);
        $res = $this->db->get()->row_array();

        $res['filled_shift_hours'] = $res['filled_shift_hours'] ?? 0;
        $res['filled_shift_hours_previous'] = $res['filled_shift_hours_previous'] ?? 0;
        $res['total_filled_shift_hours'] = $res['total_filled_shift_hours'] ?? 0;

        return $res;
    }

    function get_onboard_or_participant_count($session) {
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN 1  ELSE '0' END) as onboard_participant", false);
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN 1  ELSE '0' END) as onboard_participant_previous", false);
        $this->db->select("sum(CASE WHEN p.status = 1 AND p.archive = 0 THEN 1  ELSE '0' END) as total_active_participant", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_user_active_inactive_history sub_history where (date(sub_history.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "') AND sub_history.id = p.id AND user_type = 2 AND action_type = 2  limit 1) 
THEN 1  ELSE '0' END) as inactive_participant", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_user_active_inactive_history sub_history where (date(sub_history.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "') AND sub_history.id = p.id AND user_type = 2 AND action_type = 2  limit 1) 
THEN 1  ELSE '0' END) as inactive_participant_previous", false);

        $this->db->from('tbl_participant as p');
        $res = $this->db->get()->row_array();

        $res['onboard_participant'] = $res['onboard_participant'] ?? 0;
        $res['onboard_participant_previous'] = $res['onboard_participant_previous'] ?? 0;
        $res['total_active_participant'] = $res['total_active_participant'] ?? 0;
        $res['inactive_participant'] = $res['inactive_participant'] ?? 0;
        $res['inactive_participant_previous'] = $res['inactive_participant_previous'] ?? 0;

        return $res;
    }

    function get_onboard_or_active_member_count($session) {
        $this->db->select("sum(CASE WHEN date(m.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN 1  ELSE '0' END) as onboard_member", false);
        $this->db->select("sum(CASE WHEN date(m.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN 1  ELSE '0' END) as onboard_member_previous", false);
        $this->db->select("sum(CASE WHEN m.status = 1 AND m.archive = 0 THEN 1  ELSE '0' END) as total_active_member", false);

        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $res = $this->db->get()->row_array();

        $res['onboard_member'] = $res['onboard_member'] ?? 0;
        $res['onboard_member_previous'] = $res['onboard_member_previous'] ?? 0;
        $res['total_active_member'] = $res['total_active_member'] ?? 0;

        return $res;
    }

    function get_active_or_onboard_org_count($session) {
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN 1  ELSE '0' END) as onboard_org", false);
        $this->db->select("sum(CASE WHEN date(created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN 1  ELSE '0' END) as onboard_org_previous", false);
        $this->db->select("sum(CASE WHEN status = 1 AND archive = 1 THEN 1  ELSE '0' END) as total_active_org", false);

        $this->db->from('tbl_organisation');
        $res = $this->db->get()->row_array();

        $res['onboard_org'] = $res['onboard_org'] ?? 0;
        $res['onboard_org_previous'] = $res['onboard_org_previous'] ?? 0;
        $res['total_active_org'] = $res['total_active_org'] ?? 0;

        return $res;
    }

    function make_calculate_arr($x, $y) {
        $x = (float) $x;
        $y = (float) $y;
        $percent = 0;
        if ($x != 0 || $y != 0) {
            if ($y == 0) {
                $percent = ($x * 100);
            } else {
                $percent = (($x - $y) / $y) * 100;
            }


            $percent = number_format((float) $percent, 2, '.', '');
        }

        $data = array(
            'current' => strpos($x, '.') ? number_format($x, 2, '.', '') : $x,
            'previous' => strpos($y, '.') ? number_format($y, 2, '.', '') : $y,
            'percent' => abs($percent),
            'status' => ($percent > 0) ? 1 : (($percent == 0) ? 3 : 2),
        );

        return $data;
    }

    function dateDiffInDays($date1, $date2) {
        // Calulating the difference in timestamps 
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours 
        // 24 * 60 * 60 = 86400 seconds 
        return abs(round($diff / 86400));
    }

    function dashboard_intake_graph($request) {
        
        $this->db->select(["rsl.id", "rsl.stage_number", "count(ra.id) as count", "year(ra.created) as year", "date(ra.created) as created"]);
        $this->db->from("tbl_recruitment_applicant as ra");

        $this->db->join("tbl_recruitment_stage as rs", " rs.id = ra.current_stage AND rs.archive = 0", "INNER");
        $this->db->join("tbl_recruitment_stage_label as rsl", "rsl.id = rs.stage_label_id AND rsl.archive = 0", "INNER");

        $this->db->where("date(ra.created) BETWEEN '" . $request['start_date'] . "' AND '" . $request['end_date'] . "'", null, false);
        $this->db->group_by('rsl.id');
        $this->db->order_by('ra.created');
        $this->db->order_by('rsl.stage_number');

        $applicant_result = $this->db->get()->result_array();
        if (!empty($applicant_result)) {
            $applicant_result = array_column($applicant_result, 'count', 'id');
        }


        $this->db->select("cs.parent_id as id");
        $this->db->select("count(case when (cs.id = 1 AND cs.parent_id = 0) OR (cs.parent_id != 0) THEN cp.id end) as count", false);
        $this->db->from("tbl_crm_participant as cp");

        $this->db->join("tbl_crm_participant_stage as cps", "cps ON cps.crm_participant_id = cp.id AND cps.status = 3", "INNER");
        $this->db->join("tbl_crm_stage as cs", "cs.id = cps.stage_id AND cs.status = 1", "INNER");

        $this->db->where("date(cp.created) BETWEEN '" . $request['start_date'] . "' AND '" . $request['end_date'] . "'", null, false);
        $this->db->group_by('cs.parent_id');
        $this->db->order_by('cs.parent_id');

        $crm_participant_result = $this->db->get()->result_array();

        if (!empty($crm_participant_result)) {
            $crm_participant_result = array_column($crm_participant_result, 'count', 'id');
            $crm_participant_result[1] = $crm_participant_result[0] ?? 0;
        }

        $graph_data = ['row1' => [], 'row2' => []];

        $recruitment_stage = $this->get_recruitment_stage();
        if (!empty($recruitment_stage)) {
            foreach ($recruitment_stage as $val) {
                $graph_data['row1'][] = isset($applicant_result[$val->id]) ? $applicant_result[$val->id] : 0;
            }
        }

        $crm_stage = $this->get_crm_stage();
        if (!empty($crm_stage)) {
            foreach ($crm_stage as $val) {
                $graph_data['row2'][] = isset($crm_participant_result[$val->id]) ? $crm_participant_result[$val->id] : 0;
            }
        }

        return $graph_data;
    }

    function get_recruitment_stage() {
        return $this->basic_model->get_record_where('recruitment_stage_label', ['id', 'stage_number'], ['archive' => 0]);
    }

    function get_crm_stage() {
        return $this->basic_model->get_record_where('crm_stage', ['id'], ['status' => 1, 'parent_id' => 0]);
    }

    function compare_between_org_or_site_or_sub_org($request) {

        $this->db->select("count(CASE when parent_org > 0 then 1 end) as parent_org_count");
        $this->db->select("count(CASE when parent_org = 0 then 1 end) as sub_org_count");
        $this->db->from("tbl_organisation as o");
        $this->db->where("date(o.created) BETWEEN '" . $request['start_date'] . "' AND '" . $request['end_date'] . "'", null, false);
        $this->db->where("o.status", 1);
        $this->db->where("o.archive", 0);
        $org_res = $this->db->get()->row_array();

        $this->db->select("count(os.id) as site_count");
        $this->db->from("tbl_organisation_site as os");
        $this->db->where("date(os.created) BETWEEN '" . $request['start_date'] . "' AND '" . $request['end_date'] . "'", null, false);
        $this->db->where("os.status", 1);
        $this->db->where("os.archive", 0);
        $site_res = $this->db->get()->row_array();

        $res['parent_org_count'] = $org_res['parent_org_count'] ?? 0;
        $res['sub_org_count'] = $org_res['sub_org_count'] ?? 0;
        $res['site_count'] = $site_res['site_count'] ?? 0;

        return $res;
    }

}
