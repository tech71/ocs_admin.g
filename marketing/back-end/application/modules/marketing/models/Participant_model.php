<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function participant_specific_count($reqType) {
        $participant_res = $this->get_onboard_or_participant_count($reqType);

        $resigned_participant = $this->get_participant_resigned_count($reqType);


        $result['participant_resigned_count_sp'] = stats_percentage_calculation($resigned_participant['resigned_participant'], $resigned_participant['resigned_participant_pervious']);

//        last_query();
        // count of participant who onboareded according month and year
        $result['onboarded_participant_count_sp'] = stats_percentage_calculation($participant_res['onboard_participant'], $participant_res['onboard_participant_previous']);

        // count of iactive participant according month and year
        $result['inactive_participant_count_sp'] = stats_percentage_calculation($participant_res['inactive_participant'], $participant_res['inactive_participant_previous']);

        // count of active participant according to month and year
        $result['active_participant_count_sp'] = stats_percentage_calculation($participant_res['active_participant'], $participant_res['active_participant_previous']);

        // count of active with shoft according month and year
        $result['active_with_shift_count_sp'] = stats_percentage_calculation($participant_res['active_with_shift'], $participant_res['active_with_shift_previous']);

        // avg participant who onboard
        $x = $participant_res['onboard_participant'] ? number_format(($participant_res['onboard_participant'] / $reqType['curr_max_days']), 2, '.', '') : 0;
        $y = $participant_res['onboard_participant_previous'] ? number_format(($participant_res['onboard_participant_previous'] / $reqType['prev_max_days']), 2, '.', '') : 0;
        $result['avg_onboarded_participant_count_sp'] = stats_percentage_calculation($x, $y);

        $result['participant_count_title'] = $reqType['participant_count_title'];
        $result['hot_cold_place_title'] = $reqType['hot_cold_place_title'];


        // onboard member cold spot place count and city name
        $result['onboard_cold_place'] = $this->get_participant_and_city_hot_and_cold_spot('asc', $reqType['start_date'], $reqType['end_date']);

        // onboard member hot spot place count and city name
        $result['onboard_hot_place'] = $this->get_participant_and_city_hot_and_cold_spot('desc', $reqType['start_date'], $reqType['end_date']);

        // filled shift hot spot place count and city name
        $result['fill_shift_hot_place'] = $this->get_fill_shift_city_hot_and_cold_spot('asc', $reqType['start_date'], $reqType['end_date']);

        // filled shift cold spot place count and city name
        $result['fill_shift_cold_place'] = $this->get_fill_shift_city_hot_and_cold_spot('desc', $reqType['start_date'], $reqType['end_date']);

        return $result;
    }

    function get_onboard_or_participant_count($session) {
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' THEN 1  ELSE '0' END) as onboard_participant", false);
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' THEN 1  ELSE '0' END) as onboard_participant_previous", false);
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "' AND p.status = 1 AND p.archive = 0 THEN 1  ELSE '0' END) as active_participant", false);
        $this->db->select("sum(CASE WHEN date(p.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' AND p.status = 1 AND p.archive = 0 THEN 1  ELSE '0' END) as active_participant_previous", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_user_active_inactive_history sub_history where (date(sub_history.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "') AND sub_history.id = p.id AND user_type = 2 AND action_type = 2  limit 1) 
THEN 1  ELSE '0' END) as inactive_participant", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_user_active_inactive_history sub_history where (date(sub_history.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "') AND sub_history.id = p.id AND user_type = 2 AND action_type = 2  limit 1) 
THEN 1  ELSE '0' END) as inactive_participant_previous", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_shift as sub_s INNER JOIN tbl_shift_participant as sub_sp ON sub_sp.shiftId = sub_s.id where sub_sp.participantId = p.id AND (date(sub_s.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "') AND sub_s.status = 6 AND sub_s.booked_by IN (2,3) limit 1) 
THEN 1  ELSE '0' END) as active_with_shift", false);

        $this->db->select("sum(CASE WHEN
(select id from tbl_shift as sub_s INNER JOIN tbl_shift_participant as sub_sp ON sub_sp.shiftId = sub_s.id where sub_sp.participantId = p.id AND (date(sub_s.created) BETWEEN '" . $session['previous_start_date'] . "' AND '" . $session['previous_end_date'] . "' ) AND sub_s.status = 6 AND sub_s.booked_by IN (2,3) limit 1) 
THEN 1  ELSE '0' END) as active_with_shift_previous
", false);

        $this->db->from('tbl_participant as p');
        $res = $this->db->get()->row_array();
//        last_query();
        $res['onboard_participant'] = $res['onboard_participant'] ?? 0;
        $res['onboard_participant_previous'] = $res['onboard_participant_previous'] ?? 0;
        $res['active_participant'] = $res['active_participant'] ?? 0;
        $res['active_participant_previous'] = $res['active_participant_previous'] ?? 0;
        $res['inactive_participant'] = $res['inactive_participant'] ?? 0;
        $res['inactive_participant_previous'] = $res['inactive_participant_previous'] ?? 0;
        $res['active_with_shift'] = $res['active_with_shift'] ?? 0;
        $res['active_with_shift_previous'] = $res['active_with_shift_previous'] ?? 0;

        return $res;
    }

    function get_session_according_to_request($request) {
        $view_type = isset($request->view_type) ? $request->view_type : 'year';

        $return = [];
        $previous_year = date('Y', strtotime('-1 year'));
        if ($view_type == 'year') {
            $start_date = $finicial_start_date = date('Y') . '-07-01';
            $end_date = $finicial_end_date = date('Y', strtotime('+1 year')) . '-06-30';

            $previous_start_date = $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_end_date = $previous_finicial_end_date = date('Y') . '-06-30';

            $curr_maxdays = dateDiffInDays($start_date, date('Y-m-d'));
            $prev_maxdays = dateDiffInDays($previous_start_date, $previous_end_date);

            $return['participant_count_title'] = [
                'current' => DateFormate($start_date, "Y") . '-' . DateFormate($end_date, "Y"),
                'previous' => DateFormate($previous_start_date, "Y") . '-' . DateFormate($previous_end_date, "Y"),
                'percentage' => 'previous year'
            ];
            $return['hot_cold_place_title'] = "This year's";
        } elseif ($view_type == 'current_month') {

            $previous_start_date = date("$previous_year-m-01");
            $previous_end_date = date("$previous_year-m-t");

            $curr_maxdays = date('d');
            $prev_maxdays = date("j", strtotime("last day of previous month"));

            $start_date = $first_day_this_month = date('Y-m-01');
            $end_date = $last_day_this_month = date('Y-m-t');
            $return['participant_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
            $return['hot_cold_place_title'] = "This month's";
        } elseif ($view_type == 'selected_month' && !empty($request->specific_month)) {
            $x = explode('-', $request->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];


            $start_date = date("$year-$month-01");
            $end_date = date("$year-$month-t");

            $previous_start_date = date("$previous_year-$month-01");
            $previous_end_date = date("$previous_year-$month-t");

            $curr_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $prev_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $previous_year);
            $return['hot_cold_place_title'] = DateFormate($start_date, "M Y");
            $return['participant_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
        }

        $return['start_date'] = $start_date;
        $return['end_date'] = $end_date;
        $return['previous_start_date'] = $previous_start_date;
        $return['previous_end_date'] = $previous_end_date;

        $return['curr_max_days'] = (int) $curr_maxdays;
        $return['prev_max_days'] = (int) $prev_maxdays;

        return $return;
    }

    function get_participant_resigned_count($session) {

        // session mean first query for current year or month wise count

        $this->db->select('count(p.id) as resigned_participant');
        $this->db->from('tbl_participant as p');
        $this->db->join("tbl_user_active_inactive_history as ai_history", "ai_history.userId = p.id AND user_type = 2 AND action_type = 2", "INNER");
        $this->db->join("tbl_participant_email as pe", "pe.participantId = p.id AND pe.primary_email = 1", "INNER");

        $this->db->join("tbl_crm_participant as cp", "cp.firstname = p.firstname AND cp.middlename = p.middlename AND cp.lastname = p.lastname", "INNER");
        $this->db->join("tbl_crm_participant_email as cpe", "cpe.crm_participant_id = cp.id AND cpe.email = pe.email AND cpe.primary_email = 1", "INNER");
        $this->db->where("date(ai_history.created) < date(cp.created) AND (date(cp.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "')", null, false);

        $res1 = $this->db->get()->row_array();


        //second query for compare to current

        $this->db->select('count(p.id)  as resigned_participant_pervious');
        $this->db->from('tbl_participant as p');
        $this->db->join("tbl_user_active_inactive_history as ai_history", "ai_history.userId = p.id AND user_type = 2 AND action_type = 2", "INNER");
        $this->db->join("tbl_participant_email as pe", "pe.participantId = p.id AND pe.primary_email = 1", "INNER");

        $this->db->join("tbl_crm_participant as cp", "cp.firstname = p.firstname AND cp.middlename = p.middlename AND cp.lastname = p.lastname", "INNER");
        $this->db->join("tbl_crm_participant_email as cpe", "cpe.crm_participant_id = cp.id AND cpe.email = pe.email AND cpe.primary_email = 1", "INNER");
        $this->db->where("date(ai_history.created) < date(cp.created) AND (date(cp.created) BETWEEN '" . $session['start_date'] . "' AND '" . $session['end_date'] . "')", null, false);

        $res2 = $this->db->get()->row_array();

        $response['resigned_participant'] = $res1['resigned_participant'] ?? 0;
        $response['resigned_participant_pervious'] = $res2['resigned_participant_pervious'] ?? 0;

        return $response;
    }

    function participant_intake_droplate_graph($reqData) {
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        if (date('m') <= 6) {//Upto June 2014-2015
            $start_year = $financial_start = (date('Y') - 1);
            $end_year = $financial_end = (int) date('Y');
        } else {//After June 2015-2016
            $start_year = $financial_start = date('Y');
            $end_year = $financial_end = (int) (date('Y') + 1);
        }

        $previous_active_year = (date('Y') - 1);
        $current_active_year = date('Y');
        $curr_prev_year = [date('Y'), (date('Y') - 1)];

        $this->db->select(["cs.parent_id as id", "count(case when (cs.id = 1 AND cs.parent_id = 0) OR (cs.parent_id != 0) THEN cp.id end) as count"]);
        $this->db->from("tbl_crm_participant as cp");

        $this->db->join("tbl_crm_participant_stage as cps", "cps.crm_participant_id = cp.id AND cps.status = 3", "INNER");
        $this->db->join("tbl_crm_stage as cs", "cs.id = cps.stage_id AND cs.status = 1", "INNER");

        if ($view_type == 'current_month') {


            $this->db->where('MONTH(cp.created) = MONTH(CURRENT_DATE())', null, false);
            $this->db->where_in('year(cp.created)', $curr_prev_year);
            $this->db->group_by('cs.parent_id, year(cp.created)');
        } elseif ($view_type == 'selected_month' && !empty($reqData->specific_month)) {
            $x = explode('-', $reqData->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];

            $current_active_year = $year;
            $curr_prev_year = ($year - 1);

            $search_years = [$year, ($year - 1)];
            $this->db->where("MONTH(cp.created) = (" . addslashes($month) . ")", null, false);
            $this->db->where_in('year(cp.created)', $search_years);

            $this->db->group_by('cs.parent_id, year(cp.created)');
        } else {
            $current_active_year = $financial_start . '-' . $financial_end;
            $financial_start = $financial_start . '-07-01';
            $financial_end = $financial_end . '-06-30';
            $previous_year = date('Y', strtotime('-1 year'));

            $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_finicial_end_date = date('Y') . '-06-30';


            $this->db->select("CASE WHEN MONTH(cp.created) >= 7 THEN concat(YEAR(cp.created), '-',YEAR(cp.created)+1) ELSE concat(YEAR(cp.created)-1,'-', YEAR(cp.created)) END AS financial_year");
            $this->db->where("date(cp.created) BETWEEN '" . $previous_finicial_start_date . "' AND '" . $financial_end . "'", null, false);
            $this->db->group_by('financial_year, cs.parent_id');
            $this->db->order_by('cp.created');
        }

        $this->db->order_by('cs.parent_id');

        $result = $this->db->get()->result_array();



        $data = ['row1' => [], 'row2' => []];
        if (!empty($result)) {
            foreach ($result as $val) {
                $val['id'] = ($val['id'] == 0) ? 1 : $val['id'];
                $difference_key = ($view_type == 'current_month' || $view_type == 'selected_month') ? 'year' : 'financial_year';

                if ($val[$difference_key] == $current_active_year) {
                    $data['row1'][$val['id']] = $val['count'];
                } else {
                    $data['row2'][$val['id']] = $val['count'];
                }
            }
        }


        $graph_data = [];
        $recruitment_stage = $this->get_crm_stage();
        if (!empty($recruitment_stage)) {
            foreach ($recruitment_stage as $val) {
                $graph_data['row1'][] = isset($data['row1'][$val->id]) ? $data['row1'][$val->id] : 0;
                $graph_data['row2'][] = isset($data['row2'][$val->id]) ? $data['row2'][$val->id] : 0;
            }
        }

        return $graph_data;
    }

    function get_crm_stage() {
        return $this->basic_model->get_record_where('crm_stage', ['id'], ['status' => 1, 'parent_id' => 0]);
    }

    function member_count_by_postcode_subquery($selected_member_type) {
        $this->db->flush_cache();
        $this->db->select(['count(m.id)']);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_address as ma', 'ma.memberId = m.id AND m.status = 1 AND m.archive = 0');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $this->db->where('ma.postal = ss.postcode', null, false);

        if ($selected_member_type === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created >= '" . $previous_month . "'", null, false);
        } elseif ($selected_member_type === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created < '" . $previous_month . "'", null, false);
        } elseif ($selected_member_type === 'active_shift') {

            $this->db->join('tbl_shift_member as sm', 'sm.memberId = m.id AND sm.status = 3', 'INNER');
            $this->db->join('tbl_shift as s', 'sm.shiftId = s.id AND s.status = 7', 'INNER');


            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
        } elseif ($selected_member_type === 'without_shift') {

            $this->db->where("select sub_sm.memberId from tbl_shift_member as sub_sm INNER join tbl_shift as sub_s ON sub_s.id = sub_sm.shiftId AND sub_s.status = 7 AND '" . DATE_TIME . "' BETWEEN start_time and end_time where sub_sm.status = 3 AND sub_sm.memberId = m.id))", null, false);
        }

        return $this->db->get_compiled_select();
    }

    function participant_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {

            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created >= '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created < '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'active_shift') {
            $this->db->join('tbl_shift_participant as sp', 'sp.shiftId = s.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');

            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'inactive_and_not_archived') {
            $this->db->where("p.status", 0);
            $this->db->where("p.archive", 0);
        }

        $this->db->select(['count(pa.participantId) as s_count']);
        $this->db->from('tbl_participant as p');
        $this->db->join('tbl_participant_address as pa', 'p.id = pa.participantId AND p.archive = 0', 'INNER');
        $this->db->where('pa.postal = ss.postcode', null, false);

        return $this->db->get_compiled_select();
    }

    function shift_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'request_by_particpant') {
            $this->db->where('s.status', 1);
        } elseif ($member_compare_with_specific === 'filled_by_member') {
            $this->db->where('s.status', 7);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_member') {

            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'member'", 'INNER');
            $this->db->where('s.status', 5);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_participant') {
            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'participant'", 'INNER');
            $this->db->where('s.status', 5);
        }

        $this->db->select(['count(s.id) as s_count']);
        $this->db->join('tbl_shift_location as sl', 'sl.postal = ma.postal AND s.id = sl.shiftId', 'INNER');
        $this->db->from('tbl_shift as s', 's.id = sl.shiftId');
        $this->db->where('sl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function fms_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'rep_by_participant') {
            $this->db->where('fc.initiated_type', 2);
        } elseif ($member_compare_with_specific === 'rep_by_member') {
            $this->db->where('fc.initiated_type', 1);
        } elseif ($member_compare_with_specific === 'rep_by_org') {
            $this->db->where('fc.initiated_type', 3);
        }

        $this->db->select(['count(fc.id) as s_count']);
        $this->db->join('tbl_fms_case_location as fcl', 'fc.id = fcl.caseId', 'INNER');
        $this->db->from('tbl_fms_case as fc');

        $this->db->where('fcl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function org_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        $this->db->select(['count(o.id) as s_count']);
        $this->db->join('tbl_organisation_address as oa', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');
        $this->db->from('tbl_organisation as o', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');

        $orgId = $member_compare_with_specific->value ?? 0;
        if ($orgId > 0) {
            $this->db->where('o.id', $orgId);
        }

        $this->db->where('oa.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function get_participant_by_postcode($reqData) {
        $selected_participant_type = (!empty($reqData->selected_participant_type)) ? $reqData->selected_participant_type : 'new';
        $participant_compare_with = (!empty($reqData->participant_compare_with)) ? $reqData->participant_compare_with : 'participant';
        $participant_compare_with_specific = (!empty($reqData->participant_compare_with_specific)) ? $reqData->participant_compare_with_specific : '';

        $member_sub_query = $this->participant_count_by_postcode_subquery($selected_participant_type);

        $sub_query2 = '';
        if ($participant_compare_with === 'member') {
            $sub_query2 = $this->member_count_by_postcode_subquery($participant_compare_with_specific);
        } elseif ($participant_compare_with === 'participant') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($participant_compare_with_specific);
        } elseif ($participant_compare_with === 'shift') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($participant_compare_with_specific);
        } elseif ($participant_compare_with === "fms") {
            $sub_query2 = $this->fms_count_by_postcode_subquery($participant_compare_with_specific);
        } elseif ($participant_compare_with === "org") {
            $sub_query2 = $this->org_count_by_postcode_subquery($participant_compare_with_specific);
        } elseif ($participant_compare_with === "working_area") {
            
        }

        $this->db->flush_cache();
        $this->db->select(['ss.longitude as lng', 'ss.latitude as lat']);
        $this->db->from('tbl_suburb_state as ss');
        $this->db->select('(' . $member_sub_query . ') as f_count', false);

        if ($sub_query2) {
            $this->db->select('(' . $sub_query2 . ') as s_count');
        } else {
            $this->db->select('"0" as s_count');
        }

        $this->db->group_by('ss.postcode');
        $this->db->having('f_count > 0 || s_count > 0', null, false);

        $this->db->limit('15');
        $result = $this->db->get()->result();

        return $result;
    }

    function get_participant_and_city_hot_and_cold_spot($order, $start_date, $end_date) {
        $this->db->select(["count(p.id) as count", "pa.city"], false);
        $this->db->from('tbl_participant as p');
        $this->db->join('tbl_participant_address as pa', 'pa.participantId = p.id', "INNER");

        $this->db->where("date(p.created) BETWEEN '$start_date' AND '$end_date'");
        $this->db->where("pa.city != ''", null, false);
        $this->db->group_by("pa.city");
        $this->db->order_by("count", $order);
        $this->db->limit('3');

        return $this->db->get()->result();
    }

    function get_fill_shift_city_hot_and_cold_spot($order, $start_date, $end_date) {
        $this->db->select(['count(s.id) as count', "sl.suburb as city"]);
        $this->db->from('tbl_shift as s');
        $this->db->join('tbl_shift_location as sl', 'sl.shiftId = s.id');
        $this->db->where("date(s.created) BETWEEN '$start_date' AND '$end_date'");
        $this->db->where('s.status', 7);
        $this->db->where('sl.suburb != ""', null, false);
        $this->db->group_by("sl.suburb");
        $this->db->order_by("count", $order);
        $this->db->limit('3');

        return $this->db->get()->result();
    }

    function get_participant_shift_graph_data($reqData) {
        $graph_title = '';
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        if (date('m') <= 6) {//Upto June 2014-2015
            $start_year = $financial_start = (date('Y') - 1);
            $end_year = $financial_end = (int) date('Y');
        } else {//After June 2015-2016
            $start_year = $financial_start = date('Y');
            $end_year = $financial_end = (int) (date('Y') + 1);
        }

        $financial_start = DateFormate($financial_start . '-07-01', 'Y-m-d');
        $financial_end = DateFormate($financial_end . '-06-30', 'Y-m-d');


        $this->db->select("count(CASE when status = 7 THEN 1  end) as total_filled_shift", false);
        $this->db->select("count(CASE when status = 1 AND created_by = 0 AND funding_type = 3 THEN 1 end) as total_welfare_unfilled_shift", false);
        $this->db->select("count(CASE when status = 1 AND created_by = 0 AND funding_type = 1 THEN 1 end) as total_ndis_unfilled_shift", false);
        $this->db->select("sum(CASE when status = 7 THEN (TIME_TO_SEC(TIMEDIFF(end_time, start_time))/60/60 ) else 0 end) as filled_shift_sum", false);
        $this->db->select("(year(start_time)) as year", false);
        $this->db->select("(month(start_time)) as month", false);


        if ($view_type == 'current_month' || $view_type == 'selected_month') {
            $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
            $this->db->select("(day(start_time)) as current_term", false);
            $this->db->select("((start_time)) as date", false);
        } else {
            $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
            $this->db->select("(month(start_time)) as current_term", false);
        }

        $this->db->from('tbl_shift');

        if ($view_type == 'current_month') {

            $this->db->where('MONTH(start_time) = MONTH(CURRENT_DATE())', null, false);

            $this->db->group_by('date(start_time)');
        } elseif ($view_type == 'selected_month' && !empty($reqData->specific_month)) {
            $x = explode('-', $reqData->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];
            $this->db->where("MONTH(start_time) = (" . addslashes($month) . ")", null, false);
            $this->db->where("YEAR(start_time) = (" . addslashes($year) . ")", null, false);

            $this->db->group_by('date(start_time)');
        } else {
            $this->db->where("date(start_time) BETWEEN '" . $financial_start . "' AND '" . $financial_end . "'", null, false);
            $this->db->group_by('month(start_time)');
            $this->db->order_by('start_time');
        }

        $result = $this->db->get()->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $val->filled_shift_sum = round($val->filled_shift_sum, 2);

                $x = round(@($val->filled_shift_sum / $val->total_filled_shift));
                $val->filled_shift_avg = is_nan($x) ? 0 : $x;
            }
        }

        $result_arr = [];
        if ($view_type == 'current_month' || $view_type == 'selected_month') {
            if ($view_type == 'current_month') {
                $graph_title = DateFormate(DATE_TIME, 'F Y');
                $months_date = getMonthAllDate(DATE_TIME);
            } else {

                $date = date("Y-m-d", strtotime($year . '-' . $month, '-1'));
                $graph_title = DateFormate($date, 'F Y');
                $months_date = getMonthAllDate($date);
            }

            $result_arr = obj_to_arr($result);
            $result_arr = pos_index_change_array_data($result_arr, 'date');

            if (!empty($months_date)) {
                foreach ($months_date as $val) {

                    if (empty($result_arr[$val])) {
                        $result_arr[$val] = [
                            'total_welfare_unfilled_shift' => 0,
                            'total_ndis_unfilled_shift' => 0,
                            'filled_shift_sum' => 0,
                            'filled_shift_avg' => 0,
                            'total_filled_shift' => 0,
                            'current_term' => ((int) DateFormate($val, 'd')) . get_specific_number_term(DateFormate($val, 'd')),
                            'date' => $val,
                            'custome' => true];
                    } else {
                        $result_arr[$val]['current_term'] = ((int) DateFormate($val, 'd')) . get_specific_number_term(DateFormate($val, 'd'));
                    }
                }
            }

            ksort($result_arr);
            $result_arr = array_values($result_arr);
        } elseif ($view_type == 'year') {
            $result_arr = obj_to_arr((array) $result);
            $result_arr = pos_index_change_array_data($result_arr, 'month');

            $start_month = 7;
            $end_month = 6;
            $new_special_order_year_result = [];
            $graph_title = $start_year . ' - ' . $end_year;

            $i = 0;
            for ($start_month; $i < 13; $start_month++) {

                if (empty($result_arr[$start_month])) {
                    $new_special_order_year_result[$start_month] = [
                        'total_welfare_unfilled_shift' => 0,
                        'total_ndis_unfilled_shift' => 0,
                        'filled_shift_sum' => 0,
                        'filled_shift_avg' => 0,
                        'total_filled_shift' => 0,
                        'current_term' => ((int) ($start_month)) . ' - ' . get_month_name($start_month),
                        'month' => $start_month,
                        'custome' => true];
                } else {
                    $result_arr[$start_month]['current_term'] = ((int) ($start_month)) . ' - ' . get_month_name($start_month);
                    $new_special_order_year_result[$start_month] = $result_arr[$start_month];
                }

                if ($start_month == 12) {
                    $start_month = 1;

                    $start_year++;
                }

                if ($start_month == $end_month && $start_year == $end_year) {
                    break;
                }
                $i++;
            }

            $result_arr = array_values($new_special_order_year_result);
        }

        return ['graph_shifts' => $result_arr, 'shift_graph_title' => $graph_title];
    }

    function comparison_participant_by_funding_type($reqData) {
        $this->db->select(["count(p.id) as count", "pp.funding_type"]);
        $this->db->from("tbl_participant as p");
        $this->db->join("tbl_participant_plan as pp", "pp.participantId = p.id", "INNER");
        $this->db->where("(date(pp.start_date) BETWEEN '" . $reqData['start_date'] . "' AND '" . $reqData['end_date'] . "') OR (date(pp.end_date) BETWEEN '" . $reqData['start_date'] . "' AND '" . $reqData['end_date'] . "')");
        $this->db->where("p.status", 1);
        $this->db->where("p.archive", 0);
        $this->db->group_by("pp.funding_type");

        $result = $this->db->get()->result_array();
        $res = array_column($result, 'count', 'funding_type');

        $return = ['private' => $res[2] ?? 0, 'ndis' => $res[1] ?? 0, 'welfare' => $res[3] ?? 0];
        return $return;
    }

}
