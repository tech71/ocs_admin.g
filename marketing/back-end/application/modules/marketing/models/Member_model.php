<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_member_shift_graph_data($reqData) {
        $graph_title = '';
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        if (date('m') <= 6) {//Upto June 2014-2015
            $start_year = $financial_start = (date('Y') - 1);
            $end_year = $financial_end = (int) date('Y');
        } else {//After June 2015-2016
            $start_year = $financial_start = date('Y');
            $end_year = $financial_end = (int) (date('Y') + 1);
        }

        $financial_start = DateFormate($financial_start . '-07-01', 'Y-m-d');
        $financial_end = DateFormate($financial_end . '-06-30', 'Y-m-d');


        $this->db->select("CASE when status = 7 THEN count(id) else 0 end as total_filled_shift", false);
        $this->db->select("CASE when status = 5 THEN count(id) else 0 end as total_cancel_shift", false);

        $this->db->select("CASE when status = 1 AND day(start_time)< day(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);

        $this->db->select("CASE when status = 7 THEN sum(TIME_TO_SEC(TIMEDIFF(end_time, start_time))/60/60 ) else 0 end as filled_shift_sum", false);
        $this->db->select("CASE when status = 7 THEN  avg(TIME_TO_SEC(TIMEDIFF(end_time, start_time))/60/60 ) else 0 end as filled_shift_avg", false);

        if ($view_type == 'current_month' || $view_type == 'selected_month') {
            $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
            $this->db->select("(day(start_time)) as current_term", false);
            $this->db->select("((start_time)) as date", false);
        } else {
            $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
            $this->db->select("(month(start_time)) as current_term", false);
            $this->db->select("(month(start_time)) as month", false);
        }

        $this->db->from('tbl_shift');

        if ($view_type == 'current_month') {

            $this->db->where('MONTH(start_time) = MONTH(CURRENT_DATE())', null, false);

            $this->db->group_by('date(start_time)');
        } elseif ($view_type == 'selected_month' && !empty($reqData->specific_month)) {
            $x = explode('-', $reqData->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];
            $this->db->where("MONTH(start_time) = (" . addslashes($month) . ")", null, false);
            $this->db->where("YEAR(start_time) = (" . addslashes($year) . ")", null, false);

            $this->db->group_by('date(start_time)');
        } else {
            $this->db->where("date(start_time) BETWEEN '" . $financial_start . "' AND '" . $financial_end . "'", null, false);
            $this->db->group_by('month(start_time)');
            $this->db->order_by('start_time');
        }

        $result = $this->db->get()->result();

        if (!empty($result)) {
            foreach ($result as $val) {
                $val->filled_shift_sum = round($val->filled_shift_sum, 2);
                $val->filled_shift_avg = round($val->filled_shift_avg, 2);
            }
        }

        $result_arr = [];
        if ($view_type == 'current_month' || $view_type == 'selected_month') {
            if ($view_type == 'current_month') {
                $graph_title = DateFormate(DATE_TIME, 'F Y');
                $months_date = getMonthAllDate(DATE_TIME);
            } else {

                $date = date("Y-m-d", strtotime($year . '-' . $month, '-1'));
                $graph_title = DateFormate($date, 'F Y');
                $months_date = getMonthAllDate($date);
            }

            $result_arr = obj_to_arr($result);
            $result_arr = pos_index_change_array_data($result_arr, 'date');

            if (!empty($months_date)) {
                foreach ($months_date as $val) {

                    if (empty($result_arr[$val])) {
                        $result_arr[$val] = ['total_filled_shift' => 0,
                            'total_cancel_shift' => 0,
                            'total_unfilled_shift' => 0,
                            'filled_shift_sum' => 0,
                            'filled_shift_avg' => 0,
                            'current_term' => ((int) DateFormate($val, 'd')) . get_specific_number_term(DateFormate($val, 'd')),
                            'date' => $val,
                            'custome' => true];
                    } else {
                        $result_arr[$val]['current_term'] = ((int) DateFormate($val, 'd')) . get_specific_number_term(DateFormate($val, 'd'));
                    }
                }
            }

            ksort($result_arr);
            $result_arr = array_values($result_arr);
        } elseif ($view_type == 'year') {
            $result_arr = obj_to_arr($result);
            $result_arr = pos_index_change_array_data($result_arr, 'month');

            $start_month = 7;
            $end_month = 6;
            $new_special_order_year_result = [];
            $graph_title = $start_year . ' - ' . $end_year;

            $i = 0;
            for ($start_month; $i < 13; $start_month++) {

                if (empty($result_arr[$start_month])) {
                    $new_special_order_year_result[$start_month] = ['total_filled_shift' => 0,
                        'total_cancel_shift' => 0,
                        'total_unfilled_shift' => 0,
                        'filled_shift_sum' => 0,
                        'filled_shift_avg' => 0,
                        'current_term' => ((int) ($start_month)) . ' - ' . get_month_name($start_month),
                        'month' => $start_month,
                        'custome' => true];
                } else {
                    $result_arr[$start_month]['current_term'] = ((int) ($start_month)) . ' - ' . get_month_name($start_month);
                    $new_special_order_year_result[$start_month] = $result_arr[$start_month];
                }

                if ($start_month == 12) {
                    $start_month = 1;

                    $start_year++;
                }

                if ($start_month == $end_month && $start_year == $end_year) {
                    break;
                }
                $i++;
            }

            $result_arr = array_values($new_special_order_year_result);
        }

        return ['graph_shifts' => $result_arr, 'shift_graph_title' => $graph_title];
    }

    function dateDiffInDays($date1, $date2) {
        // Calulating the difference in timestamps 
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours 
        // 24 * 60 * 60 = 86400 seconds 
        return abs(round($diff / 86400));
    }

    function get_member_specific_counts($request) {
        $result = [];
        $view_type = isset($request->view_type) ? $request->view_type : 'year';

        $previous_year = date('Y', strtotime('-1 year'));
        if ($view_type == 'year') {
            $start_date = $finicial_start_date = date('Y') . '-07-01';
            $end_date = $finicial_end_date = date('Y', strtotime('+1 year')) . '-06-30';

            $previous_start_date = $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_end_date = $previous_finicial_end_date = date('Y') . '-06-30';

            $curr_maxdays = $this->dateDiffInDays($start_date, date('Y-m-d'));
            $prev_maxdays = $this->dateDiffInDays($previous_start_date, $previous_end_date);

            $result['member_count_title'] = [
                'current' => DateFormate($start_date, "Y") . '-' . DateFormate($end_date, "Y"),
                'previous' => DateFormate($previous_start_date, "Y") . '-' . DateFormate($previous_end_date, "Y"),
                'percentage' => 'previous year'
            ];

            $result['hot_cold_place_title'] = "This year's";
        } elseif ($view_type == 'current_month') {

            $previous_start_date = date("$previous_year-m-01");
            $previous_end_date = date("$previous_year-m-t");

            $curr_maxdays = date('d');
            $prev_maxdays = date("j", strtotime("last day of previous month"));

            $start_date = $first_day_this_month = date('Y-m-01');
            $end_date = $last_day_this_month = date('Y-m-t');

            $result['member_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
            $result['hot_cold_place_title'] = "This month's";
        } elseif ($view_type == 'selected_month' && !empty($request->specific_month)) {
            $x = explode('-', $request->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];


            $start_date = date("$year-$month-01");
            $end_date = date("$year-$month-t");

            $previous_start_date = date("$previous_year-$month-01");
            $previous_end_date = date("$previous_year-$month-t");

            $curr_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $prev_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $previous_year);

            $result['member_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
            $result['hot_cold_place_title'] = DateFormate($start_date, "M Y");
        }

        $curr_maxdays = (int) $curr_maxdays;
        $prev_maxdays = (int) $prev_maxdays;

        $this->db->select("sum(CASE WHEN date(m.created) BETWEEN '$start_date' AND '$end_date' THEN 1  ELSE '0' END) as onboard_member", false);
        $this->db->select("sum(CASE WHEN date(m.created) BETWEEN '$previous_start_date' AND '$previous_end_date' THEN 1  ELSE '0' END) as onboard_member_previous", false);
        $this->db->select("sum(CASE WHEN date(m.created) < '$end_date' AND m.status = 1 AND m.archive = 0 THEN 1  ELSE '0' END) as active_member", false);
        $this->db->select("sum(CASE WHEN date(m.created) < '$previous_end_date'  AND m.archive = 0 AND m.status =1 THEN 1  ELSE '0' END) as active_member_previous", false);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $result_member_data = $this->db->get()->row_array();

        $result_member_data['onboard_member'] = ($result_member_data['onboard_member']) ?? 0;
        $result_member_data['onboard_member_previous'] = ($result_member_data['onboard_member_previous']) ?? 0;
        $result_member_data['active_member'] = ($result_member_data['active_member']) ?? 0;
        $result_member_data['active_member_previous'] = ($result_member_data['active_member_previous']) ?? 0;



        $this->db->select("sum(CASE WHEN date(tbl_shift.start_time) BETWEEN '$start_date' AND '$end_date' AND tbl_shift_member.status=1 AND tbl_shift.status = 2  THEN 1 ELSE '0' END) as member_count_not_accept_shift ", false);
        $this->db->select("sum(CASE WHEN date(tbl_shift.start_time) BETWEEN '$previous_start_date' AND '$previous_end_date' AND tbl_shift_member.status=1 AND tbl_shift.status = 2  THEN 1 ELSE '0' END) as member_count_not_accept_shift_previous", false);
        $this->db->select("sum(CASE WHEN date(tbl_shift.start_time) BETWEEN '$start_date' AND '$end_date' AND tbl_shift_member.status=3  THEN 1 ELSE '0' END) as member_count_accept_shift", false);
        $this->db->select("sum(CASE WHEN date(tbl_shift.start_time) BETWEEN '$previous_start_date' AND '$previous_end_date' AND tbl_shift_member.status=3  THEN 1 ELSE '0' END) as member_count_accept_shift_previous", false);
        $this->db->from('tbl_shift');
        $this->db->join('tbl_shift_member', 'tbl_shift_member.shiftId = tbl_shift.id', 'inner');
        $this->db->where('tbl_shift.status!=', 8);
        $member_shift_info = $this->db->get()->row_array();

        $member_shift_info['member_count_not_accept_shift'] = $member_shift_info['member_count_not_accept_shift'] ?? 0;
        $member_shift_info['member_count_not_accept_shift_previous'] = $member_shift_info['member_count_not_accept_shift_previous'] ?? 0;
        $member_shift_info['member_count_accept_shift'] = $member_shift_info['member_count_accept_shift'] ?? 0;
        $member_shift_info['member_count_accept_shift_previous'] = $member_shift_info['member_count_accept_shift_previous'] ?? 0;

        $current_reapplied = $this->get_re_applied_member_count($start_date, $end_date);
        $previous_reapplied = $this->get_re_applied_member_count($previous_start_date, $previous_end_date);
        $result['re_applied_member_count'] = stats_percentage_calculation($current_reapplied, $previous_reapplied);

        // count of member who onboard according month and year
        $x = $result_member_data['onboard_member'];
        $y = $result_member_data['onboard_member_previous'];
        $result['onboard_member_count'] = stats_percentage_calculation($x, $y);

        // avg of member who onboard according month and year
        $x = $result_member_data['onboard_member'] ? number_format(($result_member_data['onboard_member'] / $curr_maxdays), 2, '.', '') : 0;
        $y = $result_member_data['onboard_member_previous'] ? number_format(($result_member_data['onboard_member_previous'] / $prev_maxdays), 2, '.', '') : 0;
        $result['avg_onboard_member_count'] = stats_percentage_calculation($x, $y);

        // count of active member according to month or year
        $x = $result_member_data['active_member'];
        $y = $result_member_data['active_member_previous'];
        $result['active_member_count'] = stats_percentage_calculation($x, $y);

        // member count wo could not accept shift count
        $x = $member_shift_info['member_count_not_accept_shift'];
        $y = $member_shift_info['member_count_not_accept_shift_previous'];
        $result['count_not_accepted_shift_count'] = stats_percentage_calculation($x, $y);

        // member count of accept shift according to year or month wise
        $x = $member_shift_info['member_count_accept_shift'];
        $y = $member_shift_info['member_count_accept_shift_previous'];
        $result['accepted_shift_count'] = stats_percentage_calculation($x, $y);




        // onboard member cold spot place count and city name
        $result['onboard_cold_place'] = $this->get_member_and_city_hot_and_cold_spot('asc', $start_date, $end_date);

        // onboard member hot spot place count and city name
        $result['onboard_hot_place'] = $this->get_member_and_city_hot_and_cold_spot('desc', $start_date, $end_date);

        // filled shift hot spot place count and city name
        $result['fill_shift_hot_place'] = $this->get_fill_shift_city_hot_and_cold_spot('asc', $start_date, $end_date);

        // filled shift cold spot place count and city name
        $result['fill_shift_cold_place'] = $this->get_fill_shift_city_hot_and_cold_spot('desc', $start_date, $end_date);


        return $result;
    }

    function get_recruitment_stage() {
        return $this->basic_model->get_record_where('recruitment_stage_label', ['id', 'stage_number'], ['archive' => 0]);
    }

    function get_member_intake_droplate_graph($reqData) {
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        if (date('m') <= 6) {//Upto June 2014-2015
            $start_year = $financial_start = (date('Y') - 1);
            $end_year = $financial_end = (int) date('Y');
        } else {//After June 2015-2016
            $start_year = $financial_start = date('Y');
            $end_year = $financial_end = (int) (date('Y') + 1);
        }

        $previous_active_year = (date('Y') - 1);
        $current_active_year = date('Y');
        $curr_prev_year = [date('Y'), (date('Y') - 1)];

        $this->db->select(["rsl.id", "rsl.stage_number", "count(ra.id) as count", "year(ra.created) as year", "date(ra.created) as created"]);
        $this->db->from("tbl_recruitment_applicant as ra");

        $this->db->join("tbl_recruitment_stage as rs", " rs.id = ra.current_stage AND rs.archive = 0", "INNER");
        $this->db->join("tbl_recruitment_stage_label as rsl", "rsl.id = rs.stage_label_id AND rsl.archive = 0", "INNER");

        if ($view_type == 'current_month') {
            $this->db->where('MONTH(ra.created) = MONTH(CURRENT_DATE())', null, false);
            $this->db->where_in('year(ra.created)', $curr_prev_year);
            $this->db->group_by('rsl.id, year(ra.created)');
        } elseif ($view_type == 'selected_month' && !empty($reqData->specific_month)) {
            $x = explode('-', $reqData->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];

            $current_active_year = $year;
            $curr_prev_year = ($year - 1);

            $search_years = [$year, ($year - 1)];
            $this->db->where("MONTH(ra.created) = (" . addslashes($month) . ")", null, false);
            $this->db->where_in('year(ra.created)', $search_years);

            $this->db->group_by('rsl.id, year(ra.created)');
        } else {
            $current_active_year = $financial_start . '-' . $financial_end;
            $financial_start = $financial_start . '-07-01';
            $financial_end = $financial_end . '-06-30';
            $previous_year = date('Y', strtotime('-1 year'));

            $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_finicial_end_date = date('Y') . '-06-30';


            $this->db->select("CASE WHEN MONTH(ra.created) >= 7 THEN concat(YEAR(ra.created), '-',YEAR(ra.created)+1) ELSE concat(YEAR(ra.created)-1,'-', YEAR(ra.created)) END AS financial_year");
            $this->db->where("date(ra.created) BETWEEN '" . $previous_finicial_start_date . "' AND '" . $financial_end . "'", null, false);
            $this->db->group_by('financial_year, rsl.id');
            $this->db->order_by('ra.created');
        }

        $this->db->order_by('rsl.stage_number');

        $result = $this->db->get()->result_array();


        $data = ['row1' => [], 'row2' => []];
        if (!empty($result)) {
            foreach ($result as $val) {

                $difference_key = ($view_type == 'current_month' || $view_type == 'selected_month') ? 'year' : 'financial_year';

                if ($val[$difference_key] == $current_active_year) {
                    $data['row1'][$val['id']] = $val['count'];
                } else {
                    $data['row2'][$val['id']] = $val['count'];
                }
            }
        }


        $graph_data = [];
        $recruitment_stage = $this->get_recruitment_stage();
        if (!empty($recruitment_stage)) {
            foreach ($recruitment_stage as $val) {
                $graph_data['row1'][] = isset($data['row1'][$val->id]) ? $data['row1'][$val->id] : 0;
                $graph_data['row2'][] = isset($data['row2'][$val->id]) ? $data['row2'][$val->id] : 0;
            }
        }

        return ['member_intake_graph' => $graph_data, 'recruitment_stage' => $recruitment_stage];
    }

    function member_count_by_postcode_subquery($selected_member_type) {
        $this->db->flush_cache();
        $this->db->select(['count(m.id)']);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_address as ma', 'ma.memberId = m.id AND m.status = 1 AND m.archive = 0');
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $this->db->where('ma.postal = ss.postcode', null, false);

        if ($selected_member_type === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created >= '" . $previous_month . "'", null, false);
        } elseif ($selected_member_type === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created < '" . $previous_month . "'", null, false);
        } elseif ($selected_member_type === 'active_shift') {

            $this->db->join('tbl_shift_member as sm', 'sm.memberId = m.id AND sm.status = 3', 'INNER');
            $this->db->join('tbl_shift as s', 'sm.shiftId = s.id AND s.status = 7', 'INNER');


            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
        } elseif ($selected_member_type === 'without_shift') {

            $this->db->where("select sub_sm.memberId from tbl_shift_member as sub_sm INNER join tbl_shift as sub_s ON sub_s.id = sub_sm.shiftId AND sub_s.status = 7 AND '" . DATE_TIME . "' BETWEEN start_time and end_time where sub_sm.status = 3 AND sub_sm.memberId = m.id))", null, false);
        }

        return $this->db->get_compiled_select();
    }

    function participant_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {

            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created >= '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created < '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'active_with_at_least_one_shift') {
            $this->db->join('tbl_shift_participant as sp', 'sp.shiftId = s.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');

            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'inactive_and_not_archived') {
            $this->db->where("p.status", 0);
            $this->db->where("p.archive", 0);
        }

        $this->db->select(['count(pa.participantId) as s_count']);
        $this->db->from('tbl_participant as p');
        $this->db->join('tbl_participant_address as pa', 'p.id = pa.participantId AND p.archive = 0', 'INNER');
        $this->db->where('pa.postal = ss.postcode', null, false);

        return $this->db->get_compiled_select();
    }

    function shift_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'request_by_particpant') {
            $this->db->where('s.status', 1);
        } elseif ($member_compare_with_specific === 'filled_by_member') {
            $this->db->where('s.status', 7);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_member') {

            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'member'", 'INNER');
            $this->db->where('s.status', 5);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_participant') {
            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'participant'", 'INNER');
            $this->db->where('s.status', 5);
        }

        $this->db->select(['count(s.id) as s_count']);
        $this->db->join('tbl_shift_location as sl', 'sl.postal = ma.postal AND s.id = sl.shiftId', 'INNER');
        $this->db->from('tbl_shift as s', 's.id = sl.shiftId');
        $this->db->where('sl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function fms_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'rep_by_participant') {
            $this->db->where('fc.initiated_type', 2);
        } elseif ($member_compare_with_specific === 'rep_by_member') {
            $this->db->where('fc.initiated_type', 1);
        } elseif ($member_compare_with_specific === 'rep_by_org') {
            $this->db->where('fc.initiated_type', 3);
        }

        $this->db->select(['count(fc.id) as s_count']);
        $this->db->join('tbl_fms_case_location as fcl', 'fc.id = fcl.caseId', 'INNER');
        $this->db->from('tbl_fms_case as fc');

        $this->db->where('fcl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function org_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        $this->db->select(['count(o.id) as s_count']);
        $this->db->join('tbl_organisation_address as oa', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');
        $this->db->from('tbl_organisation as o', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');

        $orgId = $member_compare_with_specific->value ?? 0;
        if ($orgId > 0) {
            $this->db->where('o.id', $orgId);
        }

        $this->db->where('oa.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function get_member_by_postcode($reqData) {
        $selected_member_type = (!empty($reqData->selected_member_type)) ? $reqData->selected_member_type : 'new';
        $member_compare_with = (!empty($reqData->member_compare_with)) ? $reqData->member_compare_with : 'participant';
        $member_compare_with_specific = (!empty($reqData->member_compare_with_specific)) ? $reqData->member_compare_with_specific : 'new';

        $member_sub_query = $this->member_count_by_postcode_subquery($selected_member_type);

        $sub_query2 = '';
        if ($member_compare_with === 'member') {
            $sub_query2 = $this->member_count_by_postcode_subquery($member_compare_with_specific);
        } elseif ($member_compare_with === 'participant') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($member_compare_with_specific);
        } elseif ($member_compare_with === 'shift') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($member_compare_with_specific);
        } elseif ($member_compare_with === "fms") {
            $sub_query2 = $this->fms_count_by_postcode_subquery($member_compare_with_specific);
        } elseif ($member_compare_with === "org") {
            $sub_query2 = $this->org_count_by_postcode_subquery($member_compare_with_specific);
        } elseif ($member_compare_with === "working_area") {
            
        }

        $this->db->flush_cache();
        $this->db->select(['ss.longitude as lng', 'ss.latitude as lat']);
        $this->db->from('tbl_suburb_state as ss');
        $this->db->select('(' . $member_sub_query . ') as f_count', false);

        if ($sub_query2) {
            $this->db->select('(' . $sub_query2 . ') as s_count');
        } else {
            $this->db->select('"0" as s_count');
        }

        $this->db->group_by('ss.postcode');
        $this->db->having('f_count > 0 || s_count > 0', null, false);

        $this->db->limit('15');
        $result = $this->db->get()->result();
        return $result;
    }

    function get_member_and_city_hot_and_cold_spot($order, $start_date, $end_date) {
        $this->db->select(["count(m.id) as count", "ma.city"], false);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_address as ma', 'ma.memberId = m.id', "INNER");
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $this->db->where("date(m.created) BETWEEN '$start_date' AND '$end_date'");
        $this->db->where("ma.city != ''", null, false);
        $this->db->group_by("ma.city");
        $this->db->order_by("count", $order);
        $this->db->limit('3');

        return $this->db->get()->result();
    }

    function get_fill_shift_city_hot_and_cold_spot($order, $start_date, $end_date) {
        $this->db->select(['count(s.id) as count', "sl.suburb as city"]);
        $this->db->from('tbl_shift as s');
        $this->db->join('tbl_shift_location as sl', 'sl.shiftId = s.id');
        $this->db->where("date(s.created) BETWEEN '$start_date' AND '$end_date'");
        $this->db->where('s.status', 7);
        $this->db->where('sl.suburb != ""', null, false);
        $this->db->group_by("sl.suburb");
        $this->db->order_by("count", $order);
        $this->db->limit('3');

        return $this->db->get()->result();
    }

    function get_session_according_to_request($request) {
        $view_type = isset($request->view_type) ? $request->view_type : 'year';

        $return = [];
        $previous_year = date('Y', strtotime('-1 year'));
        if ($view_type == 'year') {
            $start_date = $finicial_start_date = date('Y') . '-07-01';
            $end_date = $finicial_end_date = date('Y', strtotime('+1 year')) . '-06-30';

            $previous_start_date = $previous_finicial_start_date = $previous_year . '-07-01';
            $previous_end_date = $previous_finicial_end_date = date('Y') . '-06-30';

            $curr_maxdays = $this->dateDiffInDays($start_date, date('Y-m-d'));
            $prev_maxdays = $this->dateDiffInDays($previous_start_date, $previous_end_date);

            $return['dashboard_count_title'] = [
                'current' => DateFormate($start_date, "Y") . '-' . DateFormate($end_date, "Y"),
                'previous' => DateFormate($previous_start_date, "Y") . '-' . DateFormate($previous_end_date, "Y"),
                'percentage' => 'previous year'
            ];
        } elseif ($view_type == 'current_month') {

            $previous_start_date = date("$previous_year-m-01");
            $previous_end_date = date("$previous_year-m-t");

            $curr_maxdays = date('d');
            $prev_maxdays = date("j", strtotime("last day of previous month"));

            $start_date = $first_day_this_month = date('Y-m-01');
            $end_date = $last_day_this_month = date('Y-m-t');
            $return['dashboard_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
        } elseif ($view_type == 'selected_month' && !empty($request->specific_month)) {
            $x = explode('-', $request->specific_month);
            $month = (int) $x[0];
            $year = (int) $x[1];


            $start_date = date("$year-$month-01");
            $end_date = date("$year-$month-t");

            $previous_start_date = date("$previous_year-$month-01");
            $previous_end_date = date("$previous_year-$month-t");

            $curr_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $prev_maxdays = cal_days_in_month(CAL_GREGORIAN, $month, $previous_year);
            $return['dashboard_count_title'] = ['current' => DateFormate($start_date, "M Y"), 'previous' => DateFormate($previous_start_date, "M Y"), 'percentage' => 'previous month last year'];
        }

        $return['start_date'] = $start_date;
        $return['end_date'] = $end_date;
        $return['previous_start_date'] = $previous_start_date;
        $return['previous_end_date'] = $previous_end_date;

        $return['curr_max_days'] = (int) $curr_maxdays;
        $return['prev_max_days'] = (int) $prev_maxdays;

        return $return;
    }

    function get_member_work_area_graph($reqData) {

        $this->db->select(["count(m.id) as count", "mwa.work_area"]);
        $this->db->from("tbl_member as m");

        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $this->db->join("tbl_member_work_area as mwa", "m.id = mwa.memberId AND mwa.work_area IN(1,5) AND mwa.work_status IN(1,2)", "INNER");

        $this->db->where("(date(mwa.created) BETWEEN '" . $reqData['start_date'] . "' AND '" . $reqData['end_date'] . "') OR (date(mwa.updated) BETWEEN '" . $reqData['start_date'] . "' AND '" . $reqData['end_date'] . "')", null, false);

        $this->db->group_by('mwa.work_area');
        $result = $this->db->get()->result_array();

        $res = array_column($result, 'count', 'work_area');


        return ['ndis' => $res[1] ?? 0, 'welfare' => $res[5] ?? 0];
    }

    function get_re_applied_member_count($start_date, $end_date) {
        $this->db->select("count(m.id) as count");
        $this->db->from("tbl_member as m");
        $this->db->join('tbl_department as d', 'd.id = m.department AND d.short_code = "external_staff"', 'inner');
        $this->db->join("tbl_member_email as me", "me.memberId = m.id AND me.primary_email = 1", "INNER");
        $this->db->join("tbl_member_phone as mp", "mp.memberId = m.id AND mp.primary_phone = 1", "INNER");

        $this->db->join("tbl_user_active_inactive_history as ai_history", "ai_history.userId = m.id AND ai_history.action_type = 2 AND ai_history.user_type = 1", "INNER");

        $this->db->join("tbl_recruitment_applicant as ra", "ra.firstname = m.firstname AND ra.middlename = m.middlename AND ra.lastname = m.lastname", "INNER");
        $this->db->join("tbl_recruitment_applicant_phone as rap", "rap.applicant_id = ra.id AND rap.primary_phone = 1 AND rap.phone = mp.phone", "INNER");
        $this->db->join("tbl_recruitment_applicant_email as rae", "rae.applicant_id = ra.id AND rae.primary_email = 1 AND rae.email = me.email", "INNER");

        $this->db->where("date(ai_history.created) < date(ra.date_applide)", null, false);
        $this->db->where("ai_history.id IN (SELECT MAX(id) FROM tbl_user_active_inactive_history GROUP BY userId, user_type)", null, false);
        $this->db->where("NOT EXISTS (select id from tbl_user_active_inactive_history where action_type = 2 AND user_type = 1 and date(created) > date(ai_history.created))", null, false);
        $this->db->where("ra.date_applide BETWEEN '" . $start_date . "' AND '" . $end_date . "'");
        $res = $this->db->get()->row_array();
        $res['count'] = $res['count'] ?? 0;
        return $res['count'];
    }

}
