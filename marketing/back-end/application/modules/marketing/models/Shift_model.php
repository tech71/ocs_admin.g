<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shift_model extends CI_Model 
{
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function get_shift_data($reqData)
    {
        $graph_title = '';
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        $specific_month = isset($reqData->specific_month)?$reqData->specific_month:''; 
        $return_ary = [];
        $return_ary['shift_hour_count'] = $this->get_shift_hours_data($view_type,$specific_month);
        $return_ary['shifts_stats'] = $this->get_member_stats($view_type,$specific_month);
        $return_ary['graph_data'] = $this->shift_graph_data($view_type,$specific_month); 
        $return_ary['server_view_type'] = $this->viewtype_and_details($view_type,$specific_month);
        return $return_ary;
    } 
    
    public function viewtype_and_details($view_type,$specific_month)
    {
        if($view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();
            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];
            $previous_finicial_start_date = $years['previous_finicial_start_date'];
            $previous_finicial_end_date = $years['previous_finicial_end_date'];
            return array('type'=>$view_type,'date'=>date('Y',strtotime($financial_start)).'/'.date('y',strtotime($financial_end)),'previous_year'=>date('Y',strtotime($previous_finicial_start_date)).'/'.date('y',strtotime($previous_finicial_end_date)));
        }
        else
        {
            $months_record = get_current_previous_month($view_type,$specific_month);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];
            $previous_month = $months_record['previous_month'];
            $previous_year = $months_record['previous_year'];
            return array('type'=>'Month','date'=>$current_month.'/'.$current_year,'previous_year'=>$previous_month.'/'.$previous_year);
        }
    }
    
    function get_shift_hours_data($view_type,$specific_month) 
    {
        if($view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();

            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];

            $previous_finicial_start_date = $years['previous_finicial_start_date'];
            $previous_finicial_end_date = $years['previous_finicial_end_date'];

            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$financial_start' AND '$financial_end' AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as shift_filled_this", false);
            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$previous_finicial_start_date' AND '$previous_finicial_end_date' AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as shift_filled_previous", false);

            $this->db->select("AVG(CASE WHEN date(start_time) BETWEEN '$financial_start' AND '$financial_end' AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as avg_shift_filled_this", false);
            $this->db->select("AVG(CASE WHEN date(start_time) BETWEEN '$previous_finicial_start_date' AND '$previous_finicial_end_date' AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as avg_shift_filled_previous", false);

            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$financial_start' AND '$financial_end' AND booked_by = 2 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_by_participant_this", false);

            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$previous_finicial_start_date' AND '$financial_end' AND booked_by = 2 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_by_participant_previous", false);

            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$financial_start' AND '$financial_end' AND created_by = 1 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_via_participant_portal_this", false);

            $this->db->select("SUM(CASE WHEN date(start_time) BETWEEN '$previous_finicial_start_date' AND '$financial_end' AND created_by = 1 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_via_participant_portal_previous", false);
        }else{

            $months_record = get_current_previous_month($view_type,$specific_month);

            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];

            $previous_month = $months_record['previous_month'];
            $previous_year = $months_record['previous_year'];

            $this->db->select("SUM(CASE WHEN month(start_time) = $current_month AND year(start_time) = $current_year AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as shift_filled_this", false);
            $this->db->select("SUM(CASE WHEN month(start_time) = $previous_month AND year(start_time) = $previous_year AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as shift_filled_previous", false);           

            $this->db->select("AVG(CASE WHEN month(start_time) = $current_month AND year(start_time) = $current_year AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as avg_shift_filled_this", false);
            $this->db->select("AVG(CASE WHEN month(start_time) = $previous_month AND year(start_time) = $previous_year AND status = 7 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as avg_shift_filled_previous", false);

            $this->db->select("SUM(CASE WHEN month(start_time) = $current_month AND year(start_time) = $current_year AND booked_by = 2 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_by_participant_this", false);
            $this->db->select("SUM(CASE WHEN month(start_time) = $previous_month AND year(start_time) = $previous_year AND booked_by = 2 THEN HOUR(TIMEDIFF(end_time, start_time)) ELSE '0' END) as hrs_of_shift_requested_by_participant_previous", false);

            $this->db->select("SUM(CASE WHEN month(start_time) = $current_month AND year(start_time) = $current_year AND created_by = 1 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_via_participant_portal_this", false);
            $this->db->select("SUM(CASE WHEN month(start_time) = $previous_month AND year(start_time) = $previous_year AND created_by = 1 THEN HOUR(TIMEDIFF(end_time, start_time))  ELSE '0' END) as hrs_of_shift_requested_via_participant_portal_previous", false);
        }

        $this->db->from('tbl_shift');
        $result = $this->db->get()->row_array(); 
        
        $main_ary = [];
        if(!empty($result))
        {
            $main_ary['shift_filled'] = stats_percentage_calculation($result['shift_filled_this'], $result['shift_filled_previous']);
            $main_ary['avg_shift_filled'] = stats_percentage_calculation(number_format($result['avg_shift_filled_this'],2,'.',''), number_format($result['avg_shift_filled_previous'],2,'.',''));
            $main_ary['shift_requested_by_participant'] = stats_percentage_calculation($result['hrs_of_shift_requested_by_participant_this'], $result['hrs_of_shift_requested_by_participant_previous']);
            $main_ary['shift_requested_via_participant_portal'] = stats_percentage_calculation($result['hrs_of_shift_requested_via_participant_portal_this'], $result['hrs_of_shift_requested_via_participant_portal_previous']);
        } 

        $return_ary = $this->shift_allocated_to_member($view_type,$specific_month);
        if(!empty($return_ary))
        {
            $main_ary['shift_manually_alloted'] = stats_percentage_calculation($return_ary['hrs_of_shift_manually_alloted_this'], $return_ary['hrs_of_shift_manually_alloted_previous']);
        }
        return $main_ary;
    }

    function get_member_stats($view_type,$specific_month)
    { 
        $array = [];       
        if (isset($view_type) && $view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();
            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];
        }
        else
        {
            $months_record = get_current_previous_month($view_type,$specific_month);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.booked_by = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }else{
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.booked_by = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_shift_location as sl');
        $this->db->join('tbl_shift as s', 's.id = sl.shiftId AND s.booked_by = 2', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_hot_spots_shift = $this->db->get();
        $shift_requested_hot_spots = $sql_hot_spots_shift->result_array();
        if(!empty($shift_requested_hot_spots)){
            $array['shift_requested_hot_spots'] = $shift_requested_hot_spots;
        }else{
            $array['shift_requested_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.booked_by = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.booked_by = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_shift_location as sl');
        $this->db->join('tbl_shift as s', 's.id = sl.shiftId AND s.booked_by = 2', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_cold_spots_shift = $this->db->get();
        $shift_requested_cold_spots = $sql_cold_spots_shift->result_array();
        if(!empty($shift_requested_cold_spots)){
            $array['shift_requested_cold_spots'] = $shift_requested_cold_spots;
        }else{
            $array['shift_requested_cold_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 7 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 7 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_shift_location as sl');
        $this->db->join('tbl_shift as s', 's.id = sl.shiftId AND s.status = 7', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_filled_hot_spots_shift = $this->db->get();
        $shift_filled_hot_spots = $sql_filled_hot_spots_shift->result_array();
        if(!empty($shift_filled_hot_spots)){
            $array['shift_filled_hot_spots'] = $shift_filled_hot_spots;
        }else{
            $array['shift_filled_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 7 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 7 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_shift_location as sl');
        $this->db->join('tbl_shift as s', 's.id = sl.shiftId AND s.status = 7', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_filled_cold_spots_shift = $this->db->get();
        $shift_filled_cold_spots = $sql_filled_cold_spots_shift->result_array();
        if(!empty($shift_filled_cold_spots)){
            $array['shift_filled_cold_spots'] = $shift_filled_cold_spots;
        }else{
            $array['shift_filled_cold_spots'] = [];
        }
        return $array;
    }

    function shift_allocated_to_member($view_type,$specific_month)
    {
        if($view_type == 'year')
        {
           $years = get_current_n_previous_financial_year();

           $financial_start = $years['financial_start'];
           $financial_end = $years['financial_end'];

           $previous_finicial_start_date = $years['previous_finicial_start_date'];
           $previous_finicial_end_date = $years['previous_finicial_end_date'];

           $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.created_by = 1 THEN HOUR(TIMEDIFF(s.end_time, s.start_time))  ELSE 0 END) as hrs_of_shift_manually_alloted_this", false);

           $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$previous_finicial_start_date' AND '$financial_end' AND s.booked_by = 2 THEN HOUR(TIMEDIFF(s.end_time, s.start_time))  ELSE 0 END) as hrs_of_shift_manually_alloted_previous", false);
           $this->db->join('tbl_shift_member', 'tbl_shift_member.shiftId = s.id AND tbl_shift_member.assign_type=1', 'inner');
           $this->db->from('tbl_shift as s');
           $result = $this->db->get()->row_array();
           return $result;
        }
        else
       {
            $months_record = get_current_previous_month($view_type,$specific_month);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];

            $previous_month = $months_record['previous_month'];
            $previous_year = $months_record['previous_year'];

            $this->db->select("SUM(CASE WHEN month(start_time) = $current_month AND year(start_time) = $current_year AND s.created_by = 1 THEN HOUR(TIMEDIFF(s.end_time, s.start_time))  ELSE 0 END) as hrs_of_shift_manually_alloted_this", false);

            $this->db->select("SUM(CASE WHEN month(start_time) = $previous_month AND year(start_time) = $previous_year AND s.booked_by = 2 THEN HOUR(TIMEDIFF(s.end_time, s.start_time))  ELSE 0 END) as hrs_of_shift_manually_alloted_previous", false);
            $this->db->join('tbl_shift_member', 'tbl_shift_member.shiftId = s.id AND tbl_shift_member.assign_type=1', 'inner');
            $this->db->from('tbl_shift as s');
            $result = $this->db->get()->row_array();
            return $result;
        }
    }

    function shift_graph_data($view_type,$specific_month)
    {
        $graph_constant = [];
        $graph_constant[] = array('Month','Shift Completed','Not filled','Shifts Requested');

        if($view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();
            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];
            $previous_finicial_start_date = $years['previous_finicial_start_date'];
            $previous_finicial_end_date = $years['previous_finicial_end_date'];

            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 2 THEN 1  ELSE 0 END) as shift_requested,DATE_FORMAT(s.start_time,'%M') as month", false);
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 7 THEN 1  ELSE 0 END) as shift_filled", false);
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 1 THEN 1  ELSE 0 END) as shift_unfilled", false);
            $this->db->select("SUM(CASE WHEN date(s.start_time) BETWEEN '$financial_start' AND '$financial_end' AND s.status = 6 THEN 1  ELSE 0 END) as shift_completed", false);

            $this->db->where("date(s.start_time) BETWEEN '$financial_start' AND '$financial_end'");
            $this->db->group_by('month(s.start_time),s.funding_type');
            $this->db->order_by('year(s.start_time),month(s.start_time)');
        }
        else
        {
            $months_record = get_current_previous_month($view_type,$specific_month);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];

            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 2 THEN 1  ELSE 0 END) as shift_requested,DATE_FORMAT(s.start_time,'%D') as month", false);
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 7 THEN 1  ELSE 0 END) as shift_filled", false);
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 1 THEN 1  ELSE 0 END) as shift_unfilled", false);
            $this->db->select("SUM(CASE WHEN month(s.start_time) = $current_month AND year(s.start_time) = $current_year AND s.status = 6 THEN 1  ELSE 0 END) as shift_completed", false);
            $this->db->where(array("month(s.start_time)"=>$current_month,"year(s.start_time)"=>$current_year));
            $this->db->group_by('month(s.start_time),s.funding_type');
            $this->db->order_by('date(s.start_time)');
        }
        $this->db->from('tbl_shift as s');
        $rows = $this->db->get()->result_array();
        #last_query();
        return $sql_hot_spots_shift = !empty($rows)?$rows:[array('month'=>'','shift_completed'=>0,'shift_filled'=>0,'shift_requested'=>0,'shift_unfilled'=>0)];    
    }

    function shift_count_by_key_subquery($selected_shift_type) {
        $this->db->flush_cache();
        $this->db->select(['count(m.id)']);
        $this->db->from('tbl_shift as m');
        $this->db->join('tbl_shift_location as ma', 'ma.shiftId = m.id');

        if ($selected_shift_type === 'participant') {
            $this->db->where(array('booked_by'=>2));
        } elseif ($selected_shift_type === 'member_filled') {
            $this->db->where(array('status'=>7));
        } 

        return $this->db->get_compiled_select();
    }

    function participant_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created >= '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created < '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'active_with_at_least_one_shift') {
            $this->db->join('tbl_shift_participant as sp', 'sp.shiftId = p.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');

            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'inactive_and_not_archived') {
            $this->db->where("p.status", 0);
            $this->db->where("p.archive", 0);
        }
        $this->db->select(['count(pa.participantId) as s_count']);
        $this->db->from('tbl_participant as p');
        $this->db->join('tbl_participant_address as pa', 'p.id = pa.participantId AND p.archive = 0', 'INNER');
        $this->db->where('pa.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function shift_count_by_type_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        $temp = shift_type_interval($member_compare_with_specific);
        $start_time = $temp['start_time'];
        $end_time = $temp['end_time'];
        if ($member_compare_with_specific === 'am' || $member_compare_with_specific === 'ao' || $member_compare_with_specific === 'so' || $member_compare_with_specific === 'pm')
        { 
            $this->db->where(" time('p.start_time') BETWEEN '".$start_time."' and '".$end_time."' ", null, false);
        } 
        elseif ($member_compare_with_specific === 'transferred') {
            #$this->db->where("p.ao", 1);
        }
        $this->db->select(['count(pa.id) as s_count']);
        $this->db->from('tbl_shift as p');
        $this->db->join('tbl_shift_location as pa', 'p.id = pa.shiftId', 'INNER');
        $this->db->where('pa.postal = ss.postcode', null, false);
      
        return $this->db->get_compiled_select();
    }

    function member_count_by_postcode_subquery($member_compare_with_specific) { 
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created >= '" . $previous_month . "'", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created < '" . $previous_month . "'", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'active_shift') {
            $this->db->join('tbl_shift_member as sp', 'sp.memberId = m.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');
            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'without_shift') {
            $this->db->where("m.status", 0);
            $this->db->where("m.archive", 0);
        }
        $this->db->select(['count(ma.memberId) as s_count']);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_address as ma', 'm.id = ma.memberId AND m.archive = 0', 'INNER');
        $this->db->where('ma.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function shift_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'request_by_particpant') {
            $this->db->where('s.status', 1);
        } elseif ($member_compare_with_specific === 'filled_by_member') {
            $this->db->where('s.status', 7);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_member') {

            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'member'", 'INNER');
            $this->db->where('s.status', 5);
        } elseif ($member_compare_with_specific === 'shift_cancel_by_participant') {
            $this->db->join('tbl_shift_cancelled as sc', "sc.shiftId = s.id AND cancel_type = 'participant'", 'INNER');
            $this->db->where('s.status', 5);
        }

        $this->db->select(['count(s.id) as s_count']);
        $this->db->join('tbl_shift_location as sl', 'sl.postal = ma.postal AND s.id = sl.shiftId', 'INNER');
        $this->db->from('tbl_shift as s', 's.id = sl.shiftId');
        $this->db->where('sl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function fms_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'rep_by_participant') {
            $this->db->where('fc.initiated_type', 2);
        } elseif ($member_compare_with_specific === 'rep_by_member') {
            $this->db->where('fc.initiated_type', 1);
        } elseif ($member_compare_with_specific === 'rep_by_org') {
            $this->db->where('fc.initiated_type', 3);
        }

        $this->db->select(['count(fc.id) as s_count']);
        $this->db->join('tbl_fms_case_location as fcl', 'fc.id = fcl.caseId', 'INNER');
        $this->db->from('tbl_fms_case as fc');

        $this->db->where('fcl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function org_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        $this->db->select(['count(o.id) as s_count']);
        $this->db->join('tbl_organisation_address as oa', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');
        $this->db->from('tbl_organisation as o', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');

        $this->db->where('oa.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    function get_shift_by_post_code($reqData) 
    {
        $selected_shift_type = (!empty($reqData->selected_shift_type)) ? $reqData->selected_shift_type : 'participant';
        $compare_against = (!empty($reqData->compare_against)) ? $reqData->compare_against : 'participant';
        $compare_against_2 = (!empty($reqData->compare_against_2)) ? $reqData->compare_against_2 : 'new';

        $member_sub_query = $this->shift_count_by_key_subquery($selected_shift_type);

        $sub_query2 = '';
        if ($compare_against === 'participant') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($compare_against_2);
        } elseif ($compare_against === "member") {
            $sub_query2 = $this->member_count_by_postcode_subquery($compare_against_2);
        } elseif ($compare_against === 'shift') {
            $sub_query2 = $this->shift_count_by_type_postcode_subquery($compare_against_2);
        } elseif ($compare_against === "fms") {
            $sub_query2 = $this->fms_count_by_postcode_subquery($compare_against_2);
        } elseif ($compare_against === "org") {
            $sub_query2 = $this->org_count_by_postcode_subquery($compare_against_2);
        } 
        $this->db->flush_cache();
        $this->db->select(['ss.longitude as lng', 'ss.latitude as lat']);
        $this->db->from('tbl_suburb_state as ss');
        $this->db->select('(' . $member_sub_query . ') as f_count', false);

        if ($sub_query2) {
            $this->db->select('(' . $sub_query2 . ') as s_count');
        } else {
            $this->db->select('"0" as s_count');
        }
        $this->db->group_by('ss.postcode');
        $this->db->having('f_count > 0 || s_count > 0', null, false);
        $this->db->limit('15');
        $result = $this->db->get()->result();
        #  last_query();
        return $result;
    }
}
