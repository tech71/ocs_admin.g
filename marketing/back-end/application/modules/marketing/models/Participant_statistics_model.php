<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Participant_statistics_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function participant_statistics_details($viewType) {
        $data = array();
        $comparisons = array('onboarded', 'no_of_days_average', 'active', 'inactive', 'participant_shift', 'resigned','participant_intake_drop_out','participant_funding');

        foreach ($comparisons as $comparison) {
            switch ($comparison) {
                case 'onboarded':
                    $data[$comparison] = $this->get_onboarded_details($viewType);
                break;
                case 'no_of_days_average':
                    $data[$comparison] = $this->get_no_of_days_average_details($viewType);
                break;
                break;
                case 'active':
                    $data[$comparison] = $this->get_activeorinactive_participant_details($viewType, '1');
                break;
                case 'inactive':
                    $data[$comparison] = $this->get_activeorinactive_participant_details($viewType, '0');
                break;
                case 'participant_shift':
                    $data[$comparison] = $this->get_participant_shift_details($viewType);
                break;
                case 'resigned':
                    $data[$comparison] = $this->get_resigned_details($viewType);
                break;
                case 'participant_intake_drop_out':
                    $data['current'] = $this->get_participant_intake_drop_out_details_this_year($viewType);
                    $data['last'] = $this->get_participant_intake_drop_out_details_last_year($viewType);
                    $data[$comparison] = array_merge($data['current'],$data['last']);
                break;
                case 'participant_funding':
                    $data[$comparison] = $this->get_participant_funding_details($viewType);
                break;
            }
        }
        return $data;
    }
    public function get_onboarded_details($viewType) {
        if ($viewType == 'year') {
            $current = 'YEAR(pr.created) =  YEAR(CURDATE()) ';
            $last = 'YEAR(pr.created) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
        } else {
            $current = 'MONTH(pr.created) = MONTH(CURDATE()) ';
            $last = 'MONTH(pr.created) =  (NOW() - INTERVAL 1 MONTH)';
        }
        $dt_query = $this->db->select('
        COUNT(IF( ' . $current . ', 1, NULL)) as current,
        COUNT(IF( ' . $last . '  , 1, NULL)) as last
        ');
        $this->db->from('tbl_participant as pr');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //echo $this->db->last_query();
        $result = $query->row();
        $percentage_val = '';
        $average = $result->last - $result->current;
        $percentage = 0;
        $color='';  $pr='';
        if ($average != 0) {
            $percentage = $average / $result->current * 100;
            if ($percentage > 0) {
                $color ='green';
                $pr = 'hcm-mg-ie-profit';
                $percentage_val = $percentage . '% increases';
            } else {
                $percentage = 0;
                $color ='red';
                $pr = 'hcm-mg-ie-lost';
                $percentage_val = $percentage . '% decreases';
            }
        }
        $result->percentage = $percentage_val;
        $result->percentage_color = $color;
        $result->percentage_type = $pr;
        return (array)$result;
    }
    public function get_activeorinactive_participant_details($viewType, $status) {
        if ($viewType == 'year') {
            $current = 'YEAR(pr.created) =  YEAR(CURDATE()) && pr.status=' . $status . '';
            $last = 'YEAR(pr.created) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR)) && pr.status=' . $status . '';
        } else {
            $current = 'MONTH(pr.created) = MONTH(CURDATE()) && pr.status=' . $status . '';
            $last = 'MONTH(pr.created) =  (NOW() - INTERVAL 1 MONTH) && pr.status=' . $status . '';
        }
        //$dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $dt_query = $this->db->select('
      COUNT(IF( ' . $current . ', 1, NULL)) as current,
      COUNT(IF( ' . $last . '  , 1, NULL)) as last
      ');
        $this->db->from('tbl_participant as pr');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //echo $this->db->last_query();
        $result = $query->row();
        $percentage_val = '';
        $average = $result->last - $result->current;
        $percentage = 0;
        $color='red';  $pr='hcm-mg-ie-lost'; $percentage_val='0 % decreases';
        if ($average != 0) {
            $percentage = $average / $result->current * 100;
            if ($percentage >= 0) {
              $color ='green';
              $pr = 'hcm-mg-ie-profit';
              $percentage_val = $percentage . '% increases';
            } else {
                $color ='red';
                $percentage = 0;
                $pr = 'hcm-mg-ie-lost';
                $percentage_val = $percentage . '% decreases';
            }
        }
        $result->percentage = $percentage_val;
        $result->percentage_color = $color;
        $result->percentage_type = $pr;
        return (array)$result;
    }
    public function get_no_of_days_average_details($viewType) {
        //crm created date - participant added date = days
        if ($viewType == 'year') {
            $current = 'YEAR(cpr.created) =  YEAR(CURDATE()) ';
            $last = 'YEAR(cpr.created) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
        } else {
            $current = 'MONTH(cpr.created) = MONTH(CURDATE()) ';
            $last = 'MONTH(cpr.created) =  (NOW() - INTERVAL 1 MONTH)';
        }
        $dt_query = $this->db->select('

          COUNT(IF( ' . $current . ', 1, NULL)) as current,
          COUNT(IF( ' . $last . '  , 1, NULL)) as last,
          DATEDIFF(pr.created,cpr.booking_date)  as days,
          ');
        $this->db->from('tbl_crm_participant as cpr');
        $this->db->join('tbl_crm_participant as pr', 'pr.ndis_num = cpr.ndis_num', 'left');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //  echo $this->db->last_query();
        $result = $query->row();
        $percentage_val = '';
        $average = $result->last - $result->current;
        $percentage = 0;
        $color='';  $pr='';
        if ($average != 0) {
            $percentage = $average / $result->current * 100;
            if ($percentage > 0) {
                $color ='green';
                $pr = 'hcm-mg-ie-profit';
                $percentage_val = $percentage . '% increases';
            } else {
                $color ='red';
                $percentage = 0;
                $pr = 'hcm-mg-ie-lost';
                $percentage_val = $percentage . '% decreases';
            }
        }
        $result->percentage = $percentage_val;
        $result->percentage_color = $color;
        $result->percentage_type = $pr;
        return (array)$result;
    }
    public function get_participant_shift_details($viewType) {
        if ($viewType == 'year') {
            $current = 'YEAR(pr.created) =  YEAR(CURDATE()) ';
            $last = 'YEAR(pr.created) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
        } else {
            $current = 'MONTH(pr.created) = MONTH(CURDATE()) ';
            $last = 'MONTH(pr.created) =  (NOW() - INTERVAL 1 MONTH)';
        }
        $dt_query = $this->db->select('
      COUNT(IF( ' . $current . ', 1, NULL)) as current,
      COUNT(IF( ' . $last . '  , 1, NULL)) as last,
      ');
        $this->db->from('tbl_participant as pr');
        $this->db->join('tbl_shift_participant as sf_pr', 'sf_pr.participantId = pr.id AND sf_pr.status = 1', 'left');
        $this->db->join('tbl_shift as sf', 'sf.id = sf_pr.shiftId', 'left');
        $this->db->where_in('sf.status', array(1, 2, 3, 7));
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        $result = $query->row();
        $percentage_val = '';
        $average = $result->last - $result->current;
        $percentage = 0; $pr='';
          $color='';
        if ($average != 0) {
            $percentage = $average / $result->current * 100;
            if ($percentage > 0) {
                $color ='green';
                $pr = 'hcm-mg-ie-profit';
                $percentage_val = $percentage . '% increases';
            } else {
                $color ='red';
                $percentage = 0;
                $pr = 'hcm-mg-ie-lost';
                $percentage_val = $percentage . '% decreases';
            }
        }

        $result->percentage = $percentage_val;
        $result->percentage_color = $color;
          $result->percentage_type = $pr;
        return (array)$result;
    }
    public function get_resigned_details($viewType) {
        if ($viewType == 'year') {
            $current = 'YEAR(pr.created) =  YEAR(CURDATE()) ';
            $last = 'YEAR(pr.created) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
        } else {
            $current = 'MONTH(pr.created) = MONTH(CURDATE()) ';
            $last = 'MONTH(pr.created) =  (NOW() - INTERVAL 1 MONTH)';
        }
        $dt_query = $this->db->select('
        COUNT(IF( ' . $current . ', 1, NULL)) as current,
        COUNT(IF( ' . $last . '  , 1, NULL)) as last
        ');
        $this->db->from('tbl_participant as pr');
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
        //echo $this->db->last_query();
        $result = $query->row();
        $percentage_val = '';
        $average = $result->last - $result->current;
        $percentage = 0; $pr='';
          $color='';
        if ($average != 0) {
            $percentage = $average / $result->current * 100;
            if ($percentage > 0) {
                $color ='green';
                  $pr = 'hcm-mg-ie-profit';
                $percentage_val = $percentage . '% increases';
            } else {
                $percentage = 0;
                  $color ='red';
                    $pr = 'hcm-mg-ie-lost';
                $percentage_val = $percentage . '% decreases';
            }
        }
        $result->percentage = $percentage_val;
        $result->percentage_color = $color;
          $result->percentage_type = $pr;
        return (array)$result;
    }


    public function get_participant_intake_drop_out_details_this_year($viewType){
        if ($viewType == 'year') {
            $current = 'YEAR(tbl_crm_participant.booking_date) =  YEAR(CURDATE()) ';
            //$last = 'YEAR(tbl_crm_participant.booking_date) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
        } else {
            $current = 'MONTH(tbl_crm_participant.booking_date) = MONTH(CURDATE())  AND YEAR(tbl_crm_participant.booking_date) =  YEAR(CURDATE()) ';
          //  $last = 'MONTH(tbl_crm_participant.booking_date) =  (NOW() - INTERVAL 1 MONTH)';
        }


      $src_columns = array('tbl_crm_participant.id', 'tbl_crm_participant.stage_status', 'tbl_crm_stage.parent_id');
      $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $src_columns)), false);
      $this->db->from(TBL_PREFIX . 'crm_participant');
      $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');
      $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'left');
      $this->db->join('tbl_crm_participant_stage', 'tbl_crm_participant_stage.crm_participant_id = tbl_crm_participant.id', 'left');
      $this->db->where($current);
      $this->db->group_by("tbl_crm_stage.level");
      $result = $this->db->get()->result();
      $stage1 = 0; $stage2=0; $stage3 =0;
      $pos = array();
      foreach ($result as $key => $val) {
        if($val->stage_status>=1 && $val->stage_status<2){

           $stage1++;
        } else  if($val->stage_status>=2 && $val->stage_status<=5){

           $stage2++;
        } else if($val->stage_status>=6 && $val->stage_status<=9){

            $stage3++;
        }
      }
      // $pos['stage1']  = $stage1;
      // $pos['stage2'] = $stage2;
      // $pos['stage3'] = $stage3;
      $pos['data1'] = array($stage1,$stage2,$stage3);
    //  $pos['data1Bg'] = array('CT.PARTICIPANTCOLOR8', 'CT.PARTICIPANTCOLOR9', 'CT.PARTICIPANTCOLOR10');
      return $pos;

    }

    public function get_participant_intake_drop_out_details_last_year($viewType){
      if ($viewType == 'year') {
          //$current = 'YEAR(tbl_crm_participant.booking_date) =  YEAR(CURDATE()) ';
          $last = 'YEAR(tbl_crm_participant.booking_date) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
      } else {
          //$current = 'MONTH(tbl_crm_participant.booking_date) = MONTH(CURDATE())  AND YEAR(tbl_crm_participant.booking_date) =  YEAR(CURDATE()) ';
          $last = 'MONTH(tbl_crm_participant.booking_date) =  (NOW() - INTERVAL 1 MONTH) AND YEAR(tbl_crm_participant.booking_date) =  YEAR(CURDATE())';
      }
    $src_columns = array('tbl_crm_participant.id', 'tbl_crm_participant.stage_status', 'tbl_crm_stage.parent_id');
    $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $src_columns)), false);
    $this->db->from(TBL_PREFIX . 'crm_participant');
    $this->db->join('tbl_crm_participant_address', 'tbl_crm_participant_address.crm_participant_id = tbl_crm_participant.id', 'left');
    $this->db->join('tbl_crm_stage', 'tbl_crm_stage.id = tbl_crm_participant.stage_status', 'left');
    $this->db->join('tbl_crm_participant_stage', 'tbl_crm_participant_stage.crm_participant_id = tbl_crm_participant.id', 'left');
    $this->db->where($last);
    $this->db->group_by("tbl_crm_stage.level");
    $result = $this->db->get()->result();
    $stage1 = 0; $stage2=0; $stage3 =0;
    $pos = array();
    foreach ($result as $key => $val) {
      if($val->stage_status>=1 && $val->stage_status<2){

         $stage1++;
      } else  if($val->stage_status>=2 && $val->stage_status<=5){

         $stage2++;
      } else if($val->stage_status>=6 && $val->stage_status<=9){

          $stage3++;
      }
    }
    // $pos['stage1']  = $stage1;
    // $pos['stage2'] = $stage2;
    // $pos['stage3'] = $stage3;
    $pos['data2'] = array($stage1,$stage2,$stage3);
    //$pos['data2Bg'] = array('CT.COLOR3', 'CT.COLOR4', 'CT.COLOR5');
    return $pos;
    }


    public function get_participant_funding_details($viewType){
      if ($viewType == 'year') {
          $thisyear = 'YEAR(pr.created) =  YEAR(CURDATE())';
      } else {
          $thismonth = 'MONTH(pr.created) = MONTH(CURDATE())';
      }
      $dt_query = $this->db->select('
       pr.id
      ');
      $this->db->from('tbl_crm_participant as pr');
      $this->db->group_by("pr.ndis_plan");
      $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
      $result = $query->result();
      $ndis=0;$welfare=0;$private=0;
      $pos=array();
      foreach ($result as $key => $val) {
        if($val->ndis_plan=1){
           $ndis++;
        } else  if($val->ndis_plan=2){
           $welfare++;
        } else if($val->ndis_plan=3){
            $private++;
        }
      }
      // $pos['ndis'] =$ndis;
      // $pos['welfare'] =$welfare;
      // $pos['private'] =$private;
      $pos=  array(
          'data' => array('100', '50', '30'),
        );
      return $pos;
    }
    public function participant_postcode($reqData) {
            $filter = $reqData->filter;
            $attribute = $reqData->attribute;
            $type = $reqData->type;
            $data = array();
            switch ($attribute) {
              case 'participant':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type,'participantId');
                break;
              case 'member':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type,'memberId');
                break;
              case 'shifts':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type,'shiftId');
                break;
              case 'organisation':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type,'organisationId');
                break;
              case 'fms_cases':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type,'caseId');
                break;
              case 'funding_type':
                $data['data'] = $this->get_lat_long($filter,$attribute,$type);
                break;
              default:
                // code...
                break;
            }
            // var_dump($data);
            $res = array(
              array(
              'type' => 'participant',
              'posArr' => $data['data']
            )
            );

            return $res;
        }
        public function get_lat_long($filter,$attribute,$type,$id=null){
          $this->db->select("tbl_".$attribute."_address.postal");
          $this->db->select('COUNT(DATE_SUB(DATE_FORMAT(tbl_'.$attribute.'.created, "%Y-%m-%d"), INTERVAL 1 WEEK )) as allcreatedinthisweek');
          $this->db->from('tbl_'.$attribute.'');
          $this->db->join('tbl_'.$attribute.'_address', 'tbl_'.$attribute.'_address.'.$id.' = tbl_'.$attribute.'.id', 'left');
          $this->db->where('tbl_'.$attribute.'.status=' . $filter);
          $this->db->group_by("tbl_".$attribute."_address.postal");
          $postcode = $this->db->get()->result();
          $src_columns = array('count(tbl_'.$attribute.'.id) as count',
          // 'CASE  WHEN   tbl_crm_stage.stage_id=1 then count(tbl_crm_'.$attribute.'.stage_id) as stage1Count',
          'tbl_'.$attribute.'.id', 'tbl_'.$attribute.'_address.postal', 'tbl_'.$attribute.'_address.lat', 'tbl_'.$attribute.'_address.long');
          $data = array();
          $pos2 = array();

          foreach ($postcode as $val2) {
              $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $src_columns)), false);
              $this->db->select('COUNT('.TBL_PREFIX . ''.$attribute.'.id) as count');
              $this->db->from(TBL_PREFIX . ''.$attribute.'');
              $this->db->join('tbl_'.$attribute.'_address', 'tbl_'.$attribute.'_address.'.$id.' = tbl_'.$attribute.'.id', 'left');
              $this->db->where('tbl_'.$attribute.'_address.postal=' . $val2->postal);

              $query = $this->db->get()->result();
              $data = $query;
              $pos = array();
              foreach ($data as $key => $val) {
                if($val->lat!=NULL){
                  $pos['lat'] = $val->lat;
                  $pos['lng'] = $val->long;
                  $pos['numbers'] =  $val->count;
                  $pos2[] = $pos;}
              }

          }
          return $pos2;
        }

        public function get_participant_shift_days($viewType) {
            if ($viewType == 'year') {
                $current = 'YEAR(pr.created) =  YEAR(CURDATE()) ';
                $last = 'YEAR(pr.created) = YEAR(DATE_SUB(CURDATE(),  INTERVAL 1 YEAR))';
            } else {
                // $current = 'MONTH(pr.created) = MONTH(CURDATE()) ';
                // $last = 'MONTH(pr.created) =  (NOW() - INTERVAL 1 MONTH)';
                $month = 'DATEDIFF(DAY, [Date], DATEADD(MONTH, 1, [Date])) AS TotalDayinMonth';
            }
            $month1= date('m');// Present Month
            $year= date('Y'); // Preseent year

            $num_of_days = date('t',mktime(0,0,0,$month1+1,1,$year));
            for($count = 0; $count < $num_of_days ; $count++){

              // SELECT shift_date dateofmonth, COUNT(*) FROM tbl_shift WHERE YEAR(shift_date) = 2019 AND MONTH(shift_date) = 10 GROUP BY dateofmonth ORDER BY dateofmonth
            }
            $dt_query = $this->db->select('
          COUNT(IF( ' . $month . ', 1, NULL)) as current,
          ');
            $this->db->from('tbl_participant as pr');
            $this->db->join('tbl_shift_participant as sf_pr', 'sf_pr.participantId = pr.id AND sf_pr.status = 1', 'left');
            $this->db->join('tbl_shift as sf', 'sf.id = sf_pr.shiftId', 'left');
            $this->db->where_in('sf.status', array(1, 2, 3, 7));
            $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());
            $result = $query->result_array();
            $percentage_val = '';
            $average = $result->last - $result->current;
            $percentage = 0; $pr='';
              $color='';
            if ($average != 0) {
                $percentage = $average / $result->current * 100;
                if ($percentage > 0) {
                    $color ='green';
                    $pr = 'hcm-mg-ie-profit';
                    $percentage_val = $percentage . '% increases';
                } else {
                    $color ='red';
                    $percentage = 0;
                    $pr = 'hcm-mg-ie-lost';
                    $percentage_val = $percentage . '% decreases';
                }
            }

            $result->percentage = $percentage_val;
            $result->percentage_color = $color;
              $result->percentage_type = $pr;
            return (array)$result;
        }

        function get_participant_shift_graph_data($reqData) {
            $graph_title = '';
            $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
            if (date('m') <= 6) {//Upto June 2014-2015
                $start_year = $financial_start = (date('Y') - 1);
                $end_year = $financial_end = (int) date('Y');
            } else {//After June 2015-2016
                $start_year = $financial_start = date('Y');
                $end_year = $financial_end = (int) (date('Y') + 1);
            }

            $financial_start = DateFormate($financial_start . '-07-01', 'Y-m-d');
            $financial_end = DateFormate($financial_end . '-06-30', 'Y-m-d');


            $this->db->select("CASE when status = 7 THEN count(id) else 0 end as total_filled_shift", false);
            $this->db->select("CASE when status = 5 THEN count(id) else 0 end as total_cancel_shift", false);

            $this->db->select("CASE when status = 1 AND day(start_time)< day(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);

            $this->db->select("CASE when status = 7 THEN sum(TIME_TO_SEC(TIMEDIFF(end_time, start_time))/60/60 ) else 0 end as filled_shift_sum", false);
            $this->db->select("CASE when status = 7 THEN  avg(TIME_TO_SEC(TIMEDIFF(end_time, start_time))/60/60 ) else 0 end as filled_shift_avg", false);

            if ($view_type == 'current_month' || $view_type == 'selected_month') {
                $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
                $this->db->select("(day(start_time)) as current_term", false);
                $this->db->select("((start_time)) as date", false);
            } else {
                $this->db->select("CASE when status = 1 AND date(start_time) < date(CURRENT_DATE()) THEN count(id) else 0 end as total_unfilled_shift", false);
                $this->db->select("(month(start_time)) as current_term", false);
                $this->db->select("(month(start_time)) as month", false);
            }

            $this->db->from('tbl_shift');

            if ($view_type == 'current_month') {

                $this->db->where('MONTH(start_time) = MONTH(CURRENT_DATE())', null, false);

                $this->db->group_by('date(start_time)');
            } elseif ($view_type == 'selected_month' && !empty($reqData->specific_month)) {
                $x = explode('-', $reqData->specific_month);
                $month = (int) $x[0];
                $year = (int) $x[1];
                $this->db->where("MONTH(start_time) = (" . addslashes($month) . ")", null, false);
                $this->db->where("YEAR(start_time) = (" . addslashes($year) . ")", null, false);

                $this->db->group_by('date(start_time)');
            } else {
                $this->db->where("date(start_time) BETWEEN '" . $financial_start . "' AND '" . $financial_end . "'", null, false);
                $this->db->group_by('month(start_time)');
                $this->db->order_by('start_time');
            }

            $result = $this->db->get()->result();

            if (!empty($result)) {
                foreach ($result as $val) {
                    $val->filled_shift_sum = round($val->filled_shift_sum, 2);
                    $val->filled_shift_avg = round($val->filled_shift_avg, 2);
                }
            }

            $result_arr = [];
            if ($view_type == 'current_month' || $view_type == 'selected_month') {
                if ($view_type == 'current_month') {
                    $graph_title = DateFormate(DATE_TIME, 'F Y');
                    $months_date = getMonthAllDate(DATE_TIME);
                } else {

                    $date = date("Y-m-d", strtotime($year . '-' . $month, '-1'));
                    $graph_title = DateFormate($date, 'F Y');
                    $months_date = getMonthAllDate($date);
                }

                $result_arr = obj_to_arr($result);
                $result_arr = pos_index_change_array_data($result_arr, 'date');

                if (!empty($months_date)) {
                    foreach ($months_date as $val) {

                        if (empty($result_arr[$val])) {
                            $result_arr[$val] = ['total_filled_shift' => 0,
                                'total_cancel_shift' => 0,
                                'total_unfilled_shift' => 0,
                                'filled_shift_sum' => 0,
                                'filled_shift_avg' => 0,
                                'current_term' => ((int) DateFormate($val, 'd')) . $this->get_specific_number_term(DateFormate($val, 'd')),
                                'date' => $val,
                                'custome' => true];
                        } else {
                            $result_arr[$val]['current_term'] = ((int) DateFormate($val, 'd')) . $this->get_specific_number_term(DateFormate($val, 'd'));
                        }
                    }
                }

                ksort($result_arr);
                $result_arr = array_values($result_arr);
            } elseif ($view_type == 'year') {
                $result_arr = obj_to_arr($result);
                $result_arr = pos_index_change_array_data($result_arr, 'month');

                $start_month = 7;
                $end_month = 6;
                $new_special_order_year_result = [];
                $graph_title = $start_year . ' - ' . $end_year;

                $i = 0;
                for ($start_month; $i < 13; $start_month++) {

                    if (empty($result_arr[$start_month])) {
                        $new_special_order_year_result[$start_month] = ['total_filled_shift' => 0,
                            'total_cancel_shift' => 0,
                            'total_unfilled_shift' => 0,
                            'filled_shift_sum' => 0,
                            'filled_shift_avg' => 0,
                            'current_term' => ((int) ($start_month)) . ' - ' . $this->get_month_name($start_month),
                            'month' => $start_month,
                            'custome' => true];
                    } else {
                        $result_arr[$start_month]['current_term'] = ((int) ($start_month)) . ' - ' . $this->get_month_name($start_month);
                        $new_special_order_year_result[$start_month] = $result_arr[$start_month];
                    }

                    if ($start_month == 12) {
                        $start_month = 1;

                        $start_year++;
                    }

                    if ($start_month == $end_month && $start_year == $end_year) {
                        break;
                    }
                    $i++;
                }

                $result_arr = array_values($new_special_order_year_result);
            }

            return ['graph_shifts' => $result_arr, 'shift_graph_title' => $graph_title];
        }

        function get_month_name($d) {
            $months = ['1' => "Jan", '2' => "Feb", '3' => "Mar", '4' => "Apr", '5' => "May", '6' => "Jun",
                '7' => "Jul", '8' => "Aug", '9' => "Sept", '10' => "Oct", '11' => "Nov", '12' => "Dec"];

            return $months[$d];
        }

        function get_specific_number_term($d) {

            if ($d > 3 && $d < 21)
                return 'th';
            switch ($d % 10) {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
}
