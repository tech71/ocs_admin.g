<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fms_model extends CI_Model 
{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_fms_details($reqData)
    {
        $graph_title = '';
        $view_type = !empty($reqData->view_type) ? $reqData->view_type : 'current_month';
        $specific_month = isset($reqData->specific_month)?$reqData->specific_month:''; 
        $return_ary = [];
        $return_ary['hour_count'] = $this->get_fms_data($view_type,$specific_month);
        $return_ary['fms_stats'] = $this->get_fms_stats($view_type,$specific_month);
        $return_ary['server_view_type'] = $this->viewtype_and_details($view_type,$specific_month);
        return $return_ary;
    }

    public function viewtype_and_details($view_type,$specific_month)
    {
        if($view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();
            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];
            $previous_finicial_start_date = $years['previous_finicial_start_date'];
            $previous_finicial_end_date = $years['previous_finicial_end_date'];
            return array('type'=>$view_type,'date'=>date('Y',strtotime($financial_start)).'/'.date('y',strtotime($financial_end)),'previous_year'=>date('Y',strtotime($previous_finicial_start_date)).'/'.date('y',strtotime($previous_finicial_end_date)));
        }
        else
        {
            $months_record = get_current_previous_month($view_type,$specific_month);
            //pr($months_record);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];

            $previous_month = $months_record['previous_month'];
            $previous_year = $months_record['previous_year'];

            return array('type'=>'Month','date'=>$current_month.'/'.$current_year,'previous_year'=>$previous_month.'/'.$previous_year);
        }
    }

    public function get_fms_data($view_type,$specific_month) 
    {
        if($view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();

            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];

            $previous_finicial_start_date = $years['previous_finicial_start_date'];
            $previous_finicial_end_date = $years['previous_finicial_end_date'];

            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$financial_start' AND '$financial_end' AND initiated_type = 1 THEN 1  ELSE '0' END) as reported_by_member_this", false);
            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$previous_finicial_start_date' AND '$previous_finicial_end_date' AND initiated_type = 1 THEN 1  ELSE '0' END) as reported_by_member_previous", false);

            $this->db->select("AVG(CASE WHEN date(created) BETWEEN '$financial_start' AND '$financial_end' AND status = 1 THEN DATEDIFF(completed_date, created)  ELSE '0' END) as avg_resolve_this", false);
            $this->db->select("AVG(CASE WHEN date(created) BETWEEN '$previous_finicial_start_date' AND '$previous_finicial_end_date' AND status = 1 THEN DATEDIFF(completed_date, created)  ELSE '0' END) as avg_resolve_previous", false);

            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$financial_start' AND '$financial_end' AND initiated_type = 3 THEN 1  ELSE '0' END) as reported_by_org_this", false);

            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$previous_finicial_start_date' AND '$financial_end' AND initiated_type = 3 THEN 1  ELSE '0' END) as reported_by_org_previous", false);

            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$financial_start' AND '$financial_end' AND initiated_type = 2 THEN 1  ELSE '0' END) as reported_by_participant_this", false);

            $this->db->select("SUM(CASE WHEN date(created) BETWEEN '$previous_finicial_start_date' AND '$financial_end' AND initiated_type = 2 THEN 1  ELSE '0' END) as reported_by_participant_previous", false);
        }else{

            $months_record = get_current_previous_month($view_type,$specific_month);

            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];

            $previous_month = $months_record['previous_month'];
            $previous_year = $months_record['previous_year'];

            $this->db->select("SUM(CASE WHEN month(created) = $current_month AND year(created) = $current_year AND initiated_type = 1 THEN 1  ELSE '0' END) as reported_by_member_this", false);
            $this->db->select("SUM(CASE WHEN month(created) = $previous_month AND year(created) = $previous_year AND initiated_type = 1 THEN 1  ELSE '0' END) as reported_by_member_previous", false);           

            $this->db->select("AVG(CASE WHEN month(created) = $current_month AND year(created) = $current_year AND status = 1 THEN DATEDIFF(completed_date, created)  ELSE '0' END) as avg_resolve_this", false);
            $this->db->select("AVG(CASE WHEN month(created) = $previous_month AND year(created) = $previous_year AND status = 1 THEN DATEDIFF(completed_date, created)  ELSE '0' END) as avg_resolve_previous", false);

            $this->db->select("SUM(CASE WHEN month(created) = $current_month AND year(created) = $current_year AND initiated_type = 3 THEN 1  ELSE '0' END) as reported_by_org_this", false);
            $this->db->select("SUM(CASE WHEN month(created) = $previous_month AND year(created) = $previous_year AND initiated_type = 3 THEN 1 ELSE '0' END) as reported_by_org_previous", false);

            $this->db->select("SUM(CASE WHEN month(created) = $current_month AND year(created) = $current_year AND initiated_type = 2 THEN 1  ELSE '0' END) as reported_by_participant_this", false);
            $this->db->select("SUM(CASE WHEN month(created) = $previous_month AND year(created) = $previous_year AND initiated_type = 2 THEN 1  ELSE '0' END) as reported_by_participant_previous", false);
        }

        $this->db->from('tbl_fms_case');
        $this->db->where(array('fms_type'=>0));
        $result = $this->db->get()->row_array(); 
        
        $main_ary = [];
        if(!empty($result))
        {
            $main_ary['member_reported'] = stats_percentage_calculation($result['reported_by_member_this'], $result['reported_by_member_previous']);
            $main_ary['participant_reported'] = stats_percentage_calculation($result['reported_by_participant_this'], $result['reported_by_participant_previous']);
            $main_ary['avg_fms'] = stats_percentage_calculation(number_format($result['avg_resolve_this'],2,'.',''), number_format($result['avg_resolve_previous'],2,'.',''));
            $main_ary['org_reported'] = stats_percentage_calculation($result['reported_by_org_this'], $result['reported_by_org_previous']);
        } 
        return $main_ary;
    }

    public function get_fms_stats($view_type,$specific_month)
    { 
        $array = [];       
        if (isset($view_type) && $view_type == 'year')
        {
            $years = get_current_n_previous_financial_year();
            $financial_start = $years['financial_start'];
            $financial_end = $years['financial_end'];
        }
        else
        {
            $months_record = get_current_previous_month($view_type,$specific_month);
            $current_month = $months_record['current_month'];
            $current_year = $months_record['current_year'];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 2', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_hot_spots_shift = $this->db->get();

        $participant_hot_spots = $sql_hot_spots_shift->result_array();
        if(!empty($participant_hot_spots)){
            $array['participant_hot_spots'] = $participant_hot_spots;
        }else{
            $array['participant_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 2 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 2', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_cold_spots_shift = $this->db->get();
        $participant_cold_spots = $sql_cold_spots_shift->result_array();
        if(!empty($participant_cold_spots)){
            $array['participant_cold_spots'] = $participant_cold_spots;
        }else{
            $array['participant_cold_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 1 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 1 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 1', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_filled_hot_spots_shift = $this->db->get();

        $member_hot_spots = $sql_filled_hot_spots_shift->result_array();
        if(!empty($member_hot_spots)){
            $array['member_hot_spots'] = $member_hot_spots;
        }else{
            $array['member_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 1 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 1 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 1', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_filled_cold_spots_shift = $this->db->get();
        $member_cold_spots = $sql_filled_cold_spots_shift->result_array();
        if(!empty($member_cold_spots)){
            $array['member_cold_spots'] = $member_cold_spots;
        }else{
            $array['member_cold_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 3 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 3 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 3', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_filled_hot_spots_shift = $this->db->get();
        $org_hot_spots = $sql_filled_hot_spots_shift->result_array();
        if(!empty($org_hot_spots)){
            $array['org_hot_spots'] = $org_hot_spots;
        }else{
            $array['org_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type = 3 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type = 3 THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', 's.id = sl.caseId AND s.initiated_type = 3', 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_filled_cold_spots_shift = $this->db->get();
        $org_cold_spots = $sql_filled_cold_spots_shift->result_array();
        if(!empty($org_cold_spots)){
            $array['org_cold_spots'] = $org_cold_spots;
        }else{
            $array['org_cold_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type IN('4,5,6,7') THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type IN('4,5,6,7') THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', "s.id = sl.caseId AND s.initiated_type IN('4,5,6,7')", 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','DESC');
        $this->db->limit(3);
        $sql_filled_hot_spots_shift = $this->db->get();
        $other_hot_spots = $sql_filled_hot_spots_shift->result_array();
        if(!empty($other_hot_spots)){
            $array['other_hot_spots'] = $other_hot_spots;
        }else{
            $array['other_hot_spots'] = [];
        }

        if($view_type == 'year'){
            $this->db->select("SUM(CASE WHEN date(s.created) BETWEEN '$financial_start' AND '$financial_end' AND s.initiated_type IN('4,5,6,7') THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }
        else{
            $this->db->select("SUM(CASE WHEN month(s.created) = $current_month AND year(s.created) = $current_year AND s.initiated_type IN('4,5,6,7') THEN 1  ELSE 0 END) as my_count,sl.address", false);
        }

        $this->db->from('tbl_fms_case_location as sl');
        $this->db->join('tbl_fms_case as s', "s.id = sl.caseId AND s.initiated_type IN('4,5,6,7')", 'inner');
        $this->db->group_by('sl.address,sl.state,sl.suburb,sl.postal');
        $this->db->order_by('my_count','ASC');
        $this->db->limit(3);
        $sql_filled_cold_spots_shift = $this->db->get();
        $other_cold_spots = $sql_filled_cold_spots_shift->result_array();
        if(!empty($other_cold_spots)){
            $array['other_cold_spots'] = $other_cold_spots;
        }else{
            $array['other_cold_spots'] = [];
        }
        return $array;
    }

    public function fms_count_by_key_subquery() {
        $this->db->flush_cache();
        $this->db->select(['count(m.id)']);
        $this->db->from('tbl_fms_case as m');
        $this->db->join('tbl_fms_case_location as ma', 'ma.caseId = m.id');
        $this->db->where('ma.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    public function participant_count_by_postcode_subquery($member_compare_with_specific) {
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created >= '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("p.created < '" . $previous_month . "'", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'active_with_at_least_one_shift') {
            $this->db->join('tbl_shift_participant as sp', 'sp.shiftId = p.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');

            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("p.status", 1);
        } elseif ($member_compare_with_specific === 'inactive_and_not_archived') {
            $this->db->where("p.status", 0);
            $this->db->where("p.archive", 0);
        }
        $this->db->select(['count(pa.participantId) as s_count']);
        $this->db->from('tbl_participant as p');
        $this->db->join('tbl_participant_address as pa', 'p.id = pa.participantId AND p.archive = 0', 'INNER');
        $this->db->where('pa.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    public function member_count_by_postcode_subquery($member_compare_with_specific) { 
        $this->db->flush_cache();
        if ($member_compare_with_specific === 'new') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created >= '" . $previous_month . "'", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'existing') {
            $previous_month = date('Y-m-d', strtotime('-1 months'));
            $this->db->where("m.created < '" . $previous_month . "'", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'active_shift') {
            $this->db->join('tbl_shift_member as sp', 'sp.memberId = m.id', 'INNER');
            $this->db->join('tbl_shift as s', 'sp.shiftId = s.id AND s.status = 7 AND archive = 0', 'INNER');
            $this->db->where("'" . DATE_TIME . "' BETWEEN start_time and end_time", null, false);
            $this->db->where("m.status", 1);
        } elseif ($member_compare_with_specific === 'without_shift') {
            $this->db->where("m.status", 0);
            $this->db->where("m.archive", 0);
        }
        $this->db->select(['count(ma.memberId) as s_count']);
        $this->db->from('tbl_member as m');
        $this->db->join('tbl_member_address as ma', 'm.id = ma.memberId AND m.archive = 0', 'INNER');
        $this->db->where('ma.postal = ss.postcode', null, false);
        #last_query();
        return $this->db->get_compiled_select();
    }

    public function org_count_by_postcode_subquery($member_compare_with_specific) 
    {
        $this->db->flush_cache();
        $this->db->select(['count(o.id) as s_count']);
        $this->db->join('tbl_organisation_address as oa', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');
        $this->db->join('tbl_fms_case_location as fcl', 'fcl.postal = oa.postal', 'INNER');
        $this->db->from('tbl_organisation as o', 'o.id = oa.organisationId AND o.archive = 0', 'INNER');
        $this->db->where('fcl.postal = ss.postcode', null, false);
        return $this->db->get_compiled_select();
    }

    public function get_fms_by_post_code($reqData) 
    {
        $compare_against = (!empty($reqData->compare_against)) ? $reqData->compare_against : '';
        $member_compare_with_specific = (!empty($reqData->compare_against_2)) ? $reqData->compare_against_2 : '';

        $fms_sub_query = $this->fms_count_by_key_subquery();

        $sub_query2 = '';
        if ($compare_against === 'participant') {
            $sub_query2 = $this->participant_count_by_postcode_subquery($member_compare_with_specific);
        }elseif ($compare_against === "org") {
            $sub_query2 = $this->org_count_by_postcode_subquery($member_compare_with_specific);
        }elseif ($compare_against === "member") {
            $sub_query2 = $this->member_count_by_postcode_subquery($member_compare_with_specific);
        }

        $this->db->flush_cache();
        $this->db->select(['ss.longitude as lng', 'ss.latitude as lat']);
        $this->db->from('tbl_suburb_state as ss');
        $this->db->select('(' . $fms_sub_query . ') as f_count', false);

        if ($sub_query2)
        {
            $this->db->select('(' . $sub_query2 . ') as s_count');
        } 
        else 
        {
            $this->db->select('"0" as s_count');
        }
        $this->db->group_by('ss.postcode');
        $this->db->having('f_count > 0 || s_count > 0', null, false);
        $this->db->limit('15');
        $result = $this->db->get()->result();
        return $result;
    }
}
