<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->load->model('Admin_model');
        $this->load->model('Basic_model');
    }

    public function index() {
      $response = array();
      $reqData = request_handler();
      if (!empty($reqData)) {
       $this->load->model('Admin_model');
       $result = $this->Admin_model->get_user_profile($reqData->adminId);
         //var_dump($result);exit;
       if($result){
        $response = array('status' => true, 'data' => $result );
    } else {
        $response = array('status' => false, 'data' => []);
    }
}
echo json_encode($response);
}
public function profile_update() 
{
    $response = array();
    $reqData = request_handler();
    if (!empty($reqData)) 
    {        
        $validate_data = (array)$reqData->data->userdata;
        $temp_rule = array(
            array('field' => 'fullname', 'label' => 'Full Name', 'rules' => 'required'),
            array('field' => 'mobile', 'label' => 'Mobile', 'rules' => 'required|is_natural'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'required|valid_email'),
        );
       
        if(isset($validate_data['c_password']) && !empty($validate_data['c_password']))
        {
            $a = array(
                array('field' => 'c_password', 'label' => 'Current Password', 'rules' => 'required'),
                array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'required'),
                array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'required|matches[new_password]')
            );
        }
        if(!empty($a))
        $validation_rules = array_merge($a,$temp_rule);
        else
        $validation_rules = $temp_rule;


        $this->form_validation->set_data($validate_data);
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run())
        {
            $result = $this->Admin_model->update_profile($reqData);
            if($result['status'])
            {
                $response = array('status' => true,'msg' => system_msgs('profile_saved'));
            } 
            else
            {
                $response = array('status' => false,'msg' => $result['msg']);
            }
        } 
        else
        {
            $errors = $this->form_validation->error_array();
            $response = array('status' => false, 'msg' => implode(', ', $errors));
        }
    }
    echo json_encode($response);
}
public function sendVerifyCode() {
  $response = array();
  $reqData = request_handler();     
  if (!empty($reqData)) {    
   $result = $this->Admin_model->get_user_profile($reqData->adminId); 
   $random_number = rand(1000,10000);       
   $body="Verfication code $random_number";
   $subject="Profile verfication code";
   send_mail_smtp($result->email, $subject, $body);
   $result = $this->Admin_model->update_verification_code_profile_sent($reqData->adminId,$random_number);
   if($result){
    $response = array('status' => true,'success' => system_msgs('verification_code_sent'));
} else {
   $response = array('status' => false,'error' => system_msgs('verification_code_error'));
}
}
echo json_encode($response);

}
public function verifyCodeSent() {
  $response = array();
  $reqData = request_handler();     
  if (!empty($reqData)) {        
    $result = $this->Admin_model->update_verification_code_profile($reqData->adminId,$reqData->data->code);
    if($result){
        $response = array('status' => true,'success' => system_msgs('verification_code_verified'));
    } else {
      $response = array('status' => false,'error' => system_msgs('verification_code_wrong'));
  }
}
echo json_encode($response);

}
}
