<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fms extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Fms_model');
        $this->form_validation->CI = & $this;
    }

    public function get_fms_details() 
    {
        $reqData = request_handler();
        $reqData = ($reqData->data);
        if (!empty($reqData)) {
            $x = $this->Fms_model->get_fms_details($reqData);
            $result = array("status" => true, "data" => $x);
            echo json_encode($result);
        }
    }

    public function get_fms_by_post_code() 
    {
        $reqData = request_handler();
        $reqData = ($reqData->data);
        if (!empty($reqData)) {
            $x = $this->Fms_model->get_fms_by_post_code($reqData);
            $result = array("status" => true, "data" => $x);
            echo json_encode($result);
        }
    }

}
