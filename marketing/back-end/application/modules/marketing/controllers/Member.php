<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Member_model');
        $this->form_validation->CI = & $this;
    }

    public function index() {
        
    }

    public function member_graph_details() {
        $reqData = request_handler();
        $reqData = ($reqData->data);

        if (!empty($reqData)) {
            $session = $this->Member_model->get_session_according_to_request($reqData);
            $response = $this->Member_model->get_member_specific_counts($reqData);

            $x = $this->Member_model->get_member_shift_graph_data($reqData);
            $response['graph_shifts'] = $x['graph_shifts'];
            $response['shift_graph_title'] = $x['shift_graph_title'];

            $x = $this->Member_model->get_member_intake_droplate_graph($reqData);
            $response['member_intake_graph'] = $x['member_intake_graph'];
            $response['recruitment_stage'] = $x['recruitment_stage'];

            $response['member_postcode'] = $this->Member_model->get_member_by_postcode($reqData);
            $response['member_work_area_graph'] = $this->Member_model->get_member_work_area_graph($session);

            $result = array("status" => true, "data" => $response);
            echo json_encode($result);
        }
    }

}
