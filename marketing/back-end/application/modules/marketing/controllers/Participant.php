<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Participant_model');
        $this->form_validation->CI = & $this;
    }

    public function index() {
        
    }

    public function get_participant_graph_details() {
        $reqData = request_handler();

        if (!empty($reqData->data)) {
            $session = $this->Participant_model->get_session_according_to_request($reqData->data);

            $result = $this->Participant_model->participant_specific_count($session);
            
            $result['comparison_by_funding_type'] = $this->Participant_model->comparison_participant_by_funding_type($session);
          

            $x = $this->Participant_model->get_participant_shift_graph_data($reqData->data);
            $result = array_merge($result, $x);

            $result['participant_intake_graph'] = $this->Participant_model->participant_intake_droplate_graph($reqData->data);

            $response = array('status' => true, 'data' => $result);
        } else {
            $response = array('status' => false, 'error' => 'No data found');
        }

        echo json_encode($response);
    }

    public function get_participant_postcode_data() {
        $reqData = request_handler();

        if (!empty($reqData->data)) {
            $result['participant_match_with_postcode'] = $this->Participant_model->get_participant_by_postcode($reqData->data);

            $response = array('status' => true, 'data' => $result);
        } else {
            $response = array('status' => false, 'error' => 'No data found');
        }
        echo json_encode($response);
    }

}
