<?php

use classRoles as adminRoles;
use AdminClass as AdminClass;

require_once APPPATH . 'Classes/admin/formCustomValidation.php';
//require_once APPPATH . 'Classes/websocket/Websocket.php';

defined('BASEPATH') OR exit('No direct script access allowed');

//class Master extends MX_Controller
class Dashboard extends CI_Controller {

    use formCustomValidation;

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->loges->setLogType('user_admin');
        $this->load->library('form_validation');
        $this->load->model('Dashboard_model');
    }

    public function index() {
        
    }

    public function get_user_details() {
        $reqData = request_handler('access_admin', 1, 1);
        require_once APPPATH . 'Classes/admin/admin.php';
        $objAdmin = new AdminClass\Admin();

        if ($reqData->data) {
            $objAdmin->setAdminid($reqData->data->AdminId);

            $result = $objAdmin->get_admin_details();
            if (!empty($result)) {
                $result['PhoneInput'] = $objAdmin->get_admin_phone_number();
                $result['EmailInput'] = $objAdmin->get_admin_email();

                $admins_its = $this->config->item('super_admins');

                $result['its_super_admin'] = in_array($objAdmin->getAdminid(), $admins_its);
                $response = array('status' => true, 'data' => $result);
            } else {
                $response = array('status' => false, 'data' => 'Invalid request');
            }

            echo json_encode($response);
        }
    }

    // this method use for check user email already exist
    public function check_user_emailaddress_already_exist($sever_emailAddress = array(), $adminId_NEW = false) {
        $this->load->model('admin_model');
        $adminId = false;

        if ($this->input->get() || $sever_emailAddress) {
            $emails = ($this->input->get()) ? $this->input->get() : (array) $sever_emailAddress;

            if ($this->input->get('adminId') || $adminId_NEW) {
                $adminId = ($this->input->get('adminId')) ? $this->input->get('adminId') : $adminId_NEW;
                if (array_key_exists('adminId', $emails)) {
                    unset($emails['adminId']);
                };
            }


            foreach ($emails as $val) {
                $result = $this->admin_model->check_dublicate_email($val, $adminId);
                if ($sever_emailAddress) {
                    if (!empty($result)) {
                        $this->form_validation->set_message('check_user_emailaddress_already_exist', 'this ' . $result[0]->email . ' Email address Allready Exist');
                        return false;
                    }
                }

                if ($this->input->get()) {
                    if (!empty($result)) {
                        echo 'false';
                    } else {
                        echo 'true';
                    }
                }

                return true;
            }
        }
    }

    public function update_password() {
        $reqData = request_handler('access_admin');

        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $adminAuth->setAdminid($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'password', 'label' => 'confirm current', 'rules' => 'required'),
                array('field' => 'new_password', 'label' => 'new password', 'rules' => 'required|min_length[6]'),
                array('field' => 'confirm_password', 'label' => 'confirm new', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {

                $adminAuth->setPassword($reqData->password);
                $check_password = $adminAuth->verifyCurrentPassword($reqData->password);

                if ($check_password) {
                    // set new password we need to update in db
                    $adminAuth->setPassword($reqData->new_password);

                    // update password of admin
                    $adminAuth->reset_password();

                    $response = array('status' => true, 'success' => 'Your password update successfully');
                } else {
                    $response = array('status' => false, 'error' => 'Your password not match with current password');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function update_password_recovery_email() {
        $reqData = request_handler('access_admin');

        require_once APPPATH . 'Classes/admin/auth.php';
        $adminAuth = new Admin\Auth\Auth();

        $adminAuth->setAdminid($reqData->adminId);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $this->form_validation->set_data((array) $reqData);

            $validation_rules = array(
                array('field' => 'email', 'label' => 'email', 'rules' => 'required|valid_email'),
                array('field' => 'confirm_email', 'label' => 'confir email', 'rules' => 'required|valid_email'),
                array('field' => 'password', 'label' => 'password', 'rules' => 'required'),
            );

            // set rules form validation
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $adminAuth->setPassword($reqData->password);
                $check_password = $adminAuth->verifyCurrentPassword();

                if (!$check_password) {
                    $response = array('status' => false, 'error' => 'Your enter password not match with your current password');
                    echo json_encode($response);
                    exit();
                }

                $adminAuth->setPrimaryEmail($reqData->email);

                $check_email = $adminAuth->checkExistingEmail();


                if (empty($check_email)) {
                    $token = array('email' => $reqData->email, 'time' => DATE_TIME, 'adminId' => $adminAuth->getAdminid());
                    $token = encrypt_decrypt('encrypt', json_encode($token));

                    $adminAuth->setToken($token);
                    $this->basic_model->update_records('member', array('otp' => $token), array('id' => $adminAuth->getAdminid()));

                    $result = $this->basic_model->get_row('member', array("firstname", "lastname"), array('id' => $adminAuth->getAdminid()));

                    $adminAuth->setFirstname($result->firstname);
                    $adminAuth->setLastname($result->lastname);

                    // send email verification email
                    $adminAuth->sendUpdatePasswordRecoveryEmail();

                    $response = array('status' => true, 'success' => 'Please verify your entered email address, we sended email to your entered email');
                } else {
                    $response = array('status' => false, 'error' => 'This email already exist to another user');
                }
            } else {
                $errors = $this->form_validation->error_array();
                $response = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($response);
        }
    }

    public function dashboard_graph_details() {
        $reqData = request_handler('access_admin', 1);

        if (!empty($reqData->data)) {
            $reqData = $reqData->data;

            $session = $this->Dashboard_model->get_session_according_to_request($reqData);
            
            $response = $this->Dashboard_model->get_dashboard_count($session);

            $response['member_vs_participant_intake'] = $this->Dashboard_model->dashboard_intake_graph($session);

            $response['org_comparison'] = $this->Dashboard_model->compare_between_org_or_site_or_sub_org($session);

            echo json_encode(['status' => true, 'data' => $response]);
        }
    }

}
