<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Organisation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Organisation_model');
        $this->form_validation->CI =& $this;
    }
    public function index()
    {
    }

    public function get_org_details(){
      $response  = array();
      $reqData= request_handler();
      var_dump($reqData);
      if(!empty($reqData)){
      $result  = $this->Organisation_model->get_org_details($reqData);
      if (!empty($result)) {
          $response = array('status' => true, 'data' => $result );
      } else {
          $response = array('status' => false, 'data' => '');
      }
      } else {
      $response = array('status' => false, 'data' => '');
      }
      echo json_encode($response);
    }
  }
