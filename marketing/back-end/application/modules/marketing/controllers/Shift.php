<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shift extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Shift_model');
        $this->form_validation->CI = & $this;
    }

    public function shift_graph_details() 
    {
        $reqData = request_handler();
        $reqData = ($reqData->data);
        if (!empty($reqData)) {
            $x = $this->Shift_model->get_shift_data($reqData);
            $result = array("status" => true, "data" => $x);
            echo json_encode($result);
        }
    }

    public function get_shift_by_post_code() 
    {
        $reqData = request_handler();
        $reqData = ($reqData->data);
        if (!empty($reqData)) {
            $x = $this->Shift_model->get_shift_by_post_code($reqData);
            $result = array("status" => true, "data" => $x);
            echo json_encode($result);
        }
    }

}
