<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApplicantRequiredValidation
 *
 * @author user YDT
 */
class ApplicantRequiredValidation {

    protected $CI;
    protected $applicantId;
    protected $current_stage;
    protected $error;
    protected $error_stutus;
    protected $validation_rule = [
        'flagged' => false,
        'flagged_pending' => false,
        'not_flagged_pending' => false,
        'not_flagged' => false,
        'duplicate' => false,
        'duplicate_pending' => false,
        'not_duplicate_pending' => false,
        'not_duplicate' => false,
        'current_stage' => false,
        'in_progress' => false,
        'not_archive' => false,
        'not_hired' => false,
        'on_stage_complete_verified_details' => false,
    ];

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI = & get_instance();
        $this->applicantId = 0;
    }

    function setApplicantId($applicantId) {
        $this->applicantId = (int) $applicantId;
    }

    function setValidationRule($rule) {
        $this->validation_rule = array_replace($this->validation_rule, $rule);
    }

    function check_applicant_required_checks($extra_params = []) {
        $this->CI->db->select(['ra.id', 'ra.status', 'ra.archive', 'ra.flagged_status', 'ra.current_stage', 'ra.duplicated_status']);
        $this->CI->db->from('tbl_recruitment_applicant as ra');
        $this->CI->db->where('ra.id', $this->applicantId);

        if ($this->validation_rule['duplicate_pending'] || $this->validation_rule['not_duplicate_pending'] || $this->validation_rule['not_duplicate'] || $this->validation_rule['duplicate']) {
            $this->CI->db->select('(select status from tbl_recruitment_applicant_duplicate_status where applicant_id = ra.id) as duplicate_m_status');
        }

        if ($this->validation_rule['current_stage']) {
            $this->CI->db->select(['rs.stage']);
            $this->CI->db->join('tbl_recruitment_stage as rs', 'rs.id = ra.current_stage and rs.archive = 0', 'inner');
        }

        $query = $this->CI->db->get();
        $res = $query->row();

        if (empty($res)) {
            $this->error[] = 'Applicant id not valid.';
        } else {
            if ($this->validation_rule) {
                foreach ($this->validation_rule as $rule => $status) {
                    if (!$status) {
                        continue;
                    }

                    switch (strtolower($rule)) {
                        case 'flagged':
                            if ((int) $res->flagged_status !== 1) {
                                $this->error[] = 'Applicant not flagged.';
                            }
                            break;

                        case 'not_flagged':
                            if ((int) $res->flagged_status !== 0) {
                                $this->error[] = 'Applicant is flagged.';
                            }
                            break;

                        case 'not_flagged_pending':
                            if (in_array((int) $res->flagged_status, [1, 3])) {
                                $this->error[] = 'Applicant requested for the flage.';
                            }
                            break;

                        case 'flagged_pending':
                            if (in_array((int) $res->flagged_status, [0, 2])) {
                                $this->error[] = 'Applicant not requested for flagged.';
                            }
                            break;

                        case 'duplicate':
                            if ((int) $res->duplicated_status !== 1) {
                                $this->error[] = 'Applicant not duplicate.';
                            }
                            break;

                        case 'duplicate_pending':
                            if ((int) $res->duplicate_m_status !== 1) {
                                $this->error[] = 'Applicant is not pending stage of duplicate.';
                            }
                            break;
                        case 'not_duplicate_pending':
                            if ((int) $res->duplicate_m_status === 1) {
                                $this->error[] = 'Applicant is pending for mark as duplicate.';
                            }
                            break;

                        case 'not_duplicate':
                            if ($res->duplicated_status != 0) {
                                $this->error[] = 'Applicant is duplicate.';
                            }
                            break;

                        case 'current_stage':
                            if ((int) $res->current_stage !== $this->current_stage) {
                                $this->error[] = 'Applicant current stage not valid for this action.';
                            }
                            break;

                        case 'in_progress':
                            if ((int) $res->status !== 1) {
                                $this->error[] = 'Applicant should be in progress stage.';
                            }
                            break;
                        case 'not_archive':
                            if ($res->archive == 1) {
                                $this->error[] = 'Applicant already archived.';
                            }
                            break;
                        case 'not_hired':
                            if ($res->status == 3) {
                                $this->error[] = 'Applicant already hired.';
                            }
                            break;
                        case 'on_stage_complete_verified_details':
                            $response = $this->on_stage_complete_wise_details_verified($this->applicantId, $extra_params->stageId);
                            
                            if(empty($response['status'])){
                                $this->error[] = $response['error'];
                            }
                            break;
                    }
                }
            }
        }

        if (!empty($this->error)) {
            return ['status' => false, 'error' => $this->error];
        } else {
            return ['status' => true];
        }
    }

    function on_stage_complete_wise_details_verified($applicantId, $stageId) {
        $this->CI->load->model('recruitment/Recruitment_applicant_model');

        $response = ['status' => true];

        if ($stageId == 2) {
            $res = $this->CI->Recruitment_applicant_model->get_applicant_phone_interview_classification($applicantId);

            if (empty($res)) {
                $response = ['status' => false, 'error' => 'Phone interview classificaiton not completed.'];
            }
        }
        
        return $response;
    }

}
