import React, { Component } from 'react';
import jQuery from "jquery";
import moment from 'moment-timezone';
import { ROUTER_PATH, BASE_URL, LOGIN_DIFFERENCE } from '../config.js';
// import { confirmAlert, createElementReconfirm } from 'react-confirm-alert'; // Import
// import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import axios from 'axios';

//check user login or not
export function checkItsLoggedIn() {
    if (getLoginToken()) {
        window.location = ROUTER_PATH + 'Dashboard';
    }
}

export function checkItsNotLoggedIn() {
    check_loginTime();
    if (!getLoginToken()) {
        window.location = ROUTER_PATH;
    }
}

export function getLoginTIme() {
    return localStorage.getItem("dateTime")
}

export function setLoginTIme(dateTime) {
    localStorage.setItem("dateTime", dateTime);

}

export function checkLoginWithReturnTrueFalse() {
    if (!getLoginToken()) {
        return false
    } else {
        return true
    }
}


export function setRemeber(data) {

    var d = new Date();
    var cookieDays = 7;
    d.setTime(d.getTime() + (cookieDays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    setCookie('adminUsername', data.username, cookieDays);
    setCookie('adminPassword', data.password, cookieDays);
    setCookie('adminRemember', data.remember, cookieDays);
}

export function getRemeber() {
    var username = getCookie('adminUsername');
    var password = getCookie('adminPassword');
    var remember = getCookie('adminRemember');
    var data = {username: ((username != undefined) ? username : ''), password: ((password != undefined) ? password : ''),remember: ((remember != undefined) ? remember : '')}
   return data;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function check_loginTime() {
    var DATE_TIME = getLoginTIme();
    var server = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(server);
    const diffDuration = moment.duration(diff);

    if (diffDuration.days() > 0) {
        logout();
    } else if (diffDuration.hours() > 0) {
        logout();
    } else if (diffDuration.minutes() > LOGIN_DIFFERENCE) {
        logout();
    }
}

export function getLoginToken() {
    return localStorage.getItem("marketing_membertoken")
}

export function setLoginToken(token) {
    localStorage.setItem("marketing_membertoken", token);
}

export function logout() {
   postData('member/auth/member_logout', getLoginToken(ROUTER_PATH));
    localStorage.removeItem('marketing_membertoken');
    localStorage.removeItem('marketing_memberid');
    window.location = ROUTER_PATH;
}

export function getFirstname() {
    return localStorage.getItem("firstname")
}

export function setFirstname(firstname) {
    localStorage.setItem("firstname", firstname);
}

export function getPercentage() {
    return localStorage.getItem("Percentage")
}

export function setPercentage(Percentage) {
    localStorage.setItem("Percentage", Percentage);
}

export function getPinToken() {
    return localStorage.getItem("marketing_memberid")
}
export function getOCSIdToken() {
    return localStorage.getItem("marketing_memberid")
}

export function setPinToken(token) {
    localStorage.setItem("marketing_memberid", token);
}

export function destroyPinToken() {
    localStorage.removeItem('marketing_memberid');
    window.location = 'admin/Login/dashboard';
}

export function getPermission() {
    return localStorage.getItem("permission")
}

export function setPermission(permission) {
    localStorage.setItem("permission", permission);
}

export function getQueryStringValue(key)
{
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

export function postData(url, data) {
    var request_data = {token: getLoginToken(), data: data}
    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: JSON.stringify(request_data)
        }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }

                    // if token is verified then update date client side time
                    if (responseJson.status) {
                      //  setLoginTIme(moment())
                    }

                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export function IsValidJson() {
    return true;
}



export function handleDateChangeRaw(e) {
    e.preventDefault();
}

export function getTimeAgoMessage(DATE_TIME) {
    moment.tz.setDefault("Australia/Melbourne");
    var messageTime = moment(DATE_TIME)

    var currentDateTime = moment()

    const diff = currentDateTime.diff(messageTime);
    const diffDuration = moment.duration(diff);


    if (diffDuration.days() > 0) {
        return diffDuration.days() + ' Days.'
    } else if (diffDuration.hours() > 0) {
        return diffDuration.hours() + ' hr.'
    } else if (diffDuration.minutes() > 0) {
        return diffDuration.minutes() + ' Min.'
    } else {
        return 'Just now'
    }
}
export function postImageData(url, data) {
    data.append('id', getOCSIdToken());
    data.append('token', getLoginToken());

    return new Promise((resolve, reject) => {
        fetch(BASE_URL + url, {
            method: 'POST',
            body: data
        }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.token_status) {
                        logout();
                    }
                    if (responseJson.server_status) {
                        logout();
                    }

                    // if token is verified then update date client side time
                    if (responseJson.status) {
                        setLoginTIme(moment())
                    }

                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
    });
}

export const downloadAttachment = (url) => {
//    window.location.href = url;
    window.open(url, 'name');
}

export function handleShareholderNameChange(obj, stateName, index, fieldName, value) {
    var state = {};
    var tempField = {};
    var List = obj.state[stateName];
    List[index][fieldName] = value

    state[stateName] = List;
    obj.setState(state);
}

export function getOptionsOrganisationName(e) {
     if (!e || e.length < 1) {
        return Promise.resolve({ options: [] });
    }

    return postData('common/Common/get_org_name', { search: e,}).then((json) => {
        return { options: json };
    });
}


