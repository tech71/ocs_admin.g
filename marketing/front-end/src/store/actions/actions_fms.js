import {postData} from '../../service/common.js';
export const FMS_DETAILS = 'FMS_DETAILS'
export const FMS_BY_POST_CODE = 'FMS_BY_POST_CODE'

export const updateFmsDetails = (data) => ({
        type: FMS_DETAILS,
        data
})

// ascronus middleware for fetch data member graph details
function fetchFmsDetails(request) {
    return dispatch => {
        return postData('marketing/fms/get_fms_details', request).then((result) => {
            if (result.status) {
                dispatch(updateFmsDetails(result.data))
            }
        });
    }
}

// middle ware for set data of mail content
export function getFmsDetails(request) {
    return (dispatch, getState) => {
        return dispatch(fetchFmsDetails(request))
    }
}


// middle ware for set data of mail content
export function getFmsByPostCode(request) {
    return (dispatch, getState) => {
        return dispatch(fetchFmsByPostCode(request))
    }
}

// ascronus middleware for fetch data member graph details
function fetchFmsByPostCode(request) {
    return dispatch => {
        return postData('marketing/fms/get_fms_by_post_code', request).then((result) => {
            if (result.status) {
                dispatch(updateFmsByPostCode(result.data))
            }
        });
    }
}

export const updateFmsByPostCode = (data) => ({
        type: FMS_BY_POST_CODE,
        data
})