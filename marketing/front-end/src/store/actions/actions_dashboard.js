import {postData} from '../../service/common.js';
export const DASHBOARD_GRAPH_DETAILS = 'DASHBOARD_GRAPH_DETAILS'

export const updateDashboardGraphDetails = (data) => ({
        type: DASHBOARD_GRAPH_DETAILS,
        data
    })

// ascronus middleware for fetch data member graph details
function fetchDashboardGraphDetails(request) {
    return dispatch => {
        return postData('marketing/Dashboard/dashboard_graph_details', request).then((result) => {
            if (result.status) {
                dispatch(updateDashboardGraphDetails(result.data))
            }
        });
    }
}

// middle ware for set data of mail content
export function getDashboardGraphDetails(request) {
    return (dispatch, getState) => {
        return dispatch(fetchDashboardGraphDetails(request))
    }
}
