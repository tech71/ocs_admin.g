import {postData} from '../../service/common.js';
export const ORGANISATION_DETAILS = 'ORGANISATION_DETAILS'


export function orgDetails(data) {
  return dispatch => {
    return postData('marketing/organisation/get_org_details',data)
    .then((json) => {
      if(json.status){
        dispatch(request(json.data));
        return json;
      }
    });
  };
  function request(request) { return { type: ORGANISATION_DETAILS,data: request } }
}
