import {postData} from '../../service/common.js';
export const MEMBER_GRAPH_DETAILS = 'MEMBER_DETAILS'

export const updateMemberGraphDetails = (data) => ({
        type: MEMBER_GRAPH_DETAILS,
        data
})

// ascronus middleware for fetch data member graph details
function fetchMemberGraphDetails(request) {
    return dispatch => {
        return postData('marketing/member/member_graph_details', request).then((result) => {
            if (result.status) {
                dispatch(updateMemberGraphDetails(result.data))
            }
        });
    }
}

// middle ware for set data of mail content
export function getMemberGraphDetails(request) {
    return (dispatch, getState) => {
        return dispatch(fetchMemberGraphDetails(request))
    }
}
