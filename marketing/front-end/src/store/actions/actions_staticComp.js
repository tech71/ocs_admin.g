import {postData} from '../../service/common.js';
import moment from 'moment-timezone';
export const SIDEBAROPEN = 'sidebarOpen';
export const SIDEBARCLOSE = 'closeSidebar';
export const LOGIN = 'logIn';
export const LOGOUT = 'logOut';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const GET_PROFILE = 'GET_PROFILE';
export const LOADERSTATE = 'LOADERSTATE';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const STATISTICS_COUNTS = 'STATISTICS_COUNTS';
export const PARTICIPANT_POSTCODE = 'PARTICIPANT_POSTCODE';
export const PROFILE_UPDATE = 'PROFILE_UPDATE';
export const LOGIN_SUCCESS_TWO_FACTOR_LOGIN = 'LOGIN_SUCCESS_TWO_FACTOR_LOGIN';

export  function login(data) {
  return dispatch => {
     dispatch(request({ data }));
    return postData('marketing/login/check_login',  data).then((json) => {
      if(json.status){
        if(json.two_factor_login!='1')
        {
            localStorage.setItem("marketing_membertoken", json.token);
            localStorage.setItem("dateTime",moment());
            dispatch(success(json.success));
            return json;
        }else{ 
            dispatch(twoFactorLogin(json));
            return json;
        }            
      } else {
        dispatch(failure(json.error));
      }

      })
      .catch(error => dispatch(failure(error.toString())));
  };
    function request(request) { return { type: LOGIN,request: request } }
    function success(success) { return { type: LOGIN_SUCCESS,success: success } }
    function failure(error) { return { type: LOGIN_FAILURE,error: error } }
    function twoFactorLogin(info) { return { type: LOGIN_SUCCESS_TWO_FACTOR_LOGIN,info:info } }
}
export function forgot_password(data) {
  return dispatch => {
  //    dispatch(request({ data }));
    return postData('marketing/login/request_reset_password',  data);

  };
  function request(request) { return { type: FORGOT_PASSWORD,request: request } }
}
export function reset_password(data) {
  return dispatch => {
    return postData('marketing/login/reset_password', data);
};

}
export function verify_token(data) {
  return dispatch => {
    return postData('marketing/login/verify_reset_password_token',  data);
  };
}

export  function get_profile() {
  return dispatch => {
     // dispatch(request({ data }));
    return postData('marketing/profile')
    .then((json) => {
      if(json.status){
        dispatch(request(json.data));
        return json;
      }
    });
  };
  function request(request) { return { type: GET_PROFILE,request: request } }
}
export  function updateProfile(data) {
  return dispatch => {
     // dispatch(request({ data }));
    return postData('marketing/profile/profile_update',data)
    .then((json) => {
      if(json.status){
        dispatch(request(json.success));
        return json;
      }
    });
  };
  function request(request) { return { type: PROFILE_UPDATE,request: request } }
}

export function statistics_counts(data) {
  return dispatch => {
    return postData('marketing/dashboard/statistics_counts',  data).then((json) => {
      console.log(json);
      if(json.status){
        dispatch(request(json.success));
        return json;
      } else {
         // dispatch(failure(json.error));
      }
      })
};
function request(request) { return { type: STATISTICS_COUNTS,request: request } }
}

export function participant_postcode(data) {
  return dispatch => {
    return postData('marketing/Participant/participant_postcode',  data).then((json) => {
      console.log("json");
      if(json.status){
        dispatch(request_participant_postcode_data(json.success));
        return json;
      } else {
         // dispatch(failure(json.error));
      }
      })

};
function request_participant_postcode_data(request) { return { type: PARTICIPANT_POSTCODE,request: request } }
}

export function loader(loadState){return {type:LOADERSTATE, loadState:loadState}}
