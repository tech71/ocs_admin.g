import {postData} from '../../service/common.js';
export const PARTICIPANT_POSTCODE_DATA = 'PARTICIPANT_POSTCODE_DATA'
export const PARTICIPANT_GRAPH_DETAILS = 'PARTICIPANT_GRAPH_DETAILS'



export function getParticipantPostcodeDetails(data) {
    return dispatch => {

        return postData('marketing/participant/get_participant_postcode_data', data).then((json) => {
            if (json.status) {
                dispatch(request(json.data));
                return json;
            }
        });
    }

    function request(request) {
        return {type: PARTICIPANT_POSTCODE_DATA, data: request}
    }
}

// ascronus middleware for fetch data member graph details
export function getParticipantGraphDetails(data) {

    return dispatch => {
        return postData('marketing/participant/get_participant_graph_details', data).then((result) => {
            if (result.status) {
                dispatch(request(result.data))
            }
        });
    }
    
    function request(data){ return{
        type: PARTICIPANT_GRAPH_DETAILS,
        data
    }}
}

