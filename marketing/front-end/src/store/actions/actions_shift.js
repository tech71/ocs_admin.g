import {postData} from '../../service/common.js';
export const SHIFT_GRAPH_DETAILS = 'SHIFT_GRAPH_DETAILS'
export const SHIFT_BY_POST_CODE = 'SHIFT_BY_POST_CODE'

export const updateShiftGraphDetails = (data) => ({
        type: SHIFT_GRAPH_DETAILS,
        data
})

// ascronus middleware for fetch data member graph details
function fetchShiftGraphDetails(request) {
    return dispatch => {
        return postData('marketing/shift/shift_graph_details', request).then((result) => {
            if (result.status) {
                dispatch(updateShiftGraphDetails(result.data))
            }
        });
    }
}

// middle ware for set data of mail content
export function getShiftGraphDetails(request) { 
    return (dispatch, getState) => {
        return dispatch(fetchShiftGraphDetails(request))
    }
}


// middle ware for set data of mail content
export function getShiftDetailsWithPostCode(request) {
    return (dispatch, getState) => {
        return dispatch(fetchShiftByPostCode(request))
    }
}

// ascronus middleware for fetch data member graph details
function fetchShiftByPostCode(request) {
    return dispatch => {
        return postData('marketing/shift/get_shift_by_post_code', request).then((result) => {
            if (result.status) {
                dispatch(updateShiftByPostCode(result.data))
            }
        });
    }
}

export const updateShiftByPostCode = (data) => ({
        type: SHIFT_BY_POST_CODE,
        data
})