import * as actionType from '../actions/index.js';

const initialState = {
    onboarded_participant_count_sp: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 1
    },
    inactive_participant_count_sp: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 1
    },
    avg_onboarded_participant_count_sp: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 1
    },
    active_participant_count_sp: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 1
    },
    active_with_shift_count_sp: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 1
    },
    participant_postcode: [],
    participant_count_title: {
        current: "",
        previous: "",
        percentage: ""
    },
    participant_intake_graph: {
        row1: [
            0,
            0,
            0
        ],
        row2: [
            0,
            0,
            0
        ]
    },
    participant_dropdown: [
        {value: 'new', label: 'New'},
        {value: 'existing', label: 'Existing'},
        {value: 'active_shift', label: 'Active With at least one Shift'},
        {value: 'inactive_not_archived', label: 'Inactive And not archived'},
    ],
    compare_with_dropdown: [
        {value: 'participant', label: 'Participant'},
        {value: 'member', label: 'Member'},
        {value: 'shift', label: 'Shifts'},
        {value: 'fms', label: 'FMS cases'},
        {value: 'org', label: 'Organisations'},
    ],
    special_dropdown: {
        participant: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_shift', label: 'Active With at least one Shift'},
            {value: 'inactive_not_archived', label: 'Inactive And not archived'},
        ],
        member: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_shift', label: 'Active with Shifts'},
            {value: 'without_shift', label: 'Without shifts'},
        ],
        shift: [
            {value: 'request_by_particpant', label: 'Requested by Participant'},
            {value: 'filled_by_member', label: 'Filled by Member'},
            {value: 'shift_cancel_by_member', label: 'Shifts cancelled by Member'},
            {value: 'shift_cancel_by_participant', label: 'Shifts cancelled by participant'},
        ],
        fms: [
            {value: 'rep_by_participant', label: 'Reported by Participant'},
            {value: 'rep_by_member', label: 'Reported by Member'},
            {value: 'rep_by_org', label: 'Reported by Organisation'},
        ],
        org: [],
    },
    participant_match_with_postcode: [],
    hot_cold_place_title: '',
    onboard_cold_place: [],
    onboard_hot_place: [],
    fill_shift_hot_place: [],
    fill_shift_cold_place: [],
    graph_shifts: [],
    comparison_by_funding_type: {
        private: 0,
        ndis: 0,
        welfare: 0
    }
}

const reducer_participant = (state = initialState, action) => {
    switch (action.type) {
        case actionType.PARTICIPANT_POSTCODE_DATA:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data
            }
        case actionType.PARTICIPANT_GRAPH_DETAILS:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data
            }

    }

    return state;
};

export default reducer_participant;
