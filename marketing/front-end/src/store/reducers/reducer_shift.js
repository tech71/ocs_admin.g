import * as actionType from '../actions/index.js';

const initialState = {
    shift_by_post_code:[],
    graph_data:[],
    shift_hour_count:{
        shift_filled:{},
        shift_requested_by_participant:{},
        avg_shift_filled:{},
        shift_requested_via_participant_portal:{},
        shift_manually_alloted:{},
    },
    shifts_stats:{
        shift_requested_hot_spots:[],
        shift_filled_hot_spots:[],
        shift_requested_cold_spots:[],
        shift_filled_cold_spots:[]
    },
    server_view_type:[],
    shift_postcode:[],
    options_shift:[
        {value: 'participant_request', label: 'Requested by Participant'},
        {value: 'member_filled', label: 'Filled by Member'},
    ],
    compare_with_dropdown: [
        {value: 'participant', label: 'Participant'},
        {value: 'member', label: 'Member'},
        {value: 'shift', label: 'Shifts Type'},
        {value: 'org', label: 'Organisations'},
        {value: 'fms', label: 'FMS cases'}
    ],
    special_dropdown: {
        participant: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_with_at_least_one_shift', label: 'Active With at least one Shift'},
            {value: 'inactive_and_not_archived', label: 'Inactive And not archived'}
        ],
        member: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_shift', label: 'Active With Shifts'},
            {value: 'without_shift', label: 'Without shifts'},
        ],
        shift: [
            {value: 'am', label: 'Morning'},
            {value: 'pm', label: 'Evening'},
            {value: 'ao', label: 'Active Overnight'},
            {value: 'so', label: 'Sleepover'},
            {value: 'transferred', label: 'Transferred'},
        ],
        org: [
            {value: 'house', label: 'House'},
            {value: 'site', label: 'Site'}
        ],
        fms: [
            {value: 'rep_by_participant', label: 'Reported by Participant'},
            {value: 'rep_by_member', label: 'Reported by Member'},
            {value: 'rep_by_org', label: 'Reported by Organisation'},
        ],
    }
   }


const reducer_shift = (state = initialState, action) => {
    switch (action.type) {
        case actionType.SHIFT_GRAPH_DETAILS:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data}

        case actionType.SHIFT_BY_POST_CODE:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, shift_by_post_code:data}

        default :
    }
    return state;
};
export default reducer_shift;