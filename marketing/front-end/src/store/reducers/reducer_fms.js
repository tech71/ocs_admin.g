import * as actionType from '../actions/index.js';

const initialState = {
    fms_by_post_code:[],
   	hour_count:{
   		avg_fms:{},
   		member_reported:{},
   		org_reported:{},
   		participant_reported:{},
   	},
   	fms_stats:{
        member_cold_spots:[],
        member_hot_spots:[],
        org_cold_spots:[],
        org_hot_spots:[],
        participant_cold_spots:[],
        participant_hot_spots:[],
        other_hot_spots:[],
        other_cold_spots:[],
   	},
   	server_view_type:[],
    shift_postcode:[],
    compare_with_dropdown: [
        {value: 'participant', label: 'Participant'},
        {value: 'member', label: 'Member'},
        {value: 'org', label: 'Organisations'},
    ],
    special_dropdown: {
        participant: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_with_at_least_one_shift', label: 'Active With at least one Shift'},
            {value: 'inactive_and_not_archived', label: 'Inactive And not archived'}
        ],
        member: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_shift', label: 'Active With Shifts'},
            {value: 'without_shift', label: 'Without shifts'},
        ],
        org: [],
    }
   }

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.FMS_DETAILS:
             var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data} 

        case actionType.FMS_BY_POST_CODE:
             var data = JSON.parse(JSON.stringify(action.data));
            return {...state, fms_by_post_code:data}
        default :
    }
    return state;
};
export default reducer;


