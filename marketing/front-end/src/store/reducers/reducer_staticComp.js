import * as actionType from '../actions/index.js';

const initialState = {
    sidebarState: '',
    loginState: false,
    loginError:'',
    loginSuccess:'',
    profileUpdate:'',
    summary:[],
    processedInvoice:[],
    loading:false,
    graphdata:[],
    forgot_password:false,
    statistics_counts:''
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.SIDEBAROPEN:
            // console.log('sidebaropen') ;
            const prevSideState = state.sidebarState;
            if (prevSideState === action.pos) {
                return {
                    ...state,
                    sidebarState: ''
                }
            }
            else {
                return {
                    ...state,
                    sidebarState: action.pos
                }
            }
            case actionType.SIDEBARCLOSE:
            // console.log('logout') ;
            return {
                sidebarState: ''
            }
            case actionType.LOGOUT:
            //console.log('logout') ;
            return {
                sidebarState: '',
                loginState: false
            }
            case actionType.FORGOT_PASSWORD:
            // console.log('logout') ;
            return {
                sidebarState: '',
                loginState: true
            }
            case actionType.LOGIN:
            return {
                loginState: false
            }
            case actionType.LOGIN_SUCCESS:
            return {
                loginState: true,
                loginSuccess:action.success
            }
            case actionType.PROFILE_UPDATE:

            return {
                loginState: true,
                profileUpdate:action.request
            }			
            case actionType.LOGIN_FAILURE:

            return {
                loginState: false,
                loginError:action.error
            }
            case actionType.STATISTICS_COUNTS:

            return {
                loginState: false,
                loginError:action.error
            }
            case actionType.LOGIN_SUCCESS_TWO_FACTOR_LOGIN:

            return {
                loginState: true,
                enableTwoFactorLogin:true,
                info:action.info
            }
            default :
        }
        return state;
    };
    export default reducer;
