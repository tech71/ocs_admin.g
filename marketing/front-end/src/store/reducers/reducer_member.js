import * as actionType from '../actions/index.js';

const initialState = {
    onboard_member_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    avg_onboard_member_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    active_member_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    accepted_shift_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    count_not_accepted_shift_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    re_applied_member_count: {
        current: 0,
        previous: 0,
        percent: 0,
        status: 0
    },
    member_work_area_graph: {
      ndis: 0,
      welfare: 0
    },
    onboard_cold_place: [],
    onboard_hot_place: [],
    fill_shift_hot_place: [],
    fill_shift_cold_place: [],
    member_count_title: [],
    hot_cold_place_title: '',
    graph_shifts: [],
    member_intake_graph: {row1: [], row2: []},
    recruitment_stage: [],
    member_postcode: [],
    member_dropdown: [
        {value: 'new', label: 'New'},
        {value: 'existing', label: 'Existing'},
        {value: 'active_shift', label: 'Active Shift'},
        {value: 'without_shift', label: 'Without shifts'},
    ],
    compare_with_dropdown: [
        {value: 'participant', label: 'Participant'},
        {value: 'member', label: 'Member'},
        {value: 'shift', label: 'Shifts'},
        {value: 'fms', label: 'FMS cases'},
        {value: 'org', label: 'Organisations'},
    ],
    special_dropdown: {
        participant: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_with_at_least_one_shift', label: 'Active With at least one Shift'},
            {value: 'inactive_and_not_archived', label: 'Inactive And not archived'}
        ],
        member: [
            {value: 'new', label: 'New'},
            {value: 'existing', label: 'Existing'},
            {value: 'active_shift', label: 'Active Shift'},
            {value: 'without_shift', label: 'Without shifts'},
        ],
        shift: [
            {value: 'request_by_particpant', label: 'Requested by Participant'},
            {value: 'filled_by_member', label: 'Filled by Member'},
            {value: 'shift_cancel_by_member', label: 'Shifts cancelled by Member'},
            {value: 'shift_cancel_by_participant', label: 'Shifts cancelled by participant'},
        ],
        fms: [
            {value: 'rep_by_participant', label: 'Reported by Participant'},
            {value: 'rep_by_member', label: 'Reported by Member'},
            {value: 'rep_by_org', label: 'Reported by Organisation'},
        ],
        org: [],
    },
    
}

const reducer_member = (state = initialState, action) => {

    switch (action.type) {
        case actionType.MEMBER_GRAPH_DETAILS:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data}

        default :
    }
    return state;
};



export default reducer_member;
