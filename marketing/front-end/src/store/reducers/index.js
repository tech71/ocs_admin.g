import { combineReducers } from 'redux';

import reducer_staticComp from './reducer_staticComp';
import reducer_participant from './reducer_participant';
import reducer_org from './reducer_org';
import reducer_member from './reducer_member';
import reducer_shift from './reducer_shift';
import reducer_dashboard from './reducer_dashboard';
import reducer_fms from './reducer_fms';


export default combineReducers({
    reducer_staticComp,reducer_participant,reducer_org, reducer_member,reducer_shift,reducer_fms, reducer_dashboard
});
