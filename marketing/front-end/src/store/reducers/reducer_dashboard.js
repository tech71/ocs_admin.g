import * as actionType from '../actions/index.js';

const initialState = {
     onboard_member_count_sp: {
      "current": 0,
      "previous": 1,
      "percent": 100,
      "status": 2
    },
    fms_count_sp: {
      "current": 0,
      "previous": 0,
      "percent": 0,
      "status": 3
    },
    filled_shift_hours_sp: {
      "current": 0,
      "previous": 0,
      "percent": 0,
      "status": 3
    },
    onboarded_participant_count_sp: {
      "current": 0,
      "previous": 0,
      "percent": 0,
      "status": 3
    },
    onboard_org_count_sp: {
      "current": 4,
      "previous": 0,
      "percent": 400,
      "status": 1
    },
   
    member_intake_graph: {row1: [], row2: []},
    dashboard_count_title: {
        percentage: '',
    },
    org_comparison: {
      parent_org_count: 0,
      sub_org_count: 0,
      site_count: 0
    },
    member_vs_participant_intake: {row1: [], row2: []},
}

const reducer_dashboard = (state = initialState, action) => {

    switch (action.type) {
        case actionType.DASHBOARD_GRAPH_DETAILS:
            var data = JSON.parse(JSON.stringify(action.data));
            return {...state, ...data}

    }
    return state;
};



export default reducer_dashboard;
