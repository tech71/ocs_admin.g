export const COLOR1 = '#bac9d3';
export const COLOR2 = '#5a87a3';
export const COLOR3 = '#29658a';
export const COLOR4 = '#5f8ca8';
export const COLOR5 = '#c9d8e2';

export const COLOR6 = '#354d5c';
export const COLOR7 = '#2f5973';
export const COLOR8 = '#2c5c7a';
export const COLOR9 = '#c9d8e2';
export const COLOR10 = '#dbe5ec';

export const GREENCOLOR1 = '#1db259';
export const GREENCOLOR2 = '#43bb73';
export const GREENCOLOR3 = '#b7dcc6';
export const GREENCOLOR4 = '#8aa595';

export const PARTICIPANTCOLOR1 = '#b175ff';
export const PARTICIPANTCOLOR2 = '#bb8afa';
export const PARTICIPANTCOLOR3 = '#dbccef';
export const PARTICIPANTCOLOR4 = '#c497fe';
export const PARTICIPANTCOLOR5 = '#ebdcff';
export const PARTICIPANTCOLOR6 = '#f9f6fd';
export const PARTICIPANTCOLOR7 = '#ffffff';
export const PARTICIPANTCOLOR8 = '#992bff';
export const PARTICIPANTCOLOR9 = '#b360ff';
export const PARTICIPANTCOLOR10 = '#e5caff';

export const SHIFTCOLOR1 = '#e07196';
export const SHIFTCOLOR2 = '#e287a6';
export const SHIFTCOLOR3 = '#e8ccd6';
export const SHIFTCOLOR4 = '#e8dee2';
export const SHIFTCOLOR5 = '#fefefe';

export const MEMBERCOLOR1 = "#fd8865";
export const MEMBERCOLOR2 = "#f9a287";
export const MEMBERCOLOR3 = "#eed1c8";
export const MEMBERCOLOR4 = "#ff8a67";
export const MEMBERCOLOR5 = "#ffa88d";
export const MEMBERCOLOR6 = "#ffe2d9";
export const MEMBERCOLOR7 = "#fff9f7";
export const MEMBERCOLOR8 = "#ffffff";

export const MEMBERCOLOR9 = "#8b513f";
export const MEMBERCOLOR10 = "#c46040";
export const MEMBERCOLOR11 = "#d96642";
export const MEMBERCOLOR12 = "#ff7043";
export const MEMBERCOLOR13 = "#ff9472";
export const MEMBERCOLOR14 = "#ffb7a1";
export const MEMBERCOLOR15 = "#fecfc0";

export const ORGANISATION1 = "#1eb35a";
export const ORGANISATION2 = "#45bd75";
export const ORGANISATION3 = "#b8ddc7";
export const ORGRES = "#d32f2f";