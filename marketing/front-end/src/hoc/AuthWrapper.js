import React from 'react';

const AuthWrapper = (props) => {
    return (
        <React.Fragment>
            <section className='authwrapperSec'>
                <div className='container-fluid'>
                    <div className='row justify-content-center '>
                        <div className='col-xl-10 col-lg-11 col-md-12 col-12'>
                            {props.children}
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
}


export default AuthWrapper;
