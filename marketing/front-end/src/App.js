import React, { Component } from 'react';
import { ROUTER_PATH } from './config.js';
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import ScrollToTop from 'react-router-scroll-top';
import { checkItsNotLoggedIn, checkLoginWithReturnTrueFalse } from './service/common.js';
// components imports
import { ToastContainer } from 'react-toastify';
import Login from './components/webs/Login';
import ForgotPassword from './components/webs/ForgotPassword';
import TwoFactorLogin from './components/webs/TwoFactorLogin';
import Loader from './components/utils/Loader';
import Dashboard from './components/Dashboard';
import Participants from './components/Participants';
import Members from './components/Members';
import Organisations from './components/Organisations';
import Shifts from './components/Shifts';
import Fms from './components/Fms';
import Configuration from './components/Configuration';
import Profile from './components/Profile';
import UI from './components/UI';

//module css imports
import 'react-select-plus/dist/react-select-plus.css';
import "react-datepicker/dist/react-datepicker.css";
import './App.css';
import './responsive.css';

import './service/custom_script.js';
import {getPermission} from './service/common';
class App extends Component {
  constructor(props) {
        super(props);

        this.state = {}
  }
  render() {
    return (
      <React.Fragment>
        {this.props.loading ? <Loader /> : null}
        <section className={'main_wrapper ' + (this.props.sidebarState === 'right' ? ' toggle_right' : (this.props.sidebarState === 'left') ? ' toggle_left' : '')} >
          <Router>
            <ScrollToTop >
              <Switch>
                <Route exact path={'/'} render={() => <Login auth={this.props} />}  />
                <Route exact path={'/forgot_password'} render={() => <ForgotPassword />} />
                <Route exact path={'/dashboard'} render={(props) => <Dashboard {...props} />} />
                <Route exact path={'/login_otp'} render={(props) => <TwoFactorLogin props={props}/>} />
                <Route exact path={'/dashboard'} render={() => <Dashboard />} />
                <Route exact path={'/participants'} render={() => <Participants />} />
                <Route exact path={'/members'} render={() => <Members />} />
                <Route exact path={'/organisations'} render={() => <Organisations />} />
                <Route exact path={'/shifts'} render={() => <Shifts />} />
                <Route exact path={'/fms'} render={() => <Fms />} />
                <Route exact path={'/configuration'} render={() => <Configuration />} />
                <Route exact path={'/profile'} render={() => <Profile />} />
                <Route exact path={'/ui'} render={() => <UI />} />
                <Route exact path={'*'} render={() => 'not found'} />
              </Switch>
            </ScrollToTop>
          </Router>
          <ToastContainer />
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    sidebarState: state.reducer_staticComp.sidebarState,
    loading: state.reducer_staticComp.loading,
    loginState: state.reducer_staticComp.loginState
  };
};
export default connect(mapStateToProps, null)(App);
