export const ROUTER_PATH = '/';
export const LOGIN_DIFFERENCE = 60; // 60 minits;
export const GOOGLE_MAP_KEY = process.env.REACT_APP_OCS_GOOGLE_MAP_KEY;
export const WS_URL = process.env.REACT_APP_OCS_WS_URL;
export const PIN_DATA = {'fms':'1','admin':'2','incident':'3'};
export const PAGINATION_SHOW = 0;
export const BASE_URL = process.env.REACT_APP_OCS_API_HOST;
//export const BASE_URL = 'https://marketingapi.dev.healthcaremgr.net/';

export const DATA_CONSTANTS = {
    FILTER_WAIT_INTERVAL : 1000,
    FILTER_ENTER_KEY : 13
};
