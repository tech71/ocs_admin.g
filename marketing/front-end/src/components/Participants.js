import React, { Component } from 'react';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import GMap from './utils/GMap';
import { Chart } from "react-google-charts";
import * as CT from '../hoc/Color';
import GraphDoughnut from './utils/GraphDoughnut';
import StackedBarGraph from './utils/StackedBarGraph';
import MainHeading from './utils/MainHeading';
import FunnelGraph from './utils/FunnelGraph';
import HotColdSpotPlaceCount from './participant/HotColdSpotPlaceCount';
import ParticipantSpecificCount from './participant/ParticipantSpecificCount';
import { getParticipantPostcodeDetails, getParticipantGraphDetails } from '../store/actions';
import {ChronologicalYearMonth} from './utils/ChronologicalYearMonth';
import { getOptionsOrganisationName } from '../service/common.js';

class Participants extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view_type:'year',
            selected_participant_type: 'new',
            participant_compare_with: 'participant',
            participant_compare_with_specific: 'new',
        };

    }

    componentWillMount(){
      var req = {view_type: this.state.view_type};
      this.getParticipantPostcodeDetails(req);
      this.props.getParticipantGraphDetails(req);
      this.setState({filter:"1",attribute:"participant",type:"11"})
    }


    onChangeFIlterPostocde(key, value) {
        var state = {}
        if(key === 'participant_compare_with'){
            state[key] = '';
        }
        state[key] = value;
        
        this.setState(state, () => {
            this.getParticipantPostcodeDetails();
        });
    }
    
    getParticipantPostcodeDetails = () => {
        var req = {selected_participant_type: this.state.selected_participant_type, participant_compare_with: this.state.participant_compare_with, participant_compare_with_specific: this.state.participant_compare_with_specific};
        this.props.getParticipantPostcodeDetails(req);
    }
  
    onChangeFIlter=(key, value)=>{
        var state = {}
        if(key === 'participant_compare_with'){
            state[key] = '';
        }
        state[key] = value;
        
        this.setState(state,()=>{
            if(value == 'current_month' || value == 'year'){
                this.setState({specific_month: ''});
            }
             
            if(value != 'selected_month'){
                var req = {view_type: this.state.view_type, specific_month: this.state.specific_month, selected_member_type: this.state.selected_member_type, member_compare_with: this.state.member_compare_with, member_compare_with_specific: this.state.member_compare_with_specific};
                this.props.getParticipantGraphDetails(req);
            }
        });
    }

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    render() {

        /* ------- change api data formate in actual required intake graph data -------- */
        var funnelData = {
            data1: this.props.participant_intake_graph.row1,
            data1Bg: [CT.PARTICIPANTCOLOR8, CT.PARTICIPANTCOLOR9, CT.PARTICIPANTCOLOR10],
            data2: this.props.participant_intake_graph.row2,
            data2Bg: [CT.COLOR3, CT.COLOR4, CT.COLOR5]
        }
        
            var comparison_by_funding_type = this.props.comparison_by_funding_type;
            let circleData = {
                labels: ['NDIS', 'Welfare', 'Private'],
                datasets: [{
                    data: [comparison_by_funding_type.ndis, comparison_by_funding_type.welfare, comparison_by_funding_type.private],
                    backgroundColor: [CT.PARTICIPANTCOLOR1, CT.PARTICIPANTCOLOR2, CT.PARTICIPANTCOLOR3]
                }],
            };
       

         
        /* ----- chnage api data formate in actual required graph data -------*/
        var tooltip = {'role' : "tooltip", 'type': "string", 'p': {'html': true}};
        var shiftGraphLabel = ['Month', 'filled shift', tooltip, 'cancelled shift', tooltip, 'No suitable member available', tooltip];
        
        var shiftGraphData = this.props.graph_shifts;
         shiftGraphData = this.props.graph_shifts.map((val, index) => {
            return [(val.current_term), 
                    parseFloat(val.total_welfare_unfilled_shift), '<div>Total shift requested:'+val.total_ndis_unfilled_shift + '</div><div>From welfare:'+val.total_welfare_unfilled_shift + '</div><div>From NDIS funeded:'+val.total_ndis_unfilled_shift + '</div>',
                    parseFloat(val.total_ndis_unfilled_shift), '<div>Total shift requested:'+val.total_ndis_unfilled_shift + '</div><div>From welfare:'+val.total_welfare_unfilled_shift + '</div><div>From NDIS funeded:'+val.total_ndis_unfilled_shift + '</div>',
                    parseFloat(val.total_filled_shift), '<div>Total shift filled:</div><div>'+val.total_filled_shift+'</div>'+ '<div>'+val.filled_shift_sum+' hrs</div><div>Average '+val.filled_shift_avg+' hrs per day</div>',
                ]
        });
        shiftGraphData = [shiftGraphLabel, ...shiftGraphData]
        
         /* -------------ready data form member postcode map -------------------*/
        var ParticipantPostcode= [{type: 'member', posArr: this.props.participant_match_with_postcode}];

        return (

            <React.Fragment>
                <Header />
                <AuthWrapper>
                    <MainHeading name={"Participants"} />
                    <div className="bor_bot1 pb-4 mt-4 filter_area d-flex align-items-center">
                        <div className="vw_side"><strong>View:</strong></div>
                        <div className="d-flex align-items-center ">
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'current_month')}/>
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>
                            <div className="d-flex align-items-center">
                                <label className="cstm_radio">
                                    <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'selected_month')}/>
                                    <div className="rad_lab">Selected Month</div>
                                </label>
                                <div className="fil_datie">
                                     <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month'? false: true}
                                    />
                                </div>
                            </div>
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" checked={this.state.view_type == 'year' || ''} onChange={()=>this.onChangeFIlter('view_type', 'year')}/>
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-4 prog_inact_row">
                        <ParticipantSpecificCount {...this.props.onboarded_participant_count_sp} count_title="Participants currently being onboarded in" />
                        <ParticipantSpecificCount {...this.props.avg_onboarded_participant_count_sp} count_title="Days on average to onboard new Participants in" />
                        <ParticipantSpecificCount {...this.props.active_participant_count_sp} count_title="Active Participants in" />
                        <ParticipantSpecificCount {...this.props.active_with_shift_count_sp} count_title="Active Participants with shifts in" />
                        <ParticipantSpecificCount {...this.props.inactive_participant_count_sp} count_title="Inactive participants as of " />
                        <ParticipantSpecificCount {...this.props.participant_resigned_count_sp} count_title="Participants have re-signed up in" />
                    </div>

                    <div className="main_heading_cmn- mt-2 bor_top1">
                        <h3><strong>Participants by Postcode</strong></h3>
                    </div>

                    <div className="row mt-4 mb-4 justify-content-end">
                        <div className="col-xl-4 col-lg-5 d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2"><strong>Participant</strong></div>
                            <div className='cstm_select slct_cstm2 bg_grey flex-1'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Select status"
                                    options={this.props.participant_dropdown}
                                    onChange={(e) => this.onChangeFIlterPostocde('selected_participant_type', e)}
                                    value={this.state.selected_participant_type}
                                />
                            </div>
                        </div>
                        <div className="col-xl-7 col-lg-7  d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2 flex-1 text-center"><strong>Compared Against</strong></div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1 pl-2'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Module"
                                    options={this.props.compare_with_dropdown}
                                    onChange={(e) => this.onChangeFIlterPostocde('participant_compare_with', e)}
                                    value={this.state.participant_compare_with}

                                />
                            </div>
                           
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1  pl-2' >
                                {this.state.member_compare_with === 'org'? 
                                <Select.Async
                                        cache={false}
                                        name="form-field-name"
                                        clearable={false}
                                        value={this.state.participant_compare_with_specific}
                                        loadOptions={(e) => getOptionsOrganisationName(e)}
                                        placeholder='Search'
                                        onChange={(e) => this.onChangeFIlterPostocde('participant_compare_with_specific', e)}
                                    />: 
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Sub-Category"
                                    options={this.props.special_dropdown[this.state.participant_compare_with]}
                                    onChange={(e) => this.onChangeFIlterPostocde('member_compare_with_specific', e)}
                                    value={this.state.member_compare_with_specific}
                                />}
                            </div>
                        </div> 
                    </div>
                    {/* row ends */}

                    <div className="map_area mt-3 mb-2">
                        <GMap MarkersModule={{f_module:'participant', s_module: this.state.participant_compare_with}} position={ParticipantPostcode} />
                    </div>

                    <div className="main_heading_cmn- mt-4 bor_top1">
                        <h3><strong>Participant Statistics</strong></h3>
                    </div>


                    <div className="row mt-3 mb-3">
                        <HotColdSpotPlaceCount place={this.props.onboard_hot_place} color={CT.PARTICIPANTCOLOR1} title="Onbording Hot Spots" />
                        <HotColdSpotPlaceCount place={this.props.fill_shift_hot_place} color={CT.SHIFTCOLOR1} title="Filled shifts Hot Spots" />
                        <HotColdSpotPlaceCount place={this.props.onboard_cold_place} color={CT.PARTICIPANTCOLOR1} title="Filled shifts Hot Spots" />
                        <HotColdSpotPlaceCount place={this.props.fill_shift_cold_place} color={CT.SHIFTCOLOR1} title="Onbording Cold Spots" />
                    </div>
                   
                    <div className="row">
                        <div className="col-xl-6 col-lg-12 col-md-12 col-12 mb-2">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-5 mt-3 text-center">
                                    <strong>Intake Drop Out Rate by Section This Month:Last Year Vs This Year</strong>
                                </h5>

                                <div className="mb-1 mt-4 funnel_flex">
                                    <div className="l_col">
                                        <ul className=" tickie_ul ">
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR8 }}></span>
                                                <div>Stage 1 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR9 }}></span>
                                                <div>Stage 2 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR10 }}></span>
                                                <div>Stage 3 Last Year</div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="m_col">
                                        <FunnelGraph
                                            data={funnelData}
                                            type='funnel_2'
                                        />
                                    </div>
                                    <div className="r_col">
                                        <ul className="tickie_ul tickie_right">
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR3 }}></span>
                                                <div>Stage 1 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR4 }}></span>
                                                <div>Stage 2 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR5 }}></span>
                                                <div>Stage 3 this Year</div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                        <div className="col-xl-6 col-lg-12 col-md-12 col-12 mb-2">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-4 mt-3 text-center">
                                    <strong>Types of Participants Funding as of this Month</strong>
                                </h5>
                                <div className="d-flex align-items-end typesParti__ mt-5 parti_flx">
                                    <div className="partic_dough flex-1 pl-5">
                                        <GraphDoughnut data={circleData} />
                                    </div>
                                    <div className="legend_tickie">
                                        <ul className=" tickie_ul pl-5">
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR1 }}></span>
                                                <div>NDIS</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR2 }}></span>
                                                <div>Welfare</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR3 }}></span>
                                                <div>Private</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="row mt-3 mb-3">
                        <div className="col-12">

                            <div className="statis_bx__">
                                <h4 className="cmn_clr1 mg_ics_head  mt-2 text-center">
                                    <strong>Shifts Requested by Participants this Month</strong>
                                </h4>

                                <div>
                                    <StackedBarGraph
                                        data={shiftGraphData}
                                        xAxisTitle='Qty'
                                        yAxisTitle='Day of Month'
                                        colors={[CT.PARTICIPANTCOLOR1, CT.PARTICIPANTCOLOR3, CT.SHIFTCOLOR1]}
                                        series={{ 2: { type: 'line' } }}
                                        height={'500px'}
                                    />
                                </div>
                                <div className="mt-3">
                                    <ul className="d-flex tickie_ul pl-5">
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR1 }}></span>
                                            <div>Requests from NDIS Funded</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR2 }}></span>
                                            <div>Requests from Welfare Funded</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                            <div>Shifts Filled</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}


                </AuthWrapper>


            </React.Fragment>

        );
    }
}


const mapStateToProps = (state) => {
    return {
                ...state.reducer_participant
    }
};
export default connect(mapStateToProps,{getParticipantPostcodeDetails, getParticipantGraphDetails})(Participants);
