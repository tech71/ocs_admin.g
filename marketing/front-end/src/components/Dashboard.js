import React, { Component } from 'react';
import * as CT from '../hoc/Color';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import BarGraph from './utils/BarGraph';
import GraphDoughnut from './utils/GraphDoughnut';
import MainHeading from './utils/MainHeading';
import Select from 'react-select-plus';
import FunnelGraph from './utils/FunnelGraph';
import TotalCounts from './dashboard/TotalCounts';
import DashboardSpecificCount from './dashboard/DashboardSpecificCount';
import { getDashboardGraphDetails } from '../store/actions';
import {ChronologicalYearMonth, nth} from './utils/ChronologicalYearMonth';


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            view_type: 'current_month',
            funnelData: {
                data1:[50000, 40000, 30000],
                data1Bg : ['#b175ff', '#c497fe', '#ebdcff'],
                data2:[11000, 40000, 50000, 20000, 10000, 3000, 10000],
                data2Bg :['#ff8a67', '#ffa88d', '#fee1d8', '#fef8f6', '#fef8f6', '#fef8f6', '#ffffff']
            },
            org_comparison_active: {all: true, }
        };

    }

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };
    
    componentDidMount(){
        var req = {view_type: this.state.view_type};
        this.props.getDashboardGraphDetails(req);
    }
    
    onChangeFIlter=(key, value)=>{
        var state = {}
        if(key === 'member_compare_with'){
            state[key] = '';
        }
        state[key] = value;
        
        this.setState(state,()=>{
            if(value == 'current_month' || value == 'year'){
                this.setState({specific_month: ''});
            }
             
            if(value != 'selected_month'){
                var req = {view_type: this.state.view_type, specific_month: this.state.specific_month};
                this.props.getDashboardGraphDetails(req);
            }
        });
    }
    
    orgCompareGraphFilter = (e) => {
        var state = this.state.org_comparison_active;
        state[e.target.value] = e.target.checked;
        if(e.target.value == 'all' && e.target.checked){
            state['parnet_org'] = false;
            state['sub_org'] = false;
            state['site_comp'] = false;
        }else{
            state['all'] = false;
        }
        
        this.setState({org_comparison_active: state});
    }

    render() {
        console.log(this.props);
         /* ------- change api data formate in actual required intake graph data -------- */
        var funnelData = {
            data1: this.props.member_vs_participant_intake.row1,
            data1Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11],
            data2: this.props.member_vs_participant_intake.row2,
            data2Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11, CT.MEMBERCOLOR12, CT.MEMBERCOLOR13, CT.MEMBERCOLOR14, CT.MEMBERCOLOR15]
        }
        
        /* ---------- change api data in required formate and show according to required check box data -----------*/
        var ca = this.state.org_comparison_active;
        var i = 0;
        var comp_data = [];
        if(ca.all === true){
            comp_data = [this.props.org_comparison.parent_org_count, this.props.org_comparison.sub_org_count, this.props.org_comparison.site_count]
        } 
        if(ca.parnet_org === true){
            comp_data[i] = this.props.org_comparison.parent_org_count;
            i++;
        } 
        if(ca.sub_org === true){
            comp_data[i] = this.props.org_comparison.sub_org_count;
            i++;
        }
        if(ca.site_comp === true){
            comp_data[i] = this.props.org_comparison.site_count;
            i++;
        }
        
        const orgsData = {
            labels: ['Parent Org', 'Sub Org', 'Site'],
            datasets: [{
                data: comp_data,
                backgroundColor: [CT.GREENCOLOR1, CT.GREENCOLOR2, CT.GREENCOLOR3],
            }],
        };
        
        const graphdata = {
            labels: ['Data 1', 'Data 2', 'Data 3'],
            datasets: [{
                data: ['10', '5', '7', '0'],
                backgroundColor: [CT.COLOR1, CT.COLOR2, CT.COLOR3],
            }]

        };

        return (

            <React.Fragment>
                <Header />
                <AuthWrapper>
                    <MainHeading name={"Dashboard"} />

                    <div className="bor_bot1 pb-4 mt-4 filter_area d-flex align-items-center">
                        <div className="vw_side"><strong>View:</strong></div>
                        <div className="d-flex align-items-center ">
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'current_month')}/>
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>
                            <div className="d-flex align-items-center">
                                <label className="cstm_radio">
                                    <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'selected_month')}/>
                                    <div className="rad_lab">Selected Month</div>
                                </label>
                                <div className="fil_datie">
                                     <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false} 
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month'? false: true}
                                    />
                                </div>
                            </div>
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" checked={this.state.view_type == 'year' || ''} onChange={()=>this.onChangeFIlter('view_type', 'year')}/>
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className="cat_counts row mt-4">
                        <TotalCounts title="Total Participants" count={this.props.total_active_participant_count} for="participants" />
                        <TotalCounts title="Total Members" count={this.props.total_active_member_count} for="members" />
                        <TotalCounts title="Total Shifts Hours Filled" count={this.props.total_filled_shift_hours} for="shifts" />
                        <TotalCounts title="Total Orgs" count={this.props.total_fms_count} for="orgs" />
                        <TotalCounts title="Total FMS Cases Made" count={this.props.total_active_org} for="fms" />
                    </div>
                   

                    <div className="row mt-4 prog_inact_row">

                        <DashboardSpecificCount {...this.props.onboarded_participant_count_sp} count_title="new participants onboarded" />
                        
                        <DashboardSpecificCount {...this.props.inactive_participant_count_sp} count_title="inactive participants" />
                        
                        <DashboardSpecificCount {...this.props.onboard_member_count_sp} count_title="new members onboarded" />
                        
                        <DashboardSpecificCount {...this.props.onboard_org_count_sp} count_title="new organisations onboarded" />
                        
                        <DashboardSpecificCount {...this.props.filled_shift_hours_sp} count_title="hrs shifts filled" />
                        
                        <DashboardSpecificCount {...this.props.fms_count_sp} count_title="fms cases recorded" />

                    </div>

                    {/* row ends */}


                    <div className="row mt-2 mb-4">
                        <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="big_graphBx">
                                <h5 className="text-center cmn_clr1 mb-2 mt-2">
                                    <strong>Participant Intake vs Member Intake by Year</strong>
                                </h5>
                                    <FunnelGraph data={funnelData} />
                                  
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12">
                            <div className="big_graphBx">
                                <h5 className="text-center cmn_clr1 mb-2 mt-2">
                                    <strong>Type of Orgs</strong>
                                </h5>
                                <div className="d-flex mt-4 align-items-end dou_flex">
                                    <div className="data_lf">
                                        <div className="flex-1 mb-4 doug_data">
                                            {ca.all || ca.parnet_org ?<div className="" style={{ color: CT.GREENCOLOR1 }}>Patent Org = {this.props.org_comparison.parent_org_count}</div>: ''}
                                            {ca.all || ca.sub_org ?<div className="" style={{ color: CT.GREENCOLOR2 }}>Sub Org = {this.props.org_comparison.sub_org_count}</div>: ''}
                                            {ca.all || ca.site_comp ?<div className="" style={{ color: CT.GREENCOLOR3 }}>Site = {this.props.org_comparison.site_count}</div>: ''}
                                        </div>
                                        <div className="data_bx">

                                            <ul className="data_checks">
                                                <li>
                                                    <label className='cstmChekie clrTpe2'>
                                                        <input type="checkbox" className="" name="org_comp" onChange={this.orgCompareGraphFilter} value="all" checked={this.state.org_comparison_active.all || ''} />
                                                        <div className="chkie">All Type of Orgs</div>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label className='cstmChekie clrTpe2'>
                                                        <input type="checkbox" className="" name="org_comp" onChange={this.orgCompareGraphFilter} value="parnet_org" checked={this.state.org_comparison_active.parnet_org || ''} />
                                                        <div className="chkie">Parent Orgs</div>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label className='cstmChekie clrTpe2'>
                                                        <input type="checkbox" className="" name="org_comp" onChange={this.orgCompareGraphFilter} value="sub_org" checked={this.state.org_comparison_active.sub_org || ''} />
                                                        <div className="chkie">Sub Orgs</div>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label className='cstmChekie clrTpe2'>
                                                        <input type="checkbox" className="" name="org_comp" onChange={this.orgCompareGraphFilter} value="site_comp" checked={this.state.org_comparison_active.site_comp || ''} />
                                                        <div className="chkie">Sites</div>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div className="pl-3 data_rg">
                                        <div className="doughnut_gr">
                                            <GraphDoughnut data={orgsData} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}


                    <div className="main_heading_cmn- mt-2 bor_top1 ">
                        <h2><strong>Our Reach</strong></h2>
                    </div>

                    <div className="row reach_row mb-5 mt-3">
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div className="react_box">
                                <h5 className="text-center cmn_clr1 mb-2"><strong>Graph Title</strong></h5>
                                <div className="d-flex reachgraph_area__ mt-3 align-items-center">
                                    <div>
                                        <div className='grph_dv'>
                                            <BarGraph data={graphdata} />
                                        </div>
                                    </div>
                                    <div className="flex-1 data_dv">
                                        <div className="" style={{ color: CT.COLOR1 }}>Data 1</div>
                                        <div className="" style={{ color: CT.COLOR2 }}>Data 2</div>
                                        <div className="" style={{ color: CT.COLOR3 }}>Data 3</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}

                </AuthWrapper>
            </React.Fragment>

        );
    }
}


const mapStateToProps = state => ({
     ...state.reducer_dashboard,
})

const mapDispatchtoProps = (dispach) => {
    return {
        getDashboardGraphDetails: (req) => dispach(getDashboardGraphDetails(req)),
    }
};

export default connect(mapStateToProps, mapDispatchtoProps)(Dashboard);

