import React, { Component } from 'react';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import { ChronologicalYearMonth, nth } from './utils/ChronologicalYearMonth';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import GMap from './utils/GMap';
import * as CT from '../hoc/Color';
import GraphDoughnut from './utils/GraphDoughnut';
import StackedBarGraph from './utils/StackedBarGraph';
import MainHeading from './utils/MainHeading';
import { connect } from 'react-redux';
import { getFmsDetails, getFmsByPostCode } from '../store/actions';
import FmsSpecificCount from './fms/FmsSpecificCount';
import HotColdSpotPlaceCountCommon from './utils/HotColdSpotPlaceCountCommon';


class Fms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            view_type: 'year',
            MarkersColor: { f_color: "#FF8967", s_color: "#FF7045" }
        };
    }

    componentDidMount() {
        var req = { view_type: this.state.view_type };
        this.props.getFmsDetails(req);
        var postcode = { compare_against: this.state.compare_against, compare_against_2: this.state.compare_against_2 }
        this.props.getFmsByPostCode(postcode);
    }

    onChangeFIlter = (key, value) => {
        var state = {}
        state[key] = value;

        this.setState(state, () => {
            if (value == 'current_month' || value == 'year') {
                this.setState({ specific_month: '' });
            }

            if (value != 'selected_month') {
                var req = { view_type: this.state.view_type, specific_month: this.state.specific_month, selected_member_type: this.state.selected_member_type, member_compare_with: this.state.member_compare_with, member_compare_with_specific: this.state.member_compare_with_specific };
                this.props.getFmsDetails(req);
            }
        });
    }

    onChangeLocationFilter = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var postcode = { compare_against: this.state.compare_against, compare_against_2: this.state.compare_against_2 }
            this.props.getFmsByPostCode(postcode);
        });
    }

    render() {
        var fms_by_post_code = [{ type: 'fms', posArr: this.props.fms_by_post_code }];
        return (
            <React.Fragment>
                <Header />
                <AuthWrapper>
                    <MainHeading name={"FMS"} />
                    <div className="bor_bot1 pb-4 mt-4 filter_area d-flex align-items-center">
                        <div className="vw_side"><strong>View:</strong></div>
                        <div className="d-flex align-items-center ">
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" checked={this.state.view_type == 'year' || ''} onChange={() => this.onChangeFIlter('view_type', 'year')} />
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'current_month')} />
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>
                            <div className="d-flex align-items-center">
                                <label className="cstm_radio">
                                    <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'selected_month')} />
                                    <div className="rad_lab">Selected Month</div>
                                </label>
                                <div className="fil_datie">
                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month' ? false : true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-4 prog_inact_row">

                        <FmsSpecificCount {...this.props.member_reported} count_title="FMS cases reported by Members in" />

                        <FmsSpecificCount {...this.props.participant_reported} count_title="FMS cases reported by Participants in" />

                        <FmsSpecificCount {...this.props.org_reported} count_title="FMS cases reported by Organistions in" />

                        <FmsSpecificCount {...this.props.avg_fms} count_title="days on average it took to resolve on FMS case in" />

                    </div>
                    {/* row ends */}
                    <div className="main_heading_cmn- mt-2 bor_top1">
                        <h3><strong>FMS by Postcode</strong></h3>
                    </div>

                    <div className="row mt-4 mb-4 justify-content-end">
                        <div className="col-xl-4 col-lg-5 d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2"><strong>FMS Cases (All)</strong></div>
                            <div className='cstm_select slct_cstm2 bg_grey flex-1'></div>
                        </div>
                        <div className="col-xl-7 col-lg-7  d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2 flex-1 text-center"><strong>Compared Against</strong></div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1 pl-2'>
                                <Select name="compare_against"
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Module"
                                    options={this.props.compare_with_dropdown}
                                    onChange={(e) => this.onChangeLocationFilter('compare_against', e)}
                                    value={this.state.compare_against}
                                />
                            </div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1  pl-2' >
                                <Select name="compare_against_2"
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Sub-Category"
                                    options={this.props.special_dropdown[this.state.compare_against]}
                                    onChange={(e) => this.onChangeLocationFilter('compare_against_2', e)}
                                    value={this.state.compare_against_2}

                                />
                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                    <div className="map_area mt-3 mb-2">
                        <GMap MarkersModule={{ f_module: 'fms', s_module: this.state.compare_against }} position={fms_by_post_code} />
                    </div>

                    <div className="main_heading_cmn- mt-4 bor_top1">
                        <h3><strong>FMS Statistics</strong></h3>
                    </div>

                    <div className="row mt-3 mb-3 stats_rw15">
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.participant_hot_spots} title={this.props.server_view_type.date+" FMS Cases by Participant Hot Spots"} color={CT.PARTICIPANTCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.member_hot_spots} title={this.props.server_view_type.date+" FMS Cases by Member Hot Spots"} color={CT.MEMBERCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.org_hot_spots} title={this.props.server_view_type.date+" FMS Cases by Org Hot Spots"} color={CT.ORGANISATION1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.other_hot_spots} title={this.props.server_view_type.date+" FMS Cases by Other Hot Spots"} color={CT.ORGRES} />

                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.participant_cold_spots} title={this.props.server_view_type.date+" FMS Cases by Participant Cold Spots"} color={CT.PARTICIPANTCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.member_cold_spots} title={this.props.server_view_type.date+" FMS Cases by Member Cold Spots"} color={CT.MEMBERCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.org_cold_spots} title={this.props.server_view_type.date+" FMS Cases by Org Cold Spots"} color={CT.ORGANISATION1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.fms_stats.other_cold_spots} title={this.props.server_view_type.date+" FMS Cases by Other Cold Spots"} color={CT.ORGRES} />
                    </div>
                   

                {/* row ends */}
                </AuthWrapper>
            </React.Fragment >
        );
    }
}

//
const mapStateToProps = state => ({
    server_view_type: state.reducer_fms.server_view_type,
    member_reported: state.reducer_fms.hour_count.member_reported,
    org_reported: state.reducer_fms.hour_count.org_reported,
    participant_reported: state.reducer_fms.hour_count.participant_reported,
    avg_fms: state.reducer_fms.hour_count.avg_fms,
    fms_stats: state.reducer_fms.fms_stats,
    compare_with_dropdown: state.reducer_fms.compare_with_dropdown,
    special_dropdown: state.reducer_fms.special_dropdown,
    fms_by_post_code: state.reducer_fms.fms_by_post_code
})
const mapDispatchtoProps = (dispatch) => {
    return {
        getFmsDetails: (request) => dispatch(getFmsDetails(request)),
        getFmsByPostCode: (request) => dispatch(getFmsByPostCode(request)),
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Fms);
