import React, { Component } from 'react';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import GMap from './utils/GMap';
import { Chart } from "react-google-charts";
import * as CT from '../hoc/Color';
import GraphDoughnut from './utils/GraphDoughnut';
import StackedBarGraph from './utils/StackedBarGraph';
import MainHeading from './utils/MainHeading';
import { orgDetails } from '../store/actions';
import { ChronologicalYearMonth, nth } from './utils/ChronologicalYearMonth';
//import { postData } from 'service/common.js';

class Organisations extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view_type: 'year',
            //MarkersColor: { f_color: "#FF8967", s_color: "#FF7045" }
            startDate: new Date(),
            positions: [
                {
                    type: 'organisation',
                    posArr: [
                        { lat: 22.7171903, lng: 75.87717980000002, numbers: '5' },
                        { lat: 15.7171903, lng: 20.87717980000002, numbers: '2' },
                        { lat: 55.7171903, lng: 59.87717980000002, numbers: '3' }
                    ]
                },
                {
                    type: 'sub-organisation',
                    posArr: [
                        { lat: 42.03, lng: 42.02, numbers: '60' },
                        { lat: 72.7171903, lng: 75.87717980000002, numbers: '155' },
                        { lat: 78.7171903, lng: 78.87717980000002, numbers: '200' }
                    ]
                }
            ],
        };
    }

    getAllOrg=()=>{
        /*postData('recruitment/Recruitment_question/get_recruitment_topic_list', {}).then((result) => {
            if (result.status) {
                this.setState({ topicList: result.data });
            }
        });  */  
    }

    componentWillMount(){
        var req = { view_type: this.state.view_type };
        //this.props.orgDetails(req);
        //var postcode = { compare_against: this.state.compare_against, compare_against_2: this.state.compare_against_2 }
        //this.props.getFmsByPostCode(postcode);
    }

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    onChangeFIlter = (key, value) => {
        var state = {}
        state[key] = value;

        this.setState(state, () => {
            if (value == 'current_month' || value == 'year') {
                this.setState({ specific_month: '' });
            }

            if (value != 'selected_month') {
                var req = { view_type: this.state.view_type, specific_month: this.state.specific_month, selected_member_type: this.state.selected_member_type, member_compare_with: this.state.member_compare_with, member_compare_with_specific: this.state.member_compare_with_specific };
                //this.props.orgDetails(req);
            }
        });
    }

    render() {
        const orgsData = {
            labels: ['NDIS', 'Welfare', 'Private'],
            datasets: [{
                data: ['130', '80', '50'],
                backgroundColor: [CT.ORGANISATION1, CT.ORGANISATION2, CT.ORGANISATION3]
            }],

        };


        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];

        const shiftRequstData = [
            [
                'Month',
                'NDIS Funded',
                'Welfare Funded',
                'Shifts Filled',
            ],
            ['July', 150, 100, 614.6],
            ['August', 120, 100, 682],
            ['September', 200, 100, 623],
            ['October', 250, 100, 609.4],
            ['November', 220, 120, 569.6],
            ['December', 250, 150, 614.6],
            ['January', 220, 120, 682],
            ['February', 200, 100, 623],
            ['March', 150, 100, 609.4],
            ['April', 120, 100, 569.6],
            ['May', 100, 80, 614.6],
            ['June', 80, 50, 682]
        ];

        const availData = [
            [
                'Month',
                'Houses Filled',
                'Shifts Filled',
            ],
            ['July', 150, 614.6],
            ['August', 120, 682],
            ['September', 200, 623],
            ['October', 250, 609.4],
            ['November', 220, 569.6],
            ['December', 250, 614.6],
            ['January', 220, 682],
            ['February', 200, 623],
            ['March', 150, 609.4],
            ['April', 120, 569.6],
            ['May', 100, 614.6],
            ['June', 80, 682]
        ]


        return (
            <React.Fragment>
                <Header />
                <AuthWrapper>

                    <MainHeading name={"Organisations"} />
                    <div className="bor_bot1 pb-4 mt-4 filter_area align-items-center ">
                    {/* d-flex align-items-center flex-3-orgs col-lg-12 col-xl-12 col-md-12 */}
                        <div className="orgGridie">

                            <div className="fnt_16"><strong>View:</strong></div>

                            <div className=" ti_col">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" defaultChecked />
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>

                            <div className=" fi_col">
                                <label className="cstm_radio">
                                     <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'current_month')} />
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>

                            <div className="d-flex align-items-center  si_col">
                                <label className="cstm_radio">
                                   <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'selected_month')} />
                                    <div className="rad_lab">Selected Month</div>
                                </label>

                                <div className='cstm_select slct_cstm2 bg_grey flex-1 min_wid_150'>
                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month' ? false : true}
                                    />
                                </div>
                            </div>

                            <div className=" d-flex align-items-center foi_col">
                                
                                <div className='cstm_select slct_cstm2 bg_grey flex-1 min_wid_150' >
                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false} Clearable={false}
                                        placeholder="All Orgs"
                                        options={options}
                                        onChange={(e) => this.setState({ orgs: e })}
                                        value={this.state.orgs}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-4 prog_inact_row">

                        <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="inact_box__ fnt_big">

                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>147</strong></div>
                                    <div className="pl-2 desc_int" >Active Organisations currently this financial year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>144</strong></div>
                                    <div className="pl-2 desc_int" >Active Organisations at this time last year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int icon_ar green"><i className="icon hcm-mg-ie-profit arw_ic_pr"></i></div>
                                    <div className="pl-2 desc_int " >thats a 2% increase in Organisations this financial year</div>
                                </div>

                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="inact_box__ fnt_big">
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>8.7</strong></div>
                                    <div className="pl-2 desc_int" >hours of shifts filled via Organisation Portal this financial Year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>7.5</strong></div>
                                    <div className="pl-2 desc_int" >hours of shifts filled via Organisation Portal last financial Year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int icon_ar green"><i className="icon hcm-mg-ie-profit arw_ic_pr"></i></div>
                                    <div className="pl-2 desc_int " >thats a 16% increase of shifts filled via Portal this financial Year</div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="inact_box__ fnt_big">
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>3</strong></div>
                                    <div className="pl-2 desc_int" >Inactive Organisations currently this financial year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>10</strong></div>
                                    <div className="pl-2 desc_int" >Inactive Organisations at this time last year</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int icon_ar green"><i className="icon hcm-mg-ie-lost arw_ic_pr"></i></div>
                                    <div className="pl-2 desc_int " >thats a 330% decrease of inactive organisations this financial year</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* row ends */}

                    <div className="main_heading_cmn- mt-2 bor_top1">
                        <h3><strong>Organisations by Postcode</strong></h3>
                    </div>

                    <div className="row mt-4 mb-4 justify-content-end">
                        <div className="col-xl-4 col-lg-12 d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2"><strong>Organisation</strong></div>
                            <div className='cstm_select slct_cstm2 bg_grey flex-1'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Select status"
                                    options={options}
                                    onChange={(e) => this.setState({ Organisation: e })}
                                    value={this.state.Organisation}

                                />
                            </div>
                        </div>
                        <div className="col-xl-7 col-lg-7  d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2 flex-1 text-right"><strong>Compared Against</strong></div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1 pl-2'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Module"
                                    options={options}
                                    onChange={(e) => this.setState({ Module: e })}
                                    value={this.state.Module}

                                />
                            </div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1  pl-2' >
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Sub-Category"
                                    options={options}
                                    onChange={(e) => this.setState({ Category: e })}
                                    value={this.state.Category}

                                />
                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                    <div className="map_area mt-3 mb-2">
                        <GMap position={this.state.positions} />
                    </div>

                    <div className="main_heading_cmn- mt-4 bor_top1">
                        <h3><strong>Organisations Statistics</strong></h3>
                    </div>


                    <div className="row mt-3 mb-3">

                        <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                            <div className="statis_bx__">
                                <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                                    <div className="mg_ics cmn_clr1"><i className="icon hcm-mg-thumbs-up"></i></div>
                                    <div><strong>Top Performing orgs</strong></div>
                                </h6>
                                <div className="marker_listBox">
                                    <div><span className="pr-2">1.</span> Organisation 1($000)</div>
                                    <div><span className="pr-2">2.</span> Organisation 2($000)</div>
                                    <div><span className="pr-2">3.</span> Organisation 3($000)</div>
                                    <div><span className="pr-2">4.</span> Organisation 4($000)</div>
                                    <div><span className="pr-2">5.</span> Organisation 5($000)</div>
                                   
                                </div>
                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                            <div className="statis_bx__">
                                <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                                    <div className="mg_ics cmn_clr1" ><i className="icon hcm-mg-thumbs-down"></i></div>
                                    <div><strong>Lowest Performing orgs</strong></div>
                                </h6>
                                <div className="marker_listBox">
                                    <div><span className="pr-2">1.</span> Organisation 1($000)</div>
                                    <div><span className="pr-2">2.</span> Organisation 2($000)</div>
                                    <div><span className="pr-2">3.</span> Organisation 3($000)</div>
                                    <div><span className="pr-2">4.</span> Organisation 4($000)</div>
                                    <div><span className="pr-2">5.</span> Organisation 5($000)</div>
                                   

                                </div>
                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                            <div className="statis_bx__">
                                <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                                    <div className="mg_ics cmn_clr1"><i className="icon hcm-mg-thumbs-up"></i></div>
                                    <div><strong>Top Performing Sub-Orgs</strong></div>
                                </h6>
                                <div className="marker_listBox">
                                    <div><span className="pr-2">1.</span> Organisation 1($000)</div>
                                    <div><span className="pr-2">2.</span> Organisation 2($000)</div>
                                    <div><span className="pr-2">3.</span> Organisation 3($000)</div>
                                    <div><span className="pr-2">4.</span> Organisation 4($000)</div>
                                    <div><span className="pr-2">5.</span> Organisation 5($000)</div>
                                   
                                </div>
                            </div>
                        </div>

                        <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                            <div className="statis_bx__">
                                <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                                    <div className="mg_ics cmn_clr1"><i className="icon hcm-mg-thumbs-up"></i></div>
                                    <div><strong>Lowest Performing Sub-Orgs</strong></div>
                                </h6>
                                <div className="marker_listBox">
                                    <div><span className="pr-2">1.</span> Organisation 1($000)</div>
                                    <div><span className="pr-2">2.</span> Organisation 2($000)</div>
                                    <div><span className="pr-2">3.</span> Organisation 3($000)</div>
                                    <div><span className="pr-2">4.</span> Organisation 4($000)</div>
                                    <div><span className="pr-2">5.</span> Organisation 5($000)</div>
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                    <div className="row mt-3 mb-3">
                        <div className="col-12">

                            <div className="statis_bx__">
                                <h4 className="cmn_clr1 mg_ics_head  mt-2 text-center">
                                    <strong>Shifts by Organisations or Sub Organisations</strong>
                                </h4>

                                <div>
                                    <StackedBarGraph
                                        data={shiftRequstData}
                                        xAxisTitle='Qty'
                                        yAxisTitle='Month'
                                        colors={[CT.ORGANISATION1, CT.ORGANISATION3, CT.SHIFTCOLOR1]}
                                        series={{ 2: { type: 'line' } }}
                                        height={'500px'}
                                    />
                                </div>
                                <div className="mt-3">
                                    <ul className="d-flex tickie_ul pl-5">
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.ORGANISATION1 }}></span>
                                            <div>Organisation</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.ORGANISATION3 }}></span>
                                            <div>Sub Organisation</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                            <div>Shifts Offered</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}

                    <div className="row mt-3 mb-3">
                        <div className="col-12">

                            <div className="statis_bx__">
                                <h4 className="cmn_clr1 mg_ics_head  mt-2 text-center">
                                    <strong>Availability of Houses per Month</strong>
                                </h4>

                                <div>
                                    <StackedBarGraph
                                        data={availData}
                                        xAxisTitle='Qty'
                                        yAxisTitle='Month'
                                        colors={[CT.ORGANISATION1, CT.SHIFTCOLOR1]}
                                        series={{ 1: { type: 'line' } }}
                                        height={'500px'}
                                    />
                                </div>
                                <div className="mt-3">
                                    <ul className="d-flex tickie_ul pl-5">
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.ORGANISATION1 }}></span>
                                            <div>Houses Filled</div>
                                        </li>

                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                            <div>Shifts Filled</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}


                    <div className="row ">

                        <div className="col-lg-6 col-md-12 col-12">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-4 mt-3 text-center">
                                    <strong>Funding Types used in Organisations</strong>
                                </h5>
                                <div className="d-flex align-items-end typesParti__">
                                    <div className="partic_dough flex-1 pl-5">
                                        <GraphDoughnut data={orgsData} />
                                    </div>
                                    <div className="legend_tickie">
                                        <ul className=" tickie_ul pl-5">


                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.ORGANISATION1 }}></span>
                                                <div>NDIS</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.ORGANISATION2 }}></span>
                                                <div>Welfare</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.ORGANISATION3 }}></span>
                                                <div>Private</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </AuthWrapper>


            </React.Fragment>

        );
    }
}



const mapStateToProps = (state) => {
  console.log(state)
    return {
        org_statstics_data: state.reducer_participant.org_statstics
    }
};
export default connect(mapStateToProps,{orgDetails})(Organisations);
