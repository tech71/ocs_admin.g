import React, { Component } from 'react';

import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import MainHeading from './utils/MainHeading';

class Configuration extends Component {


    constructor(props) {
        super(props);
        this.state = {
        };

    }

    render() {


        return (

            <React.Fragment>


                <Header />
                <AuthWrapper>



                    <MainHeading name={"Configuration"} />

                    <div className="row mt-4 mb-3">

                        <div className="col-12">
                            <div className="configBox">

                                <div className="row">
                                    <div className="col-12">
                                        <div className="d-flex justify-content-center align-items-center mb-2 api_dv">
                                            <div className="pr-2">Google API Token</div>
                                            <div>
                                                <div className="config_inp success">
                                                    <input type="text" className="csForm_control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center mb-2 api_dv">
                                            <div className="pr-2">Facebook API Token</div>
                                            <div>
                                                <div className="config_inp">
                                                    <input type="text" className="csForm_control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                </AuthWrapper>


            </React.Fragment>

        );
    }
}





export default Configuration;
