import React, { Component } from 'react';
import { connect } from 'react-redux';

class HotColdSpotPlaceCount extends Component {
    render() {        
        return (
            <React.Fragment>
                        <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                            <div className="statis_bx__">
                                <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                                    <div className="mg_ics" style={{ color: this.props.color }}><i className="icon hcm-mg-location"></i></div>
                                    <div><strong>{this.props.hot_cold_place_title} {this.props.title}</strong></div>
                                </h6>
                                
                                {this.props.place.length > 0?
                                <div className="marker_listBox">
                                    {this.props.place.map((val, index) => (
                                        <div><span className="pr-2">{index+1}.</span>{val.city}({val.count})</div>
                                    ))}
                                </div>: 
                                <div className="marker_listBox">
                                    No {this.props.title}
                                </div>}
                                        
                            </div>
                        </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    hot_cold_place_title: state.reducer_participant.hot_cold_place_title,
})
const mapDispatchtoProps = (dispatch) => {
    return {
       
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(HotColdSpotPlaceCount);