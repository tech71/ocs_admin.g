import React, { Component } from 'react';


import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';

import * as CT from '../hoc/Color';

import MainHeading from './utils/MainHeading';
import FunnelGraph from './utils/FunnelGraph';
import CanvasCheck from './utils/CanvasCheck';
class UI extends Component {


    constructor(props) {
        super(props);
        this.state = {
           
            funnelData: {
                data1: [2000, 2000, 2500, 2500, 2500, 1000, 1000],
                data1Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11, CT.MEMBERCOLOR12, CT.MEMBERCOLOR13, CT.MEMBERCOLOR14, CT.MEMBERCOLOR15],
                data2: [2000, 2000, 2500, 2500, 2500, 1000, 1000],
                data2Bg: [CT.COLOR6, CT.COLOR7, CT.COLOR8, CT.COLOR3, CT.COLOR4, CT.COLOR9, CT.COLOR10]
            },
            funnelData2: {
                data1: [55000, 50000, 50000],
                data1Bg: [CT.PARTICIPANTCOLOR8, CT.PARTICIPANTCOLOR9, CT.PARTICIPANTCOLOR10],
                data2: [50000, 50000, 50000],
                data2Bg: [CT.COLOR3, CT.COLOR4, CT.COLOR5]
            },
            funnelData3: {
                data1: [1000, 2000, 3000, 4000, 5000, 6000, 7000],
                data1Bg: [CT.PARTICIPANTCOLOR1, CT.PARTICIPANTCOLOR2, CT.PARTICIPANTCOLOR3, CT.PARTICIPANTCOLOR4, CT.PARTICIPANTCOLOR5, CT.PARTICIPANTCOLOR6, CT.PARTICIPANTCOLOR7],
                data2: [2100, 2200, 2300, 2400, 2500, 2600, 3000],
                data2Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11, CT.MEMBERCOLOR12, CT.MEMBERCOLOR13, CT.MEMBERCOLOR14, CT.MEMBERCOLOR15]
            },
            funnelData4: {
                data1: [15000, 30000, 45000],
                data1Bg: [CT.SHIFTCOLOR1, CT.SHIFTCOLOR2, CT.SHIFTCOLOR3],
                data2: [10000, 40000, 80000],
                data2Bg: [CT.ORGANISATION1, CT.ORGANISATION2, CT.ORGANISATION3]
            }
        };

    }

    changeParams = () => {
        var funnelData = {
            data1: [1000, 2000, 3000, 4000, 5000, 6000, 7000],
                data1Bg: [CT.PARTICIPANTCOLOR1, CT.PARTICIPANTCOLOR2, CT.PARTICIPANTCOLOR3, CT.PARTICIPANTCOLOR4, CT.PARTICIPANTCOLOR5, CT.PARTICIPANTCOLOR6, CT.PARTICIPANTCOLOR7],
                data2: [2100, 2200, 2300, 2400, 2500, 2600, 3000],
                data2Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11, CT.MEMBERCOLOR12, CT.MEMBERCOLOR13, CT.MEMBERCOLOR14, CT.MEMBERCOLOR15]
        }

        this.setState({
            funnelData: funnelData
        })


    }


    render() {

      

    
        return (

            <React.Fragment>


                <Header />
                <AuthWrapper>

                    <MainHeading name={"UI"} />


                   

                    <div className="row mt-5">

                        <div className="col-lg-6 col-md-12 col-12 mb-3">
                            <div className="statis_bx__">
                            <button className="btn btn-primary" onClick={this.changeParams}>change params</button>
                                        <FunnelGraph
                                            data={this.state.funnelData}
                                            type='funnel_1'
                                        />
                                   
                            </div>
                        </div>
                       
                        <div className="col-lg-6 col-md-12 col-12 mb-3">
                            <div className="statis_bx__">
                                   
                                        <FunnelGraph
                                            data={this.state.funnelData2}
                                            type='funnel_2'
                                        />         
                               
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-12 col-12 mb-3">
                            <div className="statis_bx__">

                                        <FunnelGraph
                                            data={this.state.funnelData3}
                                            type='funnel_1'
                                        />
                                   
                            </div>
                        </div>
                       
                        <div className="col-lg-6 col-md-12 col-12 mb-3">
                            <div className="statis_bx__">
                                   
                                        <FunnelGraph
                                            data={this.state.funnelData4}
                                            type='funnel_2'
                                        />         
                               
                            </div>
                        </div>

                        <div className="col-lg-8 col-md-12 col-12 mb-3">
                            <div className="statis_bx__">
                            <h5 className="cmn_clr1 mg_ics_head mb-5 mt-3 text-center">
                                    <strong>Dummy if no data pass</strong>
                                </h5>
                                        <FunnelGraph />         
                               
                            </div>
                        </div>
                       
                    
                    
                    </div>

            <div className="row">
                <div className="col-6">
                    <CanvasCheck 
                        data={[0, 30, 150, 150]}
                        color={'#abf763'}
                        hoverColor={'#88d143'}
                        type={"1"}
                        val={'50'}
                    />
                </div>

                <div className="col-6">
                    <CanvasCheck 
                        data={[0, 30, 100, 100]}
                        color={'#354d5c'}
                        hoverColor={'#43799c'}
                        type={"2"}
                        val={'70'}
                    />
                </div>
            </div>



                

                </AuthWrapper>


            </React.Fragment>

        );
    }
}





export default UI;
