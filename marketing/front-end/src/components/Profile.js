import React, { Component } from 'react';
import { ROUTER_PATH, BASE_URL } from '../config.js';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import { connect } from 'react-redux';
import MainHeading from './utils/MainHeading';
import '../service/jquery.validate.js';
import jQuery from "jquery";
import { checkItsLoggedIn, postData, getRemeber, setRemeber, setLoginToken, setPermission, setLoginTIme, setFullName} from '../service/common.js';
import { get_profile,updateProfile } from '../store/actions';
import { ToastContainer,toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Modal from './utils/Modal';
import { ToastUndo } from '../service/ToastUndo.js'

class Configuration extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            submit_form:false,
            userdata: { },
            activeFields: {
                fullname: false,
                username: false,
                password: false,
                mobile: false,
                email: false               
           },
            btnActive:false,
            verifyModal:false,
            codeVerified:false
          
        };
     // checkItsLoggedIn(ROUTER_PATH);
    }

    componentDidMount(){
     
      //if(this.props.loginState){
        this.props.get_profile().then((res) => {
          if(res.status){
            this.setState({userdata: res.data});
          }
        });
      //}
    }

    handleChange = (e) => {
        var data = this.state.userdata;
        data[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState({ data: data, btnActive:true,'submit_form':true });
        // console.log(this.state.data)
    }

    closeCatModal=()=>{
        this.setState({ verifyModal:false }); 
    }
    ActiveEdit = (key) => {
        var activeFields = this.state.activeFields;
        activeFields[key] = !activeFields[key].value;
        this.setState({ activeFields: activeFields });
    }

    submitHandler = (e) => {
        e.preventDefault();
        /*if(this.state.userdata.two_way && !this.state.codeVerified)
        {
            this.sendVerificationCode();
            this.setState({ verifyModal:true }); 
        }
        else{
          this.profileUpdate();            
        }*/
        this.profileUpdate();            
    }

    sendVerificationCode=()=>{
        postData('marketing/profile/sendVerifyCode').then((res) => {
            if(res.status){                 
                toast.success(res.success, {                  
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.props.get_profile();
               
            }
            else{
                toast.error(res.error, {                   
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });               
               
            }
        });
    }

    submitVerificationCode=(e)=>{
        e.preventDefault();
        let data={code:this.state.data.verification_code}
        postData('marketing/profile/verifyCodeSent',data).then((res) => {
            if(res.status){                 
                toast.success(res.success, {                   
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
                this.props.get_profile();
                this.setState({ verifyModal:false,codeVerified:true });                 
            }
            else{
                toast.error(res.error, {               
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });               
               
            }
        });
    }

    profileUpdate=()=>{
        
        if(this.state.submit_form)
        {
           jQuery('#profForm').validate();
            if(jQuery('#profForm').valid()){
                postData('marketing/profile/profile_update', this.state).then((result) => {
                    if (result.status) { 
                     this.setState({submit_form: false},()=>{

                        //toast.success(this.props.profileUpdated, {
                        toast.success(result.msg, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                        this.props.get_profile();
                        this.setState({        
                            activeFields: {
                            fullname: false,
                            username: false,
                            password: false,
                            mobile: false,
                            email: false,
                        }})
                     })                
                    } else {
                        toast.error(<ToastUndo message={result.msg} showType={'e'} />, {
                            position: toast.POSITION.TOP_CENTER,
                            hideProgressBar: true
                        });
                    }
                });
            }
        }
    }

    render() {
        var btnActive = this.state.btnActive;

        return (
            <React.Fragment>

          <ToastContainer />
                <Header />
                <AuthWrapper>
                    <MainHeading name={"Profile"} />
                    <div className="row mt-4 mb-3">

                        <div className="col-12">
                            <div className="profileBox">

                                <div className='text-center prof_ic159 mt-2 mb-5'>
                                    <span><i className='icon hcm-mg-c-boy'></i></span>
                                </div>


                                <form className="profForm" id="profForm" onSubmit={this.submitHandler}>
                                <div className="prof_fieldBox">

                                    <div className="prof_formGroup mb-4">
                                        <div className="left_p"><strong>Full Name</strong></div>
                                        <div className="right_p">
                                            <div className="field_desc" style={{ display: this.state.activeFields.fullname ? 'none' : 'block' }}>
                                                {this.state.userdata.fullname}
                                                <i className="icon hcm-mg-edit-modify-streamline edit_ic" onClick={() => this.ActiveEdit('fullname')}></i>
                                            </div>
                                            <div className="field_inp_box" style={{ display: this.state.activeFields.fullname ? 'block' : 'none' }}>
                                                <input
                                                    type="text"
                                                    className="field_inp csForm_control"
                                                    name="fullname"
                                                    value={this.state.userdata.fullname || ''}
                                                    onChange={this.handleChange}
                                                    data-rule-required={"true"}
                                                    data-msg-required={"Please enter Fullname"}
                                                />
                                                 
                                            </div>
                                        </div>
                                    </div>

                                   
                                    <div className="prof_formGroup mb-4">
                                        <div className="left_p"><strong>User Name</strong></div>
                                        <div className="right_p">
                                            <div className="field_desc" style={{ display: this.state.activeFields.username ? 'none' : 'block' }}>
                                                {this.state.userdata.username}
                                             {/*<i className="icon hcm-mg-edit-modify-streamline edit_ic" onClick={() => this.ActiveEdit('username')}></i>*/}
                                            </div>
                                            <div className="field_inp_box" style={{ display: this.state.activeFields.username ? 'block' : 'none' }}>
                                                <input
                                                    type="text"
                                                    className="field_inp csForm_control"
                                                    name="username"
                                                    value={this.state.userdata.username || ''}
                                                    onChange={this.handleChange}
                                                    data-rule-required={"true"}
                                                    data-msg-required={"Please enter Username"}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="prof_formGroup mb-4">
                                        <div className="left_p"><strong style={{ display: this.state.activeFields.password ? 'none' : 'block' }}>Password</strong></div>
                                        <div className="right_p">
                                            <div className="field_desc" style={{ display: this.state.activeFields.password ? 'none' : 'block' }}>
                                                *******
                                                <i className="icon hcm-mg-edit-modify-streamline edit_ic" onClick={() => this.ActiveEdit('password')}></i>
                                            </div>
                                            <div className="field_inp_box" style={{ display: this.state.activeFields.password ? 'block' : 'none' }}>

                                                <div className="paswrdArea">

                                                    <div className="ps_group">
                                                        <div className="p_l_side"><strong>Current Password</strong></div>
                                                        <div className="p_r_side"><input
                                                            type="password"
                                                            className="field_inp csForm_control"
                                                            name="c_password"
                                                            onChange={this.handleChange}
                                                            autoComplete="new-password"
                                                        /></div>
                                                    </div>

                                                    <div className="ps_group">
                                                        <div className="p_l_side"><strong>New Password</strong></div>
                                                        <div className="p_r_side"><input
                                                            type="password"
                                                            className="field_inp csForm_control"
                                                            name="new_password"
                                                            id="new_password"
                                                            autoComplete="new-password"
                                                            onChange={this.handleChange}
                                                        /></div>
                                                    </div>

                                                    <div className="ps_group">
                                                        <div className="p_l_side"><strong>Confirm New Password</strong></div>
                                                        <div className="p_r_side"><input
                                                            type="password"
                                                            className="field_inp csForm_control"
                                                            name="confirm_password"
                                                            autoComplete="new-password"
                                                            onChange={this.handleChange}
                                                            data-rule-equalto={'#new_password'}
                                                            data-msg-equalto={'Please enter same password'}
                                                        /></div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="prof_formGroup mb-4">
                                        <div className="left_p"><strong>Mobile No.</strong></div>
                                        <div className="right_p">
                                            <div className="field_desc" style={{ display: this.state.activeFields.mobile ? 'none' : 'block' }}>
                                                {this.state.userdata.mobile}
                                                <i className="icon hcm-mg-edit-modify-streamline edit_ic" onClick={() => this.ActiveEdit('mobile')}></i>
                                            </div>
                                            <div className="field_inp_box" style={{ display: this.state.activeFields.mobile ? 'block' : 'none' }}>
                                                <input
                                                    type="text"
                                                    className="field_inp csForm_control"
                                                    name="mobile"
                                                    value={this.state.userdata.mobile || ''}
                                                    onChange={this.handleChange}
                                                    data-rule-required={"true"}
                                                    data-msg-required={"Please enter No."}
                                                    data-rule-phonenumber={true}
                                                />
                                            </div>
                                        </div>
                                        <div className="pl-4 pr-3 pt-2">
                                            <label className='cstmChekie clrTpe2 '>
                                                <input type="checkbox" name="two_factor_login"
                                                onChange={this.handleChange}
                                                checked={this.state.userdata.two_factor_login && this.state.userdata.two_factor_login==1?'checked':''} />
                                                <div className="chkie">Use Two factor notification</div>
                                            </label>

                                        </div>
                                    </div>

                                    <div className="prof_formGroup mb-4">
                                        <div className="left_p"><strong>Primary Email</strong></div>
                                        <div className="right_p">
                                            <div className="field_desc" style={{ display: this.state.activeFields.email ? 'none' : 'block' }}>
                                                {this.state.userdata.email}
                                                <i className="icon hcm-mg-edit-modify-streamline edit_ic" onClick={() => this.ActiveEdit('email')}></i>
                                            </div>
                                            <div className="field_inp_box" style={{ display: this.state.activeFields.email ? 'block' : 'none' }}>
                                                <input
                                                    type="text"
                                                    className="field_inp csForm_control"
                                                    name="email"
                                                    value={this.state.userdata.email || ''}
                                                    onChange={this.handleChange}
                                                    data-rule-required={"true"}
                                                    data-msg-required={"Please enter Email"}
                                                    data-rule-email={true}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="prof_formGroup justify-content-end mt-5 mb-5"  >
                                        <button className="btn cmn-btn1">Update Changes</button>
                                    </div>

                                </div>

                                </form>
                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                    <Modal
                        show={this.state.verifyModal}
                        close={this.closeCatModal}
                        heading={'Please enter the verification code sent to your mobile'}
                    >
                        <form className="profForm" id="profForm" onSubmit={this.submitVerificationCode}>
                            <div className="form-group d-flex align-items-center veriFy_frmGr">
                                <label className="mb-2">Verification Code</label>
                                <input type="text" className="form-control mb-2" name="verification_code"  onChange={this.handleChange} placeholder="| Enter Code" />
                            </div>
                            <div className="form-group d-flex align-items-center justify-content-center btn_area157_">
                                <div className="cmn_clr1 resent_c " onClick={()=>this.sendVerificationCode()}>Resend Code</div>
                                <div><button className="btn cmn-btn1 btn-block">Submit</button></div>
                            </div>
                        </form>

                    </Modal>
                </AuthWrapper>
            </React.Fragment>
        );
    }
}



const mapStateToProps = (state) => {
return  {
  loginState: state.reducer_staticComp.loginState,
  loginSuccess:state.reducer_staticComp.loginSuccess,
  loginError:state.reducer_staticComp.loginError,
  profileUpdated:state.reducer_staticComp.profileUpdate
  }
};
export default connect(mapStateToProps,{get_profile,updateProfile})(Configuration);
