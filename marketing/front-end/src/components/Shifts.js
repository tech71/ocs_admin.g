import React, { Component } from 'react';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import GMap from './utils/GMap';
import * as CT from '../hoc/Color';
import GraphDoughnut from './utils/GraphDoughnut';
import StackedBarGraph from './utils/StackedBarGraph';
import MainHeading from './utils/MainHeading';
import { ChronologicalYearMonth, nth } from './utils/ChronologicalYearMonth';
import { connect } from 'react-redux';
import { getShiftGraphDetails, getShiftDetailsWithPostCode } from '../store/actions';
import ShiftSpecificCount from './shift/ShiftSpecificCount';
import HotColdSpotPlaceCountCommon from './utils/HotColdSpotPlaceCountCommon';

class Shifts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            view_type: 'year',
            selected_shift_type: '',
            compare_against: 'participant',
            compare_against_2:'new'
        };
    }

    componentDidMount() {
        var req = { view_type: this.state.view_type };
        this.props.getShiftGraphDetails(req);
        var req2 = { selected_shift_type: this.state.selected_shift_type, compare_against: this.state.compare_against, compare_against_2: this.state.compare_against_2 };
        this.props.getShiftDetailsWithPostCode(req2);
    }

    onChangeFIlter = (key, value) => {
        var state = {}
        state[key] = value;

        this.setState(state, () => {
            if (value == 'current_month' || value == 'year') {
                this.setState({ specific_month: '' });
            }

            if (value != 'selected_month') {
                var req = { view_type: this.state.view_type, specific_month: this.state.specific_month };
                this.props.getShiftGraphDetails(req);
            }
        });
    }

    onChangeLocationFilter = (key, value) => {
        var state = {}
        state[key] = value;
        this.setState(state, () => {
            var req = { selected_shift_type: this.state.selected_shift_type, compare_against: this.state.compare_against, compare_against_2: this.state.compare_against_2 };
            this.props.getShiftDetailsWithPostCode(req);
        });
    }

    
    render() {
        var shift_by_post_code= [{type: 'shift', posArr: this.props.shift_by_post_code}];

        var tooltip = { 'role': "tooltip", 'type': "string", 'p': { 'html': true } };
        var shiftGraphLabel = ['Month', 'filled shift', tooltip, 'cancelled shift', tooltip, 'No suitable member available', tooltip];

        var shiftGraphData = this.props.graph_shifts.map((val, index) => {
            return [(val.month),
            parseInt(val.shift_completed), '<div>Shifts Completed:</div><div>' + val.shift_completed + '</div>',
            parseInt(val.shift_unfilled), '<div>Not Filled:</div><div>' + val.shift_unfilled + ' </div>',
            parseInt(val.shift_filled), '<div>Total shift filled:</div><div>' + val.shift_filled + '</div>',
                //parseInt(val.shift_completed), '<div>Total shift filled:</div><div>'+val.shift_completed+'</div>',
            ]
        });

        shiftGraphData = [shiftGraphLabel, ...shiftGraphData]

        const shiftData = {
            labels: ['Morning', 'Evening  Overnight', 'Active Overnight', 'Transferred'],
            datasets: [{
                data: ['200', '120', '50', '10', '5'],
                backgroundColor: [CT.SHIFTCOLOR1, CT.SHIFTCOLOR2, CT.SHIFTCOLOR3, CT.SHIFTCOLOR4, CT.SHIFTCOLOR5]
            }],

        };

        return (
            <React.Fragment>
                <Header />
                <AuthWrapper>
                    <MainHeading name={"Shifts"} />
                    <div className="bor_bot1 pb-4 mt-4 filter_area d-flex align-items-center">
                        <div className="vw_side"><strong>View:</strong></div>
                        <div className="d-flex align-items-center ">
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" checked={this.state.view_type == 'year' || ''} onChange={() => this.onChangeFIlter('view_type', 'year')} />
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'current_month')} />
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>
                            <div className="d-flex align-items-center">
                                <label className="cstm_radio">
                                    <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={() => this.onChangeFIlter('view_type', 'selected_month')} />
                                    <div className="rad_lab">Selected Month</div>
                                </label>
                                <div className="fil_datie">
                                    <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false}
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month' ? false : true}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-4 prog_inact_row">

                        <ShiftSpecificCount {...this.props.shift_filled} count_title="hours of shifts were filled in" />

                        <ShiftSpecificCount {...this.props.shift_requested_by_participant} count_title="hours of shifts were requested by Participants in" />

                        <ShiftSpecificCount {...this.props.avg_shift_filled} count_title="Hours on average filled per day for" />

                        <ShiftSpecificCount {...this.props.shift_requested_via_participant_portal} count_title="hours of shifts requested via Participant portal in" />

                        <ShiftSpecificCount {...this.props.avg_fms} count_title="hours of shifts accepted via Members app in" />

                        <ShiftSpecificCount {...this.props.shift_manually_alloted} count_title="hours of shifts were manually allocated to a Member in" />
                    </div>

                    <div className="main_heading_cmn- mt-2 bor_top1">
                        <h3><strong>Shifts by Postcode</strong></h3>
                    </div>

                    <div className="row mt-4 mb-4 justify-content-end">

                        <div className="col-xl-4 col-lg-5 d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2"><strong>Shift</strong></div>
                            <div className='cstm_select slct_cstm2 bg_grey flex-1'>
                                <Select name="selected_shift_type"
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Select status"
                                    options={this.props.options_shift}
                                    onChange={(e) => this.onChangeLocationFilter('selected_shift_type', e)}
                                    value={this.state.selected_shift_type}
                                />
                            </div>
                        </div>
                        <div className="col-xl-7 col-lg-7  d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2 flex-1 text-center"><strong>Compared Against</strong></div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1 pl-2'>

                                <Select name="compare_against "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Module"
                                    options={this.props.compare_with_dropdown}
                                    onChange={(e) => this.onChangeLocationFilter('compare_against', e)}
                                    value={this.state.compare_against}

                                />
                            </div>

                            <div className='cstm_select  slct_cstm2 bg_grey flex-1  pl-2' >
                                <Select name="compare_against_2"
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Sub-Category"
                                    options={this.props.special_dropdown[this.state.compare_against]}
                                    onChange={(e) => this.onChangeLocationFilter('compare_against_2', e)}
                                    value={this.state.compare_against_2}
                                />
                            </div>
                        </div>

                    </div>
                    {/* row ends */}

                    <div className="map_area mt-3 mb-2">
                        <GMap MarkersModule={{f_module:'fms', s_module: this.state.compare_against}} position={shift_by_post_code} />
                    </div>

                    <div className="main_heading_cmn- mt-4 bor_top1">
                        <h3><strong>Shifts Statistics</strong></h3>
                    </div>

                    <div className="row mt-3 mb-3">
                        <HotColdSpotPlaceCountCommon loopData={this.props.shifts_stats.shift_requested_hot_spots} title={this.props.server_view_type.date+" Shift Request Hot Spots"} color={CT.PARTICIPANTCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.shifts_stats.shift_filled_hot_spots} title={this.props.server_view_type.date+" Filled shifts Hot Spots"} color={CT.MEMBERCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.shifts_stats.shift_requested_cold_spots} title={this.props.server_view_type.date+" Shift Request Cold Spots"} color={CT.PARTICIPANTCOLOR1} />
                        <HotColdSpotPlaceCountCommon loopData={this.props.shifts_stats.shift_filled_cold_spots} title={this.props.server_view_type.date+" Filled shifts Cold Spots"}  color={CT.MEMBERCOLOR1} />
                    </div>

                    {/* row ends */}

                    <div className="row mt-3 mb-3">
                        <div className="col-12">

                            <div className="statis_bx__">
                                <h4 className="cmn_clr1 mg_ics_head  mt-2 text-center">
                                    <strong>Total Shifts in {(this.props.server_view_type.type == 'year') ? 'Financial year' : 'Month'} {this.props.server_view_type.date}</strong>
                                </h4>

                                <div>
                                    <StackedBarGraph
                                        data={shiftGraphData}
                                        xAxisTitle='Qty'
                                        yAxisTitle={this.props.server_view_type.type == 'year' ? "Months of Year" : "Days of Month"}
                                        colors={[CT.SHIFTCOLOR1, CT.SHIFTCOLOR3, CT.PARTICIPANTCOLOR1]}
                                        series={{ 2: { type: 'line' } }}
                                        height={'500px'}
                                    />
                                </div>
                                <div className="mt-3">
                                    <ul className="d-flex tickie_ul pl-5">
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.PARTICIPANTCOLOR1 }}></span>
                                            <div>Shifts Requested</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                            <div>Shifts Completed/Filled</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR3 }}></span>
                                            <div>Cancelled Shifts/Not Filled</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/*<div className="row">
                        <div className="col-xl-6 col-lg-10 col-md-12 col-12">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-4 mt-3 text-center">
                                    <strong>Hours Filled by Shift Type</strong>
                                </h5>
                                <div className="d-flex align-items-end typesParti__">
                                    <div className="partic_dough flex-1 pl-5">
                                        <GraphDoughnut data={shiftData} />
                                    </div>
                                    <div className="legend_tickie">
                                        <ul className=" tickie_ul pl-5">
                                                                                           
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                                <div>Morning</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR2 }}></span>
                                                <div>Evening</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR3 }}></span>
                                                <div>Overnight</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR4 }}></span>
                                                <div>Active Overnight</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR5 }}></span>
                                                <div>Transferred</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>*/}
                </AuthWrapper>
            </React.Fragment>
        );
    }
}
/*const mapStateToProps = state =>{
    console.log('here',state.reducer_shift);
}*/

const mapStateToProps = state => ({
    graph_shifts: state.reducer_shift.graph_data,
    shifts_stats: state.reducer_shift.shifts_stats,
    server_view_type: state.reducer_shift.server_view_type,
    shift_hour_count: state.reducer_shift.shift_hour_count,
    shift_filled: state.reducer_shift.shift_hour_count.shift_filled,
    shift_requested_by_participant: state.reducer_shift.shift_hour_count.shift_requested_by_participant,
    avg_shift_filled: state.reducer_shift.shift_hour_count.avg_shift_filled,
    shift_requested_via_participant_portal: state.reducer_shift.shift_hour_count.shift_requested_via_participant_portal,
    shift_manually_alloted: state.reducer_shift.shift_hour_count.shift_manually_alloted,
    options_shift: state.reducer_shift.options_shift,
    compare_with_dropdown: state.reducer_shift.compare_with_dropdown,
    special_dropdown: state.reducer_shift.special_dropdown,
    shift_by_post_code:state.reducer_shift.shift_by_post_code

})
const mapDispatchtoProps = (dispatch) => {
    return {
        getShiftGraphDetails: (request) => dispatch(getShiftGraphDetails(request)),
        getShiftDetailsWithPostCode: (request) => dispatch(getShiftDetailsWithPostCode(request)),
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Shifts);