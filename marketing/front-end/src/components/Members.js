import React, { Component } from 'react';
import Select from 'react-select-plus';
import DatePicker from "react-datepicker";
import { connect } from 'react-redux';
import Header from './utils/Header';
import AuthWrapper from '../hoc/AuthWrapper';
import GMap from './utils/GMap';
import { Chart } from "react-google-charts";
import * as CT from '../hoc/Color';
import GraphDoughnut from './utils/GraphDoughnut';
import MemberSpecificCount from './member/MemberSpecificCount';
import HotColdSpotPlaceCount from './member/HotColdSpotPlaceCount';

import StackedBarGraph from './utils/StackedBarGraph';
import MainHeading from './utils/MainHeading';
import FunnelGraph from './utils/FunnelGraph';
import {ChronologicalYearMonth, nth} from './utils/ChronologicalYearMonth';

import { getMemberGraphDetails } from '../store/actions';
import { getOptionsOrganisationName } from '../service/common.js';



class Members extends Component {
    constructor(props) {
        super(props);
        this.state = {
            view_type: 'current_month',
            selected_member_type: 'new',
            member_compare_with: 'participant',
            member_compare_with_specific: 'new',
            MarkersColor : {f_color: "#FF8967", s_color: "#FF7045"}
        };
    }
    
    componentDidMount(){
        var req = {view_type: this.state.view_type};
        this.props.getMemberGraphDetails(req);
    }

    onChangeFIlter=(key, value)=>{
        var state = {}
        if(key === 'member_compare_with'){
            state[key] = '';
        }
        state[key] = value;
        
        this.setState(state,()=>{
            if(value == 'current_month' || value == 'year'){
                this.setState({specific_month: ''});
            }
             
            if(value != 'selected_month'){
                var req = {view_type: this.state.view_type, specific_month: this.state.specific_month, selected_member_type: this.state.selected_member_type, member_compare_with: this.state.member_compare_with, member_compare_with_specific: this.state.member_compare_with_specific};
                this.props.getMemberGraphDetails(req);
            }
        });
    }

    render() {
        

        var member_work_area_graph = this.props.member_work_area_graph
        const workAreaGraphData = {
            labels: ['Welfare', 'NDIS/Disability'],
            datasets: [{
                data: [member_work_area_graph.ndis, member_work_area_graph.welfare],
                backgroundColor: [CT.MEMBERCOLOR1, CT.MEMBERCOLOR2]
            }],
            
        };


        /* ------- change api data formate in actual required intake graph data -------- */
        var funnelData = {
            data1: this.props.member_intake_graph.row1,
            data1Bg: [CT.MEMBERCOLOR9, CT.MEMBERCOLOR10, CT.MEMBERCOLOR11, CT.MEMBERCOLOR12, CT.MEMBERCOLOR13, CT.MEMBERCOLOR14, CT.MEMBERCOLOR15],
            data2: this.props.member_intake_graph.row2,
            data2Bg: [CT.COLOR6, CT.COLOR7, CT.COLOR8, CT.COLOR3, CT.COLOR4, CT.COLOR9, CT.COLOR10]
        }
       
        
        /* ----- chnage api data formate in actual required graph data -------*/
        var tooltip = {'role' : "tooltip", 'type': "string", 'p': {'html': true}};
        var shiftGraphLabel = ['Month', 'filled shift', tooltip, 'cancelled shift', tooltip, 'No suitable member available', tooltip];
        
        var shiftGraphData = this.props.graph_shifts;
         shiftGraphData = this.props.graph_shifts.map((val, index) => {
            return [(val.current_term), 
                    parseFloat(val.total_unfilled_shift), '<div>No suitable member available:</div><div>'+val.total_unfilled_shift+' not suitable</div>',
                    parseFloat(val.total_cancel_shift), '<div>Shifts Cancelled:</div><div>'+val.total_cancel_shift+' shift cancelled</div><div>5 shift not refilled</div>',
                    parseFloat(val.total_filled_shift), '<div>Total shift filled:</div><div>'+val.filled_shift_sum+'</div>'+ '<div>Averate '+val.filled_shift_avg+' hrs per day</div>',
                ]
        });
        shiftGraphData = [shiftGraphLabel, ...shiftGraphData]

        /* -------------ready data form member postcode map -------------------*/
        var MemberPostcode= [{type: 'member', posArr: this.props.member_postcode}];
       
        return (

            <React.Fragment>
                <Header />
                <AuthWrapper>

                    <MainHeading name={"Members"} />

                    <div className="bor_bot1 pb-4 mt-4 filter_area d-flex align-items-center">
                        <div className="vw_side"><strong>View:</strong></div>
                        <div className="d-flex align-items-center ">
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Current" name="view" checked={this.state.view_type == 'current_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'current_month')}/>
                                    <div className="rad_lab">Current Month</div>
                                </label>
                            </div>
                            <div className="d-flex align-items-center">
                                <label className="cstm_radio">
                                    <input type="radio" id="Selected" name="view" checked={this.state.view_type == 'selected_month' || ''} onChange={()=>this.onChangeFIlter('view_type', 'selected_month')}/>
                                    <div className="rad_lab">Selected Month</div>
                                </label>
                                <div className="fil_datie">
                                     <Select name="view_by_status "
                                        simpleValue={true}
                                        searchable={false} 
                                        clearable={false}
                                        placeholder=""
                                        options={ChronologicalYearMonth()}
                                        onChange={(e) => this.onChangeFIlter('specific_month', e)}
                                        value={this.state.specific_month}
                                        disabled={this.state.view_type == 'selected_month'? false: true}
                                    />
                                </div>
                            </div>
                            <div className="pr-4">
                                <label className="cstm_radio">
                                    <input type="radio" id="Financial" name="view" checked={this.state.view_type == 'year' || ''} onChange={()=>this.onChangeFIlter('view_type', 'year')}/>
                                    <div className="rad_lab">Current Financial Year</div>
                                </label>
                            </div>
                        </div>
                    </div>


                    <div className="row mt-4 prog_inact_row">

                        <MemberSpecificCount {...this.props.onboard_member_count} count_title="new member onboarded in" />

                        <MemberSpecificCount {...this.props.avg_onboard_member_count} count_title="days on average to onboard a new Member" />
                        
                        <MemberSpecificCount {...this.props.active_member_count} count_title="current active Members in the system as of" />
                        
                        <MemberSpecificCount {...this.props.accepted_shift_count} count_title="accepted shifts from Members" />
                        
                        <MemberSpecificCount {...this.props.count_not_accepted_shift_count} count_title="Members who didn't accepted a shift in" />
                        
                        <MemberSpecificCount {...this.props.re_applied_member_count} count_title="Members who have re-applied to HCM after leaving in" />
                        
                    </div>

                    {/* row ends */}


                    <div className="main_heading_cmn- mt-2 bor_top1">
                        <h3><strong>Members by Postcode</strong></h3>
                    </div>

                    <div className="row mt-4 mb-4 justify-content-end">
                        <div className="col-xl-4 col-lg-5 d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2"><strong>Members</strong></div>
                            <div className='cstm_select slct_cstm2 bg_grey flex-1'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Select status"
                                    options={this.props.member_dropdown}
                                    onChange={(e) => this.onChangeFIlter('selected_member_type', e)}
                                    value={this.state.selected_member_type}

                                />
                            </div>
                        </div>
                        <div className="col-xl-7 col-lg-7  d-flex align-items-center mt-1 mb-1">
                            <div className="pr-2 flex-1 text-center"><strong>Compared Against</strong></div>
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1 pl-2'>
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Module"
                                    options={this.props.compare_with_dropdown}
                                    onChange={(e) => this.onChangeFIlter('member_compare_with', e)}
                                    value={this.state.member_compare_with}

                                />
                            </div>
                           
                            <div className='cstm_select  slct_cstm2 bg_grey flex-1  pl-2' >
                                {this.state.member_compare_with === 'org'? 
                                <Select.Async
                                        cache={false}
                                        name="form-field-name"
                                        clearable={false}
                                        value={this.state.member_compare_with_specific}
                                        loadOptions={(e) => getOptionsOrganisationName(e)}
                                        placeholder='Search'
                                        onChange={(e) => this.onChangeFIlter('member_compare_with_specific', e)}
                                    />: 
                                <Select name="view_by_status "
                                    simpleValue={true}
                                    searchable={false} Clearable={false}
                                    placeholder="Please Select: Sub-Category"
                                    options={this.props.special_dropdown[this.state.member_compare_with]}
                                    onChange={(e) => this.onChangeFIlter('member_compare_with_specific', e)}
                                    value={this.state.member_compare_with_specific}
                                />}
                            </div>
                        </div> 
                    </div>
                    {/* row ends */}

                    <div className="map_area mt-3 mb-2">
                        <GMap MarkersModule={{f_module:'member', s_module: this.state.member_compare_with}} position={MemberPostcode} />
                    </div>

                    <div className="main_heading_cmn- mt-4 bor_top1">
                        <h3><strong>Members Statistics</strong></h3>
                    </div>


                    <div className="row mt-3 mb-3">
                    
                        <HotColdSpotPlaceCount color={CT.MEMBERCOLOR1} place={this.props.onboard_hot_place} title={"Onbording Hot Spots"} />
                        
                        <HotColdSpotPlaceCount color={CT.SHIFTCOLOR1} place={this.props.onboard_cold_place} title={"Filled shifts Hot Spots"} />
                        
                        <HotColdSpotPlaceCount color={CT.MEMBERCOLOR1} place={this.props.fill_shift_hot_place} title={"Onbording Cold Spots"} />
                        
                        <HotColdSpotPlaceCount color={CT.SHIFTCOLOR1} place={this.props.fill_shift_cold_place} title={"Filled shifts Cold Spots"} />
                       
                    </div>
                    {/* row ends */}


                    <div className="row">
                        <div className="col-lg-6 col-md-12 col-12">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-4 mt-3 text-center">
                                    <strong>Intake Drop Out Rate by Section :2018 Vs 2019</strong>
                                </h5>

                                <div className="mb-1 mt-4 funnel_flex">

                                    <div className="l_col">
                                        <ul className=" tickie_ul ">
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR9 }}></span>
                                                <div>Stage 1 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR10 }}></span>
                                                <div>Stage 2 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR11 }}></span>
                                                <div>Stage 3 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR12 }}></span>
                                                <div>Stage 4 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR13 }}></span>
                                                <div>Stage 5 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR14 }}></span>
                                                <div>Stage 6 Last Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR15 }}></span>
                                                <div>Stage 7 Last Year</div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div className="m_col">
                                        <FunnelGraph
                                            data={funnelData}
                                            type='funnel_1'
                                        />
                                    </div>

                                    <div className="r_col">
                                        <ul className="tickie_ul tickie_right">
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR6 }}></span>
                                                <div>Stage 1 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR7 }}></span>
                                                <div>Stage 2 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR8 }}></span>
                                                <div>Stage 3 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR3 }}></span>
                                                <div>Stage 4 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR4 }}></span>
                                                <div>Stage 5 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR9 }}></span>
                                                <div>Stage 6 this Year</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.COLOR10 }}></span>
                                                <div>Stage 7 this Year</div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-12 col-12">
                            <div className="statis_bx__">
                                <h5 className="cmn_clr1 mg_ics_head mb-4 mt-3 text-center">
                                    <strong>Types of Members Work Area Available as of March 2019</strong>
                                </h5>
                                <div className="d-flex align-items-end typesParti__">
                                    <div className="partic_dough flex-1 pl-5">
                                        <GraphDoughnut data={workAreaGraphData} />
                                    </div>
                                    <div className="legend_tickie">
                                        <ul className=" tickie_ul pl-5">


                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR1 }}></span>
                                                <div>Welfare</div>
                                            </li>
                                            <li className="mb-2">
                                                <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR2 }}></span>
                                                <div>NDIS/Disability</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div className="row mt-3 mb-3">
                        <div className="col-12">

                            <div className="statis_bx__">
                                <h4 className="cmn_clr1 mg_ics_head  mt-2 text-center">
                                    <strong>Shifts not filled by Members in {this.props.shift_graph_title}</strong>
                                </h4>

                                <div>
                                    <StackedBarGraph
                                        data={shiftGraphData}
                                        xAxisTitle='Qty'
                                        yAxisTitle={this.state.view_type == 'year'? "Day of Month": "Month of Year"}
                                        colors={[CT.MEMBERCOLOR1, CT.MEMBERCOLOR3, CT.SHIFTCOLOR1]}
                                        series={{ 2: { type: 'line' } }}
                                        height={'500px'}
                                    />
                                </div>
                                <div className="mt-3">
                                    <ul className="d-flex tickie_ul pl-5">
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR1 }}></span>
                                            <div>No Suitable Member Availabe</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.MEMBERCOLOR3 }}></span>
                                            <div>Shift Cancelled</div>
                                        </li>
                                        <li>
                                            <span className="tickie" style={{ backgroundColor: CT.SHIFTCOLOR1 }}></span>
                                            <div>Shifts Filled</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* row ends */}
                </AuthWrapper>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
            ...state.reducer_member,
})
const mapDispatchtoProps = (dispatch) => {
    return {
        getMemberGraphDetails: (request) => dispatch(getMemberGraphDetails(request)),
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Members);