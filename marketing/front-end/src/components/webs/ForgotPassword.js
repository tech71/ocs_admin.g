import React, { Component } from 'react';

import { Link, Redirect } from 'react-router-dom';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import jQuery from "jquery";
import axios from 'axios';
import moment from 'moment-timezone';
import '../../service/jquery.validate.js';
import { setRemeber, getRemeber, setLoginTIme } from '../../service/common.js';
import { connect } from 'react-redux';
import { forgot_password } from '../../store/actions';


class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            error: '',
            success: '', email: ''
        }
    }


    handleChange = (e) => {
        var state = {};
        this.setState({ success: '' });
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }


    forgotSubmit = (e) => {
        e.preventDefault();
        jQuery('#forget_password').validate()
        if (jQuery('#forget_password').valid() && !this.state.loading) {

            let { email } = this.state;
            this.props.forgot_password(this.state)
                .then((result) => {
                    if (result.status) {
                        this.setState({ success: result.success, email: '' });
                        setTimeout(() => this.setState({ redirect: true }), 2000);
                    } else {
                        this.setState({ error: result.error });
                    }
                });

        }
    }




    render() {
        if (this.state.redirect) {
            return (<Redirect to="/login" />);
        }
        return (

            <React.Fragment>
                {//this.state.redirect ? <Redirect to="/dashboard" /> : ''
                }

                <div className='Login_page row '>
                    <div className='col-12' >

                        <div className='log_faceIc'><i className='icon hcm-mg-c-boy'></i></div>
                        <h1 className='cmn_clr1 loginHdng1__'>Hello there,<br /> Welcome to the HCM Marketing Module</h1>

                        <div className='login_box '>
                            <form id="forget_password" className="login-form" method="POST" onSubmit={this.forgotSubmit}>

                                <div className='row'>

                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Email Address:</label>
                                            <input type="text" className="csForm_control" name="email" placeholder="Email Address" value={this.state.email || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>


                                    </div>
                                    <div className='col-sm-12'><br />
                                        <input type='submit' className='cmn-btn1 btn btn-block' value='Request' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.state.success ? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.state.success}</span></div> : ''}
                                        {this.state.error ? <div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.state.error}</span></div> : ''}

                                    </div>

                                    <div className='col-sm-12 text-center'>
                                    <p className='forgot_lnk__'><Link to={ROUTER_PATH} >Login here</Link></p>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>

                </div>

            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        forgot_password: state.reducer_staticComp.loginState,

    }
};
export default connect(mapStateToProps,{forgot_password})(ForgotPassword);
