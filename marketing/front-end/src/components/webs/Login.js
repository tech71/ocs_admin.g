import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import '../../service/jquery.validate.js';
import { connect } from 'react-redux';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { checkItsLoggedIn, postData, getRemeber, setRemeber, setLoginToken, setPermission, setLoginTIme, setFullName} from '../../service/common.js';
import jQuery from "jquery";
import { login } from '../../store/actions';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false,
            remember: '',
            loading: false,
            error: '',
            success: ''
        }
        checkItsLoggedIn(ROUTER_PATH);
    }

    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    loginSubmit = (e) => {
        e.preventDefault();
        jQuery('#login').validate()
        if (jQuery('#login').valid() && !this.state.loading) {
            try {
                let { username, password } = this.state;

                (this.state.remember == 1) ? setRemeber({username: this.state.username, password: this.state.password,remember:this.state.remember}) : setRemeber({username: '', password: '',remember:''});

                this.props.login(this.state)
                .then((res) => {
                if(this.props.loginState)
                {
                    if(this.props.enableTwoFactorLogin){
                        //Redirect to OTP page if 2 factor is enable
                    }
                    else{
                        setTimeout(() => {
                            this.setState({redirect:true});
                        }, 500)
                    }
                }
                });
            }
            catch(ex){
                this.state.error = "Unable to connect to server";
            }
        }
    }

  componentDidMount(){
    var loginCookie = getRemeber();
    this.setState({username: loginCookie.username});
    this.setState({password: loginCookie.password});
    this.setState({remember: loginCookie.remember});
 }

    render() {
        return (
            <React.Fragment>
            {this.state.redirect ? <Redirect to="/dashboard" /> : ''}
            {this.props.enableTwoFactorLogin ? <Redirect to={{ pathname: '/login_otp', state: { 'loggedIn':this.props.info.loggedIn,'fullname':this.props.info.fullname,requestForOtp: true,username: this.state.username, password: this.state.password,phone:this.props.info.phone } }} /> : ''}


               <div className='Login_page row '>

                    <div className='col-12' >

                        <div className='log_faceIc'><i className='icon hcm-mg-c-boy'></i></div>

                        <h1 className='cmn_clr1 loginHdng1__'>Hello there,<br /> Welcome to the HCM Marketing Module</h1>

                        <div className='login_box '>
                            <form id="login" className="login-form" onSubmit={this.loginSubmit}>

                                <div className='row'>

                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Username:</label>
                                            <input type="text" className="csForm_control" name="username" placeholder="Username"
                                            value={this.state.username || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>

                                        <div className="csform-group">
                                            <label className='inpLabel1'>Password:</label>
                                            <input type="password" className="csForm_control" name="password" placeholder="Password" value={this.state.password || ''} onChange={this.handleChange} data-rule-required="true" />
                                        </div>

                                    </div>
                                    <div className='col-sm-12'>
                                        <label className='d-flex align-items-center rmbrDv__ mr_t_15 justify-content-end'>
                                            <label className='cstmChekie clrTpe2'>
                                                <input type="checkbox" className=" " name="remember"  value={this.state.remember || ''}  checked={(this.state.remember)} onChange={this.handleChange}/>
                                                <div className="chkie">Remember Password</div>
                                            </label>

                                        </label>
                                    </div>
                                    <div className='col-sm-12'>
                                            <input type='submit' className='cmn-btn1 btn btn-block' value='Log In' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.props.loginSuccess? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.props.loginSuccess}</span></div>:''}
                                        {this.props.loginError?<div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.props.loginError}</span></div>:''}

                                    </div>
                                    <div className='col-sm-12 text-center'>
                                        <p className='forgot_lnk__'>
                                            <Link to={ROUTER_PATH + 'forgot_password'}>Forgot Password?</Link>
                                        </p>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>


                    <div className="foot_cmn login_foot cmn_clr1">
                        <div>Terms & Conditions <div>&copy; All Rights Reserved Healthcare Manager</div></div>
                    </div>
                </div>

            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
return  {
  loginState: state.reducer_staticComp.loginState,
  loginSuccess:state.reducer_staticComp.loginSuccess,
  loginError:state.reducer_staticComp.loginError,
  enableTwoFactorLogin:state.reducer_staticComp.enableTwoFactorLogin,
  info:state.reducer_staticComp.info,
  }
};
export default connect(mapStateToProps,{login})(Login);
