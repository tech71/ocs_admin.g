import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import '../../service/jquery.validate.js';
import { connect } from 'react-redux';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { checkItsLoggedIn, postData, getRemeber, setRemeber, setLoginToken, setPermission, setLoginTIme, setFullName} from '../../service/common.js';
import jQuery from "jquery";
import { login } from '../../store/actions';
import { ToastContainer,toast } from 'react-toastify';
import { ToastUndo } from '../../service/ToastUndo.js';
import moment from 'moment-timezone';


class TwoFactorLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect:false,
            remember: '',
            loading: false,
            error: '',
            success: ''
        }
        checkItsLoggedIn(ROUTER_PATH);
    }

    handleChange = (e) => {
        var state = {};
        this.setState({ error: '' });
        state[e.target.name] = (e.target.type === 'checkbox' ? e.target.checked : e.target.value);
        this.setState(state);
    }

    loginSubmit = (e) => {
        e.preventDefault();
        jQuery('#codeForm').validate()
        if (jQuery('#codeForm').valid() && !this.state.loading) {
                          
            postData('marketing/login/check_login_with_code', {username: this.props.props.location.state.username, password: this.props.props.location.state.password,code:this.state.code}).then((result) => {
                if (result.status) { 
                    localStorage.setItem("marketing_membertoken", result.token);
                    localStorage.setItem("dateTime",moment());

                    setTimeout(() => {
                        this.setState({redirect:true});
                    }, 500)
                } else { 
                    toast.error(<ToastUndo message={result.error} showType={'e'} />, {
                        position: toast.POSITION.TOP_CENTER,
                        hideProgressBar: true
                    });
                }
            });            
        }
    }

    resendCode =(e) =>{
        e.preventDefault();        
        postData('marketing/login/send_authentication_code', {loggedIn:this.props.props.location.state.loggedIn,fullname:this.props.props.location.state.fullname}).then((result) => {
            if (result.status) {
                this.setState({ loading: false });
                toast.success(<ToastUndo message={result.msg} showType={'s'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            } else {
                toast.error(<ToastUndo message={result.msg} showType={'e'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                });
            }
        });
    }

  componentDidMount(){
    
    if(this.props.props.location.state && this.props.props.location.state.requestForOtp){
        
    }else{
        this.setState({invalid_request:true});
    }
 }

    render() {
    
        return (
            <React.Fragment>
            {this.state.redirect ? <Redirect to="/dashboard" /> : ''}

               <div className='Login_page row '>

                    <div className='col-12' >
                        <div className='log_faceIc'><i className='icon hcm-mg-c-boy'></i></div>
                        <h1 className='cmn_clr1 loginHdng1__'>Hello {this.props.props.location.state.fullname},<br /> Welcome to the HCM Marketing Module</h1>

                        <div className='login_box '>
                            <form id="codeForm" className="login-form" onSubmit={this.loginSubmit}>
                                <div className='row'>
                                    <span>Please enter the verification code sent to mobile number {this.props.props.location.state.phone}</span>
                                    <div className='col-sm-12'>
                                        <div className="csform-group">
                                            <label className='inpLabel1'>Verification Code:</label>
                                            <input type="text" className="csForm_control" name="code" placeholder="" onChange={this.handleChange} data-rule-required="true" />
                                        </div>
                                    </div>
                                    
                                    <div className='col-sm-12'>
                                        <input type='submit' className='cmn-btn1 btn btn-block' value='Submit' />
                                    </div>

                                    <div className='col-sm-12'>
                                        {this.props.loginSuccess? <div className='validMsgBox__ successMsg1'><i className="icon icon-input-type-check"></i><span>{this.props.loginSuccess}</span></div>:''}
                                        {this.props.loginError?<div className='validMsgBox__ errorMsg1'><i className="icon icon-alert"></i><span>{this.props.loginError}</span></div>:''}

                                    </div>
                                    <div className='col-sm-12 text-center'>
                                        <p className='forgot_lnk__'>
                                            <a onClick={(e)=>this.resendCode(e)}>Resend Code</a>
                                        </p>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    <div className="foot_cmn login_foot cmn_clr1">
                        <div>Terms & Conditions <div>&copy; All Rights Reserved Healthcare Manager</div></div>
                    </div>
                </div>

            </React.Fragment>

        );
    }
}

const mapStateToProps = (state) => {
return  {
  loginState: state.reducer_staticComp.loginState,
  loginSuccess:state.reducer_staticComp.loginSuccess,
  loginError:state.reducer_staticComp.loginError,
  enableTwoFactorLogin:state.reducer_staticComp.enableTwoFactorLogin,
  info:state.reducer_staticComp.info,
  }
};
export default connect(mapStateToProps,{login})(TwoFactorLogin);
