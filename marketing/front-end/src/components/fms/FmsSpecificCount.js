import React, { Component } from 'react';
import { connect } from 'react-redux';

class Members extends Component {
    render() {
        console.log(this.props.server_view_type);
        var incon_color = ((this.props.status == 1)? 'green': ((this.props.status == 3)? '': 'red'));
        var incon_up_down = ((this.props.status == 1)? 'profit': ((this.props.status == 3)? '': 'lost'));
        var inc_decr = ((this.props.status == 1)? 'increase': 'decrease');
        
        var percentage_string  = this.props.status == 3 ? 
                "No changes between "+((this.props.server_view_type.type=='year')?'Financial year':'previous year same month') : 
                "thats a "+this.props.percent+"% "+inc_decr+" compared to "+((this.props.server_view_type.type=='year')?'previous financial year':'previous year same month');
        return (
            <React.Fragment>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-12">
                            <div className="inact_box__ fnt_big">
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>{this.props.current}</strong></div>
                                    <div className="pl-2 desc_int" >{this.props.count_title} {this.props.server_view_type.date}</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className="pr-2 val_int"><strong>{this.props.previous}</strong></div>
                                    <div className="pl-2 desc_int" >{this.props.count_title} {this.props.server_view_type.previous_year}</div>
                                </div>
                                <div className="mb-2 intcts">
                                    <div className={"pr-2 val_int icon_ar "+ incon_color}>
                                    <i className={"icon hcm-mg-ie-"+incon_up_down+" arw_ic_pr"}></i></div>
                                    <div className="pl-2 desc_int " >{percentage_string}</div>
                                </div>
                            </div>
                        </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    server_view_type: state.reducer_fms.server_view_type,
})
const mapDispatchtoProps = (dispatch) => {
    return {
       
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(Members);