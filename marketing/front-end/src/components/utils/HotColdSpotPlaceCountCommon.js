import React, { Component } from 'react';
import { connect } from 'react-redux';

class HotColdSpotPlaceCountCommon extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <div className="col-xl-3 col-lg-6 col-md-6 col-12 mb-2">
                    <div className="statis_bx__">
                        <h6 className="cmn_clr1 mg_ics_head mb-2 mt-2 d-flex align-items-center  ">
                            <div className="mg_ics" style={{ color: this.props.color }}><i className="icon hcm-mg-location"></i></div>
                            <div><strong>{this.props.title}</strong></div>
                        </h6>
                        <div className="marker_listBox">
                            {(this.props.loopData.length > 0) ? this.props.loopData.map((val, idx) => (
                                <div><span className="pr-2">{idx + 1}.</span> {val.address} ({val.my_count})</div>
                            )) : 'No ' + this.props.title}

                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default  HotColdSpotPlaceCountCommon
