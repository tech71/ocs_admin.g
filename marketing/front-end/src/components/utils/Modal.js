import React from 'react';

const Modal = (props) => {
    return(
        <div className={"cstm_Modal__" + " " + (props.show? "show" : "")}>
                <div className={"modal-dialog__"}>
                    <i className="icon hcm-mg-cancel close_modal" onClick={props.close}></i>
                    <h2>{props.heading}</h2>

                    <div className="cstmModal_body__">
                        {props.children}
                    </div>
                </div>
        </div>
    );
}


Modal.defaultProps = {
    show: false,
    children: "",
    heading:"Modal heading"
}


export default Modal;