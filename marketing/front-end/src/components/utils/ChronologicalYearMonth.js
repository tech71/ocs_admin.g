import React, { Component } from 'react';

export function ChronologicalYearMonth() {

    var date = new Date();

    var current_month = date.getMonth() + 1;
    var current_year = date.getFullYear();

    date.setDate(date.getDate() - 6);
    date.setFullYear(date.getFullYear() - 2);

    var starting_month = date.getMonth() + 1;
    var starting_year = date.getFullYear();

    var option = []
    var i = 0;
    for (starting_month, starting_year; i < 24; starting_month++) {
        var x = {value: starting_month + '-' + starting_year, label: ("0" + starting_month).slice(-2) + '/' + starting_year};


        option[i] = x;
        if (starting_month == 12) {
            starting_month = 1;
            starting_year++
        }

        if (starting_year == current_year && starting_month == current_month) {
            break;
        }

        i++;
    }

    option.reverse();
    return option;
}

export const nth = function (d) {
    if (d > 3 && d < 21)
        return 'th';
    switch (d % 10) {
        case 1:
            return "st";
        case 2:
            return "nd";
        case 3:
            return "rd";
        default:
            return "th";
    }
}
export const months = (d, small) => {
    var months = {1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June",
        7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December"};
    
     var monthsSmall = {1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun",
        7: "Jul", 8: "Aug", 9: "Sept", 10: "Oct", 11: "Nov", 12: "Dec"};   
    
    if(small){
       return monthsSmall[d];
    }
    return months[d];
}

