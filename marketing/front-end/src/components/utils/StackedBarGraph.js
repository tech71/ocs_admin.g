import React from 'react';
import { Chart } from "react-google-charts";
import * as CT from '../../hoc/Color';

const StackedBarGraph = (props) => {
    return (
        <Chart
            //  width="100%"
            height={props.height}
            chartType="ComboChart"
            loader={<div>Loading Chart</div>}
            data={props.data}
            options={{
                title: '',
                vAxis: { title: props.xAxisTitle, gridlines:{color:'transparent'} },
                hAxis: { title: props.yAxisTitle },
                isStacked: true,
                seriesType: 'bars',
                series: props.series,
                legend: { position: "none" },
                colors: props.colors,
                backgroundColor: 'transparent',
                chartArea:{width:'90%'},
                tooltip: { isHtml: true, trigger: "visible" }
          
            }}
        />
    );
}

StackedBarGraph.defaultProps = {
    height: '300px',
    data:[
        [
            'Month',
            'NDIS Funded',
            'Welfare Funded',
            'Shifts Filled',
        ],
        ['1st', 500, 938, 614.6],
        ['2nd', 250, 1120, 682],
        ['3rd', 50, 1167, 623],
        ['4th', 180, 1110, 609.4]
    ],
    xAxisTitle :'X Axis',
    yAxisTitle : 'Y Axis',
    colors:[CT.COLOR1,CT.COLOR2,CT.COLOR3,],
    series:{2: {type: 'line'}}

}
export default StackedBarGraph;