import React, { Component } from 'react';
import Select from 'react-select-plus';
import { connect } from 'react-redux';
import * as actionType from '../../store/actions';
import { ROUTER_PATH, BASE_URL } from '../../config.js';
import { NavLink } from 'react-router-dom';
import { logout } from '../../service/common.js';

import moment from 'moment-timezone';

class Header extends Component {

    constructor() {
        super();
        this.state = {
            filterVal: '',
            drop1: true,
            drop2: true
            // logout: false
        }
    }
      
    signOut = () => {
        logout(ROUTER_PATH);
    }

    componentDidMount() {
        //console.log('here',this.props);
    }

    render() {

        return (

            <React.Fragment>

                <div className={'backdrop ' + (this.props.sidebarState === 'left' || this.props.sidebarState === 'right' ? 'show' : '')} onClick={() => this.props.closeSidebar()} ></div>

                <header className='main_header'>
                    <div className='row align-items-center'>
                        <div className='col-3 d-flex'>
                            {/*<i className='icon hcm-mg-bars hdrs_ics' onClick={() => this.props.sideBarOpen('info', 'left')}></i>*/}
                        </div>
                        <div className='col-6'> </div>
                        <div className='col-3 text-right'>
                            <div className='right_ul'>
                                <span className='Ulopts' onClick={this.signOut}>
                                    <i className='icon hcm-mg-plane hdrs_ics'></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                
                /*<nav c lassName={'sidebar left ' + (this.props.sidebarState === 'left' ? 'toggle_left' : '')}>   </nav>*/
                {/* left sidebar */}


                <nav className={'sidebar right ' + (this.props.sidebarState === 'right' ? 'toggle_right' : '')}>       </nav>
                {/* right sidebar */}

                <aside className={'main_sidebar__ ' + (this.state.fixedSidebarState ? 'fixed' : '')}>
                    <div className='sideHead1__'>
                        Marketing
                    </div>
                    <ul className='sideNavies__'>
                        <li>
                            <NavLink to={'/dashboard'}>Dashboard</NavLink>
                        </li>



                        <li className='dropdownMenu'>
                            <div className={'drpHdng' + ' ' + (this.state.drop1 ? 'open' : '')} onClick={() => this.setState({ drop1: !this.state.drop1 })}>
                                <span><strong>Company</strong></span>
                                <i className='icon hcm-mg-caret-right'></i>
                            </div>
                            <ul>
                                <li><NavLink to={'/participants'}>Participant</NavLink></li>
                                <li><NavLink to={'/members'}>Members</NavLink></li>
                                <li><NavLink to={'/organisations'}>Orgs</NavLink></li>
                                <li><NavLink to={'/shifts'}>Shifts</NavLink></li>
                                <li><NavLink to={'/fms'}>FMS</NavLink></li>
                            </ul>
                        </li>

                        <li className='dropdownMenu'>
                            <div className={'drpHdng' + ' ' + (this.state.drop2 ? 'open' : '')} onClick={() => this.setState({ drop2: !this.state.drop2 })}>
                                <span><strong>Reach</strong></span>
                                <i className='icon hcm-mg-caret-right'></i>
                            </div>
                            <ul>
                                <li><NavLink to={'#'}>Google</NavLink></li>
                                <li><NavLink to={'#'}>Facebook</NavLink></li>
                                <li><NavLink to={'/configuration'}>Configuration</NavLink></li>
                            </ul>
                        </li>

                        <li>
                            <NavLink to={'/profile'}>Profile</NavLink>
                        </li>


                    </ul>

                    {this.props.children}

                </aside>

            </React.Fragment>

        );
    }
}

const mapStateToProps = state => {
    return {
        sidebarState: state.reducer_staticComp.sidebarState,
        
    };
};

const mapDispatchToProps = dispatch => {
    return {
        sideBarOpen: (dataAb, position) => dispatch({ type: actionType.SIDEBAROPEN, pos: position, data: dataAb }),
        logOut: () => dispatch({ type: actionType.LOGOUT }),
        closeSidebar: () => dispatch({ type: actionType.SIDEBARCLOSE })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
