import React, { Component } from 'react';


class FunnelGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            type: this.props.type
        };

    }



    tooltip = (val, ctx) => {

        var x = this.state.xpointPath;
        var y = this.state.ypointPath;

        ctx.restore();
        ctx.globalCompositeOperation = 'source-over';
        // console.log(x, y, val)
        var measureWid = ctx.measureText(val).width;

        var xCordT = x;
        var yCordT = y;


        if (yCordT < 50) {
            yCordT = yCordT + 40;
        }

        ctx.beginPath();
        ctx.moveTo(xCordT, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.fillStyle = "#000";
        ctx.fill();
        ctx.closePath();

        ctx.font = "14px sans";
        ctx.fillStyle = "#fff";
        ctx.fillText(
            val, //value 
            xCordT + 10 - measureWid / 2 - 10, //x coord 
            yCordT - 5   //y coord 
        );
    }


    createShapes = (MainCoordsArray, ctx) => {



        var xpointPath = this.state.xpointPath;
        var ypointPath = this.state.ypointPath;

        for (var i = 0; i < MainCoordsArray.length; i++) {


            ctx.beginPath();
            ctx.globalCompositeOperation = 'destination-over';
            ctx.rect(MainCoordsArray[i].x, MainCoordsArray[i].y, MainCoordsArray[i].w, MainCoordsArray[i].h);
            ctx.fillStyle = MainCoordsArray[i].bg;

            var bgs = '';

            if (xpointPath !== undefined && ypointPath !== undefined) {
                bgs = ctx.isPointInPath(xpointPath, ypointPath) ? 1 : 0.8
            } else {
                bgs = 0.8;
            }


            ctx.globalAlpha = bgs
            ctx.fill();
            ctx.closePath();

           
            if (ctx.isPointInPath(xpointPath, ypointPath)) {
                this.tooltip(this.state.concatData[i], ctx)
            }

        }

    }


    createCoords = (mainArray, coordArray, color, key) => {


        var paddingLeft = this.state.paddingLeft;
        var paddingTop = this.state.paddingTop;
        var leftPart = this.state.leftPart;
        var rightPart = this.state.rightPart;

        var xCord = 0 + paddingLeft;
        var yCord = 0;


        if (key == 'rightPart') {
            xCord = rightPart + paddingLeft;
        }

        for (var i = 0; i < mainArray.length; i++) {
            var obj = {
                x: xCord,
                y: yCord + paddingTop,
                w: leftPart,
                h: mainArray[i],
                bg: color[i]
            }
            coordArray.push(obj);
            yCord = yCord + mainArray[i];
        }
    }



    funnel = (xblock, yblock, type, ctx) => {

        ctx.beginPath();
        var paddingLeft = this.state.paddingLeft;
        var paddingTop = this.state.paddingTop;

        switch (type) {
            case 'funnel_1':
                ctx.moveTo(xblock + paddingLeft, 0 + paddingTop);
                ctx.lineTo(xblock * 10, 0 + paddingTop);
                ctx.lineTo(xblock * 8 + paddingLeft, yblock * 3 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 4 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 6.5 + paddingTop);
                ctx.lineTo(xblock * 7.2 + paddingLeft, yblock * 8 + paddingTop);
                ctx.lineTo(xblock * 6 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 8.1 + paddingTop);
                ctx.lineTo(xblock * 2.2 + paddingLeft, yblock * 6.2 + paddingTop);
                ctx.lineTo(xblock * 1.9 + paddingLeft, yblock * 4.2 + paddingTop);
                ctx.lineTo(xblock * 1.9 + paddingLeft, yblock * 3 + paddingTop);
           

            case 'funnel_2':
                ctx.moveTo(xblock + paddingLeft, 0 + paddingTop);
                ctx.lineTo(xblock * 10, 0 + paddingTop);
                ctx.lineTo(xblock * 8 + paddingLeft, yblock * 3.33 + paddingTop);
                ctx.lineTo(xblock * 7.75 + paddingLeft, yblock * 5.2 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 6.7 + paddingTop);
                ctx.lineTo(xblock * 7.3 + paddingLeft, yblock * 8.5 + paddingTop);
                ctx.lineTo(xblock * 6.2 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 8.5 + paddingTop);
                ctx.lineTo(xblock * 2.4 + paddingLeft, yblock * 6.7 + paddingTop);
                ctx.lineTo(xblock * 2.2 + paddingLeft, yblock * 5 + paddingTop);
                ctx.lineTo(xblock * 2.5 + paddingLeft, yblock * 3.3 + paddingTop);
            
        }



        ctx.fillStyle = "transparent";
        ctx.fill();
        ctx.closePath();
        ctx.clip();


    }


    convertToPercentage = (arr, total, percDataArray) => {
        arr.forEach(myFunc);
        function myFunc(value, index, array) {
            var calcPercentage = value / total * 100;
            percDataArray.push(calcPercentage);
        }
    }

    percentageToNumber = (percdataArray, mainArr) => {
        percdataArray.map((perc, i) => {
            var calcNumber = perc * this.state.funnelHeight / 100;
            mainArr.push(calcNumber);
        })
    }


    creationFunnelHandler = (ctx) => {


        //dynamic data
        var data1 = this.state.data.data1;
        var data1Bg = this.state.data.data1Bg;

        var data2 = this.state.data.data2;
        var data2Bg = this.state.data.data2Bg;

        var data1Total = data1.length > 0? (data1.reduce((a, b) => parseFloat(a) + parseFloat(b))) : 0;
        var data2Total = data2.length > 0? (data2.reduce((a, b) => parseFloat(a) + parseFloat(b))) : 0;
        
        var concatData = data1.concat(data2);

        this.setState({
            concatData :concatData
        })

        var percentageData1 = [];
        var mainData1 = [];

        var percentageData2 = [];
        var mainData2 = [];

        

        this.convertToPercentage(data1, data1Total, percentageData1);
        this.convertToPercentage(data2, data2Total, percentageData2);


        this.percentageToNumber(percentageData1, mainData1);
        this.percentageToNumber(percentageData2, mainData2);


        var xblock = this.state.funnelWidth / 10;
        var yblock = this.state.funnelHeight / 10;

        this.funnel(xblock, yblock, this.state.type, ctx);

        // final coordinate calc
        var leftRects1 = [];
        var rightRects1 = [];

        this.createCoords(mainData1, leftRects1, data1Bg, 'leftPart');
        this.createCoords(mainData2, rightRects1, data2Bg, 'rightPart');

        var MainCoordsArray = leftRects1.concat(rightRects1);

        this.createShapes(MainCoordsArray, ctx);
    }

    initializeFunnel = () => {
        
        var funnelContainer = this.refs.funnelContainer;
        var funnel = this.refs.canvas;

        var containerWidth = funnelContainer.offsetWidth;

        funnel.width = containerWidth;
        funnel.height = funnel.width/5 * 4 ; //for 5/4 ratio

        var paddingLeft = 10;
        var paddingTop = 10;

        var funnelWidth = funnel.width - paddingLeft * 2;
        var funnelHeight = funnel.height - paddingTop * 2;

        var leftPart = funnelWidth / 2 - 7;
        var rightPart = funnelWidth / 2 + 7;

        var ctx = funnel.getContext("2d");

        this.setState({
            paddingLeft:paddingLeft,
            paddingTop:paddingTop,
            funnelWidth:funnelWidth,
            funnelHeight:funnelHeight,
            leftPart:leftPart,
            rightPart:rightPart
        },
            () => this.creationFunnelHandler(ctx)
        )
    }


    componentWillMount(){
        var data = this.props.data;
        var type = this.props.type;
       
        this.setState({data:data, type:type}, 
        () => { this.initializeFunnel(); })
    }

    componentDidMount(){
        
        window.addEventListener("resize", this.initializeFunnel);
        
        var funnel = this.refs.canvas;
        var ctx = funnel.getContext("2d");

        funnel.onmousemove = (e) => {
           
            var rect = e.target.getBoundingClientRect();
            var x = e.clientX - rect.left;
            var y = e.clientY - rect.top;     
   
            ctx.clearRect(0, 0, funnel.width, funnel.height);

            this.setState({
                xpointPath : x,
                ypointPath : y
            },
            () => this.initializeFunnel());

        }

        funnel.onmouseleave = (e) => {
            this.setState({
                xpointPath : 0,
                ypointPath : 0
            },
            () => this.initializeFunnel());
        }
        
    }

    componentWillReceiveProps(nextProps){
        var data = nextProps.data;
        var type = nextProps.type;
        this.setState({data:data, type:type}, 
            () => { this.initializeFunnel(); })
    }

    componentWillUnmount(){
        window.removeEventListener("resize", this.initializeFunnel);
    }

    render() {

        return (
            <div className="funnelContainer" ref={'funnelContainer'} >
                <canvas ref={'canvas'}></canvas>
            </div>
        );
    }



}

FunnelGraph.defaultProps = {
    data: {
        data1: [1, 1, 1, 1, 1],
        data1Bg: ['#383838', '#4f4f4f', '#787878', '#9c9c9c', '#b3b3b3'],
        data2: [1, 1, 1, 1, 1],
        data2Bg: ['#383838', '#4f4f4f', '#787878', '#9c9c9c', '#b3b3b3']
    },
    type: 'funnel_1'
}

export default FunnelGraph;