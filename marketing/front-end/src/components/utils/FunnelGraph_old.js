import React, { Component } from 'react';

let containerWidth, funnel, paddingLeft, paddingTop, funnelWidth, funnelHeight, leftPart, rightPart, ctx, concatData, MainCoordsArray, funnelContainer;

class FunnelGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            type: this.props.type
        };

    }


    componentWillReceiveProps(props) {
        this.setState({ data: props.data }, () => this.creationFunnelHandler())
    }

    convertToPerc = (arr, total, percDataArray) => {
        arr.forEach(myFunc);
        function myFunc(value, index, array) {
            var calcPercentage = value / total * 100;
            percDataArray.push(calcPercentage);
        }
    }


    funnel = (xblock, yblock, type) => {

        ctx.beginPath();

        switch (type) {
            case 'funnel_1':
                ctx.moveTo(xblock + paddingLeft, 0 + paddingTop);
                ctx.lineTo(xblock * 10, 0 + paddingTop);
                ctx.lineTo(xblock * 8 + paddingLeft, yblock * 3 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 4 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 6.5 + paddingTop);
                ctx.lineTo(xblock * 7.2 + paddingLeft, yblock * 8 + paddingTop);
                ctx.lineTo(xblock * 6 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 8.1 + paddingTop);
                ctx.lineTo(xblock * 2.2 + paddingLeft, yblock * 6.2 + paddingTop);
                ctx.lineTo(xblock * 1.9 + paddingLeft, yblock * 4.2 + paddingTop);
                ctx.lineTo(xblock * 1.9 + paddingLeft, yblock * 3 + paddingTop);
           

            case 'funnel_2':
                ctx.moveTo(xblock + paddingLeft, 0 + paddingTop);
                ctx.lineTo(xblock * 10, 0 + paddingTop);
                ctx.lineTo(xblock * 8 + paddingLeft, yblock * 3.33 + paddingTop);
                ctx.lineTo(xblock * 7.75 + paddingLeft, yblock * 5.2 + paddingTop);
                ctx.lineTo(xblock * 7.8 + paddingLeft, yblock * 6.7 + paddingTop);
                ctx.lineTo(xblock * 7.3 + paddingLeft, yblock * 8.5 + paddingTop);
                ctx.lineTo(xblock * 6.2 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 10 + paddingTop);
                ctx.lineTo(xblock * 3 + paddingLeft, yblock * 8.5 + paddingTop);
                ctx.lineTo(xblock * 2.4 + paddingLeft, yblock * 6.7 + paddingTop);
                ctx.lineTo(xblock * 2.2 + paddingLeft, yblock * 5 + paddingTop);
                ctx.lineTo(xblock * 2.5 + paddingLeft, yblock * 3.3 + paddingTop);
            
        }



        ctx.fillStyle = "transparent";
        ctx.fill();
        ctx.closePath();
        ctx.clip();


    }

    canvasParams = () => {

        containerWidth = funnelContainer.offsetWidth;

        funnel.width = containerWidth;
        funnel.height = funnel.width * 4 / 5;

        paddingLeft = 10;
        paddingTop = 10;

        funnelWidth = funnel.width - paddingLeft * 2;
        funnelHeight = funnel.height - paddingTop * 2;

        leftPart = funnelWidth / 2 - 7;
        rightPart = funnelWidth / 2 + 7;

        ctx = funnel.getContext("2d");


    }

    createCoords = (mainArray, coordArray, color, key) => {

        var xCord = 0 + paddingLeft;
        var yCord = 0;


        if (key == 'rightPart') {
            xCord = rightPart + paddingLeft;
        }

        for (var i = 0; i < mainArray.length; i++) {
            var obj = {
                x: xCord,
                y: yCord + paddingTop,
                w: leftPart,
                h: mainArray[i],
                bg: color[i]
            }
            coordArray.push(obj);
            yCord = yCord + mainArray[i];
        }
    }


    tooltip = (x, y, val) => {


        ctx.restore();
        ctx.globalCompositeOperation = 'source-over';
        // console.log(x, y, val)
        var measureWid = ctx.measureText(val).width;

        var xCordT = x;
        var yCordT = y;


        if (yCordT < 50) {
            yCordT = yCordT + 40;
        }

        ctx.beginPath();
        ctx.moveTo(xCordT, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.fillStyle = "#000";
        ctx.fill();
        ctx.closePath();

        ctx.font = "14px sans";
        ctx.fillStyle = "#fff";
        ctx.fillText(
            val, //value 
            xCordT + 10 - measureWid / 2 - 10, //x coord 
            yCordT - 5   //y coord 
        );
    }

    createShapes = (xpointPath, ypointPath) => {

        for (var i = 0; i < MainCoordsArray.length; i++) {

            // tooltip(xpointPath, ypointPath, 50);

            ctx.beginPath();
            ctx.globalCompositeOperation = 'destination-over';
            ctx.rect(MainCoordsArray[i].x, MainCoordsArray[i].y, MainCoordsArray[i].w, MainCoordsArray[i].h);
            ctx.fillStyle = MainCoordsArray[i].bg;

            var bgs = '';

            if (xpointPath !== undefined && ypointPath !== undefined) {
                bgs = ctx.isPointInPath(xpointPath, ypointPath) ? 1 : 0.8
            } else {
                bgs = 0.8;
            }


            ctx.globalAlpha = bgs
            ctx.fill();
            ctx.closePath();


            if (ctx.isPointInPath(xpointPath, ypointPath)) {
                this.tooltip(xpointPath, ypointPath, concatData[i])
            }

        }

    }


    creationFunnelHandler = () => {

        this.canvasParams();

        //dynamic data
        var data1 = this.state.data.data1;
        var data1Bg = this.state.data.data1Bg;

        var data2 = this.state.data.data2;
        var data2Bg = this.state.data.data2Bg;

        var data1Total = data1.reduce((a, b) => a + b);
        var data2Total = data2.reduce((a, b) => a + b);

        concatData = data1.concat(data2);

        var percentageData1 = [];
        var mainData1 = [];

        var percentageData2 = [];
        var mainData2 = [];

        this.convertToPerc(data1, data1Total, percentageData1);
        this.convertToPerc(data2, data2Total, percentageData2);


        function percToNumber(percArray, mainArr) {
            percArray.map((perc, i) => {
                var calcNumber = perc * funnelHeight / 100;
                mainArr.push(calcNumber);
            })
        }


        percToNumber(percentageData1, mainData1);
        percToNumber(percentageData2, mainData2);


        var xblock = funnelWidth / 10;
        var yblock = funnelHeight / 10;

        this.funnel(xblock, yblock, this.state.type);

        // final coordinate calc
        var leftRects1 = [];
        var rightRects1 = [];

        this.createCoords(mainData1, leftRects1, data1Bg, 'leftPart');
        this.createCoords(mainData2, rightRects1, data2Bg, 'rightPart');

        MainCoordsArray = leftRects1.concat(rightRects1);

        this.createShapes(0, 0);
    }


    intitializeFunnel = () => {

        funnel = document.createElement("CANVAS");

        this.creationFunnelHandler();


        funnel.onmousemove = (e) => {
            var rect = e.target.getBoundingClientRect(),
                x = e.clientX - rect.left,
                y = e.clientY - rect.top;

            ctx.clearRect(0, 0, funnel.width, funnel.height);
            this.createShapes(x, y);

        };

        funnelContainer.append(funnel);

    }




    resizeHandler = () => {
        this.creationFunnelHandler();
    }

    componentDidMount() {
        funnelContainer = this.refs.funnelContainer;
        this.intitializeFunnel();
        window.addEventListener("resize", this.resizeHandler);
    }

    mouseLeaveHandler = () => {
        this.creationFunnelHandler();
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeHandler);
    }


    render() {

        return (
            <div id="funnelContainer" ref={'funnelContainer'} onMouseLeave={this.mouseLeaveHandler}></div>
        );
    }



}

FunnelGraph.defaultProps = {
    data: {
        data1: [11000, 40000, 50000, 20000, 10000],
        data1Bg: ['#b175ff', '#c497fe', '#ebdcff', '#f9f6fd', '#ffffff'],
        data2: [11000, 40000, 50000, 20000, 10000],
        data2Bg: ['#ff8a67', '#ffa88d', '#fee1d8', '#fef8f6', '#ffffff']
    },
    type: 'funnel_1'
}

export default FunnelGraph;