import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import PropTypes from 'prop-types';

const GraphDoughnut = (props) => {
    return (
        <Doughnut
            data={props.data}
            height={props.height}
            legend={null}
        />
    );
}

GraphDoughnut.propTypes = {
    height:PropTypes.number,
    data:PropTypes.object
}
GraphDoughnut.defaultProps = {
    height:300,
    data:{
        labels: ['', '', ''],
        datasets: [{
            data: ['30', '25', '20'],
            backgroundColor: ['#bac9d3', '#5a87a3', '#29658a'],
            
        }]

    }
}
export default GraphDoughnut;