import React from 'react';
import { Bar } from 'react-chartjs-2';
import PropTypes from 'prop-types';

const BarGraph = (props) => {
    return (
        <Bar
            data={props.data}
            height={props.height}
            legend={null}
            options={{
                maintainAspectRatio: false, 
                scales: {
                    yAxes: [{
                        ticks: { display: false },
                        gridLines: {
                            display: false,
                            drawBorder: true
                        }
                    }],
                    xAxes: [{
                        ticks: { display: false },
                        gridLines: {
                            display: false,
                            drawBorder: true
                        }
                    }]
                }
            }}
        />
    );
}

BarGraph.propTypes = {
    height:PropTypes.number,
    data:PropTypes.object
}
BarGraph.defaultProps = {
    height:300,
    data:{
        labels: ['', '', ''],
        datasets: [{
            data: ['30', '25', '20', '0'],
            backgroundColor: ['#bac9d3', '#5a87a3', '#29658a'],
        }]

    }
}
export default BarGraph;