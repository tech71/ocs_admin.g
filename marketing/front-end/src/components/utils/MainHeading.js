import React from 'react';
import PropTypes from 'prop-types';

const MainHeading = (props) => (
    <div className={"main_heading_cmn- mt-5 bor_top1" + " " + props.className}>
        <h1><span>{props.name}</span></h1>
    </div>
);


MainHeading.propTypes = {
    name: PropTypes.string,
    className: PropTypes.string   
}

MainHeading.defaultProps = {
    name: 'Main Heading'
}
export default MainHeading;