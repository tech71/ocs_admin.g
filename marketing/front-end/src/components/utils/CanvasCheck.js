import React, { Component } from 'react';

class CanvasCheck extends Component {
    constructor(props) {
        super(props);
        this.state = {

            data:[0,0, 100, 100],
            color:"#000",
            
        }
    }


    shapies = (e, ctx, canvas, val) => {

            var rect = e.target.getBoundingClientRect();
            var  x = e.clientX - rect.left;
            var y = e.clientY - rect.top;

            ctx.clearRect(0, 0, canvas.width, canvas.height); // for demo
            ctx.beginPath();
            ctx.rect(this.state.data[0], this.state.data[1], this.state.data[2], this.state.data[3]);    
            ctx.fillStyle = ctx.isPointInPath(x, y) ? this.state.hoverColor : this.state.color;
            ctx.fill();
            ctx.closePath();

            if (ctx.isPointInPath(x, y)) {
                this.tooltip(x, y, ctx, val);
            }
    }

    tooltip = (x, y, ctx) => {


        ctx.restore();
        ctx.globalCompositeOperation = 'source-over';
        // console.log(x, y, val)
        var measureWid = ctx.measureText(this.state.val).width;

        var xCordT = x;
        var yCordT = y;


        if (yCordT < 50) {
            yCordT = yCordT + 40;
        }
        if(xCordT < 20){
            xCordT = xCordT + 10;
        }

        ctx.beginPath();
        ctx.moveTo(xCordT, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT - 20);
        ctx.lineTo(xCordT + 20 + measureWid - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT);
        ctx.lineTo(xCordT - measureWid / 2 - 10, yCordT - 20);
        ctx.fillStyle = "#000";
        ctx.fill();
        ctx.closePath();

        ctx.font = "14px sans";
        ctx.fillStyle = "#fff";
        ctx.fillText(
            this.state.val, //value 
            xCordT + 10 - measureWid / 2 - 10, //x coord 
            yCordT - 5   //y coord 
        );
        
    }


    updateCanvas() {

        var canvas = this.refs.canvas;
        const ctx = canvas.getContext('2d');

        ctx.beginPath();
        ctx.fillStyle = this.state.color;
        ctx.fillRect(this.state.data[0], this.state.data[1], this.state.data[2], this.state.data[3]);    
        ctx.fill();
        ctx.closePath();

        canvas.onmousemove = (e) => {
            this.shapies(e, ctx, canvas);
            
        }

        canvas.onmouseleave = (e) => {
            this.shapies(e, ctx, canvas);
        }



    }

    componentWillMount(){
        var data = this.props.data;
        var color = this.props.color;
        var val = this.props.val;
        var hoverColor = this.props.hoverColor;
        this.setState({data:data, color : color, val:val, hoverColor:hoverColor},
            () => this.updateCanvas()
        );

        
    }
    render() {
        return (
            <canvas ref="canvas" width={300} height={300}/>
        );
    }
}

export default CanvasCheck;