import React, { Component } from 'react';
import { Map, Marker, GoogleApiWrapper, InfoWindow } from 'google-maps-react';
import { ROUTER_PATH } from '../../config';
import {MyCustomSvg} from './MyCustomSVG.js';

const MarkerColorMappingModuleWise = {
    participant : '#b175ff',
    fms : '#d32f2f',
    member : '#FF8967',
    org : '#1eb35a',
    shift : '#e07196',
}

const ifColorSameThenUse = {
    participant : '#6f42c1',
    fms : '#d32f2f',
    member : '#FF7045',
    org : '#1eb35a',
    shift : '#e0719654',
}



class GMap extends Component {


    constructor(props) {
        super(props);
        this.state = {
            numbers: null,
            mapLoaded: false
        };

    }

    onMarkerClick = (props, marker, e, numbers) => {
        // console.log(marker);
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true,
            numbers: numbers
        });
    }

    // handleMapIdle = () => {
    //     this.setState({
    //         mapLoaded: true
    //     });
    // }
    // onMarkerMounted = element => this.onMarkerClick(element.props, element.marker, element);

    render() {
        var bounds = new this.props.google.maps.LatLngBounds();
console.log(this.props.position, 'this.props.position');
        return (
            <Map
                google={this.props.google}
                zoom={5}
                center={{lat: -25.274399, lng: 133.7751}}
                initialCenter={{lat: -25.274399, lng: 133.7751}}
                streetViewControl={false}
                fullscreenControl={false}
                mapTypeControl={false}
            // onIdle={this.handleMapIdle}
            >



                {this.props.position.map((markers, i) => {

                    return (

                        markers.posArr.map((marks, key) => {
                            var f_module = this.props.MarkersModule.f_module;
                            var s_module = this.props.MarkersModule.s_module;
                            
                            var f_color = MarkerColorMappingModuleWise[f_module];
                            var s_color = (f_module == s_module)? ifColorSameThenUse[s_module] : MarkerColorMappingModuleWise[s_module];
                            
                            var special_marker = {f_count: marks.f_count, s_count: marks.s_count, f_color, s_color}
                            return (
                                <Marker
                                    key={key}
                                    position={{ lat: marks.lat, lng: marks.lng }}
                                    bounds={bounds}
                                    icon= {{
                                        anchor: new this.props.google.maps.Point(16, 16),
                                        url: 'data:image/svg+xml;utf-8,'+ encodeURIComponent(MyCustomSvg(special_marker)),
                                    }}
                               
                                   onClick={(props, marker, e) => this.onMarkerClick(props, marker, e, marks.numbers)}
                                // ref={this.onMarkerMounted}
                                />
                            )
                        })
                       
                    )
                })}

                <InfoWindow
                    onClose={this.onInfoWindowClose}
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}
                >
                    <div className="info_content">
                        {this.state.numbers}
                    </div>
                </InfoWindow>


                
            </Map>
        );
    }
}

GMap.defaultProps = {
    position: [
        {
            type: 'participant',
            posArr: [
                { lat: 22.7171903, lng: 75.87717980000002, numbers: '5' },
                { lat: 15.7171903, lng: 20.87717980000002, numbers: '2' }
            ]
        }
    ],
    MarkersModule: {},
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCAdbO5pXFwBbUCUeKcYZzufcQZ2xxXz0Y'
})(GMap)
