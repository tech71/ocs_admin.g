import React, { Component } from 'react';
import { connect } from 'react-redux';

class TotalCounts extends Component {
    render() {
        return (
            <React.Fragment>
                        <div className="cat_cols__">
                            <div className={'catBox_ '+  this.props.for}>
                                <h4>{this.props.title}</h4>
                                <h2>{this.props.count}</h2>
                            </div>
                        </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
   
})
const mapDispatchtoProps = (dispatch) => {
    return {
       
    }
}
export default connect(mapStateToProps, mapDispatchtoProps)(TotalCounts);