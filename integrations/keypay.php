<?php
require_once 'http_client.class.php';

const API_KEY = 'am9UK2NvRllwY1dOZGRTRU1VK3JKYWZxbEh4VFBrYVhiNWlFdmFUSzYvYXdyOXVKdy8zWkRPTnJOblFEVXVWVQ';
const BUSINESS_ID = 88006;

// so PHP_EOL is formatted as a newline
header('Content-Type: text/plain');

$http = new HttpClient;
$http->set_curlopt(CURLOPT_RETURNTRANSFER, true);
$http->set_header('Accept', 'application/json');

echo 'KeyPay API Integration' . PHP_EOL;

echo PHP_EOL;
echo '1. Authentication' . PHP_EOL;
$http->set_curlopt(CURLOPT_HTTPAUTH, CURLAUTH_ANY);
$http->set_curlopt(CURLOPT_USERPWD, API_KEY . ':');
$response = $http->get('https://keypay.yourpayroll.com.au/api/v2/user');

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

echo PHP_EOL;
echo '2. Push Payrates' . PHP_EOL;
$response = $this->put('https://keypay.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/payratetemplate/{id}', [
    "externalId" => "string",
    "id" => "int32",
    "maximumQuarterlySuperContributionsBase" => "double",
    "name" => "string",
    "payCategories" => [
        [
            "calculatedRate" => "double",
            "payCategoryId" => "int32",
            "standardWeeklyHours" => "double",
            "superRate" => "double",
            "userSuppliedRate" => "double"
        ]
    ],
    "primaryPayCategoryId" => "int32",
    "reapplyToLinkedEmployees" => "boolean",
    "source" => "string",
    "superThresholdAmount" => "double"
]);

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

echo PHP_EOL;
echo '3. Create Employee' . PHP_EOL;
$response = $this->post('https://keypay.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/employee/unstructured', [
    "afsDebt" => "string",
    "anniversaryDate" => "date-time",
    "australianResident" => "string",
    "automaticallyPayEmployee" => "string",
    "bankAccount1_AccountName" => "string",
    "bankAccount1_AccountNumber" => "string",
    "bankAccount1_AllocatedPercentage" => "double",
    "bankAccount1_BSB" => "string",
    "bankAccount1_FixedAmount" => "double",
    "bankAccount2_AccountName" => "string",
    "bankAccount2_AccountNumber" => "string",
    "bankAccount2_AllocatedPercentage" => "double",
    "bankAccount2_BSB" => "string",
    "bankAccount2_FixedAmount" => "double",
    "bankAccount3_AccountName" => "string",
    "bankAccount3_AccountNumber" => "string",
    "bankAccount3_AllocatedPercentage" => "double",
    "bankAccount3_BSB" => "string",
    "bankAccount3_FixedAmount" => "double",
    "claimTaxFreeThreshold" => "string",
    "dateCreated" => "date-time",
    "dateOfBirth" => "date-time",
    "dateTaxFileDeclarationReported" => "date-time",
    "dateTaxFileDeclarationSigned" => "date-time",
    "emailAddress" => "string",
    "emergencyContact1_Address" => "string",
    "emergencyContact1_AlternateContactNumber" => "string",
    "emergencyContact1_ContactNumber" => "string",
    "emergencyContact1_Name" => "string",
    "emergencyContact1_Relationship" => "string",
    "emergencyContact2_Address" => "string",
    "emergencyContact2_AlternateContactNumber" => "string",
    "emergencyContact2_ContactNumber" => "string",
    "emergencyContact2_Name" => "string",
    "emergencyContact2_Relationship" => "string",
    "employingEntityABN" => "string",
    "employmentAgreement" => "string",
    "employmentType" => "string",
    "endDate" => "date-time",
    "externalId" => "string",
    "firstName" => "string",
    "gender" => "string",
    "hasApprovedWorkingHolidayVisa" => "string",
    "hasWithholdingVariation" => "string",
    "helpDebt" => "string",
    "homePhone" => "string",
    "hoursPerWeek" => "double",
    "id" => "int32",
    "isEnabledForTimesheets" => "string",
    "isExemptFromFloodLevy" => "string",
    "isExemptFromPayrollTax" => "string",
    "jobTitle" => "string",
    "leaveAccrualStartDateType" => "string",
    "leaveTemplate" => "string",
    "leaveYearStart" => "date-time",
    "locations" => "string",
    "maximumQuarterlySuperContributionsBase" => "double",
    "medicareLevyExemption" => "string",
    "middleName" => "string",
    "mobilePhone" => "string",
    "otherTaxOffset" => "string",
    "overrideTemplateRate" => "string",
    "payConditionRuleSet" => "string",
    "payRateTemplate" => "string",
    "paySchedule" => "string",
    "paySlipNotificationType" => "string",
    "postalAddressLine2" => "string",
    "postalPostCode" => "string",
    "postalState" => "string",
    "postalStreetAddress" => "string",
    "postalSuburb" => "string",
    "preferredName" => "string",
    "previousSurname" => "string",
    "primaryLocation" => "string",
    "primaryPayCategory" => "string",
    "rate" => "double",
    "rateUnit" => "string",
    "residentialAddressLine2" => "string",
    "residentialPostCode" => "string",
    "residentialState" => "string",
    "residentialStreetAddress" => "string",
    "residentialSuburb" => "string",
    "rosteringNotificationChoices" => "string",
    "seniorsTaxOffset" => "string",
    "startDate" => "date-time",
    "status" => "string",
    "superFund1_AllocatedPercentage" => "double",
    "superFund1_EmployerNominatedFund" => "boolean",
    "superFund1_FixedAmount" => "double",
    "superFund1_FundName" => "string",
    "superFund1_MemberNumber" => "string",
    "superFund1_ProductCode" => "string",
    "superFund2_AllocatedPercentage" => "double",
    "superFund2_EmployerNominatedFund" => "boolean",
    "superFund2_FixedAmount" => "double",
    "superFund2_FundName" => "string",
    "superFund2_MemberNumber" => "string",
    "superFund2_ProductCode" => "string",
    "superFund3_AllocatedPercentage" => "double",
    "superFund3_EmployerNominatedFund" => "boolean",
    "superFund3_FixedAmount" => "double",
    "superFund3_FundName" => "string",
    "superFund3_MemberNumber" => "string",
    "superFund3_ProductCode" => "string",
    "superThresholdAmount" => "double",
    "surname" => "string",
    "tags" => "string",
    "taxFileNumber" => "string",
    "taxVariation" => "string",
    "title" => "string",
    "workPhone" => "string",
    "workTypes" => "string"
]);

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

echo PHP_EOL;
echo '4. Report Shift Completed' . PHP_EOL;
$response = $http->post('https://keypay.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/kiosk/{kioskId}/addshift', [
    "breaks" => [
        [
            "employeeId" => "int32",
            "end" => [
                "employeeId" => "int32",
                "image" => "byte",
                "ipAddress" => "string",
                "isAdminInitiated" => "boolean",
                "kioskId" => "int32",
                "latitude" => "double",
                "longitude" => "double",
                "noteVisibility" => "string",
                "recordedTimeUtc" => "date-time",
                "utcOffset" => "string"
            ],
            "image" => "byte",
            "ipAddress" => "string",
            "isAdminInitiated" => "boolean",
            "kioskId" => "int32",
            "latitude" => "double",
            "longitude" => "double",
            "noteVisibility" => "string",
            "recordedTimeUtc" => "date-time",
            "start" => [
                "employeeId" => "int32",
                "image" => "byte",
                "ipAddress" => "string",
                "isAdminInitiated" => "boolean",
                "kioskId" => "int32",
                "latitude" => "double",
                "longitude" => "double",
                "noteVisibility" => "string",
                "recordedTimeUtc" => "date-time",
                "utcOffset" => "string"
            ],
            "utcOffset" => "string"
        ]
    ],
    "classificationId" => "int32",
    "employeeId" => "int32",
    "image" => "byte",
    "ipAddress" => "string",
    "isAdminInitiated" => "boolean",
    "kioskId" => "int32",
    "latitude" => "double",
    "locationId" => "int32",
    "longitude" => "double",
    "note" => "string",
    "noteVisibility" => "string",
    "recordedEndTimeUtc" => "date-time",
    "recordedStartTimeUtc" => "date-time",
    "recordedTimeUtc" => "date-time",
    "shiftConditionIds" => [
        "int32"
    ],
    "utcOffset" => "string",
    "workTypeId" => "int32"
]);

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

echo PHP_EOL;
echo '5. Pull Final Member Payments/Super/etc' . PHP_EOL;
echo '- Get Pay Runs' . PHP_EOL;
$response = $http->get('https://keypay.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/payrun');

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

// here we use the $response to figure out what payrun we're looking at
// and query pay slip data based on it
$response = $http->get('https://keypay.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/payrun/{payRunId}/payslips/{employeeId}');

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}

echo PHP_EOL;
echo '6. Generate Report' . PHP_EOL;
// as mentioned in the documentation, requirements are unclear for reporting - code below is just an example
$response = $http->get('https://api.yourpayroll.com.au/api/v2/business/' . BUSINESS_ID . '/report/paycategories');

if ($response === false) {
    var_dump($http->error());
    die;
} else {
    var_dump($response);
}
