export declare namespace HCM {

    interface Link {
        href: string
        templated?: boolean
        name?: string
        title?: string
    }

    type Linkable<T extends string = null> = { _links: Record<T | "self", Link> }

    type Embedded = { _embedded: { [key: string]: Resource<{[key: string]: any}>[] } }
    type Resource<T, U extends string = null> = T & Linkable<U> & Embedded
    type ResourceCollection<T, U = {}> = U & Resource<{
        data?: Resource<T>[],
        title?: string,
        description?: string
    }>


    interface DashboardIndexResponse extends Linkable {
        latest_updates: ResourceCollection<{update: string, date: string}>
        invoices:  ResourceCollection<{invoiceName: string, dueDate: string}>
        fms_feedback: Resource<{feedback: string}>
    }

    namespace SubOrgsIndexResponse {
        interface DataItem {
            OrganisationEmail: { phone: string, id: number, primary_phone: '0' | '1' }[]
            OrganisationPh: { email: string, id: number, primary_email: '0' | '1' }[]
            address_id: number
            all_contact_id: number
            city: { value: string, label: string }
            email_id: number
            key_contact: string
            key_contact_label: string
            name: string
            ocs_id: number
            ph_id: number
            position: string
            postal: string | number
            primary_address: string
            primary_email: string
            primary_phone: string
            state: number
            status: '0' | '1'
            street: string
        }
    }

    interface SubOrgsIndexResponse {
        status: boolean
        data: SubOrgsIndexResponse.DataItem[]
        count?: number | string
    }

}
