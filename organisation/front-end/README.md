# ORG PORTAL module

Maintained by:
- Joshua Orozco <joshua@ydt.com.au>


# Setup
You must run the back-end api server in cmd/powershell and make it _open_ to your network. Then install and run the front-end application
```PowerShell
# Run the backend api server to 0.0.0.0:8000. 
# Note: Do not use localhost - if you do, ajax requests on front-end will not work!
cd C:\xampp\htdocs\ocm_admin\organisation\back-end
php -S 0.0.0.0:8000

# Install npm dependencies and run the React app
# Note: AJAX requests to port 3000 will be delivered (aka proxied) to port 8000
cd C:\xampp\htdocs\ocm_admin\organisation\front-end
npm install
npm start
```
