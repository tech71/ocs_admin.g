import React from 'react';
import { history } from './history';
import {
    Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";

import PrivateRoute from './components/PrivateRoute';

import LoginPage from './pages/LoginPage';
import TempPasswordResetPage from './pages/TempPasswordResetPage';
import DashboardPage from './pages/DashboardPage';

import ForgotPasswordPage from './pages/ForgotPasswordPage';
import { userService } from './services/userService';
import LogoutPage from './pages/LogoutPage';
import PasswordResetPage from './pages/PasswordResetPage';

import SitesCreatePage from './pages/sites/CreatePage';
import SitesIndexPage from './pages/sites/IndexPage';
import SiteDetailsPage from './pages/sites/DetailsPage';

import InvoicesIndexPage from './pages/invoices/IndexPage';
import SubOrgsCreatePage from './pages/sub-orgs/CreatePage';

import './stylesheets/App.scss';
import ParentOrgReviewPage from './pages/ParentOrgReviewPage';
import SubOrgsDetailsPage from './pages/sub-orgs/DetailsPage';
import SubOrgsIndexPage from './pages/sub-orgs/IndexPage';



class App extends React.Component {
    

    render() {
        return (
            <>
                {/* TODO: Can we use BrowserRouter for this? */}
                <Router history={history}>
                    <Switch>
                        <PrivateRoute exact path='/' component={DashboardPage} />
						<PrivateRoute exact path='/sites' component={SitesIndexPage} />
                        <PrivateRoute path='/sites/create' component={SitesCreatePage} />
						<PrivateRoute path='/sites/detail/:id' component={SiteDetailsPage} />
                        <PrivateRoute path='/invoices' component={InvoicesIndexPage} />
                        <PrivateRoute path='/sub-orgs/create' component={SubOrgsCreatePage} />
                        <PrivateRoute path='/sub-orgs/:id' component={SubOrgsDetailsPage} />
                        <PrivateRoute path='/sub-orgs' component={SubOrgsIndexPage} />
                        <Route path='/forgot-password' component={ForgotPasswordPage} />
                        <Route path='/password-reset' component={PasswordResetPage} />
                        <Route path='/temp-password-reset' component={TempPasswordResetPage}/>
                        <Route path='/login' render={props => {
                            return userService.isLoggedIn()
                                ? <Redirect to={{pathname: '/', state: {from: props.location}}} />
                                : <LoginPage {...props} />
                        }} />
                        <Route path='/logout' component={LogoutPage} />
                        <PrivateRoute path='/org-review' component={ParentOrgReviewPage} />
                    </Switch>
                </Router>
            </>
        )
    }
}

export default App;
