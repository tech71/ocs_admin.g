export const LOCALSTORAGE_KEY = 'jwt';
export const LOGIN_URL = '/login';
export const AUTH_ENDPOINT = "/account/login";