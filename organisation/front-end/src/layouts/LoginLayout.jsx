import React from "react"
import LoginHeader from "../components/LoginHeader"
import LoginFooter from "../components/LoginFooter"
import { Alert } from "react-bootstrap"
import { connect } from "react-redux"

/**
 * @typedef {{className: string}} Props
 * 
 * 
 * @extends {React.Component<Props, null>}
 */
class LoginLayout extends React.Component {
    static defaultProps = {
        className: '',
    }

    handleOnClose = () => {
        const { dispatch } = this.props;
        dispatch({ type: "ALERTS_DISMISS_ALL" })
    }


    /** @param {object} alert */
    renderAlert(alert) {
        /** @type {React.CSSProperties} */
        const alertStyle = {
            position: "fixed",
            top: 60,
            left: "50%",
            width: "100%",
            maxWidth: 320,
            marginLeft: -(320*0.5),
        }

        const variant = alert.variant || "default";


        return (
            <Alert variant={variant} style={alertStyle} onClose={this.handleOnClose} dismissible>
                <div className="text-center">{alert.message}</div>
            </Alert>
        )
    }


    render() {
        const {className, children, alerts} = this.props
        
        return (
            <>
                <div className={`site-wrapper page ${className}`}>
                    <LoginHeader />
                    <div className="site-content">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    {children}
                                </div>
                            </div>
                        </div>
                    </div>
                    <LoginFooter />
                </div>
                {
                    alerts && alerts.map((alert, i) => (
                        <React.Fragment key={i}>
                            {this.renderAlert(alert)}
                        </React.Fragment>
                    ))
                }
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        alerts: state.alerts.items,
    }
}

export default connect(mapStateToProps, null)(LoginLayout)