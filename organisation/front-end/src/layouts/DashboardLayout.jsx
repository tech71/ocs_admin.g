import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import Logo from '../images/placeholder-logo-200x100.png'
import AlertIcon from '../images/Alert.png'
import InformationIcon from '../images/Information.png'
import MessagesIcon from '../images/Messages.png'
import SettingsIcon from '../images/Settings.png'
import "../stylesheets/layouts/DashboardLayout.scss";
import { Alert } from 'react-bootstrap'
import jwt from "jsonwebtoken"

/**
 * @typedef {object} StandardJWTClaimSet
 * @property {string|null} iss
 * @property {string|null} aud
 * @property {string|null} jti
 * @property {string|null} iat
 * @property {int|null} bnf
 * @property {int|null} exp
 * 
 * 
 * @typedef {object} OrganisationClaimSet
 * @property {string|null} id
 * @property {string|null} companyId
 * @property {string|null} name
 * @property {string|null} abn
 * @property {string|null} logo_file
 * @property {string|int|null} parent_org
 * @property {string|null} website
 * @property {'0'|'1'|null} payroll_tax
 * @property {'0'|'1'|null} gst
 * @property {'0'|'1'|null} enabled_portal_access
 * @property {'0'|'1'|null} status
 * @property {string|null} booking_date
 * @property {'0'|'1'|null} archive
 * @property {string|null} username
 * @property {'0'|'1'|null} is_first_time_login
 * 
 * @typedef {{className: string, claims?: (StandardJWTClaimSet & OrganisationClaimSet) | null}} Props
 * @typedef {{ href: string, name?: string, description?: string, title?: string, is_active?: boolean }} SidebarLink
 * @typedef {SidebarLink & {_links?: Record<string, SidebarLink>}} Menu
 * 
 * @extends {React.Component<Props, null>}
 */
class DashboardLayout extends React.Component {

    static defaultProps = {
        className: ""
    }

        /** @param {object} alert */
        renderAlert(alert) {
            /** @type {React.CSSProperties} */
            const alertStyle = {
                position: "fixed",
                top: 60,
                left: "50%",
                width: "100%",
                maxWidth: 320,
                marginLeft: -(320*0.5),
            }
    
            const variant = alert.variant || "default";
    
    
            return (
                <Alert variant={variant} style={alertStyle} onClose={this.handleOnClose} dismissible>
                    <div className="text-center">{alert.message}</div>
                </Alert>
            )
        }


    createSidebarUrls() {
        const { claims } = this.props;
        const organisationName = claims && claims.name ? claims.name : "- na -"

        
        // Trying to follow HAL specification
        // http://stateless.co/hal_specification.html
        /** @type {Menu[]} */
        const sidebarMenus = [
            {
                name : "dashboard",
                is_active: true,
                title: "Dashboard",
                description: "Dashboard page",
                href: "/",
                _links: {
                    "your_organisation": { href: "/org-review", title: "Your Organisation" },
                    "invoice_tracker": {href: "/invoices", title: "Invoice Tracker" },
                    "fms": {href: "/", title: "FMS" },
                    "request_staff": { href: "/", title: "Request staff"}
                }
            },
            {
                name : "suborgs",
                title: "Sub-orgs",
                description: "Suborganisation",
                href: "/sub-orgs",
                _links: {}
            },
            {
                name : "organisation",
                title: organisationName,
                description: organisationName,
                href: "/",
                _links: {
                    "your_organisation": { href: "/", title: "Your Sub-Org" },
                    "invoice_tracker": {href: "/", title: "Invoice Tracker" },
                    "fms": {href: "/", title: "FMS" },
                    "request_staff": { href: "/", title: "Request staff"}
                }
            },
            {
                name: "houses",
                title: "Houses",
                href: "/",
            },
            {
                name: "sites",
                title: "Sites",
                href: "/sites",
            }
        ]

        return sidebarMenus
    }

    /** 
     * @param {Menu[]} sidebarMenus 
     */
    renderSidebarMenus(sidebarMenus) {
        return (
            <ul>
                {
                    sidebarMenus.map((sidebarMenu, i) => (
                        <li key={i}>
                            {sidebarMenu.href && (
                                <Link to={sidebarMenu.href} title={sidebarMenu.description || undefined} className={sidebarMenu.is_active && 'active'}>
                                    {sidebarMenu.title}
                                </Link>
                            )}
                            {(sidebarMenu._links && Object.keys(sidebarMenu._links).length > 0) && (
                                <ul>
                                    {
                                        Object.keys(sidebarMenu._links).map((key, i) => {
                                            const menu = sidebarMenu._links[key];
                                            return (
                                                <li key={i}>
                                                    <Link to={menu.href} title={menu.description || undefined} className={menu.is_active && 'active'}>
                                                        {menu.title}
                                                    </Link>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            )}
                        </li>
                    ))
                }
            </ul>
        )
    }

    renderSidebarMenusOld() {
        return (
            <ul>
                <li>
                    <Link to="/" className="active">Dashboard</Link>
                    <ul>
                        <li><Link to="/">Your Organisation</Link></li>
                        <li><Link to="/">Invoice Tracker</Link></li>
                        <li><Link to="/">FMS</Link></li>
                        <li><Link to="/">Request Staff</Link></li>
                    </ul>
                </li>
                <li><Link to="/">Sub-orgs</Link></li>
                <li>
                    <Link to="/">Yooralla - Altona East</Link>
                    <ul>
                        <li><Link to="/">Your Sub-Org</Link></li>
                        <li><Link to="/">Invoice Tracker</Link></li>
                        <li><Link to="/">FMS</Link></li>
                        <li><Link to="/">Request Staff</Link></li>
                    </ul>
                </li>
                <li><Link to="/">Houses</Link></li>
                <li><Link to="/">Sites</Link></li>
            </ul>
        )
    }



    render() {
        const {className, children, alerts, claims} = this.props
        const siteHeading = claims && claims.name ? claims.name : "- na -"
        const sidebarMenus = this.createSidebarUrls()

        return (
            <>
            <div className={`layout-dashboard site-wrapper page ${className}`}>
                <aside className="sidebar sidebar-left">
                    <header className="sidebar-header bg-primary d-flex">
                        <div className="align-self-center w-100 text-center">
                            <img src={Logo} alt="Organisation logo"/>
                        </div>
                    </header>
                    <nav className="sidebar-navigation">
                        {this.renderSidebarMenus(sidebarMenus)}
                        {/* {this.renderSidebarMenusOld()} */}
                    </nav>
                    <footer className="sidebar-footer bg-primary">
                        <ul className="sidebar-footer-links">
                            <li><Link to="/">Privacy policy</Link></li>
                            <li><Link to="/">About Healthcare Manager</Link></li>
                            <li><Link to="/">Staff policies</Link></li>
                            <li><Link to="/">Terms &amp; conditions</Link></li>
                        </ul>
                        <div className="copyright text-light">
                            &copy; Healthcare Manager 2019 - All Rights Reserved
                        </div>
                    </footer>
                </aside>
                <div className="site-body">
                    <header className="site-header bg-primary text-light">
                        <div className="container-fluid d-flex align-items-center h-100 w-100 site-header-inner">
                            <div className="w-100">
                                <div className="nav-icons clearfix">
                                    <nav className="d-inline-block">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation" tabIndex="-1" title="ArrowBack" width="33">
                                                        <path d="
                                                        M20 11
                                                        H7.83
                                                        l5.59-5.59
                                                        L12 4
                                                        l-8 8 8 8 1.41-1.41
                                                        L7.83 13
                                                        H20v-2z" fill="white" stroke="transparent"></path>
                                                    </svg>
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                    <nav className="d-inline-block text-right float-right">
                                        <ul>
                                            <li>
                                                <Link to="/">
                                                    <img src={MessagesIcon} alt="Icons"/>
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    <img src={AlertIcon} alt="Icons"/>
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    <img src={SettingsIcon} alt="Icons"/>
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/">
                                                    <img src={InformationIcon} alt="Icons"/>
                                                </Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div className="site-heading-parent w-100">
                                    <h1 className="site-heading">{siteHeading}</h1>
                                </div>
                            </div>
                        </div>

                    </header>
                    <main className="site-content">
                        <div className="site-content-inner">
                            {children}
                        </div>
                    </main>
                    <footer className="site-footer">

                    </footer>
                </div>
            </div>
            {
                alerts && alerts.map((alert, i) => (
                    <React.Fragment key={i}>
                        {this.renderAlert(alert)}
                    </React.Fragment>
                ))
            }
            </>
        )
    }
}

const mapStateToProps = state => {

    const {token} = state.auth.user
    let decoded = null;
    if (token) {
        decoded = jwt.decode(token)
    }

    return {
        alerts: state.alerts.items,
        claims: decoded
    }
}


export default connect(mapStateToProps, null)(DashboardLayout)