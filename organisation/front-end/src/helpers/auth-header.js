import { LOCALSTORAGE_KEY } from "../constants";

export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}