import React from 'react'
import DashboardLayout from '../../layouts/DashboardLayout'
import { connect } from 'react-redux'
import OrgInfoSummary from '../../components/OrgInfoSummary'
import BasicDisplayForm from '../../components/BasicDisplayFormComponent';
import ContactDisplayForm from '../../components/ContactDisplayComponent';
import { Breadcrumb } from 'react-bootstrap'
import { axios } from '../../axios'
import jwt from "jsonwebtoken"

import "../../stylesheets/pages/ParentOrgReviewPage.scss";

/**
 * Renders page to display and search list of sites
 * 
 * @extends {React.Component<Props, State>}
 */
class IndexPage extends React.Component {

	/** @type {State} */
	state = {
		orgDetail: null,
		siteDetails: null,
		siteContacts: null,
		sites: [],
	}

	/**
	 * Fetch org details and sites when this page has loaded
	 */
	async componentDidMount() {
		this.setState({ loadingSites: true })
		const getOrgDetailsResponse = await axios.post("/ParentOrgController/getOrgDetails");
		const {data: getOrgDetailsResponseData} = getOrgDetailsResponse.data;
		const getSiteResponse = await axios.get("/site/get_site_details?id=" + this.props.match.params.id);
		// this.fetchOrgDetails();
		const { data: getSiteResponseData } = getSiteResponse.data;
		console.log(getSiteResponseData);

		this.setState({
			orgDetail: getOrgDetailsResponseData,
			siteDetails: getSiteResponseData,
			siteContacts: getSiteResponseData.contact_detail,
			loadingSites: false,
		})
	}
	
	/**
	 * Render org details
	 */
	renderOrgSummary() {
		const { orgDetail } = this.state

		return (
			<div className="site-information">
				<OrgInfoSummary orgDetail={orgDetail} />
			</div>
		)
	}

	/**
	 * Render breadcrumbs
	 */
	renderBreadcrumbs() {
		const { organisationName } = this.props;

		return (
			<Breadcrumb>
				<Breadcrumb.Item>
					{organisationName}
				</Breadcrumb.Item>
			</Breadcrumb>
		)
	}

	updateContactButtonHandler = (siteContact) => {
		console.log(siteContact);
	}

	toggleContactDisplay = (toggledOrgContact) => {
		const siteContacts = this.state.siteContacts;
		const newSiteContacts = siteContacts.map((orgContact, i) => {
			if (orgContact === toggledOrgContact) {
				return {
					...orgContact,
					expanded: !orgContact.expanded
				}
			}

			return orgContact;
		})

		this.setState({
			siteContacts: newSiteContacts
		})
    }

	render() {

		const orgDetailLabelAndColumnMapping = {
            Name: 'name', Address: 'primary_address',
            'Office Phone': 'site_phones', 
            'Office Email': 'site_emails',
            'ABN NO': 'abn', 'Payroll Tax': 'payroll_tax', GST: 'gst',
            
		};

		const orgContactLabelAndColumnMapping = {
            'First Name': 'firstname', 'Last Name': 'lastname',
            Position: 'position', email: 'emails',
            Phone: 'phones'
        };

		var contactComponents = [];

		//Build the contact card components
		if(this.state.siteContacts) {
			for(let i = 0; i < this.state.siteContacts.length; i++) {
				contactComponents.push(
					<div className="parent-org-details org-contacts" key={`${i}-${this.state.siteContacts[i].email}`}>
						<ContactDisplayForm expanded={this.state.siteContacts[i].expanded}
							toggleContactDisplay={this.toggleContactDisplay}
							labelAndColumnMapping={orgContactLabelAndColumnMapping}
							updateButtonHandler={this.updateContactButtonHandler} 
							organizationContact={this.state.siteContacts[i]}>
						</ContactDisplayForm>
					</div>
				);
			}
		}
		
		return (
			<DashboardLayout className="page-sub-orgs-index">
				<div className="container-fluid">
					<div className="content-block">
						{this.renderBreadcrumbs()}
					</div>
					<div className="content-block">
						{this.renderOrgSummary()}
					</div>
					<div className="entity-details">
						<BasicDisplayForm header="Site - Details" button_title="Update site details" 
						labelAndColumnMapping={orgDetailLabelAndColumnMapping}
						organizationDetail={this.state.siteDetails}
						showModal={this.displayModalHandler}>
						</BasicDisplayForm>
					</div>
					<div className="entry-header entity-contact-header">
					<h2><b>Site - Contacts</b></h2>
					</div>

					<div className="content-block">
					{contactComponents}
					</div>
				</div>
			</DashboardLayout>
		)
	}
}

const mapStateToProps = state => {
	const {token} = state.auth.user

	let claims = null;
	if (token) {
		claims = jwt.decode(token)
	}

	return {
		organisationName: (claims || {})['name']
	}
}

export default connect(mapStateToProps)(IndexPage);