import React from 'react'
import { connect } from 'react-redux'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import SimpleReactValidator from 'simple-react-validator';
import jwt from "jsonwebtoken"
import objectToFormData from "object-to-formdata";
import GooglePlacesAutocomplete from 'react-google-autocomplete'
import { Breadcrumb } from 'react-bootstrap'

import DashboardLayout from '../../layouts/DashboardLayout'
import { CustomSelect } from "../../components/CustomSelect"
import CustomCheckbox from '../../components/CustomCheckbox';
import { AddPrimarySecondaryContact } from '../../components/AddPrimarySecondaryContact'
import { findAddressComponent } from '../../helpers/google-places'
import OrgInfoSummary from '../../components/OrgInfoSummary'
import { axios } from '../../axios'
import {history} from "../../history"

import 'react-select-plus/dist/react-select-plus.css';
import "../../stylesheets/pages/sites/CreatePage.scss"

/** 
 * Adds red star to labels
 * 
 * @param {React.DetailedHTMLProps<React.LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>} props 
 */
const RequiredLabel = (props) => (
    <label htmlFor={props.htmlFor}>{props.children} <span className="required">*</span></label>
)

class CreatePage extends React.Component {
	
	state = {
		_debug: false,
		_loading: false,
		_loadingRequirements: false,
		_displayAttachToParentOrgModal: false,
		_displayAddSitesModal: false,
		states: [
			{value: "", label: "Loading...", selected: true, disabled: true},
		],
		org_requirements: [],
		selectedLogo: null,
		orgDetail: null,
	}

	constructor(props) {
		super(props);

		this.validator = new SimpleReactValidator({
			validators: {
				phone_min: {
					message: "Must be atleast 8 digits",
					rule: (val, params, validator) => {
						return val.length >= 8
					},
				},
				abn_min: {
					message: "Must be exactly 11 digits",
					rule: (val, params, validator) => {
						return val.length == 11
					},
				}
			},
			messages: {
				required: 'This field is required',
				phone: "Not a valid phone number",
				email: "Not a valid email address",
				min: "Must be atleast 8 digits",
			},
			element: (message, className) => {
				return (
					<div className={`tooltip fade top in`} role="tooltip" style={{transform: "translateY(-100%)"}}>
						<div className="tooltip-arrow" style={{left: "50%"}}></div>
						<div className="tooltip-inner">
							{message}
						</div>
					</div>
				)
			}
		})
	}

	/**
	 * Fetch states, and org details when this page (or component) is loaded
	 */
	async componentDidMount() {
		this.setState({ _loading: true, _loadingRequirements: true })
		const getOrgDetailsResponse = await axios.post("/ParentOrgController/getOrgDetails")
		const {data: getOrgDetailsResponseData} = getOrgDetailsResponse.data

		const getStateResponse = await axios.get("/common/get_state");
		const { data: getStateResponseData } = getStateResponse.data
		
		this.setState({
			_loading: false,
			_loadingRequirements: false,
			orgDetail: getOrgDetailsResponseData,
			states: getStateResponseData
		})
	}

	/**
     * Displays customized validation error message
     * 
     * @param {string} errorMessage
     */
    renderValidationError = errorMessage => {
        return (
            <span className="text-danger">
                <small>{errorMessage}</small>
            </span>
        )
    }


    /** 
     * Runs when the form has been submitted.
     * Will call `POST: suborgs/create` (Note: endpoint does not have dash)
     * 
     * @type {(values: any, formikHelpers: import('formik').FormikHelpers<any>) => void)} 
     */
    handleOnSubmit = async (values, formikHelpers) => {

		// do not include empty secondary phones and emails in submission 
		const newValues = {
			...values,
			SiteDetails: {
				...values.SiteDetails,
				phones: values.SiteDetails.phones.filter(p => p.is_primary || (!p.is_primary && p.value)),
				emails: values.SiteDetails.emails.filter(e => e.is_primary || (!e.is_primary && e.value))
			},
			KeyContactDetails: {
				...values.KeyContactDetails,
				phones: values.KeyContactDetails.phones.filter(p => p.is_primary || (!p.is_primary && p.value)),
				emails: values.KeyContactDetails.emails.filter(e => e.is_primary || (!e.is_primary && e.value))
			},
			BillingContactDetails: {
				...values.BillingContactDetails,
				phones: values.BillingContactDetails.phones.filter(p => p.is_primary || (!p.is_primary && p.value)),
				emails: values.BillingContactDetails.emails.filter(e => e.is_primary || (!e.is_primary && e.value))
			},
		}

		formikHelpers.setSubmitting(true);

		if (!this.validator.allValid()) {
			this.validator.showMessages();
			this.forceUpdate()
			return;
		}

		// server side validation
		try {
			const response = await axios.post("/site/create", objectToFormData({data:JSON.stringify(newValues)}));
			if (response.data) {
				let destination_url = response.data['destination_url'] || '/';
				history.push(destination_url);
			}
		} catch(e) {
			const {errors} = e.response.data;

			for (let field in errors) {
				formikHelpers.setFieldError(field, errors[field])
			}
		}
	}

	/**
	 * Render breadcrumbs
	 */
	renderBreadcrumbs() {
		const { organisationName } = this.props;

		return (
			<Breadcrumb>
				<Breadcrumb.Item>
					{organisationName}
				</Breadcrumb.Item>
			</Breadcrumb>
		)
	}

	/**
	 * Renders organisation information
	 */
	renderOrgSummary() {
		const { orgDetail } = this.state
		return (
			<div className="site-information">
				<OrgInfoSummary orgDetail={orgDetail}/>
			</div>
		)
	}

	renderForm() {
		const { states } = this.state

		// NOTE: all validation happens on server-side
		return (
			<Formik
				initialValues={{
					SiteDetails: {
						title: '',
						phones: [
							{ value: "", is_primary: true, required: true }
						],
						emails: [
							{ value: "", is_primary: true, required: true }
						],
						abn: '',
						site_address: '',
						city: '', // aka suburb
						state: '',
						postcode: ''
					},
					KeyContactDetails: {
						first_name: '',
						last_name: '',
						position: '',
						department: '',
						phones: [
							{ value: "", is_primary: true, required: true }
						],
						emails: [
							{ value: "", is_primary: true, required: true }
						],
						is_billing_same_as_key_contact: false
					},
					BillingContactDetails: {
						first_name: '',
						last_name: '',
						position: '',
						department: '',
						phones: [
							{ value: "", is_primary: true, required: true }
						],
						emails: [
							{ value: "", is_primary: true, required: true }
						],
						pay_roll_tax: '',
						gst: '',
					}
				}}
				validateOnBlur={false}
				validateOnChange={false}
				onSubmit={this.handleOnSubmit}
				enableReinitialize={true}
				>
				{({values, errors, handleChange, handleSubmit, setFieldValue, isSubmitting, initialValues}) => {
				return (
					<Form encType="multipart/form-data" method="POST" id="addsite">
						<div className="entry-header">
							<h2><b>Create New Site</b></h2>
						</div>

						<div className="row mt-5 mx-0">
							<h3 className="color">Site Details</h3>
						</div>

						<div className="row" >
							<div className="col-md-6">
								<RequiredLabel htmlFor="title">Title: </RequiredLabel>
								{this.validator.message("SiteDetails[title]", values.SiteDetails.title, 'required')}
								<Field name="SiteDetails[title]" id="SiteDetails-title" className="form-control border-dark" required/>
								<ErrorMessage name="SiteDetails[title]" render={this.renderValidationError}/>
							</div>

							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									required placeholder="Can Include Area Code" 
									data-rule-notequaltogroup='[".distinctPh"]' 
									data-rule-phonenumber 
									data-msg-notequaltogroup="Please enter a unique contact number" 
									errors={errors}
									label='Phone'
									values={values.SiteDetails.phones}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="SiteDetails-phones"
									namePrefix="SiteDetails[phones]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|phone|phone_min')
										}

										return this.validator.message(name, value, 'phone', {
											messages: {
												phone: "Not a valid secondary phone"
											}
										})
										
									}}
								/>
							</div>

							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									errors={errors}
									label='Email'
									values={values.SiteDetails.emails}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="SiteDetails-emails"
									namePrefix="SiteDetails[emails]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|email')
										}
										
										return this.validator.message(name, value, 'email', {
											messages: {
												email: "Not a valid secondary email"
											}
										})
									}}
								/>
							</div>
						</div>

						<div className="row mt-3">
							<div className="col-12 col-md-4">
								<RequiredLabel htmlFor="SiteDetails-site_address">Site Address:</RequiredLabel>
								{this.validator.message('SiteDetails[site_address]', values.SiteDetails.site_address, 'required')}
								<GooglePlacesAutocomplete required 
									name="SiteDetails[site_address]"
									id="SiteDetails-site_address" 
									className="form-control border-dark"
									value={values.SiteDetails.site_address}
									onPlaceSelected={/** @param {google.maps.places.PlaceResult} place */ (place) => {
										const address = {
											state: findAddressComponent(place, "administrative_area_level_1"),
											postcode: findAddressComponent(place, "postal_code"),
											city: findAddressComponent(place, "locality"),
											street_number: findAddressComponent(place, "street_number"),
											route: findAddressComponent(place, "route", true),
											address: document.getElementById("SiteDetails-site_address").value
										}

										const state = (address.state || "").toLowerCase();
										const foundState = (this.state.states || []).find(s => s.label.toLowerCase() == state)
										
										setFieldValue("SiteDetails[state]", (foundState || {}).value)
										setFieldValue("SiteDetails[site_address]", [address.street_number, address.route].join(" "))
										setFieldValue("SiteDetails[postcode]", address.postcode)
										setFieldValue("SiteDetails[city]", address.city)
									}}
									types={['address']}
									componentRestrictions={{country: "au"}}
									onChange={handleChange}
								/>
								<ErrorMessage name="SiteDetails[site_address]" render={this.renderValidationError}/>
							</div>
							<div className="col-md-2">
								<RequiredLabel htmlFor="SiteDetails-state">State</RequiredLabel>
								{this.validator.message('SiteDetails[state]', values.SiteDetails.state, 'required')}
								<CustomSelect required 
									variant="light"
									name="SiteDetails[state]"
									id="SiteDetails-state"
									className="form-control"
									value={values.SiteDetails.state}
									options={states}
									clearable={false}
									onChange={(newValue) => newValue && setFieldValue("SiteDetails[state]", newValue.value)}
								/>
								<ErrorMessage name="SiteDetails[state]" render={this.renderValidationError}/>
							</div>
							
							<div className="col-12 col-md-2">
								<RequiredLabel htmlFor="SiteDetails-city">Suburb:</RequiredLabel>
								{this.validator.message('SiteDetails[city]', values.SiteDetails.city, 'required')}
								<Field required name="SiteDetails[city]" id="SiteDetails-city" className="form-control border-dark"/>
								<ErrorMessage name="SiteDetails[city]" render={this.renderValidationError}/>
							</div>

							<div className="col-12 col-md-2">
								<RequiredLabel htmlFor="SiteDetails-postcode">Postcode</RequiredLabel>
								{this.validator.message('SiteDetails[postcode]', values.SiteDetails.postcode, 'required')}
								<Field name="SiteDetails[postcode]" id="SiteDetails-postcode" className="form-control border-dark" required data-rule-number="true" minLength="4" maxLength="4" data-rule-postcodecheck="true"/>
								<ErrorMessage name="SiteDetails[postcode]" render={this.renderValidationError}/>
							</div>
						
							<div className="col-md-2">
								<RequiredLabel htmlFor="SiteDetails-abn">ABN:</RequiredLabel>
								{this.validator.message('SiteDetails[abn]', values.SiteDetails.abn, 'required|abn_min')}
								<Field name="SiteDetails[abn]" id="SiteDetails-abn" className="form-control border-dark"/>
								<ErrorMessage name="SiteDetails[abn]" render={this.renderValidationError}/>
							</div>
						</div>
					
						<div className="row mt-5 mx-0">
							<h3 className="color">Key Contact Details</h3>
						</div>

						<div className="row">
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="KeyContactDetails-first_name">First name</RequiredLabel>
								{this.validator.message('KeyContactDetails[first_name]', values.KeyContactDetails.first_name, 'required')}
								<Field name="KeyContactDetails[first_name]" id="KeyContactDetails-first_name" className="form-control border-dark" required/>
								<ErrorMessage name="KeyContactDetails[first_name]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="KeyContactDetails-last_name">Last name</RequiredLabel>
								{this.validator.message('KeyContactDetails[last_name]', values.KeyContactDetails.last_name, 'required')}
								<Field name="KeyContactDetails[last_name]" id="KeyContactDetails-last_name" className="form-control border-dark" required/>
								<ErrorMessage name="KeyContactDetails[last_name]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="KeyContactDetails-position">Position</RequiredLabel>
								{this.validator.message('KeyContactDetails[position]', values.KeyContactDetails.position, 'required')}
								<Field name="KeyContactDetails[position]" id="KeyContactDetails-position" className="form-control border-dark" required/>
								<ErrorMessage name="KeyContactDetails[position]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<label htmlFor="KeyContactDetails-department">Department</label>
								<Field name="KeyContactDetails[department]" id="KeyContactDetails-department" className="form-control border-dark"/>
								<ErrorMessage name="KeyContactDetails[department]" render={this.renderValidationError}/>
							</div>
						</div>
					
						<div className="row mt-3">
							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									errors={errors}
									label='Phone Contact'
									values={values.KeyContactDetails.phones}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="KeyContactDetails-phones"
									namePrefix="KeyContactDetails[phones]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|phone||phone_min')
										}
										
										return this.validator.message(name, value, 'phone', {
											messages: {
												phone: "Not a valid secondary phone"
											}
										})
									}}
								/>
							</div>
							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									errors={errors}
									label='Email Contact'
									values={values.KeyContactDetails.emails}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="KeyContactDetails-emails"
									namePrefix="KeyContactDetails[emails]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|email')
										}
										
										return this.validator.message(name, value, 'email', {
											messages: {
												email: "Not a valid secondary email"
											}
										})
									}}
								/>
							</div>
							<div className="col-12 col-md-3" style={{display: "flex"}}>
								<div style={{marginTop: 30}}>
									<CustomCheckbox 
										label={`Billing contact same as Key Contact`}
										name="KeyContactDetails[is_billing_same_as_key_contact]" 
										id="KeyContactDetails-is_billing_same_as_key_contact"
										value={values.KeyContactDetails.is_billing_same_as_key_contact}
										onChange={e => {
											const checked = !!e.currentTarget.checked
											
											if (checked) {
												setFieldValue('BillingContactDetails[first_name]', values.KeyContactDetails.first_name)
												setFieldValue('BillingContactDetails[last_name]', values.KeyContactDetails.last_name)
												setFieldValue('BillingContactDetails[position]', values.KeyContactDetails.position)
												setFieldValue('BillingContactDetails[department]', values.KeyContactDetails.department)
												setFieldValue('BillingContactDetails[phones]', (values.KeyContactDetails.phones || []).map(p => ({...p})))
												setFieldValue('BillingContactDetails[emails]', (values.KeyContactDetails.emails || []).map(e => ({...e})))
											} else {
												setFieldValue('BillingContactDetails[first_name]', initialValues.KeyContactDetails.first_name)
												setFieldValue('BillingContactDetails[last_name]', initialValues.KeyContactDetails.last_name)
												setFieldValue('BillingContactDetails[position]', initialValues.KeyContactDetails.position)
												setFieldValue('BillingContactDetails[department]', initialValues.KeyContactDetails.department)
												setFieldValue('BillingContactDetails[phones]', (initialValues.KeyContactDetails.phones || []).map(p => ({...p})))
												setFieldValue('BillingContactDetails[emails]', (initialValues.KeyContactDetails.emails || []).map(e => ({...e})))
											}
											handleChange(e)
										}}
									/>
									<ErrorMessage name="KeyContactDetails[is_billing_same_as_key_contact]" render={this.renderValidationError}/>
								</div>
							</div>
						</div>
					
						<div className="row mt-5 mx-0">
							<h3 className="color">Billing Contact Details</h3>
						</div>

						<div className="row">
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="BillingContactDetails-first_name">First name</RequiredLabel>
								{this.validator.message('BillingContactDetails[first_name]', values.BillingContactDetails.first_name, 'required')}
								<Field name="BillingContactDetails[first_name]" id="BillingContactDetails-first_name" className="form-control border-dark" required/>
								<ErrorMessage name="BillingContactDetails[first_name]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="BillingContactDetails-last_name">Last name</RequiredLabel>
								{this.validator.message('BillingContactDetails[last_name]', values.BillingContactDetails.last_name, 'required')}
								<Field name="BillingContactDetails[last_name]" id="BillingContactDetails-last_name" className="form-control border-dark" required/>
								<ErrorMessage name="BillingContactDetails[last_name]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="KeyContactDetails-position">Position</RequiredLabel>
								{this.validator.message('BillingContactDetails[position]', values.BillingContactDetails.position, 'required')}
								<Field name="BillingContactDetails[position]" id="BillingContactDetails-position" className="form-control border-dark" required/>
								<ErrorMessage name="BillingContactDetails[position]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<label htmlFor="BillingContactDetails-department">Department</label>
								<Field name="BillingContactDetails[department]" id="BillingContactDetails-department" className="form-control border-dark"/>
								<ErrorMessage name="BillingContactDetails[department]" render={this.renderValidationError}/>
							</div>
						</div>
						
						<div className="row mt-3">
							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									errors={errors}
									label='Phone Contact'
									values={values.BillingContactDetails.phones}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="BillingContactDetails-phones"
									namePrefix="BillingContactDetails[phones]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|phone||phone_min')
										}

										return this.validator.message(name, value, 'phone', {
											messages: {
												phone: "Not a valid secondary phone"
											}
										})
									}}
									/>
							</div>
							<div className="col-12 col-md-3">
								<AddPrimarySecondaryContact
									errors={errors}
									label='Email Contact'
									values={values.BillingContactDetails.emails}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									renderValidationError={this.renderValidationError}
									idPrefix="BillingContactDetails-emails"
									namePrefix="BillingContactDetails[emails]"
									renderBeforeInput={({ value, name, isPrimary }) => {
										if (isPrimary) {
											return this.validator.message(name, value, 'required|email')
										}
										
										return this.validator.message(name, value, 'email', {
											messages: {
												email: "Not a valid secondary email"
											}
										})
									}}
									/>
							</div>

							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="BillingContactDetails-pay_roll_tax">Pay roll tax</RequiredLabel>
								{this.validator.message('BillingContactDetails[pay_roll_tax]', values.BillingContactDetails.pay_roll_tax, 'required')}
								<CustomSelect required 
									variant="light"
									name="BillingContactDetails[pay_roll_tax]"
									id="BillingContactDetails-pay_roll_tax"
									className="form-control"
									value={values.BillingContactDetails.pay_roll_tax}
									options={[
										{value: null, label: "Select option", selected: true, disabled: true}, 
										{value: '1', label: "Yes"}, 
										{value: '0', label: "No"}
									]}
									clearable={false}
									onChange={(newValue) => newValue && setFieldValue("BillingContactDetails[pay_roll_tax]", newValue.value)}
								/>
								<ErrorMessage name="BillingContactDetails[pay_roll_tax]" render={this.renderValidationError}/>
							</div>
							<div className="col-12 col-md-3">
								<RequiredLabel htmlFor="BillingContactDetails-gst">GST</RequiredLabel>
								{this.validator.message('BillingContactDetails[gst]', values.BillingContactDetails.gst, 'required')}
								<CustomSelect required 
									variant="light"
									name="BillingContactDetails[gst]"
									id="BillingContactDetails-gst"
									className="form-control"
									value={values.BillingContactDetails.gst}
									options={[
										{value: null, label: "Select option", selected: true, disabled: true}, 
										{value: '1', label: "Yes"}, 
										{value: '0', label: "No"}
									]}
									clearable={false}
									onChange={(newValue) => newValue && setFieldValue("BillingContactDetails[gst]", newValue.value)}
								/>
								<ErrorMessage name="BillingContactDetails[gst]" render={this.renderValidationError}/>
							</div>
						</div>

						<div className="form-group mt-5 text-right">
							<button type="submit" disabled={this.state.loading} onClick={e => {
								setFieldValue("publish", true);
								handleSubmit(e);
							}} className="btn btn-dark">Save Changes</button>
						</div>

						<div className="form-group">
							<br/>
							<br/>
						</div>
					</Form>
						)
					}
				}
			</Formik>
		)
	}

	render() {
		return (
			<DashboardLayout className="page-sites-create">
				<div className="container-fluid">
					<div className="content-block">
						{this.renderBreadcrumbs()}
					</div>
					<div className="content-block">
						{this.renderOrgSummary()}
					</div>
					<div className="content-block">
						{this.renderForm()}
					</div>
				</div>
			</DashboardLayout>
		)
	}
}

const mapStateToProps = state => {
	const {token} = state.auth.user

	let claims = null;
	if (token) {
		claims = jwt.decode(token)
	}

	return {
		organisationName: (claims || {})['name']
	}
}

export default connect(mapStateToProps)(CreatePage)
