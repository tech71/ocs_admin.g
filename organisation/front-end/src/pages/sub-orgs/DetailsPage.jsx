import React from 'react'
import { connect } from 'react-redux'

import DashboardLayout from '../../layouts/DashboardLayout'

/**
 * Displays org details
 * 
 * @typedef {{} & import('react-redux').DispatchProp} Props
 * @typedef {{}} State
 * @extends {React.Component<Props, State>}
 */
class DetailsPage extends React.Component {
    render() {
        return (
            <DashboardLayout className="page-suborgs-success">
                <div className="container-fluid">
                    <h3>
                        Successfully created suborg
                    </h3>
                </div>
            </DashboardLayout>
        )
    }
}

export default connect(null, null)(DetailsPage)