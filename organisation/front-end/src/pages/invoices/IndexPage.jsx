import React from "react"
import DashboardLayout from "../../layouts/DashboardLayout"
import { connect } from "react-redux"

class IndexPage extends React.Component {
    render() {
        return (
            <DashboardLayout className="page-invoices-index">
                <div className="container-fluid">
                    <h4>All Invoices</h4>
                </div>
            </DashboardLayout>
        )
    }
}

export default connect()(IndexPage)