import React from "react";
import DashboardLayout from '../layouts/DashboardLayout';
import { Link } from 'react-router-dom';
import OrgInfoSummary from '../components/OrgInfoSummary';
import BasicDisplayForm from '../components/BasicDisplayFormComponent';
import ContactDisplayForm from '../components/ContactDisplayComponent';
import UpdateOrgDetailModal from '../components/UpdateOrgDetailModalComponent';
import UpdateContactDetailModalComponent from '../components/updateContactDetailModalComponent';

import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { LOCALSTORAGE_KEY } from "../constants";
import { axios } from "../axios";
import { ToastContainer, toast } from 'react-toastify';

import "../stylesheets/pages/ParentOrgReviewPage.scss";
import 'react-select-plus/dist/react-select-plus.css';
import 'react-toastify/dist/ReactToastify.css';


/**
 * Component that renders the main page of an organization
*/

class ParentOrgReviewPage extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            orgDetail: null,
            orgContacts: null,
            contactTypes: [],
            showUpdateModal: false,
            showUpdateContactModal: false,
            contactToLoadInUpdateModal: null
        };

        this.httpConfig = {
            orgDataUrl: '/ParentOrgController/getOrgDetails',
            stateListUrl: '/CommonController/getStateList'
        };
    }
    
    toggleContactDisplay = (toggledOrgContact) => {
        const orgContacts = this.state.orgContacts;
        const newOrgContacts = orgContacts.map((orgContact, i) => {
            if (orgContact === toggledOrgContact) {
                return {
                    ...orgContact,
                    expanded: !orgContact.expanded
                }
            }

            return orgContact;
        })

        this.setState({
            orgContacts: newOrgContacts
        })
    }

    updateContactButtonHandler = (organizationContact) => {
        this.setState({
            showUpdateContactModal: true,//!this.state.showUpdateContactModal
            contactToLoadInUpdateModal: organizationContact
        });
    }

    modalCloseHandler = () => {
        this.setState({
            showUpdateModal: false
        });
        this.fetchOrgDetails();
    }

    updateContactModalCloseHandler = () => {
        this.setState({
            showUpdateContactModal: false
        });
        // this.fetchOrgDetails();
    }

    displayModalHandler = () => {
        this.setState({
            showUpdateModal: true
        });
    }

    orgDetailUpdated = (response) => {
        this.setState({
            showUpdateModal: false
        });

        if(response.data.status === true) {
            toast(response.data.msg, {
                toastId: 'updated_org_detail'
            });
        }

        this.fetchOrgDetails();
    }

    contactWasUpdated = (response) => {
        this.updateContactModalCloseHandler();

        if(response.data.status === true) {
            toast(response.data.msg, {
                toastId: 'updated_contact_detail'
            });
        }

        this.fetchOrgDetails();
    }

	render() {
        const orgDetailLabelAndColumnMapping = {
            Name: 'name', Address: 'primary_address',
            'Office Phone': 'OrganisationPh', 
            'Office Email': 'OrganisationEmail',
            'ABN NO': 'abn', 'Payroll Tax': 'payroll_tax', GST: 'gst',
            'Booking Status': 'booking_status'
        };

        const orgContactLabelAndColumnMapping = {
            'First Name': 'first_name', 'Last Name': 'last_name',
            Position: 'position', email: 'email_primary',
            Phone: 'phone_primary'
        };

        var contactComponents = [];

        //Build the contact card components
        if(this.state.orgContacts) {
            for(let i = 0; i < this.state.orgContacts.length; i++) {
                contactComponents.push(
                    <div className="parent-org-details org-contacts" key={`${i}-${this.state.orgContacts[i].email}`}>
                        <ContactDisplayForm expanded={this.state.orgContacts[i].expanded}
                            toggleContactDisplay={this.toggleContactDisplay}
                            labelAndColumnMapping={orgContactLabelAndColumnMapping}
                            organizationContact={this.state.orgContacts[i]}
                            updateButtonHandler={this.updateContactButtonHandler}>
                        </ContactDisplayForm>
                    </div>
                );
            }
		}
		console.log(this.state.orgDetail);

        return(
            <DashboardLayout className="page-org-review">
                <div className="container-fluid">
                    <Breadcrumb>
                        <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
                    </Breadcrumb>

					<div className="site-information">
                    <OrgInfoSummary orgDetail={this.state.orgDetail}></OrgInfoSummary>
					</div>

                    <div className="entity-details">

                        {/* The ToastContainer component has to be mounted somewhere in the app for toasts to work */}
                        <ToastContainer 
                            className='toast-container'
                            toastClassName="green-toast"
                            hideProgressBar={true}
                        />

                        <BasicDisplayForm header="Your Org - Details"  button_title="Update org details" 
                            labelAndColumnMapping={orgDetailLabelAndColumnMapping}
                            organizationDetail={this.state.orgDetail}
                            showModal={this.displayModalHandler}>
                        </BasicDisplayForm>
                    </div>

					<div className="entry-header entity-contact-header">
					<h2><b>Your Org - Contacts</b></h2>
					</div>

                    { contactComponents.length > 0&& contactComponents }
                </div>

                <UpdateOrgDetailModal modalId="updateOrgDetailModal" 
                    showPopup={this.state.showUpdateModal}
                    closeModal={this.modalCloseHandler}
                    stateList={this.state.stateList}
                    orgDetails={this.state.orgDetail}
                    afterUpdatingOrgDetail={this.orgDetailUpdated}
                >
                </UpdateOrgDetailModal>

                <UpdateContactDetailModalComponent modalId="updateContactDeatilModal" 
                    updateModalShouldLanuch={this.state.showUpdateContactModal}
                    updateButtonHandler={this.updateContactButtonHandler}
                    updateContactModalCloseBtnHandler={this.updateContactModalCloseHandler}
                    contactWasUpdatedCallback={this.contactWasUpdated}
                    loadedContact={this.state.contactToLoadInUpdateModal}
                    contactTypes={this.state.contactTypes}
                >

                </UpdateContactDetailModalComponent>

            </DashboardLayout>
        );
    }

    fetchOrgDetails() {
        axios.post(this.httpConfig.orgDataUrl).then(response => {
            var orgDetail = null;

            if(response.data !== null && typeof response.data !== 'undefined') {
                if(response.data.status == true) {
                    orgDetail = response.data.data;
                    this.setState({
                        orgDetail: orgDetail,
                        orgContacts: orgDetail.org_contacts,
                        contactTypes: orgDetail.contact_types
                    });
                }
            }
        });
    }

    componentDidMount() {
        this.fetchOrgDetails();

        //fetch state list
        axios.get(this.httpConfig.stateListUrl).then(response => {
            if(response.data.data) {
                this.setState({
                    stateList: response.data.data
                });
            }
        });
	}

	checkLoginStatus() {
		var authInfo = localStorage.getItem(LOCALSTORAGE_KEY);
		if (authInfo === null || typeof authInfo === "undefined") {
			return false;
		} else {
			return authInfo;
		}
	}
}

export default ParentOrgReviewPage;
