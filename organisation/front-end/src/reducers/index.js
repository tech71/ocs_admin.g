import { combineReducers } from 'redux';
import { auth } from './auth';
import { alerts } from "./alerts";
import { passwordReset } from "./passwordReset"

const rootReducer = combineReducers({
    auth,
    alerts,
    passwordReset
});

export default rootReducer;