import React from 'react'
import { ErrorMessage } from 'formik'
import { getIn } from 'formik'
// import AddIconBlack from "../images/Add-Icon-Black.svg"
// import RemoveIconBlack from "../images/Add-Icon-Black.svg"

/**
 * Custom component to display inputs that allow adding primary/secondary phone/email contatct
 * 
 * @param {object} props
 * @param {{value: any, is_primary?: boolean}[]} props.values
 * @param {(field: string, value: any, shouldValidate?: boolean) => void} props.setFieldValue
 * @param {(e: React.ChangeEvent<any>) => void} props.handleChange
 * @param {string} props.label
 * @param {string} props.idPrefix
 * @param {string} props.namePrefix
 * @param {string} [props.primaryText]
 * @param {string} [props.secondaryText]
 * @param {string} [props.type]
 * @param {number} [props.maxItems]
 * @param {string} [props.addIcon]
 * @param {string} [props.removeIcon]
 * @param {(data: { value: any, name: string, isPrimary: boolean }) => JSX.Element} [props.renderBeforeInput]
 * @param {(data: { value: any, name: string, isPrimary: boolean }) => JSX.Element} [props.renderAfterInput]
 */
export const AddPrimarySecondaryContact = ({
    values,
    setFieldValue,
    handleChange,
    renderValidationError,
    label,
    idPrefix,
    namePrefix,
    errors,
    // addIcon = AddIconBlack,
    // removeIcon = RemoveIconBlack,
    primaryText = ' (Primary)',
    secondaryText = ' (Secondary)',
    type = 'text',
    maxItems = 3,
    renderBeforeInput = () => null,
    renderAfterInput = () => null
}) => {


    return values.map((value, i, readonlyValues) => {

        const required = !!value.required
        const isPrimary = !!value.is_primary
        const displayAddIcon = isPrimary && readonlyValues.length < 3
        const iconStyle = {
            display: "inline-block",
            position: "absolute",
            top: 4,
            right: 4,
            cursor: "pointer"
        }

        const name = `${namePrefix}[${i}][value]`;
        const errorMessage = getIn(errors, name);

        return (
            <div key={i}>
                <label htmlFor={`${idPrefix}-${i}-value`}>
                    {label} {isPrimary ? primaryText : secondaryText} &nbsp;
                    {required ? <span className="required">*</span> : null}
                </label>
                {renderBeforeInput({ value: value.value, name, isPrimary })}
                <div style={{position: "relative"}}>
                    <input type="hidden" name={`${namePrefix}[${i}][is_primary]`} value={readonlyValues[i].is_primary}/>
                    <input type={type}
                        className="form-control border-dark" 
                        id={`${idPrefix}-${i}-value`}
                        name={name} 
                        value={readonlyValues[i].value} 
                        onChange={handleChange}
                    />
                    {
                        displayAddIcon && (
                            <span style={iconStyle} onClick={e => {
                                const vals = readonlyValues || [];
                                if (vals.length < maxItems) {
                                    const newVals = vals.concat({value: ""})
                                    setFieldValue(namePrefix, newVals)
                                }
                            }}>
                                {/* <span className="icon-decrease-icon" style={{fontSize: 22}}></span> */}
                                <span className="icon-add-icons" style={{fontSize: 30}}></span>
                            </span>
                        )
                    }
                    {
                        !isPrimary && (
                            <span style={{...iconStyle, top: 9, right: 6}} onClick={e => {
                                const vals = readonlyValues || [];
                                const newVals = vals.filter(p => p !== value);
                                setFieldValue(namePrefix, newVals)
                            }}>

                                {/* <img src={removeIcon} alt="Remove secondary contact"/> */}
                                <span className="icon-decrease-icon" style={{fontSize: 22}}></span>
                            </span>
                        )
                    }
                </div>
                {renderAfterInput({ value: value.value, name, isPrimary })}
                {
                    // <ErrorMessage ... /> will not work!
                    errorMessage && renderValidationError(getIn(errors, name))
                }
            </div>
        )
    })
}