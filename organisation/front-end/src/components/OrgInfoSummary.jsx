import React from "react";
import ReactDOM from "react-dom";

/**
 * Component to display summary information of the organization
 */
class OrgInfoSummary extends React.Component {
	constructor(props) {
        super(props);
	}

	render() {
		const orgDetail = this.props.orgDetail;
		const orgBasicDetail = (orgDetail && orgDetail.basic_detail) || null;
		
		var orgLogo = '';
		if(orgBasicDetail && orgBasicDetail.logo_file) {
			orgLogo = (<div className="org-logo">
						<img
							src={orgBasicDetail && process.env.REACT_APP_ADMIN_URL+orgBasicDetail.logo_file}
							alt=""
							width="100"
							height="100"
						/>
			</div>);
		}

		return (
			<div className="d-flex org-summary flex-row">
				{orgLogo}
				<div className="org-info">
					<ul>
						<li>
							<span className="font-weight-bold">OCS ID:</span>&nbsp;{orgBasicDetail && orgBasicDetail.ocs_id}
						</li>
						<li>
							<span className="font-weight-bold">Address:</span>&nbsp;{orgBasicDetail && orgBasicDetail.primary_address}
						</li>
						<li>
							<span className="font-weight-bold">
								Support Coordinator:
							</span>{" "}
							zzzz
						</li>
						<li>
							<span className="font-weight-bold">Status:</span>&nbsp;{(orgBasicDetail && orgBasicDetail.status) === "1" ? 'Active' : 'Inactive'}
						</li>
					</ul>
				</div>
			</div>
		);
    }
}

export default OrgInfoSummary;

