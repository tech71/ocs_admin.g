import React from "react";

import GoogleIcon from "../images/Google-Icon.svg"

import "../stylesheets/components/LoginHeader.scss"

export default function LoginHeader() {

    return (
        <div className="site-header">
            <div className="language-selection-parent text-center">
                <button type="button" className="btn btn-light" style={{verticalAlign: "middle", width: "100%", maxWidth: 240}}>
                    <img src={GoogleIcon} alt="Google Translate" />
                    <span className="inner-text">
                        Select Language
                    </span>
                    <span className="arrow-right"></span>
                </button>
            </div>
        </div>
    )
}