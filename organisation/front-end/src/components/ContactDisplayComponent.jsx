import React from 'react';
import ReactDOM from 'react-dom';
import "../stylesheets/components/ContactDisplay.scss";
import "../stylesheets/icons.css";
import EditIconBlack from "../icons/Edit-Icon-Black.svg";


/**
 * Component to render each contact's information for the organization in label:value format
 */
class ContactDisplayComponent extends React.Component {
	constructor(props) {
        super(props);

        this.state = {
        }
	}

    toggleDisplay() {
        const currentOrgContact = this.props.organizationContact;
        this.props.toggleContactDisplay(currentOrgContact)
    }
   

	render() {
        const labelsAndColumns = this.props.labelAndColumnMapping;
        const organizationContact = this.props.organizationContact;
        const contactCardLabel = organizationContact && (`${organizationContact[labelsAndColumns['First Name']]} ${organizationContact[labelsAndColumns['Last Name']]} - ${organizationContact[labelsAndColumns['Position']]}`)
        var rows = [];

        // Build the contact card
        for(let label in labelsAndColumns) {
            var i = 1;

            if(labelsAndColumns.hasOwnProperty(label)) {
				var renderedValue = '';
				console.log(organizationContact[labelsAndColumns[label]])
				if(label === 'Phone' && organizationContact && organizationContact[labelsAndColumns[label]] && typeof organizationContact[labelsAndColumns[label]] === 'object') {
					for (const [index, com_row] of organizationContact[labelsAndColumns[label]].entries()) {
						if(renderedValue)
							renderedValue += ", " + com_row.phone
						else
							renderedValue += com_row.phone
					}
				} else if(label === 'email' && organizationContact && organizationContact[labelsAndColumns[label]] && typeof organizationContact[labelsAndColumns[label]] === 'object') {
					var valueArr = []
					for (const [index, com_row] of organizationContact[labelsAndColumns[label]].entries()) {
						valueArr.push(
							<a href={`mailTo: ${com_row.email}`}>{com_row.email}</a>
						)
					}
					renderedValue = valueArr.map((r, i, readonlyArray) => (
						<React.Fragment key={i}>{r}{ (i < readonlyArray.length - 1 ? ', ' : '' ) }</React.Fragment>
					))
				} else if(organizationContact && organizationContact[labelsAndColumns[label]]) {
                    renderedValue = organizationContact[labelsAndColumns[label]];
                }

                rows.push(
                    <div className="row data-row d-flex" key={`${i}-${label}`}>
                        <div className="font-weight-bold label">{label} :</div>
                        <div className="value">&nbsp;
                            { renderedValue }
                        </div>
                    </div>
                );

                i++;
            }
        }

        return(
            <div>
                <div className={`contact-data ${this.props.expanded ? 'expanded' : 'collapsed'}`}>
                    <div className="row d-flex contact-header">
                        <div className="col-sm-6 col-md-6 font-weight-bold contact-label">{contactCardLabel}</div>
                        <div className={`col-sm-6 col-md-6 ${this.props.expanded ? 'show' : 'hide'} `} style={{textAlign: 'end'}}>
                            <span className="caret-action">{this.props.expanded ? 'CLOSE' : 'OPEN'}</span><div className="contact-caret" onClick={e => this.toggleDisplay()}></div>
                        </div>
                    </div>
                    <div className={this.props.expanded ? 'd-flex flex-column contact-details' : ''} style={{display: this.props.expanded === true ? "block": "none"}}>
                        {rows}
                        <div className="contact-actions d-flex justify-content-end align-items-center">
                            <span className="update-contact-text">UPDATE</span>
                            <img className="icon icon-update" src={EditIconBlack} onClick={ (e) => {this.props.updateButtonHandler(organizationContact)} }/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContactDisplayComponent;