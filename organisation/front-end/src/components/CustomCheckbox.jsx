import React from 'react';

// styling for <CustomCheckbox /> is located from "/src/layouts/general.scss"

/** 
 * Custom-styled checkbox
 * 
 * @param {React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & { label: JSX.Element }} props  
 */
const CustomCheckbox = (props) => {
    return (
        <div className="custom-checkbox">
            <label htmlFor={props.id}>
                <input {...props} type="checkbox" />
                <span className="box"></span>
                {props.children || props.label}
            </label>
        </div>
    )
}

export default CustomCheckbox;