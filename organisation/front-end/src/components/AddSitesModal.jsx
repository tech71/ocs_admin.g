import React, { useState } from 'react'
import { Modal, Button } from 'react-bootstrap'
// import AsyncSelect from 'react-select/async'
import { CustomAsyncSelect } from "./CustomSelect"
import { organisationService } from '../services/organisationService';

/**
 * @param {object} props 
 * @param {boolean} props.show
 * @param {ValueType<OptionTypeBase>|null} props.value
 * @param {(selectedOption:  ValueType<OptionTypeBase>) => void} props.onAdd
 * @param {() => void} props.onCancel
 */
export default function AddSitesModal({
    value = null,
    show = false,
    onAdd = () => {},
    onCancel = () => {},
}) {

    const [selectedOption, setSelectedOption] = useState(value);

    return (
        <Modal show={show} onHide={() => onCancel()}>
            <Modal.Header closeButton>
                <Modal.Title>Add sites</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <CustomAsyncSelect
                    onChange={(newValue) => setSelectedOption(newValue)}
                    value={selectedOption}
                    cache={false}
                    clearable={true}
                    loadOptions={async (inputValue, cb) => {
                        if (!inputValue) {
                            return Promise.resolve({ options: [] })
                        }

                        const res = await organisationService.searchSites(inputValue);
                        const { data } = res.data;
                        return { options: data };
                    }}
                />

                {/* <AsyncSelect
                    onChange={(value, action) => setSelectedOption(value)}
                    loadOptions={async (inputValue, callback) => {
                        const response = await organisationService.searchSites(inputValue)
                        const {data} = response.data;
                        callback(data)
                    }}
                /> */}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => onAdd(selectedOption)} disabled={!selectedOption}>
                    Add
                </Button>
                <Button variant="secondary" onClick={() => onCancel()}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}
