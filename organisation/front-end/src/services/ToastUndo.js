
import React from 'react';

const ToastUndo = (props) => {
  let icon = 'icon';
  if(props.showType =='e'){
      icon += ' icon-error-icons';
  }else if(props.showType =='s'){
       icon += ' icon-accept-approve1-ie';
  }else if(props.showType =='w'){
    icon += ' icon-accept-warning2-ie';
}else if(props.showType =='i'){
  icon += ' icon-accept-info3-ie';
}


  let data = data!= null && typeof(data) == 'string' ? props.message.split('.,').join('.\n').split('\n').map((text, index) => (
    <React.Fragment key={`${text}-${index}`}>
      {text}
      <br />
    </React.Fragment>
  )):props.message;

  let msgdata = (<p>{data}</p>);
    return (
        <div className="Toastify_content__">
        <h3>
          {msgdata} 
          <i className={icon}></i>
        </h3>
      </div>
    );
}
ToastUndo.defaultProps = {
     message: '',
    showType: 'e'
};

export { ToastUndo };


