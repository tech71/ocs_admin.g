import Axios from "axios"
import { logout } from "./actions/userActions";
import { LOGIN_URL, LOCALSTORAGE_KEY } from "./constants";
import qs from "qs"
import { store } from "./store"

// creates instance of axios
export const axios = Axios.create()

// for every 401 responses, log the user out immediately (ie. redirect to login page)
// and display reason (eg 'User session has expired')
axios.interceptors.response.use(res => res, error => {
    if (error.response.status === 401) {
        logout();
        window.location = LOGIN_URL + "?" + qs.stringify({ error_code: 1 })
        return;
    }
    
    return Promise.reject(error);
})


// flash messages
axios.interceptors.response.use(res => {
    const { _flash } = res.data;
    if (_flash) {
        Object.keys(_flash).forEach(key => {
            const variant = key;
            const message = _flash[key];
            store.dispatch({ type: "ALERTS_ADD", payload: { message, variant } })
        })

        setTimeout(() => store.dispatch({ type: "ALERTS_DISMISS_ALL" }), 2000)
    }

    return res
})

// attach Authorization on every request
// NOTE: Axios.create({ headers: authHeader() }) will not work
axios.interceptors.request.use(config => {
    let user = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
    if (user && user.token) {
        config.headers['Authorization'] = `Bearer ${user.token}`
    }

    return config;
}, error => Promise.reject(error))
