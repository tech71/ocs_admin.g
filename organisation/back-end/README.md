# HCM Organisation Portal Backend API

## Authors
- Joshua Orozco <joshua@ydt.com.au>

## Uses
- Codeigniter 3.1.11
- Some of the Laravel components
  - `Illuminate\Support`

## Setup locally
Make sure that ocm_admin has been setup properly. This application will connect to `db_ocs`. Run all pendings migrations.
```Bash
# Install dependencies
composer install

# Run this server on an open network. 
# Do not use localhost or else the front-end app will not work!
php -S 0.0.0.0:8000
```

## Miscellaneous
### Sample credentials for HCM-484
Currently the feature that creates organisation credentials and sends emails are not yet implemented.
You can use test credentials below to create usernames and passwords for organisations.
```
admin
$2y$10$jVVgTxwia8HRDUP4OvMAeObEtLj7jdSW8OAhtNr5ZYWO9v6HEH8Ya (my_password)
```
