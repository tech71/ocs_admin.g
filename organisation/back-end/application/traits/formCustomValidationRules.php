<?php

trait formCustomValidationRules {
    /**
     * custom validation function to validate postcode
     * @return bool
     */
    public function postcode_check($enteredPostcode) {
        if(!preg_match(POSTCODE_AU_REGEX_KEY, $enteredPostcode, $match)) {
            return false;
        }
            return true;        
    }

    public function phone_number_check($userEnteredValue, $validPhoneNumerPatter) {
        if(!preg_match($validPhoneNumerPatter, $userEnteredValue, $match)) {
            return false;
        }

        return true;
    }
}