<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation` table.
 */
class Organisation
{
    const TABLE_NAME = "tbl_organisation";

    public $id;
    public $companyId;
    public $name;
    public $abn;
    public $logo_file;
    public $parent_org;
    public $website;
    public $payroll_tax;
    public $gst;
    public $enable_portal_access;
    public $status;
    public $booking_status;
    public $booking_date;
    public $archive;
    public $username;
    public $password;
    public $password_reset_token;
    public $is_first_time_login;
}