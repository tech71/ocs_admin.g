<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_site_phone` table
 */
class OrganisationSitePhone extends Entity
{
	const TABLE_NAME = "tbl_organisation_site_phone";

	const PRIMARY_PHONE_PRIMARY = 1;
	const PRIMARY_PHONE_SECONDARY = 2;

	public $siteId;
	public $phone;
	public $primary_phone;
	public $archive;
}