<?php

namespace app\entities;

class FinanceManualInvoice extends Entity
{
    const TABLE_NAME = "tbl_finance_invoice";

    const BOOKED_BY_SITE_OR_HOME = 1;
    const BOOKED_BY_PARTICIPANT = 2;
    const BOOKED_BY_LOCATION = 3;
    const BOOKED_BY_ORGANISATION = 4;

    const STATUS_DRAFT = 0;
    const STATUS_SENT = 1;
    const STATUS_SENT_AND_READ = 2;
    const STATUS_ERROR_SENDING = 3;
    const STATUS_RESEND = 4;
    const STATUS_DISPUTE = 5;
    const STATUS_PAID = 6;
}
