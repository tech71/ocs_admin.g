<?php

namespace app\entities;

/**
 * Root class of all data-transfer model classes.
 * This class exist for the sake of strongly typing result set (eg. result of database query)
 */
abstract class Entity
{

}
