<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_address` table
 */
class OrganisationAddress extends Entity
{
    const TABLE_NAME = "tbl_organisation_address";
    
    const PRIMARY_ADDRESS_PRIMARY = 1;
    const PRIMARY_ADDRESS_SECONDARY = 2;
}