<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_requirement` table
 */
class OrganisationRequirement
{
    const TABLE_NAME = "tbl_organisation_requirement";
    
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var boolean
     */
    public $archive = false;

    /**
     * Format: `Y-m-d h:m:s`
     *
     * @var string
     */
    public $created;
}
