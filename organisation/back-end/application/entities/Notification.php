<?php

namespace app\entities;

/**
 * Class to represent `tbl_notification` table
 */
class Notification
{
    const TABLE_NAME = "tbl_notification";

    const USER_TYPE_MEMBER = 1;
    const USER_TYPE_PARTICIPANT = 2;
    const USER_TYPE_ORGANISATION = 3;

    const STATUS_NOT_READ = 0;
    const STATUS_READ = 1;
    
    const SENDER_TYPE_USER = 1;
    const SENDER_TYPE_ADMIN = 2;

    /**
     * @var string
     */
    public $id;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $user_type;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $shortdescription;

    /**
     * @var string
     */
    public $created;

    /**
     * @var int
     */
    public $status;

    /**
     * @var int
     */
    public $sender_type;
}
