<?php

namespace app\entities;

/**
 * Represents the set of claims decoded from JWT token
 * Extend this class if you wish to add more claims (eg, add id, username, date_created, etc)
 */
class UserIdentity
{
    /**
     * "Issued by" 
     * 
     * @var string|null
     */
    public $iss;

    /**
     * "Permitted for" claim
     *
     * @var string|null
     */
    public $aud;

    /**
     * "Identified by" claim
     *
     * @var string|null
     */
    public $jti;

    /**
     * "Issued at" claim
     *
     * @var int|null
     */
    public $iat;

    /**
     * "Can only be used after" claim
     *
     * @var int|null
     */
    public $bnf;

    /**
     * "Expires at" claim
     *
     * @var int|null
     */
    public $exp;


    /**
     * Converts array of claims to subclass of `\app\entities\UserIdentity`.
     *
     * @param array $claims
     * @return static
     */
    public static function from(array $claims)
    {
        $identity = new static;
        foreach ($claims as $name => $value) {
            if (property_exists($identity, $name)) {
                $identity->$name = $value;
            }
        }
        
        return $identity;
    }
}
