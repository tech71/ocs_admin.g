<?php

namespace app\entities;

/**
 * Contains set of details related to logged in organisation
 */
class OrganisationIdentity extends UserIdentity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $abn;

    /**
     * @var string
     */
    public $logo_file;

    /**
     * @var int
     */
    public $parent_org;

    /**
     * @var string
     */
    public $website;

    /**
     * @var bool
     */
    public $payroll_tax;

    /**
     * @var bool
     */
    public $gst;

    /**
     * @var bool
     */
    public $enable_portal_access;

    /**
     * @var bool
     */
    public $status;

    /**
     * @var bool
     */
    public $booking_status;

    /**
     * @var string|null
     */
    public $booking_date;

    /**
     * @var bool
     */
    public $archive;

    /**
     * @var string
     */
    public $username;

    /**
     * @var bool
     */
    public $is_first_time_login;


    /**
     * Converts set of claims to an instance of `\app\entities\OrganisationIdentity`.
     * Originally when JWT is decoded into a PHP array, it always return a 
     * dictionary of strings. This function converts those string values into proper 
     * data types.
     * 
     * For example: if `gst` is passed as string `"1"` it will be converted to boolean `true` 
     * before reaching `parent::from($claims)`
     * 
     * @param array $claims
     * @return static
     */
    public static function from(array $claims)
    {
        $claims = array_merge($claims, [
            'id' => (int)$claims['id'],
            'companyId' => (int) $claims['companyId'],
            'parent_org' => (int) $claims['parent_org'],
            'payroll_tax' => (bool) $claims['payroll_tax'],
            'gst' => (bool) $claims['gst'],
            'enable_portal_access' => (bool) $claims['enable_portal_access'],
            'status' => (bool) $claims['status'],
            'booking_status' => (bool) $claims['booking_status'],
            'archive' => (bool) $claims['archive'],
            'is_first_time_login' => (bool) $claims['is_first_time_login'],
        ]);

        return parent::from($claims);
    }
}