<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_all_contact` table
 */
class OrganisationSiteContact extends Entity
{
    const TABLE_NAME = "tbl_organisation_site_key_contact";

    const TYPE_SUPPORT_COORDINATOR = 1;
    const TYPE_MEMBER = 2;
    const TYPE_KEY_CONTACT = 3;
    const TYPE_BILLING = 4;
}