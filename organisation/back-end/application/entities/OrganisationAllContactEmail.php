<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_all_contact_email` table
 */
class OrganisationAllContactEmail extends Entity
{
    const TABLE_NAME = "tbl_organisation_all_contact_email";

    const PRIMARY_EMAIL_PRIMARY = 1;
    const PRIMARY_EMAIL_SECONDARY = 2;
}
