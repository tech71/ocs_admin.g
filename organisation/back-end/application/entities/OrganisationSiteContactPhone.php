<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_all_contact_phone` table
 */
class OrganisationSiteContactPhone extends Entity
{
    const TABLE_NAME = "tbl_organisation_site_key_contact_phone";

    const PRIMARY_PHONE_PRIMARY = 1;
    const PRIMARY_PHONE_SECONDARY = 2;
}