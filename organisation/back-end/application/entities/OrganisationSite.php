<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation` table.
 */
class OrganisationSite
{
	const TABLE_NAME = "tbl_organisation_site";

	public $id;
	public $organisationId;
	public $site_name;
	public $abn;
	public $street;
	public $city;
	public $state;
	public $postal;
	public $archive;
	public $status;
	public $created;
	public $updated;
}