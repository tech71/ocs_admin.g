<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_site_email` table
 */
class OrganisationSiteEmail extends Entity
{
	const TABLE_NAME = "tbl_organisation_site_email";

	const PRIMARY_PHONE_PRIMARY = 1;
	const PRIMARY_PHONE_SECONDARY = 2;

	public $siteId;
	public $email;
	public $primary_phone;
	public $archive;
}