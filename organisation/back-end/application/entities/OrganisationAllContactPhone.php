<?php

namespace app\entities;

/**
 * Class to represent a row under `tbl_organisation_all_contact_phone` table
 */
class OrganisationAllContactPhone extends Entity
{
    const TABLE_NAME = "tbl_organisation_all_contact_phone";

    const PRIMARY_PHONE_PRIMARY = 1;
    const PRIMARY_PHONE_SECONDARY = 2;
}