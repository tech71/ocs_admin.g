<?php

/**
 * Log builder library
 * Got from `admin/application/libraries/Loges.php`
 */
class Loges
{
    /** @var \MY_Controller $CI */
    protected $CI;
    protected $companyId = '1';
    protected $userId;
    protected $module;
    protected $sub_module = '0';
    protected $title;
    protected $description;
    private $specific_title = '';
    protected $created_by;
    protected $log_type;
    protected $created_type = 1;

    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI = &get_instance();
    }

    /**
     * @param int $userId
     * @return void
     */
    public function setUserID($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserID()
    {
        return $this->userId;
    }

    /**
     * @param int $companyId
     * @return void
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param string $module
     * @return void
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }


    /**
     * @param string $sub_module
     * @return void
     */
    public function setSubModule($sub_module)
    {
        $this->sub_module = $sub_module;
    }

    /**
     * @return string
     */
    public function getSubModule()
    {
        return $this->sub_module;
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $create_by
     * @return void
     */
    public function setCreatedBy($create_by)
    {
        $this->created_by = $create_by;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param int $created_type
     * @return void
     */
    public function setCreated_type($created_type)
    {
        $this->created_type = $created_type;
    }

    /**
     * @return int
     */
    public function getCreated_type()
    {
        return $this->created_type;
    }

    /**
     * @param string $log_type
     * @return void
     */
    public function setLogType($log_type)
    {
        $this->log_type = $log_type;
    }

    /**
     * @return string
     */
    public function getLogType()
    {
        return $this->log_type;
    }

    /**
     * @param string $specific_title
     * @return void
     */
    public function setSpecific_title($specific_title)
    {
        $this->specific_title = $specific_title;
    }

    /**
     * @return string
     */
    public function getSpecific_title()
    {
        return $this->specific_title;
    }

    /**
     * Inserts new log into `tbl_logs`
     *
     * @return void
     */
    public function createLog()
    {
        $this->setSubAndModuleDetails();

        $data = array(
            'companyId' => 1,
            'userId' => $this->userId,
            'module' => $this->module,
            'sub_module' => $this->sub_module,
            'title' => $this->title,
            'specific_title' => $this->specific_title,
            'description' => $this->description,
            'created_by' => $this->created_by,
            'created_type' => $this->created_type,
            'created' => DATE_TIME,
        );

        $this->CI->db->insert(TBL_PREFIX . 'logs', $data);
    }

    /**
     * Sets module and submodule details based on current `log_type`
     *
     * @return void
     */
    public function setSubAndModuleDetails()
    {
        $this->CI->db->select(['id', 'parentId']);
        $this->CI->db->from(TBL_PREFIX . 'module_title as mt');
        $this->CI->db->where('mt.key_name', $this->log_type);
        $query = $this->CI->db->get();

        $res = $query->row();
        if (!empty($res)) {
            if ($res->parentId > 0) {
                $this->setModule($res->parentId);
                $this->setSubModule($res->id);
            } else {
                $this->setModule($res->id);
            }
        }
    }
}
