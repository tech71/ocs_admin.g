<?php

/**
 * @var \MY_Output $this
 * @var string $username
 * @var string $password_reset_url
 * @var string $contact_number
 */

?>
<!DOCTYPE html>
<html>

<head>
    <title>Email HCM</title>
</head>

<body style="padding:0px; margin:0px; background:#F6F6F6">
    <table cellpadding="0" cellspacing="0" width="700px" style="margin: 0px auto;">
        <tr>
            <td style="border-radius:15px; background: #fff; overflow: hidden;">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="background: linear-gradient(-135deg, #d77f0b, #09a275); padding: 30px;" align="center">
                            <img width="200px" src="<?= base_url("assets/img/logo.png"); ?>"/>
                        </td>
                    </tr>


                    <tr>
                        <td style="font-family:sans-serif; font-size: 14px;">
                            <table width="80%" align="center">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0"  width="70%" align="center">
                                            <tr>
                                                <td>
                                                    <p><b>Dear <?= $username ?></b></p>
                                                    <p style="padding-left:15px; margin:0px;">Thanks for contacting us. Follow the directions below to reset your password.</p>
                                                    
                                                    <p style="padding-left:15px; display:inline-block; width:100%; margin: 10px auto; text-align: center;">
                                                        <a href="<?= $password_reset_url ?>" style="cursor:pointer; text-decoration:none; margin: 0px auto;  display:table; background:#09A275; color: #fff; width:130px; padding:7px 15px; border-radius: 25px; text-align: center">Reset Password</a>
                                                    </p>
                                                    
                                                    <p style="padding-left:15px; margin:0px;">
                                                        After you click the button above, you'll be prompted to complete the following steps:
                                                    </p>
                                                    <p style="padding-left:15px; margin:0px;">
                                                        1. Enter and confirm your new password.
                                                    </p>
                                                    <p style="padding-left:15px; margin:0px;">
                                                        2.  Click "Submit"
                                                    </p>
                                                    <p style="padding-left:15px;">
                                                        If you didn't request a password reset or you feel you've
                                                        received this message in error, please call our 24/7 support
                                                        team right away at 0000 000 000. If you take no action, don't
                                                        worry — nobody will be able to change your password
                                                        without access to this email.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>

                                        <p style="margin:0px;">Sincerely,</p>
                                        <p style="margin:0px;">IT Department,</p>
                                        <p style="margin:0px;">HCM.</p>
                                        <p>
                                            <div style="width:120px; border-top:1px solid #000"></div>
                                        </p>
                                        <p style="margin:0px;"> E: tom12345@hcm.com</p>
                                        <p style="margin:0px;"> P: (03) 9896 2468</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table width="80%" align="center" style="border-top:1px solid #09A275; padding:15px 0px; margin-top: 15px; font-family: sans-serif">
                            <tr>
                                <td align="right" style="border-right:1px solid #09A275; padding-right:30px; ">hcm.com.au</b></td>
                                <td align="left" style="padding-left:30px;">  <b>(03) 9896 2468</b></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="padding-top:10px; font-size:14px;">660 Canterbury Rd, Surrey Hills VIC 3127, Australia</td>
                            </tr>
                            </table> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>