<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Common_model extends \MY_Model
{
    /**
     * Get list of states
     *
     * @return array Will return list of items, each with these properties
     * - `label: string` - state code (eg VIC, QLD)
     * - `value: int` - state id 
     */
    public function get_states()
    {
        $results = $this->db
            ->select(["name", "id"])
            ->get(TBL_PREFIX . "state")
            ->result_array();

        $return = [];
        foreach ($results as $result) {
            $return[] = [
                "label" => $result["name"],
                "value" => (int) $result["id"],
            ];
        }
        
        return $return;
    }
}