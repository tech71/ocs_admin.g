<?php

use app\entities\FinanceManualInvoice;
use app\entities\Notification;
use \Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Data-access object to supply data to dashboard page
 * 
 * @property-read \CI_DB_mysqli_driver $db
 */
class DashboardModel extends \CI_Model
{
    /**
     * Fetch all contents of `tbl_notications`. 
     * Results will be sorted by date
     *
     * @param string $organisationID
     * @return array
     */
    public function findAllLatestUpdates($organisationID)
    {
        /**
         * @var \CI_DB_mysqli_result $query
         * @var Notification[] $results
         */

        $userType = Notification::USER_TYPE_ORGANISATION;
        $sql = "SELECT * 
        FROM tbl_notification n 
        WHERE n.user_type = ? 
        AND n.userId = ?
        ORDER BY n.created DESC
        LIMIT 1000";
        $query = $this->db->query($sql, [$userType, $organisationID]);
        $results = $query->result(Notification::class);

        /** @param Notification $result */
        $mappedResults = array_map(function($result) {
            return [
                "update" => $result->title,
                "date" => $result->created,
                "_links" => new \stdClass // need to encode as JS object instead of array
            ];
        }, $results);

        return $mappedResults;
    }


    /**
     * Finds all invoices by the given organisation ID. 
     * This will query from `tbl_finance_invoice`. 
     * Each resulting items will contain links to invoice
     * 
     * @param string $organisationId
     * @return array[]
     * 
     * @todo The property `_links` in each result not implemented yet
     */
    public function findAllInvoices($organisationId)
    {
        $bookedByType = FinanceManualInvoice::BOOKED_BY_ORGANISATION;

        $sql = "SELECT
            i.*, 
            o.name AS invoice_name 
        FROM tbl_finance_invoice i
        JOIN tbl_organisation o
        ON o.id = i.invoice_for
        AND i.booked_by = ?
        WHERE i.invoice_for = ?
        ORDER BY invoice_date DESC
        LIMIT 1000";

        /**
         * @var \CI_DB_mysqli_result $query
         */
        $query = $this->db->query($sql, [$bookedByType, $organisationId]);
        $resultArray = $query->result_array();

        $mappedResults = array_map(function($result) {
            return [
                "invoiceName" => Arr::get($result, "invoiceName"),
                "dueDate" => Arr::get($result, "pay_by"),
                "_links" => new \stdClass // will be encoded as JS object instead of array
            ];

        }, $resultArray);

        return $mappedResults;
    }


    /**
     * @todo Unimplemented
     *
     * @param string $organisationID
     * @return array[]
     */
    public function findAllFMSFeedbacks($organisationID)
    {
        return [];
    }
}