<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Object that can be used to fetch data from db tables
 */
class Basic_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();    // Load database
    }

    /**
     * Function to return a single row
     * 
     * @param string $table_name - name of the table to fetch data from
     * @param array $columns - column names to be selected
     * @param array $id_array - key => value pair to be used as where clause
     */
    public function get_row($table_name = '', $columns = array(), $id_array = array()) {
        if (!empty($columns)) :
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($id_array)) :
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get(TBL_PREFIX . $table_name);

        //echo last_query();
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

    /**
     * Function to return multiple rows
     * 
     * @param string $table - name of the table to fetch data from
     * @param string $column - column names to be selected
     * @param array $where - array to be used in the where clause
     */
    function get_record_where($table, $column = '', $where = '') {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from(TBL_PREFIX . $table);
        if ($where != '') {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Function to return multiple rows
     * 
     * @param string $table - name of the table to fetch data from
     * @param string $column - column names to be selected
     * @param array $where - array to be used in the where clause
     * @param array $whereLike - array to be used in the where clause
     */
    function getRecordWhereLike($table, $column = '', $where = [], $whereLike = []) {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }

        $this->db->from(TBL_PREFIX . $table);

        if(!empty($where && is_array($where))) {
            $this->db->where($where);
        }

        if(!empty($whereLike) && is_array($whereLike)) {
            $this->db->like($whereLike);
        }
        
        $query = $this->db->get();
        // dd($this->db->last_query());
        return $query->result();
    }
    
    /** 
     * Function for getting records from table with where condition and order by
     * 
     * @param string $table - name of the table to fetch data from
     * @param string $column - column names to be selected
     * @param string $where - string to be used in the where clause
     * @param string $orderby - string to be used in the order clause
     * @param string $direction - string to be used as order direction
     */
    function get_record_where_orderby($table, $column = '', $where = '', $orderby = '', $direction = 'ASC') {
        if ($column != '') {
            $this->db->select($column);
        } else {
            $this->db->select('*');
        }
        $this->db->from(TBL_PREFIX . $table);
        if ($where != '') {
            $this->db->where($where);
        }
        if ($orderby != '') {
            $this->db->order_by($orderby, $direction);
        }

        $query = $this->db->get();
        return $query->result();
    }

    /** Function for inserting records
     * 
     * @param string $table - name of the table to insert data into
     * @param string $data - the data to be inserted
     * @param boolean $multiple - indicates whether multiple records need to be inserted
    */
    function insert_records($table, $data, $multiple = FALSE) {
        if ($multiple) {
            $this->db->insert_batch(TBL_PREFIX . $table, $data);
        } else {
            $this->db->insert(TBL_PREFIX . $table, $data);
        }

        return $this->db->insert_id();
    }

    /** Function for updating records
     * 
     * @param string $table - name of the table to update data in
     * @param string $data - the data to be updated
     * @param string $where - string to be used in the where clause
    */
    function update_records($table, $data, $where) {
        $this->db->where($where);
        $this->db->update(TBL_PREFIX . $table, $data);
        return $this->db->affected_rows();
    }

    /** Function for deleting records
     * 
     * @param string $table - name of the table to delete data from
     * @param string $where - string to be used in the where clause
    */
    function delete_records($table, $where) {
        $this->db->delete(TBL_PREFIX . $table, $where);
        if (!$this->db->affected_rows()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Function to return a single row
     * 
     * @param string $table_name - name of the table to fetch data from
     * @param array $columns - column names to be selected
     * @param array $id_array - key => value pair to be used as where clause
     * @param array $order_by - columns to sort the result by
     * @param string $result_type
     */
    public function get_result($table_name = '', $id_array = '', $columns = array(), $order_by = array(), $result_type = '') {
        if (!empty($columns)) :
            $all_columns = implode(",", $columns);
            $this->db->select($all_columns);
        endif;
        if (!empty($order_by)) :
            $this->db->order_by($order_by[0], $order_by[1]);
        endif;

        if (!empty($id_array)) :
            foreach ($id_array as $key => $value) {
                $this->db->where($key, $value);
            }
        endif;
        $query = $this->db->get(TBL_PREFIX . $table_name);
        if ($query->num_rows() > 0)
            return !empty($result_type) ? $query->result_array() : $query->result();
        else
            return FALSE;
    }

    /**
     * Function to insert or update data in batch
     * @param string $action - indicates whether to insert or update data
     * @param string $table_name - name of table
     * @param array $data  - the data to insert/update
     * @param string $update_base_column_key
    */
    public function insert_update_batch($action = 'insert', $table_name = '', $data = [], $update_base_column_key = '') {
        if ($action == 'insert' && !empty($table_name) && !empty($data) && is_array($data)) {
            $this->db->insert_batch(TBL_PREFIX . $table_name, $data);
            return true;
        } elseif ($action == 'update' && !empty($table_name) && !empty($update_base_column_key) && !empty($data) && is_array($data)) {
            $this->db->update_batch(TBL_PREFIX . $table_name, $data, $update_base_column_key);
            return true;
        } else {
            return false;
        }
    }

}
