<?php

use app\entities\Organisation;
use app\entities\OrganisationAddress;
use app\entities\OrganisationAllContact;
use app\entities\OrganisationAllContactEmail;
use app\entities\OrganisationAllContactPhone;
use app\entities\OrganisationPhone;
use app\entities\OrganisationRequirement;
use Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Object to supply data for an Organisation model
 * 
 * @property-read \CI_DB_mysqli_driver $db
 * @property-read \Basic_model $Basic_model
 */
class Org_model extends \MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Basic_model");
    }


    const CONTACT_TYPES = [
        "1" => 'Support Coordinator',
        "2" => 'Member',
        "3" => 'Key Contact',
        "4" => 'Billing',
    ];

    CONST ALLOWED_SECONDARY_PHONE_NUMBERS = 2;
    CONST ALLOWED_SECONDARY_EMAILS = 2;
    CONST PHONE_NUMBER_PATTERN = '/^(?=.{8,18}$)(\(?\+?[0-9]{1,3}\)?)([ ]{0,1})?[0-9_\- \(\)]{8,18}$/';

    /**
     * Function to fetch and return organization profile
    */
    public function get_organisation_profile($objOrg) {
        $orgId = $objOrg->getId();
        $orgParentId = $objOrg->getParent_org();
        $org_ary = array();

        $tbl_1 = TBL_PREFIX . 'organisation';
        $tbl_2 = TBL_PREFIX . 'organisation_address';


        $dt_query = $this->db->select(array($tbl_1 . '.id', $tbl_1 . '.name', $tbl_1 . '.website', $tbl_1 . '.companyId', $tbl_1 . '.enable_portal_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.logo_file', $tbl_1 . '.payroll_tax', $tbl_1 . '.gst', $tbl_1 . '.abn', $tbl_1 . '.status', $tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename', $tbl_1 . '.booking_status', $tbl_1 . '.booking_date'));

        $this->db->from($tbl_1);
        $this->db->join($tbl_2, 'tbl_organisation_address.organisationId = tbl_organisation.id', 'left');
        $this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_address.state', 'left');

        if (!empty($orgParentId)) {
            $sWhere = array($tbl_1 . '.id' => $orgParentId, $tbl_1 . '.parent_org' => $orgId);
        } else {
            $sWhere = array($tbl_1 . '.id' => $orgId);
        }

        $sWhere = array($tbl_1 . '.id' => $orgId);
        $this->db->where($sWhere, null, false);
        $query = $this->db->get();

        $x = $query->row_array();

        if (!empty($x)) {
            if (!empty($orgParentId)) {
                $this->db->select('name');
                $this->db->where('id=', $orgParentId);
                $query_sub_org = $this->db->get('tbl_organisation');
                $dataResult = $query_sub_org->row_array();

                $org_ary['basic_detail']['parent_org'] = $dataResult['name'];
                $org_ary['basic_detail']['type'] = 'sub_org';
                $org_ary['basic_detail']['parent_id'] = $orgParentId;
                $org_ary['basic_detail']['page_title'] = $dataResult['name'] . ' - ' . $x['name'];

                $this->db->select('id');
                $this->db->where('organisationId', $orgId);
                $query = $this->db->get('tbl_organisation_site');

                $site_count = $query->num_rows();
                $org_ary['basic_detail']['site_count'] = $site_count;
            } else {
                $this->db->select('id');
                $this->db->where('id!=', $orgId);
                $this->db->where('archive', '0');
                $this->db->where('parent_org=', $orgId);
                $query_sub_org = $this->db->get('tbl_organisation');
                $sub_org_count = $query_sub_org->num_rows();

                $this->db->select('id');
                $this->db->where('organisationId', $orgId);
                $query = $this->db->get('tbl_organisation_site');
                $site_count = $query->num_rows();

                $org_ary['basic_detail']['site_count'] = $site_count;
                $org_ary['basic_detail']['page_title'] = $x['name'];
                $org_ary['basic_detail']['sub_org_count'] = $sub_org_count;
                $org_ary['basic_detail']['type'] = 'org';
            }


            {
                $org_ary['basic_detail']['primary_address'] = $x['street'] . ', ' . $x['city'] . ', ' . $x['postal'] . ' ' . $x['statename'];
            }

            $xx = $this->basic_model->get_result('organisation_email', array('organisationId' => $orgId, 'archive' => '0'), array('email', 'primary_email'), array('primary_email', 'ASC'));
            $org_ary['basic_detail']['OrganisationEmail'] = isset($xx) && !empty($xx) ? $xx : array();
            $yy = $this->basic_model->get_result('organisation_phone', array('organisationId' => $orgId, 'archive' => '0'), array('phone', 'primary_phone'), array('primary_phone', 'ASC'));
            $org_ary['basic_detail']['OrganisationPh'] = isset($yy) && !empty($yy) ? $yy : array();

            $ocsid = $x['ocs_id'];
            $org_ary['basic_detail']['companyId'] = $x['companyId'];
            $org_ary['basic_detail']['abn'] = $x['abn'];
            $org_ary['basic_detail']['name'] = $x['name'];
            $org_ary['basic_detail']['ocs_id'] = $x['ocs_id'];

            if(!empty($x['logo_file']) && file_exists(realpath(ORG_UPLOAD_PATH . DIRECTORY_SEPARATOR . $ocsid) . DIRECTORY_SEPARATOR . $x['logo_file'])) {
                $profile_img = ORG_UPLOAD_RELATIVE_PATH.$ocsid.DIRECTORY_SEPARATOR.$x['logo_file'];
                $imgName = basename($profile_img);
            } else {
                $profile_img = '/assets/images/Members_icons/boy.svg';
                $imgName = 'boy.svg';
            }

            $org_ary['basic_detail']['logo_file'] = $profile_img;
            $org_ary['basic_detail']['logo_file_name'] = $imgName;
            $org_ary['basic_detail']['enable_portal_access'] = $x['enable_portal_access'];
            $org_ary['basic_detail']['status'] = $x['status'];
            $org_ary['basic_detail']['booking_status'] = isset($x['booking_status']) && $x['booking_status'] == '1' ? TRUE : FALSE;
            $org_ary['basic_detail']['booking_date'] = isset($x['booking_date']) && $x['booking_date'] != '0000-00-00' ? $x['booking_date'] : '';

            $org_ary['basic_detail']['street'] = $x['street'];
            $org_ary['basic_detail']['city'] = $x['city'];
            $org_ary['basic_detail']['postal'] = $x['postal'];
            $org_ary['basic_detail']['statename'] = $x['statename'];
            $org_ary['basic_detail']['state'] = $x['state'];
            $org_ary['basic_detail']['website'] = $x['website'];
            $org_ary['basic_detail']['payroll_tax'] = $x['payroll_tax'];
            $org_ary['basic_detail']['gst'] = $x['gst'];
            $org_ary['basic_detail']['city'] = array('value' => $x['city'], 'label' => $x['city']);

            // Fetch contact details
            $org_ary['org_contacts'] = [];
            $contactsQuery = "
                SELECT organisation_id, contact_id, first_name, last_name, position, department, contact_type,
                GROUP_CONCAT(DISTINCT phone_primary) AS phone_primary,
                GROUP_CONCAT(DISTINCT phone_secondary) AS phone_secondary,
                GROUP_CONCAT(DISTINCT email_primary) AS email_primary,
                GROUP_CONCAT(DISTINCT email_secondary) AS email_secondary
                FROM (
                    SELECT con.organisationId as organisation_id, con.name AS first_name, con.id as contact_id,
                        con.lastname AS last_name, con.position, con.department, con.type AS contact_type,
                        CASE WHEN cp.primary_phone = 1 THEN `cp`.`phone` ELSE null END AS phone_primary,
                        CASE WHEN cp.primary_phone = 2 THEN `cp`.`phone` ELSE null END AS phone_secondary,
                        CASE WHEN ce.primary_email = 1 THEN `ce`.`email` ELSE null END AS email_primary,
                        CASE WHEN ce.primary_email = 2 THEN `ce`.`email` ELSE null END AS email_secondary
                    FROM `tbl_organisation_all_contact` AS `con`
                    LEFT JOIN `tbl_organisation_all_contact_email` AS `ce` ON `con`.`id` = `ce`.`contactId`
                    LEFT JOIN `tbl_organisation_all_contact_phone` AS `cp` ON `con`.`id` = `cp`.`contactId`
                    WHERE `con`.`organisationid` = ?
                    AND `ce`.`archive` = ?
                    AND `cp`.`archive` = ?) AS aa
                GROUP BY organisation_id, contact_id, first_name, last_name, position, department, contact_type";
    
            $contactsCommand = $this->db->query($contactsQuery, [$orgId, 0, 0]);

            $output = $contactsCommand->result();

            // Check if there are more than one secondary phone/email. If so, extract them into separate properties (i.e.; phone_secondary_1, phone_secondary_2, email_secondary_1, email_secondary_2)
            if(!empty($output)) {
                foreach($output as $ind => $contact) {
                    $secondaryPhNumbers = !empty($contact->phone_secondary) ? explode(",", $contact->phone_secondary) : [];
                    $secondaryEmails = !empty($contact->email_secondary) ? explode(",", $contact->email_secondary) : [];

                    for($i = 1; $i <= self::ALLOWED_SECONDARY_PHONE_NUMBERS; $i++) {
                        $output[$ind]->{"phone_secondary_$i"} = $secondaryPhNumbers[$i -1] ?? null;
                        $output[$ind]->{"email_secondary_$i"} = $secondaryEmails[$i -1] ?? null;
                    }

                    unset($contact->phone_secondary, $contact->email_secondary);
                }
            }

            $org_ary['org_contacts'] = $contactsCommand->result();

            if(!empty($org_ary['org_contacts'])) {
                foreach($org_ary['org_contacts'] as $ind => $contact) {
                    $tmp = new stdClass();
                    if(!empty($contact->contact_type)) {
                        $tmp->value = $contact->contact_type;
                        $tmp->label = self::CONTACT_TYPES[$contact->contact_type];
                    }
                    $org_ary['org_contacts'][$ind]->contact_type = $tmp;
                }
            }

            //Prepare 'contact types' which will be used by the front end as {label: , value: } objects to populate react-select-plus dropdown
            $contactTypes = [];
            $contactTypes = array_map(function($elemArr1, $elemArr2) {
                $option = new StdClass();
                $option->label = $elemArr2;
                $option->value = $elemArr1;

                return $option;
            }, array_keys(self::CONTACT_TYPES), self::CONTACT_TYPES);

            $org_ary['contact_types'] = $contactTypes;

            return array('status' => true, 'data' => $org_ary);
        }
        else {
            return array('status' => false);
        }
    }


    /**
     * Search parent organisation by name
     *
     * @param string $name
     * @return array Returns list of results. Each result item will have 
     * - `label: string` which maps to the name of organisation
     * - `value: int` which maps to ID of organisation
     */
    public function get_parent_org_name($name)
    {
        $cmd = $this->db
            ->or_like('name', $name)
            ->where('archive =', '0')
            ->where('status=', 1, FALSE)
            ->where('parent_org=', 0, FALSE)
            ->select(['id', 'name']);
        
        $query = $cmd->get(TBL_PREFIX . 'organisation');

        $rows = [];
        $results = $query->result();
        foreach ($results as $val) {
            $rows[] = [
                'label' => $val->name, 
                'value' => $val->id
            ];
        }

        return $rows;
    }


    /**
     * @return OrganisationRequirement[]
     */
    public function findAllOrganisationRequirements()
    {
        /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query("SELECT * FROM tbl_organisation_requirement WHERE archive = 0");
        $results = $query->result(OrganisationRequirement::class);
        return $results;
    }



    /**
     * Search site by given keyword query
     * 
     * @param string $search
     * @return array Returns list of results. Each result will have
     * - `label` The site name
     * - `id` The site id
     */
    public function get_site_org_unassign($search) 
    {
        $table = TBL_PREFIX . 'organisation_site';

        $this->db->like($table . '.site_name', $search);
        $this->db->where($table . '.archive', false); // false = 0
        $this->db->where($table . '.status', true); // true = 1
        $this->db->where($table . '.organisationId', 0);
        $this->db->select([$table . '.id as value', $table . '.site_name as label']);
        
        $results = $this->db->get($table);

        $resultData = $results->result_array();
        return $resultData;
    }

    /**
     * Update organization details
     * @param array $post_data - data sent by client
     * @return json
     */
    public function update_org($post_data) {
        $tablePhone = 'organisation_phone';
        $tableEmail = 'organisation_email';

        if (!empty($post_data)) {
            $org_id = $post_data['ocs_id'];
            $organisation_data = array(
                'website' => $post_data['website'],
                'payroll_tax' => isset($post_data['tax']) ? $post_data['tax'] : '',
                'gst' => isset($post_data['gst']) ? $post_data['gst'] : '0',
            );

            $this->basic_model->update_records('organisation', $organisation_data, array('id' => $org_id));

            /* Image upload */
            $upload_error = '';
            if ($post_data['is_upload_img']) {
                $config['upload_path'] = ORG_UPLOAD_PATH.DIRECTORY_SEPARATOR;
                $config['input_name'] = 'org-logo';
                $config['directory_name'] = $org_id;
                $config['allowed_types'] = 'jpg|jpeg|png';

                $is_upload = do_upload($config);
                // dd($org_id);

                if (isset($is_upload['error'])) {
                    $upload_error = strip_tags($is_upload['error']);
                } else {
                    $org_data = array('logo_file' => $is_upload['upload_data']['file_name']);
                    $this->basic_model->update_records('organisation', $org_data, array('id' => $org_id));
                }
            }

            $organisation_adr = [];

            $organisation_adr = array('organisationId' => $org_id,
                'street' => $post_data['street'],
                'city' => $post_data['suburb'],
                'postal' => $post_data['postcode'],
                'state' => $post_data['state']
            );

            if (!empty($organisation_adr)) {
                $this->basic_model->update_records('organisation_address', $organisation_adr, array('organisationId' => $org_id));
            }

            $insertPhones = $insertEmails = [];

            if (!empty($post_data['primary-phone'])) {
                /* First delete/archive old record and then insert new record */
                $this->basic_model->update_records('organisation_phone', array('archive' => '1'), array('organisationId' => $org_id));

                $insertPhones[] = [
                    'organisationId' => $org_id,
                    'phone' => $post_data['primary-phone'],
                    'primary_phone' => '1',
                    // 'archive' => '0'
                ];

                // $this->basic_model->insert_records('organisation_phone', $newPhoneRecord, FALSE);
            }

            if (!empty($post_data['primary-email'])) {
                /* First delete/archive old record and then insert new record */
                $this->basic_model->update_records('organisation_email', array('archive' => '1'), array('organisationId' => $org_id));

                $insertEmails[] = [
                    'organisationId' => $org_id,
                    'email' => $post_data['primary-email'],
                    'primary_email' => '1',
                    // 'archive' => '0'
                ];
                
                // $this->basic_model->insert_records('organisation_email', $newEmailRecord, FALSE);
            }

            if(!empty($post_data['secondary-phones'])) {
                $secondaryPhones = json_decode($post_data['secondary-phones']);

                if(!empty($secondaryPhones)) {
                    foreach($secondaryPhones as $phoneNo) {
                        $insertPhones[] = [
                            'organisationId' => $org_id,
                            'phone' => $phoneNo,
                            'primary_phone' => 2
                        ];
                    }
                }
            }

            if(!empty($post_data['secondary-emails'])) {
                $secondaryEmails = json_decode($post_data['secondary-emails']);

                if(!empty($secondaryEmails)) {
                    foreach($secondaryEmails as $email) {
                        $insertEmails[] = [
                            'organisationId' => $org_id,
                            'email' => $email,
                            'primary_email' => 2
                        ];
                    }
                }
            }
            // dd($insertEmails);
            if (!empty($insertPhones)) {
                $this->Basic_model->insert_records($tablePhone, $insertPhones, TRUE);
            }

            if (!empty($insertEmails)) {
                $this->Basic_model->insert_records($tableEmail, $insertEmails, TRUE);
            }

            return array('org_id' => $org_id, 'image_status' => $upload_error);
        }
    }

    /**
     * Creates organisation based on provided nested form data.
     * 
     * Note: It is recommended to wrap this in a transaction because
     * it is inserting into multiple tables
     *
     * @param array $postData
     * @return int|null
     */
    public function createOrganisation(array $postData)
    {
        // check `front-end/src/pages/sub-orgs/CreatePage.jsx` for the schema of submitted items
        $this->db->insert(Organisation::TABLE_NAME, [
            "name" => Arr::get($postData, "OrgLegalDetails.sub_org_legal_name"),
            "logo_file" => "",
            "parent_org" => Arr::get($postData, "parent_organisation", false),
            "website" => Arr::get($postData, "ContactDetails.website", ''),
            "payroll_tax" => Arr::get($postData, "BillingContactDetails.pay_roll_tax", false),
            "gst" => Arr::get($postData, "BillingContactDetails.gst", false),
            "enable_portal_access" => true,
        ]);

        $lastInsertedOrgId = $this->db->insert_id();

        $site = Arr::get($postData, "AttachAndAdd.sites.value", null);
        if ($site) {
            $attachedSites = [
                ["value" => $site]
            ];

            $this->attach_site_to_org($attachedSites, $lastInsertedOrgId);
        }

        // insert address
        $this->db->insert(OrganisationAddress::TABLE_NAME, [
            "organisationId" => $lastInsertedOrgId,
            "street" => Arr::get($postData, "ContactDetails.sub_org_address", ""),
            "city" => Arr::get($postData, "ContactDetails.city", ""),
            "postal" => Arr::get($postData, "ContactDetails.postcode", ""),
            "state" => Arr::get($postData, "ContactDetails.state", ""),
            "category" => Arr::get($postData, "ContactDetails.address_category", ""),
            "primary_address" => 1,
        ]);

        // insert requirements
        $requirements = [];
        foreach ($postData['SubOrgRequirements'] as $requirementId => $value) {
            if ($value) {
                $requirements[] = [
                    'organisationId' => $lastInsertedOrgId,
                    'requirementId' => $requirementId,
                ];
            }
        }

        if (!empty($requirements)) {
            // note: `tbl_organisation_requirements` is hardcoded string because it is a 'join' 
            // table between `tbl_organisation` and `tbl_organisation_requirement`
            $this->db->insert_batch("tbl_organisation_requirements", $requirements);
        }

        // insert org phones
        $orgPhones = [];
        $submittedPhones = Arr::get($postData, "ContactDetails.phones", []);

        foreach ($submittedPhones as $i => $submittedPhone) {
            $phone = Arr::get($submittedPhone, "value", null);
            if ($phone) {
                $isPrimary = Arr::get($submittedPhone, "is_primary", false);
                $orgPhones[] = [
                    'organisationId' => $lastInsertedOrgId,
                    'phone' => $phone,
                    'primary_phone' => $isPrimary ? '1' : '2',
                    'archive' => false,
                ];
            }
        }

        if (!empty($orgPhones)) {
            $this->db->insert_batch(OrganisationPhone::TABLE_NAME, $orgPhones);
        }

        // insert org emails
        $orgEmails = [];
        $submittedEmails = Arr::get($postData, "ContactDetails.emails", []);
        foreach ($submittedEmails as $i => $submittedEmail) {
            $email = Arr::get($submittedEmail, "value", null);
            if ($email) {
                $isPrimary = Arr::get($submittedEmail, "is_primary", false);
                $orgEmails[] = [
                    'organisationId' => $lastInsertedOrgId,
                    'email' => $email,
                    'primary_email' => $isPrimary ? '1' : '2',
                    'archive' => false
                ];
            }
        }

        if (!empty($orgEmails)) {
            $this->db->insert_batch("tbl_organisation_email", $orgEmails);
        }

        // INSERT KEY CONTACT DETAILS
        $org_key_contact_data = [
            'organisationId' => $lastInsertedOrgId,
            'name' => Arr::get($postData, "KeyContactDetails.first_name", ""),
            'lastname' => Arr::get($postData, "KeyContactDetails.last_name", ""),
            'position' => Arr::get($postData, "KeyContactDetails.position", ""),
            'department' => Arr::get($postData, "KeyContactDetails.department", ""),
            'type' => OrganisationAllContact::TYPE_KEY_CONTACT,
            'archive' => false
        ];
        
        $this->db->insert(OrganisationAllContact::TABLE_NAME, $org_key_contact_data);
        $key_contact_id = $this->db->insert_id();

        
        //  key contact emails
        $key_contact_emails = [];
        $submittedKeyContactEmails = Arr::get($postData, "KeyContactDetails.emails", []);
        foreach ($submittedKeyContactEmails as $i => $email) {
            $isPrimary = Arr::get($email, "is_primary", false);
            $key_contact_emails[] = [
                "contactId" => $key_contact_id,
                "email" => $email["value"],
                "primary_email" => $isPrimary ? 
                    OrganisationAllContactEmail::PRIMARY_EMAIL_PRIMARY :
                    OrganisationAllContactEmail::PRIMARY_EMAIL_SECONDARY,
                'archive' => false,
            ];
        }
        $this->db->insert_batch(OrganisationAllContactEmail::TABLE_NAME, $key_contact_emails);

        // insert phones
        $contact_phones = [];
        $submittedPhones = Arr::get($postData, "ContactDetails.phones", []);
        foreach ($submittedPhones as $i => $submittedPhone) {
            $phone = Arr::get($submittedPhone, "value", null);
            if ($phone) {
                $isPrimary = Arr::get($submittedPhone, "is_primary", false);
                $contact_phones[] = [
                    "contactId" => $key_contact_id,
                    "phone" => $submittedPhone["value"],
                    "primary_phone" => $isPrimary ? 
                        OrganisationAllContactPhone::PRIMARY_PHONE_PRIMARY :
                        OrganisationAllContactPhone::PRIMARY_PHONE_SECONDARY,
                    'archive' => false,
                ];
            }
        }

        if (!empty($contact_phones)) {
            $this->db->insert_batch(OrganisationAllContactPhone::TABLE_NAME, $contact_phones);
        }

        // INSERT BILL CONTACT DETAILS

        // bill contact details
        $org_bill_contact_data = [
            'organisationId' => $lastInsertedOrgId,
            'name' => Arr::get($postData, "BillingContactDetails.first_name", ''),
            'lastname' => Arr::get($postData, "BillingContactDetails.last_name", ''),
            'position' =>  Arr::get($postData, "BillingContactDetails.position", ''),
            'department' => Arr::get($postData, "BillingContactDetails.department", ''),
            'type' => OrganisationAllContact::TYPE_BILLING,
            'archive' => false, // false = 0, true = 1
        ];

        $this->db->insert(OrganisationAllContact::TABLE_NAME, $org_bill_contact_data);
        $bill_contact_id = $this->db->insert_id();


        // bill contact emails
        $organisation_billcontact_mail = [];
        $submittedBillContactEmails = Arr::get($postData, "BillingContactDetails.emails", []);
        foreach ($submittedKeyContactEmails as $i => $email) {
            $isPrimary = Arr::get($email, "is_primary", false);
            $organisation_billcontact_mail[] = [
                "contactId" => $bill_contact_id,
                "email" => $email["value"],
                "primary_email" => $isPrimary ? 
                    OrganisationAllContactEmail::PRIMARY_EMAIL_PRIMARY : 
                    OrganisationAllContactEmail::PRIMARY_EMAIL_SECONDARY,
                "archive" => false, // false = 0, true = 1
            ];
        }
        if (!empty($submittedBillContactEmails)) {
            $this->db->insert_batch(OrganisationAllContactEmail::TABLE_NAME, $organisation_billcontact_mail);
        }

        // bill contact phones
        $organisation_billcontact_ph = [];
        $submittedBillContactPhones = Arr::get($postData, "BillingContactDetails.phones", []);
        foreach ($submittedBillContactPhones as $i => $phone) {
            $isPrimary = Arr::get($phone, "is_primary", false);
            $organisation_billcontact_ph[] = [
                "contactId" => $bill_contact_id,
                "phone" => $phone["value"],
                "primary_phone" => $isPrimary ? 
                    OrganisationAllContactPhone::PRIMARY_PHONE_PRIMARY :
                    OrganisationAllContactPhone::PRIMARY_PHONE_SECONDARY,
                'archive' => false, // false = 0, true = 1
            ];
        }

        if (!empty($organisation_billcontact_ph)) {
            $this->db->insert_batch(OrganisationAllContactPhone::TABLE_NAME, $organisation_billcontact_ph);
        }

        return $lastInsertedOrgId;
    }


    /**
     * Upload submitted organisation logo via `\CI_Upload` library.
     * 
     * Logo will be uploaded to the admin portals uploads folder (specifically `admin/uploads/organisation/{orgID}`)
     * 
     * To access the errors for this upload, you will have to call `$this->upload->display_error(null, null)` from the caller.
     * For example
     * ```php
     * 
     * $orgModel = $this->OrganisationModel;
     * if ($orgModel->uploadOrganisationLogo($orgId, "logo") {
     *     $errorMessage = $this->upload->display_error(null, null);
     *     // ...
     * }
     * 
     * ```
     *
     * @param int $orgId
     * @param string $inputName
     * @return \CI_Upload|null
     */
    public function uploadOrganisationLogo($orgId, $inputName = "logo")
    {
        $tgtDirNoTrailingSlash = ORG_UPLOAD_PATH . $orgId;

        // create dir if not exists, add index.html automatically
        // based on `admin/backend/application/helpers/common_helper.php`
        if (!is_dir($tgtDirNoTrailingSlash)) {
            mkdir($tgtDirNoTrailingSlash, 0755);
            $resource = fopen($tgtDirNoTrailingSlash . DIRECTORY_SEPARATOR . "index.html", "w");
            fclose($resource);
        }

        $this->load->library("upload", [
            "upload_path" => $tgtDirNoTrailingSlash,
            "input_name" => $inputName,
            "allowed_types" => "jpg|jpeg|png",
            "max_size" => 5120,
        ]);

        $success = $this->upload->do_upload($inputName);
        if ($success) {
            return $this->upload;
        }

        return null;
    }


    /**
     * Attach sites to given org id
     *
     * @param array $siteOrgData Multidimensional array with each item containing at least the `value` property
     * eg 
     * ```php
     * $siteOrgData = [ ["value" => 1234], ["value" => 1235] ]
     * ```
     * @param int $orgId
     * @return bool
     */
    public function attach_site_to_org($siteOrgData = [], $orgId = 0) {
        $res = false;
        if (count($siteOrgData) > 0 && (int) $orgId > 0) {
            foreach ($siteOrgData as $val) {
                $val = !empty($val) && is_object($val) ? (array) $val : $val;
                if (isset($val['value']) && !empty($val['value']) && is_numeric($val['value'])) {
                    $this->Basic_model->update_records('organisation_site', ['organisationId' => $orgId], ['archive' => '0', 'organisationId' => '0', 'id' => $val['value']]);
                    $res = true;
                }
            }
        }
        return $res;
    }


    /**
     * Gets list of suborgs
     * 
     * @param mixed $objOrg 
     * Instance of `OrganisationClass\Organisation::class` which 
     * contains search keyword and pagination params
     * 
     * Note: This class is imported from outside of org portal.
     * See `ocs_admin/admin/application/Classes/organisation/Organisation.php`
     * @return array Will return associative array with:
     * - `count: int` Number of results
     * - `data: array` 
     * 
     * @todo Refactor/replicate to accept assoc array instead
     */
    public function get_sub_org($objOrg) 
    {
        $orgId = $objOrg->getId();
        $orgStatus = $objOrg->getStatus();
        $orgText_search = $objOrg->gettext_search();
        $limit = $objOrg->getLimit();
        $page = $objOrg->getPage();
        $tbl_phone = 'organisation_phone';
        $tbl_email = 'organisation_email';
        $tbl_phone_prefix = TBL_PREFIX . $tbl_phone;
        $tbl_email_prefix = TBL_PREFIX . $tbl_email;

        $tbl_1 = TBL_PREFIX . 'organisation';
        $tbl_2 = TBL_PREFIX . 'organisation_address';
        $tbl_3 = TBL_PREFIX . 'organisation_email';
        $tbl_4 = TBL_PREFIX . 'organisation_phone';
        $tbl_5 = TBL_PREFIX . 'organisation_all_contact';
        $select_column = array($tbl_1 . '.name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.status', $tbl_2 . '.id as address_id', $tbl_2 . '.street', $tbl_2 . '.city', $tbl_2 . '.postal', $tbl_2 . '.state', $tbl_2 . '.primary_address', 'tbl_state.name as statename', $tbl_3 . '.email', $tbl_4 . '.primary_phone', $tbl_4 . '.phone', $tbl_3 . '.primary_email', $tbl_5 . '.name as key_contact', $tbl_5 . '.position', $tbl_5 . '.id as all_contact_id', $tbl_3 . '.id as email_id', $tbl_4 . '.id as ph_id');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
        $this->db->from($tbl_1);
        $this->db->join($tbl_2, "tbl_organisation_address.organisationId = tbl_organisation.id AND tbl_organisation_address.primary_address = 1", "left");
        $this->db->join($tbl_3, "tbl_organisation_email.organisationId = tbl_organisation.id AND tbl_organisation_email.primary_email = 1 and tbl_organisation_email.archive='0'", "left");
        $this->db->join($tbl_4, "tbl_organisation_phone.organisationId = tbl_organisation.id AND tbl_organisation_phone.primary_phone = 1 and tbl_organisation_phone.archive='0'", "left");
        $this->db->join($tbl_5, "tbl_organisation_all_contact.organisationId = tbl_organisation.id AND tbl_organisation_all_contact.type = 3 AND tbl_organisation_all_contact.archive = '0'", "left", TRUE);
        $this->db->join("tbl_state", "tbl_state.id = tbl_organisation_address.state", "left");

        $this->db->where($tbl_1 . '.archive=', '0');
        $this->db->where($tbl_1 . '.status=', $orgStatus);
        $this->db->where($tbl_1 . '.parent_org=', $orgId);
        $this->db->where($tbl_1 . '.id!=', $orgId);

        if (!empty($orgText_search)) {
            #OCPID,Org Name,Primary Contact Name,Sub Org Name,  Head Office Address
            $this->db->group_start();
            $src_columns = array($tbl_1 . ".id", $tbl_1 . ".name", "CONCAT(tbl_organisation_all_contact.name,' ',tbl_organisation_all_contact.lastname)", $tbl_2 . ".street", $tbl_2 . ".city", $tbl_2 . ".postal", $tbl_2 . ".state",);

            for ($i = 0; $i < count($src_columns); $i++) {
                $column_search = $src_columns[$i];
                if (strstr($column_search, "as") !== false) {
                    $serch_column = explode(" as ", $column_search);
                    $this->db->or_like($serch_column[0], $orgText_search);
                } else {
                    $this->db->or_like($column_search, $orgText_search);
                }
            }
            $this->db->group_end();
        }

        $this->db->group_by($tbl_1 . '.id');
        $this->db->limit($limit, ($page * $limit));
        $query = $this->db->get();
        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }
        $x = $query->result_array();
        $org_ary = array();
        if (!empty($x)) {
            foreach ($x as $key => $value) {
                $temp['name'] = $value['name'];
                $temp['status'] = $value['status'];
                $temp['ocs_id'] = $value['ocs_id'];
                $temp['key_contact'] = $value['key_contact'] . '-' . $value['position'];
                $temp['key_contact_label'] = $value['key_contact'];
                $temp['position'] = $value['position'];

                $temp['address_id'] = $value['address_id'];
                $temp['all_contact_id'] = $value['all_contact_id'];
                $temp['email_id'] = $value['email_id'];
                $temp['ph_id'] = $value['ph_id'];

                if (isset($value['primary_address'])) {
                    $temp['primary_address'] = $value['street'] . ', ' . $value['city'] . ', ' . $value['postal'] . ' ' . $value['statename'];
                    $temp['street'] = $value['street'];
                    $temp['city'] = array('value' => $value['city'], 'label' => $value['city']);
                    $temp['postal'] = $value['postal'];
                    $temp['state'] = $value['state'];
                }

                if (isset($value['primary_phone'])) {
                    $temp['primary_phone'] = $value['phone'];
                }

                if (isset($value['primary_email'])) {
                    $temp['primary_email'] = $value['email'];
                }
                /* Get organisatin phone no */
                $zPhone = $this->Basic_model->get_result($tbl_phone, array($tbl_phone_prefix . '.organisationId' => $value['ocs_id'], $tbl_phone_prefix . '.archive' => '0'), $columns = array($tbl_phone_prefix . '.phone', $tbl_phone_prefix . '.primary_phone', $tbl_phone_prefix . '.id'));
                if (!empty($zPhone)) {
                    $ph_temp = array();
                    foreach ($zPhone as $key => $valPh) {
                        $ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
                    }
                    $temp['OrganisationPh'] = $ph_temp;
                }

                /* Get organisatin email no */
                $zMail = $this->Basic_model->get_result($tbl_email, array($tbl_email_prefix . '.organisationId' => $value['ocs_id'], $tbl_email_prefix . '.archive' => '0'), $columns = array($tbl_email_prefix . '.email', $tbl_email_prefix . '.primary_email', $tbl_email_prefix . '.id'));
                if (!empty($zMail)) {
                    $email_temp = array();
                    foreach ($zMail as $key => $valmail) {
                        $email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
                    }
                    $temp['OrganisationEmail'] = $email_temp;
                }
                $org_ary[] = $temp;
            }
        }
        return ['count' => $dt_filtered_total, 'data' => $org_ary];
    }

    /**
     * Update a contact of an organization 
     */
    public function add_org_contact($reqData, $contactId) {
        $tablePhone = 'organisation_all_contact_phone';
        $tableEmail = 'organisation_all_contact_email';

        if (!empty($reqData)) {
            /* First delete old record and then insert new record */
            $this->Basic_model->update_records($tablePhone, array('archive' => '1'), array('contactId' => $contactId));
            $this->Basic_model->update_records($tableEmail, array('archive' => '1'), array('contactId' => $contactId));

            $insertPhones = $insertEmails = [];

            if(!empty($reqData['primary-phone'])) {
                $insertPhones[] = [
                    'contactId' => $contactId,
                    'phone' => $reqData['primary-phone'],
                    'primary_phone' => 1
                ];
            }

            if(!empty($reqData['secondary-phones'])) {
                $secondaryPhones = json_decode($reqData['secondary-phones']);

                if(!empty($secondaryPhones)) {
                    foreach($secondaryPhones as $phoneNo) {
                        $insertPhones[] = [
                            'contactId' => $contactId,
                            'phone' => $phoneNo,
                            'primary_phone' => 2
                        ];
                    }
                }
            }

            if(!empty($reqData['primary-email'])) {
                $insertEmails[] = [
                    'contactId' => $contactId,
                    'email' => $reqData['primary-email'],
                    'primary_email' => 1
                ];
            }

            if(!empty($reqData['secondary-emails'])) {
                $secondaryEmails = json_decode($reqData['secondary-emails']);

                if(!empty($secondaryEmails)) {
                    foreach($secondaryEmails as $email) {
                        $insertEmails[] = [
                            'contactId' => $contactId,
                            'email' => $email,
                            'primary_email' => 2
                        ];
                    }
                }
            }

            if (!empty($insertPhones)) {
                $this->Basic_model->insert_records($tablePhone, $insertPhones, TRUE);
            }

            if (!empty($insertEmails)) {
                $this->Basic_model->insert_records($tableEmail, $insertEmails, TRUE);
            }
        }
    }

}
