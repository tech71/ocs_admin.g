<?php

defined('BASEPATH') or exit('No direct script access allowed');

use \Illuminate\Support\Arr;

/**
 * Data access object for accessing and writing data to `tbl_organisation`
 */
class AccountModel extends \MY_Model
{
    const TBL_ORGANISATION = "tbl_organisation";
    const TBL_ORGANISATION_EMAIL = "tbl_organisation_email";

    /**
     * Finds user from `tbl_organisation` by username and password
     *
     * @param string $username
     * @param string $password
     * @return array|null
     */
    public function findOrganisationByCredentials($username, $password)
    {
        $table = self::TBL_ORGANISATION;

        // get by username
        /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query("SELECT * FROM {$table} WHERE username = ? LIMIT 1", [$username]);
        $firstRow = $query->row_array();

        if (!$firstRow) {
            return null;
        }

        // check password
        $passwordHashed = Arr::get($firstRow, "password", null);
        if ($passwordHashed && password_verify($password, $passwordHashed)) {
            return $firstRow;
        }

        return null;

    }


    /**
     * Check if password reset token exists
     *
     * @param string $token The JWT password reset token
     * @return array|null Returns array if found or null if empty
     */
    public function findOrganisationPasswordResetToken($token)
    {
        $table = self::TBL_ORGANISATION;

        /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query("SELECT * FROM {$table} WHERE password_reset_token = ?", [$token]);
        $result = $query->first_row('array');

        if (!$result) {
            return null;
        }

        return $result;
    }


    /**
     * Check if given username has updated his/her password.

     * @param array|object $organizationDetails
     * @return boolean
     */
    public function isFirstTimeLogin($organizationDetails)
    {
        $table = self::TBL_ORGANISATION;
        $array = Arr::only($organizationDetails, ["id", "username"]);

       /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query("SELECT is_first_time_login FROM {$table} WHERE id = ? AND username = ?", [$array['id'], $array['username']]);
        if ($result = $query->first_row("array")) {
            return !!Arr::get($result, "is_first_time_login");
        }

        return true;
    }


    /**
     * Saves password reset token to db
     *
     * @param string $resetToken
     * @param int $organisationId
     * @return bool
     */
    public function savePasswordResetToken($resetToken, $organisationId)
    {
        return $this->db->update(self::TBL_ORGANISATION, 
            ["password_reset_token" => $resetToken], 
            ["id" => $organisationId]
        );
    }

    
    /**
     * Resets organisation's password and removes password reset token
     *
     * @param string $hashedPassword Password must be hashed via `password_hash` function
     * @param int $organisationId
     * @param string $token
     * @return bool
     */
    public function resetPassword($hashedPassword, $organisationId, $token)
    {
        $condition = [
            "id" => $organisationId,
            "password_reset_token" => $token
        ];

        $isPasswordUpdated = $this->db->update(self::TBL_ORGANISATION, [
            "password" => $hashedPassword,
            "password_reset_token" => null,
        ], $condition);

        return $isPasswordUpdated;
    }


    /**
     * Finds organisation by email address. Will return a single result or null if not found
     *
     * @param string $email
     * @return array|null
     */
    public function findOrganisationByEmail($email)
    {
        $orgTable = self::TBL_ORGANISATION;
        $orgEmailTable = self::TBL_ORGANISATION_EMAIL;

        $sql = "SELECT 
            o.*, 
            e.email
        FROM {$orgTable} o
        JOIN {$orgEmailTable} e
        ON o.id = e.organisationId
        WHERE email = ?
        LIMIT 1";

        /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query($sql, [$email]);
        $firstRow = $query->first_row('array');
        if (!$firstRow) {
            return null;
        }

        return $firstRow;
    }


    /**
     * Change password reset token by given ID
     *
     * @param string $resetToken
     * @param int $organisationId
     * @return bool
     */
    public function updatePasswordResetToken($resetToken, $organisationId)
    {
        $condition = ["id" => $organisationId];
        $result = $this->db->update(self::TBL_ORGANISATION, ["password_reset_token" => $resetToken], $condition);
        return $result;
    }


    /**
     * Updates the password of the organisation with given ID. 
     * 
     * Also it will set the `is_first_time_login` field to `false` 
     * to indicate that the user has no longer accessing the system for 
     * the first time
     *
     * @param int $organisationId
     * @param string $newHashedPassword
     * @return boolean
     */
    public function updatePassword($organisationId, $newHashedPassword)
    {
        $condition = ["id" => $organisationId];
        $result = $this->db->update(self::TBL_ORGANISATION, [
            "password" => $newHashedPassword,
            "is_first_time_login" => false
        ], $condition);

        return $result;
    }
}
