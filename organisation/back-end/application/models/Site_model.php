<?php

use app\entities\OrganisationSite;
use app\entities\OrganisationSiteEmail;
use app\entities\OrganisationSitePhone;
use app\entities\OrganisationSiteContact;
use app\entities\OrganisationSiteContactEmail;
use app\entities\OrganisationSiteContactPhone;
use Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Object to supply data for an Organisation model
 * 
 * @property-read \CI_DB_mysqli_driver $db
 * @property-read \Basic_model $Basic_model
 */
class Site_model extends \MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Basic_model");
	}

	/**
	 * Creates organisation based on provided nested form data.
	 * 
	 * Note: It is recommended to wrap this in a transaction because
	 * it is inserting into multiple tables
	 *
	 * @param array $postData
	 * @return int|null
	 */
	public function createSite(array $postData)
	{
		# adding the site information into "tbl_organisation_site" table
		$this->db->insert(OrganisationSite::TABLE_NAME, [
			"site_name" => Arr::get($postData, "SiteDetails.title"),
			"organisationId" => Arr::get($postData, "parent_organisation"),
			"street" => Arr::get($postData, "SiteDetails.site_address"),
			"city" => Arr::get($postData, "SiteDetails.city"),
			"state" => Arr::get($postData, "SiteDetails.state"),
			"postal" => Arr::get($postData, "SiteDetails.postcode"),
			"abn" => Arr::get($postData, "SiteDetails.abn"),
			"archive" => 0,
			"status" => 1,
			"enable_portal_access" => 0,
			"created" => date("Y-m-d H:i:s")
		]);

		$lastInsertedSiteId = $this->db->insert_id();

		# inserting site phones
		$sitePhones = [];
		$submittedPhones = Arr::get($postData, "SiteDetails.phones", []);
		foreach ($submittedPhones as $i => $submittedPhone) {
			$phone = Arr::get($submittedPhone, "value", null);
			if ($phone) {
				$isPrimary = Arr::get($submittedPhone, "is_primary", false);
				$sitePhones[] = [
					'siteId' => $lastInsertedSiteId,
					'phone' => $phone,
					'primary_phone' => $isPrimary ? '1' : '2',
					'archive' => false,
				];
			}
		}
		if (!empty($sitePhones)) {
			$this->db->insert_batch(OrganisationSitePhone::TABLE_NAME, $sitePhones);
		}

		# inserting site emails
		$siteEmails = [];
		$submittedEmails = Arr::get($postData, "SiteDetails.emails", []);
		foreach ($submittedEmails as $i => $submittedEmail) {
			$email = Arr::get($submittedEmail, "value", null);
			if ($email) {
				$isPrimary = Arr::get($submittedEmail, "is_primary", false);
				$siteEmails[] = [
					'siteId' => $lastInsertedSiteId,
					'email' => $email,
					'primary_email' => $isPrimary ? '1' : '2',
					'archive' => false
				];
			}
		}
		if (!empty($siteEmails)) {
			$this->db->insert_batch(OrganisationSiteEmail::TABLE_NAME, $siteEmails);
		}

		# inserting key contacts
		$orgsite_key_contact_data = [
			'siteId' => $lastInsertedSiteId,
			'firstname' => Arr::get($postData, "KeyContactDetails.first_name", ""),
			'lastname' => Arr::get($postData, "KeyContactDetails.last_name", ""),
			'position' => Arr::get($postData, "KeyContactDetails.position", ""),
			'department' => Arr::get($postData, "KeyContactDetails.department", ""),
			'type' => OrganisationSiteContact::TYPE_KEY_CONTACT,
			'archive' => false
		];
		$this->db->insert(OrganisationSiteContact::TABLE_NAME, $orgsite_key_contact_data);
		$key_contact_id = $this->db->insert_id();
		
		# inserting key contact emails
		$key_contact_emails = [];
		$submittedKeyContactEmails = Arr::get($postData, "KeyContactDetails.emails", []);
		foreach ($submittedKeyContactEmails as $i => $email) {
			$isPrimary = Arr::get($email, "is_primary", false);
			$key_contact_emails[] = [
				"contactId" => $key_contact_id,
				"email" => $email["value"],
				"primary_email" => $isPrimary ? 
					OrganisationSiteContactEmail::PRIMARY_EMAIL_PRIMARY :
					OrganisationSiteContactEmail::PRIMARY_EMAIL_SECONDARY,
				'archive' => false,
			];
		}
		$this->db->insert_batch(OrganisationSiteContactEmail::TABLE_NAME, $key_contact_emails);

		# inserting key contact phones
		$contact_phones = [];
		$submittedPhones = Arr::get($postData, "KeyContactDetails.phones", []);
		foreach ($submittedPhones as $i => $submittedPhone) {
			$phone = Arr::get($submittedPhone, "value", null);
			if ($phone) {
				$isPrimary = Arr::get($submittedPhone, "is_primary", false);
				$contact_phones[] = [
					"contactId" => $key_contact_id,
					"phone" => $submittedPhone["value"],
					"primary_phone" => $isPrimary ? 
						OrganisationSiteContactPhone::PRIMARY_PHONE_PRIMARY :
						OrganisationSiteContactPhone::PRIMARY_PHONE_SECONDARY,
					'archive' => false,
				];
			}
		}
		if (!empty($contact_phones)) {
			$this->db->insert_batch(OrganisationSiteContactPhone::TABLE_NAME, $contact_phones);
		}

		# inserting billing contacts
		$orgsite_bill_contact_data = [
			'siteId' => $lastInsertedSiteId,
			'firstname' => Arr::get($postData, "BillingContactDetails.first_name", ''),
			'lastname' => Arr::get($postData, "BillingContactDetails.last_name", ''),
			'position' =>  Arr::get($postData, "BillingContactDetails.position", ''),
			'department' => Arr::get($postData, "BillingContactDetails.department", ''),
			'type' => OrganisationSiteContact::TYPE_BILLING,
			'archive' => false, // false = 0, true = 1
		];
		$this->db->insert(OrganisationSiteContact::TABLE_NAME, $orgsite_bill_contact_data);
		$bill_contact_id = $this->db->insert_id();


		# inserting billing contacts emails
		$bill_contact_emails = [];
		$submittedBillContactEmails = Arr::get($postData, "BillingContactDetails.emails", []);
		foreach ($submittedBillContactEmails as $i => $email) {
			$isPrimary = Arr::get($email, "is_primary", false);
			$bill_contact_emails[] = [
				"contactId" => $bill_contact_id,
				"email" => $email["value"],
				"primary_email" => $isPrimary ? 
				OrganisationSiteContactEmail::PRIMARY_EMAIL_PRIMARY : 
				OrganisationSiteContactEmail::PRIMARY_EMAIL_SECONDARY,
				"archive" => false, // false = 0, true = 1
			];
		}
		if (!empty($bill_contact_emails)) {
			$this->db->insert_batch(OrganisationSiteContactEmail::TABLE_NAME, $bill_contact_emails);
		}

		# inserting billing contacts phones
		$bill_contact_phones = [];
		$submittedBillContactPhones = Arr::get($postData, "BillingContactDetails.phones", []);
		foreach ($submittedBillContactPhones as $i => $phone) {
			$isPrimary = Arr::get($phone, "is_primary", false);
			$bill_contact_phones[] = [
				"contactId" => $bill_contact_id,
				"phone" => $phone["value"],
				"primary_phone" => $isPrimary ? 
				OrganisationSiteContactPhone::PRIMARY_PHONE_PRIMARY :
				OrganisationSiteContactPhone::PRIMARY_PHONE_SECONDARY,
				'archive' => false, // false = 0, true = 1
			];
		}

		if (!empty($bill_contact_phones)) {
			$this->db->insert_batch(OrganisationSiteContactPhone::TABLE_NAME, $bill_contact_phones);
		}

		return $lastInsertedSiteId;
	}

	/**
	 * Attach sites to given org id
	 *
	 * @param array $siteOrgData Multidimensional array with each item containing at least the `value` property
	 * eg 
	 * ```php
	 * $siteOrgData = [ ["value" => 1234], ["value" => 1235] ]
	 * ```
	 * @param int $orgId
	 * @return bool
	 */
	public function attach_site_to_org($siteOrgData = [], $orgId = 0) {
		$res = false;
		if (count($siteOrgData) > 0 && (int) $orgId > 0) {
			foreach ($siteOrgData as $val) {
				$val = !empty($val) && is_object($val) ? (array) $val : $val;
				if (isset($val['value']) && !empty($val['value']) && is_numeric($val['value'])) {
					$this->Basic_model->update_records('organisation_site', ['organisationId' => $orgId], ['archive' => '0', 'organisationId' => '0', 'id' => $val['value']]);
					$res = true;
				}
			}
		}
		return $res;
	}

	/**
	 * gets all the organization sites using the id and/or using the search term
	 */
	public function get_org_sites($orgId, $orgText_search) {
		$limit = 100;
		$tbl_1 = TBL_PREFIX . 'organisation_site';
		$select_column = array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename', $tbl_1 . '.abn');
		$dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_column)), false);
		$this->db->from($tbl_1);
		$this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_site.state', 'left');
		$this->db->where($tbl_1 . '.archive=', "0");
		$this->db->where($tbl_1 . '.organisationId=', $orgId);

		if (!empty($orgText_search)) {
			#Search by all site table 
			$this->db->group_start();
			$src_columns = array($tbl_1 . '.site_name', $tbl_1 . '.id as ocs_id', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename');

			for ($i = 0; $i < count($src_columns); $i++) {
				$column_search = $src_columns[$i];
				if (strstr($column_search, "as") !== false) {
					$serch_column = explode(" as ", $column_search);
					$this->db->or_like($serch_column[0], $orgText_search);
				} else {
					$this->db->or_like($column_search, $orgText_search);
				}
			}
			$this->db->group_end();
		}
		// $this->db->limit($limit, ($page * $limit));
		$query = $this->db->get();
		$dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;
		$x = $query->result_array();

		$site_ary = array();
		if (!empty($x)) {
			foreach ($x as $key => $value) {
				$site_id = $value['ocs_id'];
				$temp['mode'] = 'edit';
				$temp['orgId'] = $orgId;
				$temp['title'] = $value['site_name'];
				$temp['site_type'] = 1;
				$temp['ocs_id'] = $site_id;
				$temp['site_id'] = $site_id;
				$temp['site_address'] = $value['street'];
				$temp['state'] = $value['state'];
				$temp['statename'] = $value['statename'];
				$temp['postal'] = $value['postal'];
				$temp['abn'] = $value['abn'];
				$temp['address'] = $value['street'] . ', ' . $value['city'] . ', ' . $value['postal'] . ' ' . $value['statename'];
				$temp['city'] = array('value' => $value['city'], 'label' => $value['city'], 'postcode' => $value['postal']);
				$site_ary[] = $temp;
			}
		}
		return ['data' => $site_ary, 'count' => $dt_filtered_total];
	}

	function get_site_details($orgId, $siteId) {
		$org_ary = array();
		$tbl_1 = TBL_PREFIX . 'organisation_site';
		$tbl_3 = TBL_PREFIX . 'organisation_site_email';
		$tbl_4 = TBL_PREFIX . 'organisation_site_phone';

		$this->db->select(array($tbl_1 . '.organisationId', $tbl_1 . '.site_name', $tbl_1 . '.enable_portal_access', $tbl_1 . '.id as ocs_id', $tbl_1 . '.logo_file', $tbl_1 . '.status', $tbl_1 . '.street', $tbl_1 . '.city', $tbl_1 . '.postal', $tbl_1 . '.state', 'tbl_state.name as statename', $tbl_3 . '.email', $tbl_4 . '.phone', $tbl_1 . '.abn'));

		$this->db->from($tbl_1);
		$this->db->join($tbl_3, 'tbl_organisation_site_email.siteId = tbl_organisation_site.id AND tbl_organisation_site_email.primary_email = 1', 'left');
		$this->db->join($tbl_4, 'tbl_organisation_site_phone.siteId = tbl_organisation_site.id AND tbl_organisation_site_phone.primary_phone = 1', 'left');
		$this->db->join('tbl_state', 'tbl_state.id = tbl_organisation_site.state', 'left');
		$sWhere = array($tbl_1 . '.id' => $siteId, $tbl_1 . '.organisationId' => $orgId);
		$this->db->where($sWhere, null, false);
		$query = $this->db->get();

		$x = $query->row_array();

		$title = '';
		$subOrgName = '';
		$parentOrgName = '';
		if (!empty($x['organisationId'])) {
			$this->db->select("name,id,parent_org");
			$this->db->where('id', $orgId);
			$emp_qry = $this->db->get('tbl_organisation');

			$row = $emp_qry->row_array();
			$title_temp = $row['name'];

			if (isset($row['parent_org']) && $row['parent_org'] > 0) {
				$id = $row['parent_org'];
				$this->db->select("name");
				$this->db->where('id', $id);
				$emp_qr = $this->db->get('tbl_organisation');
				$rw = $emp_qr->row_array();
				$name = $rw['name'];
				$subOrgName = $title_temp;
				$parentOrgName = $name;
				$title = $name . ' - ' . $title_temp;
				$org_ary['basic_detail']['anySubOrg'] = true;
			} else {
				$parentOrgName = $row['name'];
				$org_ary['basic_detail']['anySubOrg'] = false;
				$title = $title_temp;
			}
		} else {
			return ['status' => false, 'data' => []];
		}

		$ocsid = $x['ocs_id'];
		if (file_exists(ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file']) && $x['logo_file'] != '')
			$profile_img = base_url() . ORG_UPLOAD_PATH . $ocsid . '/' . $x['logo_file'];
		else
			$profile_img = '/assets/images/Members_icons/boy.svg';

		$org_ary['basic_detail']['page_title'] = $title . ' - ' . $x['site_name'] . ' (House)';
		$org_ary['basic_detail']['parent_org'] = isset($parentOrgName) ? $parentOrgName : '';
		$org_ary['basic_detail']['type'] = 'house';
		$org_ary['basic_detail']['site_type'] = 1;
		$org_ary['basic_detail']['orgId'] = $orgId;
		$org_ary['basic_detail']['linked_to'] = isset($subOrgName) ? $subOrgName : '';
		$org_ary['basic_detail']['site_phones'] = $this->get_site_phones($siteId);
		$org_ary['basic_detail']['site_emails'] = $this->get_site_emails($siteId);
		$org_ary['basic_detail']['name'] = $x['site_name'];
		$org_ary['basic_detail']['ocs_id'] = $x['ocs_id'];
		$org_ary['basic_detail']['logo_file'] = $x['logo_file'];
		$org_ary['basic_detail']['enable_portal_access'] = $x['enable_portal_access'];
		$org_ary['basic_detail']['status'] = $x['status'];
		$org_ary['basic_detail']['primary_address'] = $x['street'] . ', ' . $x['city'] . ', ' . $x['postal'] . ' ' . $x['statename'];
		$org_ary['basic_detail']['street'] = $x['street'];
		$org_ary['basic_detail']['city'] = $x['city'];
		$org_ary['basic_detail']['postal'] = $x['postal'];
		$org_ary['basic_detail']['abn'] = $x['abn'];
		$org_ary['basic_detail']['city'] = array('value' => $x['city'], 'label' => $x['city'], 'postcode' => $x['postal']);
		$org_ary['basic_detail']['state'] = $x['state'];
		$org_ary['basic_detail']['logo_file'] = $profile_img;

		# getting contacts
		$org_ary['contact_detail'] = $this->get_site_cotact_multiple($siteId);

		return ['status' => true, 'data' => $org_ary];
	}

	function get_site_emails($id) {
		$CI = & get_instance();
		$tbl = 'organisation_site_email';
		$zMail = $CI->Basic_model->get_result($tbl, array('siteId' => $id, 'archive' => '0'), $columns = array('email', 'primary_email', 'id'));
		if (!empty($zMail)) {
			$email_temp = array();
			foreach ($zMail as $key => $valmail) {
				$email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
			}
			return $email_temp;
		}
	}

	function get_contact_emails($contactid) {
		$CI = & get_instance();
		$tbl = 'organisation_site_key_contact_email';
		$zMail = $CI->Basic_model->get_result($tbl, array('contactId' => $contactid, 'archive' => '0'), $columns = array('email', 'primary_email', 'id'));
		if (!empty($zMail)) {
			$email_temp = array();
			foreach ($zMail as $key => $valmail) {
				$email_temp[] = array('email' => $valmail->email, 'id' => $valmail->id, 'primary_email' => $valmail->primary_email);
			}
			return $email_temp;
		}
	}

	public function get_site_cotact_multiple($site_id = 0) {
        $tbl_8 = 'tbl_organisation_site_key_contact';
        $keycontacts_rows = $this->Basic_model->get_result('organisation_site_key_contact', array($tbl_8 . '.siteId' => $site_id, $tbl_8 . '.archive' => '0'), $columns = array($tbl_8 . '.position', $tbl_8 . '.department', $tbl_8 . '.id', $tbl_8 . '.street', $tbl_8 . '.city', $tbl_8 . '.postal', $tbl_8 . '.lastname', $tbl_8 . '.firstname', $tbl_8 . '.state', "CONCAT(tbl_organisation_site_key_contact.firstname,' ',tbl_organisation_site_key_contact.lastname) AS key_contact"));
        #last_query();

        if (!empty($keycontacts_rows)) {
            $kContacts_temp = array();
            foreach ($keycontacts_rows as $key => $valcontact) {
                $kEmail_temp = $this->get_contact_emails($valcontact->id);
				$kPhone_temp = $this->get_contact_phones($valcontact->id);

                $kContacts_temp[] = array(
					'key_contact_label' => $valcontact->key_contact,
					'key_contact' => $valcontact->key_contact . ' - ' . $valcontact->position, 'lastname' => $valcontact->lastname,
					'id' => $valcontact->id,
					'firstname' => $valcontact->firstname,
					'position' => $valcontact->position,
					'department' => $valcontact->department,
					'phones' => $kPhone_temp,
					'emails' => $kEmail_temp);
            }
            return $kContacts_temp;
        } else {
            return array(
                array(
                    'key_contact' => '',
                    'phones' => [['phone' => '']],
                    'emails' => [['email' => '']],
                    'firstname' => '',
                    'lastname' => '',
                    'position' => '',
                    'department' => ''
                )
            );
        }
    }

	function get_site_phones($id) {
		$CI = & get_instance();
		$tbl = 'organisation_site_phone';
		$z = $this->Basic_model->get_result($tbl, array('siteId' => $id, 'archive' => '0'), $columns = array('phone', 'primary_phone', 'id'));
		if (!empty($z)) {
			$ph_temp = array();
			foreach ($z as $key => $valPh) {
				$ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
			}
			return $ph_temp;
		}
	}

	function get_contact_phones($contactid) {
		$CI = & get_instance();
		$tbl = 'organisation_site_key_contact_phone';
		$z = $this->Basic_model->get_result($tbl, array('contactId' => $contactid, 'archive' => '0'), $columns = array('phone', 'primary_phone', 'id'));
		if (!empty($z)) {
			$ph_temp = array();
			foreach ($z as $key => $valPh) {
				$ph_temp[] = array('phone' => $valPh->phone, 'id' => $valPh->id, 'primary_phone' => $valPh->primary_phone);
			}
			return $ph_temp;
		}
	}
}
