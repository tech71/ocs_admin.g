<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



/*
|--------------------------------------------------------------------------
| HCM Org Portal constants
|--------------------------------------------------------------------------
|
| Note: When deploying to production, supply your own constants to `application/config/production/constants.php`.
| Contents of production constants file may look like this:
| 
| <?php
|
| define('FRONT_END_URL', 'https://samplebucketid.s3.apsoutheast1.amazonaws.com');
| define('JWT_TOKEN_EXPIRY', 600);
| define('JWT_SECRET_KEY', getenv('CI_JWT_SECRET_KEY'));
|
*/
defined('FRONT_END_URL')                OR define('FRONT_END_URL', 'http://localhost:3000');
defined('EXCEPTION_OUTPUT_FORMAT')      OR define('EXCEPTION_OUTPUT_FORMAT', 'application/json');
defined('JWT_SECRET_KEY')               OR define('JWT_SECRET_KEY', 'J8r$E4v6R-CSS-wmNERn'); // @todo Maybe put this in an .env file
defined('JWT_TOKEN_EXPIRY')             OR define('JWT_TOKEN_EXPIRY', 7200); // in seconds

define('APPLICATION_NAME', 'HCM'); // application Name

define('ROOT_FOLDER', '/ocs_development/');
define('DATE_TIME', date('Y-m-d H:i:s'));
define('GOOGLE_MAP_KEY', 'AIzaSyAV5zEPxeuJ0zeO_ue62RgqMwZN9cUVgJ4'); // ocs Key
define('ENABLE_MAIL', FALSE);
define('PHONE_REGEX_KEY', '/^(?=.{8,18}$)(\(?\+?[0-9]{1,3}\)?)([ ]{0,1})?[0-9_\- \(\)]{8,18}$/');
define('PHONE_AU_REGEX_KEY', '/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/');
define('DEFAULT_ATTACHMENT_UPLOAD_TYPE', 'jpg|jpeg|png|xlx|xls|doc|docx|pdf');
define('FINANCE_PAYROLL_EXEMPTION_ORG_UPLOAD_TYPE', 'jpg|jpeg|png|pdf');
define('NO_IMAGE_PATH', './assets/img/no-image.png');
define('DB_DATE_FORMAT', 'Y-m-d');
define('DATE_CURRENT', date('Y-m-d'));

defined('TBL_PREFIX')            OR define('TBL_PREFIX', 'tbl_');
defined('POSTCODE_AU_REGEX_KEY') OR define('POSTCODE_AU_REGEX_KEY', '/^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/');
defined('ADMIN_MODULE_ROOT')     OR define('ADMIN_MODULE_ROOT', realpath('../../'.'admin'.DIRECTORY_SEPARATOR.'back-end'.DIRECTORY_SEPARATOR.'application'));
defined('ORG_UPLOAD_PATH')       OR define('ORG_UPLOAD_PATH', realpath(ADMIN_MODULE_ROOT.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'organisation'.DIRECTORY_SEPARATOR));
defined('ORG_UPLOAD_RELATIVE_PATH')       OR define('ORG_UPLOAD_RELATIVE_PATH', 'uploads'.DIRECTORY_SEPARATOR.'organisation'.DIRECTORY_SEPARATOR);
