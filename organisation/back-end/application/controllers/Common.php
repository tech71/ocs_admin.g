<?php

use function PHPSTORM_META\type;

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * controller name: common
 */

class Common extends \MY_Controller {
    

	function __construct() {

		parent::__construct();
		$this->load->model("Common_model", "Common_model");
		$this->load->model("Basic_model", "basic_model");
	}

	public function index() {
		
	}

	public function archive_all() {
		$reqData = request_handler();
		$this->loges->setCreatedBy($reqData->adminId);

		if (!empty($reqData->data)) {
			$reqData = $reqData->data;

			if (!empty($reqData->loges)) {
				$this->loges->setModule($reqData->loges->module);
				$this->loges->setTitle($reqData->loges->note);
				$this->loges->setUserId($reqData->loges->userId);
				$this->loges->setDescription(json_encode($reqData));
				$this->loges->createLog();
			}

			$result = $this->basic_model->update_records($reqData->table, array('archive' => 1), $where = array('id' => $reqData->id));
			echo json_encode(array('status' => true));
		} else {
			echo json_encode(array('status' => false, 'error' => 'Invalid Request'));
		}
	}

	public function get_state() {
		$response = $this->basic_model->get_record_where('state', $column = array('name', 'id'), $where = array('archive' => 0));
		$state = null;
		foreach ($response as $val) {
			$state[] = array('label' => $val->name, 'value' => $val->id);
		}
		echo json_encode(array('status' => true, 'data' => $state));
	}

	public function get_case_primary_cat() {
		$reqData = request_handler();
		$response = $this->basic_model->get_record_where('fms_case_all_category', $column = array('name', 'id'), $where = array('archive' => '0'));
		foreach ($response as $val) {
			$state[] = array('label' => $val->name, 'value' => $val->id);
		}
		echo json_encode(array('status' => true, 'data' => $state));
	}

	public function get_shift_requirement() {
		$reqData = request_handler();
		$response = $this->basic_model->get_record_where('shift_requirement', $column = array('name', 'id'), $where = array('archive' => 0));
		foreach ($response as $val) {
			$state['shift_data'][] = array('label' => $val->name, 'value' => $val->id);
		}

		$response_org = $this->basic_model->get_record_where('shift_org_requirement', $column = array('name', 'id'), $where = array('archive' => 0));
		foreach ($response_org as $val) {
			$state['org_shift_data'][] = array('label' => $val->name, 'value' => $val->id);
		}
		echo json_encode(array('status' => true, 'data' => $state));
	}

	function get_user_information() {
		echo $this->loges->getCreated();

		$where = "user_email='" . $email . "' and user_password='" . $password . "' and user_status ='" . $status . "'";
		$where = array('user_email' => $email, 'user_password' => $password, 'user_status' => $status, 'is_deleted' => 0);
	}

	function get_suburb() {
		$reqData = request_handler();
		$reqData->data = json_decode($reqData->request_data->data);
		$suburb = isset($reqData->data->query) ? $reqData->data->query : '';
		$state = isset($reqData->data->state) ? $reqData->data->state : 0;
		$rows = $this->Common_model->get_suburb($suburb, $state);
		echo json_encode($rows);
	}

	function cookie_set() {

		if ($this->input->get('tc')) {
			setcookie('hcfd', $this->input->get('tc'), time() + (86400 * 1), "/"); // 86400 = 1 day
			if($this->input->get('rd')){
				$rd = base64_decode(urldecode($this->input->get('rd')));
				redirect($rd);
			}else{
				echo 'Access denied';
				exit;
			}
		}

	}

	function get_user_for_compose_mail() {
		$reqData = request_handler();
		$rows = $this->Common_model->get_user_for_compose_mail($reqData->data);
		echo json_encode($rows);
	}

	function get_admin_name() {
		$reqData = request_handler();
		$reqData->data = gettype($reqData->data) == 'string' ? json_decode($reqData->data) : $reqData->data;
		$currentAdminId = $reqData->adminId;
		$rows = $this->Common_model->get_admin_name($reqData->data, $currentAdminId);

		echo json_encode($rows);
	}

	function get_admin_team_department() {
		$reqData = request_handler();

		$rows = $this->Common_model->get_admin_team_department($reqData->data->search, $reqData->adminId);

		echo json_encode($rows);
	}

	function get_department() {
		request_handler();

		$rows = $this->basic_model->get_record_where('department', array('id as value', 'name as label', 'short_code'), ['archive' => 0, 'short_code' => 'internal_staff']);

		echo json_encode(array('status' => true, 'data' => $rows));
	}

	function get_global_search_option() {
		$reqData = request_handler();

		if ($reqData->data) {
			$search = $reqData->data->search;

			$data = $this->Common_model->get_global_search_data($search, $reqData->adminId);

			$rows = array('status' => true, 'data' => $data);
			echo json_encode($rows);
		}
	}

	public function get_org_requirement() {
		$reqData = request_handler();
		$response = $this->basic_model->get_record_where('organisation_requirement', $column = array('name', 'id'), $where = array('archive' => '0'));
		$state = [];
		foreach ($response as $val) {
			$state[] = array('label' => $val->name, 'value' => $val->id, 'checked' => false);
		}
		echo json_encode(array('status' => true, 'data' => $state));
	}

	public function get_member_name() {
		$reqData = request_handler();
		$reqData->data = json_decode($reqData->data);
		$post_data = isset($reqData->data->query) ? $reqData->data->query : '';
		$rows = $this->Common_model->get_member_name($post_data);
		echo json_encode($rows);
	}

	public function get_org_name() {
		$reqData = request_handler();
		$reqData->data = json_decode($reqData->data);
		$name = isset($reqData->data->query) ? $reqData->data->query : '';
		$rows = $this->Common_model->get_org_name($name);
		echo json_encode($rows);
	}

	public function archive_all_primary() {

		$reqData = request_handler();

		$this->loges->setCreatedBy($reqData->adminId);

		if (!empty($reqData->data)) {
			$reqData = $reqData->data;

			$this->load->library('Checkarchiveprimary', null, 'commonarchive');
			$this->commonarchive->setCurrentData($reqData->table);
			$result = $this->commonarchive->checkSecondaryDataExist($reqData, true);
			if (!empty($reqData->loges) && $result['status']) {
				$this->loges->setModule($reqData->loges->module);
				$this->loges->setTitle($reqData->loges->note);
				$this->loges->setUserId($reqData->loges->userId);
				$this->loges->setDescription(json_encode($reqData));
				$this->loges->createLog();
			}
			echo json_encode($result);
		} else {
			echo json_encode(array('status' => false, 'error' => 'Invalid Request'));
		}
	}

	public function get_recruitment_staff() {
		$reqData = request_handler();
		$reqData->data = json_decode($reqData->data);
		$post_data = isset($reqData->data->query) ? $reqData->data->query : '';
		$rows = $this->Common_model->get_recruitment_staff($post_data);
		echo json_encode($rows);
	}

	public function get_data() {
		$rows = $this->basic_model->get_record_where('participant_genral', '', '');
		echo json_encode($rows);
	}

	public function mediaShow($type, $file_name, $userId = '') {
		$file_name = base64_decode(urldecode($file_name));
		$this->load->helper('file');
		$this->load->helper('cookie');
		$token = get_cookie('hcfd');
		$resData =$this->Common_model->file_content_media($type,$file_name,$userId,1,['token'=>$token]);
		if($resData['status']){
			header('content-type: ' . $resData['mimetype']);
		}
		echo $resData['msg'];
		exit;
		// $data = FCPATH.APPLICANT_ATTACHMENT_UPLOAD_PATH.'100/n_e.mp4';
	}

	public function mediaShowProfile($type, $file_name, $userId = '',$genderType='0') {
		$file_name = base64_decode(urldecode($file_name));                      
		$userId = !empty($userId) ? base64_decode(urldecode($userId)) : $userId;                      
		$genderType = !empty($genderType) ? base64_decode(urldecode($genderType)) : $genderType;                      
		$resData =$this->Common_model->file_content_media($type,$file_name,$userId,0,['genderType'=>$genderType,'defaultImageShow'=>1]);
		if($resData['status']){
			header('content-type: ' . $resData['mimetype']);
				header("Content-Disposition: inline; filename=$file_name");
		}
		echo $resData['msg'];
		exit;
	}

	public function mediaDownload($type = 'all', $file_name) {
		$this->load->helper('file');
		$filePath = FCPATH;
		$filePath .= ARCHIEVE_DIR . '/';
		/* if($type == 'r'){
			//path add if specail folder created inside dir
			} */
		$filePath .= $file_name;
		$temp_filename = $filePath;

		if (is_file($temp_filename) && file_exists($temp_filename)) {
			$filename = basename($temp_filename);
			header("Content-Type:application/zip");
			header("Content-Disposition: attachment; filename=$filename");
			header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
			header("Content-Length:" . filesize($temp_filename));

			if (readfile($temp_filename)) {
				unlink($temp_filename);
			}
		} else {
			echo "File not exists";
		}
		exit;
	}
}
