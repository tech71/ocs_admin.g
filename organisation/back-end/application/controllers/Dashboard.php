<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property-read \DashboardModel $dashboard
 * @property-read \Org_model $Org_model
 */
class Dashboard extends \MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Org_model');
    }

    /**
     * `GET: /dashboard`
     *
     * @return \MY_Output
     */
    public function index()
	{
        $this->load->model("DashboardModel", "dashboard");
        $organisation = $this->ensureUserLoggedIn();
        $dashboardModel = $this->dashboard;
        $organisationID = $organisation->id;

        return $this->json([
            "latest_updates" => [
                "title" => "Latest updates",
                "data" => $dashboardModel->findAllLatestUpdates($organisationID),
                "_links" => [
                    "view_all" => [
                        "text" => "View all",
                        "href" => "/"
                    ]
                ]
            ],
            "invoices" => [
                "title" => "Invoices",
                "data" => $dashboardModel->findAllInvoices($organisationID),
                "_links" => [
                    "view_all" => [
                        "text" => "View all",
                        "href" => "/"
                    ]
                ]
            ],
            "fms_feedback" => $dashboardModel->findAllFMSFeedbacks($organisationID),
            "_links" => [
                "create_new_site" => [ "href" => "/" ],
                "create_new_sub_org" => [ "href" => "/" ],
            ]
        ]);
    }

    /**
     * Search parent organisation by name
     * `GET: /dashboard/get_parent_org_name?query={string}`
     *
     * @return \CI_Output
     */
    public function get_parent_org_name() 
    {
        $this->ensureUserLoggedIn();

        $query = $this->input->get("query");
        $rows = $this->Org_model->get_parent_org_name($query);
        return $this->json(["data" => $rows]);
    }


    /**
     * Search site by name
     *
     * @return \CI_Output
     */
    public function get_site_org()
    {
        $this->ensureUserLoggedIn();

        $query = $this->input->get("query");
        $rows = $this->Org_model->get_site_org_unassign($query);
        return $this->json(["data" => $rows]);
    }
    
}
