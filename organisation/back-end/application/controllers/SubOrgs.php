<?php

use app\entities\Organisation;
use app\entities\OrganisationRequirement;
use Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Controller for sub-org related enpoints
 * 
 * @property-read Org_model $Org_model
 * @property-read Common_model $Common_model
 */
class SubOrgs extends \MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Org_model");
    }
    
    /**
     * `GET/POST: /suborgs/create`
     * 
     * Endpoint to 
     * - get details for create suborg form
     * - creates the suborg
     */
    public function create()
    {
        $org = $this->ensureUserLoggedIn();
        $orgModel = $this->Org_model;
        
        // POST request
        if ($this->input->method() === "post") {
            $this->load->library("form_validation");

            // The form will submit form data with `values` and `logo`
            $values = $this->input->post("values");
            $post = \json_decode($values, true);

            $post = Arr::only($post, [
                "publish",
                "logo",
                "OrgLegalDetails", 
                "ContactDetails", 
                "KeyContactDetails", 
                "BillingContactDetails", 
                "SubOrgRequirements",
            ]);

            $post["parent_organisation"] = $org->id;
            
            $this->form_validation->set_data($post);
            $this->form_validation->set_rules($this->rules("create"));

            // validate phone,
            foreach ($post["ContactDetails"]["phones"] as $i => $phone) {
                $isPrimary = Arr::get($phone, "is_primary", false);

                $rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
                if (!$isPrimary && $phone['value']) {
                    $rule = "trim|callback_valid_aus_phone_number";
                }

                $this->form_validation->set_rules(
                    "ContactDetails[phones][$i][value]", 
                    $isPrimary ? "primary phone" : "secondary phone", 
                    $rule, 
                    ["valid_aus_phone_number" => "Invalid phone number format"]
                );
            }

            // primary phone is required, if secondary phone is supplied, validate them
            $phones = $post["KeyContactDetails"]["phones"];
            foreach ($phones as $i => $phone) {
                $isPrimary = Arr::get($phone, "is_primary", false);
                
                $rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
                if (!$isPrimary && $phone['value']) {
                    $rule = "trim|callback_valid_aus_phone_number";
                }
                
                $this->form_validation->set_rules(
                    "KeyContactDetails[phones][$i][value]", 
                    $isPrimary ? "primary phone" : "secondary phone", 
                    $rule,
                    ["valid_aus_phone_number" => "Invalid phone number format"]
                );
            }
            
            // primary phone is required, if secondary phone is supplied, validate them
            foreach ($post["BillingContactDetails"]["phones"] as $i => $phone) {
                $isPrimary = Arr::get($phone, "is_primary", false);

                $rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
                if (!$isPrimary && $phone['value']) {
                    $rule = "trim|callback_valid_aus_phone_number";
                }

                $this->form_validation->set_rules(
                    "BillingContactDetails[phones][$i][value]", 
                    $isPrimary ? "primary phone" : "secondary phone", 
                    $rule,
                    [ "valid_aus_phone_number" => "Invalid phone number format" ]
                );
            }

            // ---------------------------------------

            // primary email is required, if secondary email is supplied, validate them
            foreach ($post["ContactDetails"]["emails"] as $i => $email) {
                $isPrimary = Arr::get($email, "is_primary", false);

                $rule = $isPrimary ? "required|valid_email" : "trim";
                if (!$isPrimary && $email['value']) {
                    $rule = "trim|valid_email";
                }

                $this->form_validation->set_rules("ContactDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
            }

            // primary email is required, if secondary email is supplied, validate them
            foreach ($post["KeyContactDetails"]["emails"] as $i => $email) {
                $isPrimary = Arr::get($email, "is_primary", false);

                $rule = $isPrimary ? "required|valid_email" : "trim";
                if (!$isPrimary && $email['value']) {
                    $rule = "trim|valid_email";
                }

                $this->form_validation->set_rules("KeyContactDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
            }

            // primary email is required, if secondary email is supplied, validate them
            foreach ($post["BillingContactDetails"]["emails"] as $i => $email) {
                $isPrimary = Arr::get($email, "is_primary", false);

                $rule = $isPrimary ? "required|valid_email" : "trim";
                if (!$isPrimary && $email['value']) {
                    $rule = "trim|valid_email";
                }

                $this->form_validation->set_rules("BillingContactDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
            }

            if (!$this->form_validation->run()) {
                return $this->json(["errors" => $this->form_validation->error_array()], self::HTTP_BAD_REQUEST);
            }

            
            $this->db->trans_begin();
            
            // create org
            $createdOrgId = $orgModel->createOrganisation($post);
            if (!$createdOrgId) {
                $this->db->trans_rollback();
                $this->flash("danger", "Failed to create organisation");
                return $this->json([ "errors" => "Failed to create organisation" ], self::HTTP_INTERNAL_ERROR);
            }

            // upload logo
            $uploadDetails = $orgModel->uploadOrganisationLogo($createdOrgId, "logo");
            if (!$uploadDetails) {
                $this->db->trans_rollback();
                $uploadErrorMsg = $this->upload->display_errors(null, null);
                return $this->json([ "errors" => [ "logo" => $uploadErrorMsg ]], self::HTTP_BAD_REQUEST);
            }

            $where = ["id" => $createdOrgId];
            $filename = $uploadDetails->file_name;
            $this->db->update(Organisation::TABLE_NAME, ["logo_file" => $filename], $where);


            // when we get to this point means items above are successful
            $this->db->trans_commit();

            $this->flash("success", "Successfully created organisation");
            return $this->json(["destination_url" => "/sub-orgs/{$createdOrgId}"]);
        }


        // GET request
        $requirements = $orgModel->findAllOrganisationRequirements();
        $this->load->model("Common_model");
        return $this->json([
            "states" => $this->Common_model->get_states(),
            "addressCategories" => [
                [ "value" => 'example', "label" => 'Sample address type' ],
            ],
            "orgRequirements" => array_map(function (OrganisationRequirement $req) {
                return [
                    "label" => $req->name,
                    "value" => $req->id,
                    "checked" => false
                ];
            }, $requirements),
        ]);
    }


    /**
     * Find sub-org by given search term
     *
     * @return \CI_Output
     */
    public function get_sub_org() 
    {
        $org = $this->ensureUserLoggedIn();

        require_once ADMIN_MODULE_ROOT . '/Classes/organisation/Organisation.php';
        $objOrg = new OrganisationClass\Organisation();

        $search = $this->input->get("search");
        $limit = $this->input->get("limit") ?? 50;
        $page = $this->input->get("page") ?? 0;
        $status = 1;

        $objOrg->setId($org->id);
        $objOrg->settext_search($search);
        $objOrg->setLimit($limit);
        $objOrg->setPage($page);
        $objOrg->setStatus($status);

        $results = $objOrg->get_sub_org();
        if (isset($results['data']) && !empty($results['data'])) {
            return $this->json([
                'status' => true, 
                'data' => $results['data'], 
                'count' => $results['count']
            ]);
        } else {
            return $this->json([
                'status' => false, 
                'data' => [], 
                'count' => $results['count']
            ]);
        }
    }

    /**
     * Contains group of rules for this controller
     *
     * @param string $group
     * @return array
     */
    protected function rules($group)
    {
        $rules =  [
            "create" => [
                [
                    "field" => "OrgLegalDetails[sub_org_legal_name]",
                    "label" => "Sub-org's name (legal)",
                    "rules" => [
                        "required", 
                        "min_length[2]"
                    ]
                ],
                [
                    "field" => "OrgLegalDetails[abn]",
                    "label" => "ABN",
                    "rules" => [
                        "required",
                        ["valid_abn", function($value) {
                            $this->load->library("AbnValidator");
                            return \AbnValidator::isValidAbn($value);
                        }],
                        "is_unique[tbl_organisation.abn]"
                    ],
                    "errors" => [
                        "valid_abn" => "Invalid ABN"
                    ],
                ],
                [
                    "field" => "ContactDetails[sub_org_address]",
                    "label" => "Sub org address",
                    "rules" => [
                        "required",
                    ]
                ],
                [
                    "field" => "ContactDetails[city]",
                    "label" => "city",
                    "rules" => [
                        "required",
                    ]
                ],
                [
                    "field" => "ContactDetails[state]",
                    "label" => "state",
                    "rules" => [
                        "required",
                        ["valid_aus_state", function($state) {
                            /** @var \CI_DB_mysqli_result $query */
                            $query = $this->db->query("SELECT * FROM tbl_state WHERE id = ?", [strtoupper($state)]);
                            $numrows = $query->num_rows();
                            return $numrows > 0;
                        }],
                    ],
                    "errors" => [
                        "valid_aus_state" => "Not valid state"
                    ]
                ],
                [
                    "field" => "ContactDetails[postcode]",
                    "label" => "postcode",
                    "rules" => [
                        "required",
                        ["valid_postcode", function($postcode) {
                            // https://www.etl-tools.com/regular-expressions/is-australian-post-code.html
                            // Valid: 0200,7312,2415
                            // Invalid: 0300,7612,2915
                            $matches = preg_match(POSTCODE_AU_REGEX_KEY, $postcode);
                            $doesMatch = $matches === 1;
                            return $doesMatch;
                        }]
                    ],
                    "errors" => [
                        "valid_postcode" => "Not a valid postcode"
                    ]
                ],
                [
                    "field" => "ContactDetails[website]",
                    "label" => "website",
                    "rules" => [
                        "required",
                        "prep_url",
                        "valid_url"
                    ]
                ],
                [
                    "field" => "ContactDetails[address_category]",
                    "label" => "address category",
                    "rules" => [
                        "required",
                        "in_list[head,billing,other]"
                    ]
                ],
                [
                    "field" => "KeyContactDetails[first_name]",
                    "label" => "first name",
                    "rules" => [
                        "required",
                        "alpha_numeric_spaces"
                    ],
                ],
                [
                    "field" => "KeyContactDetails[last_name]",
                    "label" => "last name",
                    "rules" => [
                        "required",
                        "alpha_numeric_spaces"
                    ],
                ],
                [
                    "field" => "KeyContactDetails[position]",
                    "label" => "key contact position",
                    "rules" => [
                        "required",
                    ],
                ],
                [
                    "field" => "BillingContactDetails[first_name]",
                    "label" => "first name",
                    "rules" => [
                        "required",
                        "alpha_numeric_spaces"
                    ],
                ],
                [
                    "field" => "BillingContactDetails[last_name]",
                    "label" => "last name",
                    "rules" => [
                        "required",
                        "alpha_numeric_spaces"
                    ],
                ],
                [
                    "field" => "BillingContactDetails[position]",
                    "label" => "key contact position",
                    "rules" => [
                        "required",
                    ],
                ],
                [
                    "field" => "BillingContactDetails[pay_roll_tax]",
                    "label" => "pay roll tax",
                    "rules" => [
                        'required',
                    ],
                ],
                [
                    "field" => "BillingContactDetails[gst]",
                    "label" => "gst",
                    "rules" => [
                        'required',
                    ],
                ],
            ]
        ];




        return $rules[$group];
    }


    /**
     * Validate australian phone number. 
     * Use `callback_valid_aus_phone_number` rule to call this method 
     *
     * @param string $phone
     * @return bool
     */
    public function valid_aus_phone_number($phone) 
    {
        // https://www.etl-tools.com/regular-expressions/is-australian-phone-number.html
        // Valid: 0403111111, (03) 1111 1111, +61403111111, 02 9111 1111, 0403 111 111, 91111111
        // Invalid: 9111 11111, 99 1111 1111
        $matches = preg_match(PHONE_AU_REGEX_KEY, $phone);
        $doesMatch = $matches === 1;
        return $doesMatch;
    }


    /**
     * Check if state exists by ID.
     * Use `callback_valid_aus_state` in your validation rules to use this method
     *
     * @param string $state
     * @return bool
     */
    public function valid_aus_state($state) 
    {
        /** @var \CI_DB_mysqli_result $query */
        $query = $this->db->query("SELECT * FROM tbl_state WHERE name = ?", [strtoupper($state)]);
        $numrows = $query->num_rows();
        $found = $numrows > 0;

        if (!$found) {
            $this->form_validation->set_message("valid_aus_phone_number", "Invalid australian phone number");
        }

        return $found;
    }


}