<?php

defined('BASEPATH') or exit('No direct script access allowed');
/*
 * controller name: common
 */

class CommonController extends \MY_Controller
{
    public function __construct() {
        parent::__construct();
        // $this->load->model('Org_model');
        $this->load->model('basic_model');
    }

    /**
     * function to fetch the list of states(i.e; geographical state)
    */
    public function getStateList() {
        $reqData = request_handler();
        $response = $state = [];

        $response = $this->basic_model->get_record_where('state', $column = array('name', 'id'), $where = array('archive' => 0));

        foreach ($response as $val) {
            $state[] = [ 'label' => $val->name, 'value' => $val->id ];
        }

        echo json_encode(['status' => true, 'data' => $state]);
    }

    /**
     * function to fetch the list of suburbs(i.e; geographical state)
    */
    public function getSuburbList() {
        $reqData = request_handler();
        $state = $reqData->state;
        $searchStr = $reqData->str;

        $response = $suburb = [];

        $response = $this->basic_model
            ->getRecordWhereLike('suburb_state', $column = ['id as value', 'suburb as label', 'state', 'stateId'], ['stateId' => $state], ['suburb' => $searchStr]);

        if(!empty($response)) {
            foreach($response as $val) {
                $suburb[] = [ 'label' => $val->label, 'value' => $val->label ];
            }
        }

        echo json_encode(['status' => true, 'data' => $suburb]);
    }
}
