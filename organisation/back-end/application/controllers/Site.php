<?php

use app\entities\OrganisationSite;
use app\entities\OrganisationRequirement;
use Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Controller for sub-Site related enpoints
 * 
 * @property-read Site_model $Site_model
 * @property-read Common_model $Common_model
 */
class Site extends \MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Site_model");
	}

	/**
	 * `GET/POST: /Site/create`
	 * 
	 * Endpoint to 
	 * - get details for create subsite form
	 * - creates the subsite
	 */
	public function create()
	{
		$Org = $this->ensureUserLoggedIn();
		$siteModel = $this->Site_model;
		
		// POST request
		if ($this->input->method() === "post") {
			$this->load->library("form_validation");

			// The form will submit form data with `values` and `logo`
			$values = $this->input->post("data");
			$post = \json_decode($values, true);

			$post = Arr::only($post, [
				"SiteDetails", 
				"KeyContactDetails", 
				"BillingContactDetails"
			]);
			$post["parent_organisation"] = $Org->id;
			
			$this->form_validation->set_data($post);
			$this->form_validation->set_rules($this->rules("create"));

			// validate phone,
			foreach ($post["SiteDetails"]["phones"] as $i => $phone) {
				$isPrimary = Arr::get($phone, "is_primary", false);

				$rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
				if (!$isPrimary && $phone['value']) {
					$rule = "trim|callback_valid_aus_phone_number";
				}

				$this->form_validation->set_rules(
					"SiteDetails[phones][$i][value]", 
					$isPrimary ? "primary phone" : "secondary phone", 
					$rule, 
					["valid_aus_phone_number" => "Invalid phone number format"]
				);
			}

			// primary phone is required, if secondary phone is supplied, validate them
			$phones = $post["KeyContactDetails"]["phones"];
			foreach ($phones as $i => $phone) {
				$isPrimary = Arr::get($phone, "is_primary", false);
				
				$rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
				if (!$isPrimary && $phone['value']) {
					$rule = "trim|callback_valid_aus_phone_number";
				}
				
				$this->form_validation->set_rules(
					"KeyContactDetails[phones][$i][value]", 
					$isPrimary ? "primary phone" : "secondary phone", 
					$rule,
					["valid_aus_phone_number" => "Invalid phone number format"]
				);
			}
			
			// primary phone is required, if secondary phone is supplied, validate them
			foreach ($post["BillingContactDetails"]["phones"] as $i => $phone) {
				$isPrimary = Arr::get($phone, "is_primary", false);

				$rule = $isPrimary ? "required|callback_valid_aus_phone_number" : "trim";
				if (!$isPrimary && $phone['value']) {
					$rule = "trim|callback_valid_aus_phone_number";
				}

				$this->form_validation->set_rules(
					"BillingContactDetails[phones][$i][value]", 
					$isPrimary ? "primary phone" : "secondary phone", 
					$rule,
					[ "valid_aus_phone_number" => "Invalid phone number format" ]
				);
			}

			// ---------------------------------------

			// primary email is required, if secondary email is supplied, validate them
			foreach ($post["SiteDetails"]["emails"] as $i => $email) {
				$isPrimary = Arr::get($email, "is_primary", false);

				$rule = $isPrimary ? "required|valid_email" : "trim";
				if (!$isPrimary && $email['value']) {
					$rule = "trim|valid_email";
				}

				$this->form_validation->set_rules("SiteDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
			}

			// primary email is required, if secondary email is supplied, validate them
			foreach ($post["KeyContactDetails"]["emails"] as $i => $email) {
				$isPrimary = Arr::get($email, "is_primary", false);

				$rule = $isPrimary ? "required|valid_email" : "trim";
				if (!$isPrimary && $email['value']) {
					$rule = "trim|valid_email";
				}

				$this->form_validation->set_rules("KeyContactDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
			}

			// primary email is required, if secondary email is supplied, validate them
			foreach ($post["BillingContactDetails"]["emails"] as $i => $email) {
				$isPrimary = Arr::get($email, "is_primary", false);

				$rule = $isPrimary ? "required|valid_email" : "trim";
				if (!$isPrimary && $email['value']) {
					$rule = "trim|valid_email";
				}

				$this->form_validation->set_rules("BillingContactDetails[emails][$i][value]", $isPrimary ? "primary email" : "secondary email", $rule);
			}

			if (!$this->form_validation->run()) {
				return $this->json(["errors" => $this->form_validation->error_array()], self::HTTP_BAD_REQUEST);
			}
			
			$this->db->trans_begin();
			
			// creating Site
			$createdSiteId = $siteModel->createSite($post);
			if (!$createdSiteId) {
				$this->db->trans_rollback();
				$this->flash("danger", "Failed to create organisation site");
				return $this->json([ "errors" => "Failed to create organisation site" ], self::HTTP_INTERNAL_ERROR);
			}

			// when we get to this point means items above are successful
			$this->db->trans_commit();

			$this->flash("success", "Successfully created organisation site");
			return $this->json(["destination_url" => "/sites"]);
		}
	}

	/**
	 * Contains group of rules for this controller
	 *
	 * @param string $group
	 * @return array
	 */
	protected function rules($group)
	{
		$rules =  [
			"create" => [
				[
					"field" => "SiteDetails[title]",
					"label" => "title",
					"rules" => [
						"required"
					]
				],
				[
					"field" => "SiteDetails[abn]",
					"label" => "ABN",
					"rules" => [
						"required",
						["valid_abn", function($value) {
							$this->load->library("AbnValidator");
							return \AbnValidator::isValidAbn($value);
						}],
						"is_unique[tbl_organisation.abn]"
					],
					"errors" => [
						"valid_abn" => "Invalid ABN"
					],
				],
				[
					"field" => "SiteDetails[site_address]",
					"label" => "site address",
					"rules" => [
						"required",
					]
				],
				[
					"field" => "SiteDetails[city]",
					"label" => "suburb",
					"rules" => [
						"required",
					]
				],
				[
					"field" => "SiteDetails[state]",
					"label" => "state",
					"rules" => [
						"required",
						["valid_aus_state", function($state) {
							/** @var \CI_DB_mysqli_result $query */
							$query = $this->db->query("SELECT * FROM tbl_state WHERE id = ?", [strtoupper($state)]);
							$numrows = $query->num_rows();
							return $numrows > 0;
						}],
					],
					"errors" => [
						"valid_aus_state" => "not a valid state"
					]
				],
				[
					"field" => "SiteDetails[postcode]",
					"label" => "postcode",
					"rules" => [
						"required",
						["valid_postcode", function($postcode) {
							// https://www.etl-tools.com/regular-expressions/is-australian-post-code.html
							// Valid: 0200,7312,2415
							// Invalid: 0300,7612,2915
							$matches = preg_match(POSTCODE_AU_REGEX_KEY, $postcode);
							$doesMatch = $matches === 1;
							return $doesMatch;
						}]
					],
					"errors" => [
						"valid_postcode" => "Not a valid postcode"
					]
				],
				[
					"field" => "KeyContactDetails[first_name]",
					"label" => "first name",
					"rules" => [
						"required",
						"alpha_numeric_spaces"
					],
				],
				[
					"field" => "KeyContactDetails[last_name]",
					"label" => "last name",
					"rules" => [
						"required",
						"alpha_numeric_spaces"
					],
				],
				[
					"field" => "KeyContactDetails[position]",
					"label" => "key contact position",
					"rules" => [
						"required",
					],
				],
				[
					"field" => "BillingContactDetails[first_name]",
					"label" => "first name",
					"rules" => [
						"required",
						"alpha_numeric_spaces"
					],
				],
				[
					"field" => "BillingContactDetails[last_name]",
					"label" => "last name",
					"rules" => [
						"required",
						"alpha_numeric_spaces"
					],
				],
				[
					"field" => "BillingContactDetails[position]",
					"label" => "key contact position",
					"rules" => [
						"required",
					],
				],
				[
					"field" => "BillingContactDetails[pay_roll_tax]",
					"label" => "pay roll tax",
					"rules" => [
						'required',
					],
				],
				[
					"field" => "BillingContactDetails[gst]",
					"label" => "gst",
					"rules" => [
						'required',
					],
				],
			]
		];
		return $rules[$group];
	}

	/**
	 * Find sites by given search term
	 *
	 * @return \CI_Output
	 */
	public function get_sites() 
	{
		$org = $this->ensureUserLoggedIn();
		$siteModel = $this->Site_model;
		$search = $this->input->get("search");
		$results = $siteModel->get_org_sites($org->id, $search);
		if (isset($results['data']) && !empty($results['data'])) {
			return $this->json([
				'status' => true, 
				'data' => $results['data'], 
				'count' => $results['count']
			]);
		} else {
			return $this->json([
				'status' => false, 
				'data' => [], 
				'count' => $results['count']
			]);
		}
	}

	/**
	 * Fetches site specific information
	 *
	 * @return \CI_Output
	 */
	public function get_site_details() 
	{
		$org = $this->ensureUserLoggedIn();
		$siteModel = $this->Site_model;
		$results = $siteModel->get_site_details($org->id, $_GET['id']);
		if (isset($results['data']) && !empty($results['data'])) {
			return $this->json([
				'status' => true, 
				'data' => $results['data']
			]);
		} else {
			return $this->json([
				'status' => false, 
				'data' => []
			]);
		}
	}

	/**
	 * Validate australian phone number. 
	 * Use `callback_valid_aus_phone_number` rule to call this method 
	 *
	 * @param string $phone
	 * @return bool
	 */
	public function valid_aus_phone_number($phone) 
	{
		// https://www.etl-tools.com/regular-expressions/is-australian-phone-number.html
		// Valid: 0403111111, (03) 1111 1111, +61403111111, 02 9111 1111, 0403 111 111, 91111111
		// Invalid: 9111 11111, 99 1111 1111
		$matches = preg_match(PHONE_AU_REGEX_KEY, $phone);
		$doesMatch = $matches === 1;
		return $doesMatch;
	}

	/**
	 * Check if state exists by ID.
	 * Use `callback_valid_aus_state` in your validation rules to use this method
	 *
	 * @param string $state
	 * @return bool
	 */
	public function valid_aus_state($state) 
	{
		/** @var \CI_DB_mysqli_result $query */
		$query = $this->db->query("SELECT * FROM tbl_state WHERE name = ?", [strtoupper($state)]);
		$numrows = $query->num_rows();
		$found = $numrows > 0;

		if (!$found) {
			$this->form_validation->set_message("valid_aus_phone_number", "Invalid australian phone number");
		}

		return $found;
	}
}