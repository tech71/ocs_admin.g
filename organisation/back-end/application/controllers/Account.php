<?php

// Exceptions to short circuit action excution
use app\exceptions\BadRequestHttpException;
use app\exceptions\NotFoundHttpException;
use app\exceptions\UnauthorizedHttpException;

// Array helper from one of Laravel's components
use \Illuminate\Support\Arr;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller that handles authentication for organisation accounts.
 * 
 * To authenticate, the client has to supply username and password 
 * in exchange for JWT tokens
 * 
 * By default, JWT tokens will have 1 hour lifetime. This is configurable
 * via `JWT_TOKEN_EXPIRY` inside `application/config/constants.php`
 * 
 * @author Joshua Orozco <joshua@ydt.com.au>
 * 
 * @property-read \AccountModel $accountModel
 */
class Account extends \MY_Controller
{
    const JWT_PAYLOAD_DATE = "_date";

    const URL_PASSWORD_RESET = "/password-reset";
    const URL_TEMPORARY_PASSWORD_RESET = "/temp-password-reset";
    const URL_DASHBOARD = "/";
    const URL_LOGIN = "/login";
    const URL_LOGOUT = "/logout";


    public function __construct()
    {
        parent::__construct();
        $this->load->model("AccountModel", "accountModel");
    }
    

    /**
     * Checks whether credentials exists in table `tbl_organisation`
     *
     * @return \CI_Output
	 * @throws \app\exceptions\BadRequestHttpException When one of these happens:
	 * 	- Not an ajax request
	 *  - Invalid credentials
     */
    public function login()
    {
        if (! $this->input->is_ajax_request()) {
			throw new BadRequestHttpException("Not an ajax request");
        }

        $username = $this->input->post("username");
        $password = $this->input->post("password");
        
        // Check credentials
        $accountModel = $this->accountModel;
        $organizationDetails = $accountModel->findOrganisationByCredentials($username, $password);
        if (! $organizationDetails) {
			throw new UnauthorizedHttpException("Invalid username / password");
        }

        $claims = $organizationDetails;

        // exclude sensitive data
        $claims = Arr::except($claims, ["password", "password_reset_token"]);

        $token = $this->generateJWTToken($claims);

        $responseData = [
            "token" => $token,
            "destination_url" => self::URL_DASHBOARD,
            "message" => "Successfully logged in",
        ];

        $isFirstTimeLogin = $accountModel->isFirstTimeLogin($organizationDetails);
        if ($isFirstTimeLogin) {
            $responseData = array_merge($responseData, [
                "message" => "You are required to reset your password when your login for the first time",
                "is_first_time_login" => true,
                "temporary_password_reset_url" => self::URL_TEMPORARY_PASSWORD_RESET,
                "destination_url" => self::URL_TEMPORARY_PASSWORD_RESET,
            ]);
        }

        $saveToSession = [
            'username' => $username,
            'logged_in' => TRUE
        ];
        $this->session->set_userdata($saveToSession);

        return $this->json($responseData);
    }


    /**
     * Sends password reset email based on given email address
     *
     * @return \CI_Output|void
	 * @throws NotFoundHttpException If email address not found
	 * @throws \Exception If something went wrong while sending emails (ie. mail config not set properly)
     */
    public function request_reset_password()
    {
        $accountModel = $this->accountModel;

        $emailAddress = $this->input->post("email");

        // validate email address
		$this->load->library('email');
		$this->form_validation->set_data(["email" => $emailAddress]);
        $this->form_validation->set_rules("email", "Email", "required|valid_email");
        if (!$this->form_validation->run()) {
            return $this->json(["errors" => $this->form_validation->error_array()], self::HTTP_BAD_REQUEST);
		}		

        $orgWithEmail = $accountModel->findOrganisationByEmail($emailAddress);
        if (!$orgWithEmail) {
			throw new NotFoundHttpException("Email {$emailAddress} not found");
        }

        // create email contents
        $organizationId = Arr::get($orgWithEmail, "id");

        $resetToken = $this->generatePasswordResetToken([
            "id" => $organizationId,
            "email" => Arr::get($orgWithEmail, "email"),
        ]);

		// link to be send via email
		// eg. http://localhost:3000/password-reset?token=eyJhbGciOi...
        $resetLink = FRONT_END_URL . self::URL_PASSWORD_RESET . "?token={$resetToken}";

		$this->db->trans_begin();

		try {
			$emailContentsData = [
				"username" => Arr::get($orgWithEmail, "username", null),
				"password_reset_url" => $resetLink,
			];
			$emailContents = $this->render("email/password_reset_html", $emailContentsData, true, true);
	
			// compose email
			$adminEmail = "info@healthcaremgr.net";
	
			$email = $this->email;
			$email->set_mailtype('html');
			$email->from($adminEmail, "HCM Org Portal");
			$email->to($emailAddress);
			$email->subject("HCM Org Portal: Reset your password");
			$email->message($emailContents);
			$email->bcc($adminEmail);
			$isEmailSent = $email->send(); // can throw exception if email isn't configured properly
	
			if (!$isEmailSent) {
				throw new \Exception("Failed to send password reset email");
			}

            // save reset token to db
            $accountModel->updatePasswordResetToken($resetToken, $organizationId);
			$this->db->trans_commit();
	
			$responseData = ["message" => "Password reset email sent"];
			return $this->json($responseData);
		} catch (\Exception $e) {
			$this->db->trans_rollback();
			throw $e;
		}

    }

    
    /**
     * Verify password reset token. Password reset tokens are issued as JWT tokens
	 * 
	 * Token is not valid if
	 * - Token is missing or malformed
	 * - Token has been tampered during transport
	 * - Token has been expired (after 1 hour)
     *
     * @return \CI_Output
     */
    public function verify_reset_password_token()
    {
        $accountModel = $this->accountModel;

        // check if empty
        $token = $this->input->get("token"); // from query params
        if (!$token) {
			throw new BadRequestHttpException("Missing token");
        }

		// search password reset token
        $result = $accountModel->findOrganisationPasswordResetToken($token);
        if (!$result) {
			throw new NotFoundHttpException("Password reset link not found");
        }

        // verify token
        try {
            $claims = $this->ensureTokenValid($token);
            $expiry = $claims['exp'];
            $expireAt = date("Y-m-d H:i:s", $expiry);
            return $this->json(["message" => "Token verified", "expires_at" => $expireAt]);

        } catch (\Exception $e) {
            throw new BadRequestHttpException("Invalid token");
        }
    }


    /**
     * Reset password. Client must supply email, organisation id, password, password_repeat, token
     *
     * @return \CI_Output
     */
    public function reset_password()
    {
        $accountModel = $this->accountModel;

        // validation rules
        $postdata = $this->input->post(["id", "token", "new_password", "password_repeat", "email"]);
        $this->form_validation->set_data($postdata);
        $this->form_validation->set_rules([
            ["field" => "id", "rules" => "required"],
            ["field" => "email", "rules" => "required|valid_email"],
            ["field" => "new_password", "rules" => "required|min_length[6]"],
            ["field" => "password_repeat", "rules" => "required|matches[new_password]"],
            [
                "field" => "token", 
                "rules" => [
                    "required",
                    ["token_exists", function($token) use ($accountModel) {
                        return !!$accountModel->findOrganisationPasswordResetToken($token);
                    }] 
                ],
                "errors" => [
                    "token_exists" => "Password reset token not found"
                ]
            ],
        ]);

        // validation fails
        if (! $this->form_validation->run()) {
            $validationErrors = $this->form_validation->error_array();
            return $this->json(["errors" => $validationErrors], self::HTTP_BAD_REQUEST);
        }

        // actual password reset,
        // use transactions for safety
        $this->db->trans_begin();

        try {
            $hashedPassword = password_hash(Arr::get($postdata, "new_password"), PASSWORD_BCRYPT);
            $isPasswordUpdated = $accountModel->resetPassword(
                $hashedPassword, 
                Arr::get($postdata, "id"), 
                Arr::get($postdata, "token")
            );

            if (!$isPasswordUpdated) {
                throw new \Exception("Failed to reset password");
            }

            $this->db->trans_commit();
            return $this->json(["message" => "Password reset successfully"]);

        } catch (\Exception $e) {
			$this->db->trans_rollback();
			throw $e;
        }
    }


    /**
     * Endpoint for temporary password resets
     *
     * @return \CI_Output
     * @throws UnauthorizedHttpException When one of these happens
     * - Headers does not contain `Authorization: Bearer ....`
     * - Could not extract user claims from token
     * - Token expired
     * - User already changed their password
     * @throws \Exception When update fails (db error)
     */
    public function temp_password_reset()
    {
        $this->ensureUserLoggedIn();

        $accountModel = $this->accountModel;

        // check authorization header
        $authorizationHeader = $this->input->get_request_header("Authorization");
        $payload = $this->ensureValidAuthHeader($authorizationHeader);

        // validation
        $postData = $this->input->post(["new_password", "password_repeat"]);
        $this->form_validation->set_data($postData);
        $this->form_validation->set_rules([
            ["field" => "new_password", "label" => "New password", "rules" => "required|min_length[6]"],
            ["field" => "password_repeat", "label" => "Password repeat", "rules" => "required|min_length[6]|matches[new_password]"],
        ]);

        if (!$this->form_validation->run()) {
            return $this->json(["errors" => $this->form_validation->error_array()], self::HTTP_BAD_REQUEST);
        }

        // do not allow if user is not first time logging in
        if (!$accountModel->isFirstTimeLogin($payload)) {
            throw new UnauthorizedHttpException("You are no longer logged in for the first time");
        }
        
        // new password
        $new_password = $postData["new_password"];
        $new_password_hashed = password_hash($new_password, PASSWORD_DEFAULT);

        // update password and status in db
        $this->db->trans_begin();

        try {
            $accountModel->updatePassword($payload['id'], $new_password_hashed);
            $this->db->trans_commit();

            // Claims are updated at this point, log the user out
            return $this->json([
                "message" => "Password has been updated. Please login again with your new credentials.",
                "destination_url" => self::URL_LOGOUT,
            ]);

        } catch (\Exception $e) {
            $this->db->trans_rollback();
            throw $e; // internal server error
        }
    }



    /**
     * Generates a password reset token
     *
     * @param array $passwordResetDetails
     * @return string
     */
    protected function generatePasswordResetToken($passwordResetDetails)
    {
        $time = time();
        $passwordResetExpiry = JWT_TOKEN_EXPIRY;

        $signer = new Sha256(); // use hmac because were just using simple secret key (instead of pub/priv keys) 
        $key = new Key(JWT_SECRET_KEY);

        $builder = (new Builder())
            //->issuedBy('http://localhost:8000') // iss
            //->permittedFor('http://localhost:3000') // aud
            // ->identifiedBy('my_token_id') // jti
            ->issuedAt($time) // iat
            ->canOnlyBeUsedAfter($time) // bnf
            ->expiresAt($time + $passwordResetExpiry); // exp

        foreach ($passwordResetDetails as $claim => $value) {
            $builder->withClaim($claim, $value); // additional claims
        }

        $token = $builder->getToken($signer, $key);
        return $token->__toString();
    }


    /**
     * Create the JWT token based on given claims
     *
     * @param array|object $claims
     * @return string
     */
    protected function generateJWTToken($claims, $secret = null)
    {
        $signer = new Sha256(); // use hmac because were just using simple secret key (instead of pub/priv keys) 
        $key = new Key(JWT_SECRET_KEY);

        $time = time();
        $builder = (new Builder())
            // ->issuedBy('http://localhost:8000') // iss
            // ->permittedFor('http://localhost:3000') // aud
            // ->identifiedBy('my_token_id') // jti
            ->issuedAt($time) // iat
            ->canOnlyBeUsedAfter($time) // bnf
            ->expiresAt($time + JWT_TOKEN_EXPIRY); // exp

        foreach ($claims as $claim => $value) {
            $builder->withClaim($claim, $value); // additional claims
        }

        $token = $builder->getToken($signer, $key);
        return $token->__toString();
    }



    /**
     * Check if user claims can be extracted from bearer token 
     * provided in authorization header
     *
     * @param string|null $authorizationHeader
     * @return array User claims
     * @throws \app\exceptions\UnauthorizedHttpException 
     *      When bearer token could not be extracted
     */
    protected function ensureValidAuthHeader($authorizationHeader)
    {
        if (!$authorizationHeader) {
            throw new UnauthorizedHttpException("Missing authorization header");
        }

        $matches = [];
        if (!preg_match('/Bearer\s(\S+)/', $authorizationHeader, $matches)) {
            throw new UnauthorizedHttpException("Invalid authorization header format");
        }

        $bearer = $matches[1];
        return $this->ensureTokenValid($bearer);
    }
}
