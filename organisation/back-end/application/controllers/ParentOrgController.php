<?php

use app\exceptions\UnauthorizedHttpException;
use \Illuminate\Support\Arr;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller to handle requests related to parent organization
 * 
 * @author Yeasir Majumder <yeasir@ydt.com.au>
 * 
 */

class ParentOrgController extends \MY_Controller {

    use formCustomValidationRules;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Org_model');
        $this->load->model('basic_model');
        $this->load->library('form_validation');
        // $this->form_validation->CI = & $this;
    }

    /**
     * Function to return organization details
     */
    public function getOrgDetails() {
        $orgDetail = $this->ensureUserLoggedIn();
        $orgId = $orgDetail->id;

        require_once ADMIN_MODULE_ROOT . '/Classes/organisation/Organisation.php';
        verify_request_origin();

        $organization = new OrganisationClass\Organisation();

        $request = request_handler();

        $row = null;
        if (!empty($orgId)) {
            $organization->setParent_org('');
            $organization->setId($orgId);
            $row = $organization->get_organisation_profile();
        }

        echo json_encode($row);
        exit();
    }


    /**
     * Controller function to update organization details
     */
    public function updateOrgDetails() {
        $orgDetail = $this->ensureUserLoggedIn();
        $requestData = formRequestHandler();
        if (!empty($requestData)) {
            $validation_rules = array(
                array('field' => 'tax', 'label' => 'Payroll tax', 'rules' => 'required'),
                array('field' => 'gst', 'label' => 'GST', 'rules' => 'required'),
                array('field' => 'street', 'label' => 'Street', 'rules' => 'required'),
                array('field' => 'postcode', 'label' => 'Postcode', 
                    'rules' => ['required', 'callback_postcode_check'],
                    'errors' => ['postcode_check' => 'Invalid postcode']
                ),
                array('field' => 'suburb', 'label' => 'Suburb', 'rules' => 'required'),
                array('field' => 'website', 'label' => 'Website', 'rules' => 'required|valid_url'),
                array('field' => 'primary-phone', 'label' => 'Phone(primary)', 'rules' => 'required'),
                array('field' => 'primary-email', 'label' => 'Email(primary)', 'rules' => 'required'),
            );

            $this->form_validation->set_data($requestData);
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $ocs_row = $this->Org_model->update_org($requestData);
                $img_err = isset($ocs_row['image_status']) ? $ocs_row['image_status'] : '';
                /* logs */
                // $this->loges->setTitle("Update Organisation: " . $ocs_row['org_id']);
                // $this->loges->setUserId($request->adminId);
                // $this->loges->setDescription(json_encode($requestData));
                // $this->loges->setCreatedBy($request->adminId);
                // $this->loges->createLog();

                $return = array('status' => true, 'msg' => 'Organisation updated successfully.' . $img_err, 'orgId' => $ocs_row['org_id']);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }
            echo json_encode($return);
        }
        
    }

    /**
     * Controller function to update a contact's details
     * @return json
     */
    public function updateContactDetails() {
        $validPhoneNumberPattern = $this->Org_model::PHONE_NUMBER_PATTERN;
        $orgDetail = $this->ensureUserLoggedIn();
        $requestData = formRequestHandler();

        if (!empty($requestData)) {
            $validation_rules = array(
                array('field' => 'first-name', 'label' => 'First name', 'rules' => 'required'),
                array('field' => 'position', 'label' => 'Position', 'rules' => 'required'),
                array('field' => 'department', 'label' => 'Department', 'rules' => 'required'),
                array('field' => 'contact-type', 'label' => 'Contact type', 'rules' => 'required'),
                array('field' => 'primary-phone', 'label' => 'Primary phone', 
                    'rules' => ['required', "callback_phone_number_check[${validPhoneNumberPattern}]"],
                    'errors' => ['phone_number_check' => 'Invalid phone number']
                ),
                array('field' => 'primary-email', 'label' => 'Primary phone', 
                    'rules' => ['required', "valid_email"]
                ),
            );

            $this->form_validation->set_rules($validation_rules);
            $this->form_validation->set_message('callback_phone_number_check', 'Error Message');

            $this->form_validation->run();
            if ($this->form_validation->run()) {
                /* logs */
                // $this->loges->setUserId($request->adminId);
                // $this->loges->setCreatedBy($request->adminId);

                // if (isset($org_data['edit_mode']) && $org_data['edit_mode'] == 1)
                //     $this->loges->setTitle("Update site Contact: " . $org_data['orgId']);
                // else
                //     $this->loges->setTitle("Add site Contact: " . $org_data['orgId']);

                // $this->loges->setDescription(json_encode($data));
                // $this->loges->createLog();

                require_once ADMIN_MODULE_ROOT . '/Classes/organisation/Organisation_all_contact.php';

                $objOrg = new Organisation_all_contactClass\Organisation_all_contact();
                $objOrg->setOrganisationId($requestData['org_id']);
                $objOrg->setName($requestData['first-name']);
                $objOrg->setLastname($requestData['last-name']);
                $objOrg->setPosition($requestData['position']);
                $objOrg->setDepartment($requestData['department']);
                $objOrg->setType($requestData['contact-type']);

                if (isset($requestData['edit_mode']) && $requestData['edit_mode'] == 1) {
                    $objOrg->setId($requestData['id']);
                    $objOrg->update_org_contact();
                    $contactId = $requestData['id'];
                    $msg = 'Contact updated successfully.';
                } else {
                    $contactId = $objOrg->add_org_contact();
                    $msg = 'Contact added successfully.';
                }
                $this->Org_model->add_org_contact($requestData, $contactId);
                $return = array('status' => true, 'msg' => $msg);
            } else {
                $errors = $this->form_validation->error_array();
                $return = array('status' => false, 'error' => implode(', ', $errors));
            }

            echo json_encode($return);
        }
    }
}