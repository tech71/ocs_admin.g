<?php

class CI
{
    /**
     * Contains reference to current controller being executed
     *
     * @var \MY_Controller
     */
    public static $app;
}