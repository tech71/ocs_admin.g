<?php

use app\entities\OrganisationIdentity;
use Illuminate\Support\Arr;
use app\exceptions\UnauthorizedHttpException;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'traits/formCustomValidationRules.php';
/**
 * My custom base controller
 * 
 * @property-read \Loges $loges Log builder library. This library is autoloaded
 */
class MY_Controller extends \CI_Controller
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_INTERNAL_ERROR = 500;

    const HEADER_AUTHORIZATION = "Authorization";


    protected $_layout = "layouts/main";

    /**
     * Stores flash data
     *
     * @var array
     */
    protected $_flashdata = [];


    public function __construct()
    {
        parent::__construct();
    }
    

    /**
     * Return json output via `CI_Output`. 
     * This will also append flash data as `_flash` array
     *
     * @param array|mixed $data Will be json encoded via `\json_encode' function
     * @param int $statusCode Default to `200` which is okay
     * @return \CI_Output
     */
    protected function json($data, $statusCode = 200)
    {
        $response = $data;
        if (!empty($this->_flashdata)) {
            $response = array_merge($data, [
                "_flash" => $this->_flashdata
            ]);
        }

        return $this->output
            ->set_content_type("json")
            ->set_status_header($statusCode)
            ->set_output(\json_encode($response));
    }


    /**
     * Renders a view file with a layout using `MY_Output`.
     * You can optionally render it as view partial without the layout 
	 * or you can return it as string content
     *
     * @param string $view The view file that will be sent along with layout to `MY_Output`
     * @param array|\ArrayAccess $data 
     * @param boolean $isPartial Render view file without using layout
	 * @param boolean $returnString Return as string
     * @return \MY_Output|string
     */
    protected function render($view, $data = [], $isPartial = false, $returnString = false)
    {
        $partialContents = $this->load->view($view, $data, true);

        if ($isPartial) {
			return $returnString ? $partialContents : $this->output->set_output($partialContents);
        }

        $contents = $this->load->view($this->_layout, [
            "__content__" => $partialContents
        ], true);

        return $returnString ? $contents : $this->output->set_output($contents);
    }


    /**
     * Gets user claims from authorization header or else throw exception
     *
     * @return OrganisationIdentity
     */
    protected function ensureUserLoggedIn()
    {
        $authorizationHeader = $this->input->get_request_header(self::HEADER_AUTHORIZATION);
        if (!$authorizationHeader) {
            throw new UnauthorizedHttpException("Missing authorization header");
        }

        $matches = [];
        if (!preg_match('/Bearer\s(\S+)/', $authorizationHeader, $matches)) {
            throw new UnauthorizedHttpException("Invalid authorization header format");
        }

        $bearer = $matches[1];
        try {
            $claims = $this->ensureTokenValid($bearer);
            return OrganisationIdentity::from((array)$claims);
        } catch (\Exception $e) {
            throw new UnauthorizedHttpException($e->getMessage());
        }

    }

    /**
     * Decodes JWT token into name-value pairs (aka. claims). 
     * 
     * NOTE: All values in resulting `$claims` array will be strings
     * 
     * @param string $token
     * @return array
     * @throws \Exception If one of these happens:
     * - Could not verify token from our key
     * - Invalid key (eg. token modified, token expired)
     */
    protected function ensureTokenValid($token)
    {
        $tokenParser = (new Parser)->parse((string) $token);

        if (!$tokenParser->verify(new Sha256(), JWT_SECRET_KEY)) {
            throw new \Exception("Invalid token");
        }
        
        $data = new ValidationData();
        $isValidToken = $tokenParser->validate($data);

        if (!$isValidToken) {
            throw new \Exception("Invalid token");
        }

        $claims = [];
        foreach ($tokenParser->getClaims() as $name => $claimBasic) {
            $claims[$name] = $claimBasic->getValue();
        }

        return $claims;
    }


    /**
     * Sets flash data. 
     * Flash data is rendered in frontend as bootstrap alerts
     * 
     * Note: If your controller action throws an exception, 
     * the flash data array will not be sent
     *
     * @param 'success'|'danger'|'warning'|'info' $key
     * @param string $message
     * @return void
     */
    protected function flash($key, $message) 
    {
        $this->_flashdata[$key] = $message;
    }

}
