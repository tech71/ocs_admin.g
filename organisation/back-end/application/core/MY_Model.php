<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Contains additional extensions methods for `\CI_Model`
 */
class MY_Model extends \CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // additional extension methods
}
