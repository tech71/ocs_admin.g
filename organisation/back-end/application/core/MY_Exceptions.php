<?php
/**
 * Undocumented class
 */
class MY_Exceptions extends \CI_Exceptions
{
    const RESPONSE_JSON = "application/json";
    const RESPONSE_HTML = "text/html";
    /**
     * Displays exception in JSON format
     *
     * @param \Exception $exception
     * @return void
     */
    public function show_exception($exception)
	{
        if (EXCEPTION_OUTPUT_FORMAT === static::RESPONSE_HTML) {
            return parent::show_exception($exception);
        }
        $statusCode = 500;
        $name = "Error";
        if ($exception instanceof \app\exceptions\HttpException) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
        }
        $details = [
            "name" => $name,
            "status_code" => $statusCode,
            "message" => $exception->getMessage(),
        ];
        if (in_array(ENVIRONMENT, ["development"])) {
            $details = array_merge($details, [
                "code" => $exception->getCode(),
                "file" => $exception->getFile(),
                "line" => $exception->getLine(),
                "trace" => $exception->getTrace(),
                // "trace_string" => explode("\n", $exception->getTraceAsString()),
            ]);
        }
        $json_encode_options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        $CI = null;
        if (function_exists("get_instance") && class_exists(\CI_Controller::class)) {
            $CI =& get_instance();
        }
        if ($CI && $CI->output) {
            $CI->output
                ->set_status_header($statusCode)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(\json_encode($details, $json_encode_options))
                ->_display();
        } else {
            \set_status_header($statusCode);
            header('Content-Type: application/json; charset=UTF=8');
            echo \json_encode($details, $json_encode_options);
        }
	}
}