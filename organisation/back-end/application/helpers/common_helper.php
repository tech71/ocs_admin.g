<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * used in echoing object / array in particular for debugging
 */
function pr($str) {
	echo "<pre>";
	print_r($str);
	echo "</pre>";
}


/**
 * Note: Borrowed from `osc_admin/backend/application/common_helper`
 *
 * @param string $action Either `encrypt` or `decrypt`
 * @param string $string
 * @return string
 */
function encrypt_decrypt($action, $string)
{
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

/**
 * Function that returns POST request data
 * 
 * @return array the request data
 */
function request_handler() {
    $requestData = file_get_contents('php://input');
    
    $requestData = json_decode($requestData);
    return $requestData;
}

/**
 * Function that returns POST request data that has attachments
 * 
 * @return array the request data
 */
function formRequestHandler() {
    verify_request_origin();

    $CI = & get_instance();
    $data = $CI->input->post();

    return $data;
}

/**
 * Function to upload file
 * @param array $config_ary - the array that holds configuration detail for upload
 * @return array the request data
 */
function do_upload($config_ary) {
    $CI = & get_instance();
    $response = array();
    if (!empty($config_ary)) {
		$directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

        $config['upload_path'] = $directory_path;
        $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
        $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '5120';
        $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
        $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';


        create_directory($directory_path);

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload($config_ary['input_name'])) {
            $response = array('error' => $CI->upload->display_errors());
        } else {
            $response = array('upload_data' => $CI->upload->data());
        }
    }
    return $response;
}

/**
 * Function that created a directory
*/
function create_directory($directoryName) {
    if (!is_dir($directoryName)) {
        mkdir($directoryName, 0755);
        fopen($directoryName . "/index.html", "w");
    }
}

/**
 * function to check the api request originated from allowed front end domains
 * @param - no params
 * 
 * @return json json array
 */
function verify_request_origin() {
    $CI = & get_instance();
    $request_origin = (!empty($_SERVER['HTTP_ORIGIN'])) ? trim($_SERVER['HTTP_ORIGIN'],'/') : '';
    $allowed_origins = $CI->config->item('allowed_domains');
    
    if (!empty($allowed_origins) && in_array($request_origin, $allowed_origins)) {
        return true;
    } else {
        echo json_encode(array('status' => false, 'error' => $CI->config->item('unknown_origin')));
        exit();
    }
}