<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Note: Borrowed from `osc_admin/backend/application/common_helper`
 *
 * @param string $action Either `encrypt` or `decrypt`
 * @param string $string
 * @return string
 */
function dd($var)
{
    echo('<pre>');
    var_dump($var);
    die();
}