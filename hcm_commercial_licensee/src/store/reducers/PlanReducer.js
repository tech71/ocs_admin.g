import * as actionType from '../actions';

const initialState = {

    planDetails: {
        plan_type:'monthly plan, not locked in',
        next_payment_due:'11th Oct, 2019',
        payment_type:'Direct debit, VISA card ending 1298',
        payment:'monthly',
        payment_amount:'$3850',
        users:'20'
    },

    currentUsersDetails:{
        profiles_filled:'12/15',
        available_spaces:'3/15'
    },

    currentUsersList:[
        {id:"1", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'Admin'},
        {id:"2", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'Admin'},
        {id:"3", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'User'},
        {id:"4", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'Admin'},
        {id:"5", status:'inactive', date:'17/09/2019', name:'Amella Jordan', post:'User'},
        {id:"6", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'User'},
        {id:"7", status:'inactive', date:'17/09/2019', name:'Amella Jordan', post:'User'},
        {id:"8", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'Admin'},
        {id:"9", status:'inactive', date:'17/09/2019', name:'Amella Jordan', post:'Admin'},
        {id:"10", status:'active', date:'17/09/2019', name:'Amella Jordan', post:'User'}
       
    ],

    billingList: [
        {id:'1', card:'VISA', number:'1259', expiry:'06/20', primary:'true'},
        {id:'2', card:'American Express', number:'2058', expiry:'04/25', primary:'false'},
        {id:'3',card:'MasterCard', number:'1269', expiry:'04/32', primary:'false'}
    ],


    paymentHistory:[
        {id:'1', paid:'false', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'2', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'3', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'4', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'5', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'6', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'7', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
        {id:'8', paid:'true', invoice:'000111', amount:'$51,727.50', date:'15/10/2019'},
    ],

    paymentOptions:[
        {value:'1', label:'VISA ending 0015', primary:'false'},
        {value:'2', label:'American Express ending 2058', primary:'false'},
        {value:'3', label:'Master Card ending 1269', primary:'true'}
    ]


}

const reducer = (state = initialState, action) => {
    switch (action.type) {

    }
    return state;
}

export default reducer;