import { combineReducers } from 'redux';
import DashboardReducer from './DashboardReducer';
import NotificationReducer from './NotificationReducer';
import ProfileReducer from './ProfileReducer';
import EnquiriesReducer from './EnquiriesReducer';
import PlanReducer from './PlanReducer';

export default combineReducers({ 
    DashboardReducer,
    NotificationReducer,
    ProfileReducer,
    EnquiriesReducer,
    PlanReducer
});