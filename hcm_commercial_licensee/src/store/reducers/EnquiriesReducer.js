import * as actionType from '../actions';

const initialState = {

    enquiriesList:[
        {id:"1", status:'enabled', status2:'true', enquiry_number:'1001', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"2", status:'received', status2:'true', enquiry_number:'1002', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"3", status:'closed', status2:'false', enquiry_number:'1003', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"4", status:'closed', status2:'false', enquiry_number:'1004', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"5", status:'closed', status2:'false', enquiry_number:'1005', date:'17/09/2019', title:'[Enquiry Titlen]'},
        {id:"6", status:'closed', status2:'false', enquiry_number:'1006', date:'17/09/2019', title:'[Enquiry Titlen]'},
        {id:"7", status:'closed', status2:'false', enquiry_number:'1007', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"8", status:'closed', status2:'false', enquiry_number:'1008', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"9", status:'closed', status2:'false', enquiry_number:'1009', date:'17/09/2019', title:'[Enquiry Title]'},
        {id:"10", status:'closed', status2:'false', enquiry_number:'1010', date:'17/09/2019', title:'[Enquiry Title]'}
    ],

    enquiryDetails:{
        id:"1",
        enquiry_number:'1001',
        enquiries:[
            {
                status:'active',
                date:'17/09/2019',
                title:'[Enquiry Title]',
                description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',

            },
            {
                status:'closed',
                date:'17/09/2019',
                title:'[Enquiry Title]',
                description:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',

            }
        ]

    }

}

const reducer = (state = initialState, action) => {
    switch (action.type) {

    }
    return state;
}

export default reducer;