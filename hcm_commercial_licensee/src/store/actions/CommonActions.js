import { ROUTER_PATH, BASE_URL, LOGIN_DIFFERENCE } from '../../services/config';
import moment from 'moment-timezone';
import axios from 'axios';

export function postData(url, data) {

    var request_data = { 'token': getLoginToken(), 'data': data }
    return dispatch => {
        return new Promise((resolve, reject) => {
            fetch(BASE_URL + url, {
                method: 'POST',
                body: JSON.stringify({ request_data })
            })
                .then((response) => response.json())
                .then((responseJson) => {

                    if (responseJson.status) {
                        dispatch(setLoginTime(moment()))
                    }

                    if (responseJson.token_status) {
                        dispatch(logOut());
                    }
                    if (responseJson.server_status) {
                        dispatch(logOut());
                    }

                    resolve(responseJson);
                })
                .catch((error) => {
                    reject(error);
                    alert(error);
                });
        });

    }
}

export function postFileData(url, data, obj) {

    data.append('token', getLoginToken());

    return dispatch => {
        return new Promise((resolve, reject) => {
            axios.post(BASE_URL + url, data, {
                onUploadProgress: progressEvent => {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    if (obj != undefined && obj != null && typeof obj == 'object' && obj.hasOwnProperty('progress')) {
                        obj.setState({ progress: percentCompleted })
                    }
                }
            }).then((response) => {
                // response = response.data;
                console.log(response);

                // // if jwt token status true mean token not verified
                // if (response.token_status) {
                //    dispatch(logOut()); 
                // }

                // // if token is verified then update date client side time
                // if (response.status) {
                //     dispatch(setLoginTime(moment()))
                // }

                // // if pin status true mean pin token not verified
                // if (response.pin_status) {
                //     //destroyPinToken();
                //     let type = response.hasOwnProperty('pin_type') ? response.pin_type : '';
                //     destroyPinToken(type);
                // }

                // // if ip status true mean ip address change of current user
                // if (response.ip_address_status) {
                //     dispatch(logOut()); 

                // }

                // // if server status true mean request not came at over server
                // if (response.server_status) {
                //     dispatch(logOut()); 
                // }

                // // if permission status true mean not have permission to access this
                // if (response.permission_status) {
                //     window.location = '/admin/no_access';
                // }

                resolve(response);
            })
                .catch((error) => {
                    reject(error);
                    console.error(error);
                });
        });

    }
}


export function showMsg(type, data) {
    return {
        type: type,
        payload: data
    }
}
export function resetMsg(type) {
    return {
        type: type
    }
}

export function setLoginToken(token) {
    return dispatch => {
        localStorage.setItem("commercial_licensee_Token", token);
    }
}

export function getLoginToken() {
    return localStorage.getItem("commercial_licensee_Token");
}

export function getLoginTIme() {
    return localStorage.getItem("commercial_licensee_dateTime")
}

export function setLoginTime(dateTime) {
    return dispatch => {
        localStorage.setItem("commercial_licensee_dateTime", dateTime);
    }
}


export function checkLogin() {
    return dispatch => {
        if (getLoginToken()) {
            return true;
        } else {
            return false
        }
    }
}

export function checkNotLogin() {
    return dispatch => {
        if (!getLoginToken()) {
            return true;
        } else {
            return false
        }
    }
}

export function logOut() {
    return dispatch => {
        localStorage.removeItem('commercial_licensee_Token');
        localStorage.removeItem('commercial_licensee_dateTime');
        window.location.replace("/");
    }
    // return true;
}

export function check_loginTime() {

    var DATE_TIME = getLoginTIme();
    var server = moment(DATE_TIME)
    var currentDateTime = moment()
    const diff = currentDateTime.diff(server);
    const diffDuration = moment.duration(diff);

    return dispatch => {
        if (diffDuration.days() > 0) {
            dispatch(logOut());
        }
        else if (diffDuration.hours() > 0) {
            dispatch(logOut());
        }
        else if (diffDuration.minutes() >= LOGIN_DIFFERENCE) {
            dispatch(logOut());
        }
    }
}

