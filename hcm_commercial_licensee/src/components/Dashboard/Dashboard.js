import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH} from 'services/config';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <React.Fragment>
                
                <section className="dashboardSection cmnSec">
                    <h1 className="mainHeader">Account Dashboard</h1>

                    <div className="transBox">
                        <Row>
                            <Col xs={12} sm={12} md={5} lg={4} xl={3}>
                                <div className="white_bx__">
                                    <div className="profilePic">
                                        <div className="profInitial">JT</div>
                                        <span className={'addImg'}><i className="fa fa-plus"></i></span>
                                    </div>
                                    <div className={'mt-4 text-center profNam_det__'}>
                                        <h5 >Caring For You</h5>
                                        <h6>Juliette Townsend</h6>
                                        <Link to={`${ROUTER_PATH}profile`}>
                                            <button className="btn btn1 mt-4 mb-2">Update Profile</button>
                                            </Link>
                                        <Link to={'#'}>
                                            <div className={'color1 mt-2'}><strong>Go to HCM app</strong></div>
                                        </Link>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={12} sm={12} md={7} lg={8} xl={9}>
                                <Row>
                                    <Col xs={12} md={12} lg={12} xl={6}>
                                        <div className="white_bx__">
                                            <div>
                                                <h3 className="color1 f-bolder mt-2">My Plan</h3>

                                                <div className={'mt-4 color2'}>
                                                    <h5 className={'f-bold mb-0'}>Intermediate Plan</h5>
                                                    <div className={''}>Monthly plan not locked in</div>
                                                </div>

                                                <div className={'mt-4 color2'}>
                                                    <div className={'f-bold'}>Next payment is scheduled for</div>
                                                    <div>11th Oct 2019</div>
                                                    <div>On VISA ending **** **** **** 1298</div>
                                                </div>

                                                <Link to={'#'}>
                                                    <button className="btn btn1 mt-4 mb-2">View Plan Details</button>
                                                </Link>

                                                <Link to={`${ROUTER_PATH}plan/payment_history`}>
                                                    <div className={'color1 mt-2 mb-2'}><strong>View previous payments</strong></div>
                                                </Link>

                                            </div>
                                        </div>

                                        <div className="white_bx__">
                                            <div>
                                                <h3 className="color1 f-bolder mt-2">Users</h3>

                                                <div className={'mt-4 color2'}>
                                                    <div className={''}>20 users connected to HCM Plan</div>
                                                    <ul className={'actUl mt-2'}>
                                                        <li className={'active'}>18 active users</li>
                                                        <li className={'inactive'}>2 inactive users</li>
                                                    </ul>
                                                </div>



                                                <Link to={`${ROUTER_PATH}plan/current_users`}>
                                                    <button className="btn btn1 mt-3 mb-2">Manage Users</button>
                                                </Link>



                                            </div>
                                        </div>
                                    </Col>
                                    <Col xs={12} md={12} lg={12} xl={6}>

                                        <div className="white_bx__">
                                            <div>

                                                <h3 className="color1 f-bolder mt-2 notifyHead">
                                                    Notifications<span >30</span>
                                                </h3>

                                                <div>
                                                    <Link to={`${ROUTER_PATH}notification_list`}>
                                                        <button className="btn btn1 mt-4 mb-2">Notifications Page</button>
                                                    </Link>
                                                </div>



                                            </div>
                                        </div>

                                        <div className="white_bx__">
                                            <div>
                                                <h3 className="color1 f-bolder mt-2">Quick Help</h3>

                                                <div className={'mt-3 list_1QH'}>
                                                    <div>
                                                        <Link to={'#'} >
                                                            <span className={'color1 t_underline'}>[00015 - Enquiry Title]</span>
                                                            <span className={'active_bt ml-2'}>Active</span>
                                                        </Link>
                                                    </div>
                                                </div>

                                                <div className={'mt-3 list_2QH'}>
                                                    <div>
                                                        <Link to={'#'} >
                                                            <span className={'color1 t_underline'}>[Quick Help Link 1]</span>
                                                        </Link>
                                                    </div>
                                                    <div>
                                                        <Link to={'#'} >
                                                            <span className={'color1 t_underline'}>[Quick Help Link 2]</span>
                                                        </Link>
                                                    </div>
                                                    <div>
                                                        <Link to={'#'} >
                                                            <span className={'color1 t_underline'}>[Quick Help Link 3]</span>
                                                        </Link>
                                                    </div>
                                                    <div>
                                                        <Link to={'#'} >
                                                            <span className={'color1 t_underline'}>[Quick Help Link 4]</span>
                                                        </Link>
                                                    </div>
                                                </div>

                                                <Link to={`${ROUTER_PATH}help`}>
                                                    <button className="btn btn1 mt-4 mb-2">View More FAQ</button>
                                                </Link>

                                                <Link to={`${ROUTER_PATH}help/enquiries/new_enquiry`}>
                                                    <div className={'color1 mt-2 mb-2'}><strong>Create Ticket for Issue</strong></div>
                                                </Link>

                                            </div>
                                        </div>
                                    </Col>
                                </Row>

                            </Col>
                        </Row>
                    </div>

                </section>
            </React.Fragment>


        );
    }
}

export default Dashboard;