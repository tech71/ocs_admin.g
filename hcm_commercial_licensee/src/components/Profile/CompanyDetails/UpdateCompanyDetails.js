import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import jQuery from "jquery";
import { Col, Row } from 'react-bootstrap';
import Select from 'react-select-plus';
import ProfileSideLinks from '../ProfileSideLinks';
import MainHeading from 'hoc/MainHeading';
import { ROUTER_PATH } from '../../../services/config';
import { Link } from 'react-router-dom';
class UpdateCompanyDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            profile_details_edit: false,
            editObj: {
                name: 'Caring For You',
                abn: '01234567890',
                address: '213, care street, Melbourne 3000 VIC',
                suburb: 'VIC',
                state: 'melbourne',
                postcode: '3000',
                contact: ['987654321'],
                email: ['wecarecaringforyou@gov.vic.au'],
                website: 'www.caringforyou@gov.vic.a',
                admin_users: '12',
                billing: true,
                billing_suburb: ''
            }

        }
    }

    editHandler = (key) => {

        var mode = false;
        if (key == 'e') {
            mode = true
        }
        this.setState({
            profile_details_edit: mode
        })
    }

    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.type == 'checkbox' ? !editObj[e.target.name] : e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#updateProfile').validate();
        if (jQuery('#updateProfile').valid()) {
            alert('valid');
        }
    }

    removeRowHadler = (key, index) => {
        let editObj = this.state.editObj;
        editObj[key].splice(index, 1);
        this.setState({ editObj: editObj })
    }

    addRowHadler = (key) => {
        let editObj = this.state.editObj;
        editObj[key].push('');
        this.setState({ editObj: editObj })
    }

    selectchange = (e, key) => {
        let editObj = this.state.editObj;
        editObj[key] = e;
        this.setState({ editObj });
    }

    inputArrHandler = (e, key, index) => {
        let editObj = this.state.editObj;
        editObj[key][index] = e.target.value;
        this.setState({ editObj });
    }

    render() {

        var statesOpt = [
            { value: 'melbourne', label: 'Melbourne' },
            { value: 'state2', label: 'State 2' }
        ];
        return (
            <React.Fragment>

                <section className="detailSection cmnSec">
                    <MainHeading title={'Company Details'} />

                    <div className="transBox">

                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ProfileSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>
                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <div className={'company_details_edit'}>

                                            <div>
                                                <h3 className={'color1 f-bolder'}>Address</h3>

                                                <ul className={'dets_Ul__ mt-3'}>
                                                    <li><b>Name:</b> {this.state.editObj.name}</li>
                                                    <li><b>ABN:</b> {this.state.editObj.abn}</li>
                                                </ul>

                                                <form id={'updateProfile'} >
                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Address</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            data-rule-required={true}
                                                            name={'address'}
                                                            value={this.state.editObj.address || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Suburb</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            data-rule-required={true}
                                                            name={'suburb'}
                                                            value={this.state.editObj.suburb || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <Row>
                                                            <Col xl={9} lg={4} md={6} xs={12}>
                                                                <label className={'lab1'}>Post Code</label>
                                                                <input
                                                                    type={'text'}
                                                                    className={'form-control1'}
                                                                    data-rule-required={true}
                                                                    name={'postcode'}
                                                                    value={this.state.editObj.postcode || ''}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                    data-rule-postcodecheck={true}
                                                                />
                                                            </Col>
                                                            <Col xl={3} lg={8} md={6} xs={12}>
                                                                <label className={'lab1'}>State</label>

                                                                <div className={'cmn_select grey_bg validation'}>
                                                                    <Select
                                                                        simpleValue={true}
                                                                        name={"state"}
                                                                        value={this.state.editObj.state || ''}
                                                                        options={statesOpt}
                                                                        onChange={(e) => this.selectchange(e, 'state')}
                                                                        clearable={false}
                                                                        required={true}

                                                                    />

                                                                </div>
                                                            </Col>
                                                        </Row>

                                                    </div>
                                                    <div className={'form-group'}>

                                                        <label className={'lab1'}>Contact</label>
                                                        {
                                                            this.state.editObj.contact.map((val, i) => {
                                                                return (
                                                                    <div className={'d-flex align-items-center mb-3'} key={i}>
                                                                        <div className={'linp_l_side'}>
                                                                            <input
                                                                                type={'text'}
                                                                                className={'form-control1'}
                                                                                data-rule-required={true}
                                                                                name={'contact_' + i}
                                                                                value={val || ''}
                                                                                onChange={(e) => this.inputArrHandler(e, 'contact', i)}
                                                                                data-rule-number={true}
                                                                            />
                                                                        </div>
                                                                        <div className={'linp_r_side d-flex align-items-center justify-content-end'}>
                                                                            {

                                                                                this.state.editObj.contact.length > 1 ?
                                                                                    <span onClick={() => { this.removeRowHadler('contact', i) }} className={'actnBts1__'}> <i className={'fa fa-minus'}></i></span> : null
                                                                            }
                                                                            {

                                                                                (i + 1 == this.state.editObj.contact.length) && (this.state.editObj.contact.length < 3) ?
                                                                                    <span onClick={() => { this.addRowHadler('contact') }} className={'actnBts1__'}> <i className={'fa fa-plus'}></i></span> : null
                                                                            }
                                                                        </div>


                                                                    </div>
                                                                )
                                                            })
                                                        }



                                                    </div>
                                                    <div className={'form-group'}>

                                                        <label className={'lab1'}>Email Address</label>
                                                        {
                                                            this.state.editObj.email.map((val, i) => {
                                                                return (
                                                                    <div className={'d-flex align-items-center mb-3'} key={i}>
                                                                        <div className={'linp_l_side'}>
                                                                            <input
                                                                                type={'text'}
                                                                                className={'form-control1'}
                                                                                data-rule-required={true}
                                                                                name={'email_' + i}
                                                                                value={val || ''}
                                                                                onChange={(e) => this.inputArrHandler(e, 'email', i)}
                                                                                data-rule-email={true}
                                                                            />
                                                                        </div>
                                                                        <div className={'linp_r_side d-flex align-items-center justify-content-end'}>
                                                                            {

                                                                                this.state.editObj.email.length > 1 ?
                                                                                    <span onClick={() => { this.removeRowHadler('email', i) }} className={'actnBts1__'}>
                                                                                        <i className={'fa fa-minus'}></i>
                                                                                    </span>
                                                                                    : null
                                                                            }
                                                                            {

                                                                                (i + 1 == this.state.editObj.email.length) && (this.state.editObj.email.length < 3) ?
                                                                                    <span onClick={() => { this.addRowHadler('email') }} className={'actnBts1__'}>
                                                                                        <i className={'fa fa-plus'}></i>
                                                                                    </span>
                                                                                    :
                                                                                    null
                                                                            }
                                                                        </div>


                                                                    </div>
                                                                )
                                                            })
                                                        }

                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Website</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            data-rule-required={true}
                                                            name={'website'}
                                                            value={this.state.editObj.website || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Admin Users</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            data-rule-required={true}
                                                            name={'admin_users'}
                                                            value={this.state.editObj.admin_users || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>
                                                    <div className={'form-group'}>

                                                        <label className="customCheckie">
                                                            <input
                                                                type="checkbox"
                                                                id="billing"
                                                                onChange={(e) => this.inputHandler(e)}
                                                                value={this.state.editObj.billing}
                                                                checked={this.state.editObj.billing ? true : false}
                                                                name="billing"
                                                            />
                                                            <span>Billing information same as company</span>
                                                        </label>

                                                    </div>
                                                    {
                                                        this.state.editObj.billing == false ?
                                                            <React.Fragment>
                                                                <h3 className={'color1 f-bolder mt-3 mb-3'}>Billing Address</h3>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>Suburb</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        data-rule-required={true}
                                                                        name={'billing_suburb'}
                                                                        value={this.state.editObj.billing_suburb || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                    />
                                                                </div>
                                                                <div className={'form-group'}>
                                                                    <Row>
                                                                        <Col xl={9} lg={4} md={6} xs={12}>
                                                                            <label className={'lab1'}>Post Code</label>
                                                                            <input
                                                                                type={'text'}
                                                                                className={'form-control1'}
                                                                                data-rule-required={true}
                                                                                name={'billing_postcode'}
                                                                                value={this.state.editObj.billing_postcode || ''}
                                                                                onChange={(e) => this.inputHandler(e)}
                                                                                data-rule-postcodecheck={true}
                                                                            />
                                                                        </Col>
                                                                        <Col xl={3} lg={8} md={6} xs={12}>
                                                                            <label className={'lab1'}>State</label>
                                                                            <div className={'line_h_0 pos_relative'}>
                                                                                <label className="error w-100 mb-0 pos_absolute" htmlFor="billing_state"></label>
                                                                            </div>
                                                                            <div className={'cmn_select grey_bg validation'}>
                                                                                <Select
                                                                                    simpleValue={true}
                                                                                    name={"billing_state"}
                                                                                    value={this.state.editObj.billing_state || ''}
                                                                                    options={statesOpt}
                                                                                    onChange={(e) => this.selectchange(e, 'billing_state')}
                                                                                    clearable={false}
                                                                                    placeholder={'Select State'}
                                                                                    required={true}
                                                                                />

                                                                            </div>
                                                                        </Col>
                                                                    </Row>

                                                                </div>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>Contact No.</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        data-rule-required={true}
                                                                        name={'billing_contact'}
                                                                        value={this.state.editObj.billing_contact || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-number={true}
                                                                    />
                                                                </div>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>Email Address</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        data-rule-required={true}
                                                                        name={'billing_email'}
                                                                        value={this.state.editObj.billing_email || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-email={true}
                                                                    />
                                                                </div>
                                                            </React.Fragment>
                                                            : null
                                                    }

                                                    <div className={'form-group mt-5'}>
                                                        <button className={'btn btn1 mr-2'} onClick={this.submitHandler}>Save changes</button>
                                                        <Link to={`${ROUTER_PATH}profile`}>
                                                            <span className={'color1 f-bold c_pointer ml-2'}>Cancel</span>
                                                        </Link>

                                                    </div>
                                                </form>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(UpdateCompanyDetails);