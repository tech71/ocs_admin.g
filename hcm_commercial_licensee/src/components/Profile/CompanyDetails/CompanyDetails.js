import React from 'react';
import { Col, Row } from 'react-bootstrap';
import ProfileSideLinks from '../ProfileSideLinks';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import MainHeading from 'hoc/MainHeading';

class CompanyDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            heading: "Company Details",
            activeTab: 'company_details',

        }
    }


    tabHandler = (key, title) => {

        this.setState({
            activeTab: key,
            heading: title
        })
    }

    render() {

        return (
            <React.Fragment>


                <section className="detailSection cmnSec">
                <MainHeading title={'Company Details'} />

                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ProfileSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>
                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <div className={'company_details_view'}>
                                            <h3 className={'color1 f-bolder'}>Address</h3>
                                            <ul className={'dets_Ul__ mt-3'}>
                                                <li><b>Name:</b> {this.props.companyDetails.name}</li>
                                                <li><b>ABN:</b> {this.props.companyDetails.abn}</li>
                                                <li><b>Address:</b> {this.props.companyDetails.address}</li>
                                                <li><b>Contact Number:</b> {this.props.companyDetails.contact}</li>
                                                <li><b>Email Address:</b> {this.props.companyDetails.email}</li>
                                                <li><b>Website:</b> {this.props.companyDetails.website}</li>
                                                <li><b>Admin Users:</b> {this.props.companyDetails.admin_users}</li>
                                            </ul>
                                            <h3 className={'color1 f-bolder mt-5'}>Billing Address</h3>
                                            <ul className={'dets_Ul__ mt-3'}>

                                                <li><b>Address:</b> {this.props.companyDetails.billingAddress.address}</li>
                                                <li><b>Contact Number:</b> {this.props.companyDetails.billingAddress.contact}</li>
                                                <li><b>Email Address:</b> {this.props.companyDetails.billingAddress.email}</li>
                                            </ul>
                                            <Link to={`${ROUTER_PATH}profile/update_company_details`}>
                                            
                                            <button className={'btn btn1 mt-3'} >Update Details</button>
                                            </Link>
                                        </div>



                                    </div>
                                </div>
                            </Col>
                        </Row>

                    </div>
                </section>
            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        companyDetails: state.ProfileReducer.companyDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(CompanyDetails);