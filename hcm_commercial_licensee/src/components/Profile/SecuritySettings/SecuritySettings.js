import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UpdatePassword from './UpdatePasswod';
import SmsAuthenticationModal from './SmsAuthenticationModal';
import GoogleAuthenticatorModal from '../CompanyDetails/GoogleAuthenticatorModal';
import ProfileSideLinks from '../ProfileSideLinks';
import MainHeading from 'hoc/MainHeading';
import { Col, Row } from 'react-bootstrap';


class SecuritySettings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            updatePassword: false
        }
    }


    closeModalHandler = (key) => {

        this.setState({
            [key]: false
        })
        // if (key == 'updatePassword') {
        //     this.setState({
        //         updatePassword: false
        //     })
        // }
        // if (key == 'SmsAuthenticationModal') {
        //     this.setState({
        //         SmsAuthenticationModal: false
        //     })
        // }
    }

    openModal = (key) => {

        this.setState({
            [key]: true
        })
        // if (key == 'updatePassword') {
        //     this.setState({
        //         updatePassword: true
        //     })
        // }
        // if (key == 'SmsAuthenticationModal') {
        //     this.setState({
        //         SmsAuthenticationModal: true
        //     })
        // }
    }


    render() {
        return (
            <React.Fragment>

                <section className="detailSection cmnSec">
                    <MainHeading title={'Security Settings'} />

                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ProfileSideLinks active={3} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>
                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <div className="boxList secFeat_list">
                                            <div className={'boxie__ secFeat'}>
                                                <div><b>Password:</b>******</div>
                                                <div>
                                                    <i className={'fa fa-edit edit_feat'} onClick={() => this.openModal('updatePassword')}></i>
                                                </div>
                                            </div>

                                            <div className={'boxie__ secFeat'}>
                                                <div><b>Two Step Authentication:</b> SMS Notification</div>
                                                <div>
                                                    {
                                                        this.props.securitySettings.smsNotification ?
                                                            <span className={'ED_lab enable  mr-3'}>Enabled</span> :
                                                            <span className={'ED_lab disable  mr-3'}>Disabled</span>
                                                    }

                                                    <i className={'fa fa-edit edit_feat'} onClick={() => this.openModal('SmsAuthenticationModal')}></i>
                                                </div>
                                            </div>

                                            <div className={'boxie__ secFeat'}>
                                                <div><b>Two Step Authentication:</b> Google Authenticator</div>
                                                <div>
                                                    {
                                                        this.props.securitySettings.googleAuthentication ?
                                                            <span className={'ED_lab enable mr-3'}>Enabled</span> :
                                                            <span className={'ED_lab disable  mr-3'}>Disabled</span>
                                                    }

                                                    <i className={'fa fa-edit edit_feat'} onClick={() => this.openModal('GoogleAuthenticatorModal')}></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </Col>

                        </Row>
                    </div>

                </section>



                {
                    this.state.updatePassword ?
                        <UpdatePassword
                            show={this.state.updatePassword}
                            close={() => this.closeModalHandler('updatePassword')}
                        /> : null
                }
                {
                    this.state.SmsAuthenticationModal ?
                        <SmsAuthenticationModal
                            show={this.state.SmsAuthenticationModal}
                            close={() => this.closeModalHandler('SmsAuthenticationModal')}
                        /> : null
                }

                {
                    this.state.GoogleAuthenticatorModal ?
                        <GoogleAuthenticatorModal
                            show={this.state.GoogleAuthenticatorModal}
                            close={() => this.closeModalHandler('GoogleAuthenticatorModal')}
                        /> : null
                }



            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        securitySettings: state.ProfileReducer.securitySettings
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(SecuritySettings);