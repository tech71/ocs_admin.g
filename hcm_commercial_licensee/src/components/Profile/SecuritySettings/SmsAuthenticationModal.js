import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Modal from 'hoc/Modal';
import jQuery from "jquery";
import Select from 'react-select-plus';


class SmsAuthenticationModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editObj: {
                country_code: '',
                sms_code: ''
            }
        }
    }
    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    sendCode = () => {
        var validator = jQuery('#enableSmsAuth').validate();
        // validator.element("#mobile");
        if (jQuery('#enableSmsAuth').valid()) {
            alert('code sent')
        }
    }

    enableSmsSubmit = (e) => {
        e.preventDefault();
        var validator = jQuery('#enableSmsAuth').validate();
        validator.element("#verification_code");

        if (validator.valid()) {
            alert('code match in progress')
        }

    }

    selectchange = (e, key) => {
        let editObj = this.state.editObj;
        editObj[key] = e;
        this.setState({
            editObj: editObj
        })
    }

    render() {

        const country_codeOpt = [
            { value: '+61', label: "+61 (Australia)" },
            { value: '+91', label: "+91 (India)" },
        ]

        return (
            <React.Fragment>
                <Modal show={this.props.show} >
                    <div className={'text-right'}>
                        <i onClick={this.props.close} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                    </div>
                    {
                        this.props.securitySettings.smsNotification ?
                            <React.Fragment>

                                <h4 className={'color1 f-bolder text-center mb-3'}>Disable SMS Authentication</h4>
                                <div className={'text-center mb-4'}>
                                    <small>We have sent an SMS code to your phone number 04** **** *12<br />Please enter the 6 digit code to turn off the authentication</small>
                                </div>
                                <form id={'disableSmsAuth'}>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>SMS Code</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            data-rule-required={true}
                                            name={'sms_code'}
                                            id={'sms_code'}
                                            value={this.state.editObj.sms_code || ''}
                                            onChange={(e) => this.inputHandler(e)}
                                            data-rule-required={true}
                                        />
                                    </div>
                                    <div className={'form-group text-center'}>
                                        <span className={'c_pointer f-bolder color1 f-12 t_underline'}>Resend Code</span>
                                    </div>
                                    <div className={'form-group text-center mt-4'}>
                                        <button className={'btn btn1'}>Disable SMS Authentication</button>
                                    </div>
                                </form>

                            </React.Fragment>
                            :
                            <React.Fragment>


                                <h4 className={'color1 f-bolder text-center mb-5'}>Enable SMS Authentication</h4>
                                <form id={'enableSmsAuth'} >
                                    <div className={'form-group'}>
<<<<<<< HEAD
                                        <div className={'cmn_select grey_bg'}>
                                            <label className={'lab1'}>Country Code</label>
                                            <div className={'line_h_0'}>
                                                <label className="error w-100 mb-0" htmlFor="country_code"></label>
                                            </div>

=======
                                        <label className={'lab1'}>Country Code</label>

                                        <div className={'cmn_select grey_bg validation'}>
>>>>>>> 2400d730d5bfaf69d7e3ca1b4767c7d4a1e0c075
                                            <Select
                                                simpleValue={true}
                                                name={"country_code"}
                                                id={'country_code'}
                                                value={this.state.editObj.country_code || ''}
                                                options={country_codeOpt}
                                                onChange={(e) => this.selectchange(e, 'country_code')}
                                                clearable={false}
<<<<<<< HEAD
                                            // inputRenderer={() => <input
                                            //     type="text"
                                            //     className="define_input"
                                            //     name={"country_code"}
                                            //     defaultValue={this.state.editObj.country_code || ''}
                                            //     data-rule-required={true}
                                            //     id={'country_code'}
                                            // />}
                                            />

=======
                                                required={true}
>>>>>>> 2400d730d5bfaf69d7e3ca1b4767c7d4a1e0c075

                                            />
                                        </div>
                                    </div>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>Mobile number</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            data-rule-required={true}
                                            name={'mobile'}
                                            id={'mobile'}
                                            value={this.state.editObj.mobile || ''}
                                            onChange={(e) => this.inputHandler(e)}
                                            data-rule-required={true}
                                            data-rule-numbervalid={true}
                                            data-rule-phonenumber={true}

                                        />
                                    </div>
                                    <div className={'form-group text-center'}>
                                        <span className={'btn btn1'} onClick={this.sendCode}>Send Code</span>
                                        <div className={'mt-2'}>
                                            <span className={'c_pointer f-bolder color1 f-12 t_underline '}>Resend Code</span>
                                        </div>

                                    </div>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>Verification Code</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            data-rule-required={true}
                                            name={'verification_code'}
                                            id={'verification_code'}
                                            value={this.state.editObj.verification_code || ''}
                                            onChange={(e) => this.inputHandler(e)}
                                            data-rule-required={true}

                                        />
                                    </div>
                                    <div className={'form-group text-center mt-5'}>
                                        <button className={'btn btn1'} onClick={this.enableSmsSubmit}>Enable SMS Authentication</button>
                                    </div>

                                </form>

                            </React.Fragment>
                    }


                </Modal>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        securitySettings: state.ProfileReducer.securitySettings
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(SmsAuthenticationModal);