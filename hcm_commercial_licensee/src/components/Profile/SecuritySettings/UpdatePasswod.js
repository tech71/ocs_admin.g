import React from 'react';
import jQuery from "jquery";
import Modal from 'hoc/Modal';

class UpdatePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editObj: {
                current_password: '',
                new_password: '',
                confirm_password: ''
            }
        }
    }

    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#updatePassword').validate();
        if (jQuery('#updatePassword').valid()) {
            alert('valid');
        }
    }

    render() {
        return (
            <React.Fragment>
                <Modal show={this.props.show} >
                    <div>
                        <div className={'text-right'}>
                            <i onClick={this.props.close} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                        </div>
                        <h4 className={'color1 f-bolder text-center mb-5'}>Update Password</h4>
                        <form id={'updatePassword'} onSubmit={this.submitHandler}>
                            <div className={'form-group'}>
                                <label className={'lab1'}>Current Password</label>
                                <input
                                    type={'password'}
                                    className={'form-control1'}
                                    data-rule-required={true}
                                    name={'current_password'}
                                    id={'current_password'}
                                    value={this.state.editObj.current_password || ''}
                                    onChange={(e) => this.inputHandler(e)}
                                    data-rule-required={true}
                                />
                            </div>
                            <div className={'form-group'}>
                                <label className={'lab1'}>New Password</label>
                                <input
                                    type={'password'}
                                    className={'form-control1'}
                                    data-rule-required={true}
                                    name={'new_password'}
                                    id={'new_password'}
                                    value={this.state.editObj.new_password || ''}
                                    onChange={(e) => this.inputHandler(e)}
                                    data-rule-required={true}
                                    minLength={6}
                                />
                            </div>
                            <div className={'form-group'}>
                                <label className={'lab1'}>Confirm New Password</label>
                                <input
                                    type={'password'}
                                    className={'form-control1'}
                                    data-rule-required={true}
                                    name={'confirm_password'}
                                    value={this.state.editObj.confirm_password || ''}
                                    onChange={(e) => this.inputHandler(e)}
                                    data-rule-equalto={'#new_password'}
                                />
                            </div>

                            <div className={'form-group text-center mt-5'}>
                                <button className={'btn btn1'}>Update Password</button>
                            </div>
                        </form>
                    </div>
                </Modal>
            </React.Fragment>
        )
    }
}

export default UpdatePassword;