import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ProfileSideLinks from '../ProfileSideLinks';
import MainHeading from 'hoc/MainHeading';
import { Col, Row } from 'react-bootstrap';


class UserDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <React.Fragment>

                <section className="detailSection cmnSec">
                    <MainHeading title={'User Details'} />

                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ProfileSideLinks active={2} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>
                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <div className={'user_details_view'}>
                                            <ul className={'dets_Ul__ mt-3'}>
                                                <li><b>Name:</b> {this.props.userDetails.name}</li>
                                                <li><b>position:</b> {this.props.userDetails.position}</li>
                                                <li><b>Contact Number:</b> {this.props.userDetails.contact}</li>
                                                <li><b>Email Address:</b> {this.props.userDetails.email}</li>
                                            </ul>
                                            <button className={'btn btn1 mt-3'} >Update Details</button>
                                        </div>
                                    </div>
                                </div>
                            </Col>

                        </Row>
                    </div>

                </section>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        userDetails: state.ProfileReducer.userDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);