import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

class KnowledgeBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }



    render() {
        return (
            <React.Fragment>
                <section className="helpSection cmnSec">
                    <h1 className="mainHeader">Knowledge Base</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ul className={'dets_ul__'}>

                                    <li >
                                        <Link to={`${ROUTER_PATH}help`} className={'active'}>Knowledge Base</Link>
                                    </li>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help/enquiries`} >
                                            Enquiries
                                        </Link>
                                    </li>

                                </ul>
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>


                                        <h3 className={'color1 f-bolder'}>How can we help?</h3>
                                        <form id={'searchTopicform'} className={'mt-4'}>
                                            <label className={'lab1'}>Search</label>
                                            <div className="input-group1 right">
                                                <input
                                                    type="text"
                                                    className="form-control1"
                                                    aria-label="search_name"
                                                />
                                                <div className="input-group-append">
                                                    <span className="input-group-text" id="basic-addon2"><i className={'fa fa-search'}></i></span>
                                                </div>
                                            </div>
                                        </form>

                                        <ul className={'helpLi__list mt-4'}>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/1`}>
                                                    Help Topic One
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/2`}>
                                                    Help Topic Two
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/3`}>
                                                    Help Topic Three
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/4`}>
                                                    Help Topic Four
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/5`}>
                                                    Help Topic One
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/6`}>
                                                    Popular Article One
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={`${ROUTER_PATH}help/knowledge_base/1`}>
                                                    Popular Article Two
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        )
    }
}
export default KnowledgeBase;