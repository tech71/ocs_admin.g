import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

class TopicList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


    render() {
        return (
            <React.Fragment>
                <section className="helpSection cmnSec">
                    <h1 className="mainHeader">Knowledge Base</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ul className={'dets_ul__'}>

                                    <li >
                                        <Link to={`${ROUTER_PATH}help`} className={'active'}>Knowledge Base</Link>
                                    </li>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help/enquiries`} >
                                            Enquiries
                                        </Link>
                                    </li>

                                </ul>
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>


                                        <h3 className={'color1 f-bolder'}>Help Topic One</h3>
                                        <form id={'searchTopicform'} className={'mt-4'}>
                                            <label className={'lab1'}>Search Topics</label>
                                            <div className="input-group1 right">
                                                <input
                                                    type="text"
                                                    className="form-control1"
                                                    aria-label="search_name"
                                                />
                                                <div className="input-group-append">
                                                    <span className="input-group-text" id="basic-addon2"><i className={'fa fa-search'}></i></span>
                                                </div>
                                            </div>
                                        </form>

                                        <Row className={'mt-5'}>
                                            <Col xs={12}>
                                                <div className={'topicDv'}>
                                                    <h6 className={'color1 f-bolder'}>Help Topic one - sub category 1</h6>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                        It has survived not only five centuries,</p>
                                                    <div >
                                                        <Link className={'t_underline color1 f-bolder f-13'} to={`${ROUTER_PATH}help/knowledge_base/topic_details/1`}>
                                                            Read More
                                                        </Link>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={12}>
                                                <div className={'topicDv'}>
                                                    <h6 className={'color1 f-bolder'}>Help Topic one - sub category 2</h6>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                        It has survived not only five centuries,</p>
                                                    <div >
                                                        <Link className={'t_underline color1 f-bolder f-13'} to={'#'}>
                                                            Read More
                                                        </Link>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs={12}>
                                                <div className={'topicDv'}>
                                                    <h6 className={'color1 f-bolder'}>Help Topic one - sub category 3</h6>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                        when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                        It has survived not only five centuries,</p>
                                                    <div >
                                                        <Link className={'t_underline color1 f-bolder f-13'} to={'#'}>
                                                            Read More
                                                        </Link>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        )
    }
}
export default TopicList;