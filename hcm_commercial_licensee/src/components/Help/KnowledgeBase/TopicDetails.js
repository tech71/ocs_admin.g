import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import CreateEnquiryForm from '../Enquiries/CreateEnquiryForm';
class TopicDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }


    render() {
        return (
            <React.Fragment>
                <section className="helpSection cmnSec">
                    <h1 className="mainHeader">Knowledge Base</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ul className={'dets_ul__'}>

                                    <li >
                                        <Link to={`${ROUTER_PATH}help`} className={'active'}>Knowledge Base</Link>
                                    </li>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help/enquiries`} >
                                            Enquiries
                                        </Link>
                                    </li>

                                </ul>
                            </Col>

                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <h4 className={'color1 f-bolder'}>Help Topic one - sub category 1</h4>
                                        <div className={'mt-4'}>
                                            <div className={'mb-3'}>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an unknown printer took a galley of type and scrambled it to make a type
                                                specimen book. It has survived not only five centuries, but also the leap into
                                                electronic typesetting, remaining essentially unchanged. It was popularised in
                                                the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                                and more recently with desktop publishing software like Aldus PageMaker including
                                                versions of Lorem Ipsum.
                                            </div>
                                            <div>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when an unknown printer took a galley of type and scrambled it to make a type
                                                specimen book. It has survived not only five centuries, but also the leap into
                                                electronic typesetting, remaining essentially unchanged. It was popularised in
                                                the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                                and more recently with desktop publishing software like Aldus PageMaker including
                                                versions of Lorem Ipsum.
                                            </div>
                                        </div>
                                        <div className={'mt-5 mb-5'}>
                                            <Link to={`${ROUTER_PATH}help`}>
                                                <button className={'btn btn1'}>
                                                    <i className={'fa fa-arrow-left mr-3'}></i>
                                                    Back to Help Topics
                                                </button>
                                            </Link>
                                        </div>

                                        <h4 className={'color1 f-bolder'}>Was this article Helpful?</h4>
                                        <div className={'mb-4'}>
                                            Was this not quite what you;re looking for? Send us an enquiry for you and we'll answer your
                                             questions
                                        </div>

                                        <CreateEnquiryForm />

                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>


                </section>
            </React.Fragment>
        )
    }
}
export default TopicDetails;