import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import jQuery from "jquery";
import { postFileData } from 'store/actions';
import Modal from 'hoc/Modal';
import { toast, ToastContainer } from 'react-toastify';


class CreateEnquiryForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            formObj: {
                // name: 'md',
                // email: 'fda@gm.co',
                // enquiry_title: 'fda',
                // enquiry: 'fdaf',
                attachments: []
            }
        }
    }

    inputHandler = (e) => {
        let formObj = this.state.formObj;
        formObj[e.target.name] = e.target.value;
        this.setState({
            formObj
        })
    }

    submidHandler = (e) => {
        e.preventDefault();
        jQuery('#createEnquiryForm').validate();
        if (jQuery('#createEnquiryForm').valid()) {
            let formData = new FormData();
            formData.append('name', this.state.formObj.name);
            formData.append('email', this.state.formObj.email);
            formData.append('enquiry_title', this.state.formObj.enquiry_title);
            formData.append('enquiry', this.state.formObj.enquiry);

            let attachments = this.state.formObj.attachments;

            attachments.map(val => {
                formData.append('attachments[]', val);
            })

            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }

            this.props.postFileData('Contact_cn/dummy', formData, config);

        }
    }

    fileHandler = (e) => {
        let formObj = this.state.formObj;

        if (e.target.files[0].size <= 5000000) {
            formObj[e.target.name].push(e.target.files[0]);
            this.setState({
                formObj
            })
        }
        else {
            toast.error("Please select file less than 5mb !", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
        e.target.value = null

    }

    removeFileHandler = (index) => {
        let formObj = this.state.formObj;
        formObj['attachments'].splice(index, 1);
        this.setState({
            formObj
        })
    }

    render() {

        console.log(this.state)
        return (
            <React.Fragment>
                <form id={'createEnquiryForm'} onSubmit={this.submidHandler}>
                    <div className={'form-group'}>
                        <label className={'lab1'}>Name</label>
                        <input
                            type={'text'}
                            className={'form-control1'}
                            data-rule-required={true}
                            name={'name'}
                            value={this.state.formObj.name || ''}
                            onChange={(e) => this.inputHandler(e)}
                        />
                    </div>
                    <div className={'form-group'}>
                        <label className={'lab1'}>Email</label>
                        <input
                            type={'text'}
                            className={'form-control1'}
                            data-rule-required={true}
                            name={'email'}
                            value={this.state.formObj.email || ''}
                            onChange={(e) => this.inputHandler(e)}
                            data-rule-email={true}
                        />
                    </div>
                    <div className={'form-group'}>
                        <label className={'lab1'}>Enquiry title</label>
                        <input
                            type={'text'}
                            className={'form-control1'}
                            data-rule-required={true}
                            name={'enquiry_title'}
                            value={this.state.formObj.enquiry_title || ''}
                            onChange={(e) => this.inputHandler(e)}
                        />
                    </div>
                    <div className={'form-group'}>
                        <label className={'lab1'}>Enquiry</label>
                        <textarea
                            className={'form-control1'}
                            data-rule-required={true}
                            defaultValue={this.state.formObj.enquiry || ''}
                        ></textarea>
                    </div>
                    <div className={'form-group'}>
                        {/* <div>
                            {this.state.formObj.attachment !== undefined ? this.state.formObj.attachment.name || '' : ''}
                        </div> */}
                        <div>
                            {
                                this.state.formObj.attachments !== undefined && this.state.formObj.attachments.length > 0 ?
                                    this.state.formObj.attachments.map((val, i) => {
                                        return (
                                            <div key={i} >
                                                <i onClick={() => this.removeFileHandler(i)} className={'fa fa-close c_pointer color1 mr-2'}></i> {val.name}
                                            </div>
                                        )
                                    }) : null
                            }
                        </div>
                        <label className={'color1 f-bold c_pointer'} style={{ opacity: this.state.formObj.attachments.length > 4 ? '0.5' : '1' }}>
                            <i className={'fa fa-file-text-o'}></i> Add Attachment
                            <input
                                type={'file'}
                                name={'attachments'}
                                className={'hidden_file'}
                                onChange={(e) => this.fileHandler(e)}
                                disabled={this.state.formObj.attachments.length > 4 ? true : false}
                            />
                        </label>

                    </div>
                    <div className={'form-group text-center mt-5'}>
                        <button className={'btn btn1'}>Send Enquiry</button>
                    </div>
                </form>

                {
                    this.state.showModal ?
                        <Modal show={this.state.showModal} >

                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ showModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder  mb-5'}>You have successfully <br />submitted your enquiry</h4>
                                <div className={'f-13  mb-3'}>Please allow upto 2 working days for your enquiry to be attended to</div>
                                <div className={'f-13  mb-4'}>Your enquiry number is 00016 for your reference.</div>

                                <button className={'btn btn1 mb-3'} onClick={() => this.setState({ showModal: false })}>Okay</button>
                            </div>

                        </Modal>
                        : null
                }





            </React.Fragment>
        );
    }


}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    postFileData
}, dispatch);


export default connect(null, mapDispatchToProps)(CreateEnquiryForm);
