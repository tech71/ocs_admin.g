import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import { Col, Row } from 'react-bootstrap';


class EnquiryDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {


        }
    }



    render() {

        return (
            <React.Fragment>

                <section className={'notifySec cmnSec'}>
                    <h1 className="mainHeader">Enquiry Number:{this.props.enquiryDetails.enquiry_number}</h1>
                    <div className="transBox">
                        <div>

                            {
                                this.props.enquiryDetails.enquiries !== undefined && this.props.enquiryDetails.enquiries.length > 0 ?
                                    this.props.enquiryDetails.enquiries.map((val, i) => {
                                        return (
                                            <div className={'enquiryLi__'} key={i}>
                                                <div>
                                                    {val.status == 'active' ? <span className={'ED_lab enable  mr-2'}>Active</span> : null}
                                                    <span className={'f-bold f-13'}>Enquiry Raised:{val.date}</span>
                                                </div>
                                                <div>
                                                    <h4 className={'f-bolder color1 mt-3 mb-3'}>{val.title}</h4>
                                                    <div>{val.description}</div>
                                                    <div className={'mt-5'}>
                                                        {
                                                            val.status == 'active' ?
                                                                <React.Fragment>
                                                                    <button className={'btn btn1 mr-3 '}>Acknowledge as resolved</button>
                                                                    <span className={'color1 f-bolder c_pointer'}>Reply</span>
                                                                </React.Fragment>
                                                                :
                                                                <React.Fragment>
                                                                    <span className={'color1 f-bolder c_pointer'}>Delete</span>
                                                                </React.Fragment>
                                                        }
                                                    </div>

                                                </div>
                                            </div>
                                        )
                                    })
                                    : null
                            }

                        </div>
                        <Link to={`${ROUTER_PATH}help/enquiries`}>
                         <button className={'btn btn1 mt-5'}>Back to enquiries</button>
                        </Link>
                        
                    </div>
                </section>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        enquiryDetails: state.EnquiriesReducer.enquiryDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(EnquiryDetails);