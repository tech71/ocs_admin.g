import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactTable from 'react-table-v6';

class Enquiries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


        }
    }



    render() {

        const columns = [
            {
                Header: 'Status',
                accessor: 'status',
                width: 70,
                Cell: (props) => 
                <React.Fragment>
                    <div className={'text-center w-100'}>
                    {props.original.status == 'enabled' ? 
                    <span className={'ED_lab enable'}>Enabled</span> :
                    props.original.status == 'closed' ?
                    <span className={'ED_lab disable'}>Closed</span>
                    :
                    <span className={'ED_lab received'}>Received</span>
                    }
                    </div>
                </React.Fragment>
            },
            {
                Header: 'Status2',
                accessor: 'status2',
                width: 40,
                Cell: (props) => <React.Fragment>{props.original.status2 == 'true' ? <span className="activeNoti_ ml-2"></span> : ''}</React.Fragment>
            },
            { Header: 'Date', accessor: 'date', maxWidth: 120, Cell: (props) => <div>{props.original.date}</div> },
            { Header: 'enquiry number', accessor: 'enquiry_number', maxWidth: 70, Cell: (props) => <div className={'f-bold'}>{props.original.enquiry_number}</div> },
            { Header: "Title", accessor: 'title', Cell: (props) => <div>{props.original.title}</div> },
            {
                Header: 'Action',
                // maxWidth: 62,
                sortable: false,
                Cell: (props) => <div className="action_c w-100 pr-3">
                   
                    <Link to={`${ROUTER_PATH}help/enquiries/enquiry_details/${props.original.id}`}>
                        <i className="fa fa-eye watch_ic  mr-2" ></i>
                    </Link>

                    <i className="fa fa-close delete_ic"></i>
                </div>
            },
        ]

        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <h1 className="mainHeader">Enquiries</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ul className={'dets_ul__'}>

                                    <li>
                                        <Link to={`${ROUTER_PATH}help`}>
                                            Knowledge Base
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help/enquiries`} className={'active'}>
                                            Enquiries
                                        </Link>
                                    </li>

                                </ul>
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <Link to={`${ROUTER_PATH}help/enquiries/new_enquiry`}>
                                            <button className={'btn btn1'}>Create New Enquiry</button>
                                        </Link>
                                        <Row>
                                        <div className="table_common__ notifyTable w-100 mt-5">
                                            <ReactTable
                                                data={this.props.enquiriesList}
                                                columns={columns}
                                                minRows={1}
                                                defaultPageSize={10}
                                                loading={this.state.loading}
                                                loadingText={'loading...'}
                                                previousText={<span className="icon fa fa-angle-left previous"></span>}
                                                nextText={<span className="icon fa fa-angle-right next"></span>}
                                            />
                                        </div>
                                        </Row>

                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        enquiriesList: state.EnquiriesReducer.enquiriesList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Enquiries);