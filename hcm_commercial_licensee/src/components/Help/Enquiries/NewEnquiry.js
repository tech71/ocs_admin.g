import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import CreateEnquiryForm from './CreateEnquiryForm';


class NewEnquiry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


        }
    }


    render() {
        return (
            <React.Fragment>
                <section className="helpSection cmnSec">
                    <h1 className="mainHeader">Enquiries</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <ul className={'dets_ul__'}>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help`}>
                                            Knowledge Base
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to={`${ROUTER_PATH}help/enquiries`} className={'active'}>
                                            Enquiries
                                        </Link>
                                    </li>
                                </ul>
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                    <h3 className={'color1 f-bolder mb-5'}>Send New Enquiry</h3>
                                    <CreateEnquiryForm />
                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        )
    }
}
export default NewEnquiry;