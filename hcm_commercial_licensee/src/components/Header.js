import React from 'react';
import { NavLink } from 'react-router-dom';
import { ROUTER_PATH } from '../services/config';
import { NavDropdown } from 'react-bootstrap';

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMobNav: false
        }
    }

    toggleNavbar = (key = null) => {

        if (key == 's') {
            this.setState({
                showMobNav: true
            })
        } else {
            this.setState({
                showMobNav: false
            })
        }

    }

    render() {
        return (

            <header className="main_hdr">
                <div className={'overlay ' + (this.state.showMobNav ? 'show' : '')} onClick={this.toggleNavbar}></div>
                <div className="logo_s">
                    {/* <a className="nav_logo" href=""> */}
                    <NavLink exact className="nav-link" to={`${ROUTER_PATH}`}> <img src="/images/logo.svg" /></NavLink>
                    {/* </a> */}
                </div>

                <div className="but_s">
                    {/* <span className={'color1 f-bold prof_but c_pointer'}>
                        <span className={'prof_name'}>Welcome, Juliette </span>
                        <span className={'profPc'}>
                            JT
                           <span className={'notify'}>30</span>
                        </span>
                    </span> */}
                    <NavDropdown
                        title={
                            <span className={'color1 f-bold prof_but c_pointer'}>
                                <span className={'prof_name'}>Welcome, Juliette </span>
                                <span className={'profPc'}>
                                    JT <span className={'notify'}>30</span>
                                </span>
                            </span>
                        } 
                        id="user" 
                        placement={'bottom-end'}
                    >
                        <div>
                           <div className={'userPic2'}>
                                <span>JT</span>
                           </div>
                           <div className={'text-center pt-3'}>
                               <h5><b>Admin</b></h5>
                               <h6>Juliette</h6>
                               <button className={'btn btn1 mt-3 mb-2'}>My Account</button>
                               <div className={'color1 c_pointer mb-3 f-bolder'}>Sign Out</div>
                               <div className={'notifBox'}>
                                   <span >Notifications <span className={'notify'}>30</span></span>
                               </div>
                           </div>
                        </div>
                    </NavDropdown>
                    <span className="toggle_s" onClick={() => this.toggleNavbar('s')}>
                        <div className="toggle_ic">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </span>

                </div>

                <nav className={"nav_s " + (this.state.showMobNav ? 'show' : '')}>

                    <i className="fa fa-close close_ic" onClick={this.toggleNavbar}></i>
                    <NavLink exact className={'mobNavie_logo'} to={`${ROUTER_PATH}`}>
                        <img src="/images/logo.svg" />
                    </NavLink>
                    <ul className="navbar-navie">
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}profile`} onClick={this.toggleNavbar}>Profile</NavLink></strong>
                        </li>

                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}plan`} onClick={this.toggleNavbar}>Plan</NavLink></strong>
                        </li>
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}help`} onClick={this.toggleNavbar}>Help</NavLink></strong>
                        </li>

                    </ul>
                </nav>
            </header>
        )
    }
}

export default Header;