import React from 'react';
import ReactTable from 'react-table-v6';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Col, Row } from 'react-bootstrap';
import PlanSideLinks from '../PlanSideLinks';
import MainHeading from 'hoc/MainHeading.js';
import Modal from 'hoc/Modal';
import Select from 'react-select-plus';

class PaymentHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentObj: {
                invoice:'000111',
                amount:'$51,727.50'
            },
            paymentBy: '3',
            paymentModal:false
        }
    }

    selectchange = (e, key) => {
        this.setState({[key] : e})
    }


    makePaymentHandler = (id) => {
        this.setState({paymentModal:true})
    }


    componentWillMount() {

    }


    render() {

        const columns = [
            {
                Header: 'Status',
                accessor: 'status',
                maxWidth: 80,
                Cell: (props) =>
                    <React.Fragment>
                        <div className={'text-center w-100'}>
                            {props.original.paid == 'true' ?
                                <span className={'ED_lab enable'}>Paid</span> :
                                <span className={'ED_lab disable'}>Not Paid</span>

                            }
                        </div>
                    </React.Fragment>
            },

            { Header: 'Date', accessor: 'date', Cell: (props) => <div><b>Invoice:{props.original.invoice}</b></div> },
            { Header: 'name', accessor: 'name', Cell: (props) => <div>{props.original.amount}</div> },
            { Header: 'post', accessor: 'post', Cell: (props) => <div>{props.original.paid == 'true' ? 'Paid:' : 'Due:'}{props.original.date}</div> },
            {
                Header: 'Action',
                maxWidth: 62,
                sortable: false,
                Cell: (props) => <div className="action_c w-100 pr-3">


                    {
                        props.original.paid == 'false' ?
                            <i className="fa fa-server watch_ic  mr-2" onClick={()=>this.makePaymentHandler(props.original.id)}></i>
                            : null
                    }

                    <i className="fa fa-eye delete_ic"></i>
                </div>
            },
        ]
        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <MainHeading title={'Payment History'} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={4} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <Row>
                                            <div className="table_common__ notifyTable w-100 ">
                                                <ReactTable
                                                    data={this.props.paymentHistory}
                                                    columns={columns}
                                                    minRows={1}
                                                    defaultPageSize={10}
                                                    loading={this.state.loading}
                                                    loadingText={'loading...'}
                                                    previousText={<span className="icon fa fa-angle-left previous"></span>}
                                                    nextText={<span className="icon fa fa-angle-right next"></span>}
                                                />
                                            </div>
                                        </Row>

                                    </div>
                                </div>

                            </Col>

                        </Row>

                        <Modal show={this.state.paymentModal}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ paymentModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>
                                    Make Payment for Invoice <br />{this.state.paymentObj.invoice}
                                </h4>
                                <div className={'f-13  mb-4'}>
                                    {this.state.paymentObj.amount} will be deducted from your selected default card unless
                                    otherwise specified. Payment may take upto 3 working days to process.
                                </div>

                                <div className={'form-group'}>
                                    <div className={'line_h_0 pos_relative'}>
                                        <label className="error w-100 mb-0 pos_absolute" htmlFor="paymentBy"></label>
                                    </div>

                                    <label className={'lab1 text-left'}>Select card for payment</label>

                                    <div className={'cmn_select grey_bg '}>
                                        <Select
                                            simpleValue={true}
                                            name={"paymentBy"}
                                            id={'paymentBy'}
                                            value={this.state.paymentBy || ''}
                                            options={this.props.paymentOptions}
                                            onChange={(e) => this.selectchange(e, 'paymentBy')}
                                            clearable={false}
                                            searchable={false}
                                            inputRenderer={(props) =>
                                                <input
                                                    type="text"
                                                    className="define_input"
                                                    name={"paymentBy"}
                                                    defaultValue={this.state.paymentBy || ''}
                                                    id={'paymentBy'}

                                                />

                                            }
                                        />

                                    </div>

                                </div>

                                <div>
                                    <button onClick={() => this.setState({ paymentModal: false })} className={'btn btn1 mt-3'}>Continue with Payment</button>
                                </div>

                            </div>

                        </Modal>


                    </div>
                </section>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        paymentOptions: state.PlanReducer.paymentOptions,
        paymentHistory: state.PlanReducer.paymentHistory
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(PaymentHistory);
