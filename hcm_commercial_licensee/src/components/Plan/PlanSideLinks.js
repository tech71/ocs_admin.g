import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

const PlanSideLinks = (props) => {
    return (
        <ul className={'dets_ul__'}>
            <li>
                <Link to={`${ROUTER_PATH}plan`} className={props.active == 1 ? 'active':''}>
                    Manage Plan
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}plan/current_users`}  className={props.active == 2 ? 'active':''} >
                    Current Users
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}plan/billing`}  className={props.active == 3 ? 'active':''}>
                    Billing
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}plan/payment_history`}  className={props.active == 4 ? 'active':''}>
                    Payment History
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}plan/integrations`}  className={props.active == 5 ? 'active':''}>
                    Integrations
                </Link>
            </li>
        </ul>
    )
}

export default PlanSideLinks;