import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PlanSideLinks from '../PlanSideLinks';
import jQuery from "jquery";
import Modal from 'hoc/Modal';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

class CancelPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


            editObj: {
                reasonsArr: []
            },
            confirmCancel:{
                subscribe:true
            },
            confirmCancelModal: false

        }
    }

    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    checkboxArrHandler = (e, key) => {

        let editObj = this.state.editObj;
        let arr = editObj[key];
        let index = arr.indexOf(e.target.value);
        if (index == -1) {
            arr.push(e.target.value);
        } else {
            arr.splice(index, 1);
        }
        editObj[key] = arr;
        this.setState({
            editObj
        })

    }

    checkHandler = (e)=>{
        let obj = this.state.confirmCancel;
        obj[e.target.name] = !obj[e.target.name];
        this.setState({obj})
        
    }


    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#cancelSubsForm').validate();
        if (jQuery('#cancelSubsForm').valid()) {
            this.setState({
                confirmCancelModal: true
            })
        }

    }

    render() {


        const reasonArray = [
            { value: '1', label: 'Our company no longer wishes to use HCM' },
            { value: '2', label: 'HCM is too difficult to use' },
            { value: '3', label: 'HCM doesnt suit our needs' },
            { value: '4', label: 'HCM is too expensive' },
            { value: '5', label: 'Other' },

        ]
        console.log(this.state.editObj)
        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <h1 className="mainHeader">Cancel Plan</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <div>
                                            <div className={'f-bolder mb-3'}>Sorry you want to leave, Juliette</div>
                                            <div className={'mb-3'}>If you cancel your subscription today, you will still be charged for the current cycle you are in.</div>
                                            <div className={'mb-4'}>You will have access to your account for the next 7 days so that you can  view and download any information you need</div>

                                            <div className={'f-bolder'}>I want to leave because</div>
                                            <form id={'cancelSubsForm'} onSubmit={this.submitHandler}>
                                                <div className={'pos_relative'}>
                                                    <label className={'error lab_sp'} htmlFor={'reasons'}></label>
                                                    <ul className={'mt-3'}>

                                                        {
                                                            reasonArray !== undefined && reasonArray.length > 0 ?
                                                                reasonArray.map((val, i) => {
                                                                    return (
                                                                        <li key={i}>
                                                                            <label className="customCheckie">
                                                                                <input
                                                                                    type="checkbox"
                                                                                    id={val.value}
                                                                                    onChange={(e) => this.checkboxArrHandler(e, 'reasonsArr')}
                                                                                    value={val.value}
                                                                                    checked={
                                                                                        this.state.editObj.reasonsArr !== undefined ?
                                                                                            this.state.editObj.reasonsArr.indexOf(val.value) !== -1 ? true : false
                                                                                            : false
                                                                                    }
                                                                                    name={"reasons"}
                                                                                    data-rule-required={true}
                                                                                />
                                                                                <span>{val.label + (val.label == 'Other' ? '(Please specify)' : '')}</span>
                                                                            </label>
                                                                        </li>
                                                                    )
                                                                })
                                                                : null
                                                        }


                                                        {
                                                            this.state.editObj.reasonsArr !== undefined && this.state.editObj.reasonsArr.length > 0 ?
                                                                this.state.editObj.reasonsArr.map((val, i) => {

                                                                    if (reasonArray !== undefined && reasonArray.length > 0) {
                                                                        var other = reasonArray.find(element => element.value == val);

                                                                        if (other !== undefined && other.hasOwnProperty('label') && _.lowerCase(other.label) == 'other') {
                                                                            return (<div className="pr-3" key={i}>
                                                                                <textarea
                                                                                    className="form-control1"
                                                                                    name="reason_other"
                                                                                    onChange={(e) => this.inputHandler(e)}
                                                                                    data-rule-required={true}
                                                                                    value={(this.state.editObj.reason_other) ? this.state.editObj.reason_other : ''}></textarea>
                                                                            </div>)
                                                                        }
                                                                    }
                                                                })
                                                                : null
                                                        }
                                                    </ul>
                                                </div>

                                                <div className={'mt-5'}>
                                                    <button className={'btn btn1 mr-3'}>Confirm Cancellation</button>
                                                    <Link to={`${ROUTER_PATH}plan`}>
                                                        <span className={'color1 c_pointer f-bolder'}>Back</span>
                                                    </Link>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                            </Col>

                        </Row>

                        <Modal show={this.state.confirmCancelModal}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ confirmCancelModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>Your plan has been cancelled</h4>
                                <div className={'f-13  mb-3'}>
                                    You will access to your account for the next 7 days so that you can view and download any information you
                                    need. After that period, you will no longer be able to access any data from HCM. If you choose to sign up again
                                    in the future, your previous information will no longer be available
                                </div>

                                <div className={'f-13  mb-5'}>
                                    <div className={'form-group'}>

                                        <label className="error lab_sp" htmlFor="agree2"></label>
                                        <label className="customCheckie">
                                            <input
                                                type="checkbox"
                                                id="subscribe"
                                                onChange={(e) => this.checkHandler(e)}
                                                value={this.state.confirmCancel.subscribe}
                                                checked={this.state.confirmCancel.subscribe ? true : false}
                                                name="subscribe"
                                                data-rule-required="true"
                                            />
                                            <span>Keep me subscribed to HCM newsletter</span>
                                        </label>

                                    </div>
                                </div>
                                <div className={' mb-3'}>
                                    <button onClick={() => this.setState({ confirmCancelModal: false })} className={'btn btn1'}>Okay</button>
                                </div>
                            </div>

                        </Modal>

                    </div>
                </section>

            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        planDetails: state.PlanReducer.planDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(CancelPlan);
