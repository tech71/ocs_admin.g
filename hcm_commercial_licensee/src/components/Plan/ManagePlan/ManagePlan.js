import React from 'react';
import { Col, Row } from 'react-bootstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PlanSideLinks from '../PlanSideLinks';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

class ManagePlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


        }
    }



    render() {

        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <h1 className="mainHeader">Manage Plan</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <ul className={'dets_Ul__ mt-3 mb-3'}>
                                            <li>
                                                <b>Plan tye:</b> {this.props.planDetails.plan_type}
                                                <div><span className={'t_underline color1 f-12 f-bold c_pointer'}>View plan types</span></div>
                                            </li>
                                            <li><b>Next payment due:</b> {this.props.planDetails.next_payment_due}</li>
                                            <li><b>Payment type:</b> {this.props.planDetails.payment_type}</li>
                                            <li><b>{this.props.planDetails.payment == 'monthly' ? 'Monthly' : 'Yearly'} Payment:</b> {this.props.planDetails.payment_amount}</li>
                                            <li><b>User paid for:</b> {this.props.planDetails.users}</li>
                                        </ul>
                                        <div className={'mt-5'}>
                                            <Link to={`${ROUTER_PATH}plan/change_plan`}>
                                                <button className={'btn btn1 mr-3'}>Change Plan details</button>
                                            </Link>
                                            <Link to={`${ROUTER_PATH}plan/cancel_plan`}>
                                                <span className={'color1 f-bolder c_pointer'}>Cancel Subscription</span>
                                            </Link>
                                        </div>
                                        <div className={'mt-5'}>
                                            <Link to={`${ROUTER_PATH}plan/new_plan`}>
                                                <button className={'btn btn1 mr-3'}>Set Up Plan</button>
                                            </Link>
                                            <Link to={`${ROUTER_PATH}plan/cancel_plan`}>
                                                <span className={'color1 f-bolder c_pointer'}>Cancel Subscription</span>
                                            </Link>
                                        </div>

                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        planDetails: state.PlanReducer.planDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(ManagePlan);
