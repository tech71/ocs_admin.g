import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PlanSideLinks from '../PlanSideLinks';
import jQuery from "jquery";
import Select from 'react-select-plus';
import Modal from 'hoc/Modal';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';


class NewPlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            subscriptionOpts: [
                { value: 'yearly', label: 'Yearly' },
                { value: 'monthly', label: 'Monthly' }
            ],

            plan_typeOpts: [
                { value: 'basic', label: 'Basic ($1215 per user)' },
                { value: 'intermediate', label: 'Intermediate ($2215 per user)' },
                { value: 'complete', label: 'Complete ($3215 per user)' }
            ],


            editObj: {
                subscription: 'yearly',
                plan_type: 'complete',
                payment_type: '1',
                card_type: 'visa'
            },

            confirmUpdateModal: false

        }
    }

    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.type == 'checkbox' ? !editObj[e.target.name] : e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    selectchange = (e, key) => {
        let editObj = this.state.editObj;
        editObj[key] = e;
        this.setState({
            editObj: editObj
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#newPlanForm').validate();
        if (jQuery('#newPlanForm').valid()) {
            this.setState({
                confirmUpdateModal: true
            })
        }

    }

    render() {


        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <h1 className="mainHeader">Manage Plan</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <Row className={'justify-content-center'}>
                                            <Col xl={10} xs={12}>


                                                <form id={'newPlanForm'} onSubmit={this.submitHandler}>
                                                    <div className={'form-group'}>
                                                        <div className={'cmn_select grey_bg center'}>
                                                            <label className={'lab1'}>Subscription</label>
                                                            <div className={'line_h_0'}>
                                                                <label className="error w-100 mb-0" htmlFor="subscription"></label>
                                                            </div>

                                                            <Select
                                                                simpleValue={true}
                                                                name={"subscription"}
                                                                id={'subscription'}
                                                                value={this.state.editObj.subscription || ''}
                                                                options={this.state.subscriptionOpts}
                                                                onChange={(e) => this.selectchange(e, 'subscription')}
                                                                clearable={false}
                                                                required={true}
                                                                searchable={false}
                                                                inputRenderer={(props) =>
                                                                    <input
                                                                        type="text"
                                                                        className="define_input"
                                                                        name={"subscription"}
                                                                        defaultValue={this.state.editObj.subscription || ''}
                                                                        data-rule-required={true}
                                                                        id={'subscription'}

                                                                    />

                                                                }
                                                            />


                                                        </div>
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <div className={'cmn_select grey_bg center'}>
                                                            <label className={'lab1'}>Plan Type</label>
                                                            <div className={'line_h_0'}>
                                                                <label className="error w-100 mb-0" htmlFor="plan_type"></label>
                                                            </div>

                                                            <Select
                                                                simpleValue={true}
                                                                name={"plan_type"}
                                                                id={'plan_type'}
                                                                value={this.state.editObj.plan_type || ''}
                                                                options={this.state.plan_typeOpts}
                                                                onChange={(e) => this.selectchange(e, 'plan_type')}
                                                                clearable={false}
                                                                required={true}
                                                                searchable={false}
                                                                inputRenderer={(props) =>
                                                                    <input
                                                                        type="text"
                                                                        className="define_input"
                                                                        name={"plan_type"}
                                                                        defaultValue={this.state.editObj.plan_type || ''}
                                                                        data-rule-required={true}
                                                                        id={'plan_type'}

                                                                    />

                                                                }
                                                            />
                                                            <div><span className={'t_underline color1 f-12 f-bold c_pointer'}>View plan types</span></div>

                                                        </div>
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}><b>First Payment due:</b>Today</label>
                                                    </div>


                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Amount of lincensees using HCM</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'lincensees'}
                                                            id={'lincensees'}
                                                            value={this.state.editObj.lincensees || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                            data-rule-number={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Discount Code</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'discount_code'}
                                                            id={'discount_code'}
                                                            value={this.state.editObj.discount_code || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <ul className={'text-right amt_dets__'}>
                                                            <li><b>Sub Total:</b>$47,025.00</li>
                                                            <li><b>Discounts:</b>$00.00</li>
                                                            <li><b>GST:</b>$47,02.500</li>
                                                            <li><b>Total:</b>$51,727.50</li>
                                                        </ul>
                                                    </div>

                                                    <div className={'form-group mt-5'}>
                                                        <div className={'d-flex radGrp'}>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="visa"
                                                                    value="visa"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'visa' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="visa">Visa</label>
                                                            </div>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="master_card"
                                                                    value="master_card"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'master_card' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="master_card">Master Card</label>
                                                            </div>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="american_express"
                                                                    value="american_express"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'american_express' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="american_express">American Express</label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Name on the card</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'card_name'}
                                                            id={'card_name'}
                                                            value={this.state.editObj.card_name || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Card Number</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'card_number'}
                                                            id={'card_number'}
                                                            value={this.state.editObj.card_number || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <Row>
                                                            <Col md={6} xs={12}>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>Expiry(MM/YY)</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        name={'expiry'}
                                                                        id={'expiry'}
                                                                        value={this.state.editObj.expiry || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-required={true}
                                                                    />
                                                                </div>
                                                            </Col>
                                                            <Col md={6} xs={12}>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>CVV</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        name={'cvv'}
                                                                        id={'cvv'}
                                                                        value={this.state.editObj.cvv || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-required={true}
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>

                                                    </div>

                                                    <div className={'form-group'}>

                                                        <label className="error lab_sp" htmlFor="agree1"></label>
                                                        <label className="customCheckie">
                                                            <input
                                                                type="checkbox"
                                                                id="agree1"
                                                                onChange={(e) => this.inputHandler(e)}
                                                                value={this.state.editObj.agree1}
                                                                checked={this.state.editObj.agree1 ? true : false}
                                                                name="agree1"
                                                                data-rule-required="true"
                                                            />
                                                            <span>I agree that HCM will charge the above amount monthly to continue my subscription to the software</span>
                                                        </label>

                                                    </div>

                                                    <div className={'form-group'}>

                                                        <label className="error lab_sp" htmlFor="agree2"></label>
                                                        <label className="customCheckie">
                                                            <input
                                                                type="checkbox"
                                                                id="agree2"
                                                                onChange={(e) => this.inputHandler(e)}
                                                                value={this.state.editObj.agree2}
                                                                checked={this.state.editObj.agree2 ? true : false}
                                                                name="agree2"
                                                                data-rule-required="true"
                                                            />
                                                            <span>I have read & agree to the <Link to={'#'} className={'color1 f-bold t_underline'}>Terms & Conditions</Link></span>
                                                        </label>

                                                    </div>


                                                    <div className={'form-group mt-5'}>
                                                        <button className={'btn btn1 mr-3'}>Change Plan Details</button>
                                                        <span className={'color1 f-bolder c_pointer'}>Cancel Subscription</span>
                                                    </div>


                                                </form>
                                            </Col>
                                        </Row>

                                    </div>
                                </div>

                            </Col>

                        </Row>

                        <Modal show={this.state.confirmUpdateModal}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ confirmUpdateModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>Update Plan</h4>
                                <div className={'f-13  mb-3'}>
                                    You have selected the Complete Package to be paid yearly for 15 users Payment will be taken at the end
                                     of the cycle if your current plan and can not be changed for 12 months
                                </div>
                                <div className={'f-13  mb-5'}>
                                    A payment of $51,727.50 will be charged to your American Express card ending 4637 on 11th October 2019
                                </div>
                                <div className={' mb-3'}>
                                    <button onClick={() => this.setState({ confirmUpdateModal: false })} className={'btn btn1'}>Confirm</button>
                                </div>
                            </div>

                        </Modal>

                    </div>
                </section>

            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        planDetails: state.PlanReducer.planDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NewPlan);
