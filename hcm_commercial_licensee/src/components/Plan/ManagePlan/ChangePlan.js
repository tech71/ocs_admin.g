import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PlanSideLinks from '../PlanSideLinks';
import jQuery from "jquery";
import Select from 'react-select-plus';
import Modal from 'hoc/Modal';

class ChangePlan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            subscriptionOpts: [
                { value: 'yearly', label: 'Yearly' },
                { value: 'monthly', label: 'Monthly' }
            ],

            plan_typeOpts: [
                { value: 'basic', label: 'Basic ($1215 per user)' },
                { value: 'intermediate', label: 'Intermediate ($2215 per user)' },
                { value: 'complete', label: 'Complete ($3215 per user)' }
            ],

            payment_typeOpts: [
                { value: '1', label: 'American Express ending 4673' },
                { value: '2', label: 'VISA ending 2345' },
                { value: '3', label: 'Other ending 1574' },
            ],

            editObj: {
                subscription: '',
                plan_type: '',
                payment_type: ''
            },

            confirmUpdateModal: false

        }
    }

    inputHandler = (e) => {
        let editObj = this.state.editObj;
        editObj[e.target.name] = e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    selectchange = (e, key) => {
        let editObj = this.state.editObj;
        editObj[key] = e;
        this.setState({
            editObj: editObj
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#changePlanForm').validate();
        if (jQuery('#changePlanForm').valid()) {
            this.setState({
                confirmUpdateModal: true
            })
        }

    }

    render() {


        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <h1 className="mainHeader">Manage Plan</h1>
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={1} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>
                                        <Row className={'justify-content-center'}>
                                            <Col xl={10} xs={12}>


                                                <form id={'changePlanForm'} onSubmit={this.submitHandler}>
                                                    <div className={'form-group'}>
                                                        <div className={'cmn_select grey_bg center'}>
                                                            <label className={'lab1'}>Subscription</label>
                                                            <div className={'line_h_0'}>
                                                                <label className="error w-100 mb-0" htmlFor="subscription"></label>
                                                            </div>

                                                            <Select
                                                                simpleValue={true}
                                                                name={"subscription"}
                                                                id={'subscription'}
                                                                value={this.state.editObj.subscription || ''}
                                                                options={this.state.subscriptionOpts}
                                                                onChange={(e) => this.selectchange(e, 'subscription')}
                                                                clearable={false}
                                                                required={true}
                                                                searchable={false}
                                                                inputRenderer={(props) =>
                                                                    <input
                                                                        type="text"
                                                                        className="define_input"
                                                                        name={"subscription"}
                                                                        defaultValue={this.state.editObj.subscription || ''}
                                                                        data-rule-required={true}
                                                                        id={'subscription'}

                                                                    />

                                                                }
                                                            />


                                                        </div>
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <div className={'cmn_select grey_bg center'}>
                                                            <label className={'lab1'}>Plan Type</label>
                                                            <div className={'line_h_0'}>
                                                                <label className="error w-100 mb-0" htmlFor="plan_type"></label>
                                                            </div>

                                                            <Select
                                                                simpleValue={true}
                                                                name={"plan_type"}
                                                                id={'plan_type'}
                                                                value={this.state.editObj.plan_type || ''}
                                                                options={this.state.plan_typeOpts}
                                                                onChange={(e) => this.selectchange(e, 'plan_type')}
                                                                clearable={false}
                                                                required={true}
                                                                searchable={false}
                                                                inputRenderer={(props) =>
                                                                    <input
                                                                        type="text"
                                                                        className="define_input"
                                                                        name={"plan_type"}
                                                                        defaultValue={this.state.editObj.plan_type || ''}
                                                                        data-rule-required={true}
                                                                        id={'plan_type'}

                                                                    />

                                                                }
                                                            />
                                                            <div><span className={'t_underline color1 f-12 f-bold c_pointer'}>View plan types</span></div>

                                                        </div>
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <div className={'cmn_select grey_bg center'}>
                                                            <label className={'lab1'}>Payment Type</label>
                                                            <div className={'line_h_0'}>
                                                                <label className="error w-100 mb-0" htmlFor="plan_type"></label>
                                                            </div>

                                                            <Select
                                                                simpleValue={true}
                                                                name={"payment_type"}
                                                                id={'payment_type'}
                                                                value={this.state.editObj.payment_type || ''}
                                                                options={this.state.payment_typeOpts}
                                                                onChange={(e) => this.selectchange(e, 'payment_type')}
                                                                clearable={false}
                                                                required={true}
                                                                searchable={false}
                                                                inputRenderer={(props) =>
                                                                    <input
                                                                        type="text"
                                                                        className="define_input"
                                                                        name={"payment_type"}
                                                                        defaultValue={this.state.editObj.payment_type || ''}
                                                                        data-rule-required={true}
                                                                        id={'payment_type'}

                                                                    />

                                                                }
                                                            />

                                                        </div>
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Amount of lincensees using HCM</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'lincensees'}
                                                            id={'lincensees'}
                                                            value={this.state.editObj.lincensees || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                            data-rule-number={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Discount Code</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'discount_code'}
                                                            id={'discount_code'}
                                                            value={this.state.editObj.discount_code || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                        />
                                                    </div>
                                                    <div className={'form-group'}>
                                                        <ul className={'text-right amt_dets__'}>
                                                            <li><b>Sub Total:</b>$47,025.00</li>
                                                            <li><b>Discounts:</b>$00.00</li>
                                                            <li><b>GST:</b>$47,02.500</li>
                                                            <li><b>Total:</b>$51,727.50</li>
                                                        </ul>
                                                    </div>


                                                    <div className={'form-group mt-5'}>
                                                        <button className={'btn btn1 mr-3'}>Change Plan Details</button>
                                                        <span className={'color1 f-bolder c_pointer'}>Cancel Subscription</span>
                                                    </div>


                                                </form>
                                            </Col>
                                        </Row>

                                    </div>
                                </div>

                            </Col>

                        </Row>

                        <Modal show={this.state.confirmUpdateModal}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ confirmUpdateModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>Update Plan</h4>
                                <div className={'f-13  mb-3'}>
                                    You have selected the Complete Package to be paid yearly for 15 users Payment will be taken at the end
                                     of the cycle if your current plan and can not be changed for 12 months
                                </div>
                                <div className={'f-13  mb-5'}>
                                    A payment of $51,727.50 will be charged to your American Express card ending 4637 on 11th October 2019
                                </div>
                                <div className={' mb-3'}>
                                    <button onClick={() => this.setState({ confirmUpdateModal: false })} className={'btn btn1'}>Confirm</button>
                                </div>
                            </div>

                        </Modal>

                    </div>
                </section>

            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        planDetails: state.PlanReducer.planDetails
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(ChangePlan);
