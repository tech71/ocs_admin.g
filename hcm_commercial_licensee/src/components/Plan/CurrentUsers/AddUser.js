import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PlanSideLinks from '../PlanSideLinks';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import MainHeading from 'hoc/MainHeading.js';
import Select from 'react-select-plus';
import jQuery from "jquery";
import moment from 'moment';

class AddUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            userObj: {
                user_status: '1',
                system_role: '1',
                department:'1',
                permissions: [],
                subpermissions: [],
                permsArray: [

                    { "name": "CRM", "id": "11", "role_key": "crm" },
                    { "name": "CRM Admin", "id": "12", "role_key": "crm_admin" },
                    { "name": "Finance", "id": "16", "role_key": "finance" },
                    { "name": "Finance Admin", "id": "17", "role_key": "finance_admin" },
                    { "name": "Fms", "id": "7", "role_key": "fms" },
                    { "name": "Fms incentive", "id": "8", "role_key": "fms_incentive" },
                    { "name": "Recruitment", "id": "10", "role_key": "recruitment" },
                    { "name": "Recruitment Admin", "id": "14", "role_key": "recruitment_admin" },
                    { "name": "Plan Management Module", "id": "13", "role_key": "finance_planner" },
                    { "name": "Imail", "id": "9", "role_key": "imail" },
                    { "name": "Marketing", "id": "15", "role_key": "marketing" },
                    { "name": "Member", "id": "3", "role_key": "member" },
                    { "name": "Admin", "id": "1", "role_key": "admin" },
                    { "name": "Organization", "id": "4", "role_key": "organization" },
                    { "name": "Participant", "id": "2", "role_key": "participant" },
                    { "name": "Schedule", "id": "6", "role_key": "schedule" }

                ]
            },

            userStatusOptions: [
                { value: '1', label: 'Active' },
                { value: '2', label: 'Inactive' }
            ],

            systemRoleOptions: [
                { value: '1', label: 'Admin' },
                { value: '2', label: 'User' }
            ],
           
            departmentOptions:[
                {"value":"1","label":"Accommodation & Client Services","short_code":"internal_staff"},
                {"value":"3","label":"People & Culture","short_code":"internal_staff"},
                {"value":"4","label":"Business Systems","short_code":"internal_staff"},
                {"value":"5","label":"Marketing","short_code":"internal_staff"},
                {"value":"6","label":"Finance","short_code":"internal_staff"}
            ]

        }
    }

    inputHandler = (e) => {
        let userObj = this.state.userObj;
        userObj[e.target.name] = e.target.type == 'checkbox' ? !userObj[e.target.name] : e.target.value;
        this.setState({ userObj })
    }

    selectchange = (e, key) => {
        let userObj = this.state.userObj;
        userObj[key] = e;
        this.setState({ userObj })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#addUserForm').validate();
        if (jQuery('#addUserForm').valid()) {
            alert('valid, in progress')
        }
    }

    checkboxArrHandler = (e, key) => {

        let userObj = this.state.userObj;
        let arr = userObj[key];

        let index = arr.indexOf(e.target.value);
        if (index == -1) {
            arr.push(e.target.value);
        } else {
            arr.splice(index, 1);
        }
        userObj[key] = arr;
        this.setState({
            userObj
        })

    }

    permissionCheckHandler = (e, idx) => {

        var userObj = this.state.userObj;
        var List = userObj['permsArray'];

       
        if (List[idx].id == 7 && List[idx].id) {
            var key = List.findIndex(x => x.id == 8)
            List[key].access = false;
        }
        if (List[idx].id == 11 && List[idx].id) {
            var key = List.findIndex(x => x.id == 12)
            List[key].access = false;
        }
        if (List[idx].id == 10 && List[idx].id) {
            var key = List.findIndex(x => x.id == 14)
            List[key].access = false;
        }
        if (List[idx].id == 16 && List[idx].id) {
            var key = List.findIndex(x => x.id == 17)
            List[key].access = false;
        }

        if (!List[idx].access) {
            this.setState({ access_error: false });
            List[idx].access = true;
        } else {
            List[idx].access = false;
        }

        userObj['permsArray'] = List;

        this.setState({ userObj });

    }


    render() {


        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <MainHeading title={'Current Users'} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={2} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <form id={'addUserForm'} >
                                            <div className={'form-group'}>
                                                <label className={'lab1'}>Name</label>
                                                <input
                                                    type={'text'}
                                                    className={'form-control1'}
                                                    name={'name'}
                                                    id={'name'}
                                                    value={this.state.userObj.name || ''}
                                                    onChange={(e) => this.inputHandler(e)}
                                                    data-rule-required={true}
                                                />
                                            </div>
                                            <div className={'form-group'}>
                                                <label className={'lab1'}>Email</label>
                                                <input
                                                    type={'text'}
                                                    className={'form-control1'}
                                                    name={'email'}
                                                    id={'email'}
                                                    value={this.state.userObj.email || ''}
                                                    onChange={(e) => this.inputHandler(e)}
                                                    data-rule-required={true}
                                                    data-rule-email={true}
                                                />
                                            </div>
                                            <div className={'form-group'}>
                                                <label className={'lab1'}>Contact number</label>
                                                <input
                                                    type={'text'}
                                                    className={'form-control1'}
                                                    name={'contact'}
                                                    id={'contact'}
                                                    value={this.state.userObj.contact || ''}
                                                    onChange={(e) => this.inputHandler(e)}
                                                    data-rule-required={true}
                                                    data-rule-number={true}
                                                />
                                            </div>

                                            <div className={'form-group'}>
                                                <label className={'lab1'}>
                                                    <b>Date Added:</b> {moment(new Date()).format("DD MMMM YYYY")}
                                                </label>
                                            </div>

                                            <div className={'form-group'}>
                                                <label className={'lab1'}>User status</label>
                                                <div className={'line_h_0 pos_relative'}>
                                                    <label className="error w-100 mb-0 pos_absolute" htmlFor="user_status"></label>
                                                </div>
                                                <div className={'cmn_select grey_bg center'}>
                                                    <Select
                                                        simpleValue={true}
                                                        name={"user_status"}
                                                        id={'user_status'}
                                                        value={this.state.userObj.user_status || ''}
                                                        options={this.state.userStatusOptions}
                                                        onChange={(e) => this.selectchange(e, 'user_status')}
                                                        clearable={false}
                                                        searchable={false}
                                                        inputRenderer={(props) =>
                                                            <input
                                                                type="text"
                                                                className="define_input"
                                                                name={"user_status"}
                                                                defaultValue={this.state.userObj.user_status || ''}
                                                                id={'user_status'}
                                                                data-rule-required={true}
                                                            />

                                                        }
                                                    />

                                                </div>
                                            </div>

                                            <div className={'form-group'}>
                                                <label className={'lab1'}>System Role</label>
                                                <div className={'line_h_0 pos_relative'}>
                                                    <label className="error w-100 mb-0 pos_absolute" htmlFor="user_status"></label>
                                                </div>
                                                <div className={'cmn_select grey_bg center'}>
                                                    <Select
                                                        simpleValue={true}
                                                        name={"system_role"}
                                                        id={'system_role'}
                                                        value={this.state.userObj.system_role || ''}
                                                        options={this.state.systemRoleOptions}
                                                        onChange={(e) => this.selectchange(e, 'system_role')}
                                                        clearable={false}
                                                        searchable={false}
                                                        inputRenderer={(props) =>
                                                            <input
                                                                type="text"
                                                                className="define_input"
                                                                name={"system_role"}
                                                                defaultValue={this.state.userObj.system_role || ''}
                                                                id={'system_role'}
                                                                data-rule-required={true}
                                                            />

                                                        }
                                                    />

                                                </div>
                                            </div>


                                            {
                                                this.state.userObj.system_role == '2' ?
                                                    <React.Fragment>
                                                        <div className={'form-group'}>
                                                            <label className={'lab1'}>Position</label>
                                                            <input
                                                                type={'text'}
                                                                className={'form-control1'}
                                                                name={'position'}
                                                                id={'position'}
                                                                value={this.state.userObj.position || ''}
                                                                onChange={(e) => this.inputHandler(e)}
                                                                data-rule-required={true}
                                                            />
                                                        </div>

                                                        <div className={'form-group'}>
                                                            <label className={'lab1'}>Department</label>
                                                            <div className={'line_h_0 pos_relative'}>
                                                                <label className="error w-100 mb-0 pos_absolute" htmlFor="department"></label>
                                                            </div>
                                                            <div className={'cmn_select grey_bg center'}>
                                                                <Select
                                                                    simpleValue={true}
                                                                    name={"department"}
                                                                    id={'department'}
                                                                    value={this.state.userObj.department || ''}
                                                                    options={this.state.departmentOptions}
                                                                    onChange={(e) => this.selectchange(e, 'department')}
                                                                    clearable={false}
                                                                    searchable={false}
                                                                    required={true}
                                                                    inputRenderer={(props) =>
                                                                        <input
                                                                            type="text"
                                                                            className="define_input"
                                                                            name={"department"}
                                                                            defaultValue={this.state.userObj.department || ''}
                                                                            id={'department'}
                                                                            data-rule-required={true}
                                                                        />

                                                                    }
                                                                />

                                                            </div>
                                                        </div>

                                                        <div className={'form-group'}>
                                                            <label className={'lab1 mb-3'}>Permissions</label>

                                                            
                                                            <div className={'line_h_0 pos_relative'}>
                                                                <label className="error w-100 mb-0 pos_absolute" htmlFor="permissions"></label>
                                                            </div>
                                                            <ul className={'persmission_ul__ mb-4'}>

                                                                {
                                                                    this.state.userObj.permsArray !== undefined && this.state.userObj.permsArray !== null && this.state.userObj.permsArray.length > 0 ?
                                                                        this.state.userObj.permsArray.map((val, i) => {
                                                                            return (
                                                                                <li key={i}
                                                                                    className={
                                                                                        val.role_key == 'crm_admin' ||
                                                                                            val.role_key == 'finance_admin' ||
                                                                                            val.role_key == 'fms_incentive' ||
                                                                                            val.role_key == 'recruitment_admin' ?
                                                                                            'd-none' : ''
                                                                                    }

                                                                                >

                                                                                    <Row>

                                                                                        <Col md={6}>
                                                                                            {/* {console.log(val.status)} */}
                                                                                            <label className="customCheckie permisCheck">
                                                                                                <input
                                                                                                    type="checkbox"
                                                                                                    id={val.id}
                                                                                                    onChange={(e) => this.permissionCheckHandler(e, i)}
                                                                                                    value={val.id}
                                                                                                    name={'permissions'}
                                                                                                    checked={
                                                                                                        val.hasOwnProperty('access') && val.access ?
                                                                                                            true : false
                                                                                                    }
                                                                                                   data-rule-required={true}
                                                                                                   data-msg-required={'please select atleast 1 access'}
                                                                                                />
                                                                                                <span><span className={"mod_circle " + (val.role_key+'_img')}>{val.name.charAt(0)}</span>{val.name}</span>
                                                                                            </label>
                                                                                        </Col>
                                                                                        <Col md={6}>

                                                                                            {
                                                                                                val.role_key == 'crm' ?
                                                                                                    <label className="customCheckie permisCheck">
                                                                                                        <input
                                                                                                            type="checkbox"
                                                                                                            checked={
                                                                                                                (this.state.userObj.permsArray.find(x => x.role_key == 'crm_admin').access) ? true : false || ''
                                                                                                            }
                                                                                                            onChange={(e) => this.permissionCheckHandler(e, this.state.userObj.permsArray.findIndex(x => x.role_key == 'crm_admin'))}
                                                                                                            disabled={val.access ? false : true}
                                                                                                        />
                                                                                                        <span> Admin for module</span>
                                                                                                    </label>

                                                                                                    :

                                                                                                    val.role_key == 'finance' ?
                                                                                                        <label className="customCheckie permisCheck">
                                                                                                            <input
                                                                                                                type="checkbox"
                                                                                                                checked={
                                                                                                                    (this.state.userObj.permsArray.find(x => x.role_key == 'finance_admin').access) ? true : false || ''
                                                                                                                }
                                                                                                                onChange={(e) => this.permissionCheckHandler(e, this.state.userObj.permsArray.findIndex(x => x.role_key == 'finance_admin'))}
                                                                                                                disabled={val.access ? false : true}
                                                                                                            />
                                                                                                            <span>Admin for module</span>
                                                                                                        </label>

                                                                                                        :
                                                                                                        val.role_key == 'fms' ?
                                                                                                            <label className="customCheckie permisCheck">
                                                                                                                <input
                                                                                                                    type="checkbox"
                                                                                                                    checked={
                                                                                                                        (this.state.userObj.permsArray.find(x => x.role_key == 'fms_incentive').access) ? true : false || ''
                                                                                                                    }
                                                                                                                    onChange={(e) => this.permissionCheckHandler(e, this.state.userObj.permsArray.findIndex(x => x.role_key == 'fms_incentive'))}
                                                                                                                    disabled={val.access ? false : true}
                                                                                                                />
                                                                                                                <span>Incidents</span>
                                                                                                            </label>

                                                                                                            :
                                                                                                            val.role_key == 'recruitment' ?
                                                                                                                <label className="customCheckie permisCheck">
                                                                                                                    <input
                                                                                                                        type="checkbox"
                                                                                                                        checked={
                                                                                                                            (this.state.userObj.permsArray.find(x => x.role_key == 'recruitment_admin').access) ? true : false || ''
                                                                                                                        }
                                                                                                                        onChange={(e) => this.permissionCheckHandler(e, this.state.userObj.permsArray.findIndex(x => x.role_key == 'recruitment_admin'))}
                                                                                                                        disabled={val.access ? false : true}
                                                                                                                    />
                                                                                                                    <span>Admin for module</span>
                                                                                                                </label>

                                                                                                                : null

                                                                                            }

                                                                                        </Col>

                                                                                    </Row>

                                                                                </li>

                                                                            )
                                                                        })

                                                                        : null
                                                                }
                                                            </ul>
                                                        </div>

                                                    </React.Fragment> : null

                                            }


                                            <div className={'form-group'}>

                                                <label className="error lab_sp" htmlFor="agree1"></label>
                                                <label className="customCheckie">
                                                    <input
                                                        type="checkbox"
                                                        id="send_notifications"
                                                        onChange={(e) => this.inputHandler(e)}
                                                        value={this.state.userObj.send_notifications}
                                                        checked={this.state.userObj.send_notifications ? true : false}
                                                        name="send_notifications"
                                                    />
                                                    <span>Send user notifications</span>
                                                </label>

                                            </div>

                                            <div className={'form-group mt-5'}>
                                                <button className={'btn btn1 mr-3'} onClick={this.submitHandler}>Add New User</button>
                                                <Link to={`${ROUTER_PATH}plan/current_users`}><span className={'color1 f-bolder'}>Back</span></Link>
                                            </div>

                                        </form>


                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }

}



const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(null, mapDispatchToProps)(AddUser);
