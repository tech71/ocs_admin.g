import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PlanSideLinks from '../PlanSideLinks';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import MainHeading from 'hoc/MainHeading.js';
import Select from 'react-select-plus';
import ReactTable from 'react-table-v6';

class CurrentUsers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            searchObj: {
                filterBy: '1'
            },

            filterByOptions: [
                { value: '1', label: 'Profiles Filed' },
                { value: '2', label: 'Archieve Users' }
            ]

        }
    }

    inputHandler = (e) => {
        let searchObj = this.state.searchObj;
        searchObj[e.target.name] = e.target.value;
        this.setState({ searchObj })
    }

    selectchange = (e, key) => {
        let searchObj = this.state.searchObj;
        searchObj[key] = e;
        this.setState({ searchObj })
    }



    render() {

        const columns = [
            {
                Header: 'Status',
                accessor: 'status',
                maxWidth: 70,
                Cell: (props) =>
                    <React.Fragment>
                        <div className={'text-center w-100'}>
                            {props.original.status == 'active' ?
                                <span className={'ED_lab enable'}>Active</span> :
                                <span className={'ED_lab disable'}>Inactive</span>

                            }
                        </div>
                    </React.Fragment>
            },

            { Header: 'Date', accessor: 'date', maxWidth: 120, Cell: (props) => <div>{props.original.date}</div> },
            { Header: 'name', accessor: 'name', Cell: (props) => <div><b>{props.original.name}</b></div> },
            { Header: 'post', accessor: 'post', maxWidth: 120, Cell: (props) => <div>{props.original.post}</div> },
            {
                Header: 'Action',
                maxWidth: 62,
                sortable: false,
                Cell: (props) => <div className="action_c w-100 pr-3">

                    <Link to={`#`}>
                        <i className="fa fa-pencil watch_ic  mr-2" ></i>
                    </Link>

                    <i className="fa fa-refresh delete_ic"></i>
                </div>
            },
        ]

        return (
            <React.Fragment>
                <section className="enqSection cmnSec">
                    <MainHeading title={'Current Users'} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={2} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <ul className={'dets_Ul__ mt-3'}>
                                            <li><b>User profiles filled:</b> {this.props.currentUsersDetails.profiles_filled}</li>
                                            <li><b>Available spaces:</b> {this.props.currentUsersDetails.available_spaces}</li>
                                        </ul>
                                        <Link to={`${ROUTER_PATH}plan/addnew_user`}>
                                            <button className={'btn btn1 mt-3'}>Add New User</button>
                                        </Link>
                                        <form id={'searchTopicform'} className={'mt-4 w-100'}>
                                            <Row>



                                                <Col xl={8} lg={7} md={6} xs={12}>
                                                    <div>
                                                        <label className={'lab1'}>Search users</label>
                                                        <div className="input-group1 right">
                                                            <input
                                                                type="text"
                                                                className="form-control1"
                                                                aria-label="search_name"
                                                                value={this.state.searchObj.user}
                                                                onChange={(e) => this.inputHandler(e)}
                                                            />
                                                            <div className="input-group-append">
                                                                <span className="input-group-text color1" id="basic-addon2"><i className={'fa fa-search'}></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col xl={4} lg={5} md={6} xs={12}>

                                                    <div className={'line_h_0 pos_relative'}>
                                                        <label className="error w-100 mb-0 pos_absolute" htmlFor="subscription"></label>
                                                    </div>
                                                    <label className={'lab1'}>Subscription</label>

                                                    <div className={'cmn_select grey_bg '}>
                                                        <Select
                                                            simpleValue={true}
                                                            name={"filterBy"}
                                                            id={'filterBy'}
                                                            value={this.state.searchObj.filterBy || ''}
                                                            options={this.state.filterByOptions}
                                                            onChange={(e) => this.selectchange(e, 'filterBy')}
                                                            clearable={false}
                                                            searchable={false}
                                                            
                                                        />

                                                    </div>
                                                </Col>

                                            </Row>
                                        </form>

                                        <Row>
                                            <div className="table_common__ notifyTable w-100 mt-5">
                                                <ReactTable
                                                    data={this.props.currentUsersList}
                                                    columns={columns}
                                                    minRows={1}
                                                    defaultPageSize={10}
                                                    loading={this.state.loading}
                                                    loadingText={'loading...'}
                                                    previousText={<span className="icon fa fa-angle-left previous"></span>}
                                                    nextText={<span className="icon fa fa-angle-right next"></span>}
                                                />
                                            </div>
                                        </Row>

                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }

}

const mapStateToProps = state => {
    return {
        currentUsersDetails: state.PlanReducer.currentUsersDetails,
        currentUsersList: state.PlanReducer.currentUsersList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(CurrentUsers);
