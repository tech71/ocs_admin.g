import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PlanSideLinks from '../PlanSideLinks';
import MainHeading from 'hoc/MainHeading.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from 'hoc/Modal';

class Integrations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal1: false

        }
    }



    componentWillMount() {

    }

    render() {




        return (
            <React.Fragment>
                <section className="bilSection cmnSec">
                    <MainHeading title={'Integrations'} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={5} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>


                                        <div className={'boxList integrationFeats'}>
                                            <div className={'boxie__'}>
                                                <div className={'name'}><b>Xero</b></div>
                                                <div className={'lab'}><span className="ED_lab enable">Enabled</span></div>
                                                <div className={'action'}><i onClick={() => this.setState({ modal1: true })} className={'fa fa-pencil color1 c_pointer'}></i></div>
                                            </div>
                                            <div className={'boxie__'}>
                                                <div className={'name'}><b>Keypay</b></div>
                                                <div className={'lab'}><span className="ED_lab disable">Disabled</span></div>
                                                <div className={'action'}><i onClick={() => this.setState({ modal2: true })} className={'fa fa-pencil color1 c_pointer'}></i></div>
                                            </div>
                                            <div className={'boxie__'}>
                                                <div className={'name'}><b>SMS Messagings</b></div>
                                                <div className={'lab'}><span className="ED_lab disable">Disabled</span></div>
                                                <div className={'action'}><i className={'fa fa-pencil color1 c_pointer'}></i></div>
                                            </div>
                                            <div className={'boxie__'}>
                                                <div className={'name'}><b>Emails</b></div>
                                                <div className={'lab'}><span className="ED_lab disable">Disabled</span></div>
                                                <div className={'action'}><i className={'fa fa-pencil color1 c_pointer'}></i></div>
                                            </div>
                                        </div>



                                    </div>
                                </div>

                            </Col>

                        </Row>


                        <Modal show={this.state.modal1}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ modal1: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>
                                    Remove Xero Integration?
                                </h4>
                                <div className={'f-13  mb-3'}>
                                    Are you sure you want to remove this integration?<br />
                                    You can re-add again in the future.
                                </div>
                                <div className={' mb-3 mt-4'}>
                                    <button onClick={() => this.setState({ modal1: false })} className={'btn btn1'}>Remove Integration</button>
                                </div>

                            </div>

                        </Modal>
                        <Modal show={this.state.modal2}>
                            <div className={'text-right'}>
                                <i onClick={() => this.setState({ modal2: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                            </div>
                            <div className={'text-center'}>
                                <h4 className={'color1 f-bolder mb-4'}>
                                    Add Keypay Integration?
                                </h4>
                                <form>
                                    <div className={'form-group text-left'}>
                                        <label className={'lab1'}>Key</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            data-rule-required={true}
                                            name={'keypay'}
                                            // value={this.state.keypay || ''}
                                            // onChange={(e) => this.inputHandler(e)}
                                        />
                                    </div>
                                    <div className={' mb-3 mt-4'}>
                                        <button onClick={() => this.setState({ modal2: false })} className={'btn btn1'}>Add Integration</button>
                                    </div>
                                </form>


                            </div>
                        </Modal>
                    </div>
                </section>

            </React.Fragment>

        )
    }

}

const mapStateToProps = state => {
    return {
        billingList: state.PlanReducer.billingList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Integrations);
