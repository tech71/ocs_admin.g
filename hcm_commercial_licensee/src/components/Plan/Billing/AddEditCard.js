import React from 'react';
import PlanSideLinks from '../PlanSideLinks';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import MainHeading from 'hoc/MainHeading.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Col, Row } from 'react-bootstrap';
import jQuery from "jquery";


class AddEditCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            heading: 'Add New Card',
            editObj:{
                card_type:'visa'
            }
        }
    }

    inputHandler = (e) => {

        let editObj = this.state.editObj;
        editObj[e.target.name] =  e.target.type == 'checkbox' ? !editObj[e.target.name] : e.target.value;
        this.setState({
            editObj: editObj
        })
    }

    submitHandler = (e) => {
        e.preventDefault();
        jQuery('#CardForm').validate();
        if(jQuery('#CardForm').valid()){
            alert('valid, progress ')
        }
    }

    


    render() {
        return (
            <React.Fragment>
                <section className="bilSection cmnSec">
                    <MainHeading title={this.state.heading} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={3} />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>

                                        <Row className={'justify-content-center'}>
                                            <Col xl={10} xs={12}>

                                                <form id={'CardForm'} onSubmit={this.submitHandler}>
                                                    <div className={'form-group mt-5'}>
                                                        <div className={'d-flex radGrp'}>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="visa"
                                                                    value="visa"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'visa' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="visa">Visa</label>
                                                            </div>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="master_card"
                                                                    value="master_card"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'master_card' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="master_card">Master Card</label>
                                                            </div>

                                                            <div className="customRadio">
                                                                <input
                                                                    type="radio"
                                                                    id="american_express"
                                                                    value="american_express"
                                                                    name="card_type"
                                                                    checked={this.state.editObj.card_type == 'american_express' ? true : false}
                                                                    onChange={(e) => this.inputHandler(e)}
                                                                />
                                                                <label htmlFor="american_express">American Express</label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Name on the card</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'card_name'}
                                                            id={'card_name'}
                                                            value={this.state.editObj.card_name || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <label className={'lab1'}>Card Number</label>
                                                        <input
                                                            type={'text'}
                                                            className={'form-control1'}
                                                            name={'card_number'}
                                                            id={'card_number'}
                                                            value={this.state.editObj.card_number || ''}
                                                            onChange={(e) => this.inputHandler(e)}
                                                            data-rule-required={true}
                                                        />
                                                    </div>

                                                    <div className={'form-group'}>
                                                        <Row>
                                                            <Col md={6} xs={12}>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>Expiry(MM/YY)</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        name={'expiry'}
                                                                        id={'expiry'}
                                                                        value={this.state.editObj.expiry || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-required={true}
                                                                    />
                                                                </div>
                                                            </Col>
                                                            <Col md={6} xs={12}>
                                                                <div className={'form-group'}>
                                                                    <label className={'lab1'}>CVV</label>
                                                                    <input
                                                                        type={'text'}
                                                                        className={'form-control1'}
                                                                        name={'cvv'}
                                                                        id={'cvv'}
                                                                        value={this.state.editObj.cvv || ''}
                                                                        onChange={(e) => this.inputHandler(e)}
                                                                        data-rule-required={true}
                                                                    />
                                                                </div>
                                                            </Col>
                                                        </Row>

                                                    </div>

                                                    <div className={'form-group'}>

                                                        <label className="error lab_sp"  htmlFor="default"></label>
                                                        <label className="customCheckie defaultCardCheck">
                                                            <input
                                                                type="checkbox"
                                                                id="default"
                                                                onChange={(e) => this.inputHandler(e)}
                                                                value={this.state.editObj.default}
                                                                checked={this.state.editObj.default ? true : false}
                                                                name="default"
                                                                data-rule-required="true"
                                                            />
                                                            <span className={'f-13 f-bold'}>Set as default Card</span>
                                                        </label>

                                                    </div>

                                                    <div className={'form-group'}>

                                                        <label className="error lab_sp"  htmlFor="agree"></label>
                                                        <label className="customCheckie">
                                                            <input
                                                                type="checkbox"
                                                                id="agree"
                                                                onChange={(e) => this.inputHandler(e)}
                                                                value={this.state.editObj.agree}
                                                                checked={this.state.editObj.agree ? true : false}
                                                                name="agree"
                                                                data-rule-required="true"
                                                            />
                                                            <span>I have read & agree to the <Link to={'#'} className={'color1 f-bold t_underline'}>Terms & Conditions</Link></span>
                                                        </label>

                                                    </div>

                                                    <div className={'form-group mt-5'}>
                                                        <button className={'btn btn1 mr-3'}>Save Card</button>
                                                        <Link to={`${ROUTER_PATH}plan/billing`}>
                                                            <span className={'color1 f-bolder c_pointer'}>Back</span>
                                                        </Link>
                                                    </div>


                                                </form>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>

            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {
    return {
        billingList: state.PlanReducer.billingList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(AddEditCard);
