import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PlanSideLinks from '../PlanSideLinks';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import MainHeading from 'hoc/MainHeading.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from 'hoc/Modal';

class Billing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            billingList: [],
            deleteCard: {

            },
            confirmDeleteModal: false

        }
    }

    checkBillingHandler = (e) => {
        var billingList = this.state.billingList;

        billingList.map(val => {
            val['primary'] = 'false';
            if (e.target.id == val.id) {
                val['primary'] = 'true';
            }
        })
        this.setState({ billingList })

    }

    deleteCardHandler = (index) => {
        const billingList = this.state.billingList;
        const deleteCard = billingList[index];
        this.setState({
            deleteCard,
            confirmDeleteModal: true
        })
    }

    componentWillMount() {
        this.setState({
            billingList: this.props.billingList
        })
    }

    render() {




        return (
            <React.Fragment>
                <section className="bilSection cmnSec">
                    <MainHeading title={'Billing'} />
                    <div className="transBox">
                        <Row>
                            <Col xl={3} lg={4} md={4} xs={12} className={'text-center'}>
                                <PlanSideLinks active={3}  />
                            </Col>
                            <Col xl={9} lg={8} md={8} xs={12}>

                                <div className="white_bx__">
                                    <div className={'compPad__'}>


                                        <div className={'boxList billingFeats'}>


                                            {
                                                this.state.billingList !== undefined && this.state.billingList.length > 0 ?
                                                    this.state.billingList.map((val, i) => {
                                                        return (
                                                            <div className={' boxie__'} key={i}>
                                                                <div className={'bank__'}>
                                                                    <label className="starCheck">
                                                                        <input
                                                                            type="radio"
                                                                            id={val.id}
                                                                            onChange={(e) => this.checkBillingHandler(e)}
                                                                            value={val.id}
                                                                            checked={val.primary == 'true' ? true : false}
                                                                            name={"billing"}
                                                                        />
                                                                        <span>{val.card}</span>
                                                                    </label>
                                                                </div>
                                                                <div className={'numb__'}>{'**** **** **** ' + val.number}</div>
                                                                <div className={'exp__'}>Expiry {val.expiry}</div>
                                                                <div className={'action__'}>
                                                                    <i className={'fa fa-pencil edit'}></i>
                                                                    <i onClick={() => this.deleteCardHandler(i)} className={'fa fa-close delete'}></i>
                                                                </div>
                                                            </div>
                                                        )
                                                    }) : null
                                            }


                                        </div>


                                        <Link to={`${ROUTER_PATH}plan/addnew_card`}>
                                            <button className={'btn btn1 mt-3'}>Add New Card</button>
                                        </Link>

                                        {
                                            this.state.confirmDeleteModal ?
                                                <Modal show={this.state.confirmDeleteModal}>
                                                    <div className={'text-right'}>
                                                        <i onClick={() => this.setState({ confirmDeleteModal: false })} className={'fa fa-close color1 closeModal mr-3 c_pointer'}></i>
                                                    </div>
                                                    <div className={'text-center'}>
                                                        <h4 className={'color1 f-bolder mb-4'}>
                                                            Delete {this.state.deleteCard.card} card from billing list?
                                                        </h4>
                                                        <div className={'f-13  mb-3'}>
                                                            {this.state.deleteCard.card} card ending in {this.state.deleteCard.number},
                                                    expiring {this.state.deleteCard.expiry} will be deleted from the system. It will no longer
                                                                    come up as a payment option.You can add it back in the future if you wish to use this card
                                                                    again.
                                                </div>
                                                        <div className={' mb-3 mt-4'}>
                                                            <button onClick={() => this.setState({ confirmDeleteModal: false })} className={'btn btn1'}>Delete Card</button>
                                                        </div>
                                                    </div>
                                                </Modal> : null
                                        }



                                    </div>
                                </div>

                            </Col>

                        </Row>
                    </div>
                </section>

            </React.Fragment>

        )
    }

}

const mapStateToProps = state => {
    return {
        billingList: state.PlanReducer.billingList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Billing);
