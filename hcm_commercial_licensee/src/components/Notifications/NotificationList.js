import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactTable from 'react-table-v6';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

class NotificationList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {

        const columns = [
            {
                Header: 'Status',
                accessor: 'status',
                width: 50,
                Cell: (props) => <React.Fragment>{props.original.status == 'true' ? <span className="activeNoti_ ml-3"></span> : ''}</React.Fragment>
            },
            { Header: 'Date', accessor: 'date', maxWidth: 120, Cell: (props) => <div>{props.original.date}</div> },
            { Header: "Title", accessor: 'title', Cell: (props) => <div>{props.original.title}</div> },
            {
                Header: 'Action',
                // maxWidth: 100,
                sortable: false,
                Cell: (props) => <div className="action_c text-right w-100 pr-3">
                    <i className="fa fa-envelope edit_ic mr-2" ></i>
                    <Link to={`${ROUTER_PATH}notification_list/notification_view/${props.original.id}`}>
                        <i className="fa fa-eye watch_ic  mr-2" ></i>
                    </Link>

                    <i className="fa fa-close delete_ic"></i>
                </div>
            },
        ]


        return (
            <React.Fragment>

                <section className={'notifySec cmnSec'}>
                    <h1 className="mainHeader">Notifications</h1>
                    <div className="transBox">

                        <Row>
                            <Col xs={12}>
                                <div className="table_common__ notifyTable">
                                    <ReactTable
                                        data={this.props.notificationList}
                                        columns={columns}
                                        minRows={1}
                                        defaultPageSize={10}
                                        loading={this.state.loading}
                                        loadingText={'loading...'}
                                        previousText={<span className="icon fa fa-angle-left previous"></span>}
                                        nextText={<span className="icon fa fa-angle-right next"></span>}
                                    />
                                </div>
                            </Col>
                        </Row>

                    </div>
                </section>

            </React.Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        notificationList: state.NotificationReducer.notificationList
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NotificationList);
