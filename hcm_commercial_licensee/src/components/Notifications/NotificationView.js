import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import { Col, Row } from 'react-bootstrap';


const NotificationView = (props) => {
    return (
        <React.Fragment>
            <section className={'notifySec cmnSec'}>
                <h1 className="mainHeader">{props.notificationView.title}</h1>
                <div className="transBox">
                    <Row className={'justify-content-center'}>
                        <Col xl={10} xs={12}>
                            <div>
                                <div className={'mb-4'}>{props.notificationView.date}</div>
                                <div>{props.notificationView.description}</div>

                                <div className={'mt-5 mb-4'}>
                                    <Link to={`${ROUTER_PATH}notification_list`}>
                                        <button className={'btn btn1'}>
                                            <i className={'fa fa-arrow-left mr-3'}></i> 
                                            Back to Notifications
                                        </button>
                                    </Link>
                                    <span className={'color1 ml-3 f-bold c_pointer'}>Delete</span>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>

        </React.Fragment>
    );
}

const mapStateToProps = state => {
    return {
        notificationView: state.NotificationReducer.notificationView
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NotificationView);