import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top';
import { ROUTER_PATH, } from './services/config';
import { ToastContainer } from 'react-toastify';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-table-v6/react-table.css';
import 'react-select-plus/dist/react-select-plus.css';
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';


import './services/jquery.validate.js';
import './services/custom_script.js';

import Dashboard from './components/Dashboard/Dashboard';
import NotificationList from './components/Notifications/NotificationList';
import NotificationView from './components/Notifications/NotificationView';
import CompanyDetails from './components/Profile/CompanyDetails/CompanyDetails';
import Help from './components/Help/KnowledgeBase/KnowledgeBase';
import Enquiries from './components/Help/Enquiries/Enquiries';
import TopicList from './components/Help/KnowledgeBase/TopicList';
import TopicDetails from './components/Help/KnowledgeBase/TopicDetails';
import NewEnquiry from './components/Help/Enquiries/NewEnquiry';
import EnquiryDetails from './components/Help/Enquiries/EnquiryDetails';
import ManagePlan from './components/Plan/ManagePlan/ManagePlan';
import Footer from './components/Footer';
import Header from './components/Header';
import ChangePlan from './components/Plan/ManagePlan/ChangePlan';
import NewPlan from './components/Plan/ManagePlan/NewPlan';
import CancelPlan from './components/Plan/ManagePlan/CancelPlan';
import CurrentUsers from './components/Plan/CurrentUsers/CurrentUsers';
import AddUser from './components/Plan/CurrentUsers/AddUser';
import Billing from './components/Plan/Billing/Billing';
import AddEditCard from './components/Plan/Billing/AddEditCard';
import PaymentHistory from './components/Plan/PaymentHistory/PaymentHistory';
import Integrations from './components/Plan/Integrations/Integrations';
import UpdateCompanyDetails from './components/Profile/CompanyDetails/UpdateCompanyDetails';
import UserDetails from './components/Profile/UserDetails/UserDetails';
import SecuritySettings from './components/Profile/SecuritySettings/SecuritySettings';







function App() {
  return (

    <div className="App">
      <img src={'/images/watermark1.png'} className={'wtrMark'} />
      <Router>
      <Header />
        <ScrollToTop>
          <Switch>
            
            <Route exact path={ROUTER_PATH} render={(props) => <Dashboard props={props} />} />
            <Route exact path={ROUTER_PATH + 'notification_list'} render={(props) => <NotificationList props={props} />} />
            <Route exact path={ROUTER_PATH + 'notification_list/notification_view/:id'} render={(props) => <NotificationView props={props} />} />
            <Route exact path={ROUTER_PATH + 'profile'} render={(props) => <CompanyDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'profile/update_company_details'} render={(props) => <UpdateCompanyDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'profile/user_details'} render={(props) => <UserDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'profile/security_settings'} render={(props) => <SecuritySettings props={props} />} />
            <Route exact path={ROUTER_PATH + 'help'} render={(props) => <Help props={props} />} />
            <Route exact path={ROUTER_PATH + 'help/knowledge_base/:id'} render={(props) => <TopicList props={props} />} />
            <Route exact path={ROUTER_PATH + 'help/knowledge_base/topic_details/:id'} render={(props) => <TopicDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'help/enquiries'} render={(props) => <Enquiries props={props} />} />
            <Route exact path={ROUTER_PATH + 'help/enquiries/new_enquiry'} render={(props) => <NewEnquiry props={props} />} />
            <Route exact path={ROUTER_PATH + 'help/enquiries/enquiry_details/:id'} render={(props) => <EnquiryDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan'} render={(props) => <ManagePlan props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/change_plan'} render={(props) => <ChangePlan props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/new_plan'} render={(props) => <NewPlan props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/cancel_plan'} render={(props) => <CancelPlan props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/current_users'} render={(props) => <CurrentUsers props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/addnew_user'} render={(props) => <AddUser props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/billing'} render={(props) => <Billing props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/addnew_card'} render={(props) => <AddEditCard props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/payment_history'} render={(props) => <PaymentHistory props={props} />} />
            <Route exact path={ROUTER_PATH + 'plan/integrations'} render={(props) => <Integrations props={props} />} />
          </Switch>
        </ScrollToTop>
        <Footer />
      </Router>

      <ToastContainer autoClose={2000} />
    </div>

  );
}

export default App;
