import React from 'react';
import 'App.scss';

import { ROUTER_PATH, } from 'services/config';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-select-plus/dist/react-select-plus.css';
import 'react-table-v6/react-table.css';

import Footer from 'components/Footer';
import Header from 'components/Header';
import Dashboard from 'components/Dashboard';
import Plan from 'components/plan/Plan';
import Help from 'components/help/Help';
import Report from 'components/report/Report';
import Invoice from 'components/invoice/Invoice';
import Licensees from 'components/Licensees/Licensees';
import SecuritySettings from 'components/profile/SecuritySettings/SecuritySettings';
import CompanyDetails from 'components/profile/CompanyDetails/CompanyDetails';
import ViewLicensees from './components/Licensees/ViewLicensees';


function App() {
  return (
    <div className="App">
      <img src={'/images/watermark1.png'} className={'wtrMark'} />
      <Router>
        <Header />
        <ScrollToTop>
          <Switch>
            <Route exact path={ROUTER_PATH} render={(props)=> <Dashboard props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'licensees'} render={(props)=> <Licensees props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'licencees/view_licensees'} render={(props) => <ViewLicensees props={props} />} />
            
            <Route exact path={ROUTER_PATH + 'plan'} render={(props)=><Plan props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'help'} render={(props)=><Help props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'report'} render={(props)=><Report props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'invoice'} render={(props)=><Invoice props={props}/>}/>
            <Route exact path={ROUTER_PATH + 'profile/Details'} render={(props) => <CompanyDetails props={props} />} />
            <Route exact path={ROUTER_PATH + 'profile/security_settings'} render={(props) => <SecuritySettings props={props} />} />
           
            </Switch>
        </ScrollToTop>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
