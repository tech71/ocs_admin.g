import * as actionType from '../actions';

const initialState = {

    companyDetails: {
        name: 'Caring For You',
        abn: '01234567890',
        address: '213, care street, Melbourne 3000 VIC',
        contact: '(03) 987654321',
        personName:'Roy Root',
        email: 'wecare@caringforyou@gov.vic.au',
        website: 'www.caringforyou@gov.vic.a',
        admin_users: '12',
        billingAddress: {
            address: '213, care street, Melbourne 3000 VIC',
            contact: '(03) 987654321',
            email: 'wecare@caringforyou@gov.vic.au'
        }
    },

    userDetails:{
        name: 'Juliette Townsend', 
        position:'Manager',
        contact: '(03) 987654321',
        email: 'wecare@caringforyou@gov.vic.au'
    },

    securitySettings:{
        smsNotification:false,
        googleAuthentication:false
    }

}

const reducer = (state = initialState, action) => {
    switch (action.type) {

    }
    return state;
}

export default reducer;