import * as actionType from '../actions';

const initialState = {
    viewInvoice:{
        labels: ['Total', 'Paid', 'Outstanding'],
        datasets: [{
            data: ['50', '40', '10'],
            backgroundColor: ['#a52259 ', '#006166', '#3a2467'],
        }],
    },
    viewPlan:{
        labels: ['Inter', 'Comp', 'Basic'],
        datasets: [{
            data: ['10', '40', '50'],
            backgroundColor: ['#a52259 ', '#006166', '#3a2467'],
        }],
    },
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
		
    }
    return state;
}

export default reducer;