import { combineReducers } from 'redux';
import DashboardReducer from './DashboardReducer';
import LicenseesReducer from './LicenseesReducer';
import ProfileReducer from './ProfileReducer';

export default combineReducers({ 
    DashboardReducer,
    LicenseesReducer,
    ProfileReducer,
});