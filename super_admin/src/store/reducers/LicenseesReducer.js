import * as actionType from '../actions';
const initialState = {
    LicenseesList: [
        { id: "1", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'Admin' },
        { id: "2", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'Admin' },
        { id: "3", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'User' },
        { id: "4", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'Admin' },
        { id: "5", status: 'inactive', date: '17/09/2019', name: 'Amella Jordan', post: 'User' },
        { id: "6", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'User' },
        { id: "7", status: 'inactive', date: '17/09/2019', name: 'Amella Jordan', post: 'User' },
        { id: "8", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'Admin' },
        { id: "9", status: 'inactive', date: '17/09/2019', name: 'Amella Jordan', post: 'Admin' },
        { id: "10", status: 'active', date: '17/09/2019', name: 'Amella Jordan', post: 'User' }],
}

const reducer = (state = initialState, action) => {
    switch (action.type) {

    }
    return state;
}

export default reducer;