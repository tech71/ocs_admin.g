import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Modal from 'components/common/Modal';


class UpdatePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (

            <Modal show={this.props.show}>
                <div>
                    <div className={'text-right'}>
                        <i className={'fa fa-close color1 closeModal mr-3 c_pointer'} onClick={this.props.close}></i>
                    </div>
                    <h4 className={'color1 f-bolder text-center mb-5'}>Update Password</h4>
                    <form id={'updatePassword'}>
                        <div className={'form-group'}>
                            <label className={'lab1'}>Current Password</label>
                            <input
                                type={'password'}
                                className={'form-control1'}
                                name={'current_password'}
                            />
                        </div>
                        <div className={'form-group'}>
                            <label className={'lab1'}>New Password</label>
                            <input
                                type={'password'}
                                className={'form-control1'}
                                name={'new_password'}
                            />
                        </div>
                        <div className={'form-group'}>
                            <label className={'lab1'}>Confirm New Password</label>
                            <input
                                type={'password'}
                                className={'form-control1'}
                                name={'confirm_password'}
                            />
                        </div>
                        <div className={'form-group text-center mt-5'}>
                            <button className={'btn btn1'}>Update Password</button>
                        </div>
                    </form>

                </div>
            </Modal >

        )
    }
}

export default UpdatePassword;