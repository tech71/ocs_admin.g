import React from 'react';
import Modal from 'components/common/Modal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Select from 'react-select-plus';

class SmsAuthenticationModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editObj: {
                country_code: '+61',
                sms_code: ''
            }
        }
    }
    render() {
        const country_codeOpt = [
            { value: '+61', label: "+61 (Australia)" },
            { value: '+91', label: "+91 (India)" },
        ]
        return (
            <React.Fragment>
                <Modal show={this.props.show}>
                    <div className={'text-right'}>
                        <i className={'fa fa-close color1 closeModal mr-3 c_pointer'} onClick={this.props.close}></i>
                    </div>
                    {
                        this.props.securitySettings.smsNotification ?
                            <React.Fragment>

                                <h4 className={'color1 f-bolder text-center mb-3'}>Disable SMS Authentication</h4>
                                <div className={'text-center mb-4'}>
                                    <small>We have sent an SMS code to your phone number 04** **** *12<br />Please enter the 6 digit code to turn off the authentication</small>
                                </div>
                                <form id={'disableSmsAuth'}>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>SMS Code</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            name={'sms_code'}
                                            id={'sms_code'}
                                        />
                                    </div>
                                    <div className={'form-group text-center'}>
                                        <span className={'c_pointer f-bolder color1 f-12 t_underline'}>Resend Code</span>
                                    </div>
                                    <div className={'form-group text-center mt-4'}>
                                        <button className={'btn btn1'}>Disable SMS Authentication</button>
                                    </div>
                                </form>

                            </React.Fragment>
                            :
                            <React.Fragment>


                                <h4 className={'color1 f-bolder text-center mb-5'}>Enable SMS Authentication</h4>
                                <form id={'enableSmsAuth'} onSubmit={this.enableSmsSubmit}>
                                    <div className={'form-group'}>
                                        <div className={'cmn_select grey_bg'}>
                                            <label className={'lab1'}>Country Code</label>
                                            <div className={'line_h_0'}>
                                                <label className="error w-100 mb-0" htmlFor="country_code"></label>
                                            </div>

                                            <Select
                                                simpleValue={true}
                                                name={"country_code"}
                                                id={'country_code'}
                                                value={this.state.editObj.country_code || ''}
                                                options={country_codeOpt}
                                                onChange={(e) => this.selectchange(e, 'country_code')}
                                                clearable={false}
                                            // inputRenderer={() => <input
                                            //     type="text"
                                            //     className="define_input"
                                            //     name={"country_code"}
                                            //     defaultValue={this.state.editObj.country_code || ''}
                                            //     data-rule-required={true}
                                            //     id={'country_code'}
                                            // />}
                                            />


                                        </div>
                                    </div>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>Mobile number</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            name={'mobile'}
                                            id={'mobile'}

                                        />
                                    </div>
                                    <div className={'form-group text-center'}>
                                        <span className={'btn btn1'} onClick={this.sendCode}>Send Code</span>
                                        <div className={'mt-2'}>
                                            <span className={'c_pointer f-bolder color1 f-12 t_underline '}>Resend Code</span>
                                        </div>

                                    </div>
                                    <div className={'form-group'}>
                                        <label className={'lab1'}>Verification Code</label>
                                        <input
                                            type={'text'}
                                            className={'form-control1'}
                                            name={'verification_code'}
                                            id={'verification_code'}
                                        />
                                    </div>
                                    <div className={'form-group text-center mt-5'}>
                                        <button className={'btn btn1'}>Enable SMS Authentication</button>
                                    </div>

                                </form>

                            </React.Fragment>
                    }
                </Modal>
            </React.Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        securitySettings: state.ProfileReducer.securitySettings
    };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(SmsAuthenticationModal);