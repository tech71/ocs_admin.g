import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';

const ProfileSideLinks = (props) => {
    return (
        <ul className={'dets_ul__'}>
            <li>
                <Link to={`${ROUTER_PATH}profile/Details`} className={props.active == 1 ? 'active':''}>
                    Details
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}profile/security_settings`}  className={props.active == 3 ? 'active':''}>
                    Security Settings
                </Link>
            </li>
            <li>
                <Link to={`${ROUTER_PATH}profile/user_details`}  className={props.active == 2 ? 'active':''} >
                    User Management
                </Link>
            </li>
        </ul>
    )
}

export default ProfileSideLinks;