import React from 'react';
import { ROUTER_PATH } from 'services/config';
import { Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import Select from 'react-select-plus';
import ReactTable from 'react-table-v6';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import CounterBox from '../common/CounterBox';
import MainHeading from '../common/MainHeading';

class Licensees extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchObj: {
                filterBy: '1'
            },

            filterByOptions: [
                { value: '1', label: 'All' },
                { value: '2', label: 'Active' },
                { value: '3', label: 'Inactive' }
            ]

        }
    }

    inputHandler = (e) => {
        let searchObj = this.state.searchObj;
        searchObj[e.target.name] = e.target.value;
        this.setState({ searchObj })
    }

    selectchange = (e, key) => {
        let searchObj = this.state.searchObj;
        searchObj[key] = e;
        this.setState({ searchObj })
    }


    render() {
        const columns = [
            {
                Header: 'Status',
                accessor: 'status',
                maxWidth: 70,
                Cell: (props) =>
                    <React.Fragment>
                        <div className={'text-center w-100'}>
                            {props.original.status == 'active' ?
                                <span className={'ED_lab enable'}>Active</span> :
                                <span className={'ED_lab disable'}>Inactive</span>

                            }
                        </div>
                    </React.Fragment>
            },

            { Header: 'Date', accessor: 'date', maxWidth: 120, Cell: (props) => <div>{props.original.date}</div> },
            { Header: 'name', accessor: 'name', Cell: (props) => <div><b>{props.original.name}</b></div> },
            { Header: 'post', accessor: 'post', maxWidth: 120, Cell: (props) => <div>{props.original.post}</div> },
            {
                Header: 'Action',
                maxWidth: 62,
                sortable: false,
                Cell: (props) => <div className="action_c w-100 pr-3">

                    <Link to={`${ROUTER_PATH}ViewLicencess` }>
                        <i className="fa fa-trash-o delete_ic  mr-2"></i>
                    </Link>

                    <Link to={`${ROUTER_PATH}licencees/view_licensees` }>
                    <i className="fa fa-eye view_ic"></i>
                    </Link>
                </div>
            },
        ]

        return (
            <section className="enqSection cmnSec">
                <div className="transBox">
                <MainHeading title={'Licencess Users'} />
                    <Row>
                        <Col xl={4} lg={4} md={4} xs={12}>
                            <CounterBox counterTitle={'1,000'} setTitleCounter={'Total Licensees'}/>
                        </Col>
                        <Col xl={4} lg={4} md={4} xs={12}>
                            <CounterBox counterTitle={'2,028'}  setTitleCounter={'Licensees Users'}/>
                        </Col>
                        <Col xl={4} lg={4} md={4} xs={12}>
                            <CounterBox counterTitle={'1,157'}  setTitleCounter={'Licensees Admins'}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl={8} lg={7} md={6} xs={12} className={'align-self-end'}>
                            <div>
                                <label className={'lab1'}>Search users</label>
                                <div className="input-group1 right">
                                    <input
                                        type="text"
                                        className="form-control1"
                                        aria-label="search_name"
                                        value={this.state.searchObj.user}
                                        onChange={(e) => this.inputHandler(e)}
                                    />
                                    <div className="input-group-append">
                                        <span className="input-group-text color1" id="basic-addon2"><i className={'fa fa-search'}></i></span>
                                    </div>
                                </div>
                            </div>
                        </Col>
                        <Col xl={4} lg={5} md={6} xs={12}>

                            <div className={'line_h_0 pos_relative'}>
                                <label className="error w-100 mb-0 pos_absolute" htmlFor="subscription"></label>
                            </div>
                            <label className={'lab1'}>Filter by</label>

                            <div className={'cmn_select grey_bg '}>
                                <Select
                                    simpleValue={true}
                                    name={"filterBy"}
                                    id={'filterBy'}
                                    value={this.state.searchObj.filterBy || ''}
                                    options={this.state.filterByOptions}
                                    onChange={(e) => this.selectchange(e, 'filterBy')}
                                    clearable={false}
                                    searchable={false}
                                // inputRenderer={(props) =>
                                //     <input
                                //         type="text"
                                //         className="define_input"
                                //         name={"filterBy"}
                                //         defaultValue={this.state.searchObj.filterBy || ''}
                                //         id={'filterBy'}

                                //     />

                                // }
                                />

                            </div>
                        </Col>
                    </Row>

                    <Row>
                        {/*   <Col xs={12} sm={12} md={12} lg={12} xl={12}> */}
                        <div className="table_common__ notifyTable w-100 mt-2">
                            <ReactTable
                                TheadComponent={_ => null}
                                data={this.props.LicenseesList}
                                columns={columns}
                                minRows={1}
                                defaultPageSize={10}
                                loading={this.state.loading}
                                loadingText={'loading...'}
                                previousText={<span className="icon fa fa-angle-left previous"></span>}
                                nextText={<span className="icon fa fa-angle-right next"></span>}
                            />
                        </div>
                        {/* </Col> */}
                    </Row>
                </div>
            </section>
        );
    }
}

const mapStaterToProps = state => {
    return {
          LicenseesList: state.LicenseesReducer.LicenseesList
    };
};
const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);


export default connect(mapStaterToProps, mapDispatchToProps)(Licensees);