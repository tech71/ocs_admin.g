import React from 'react';

const Modal = (props) =>{
    return(
        <div className={"modal__ " + (props.show? 'show':'')}>
            <div className={"modal_dialog "+ (props.className)}>
               {props.children}
            </div>
        </div>
    )
}

Modal.defaultProps = {
    className:'' 
}
export default Modal;