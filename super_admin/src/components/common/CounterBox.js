
import React from 'react';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";
import { TextBlock, MediaBlock, TextRow, RectShape, RoundShape, ContentLoader, Circle, Rect } from 'react-placeholder/lib/placeholders';


const currencyFormatUpdate = (num, currencySymbol) => {
    return currencySymbol + num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
const CounterBox = (props) => {
    const classNameDataAdded = { ',': 'comma_cls', '.': 'dot_cls' };


    let extraParm = {};
    if (props.currencyFormat && props.currencySymbol != '') {
        extraParm['currencyFormat'] = props.currencySymbol;
        classNameDataAdded[props.currencySymbol] = props.currencySymbolClass;
    }
    let sourceData = props.checkLengthDataArr(props.counterTitle, props.minNumber, extraParm);
    let extraClassForSamallBox = sourceData.length > 10 && sourceData.length < 12 ? 'mnSmallNumBox' : sourceData.length >= 12 ? 'mnExtraSmallNumBox' : '';
    return (
        <div className="white_bx__ Counter_box_">
            <h5 className="color2 f-bolder mt-2 text-center">{props.setTitleCounter}</h5>
            <span className={"mnNumBox " + extraClassForSamallBox}>
                {
                    props.checkLengthDataArr(props.counterTitle, props.minNumber, extraParm).map((apli, i) => {
                        return (
                            <span key={i} className={classNameDataAdded.hasOwnProperty(apli) ? classNameDataAdded[apli] : ''}>{apli}</span>
                        )
                    })
                }
            </span>
        </div>
    )

}
CounterBox.defaultProps = {
    placeHoldeShow: false,
    counterTitle: 0,
    minNumber: 3,
    checkLengthData: (number, width) => {
        let numberData = number != undefined && number != null ? number.toString().length : 0;
        let numberDataCalc = width + 1 - numberData;
        numberDataCalc = numberDataCalc < 0 ? 0 : numberDataCalc;
        let data = new Array(numberDataCalc).join('0') + number.toString();
        return data;
    },
    checkLengthDataArr: (number, width, extraParm) => {
        let currencyFormat = extraParm != undefined && extraParm != null && typeof (extraParm) == 'object' && extraParm.hasOwnProperty('currencyFormat') ? true : false;
        let currencySymbol = currencyFormat == true ? extraParm.currencyFormat : '';
        let numberData = number != undefined && number != null ? number.toString().length : 0;
        let numberDataCalc = width + 1 - numberData;
        numberDataCalc = numberDataCalc < 0 ? 0 : numberDataCalc;
        let data = new Array(numberDataCalc).join('0') + number.toString();
        if (currencyFormat) {
            data = currencyFormatUpdate(data, currencySymbol);
        }
        return data.split('');

    },
    classNameAdd: '',
    mode: 'defualt',
    currencyFormat: false,
    currencySymbol: '$',
    currencySymbolClass: '',
    setTitleCounter:'Title Name'


};

export default CounterBox;