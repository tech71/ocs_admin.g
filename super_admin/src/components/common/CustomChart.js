import React from 'react';
import { Bar } from 'react-chartjs-2';
import { ROUTER_PATH } from 'services/config';
import { Link } from 'react-router-dom';


class CustomChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {

       /*  console.log(this.props.navbarShowData, 'linkAtif') */
        return (
            <div className="white_bx__">
                <div>
                    <h5 className="color2 f-bolder mt-2 text-center">{this.props.chartTitleShow}</h5>
                    <div className="cus-chart-super_">
                        <div className="cus-chart-div1">
                            <Bar ref="chart" data={this.props.barData} height={this.props.height} width={this.props.width}
                                legend={false}
                                options={{
                                    maintainAspectRatio: false,
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                fontSize: 10,
                                                beginAtZero: true,
                                                display: false
                                            },
                                            gridLines: {
                                                display: false,
                                                drawBorder: true
                                            }, ...this.props.scaleLabelYAxis
                                        }],
                                        xAxes: [{
                                            ticks: {
                                                fontSize: 7,
                                                display: true
                                            },
                                            gridLines: {
                                                display: false,
                                                drawBorder: true
                                            }, ...this.props.scaleLabelXAxis
                                        }]
                                    }


                                }}
                            />
                        </div>
                        <div className="cus-chart-div2">
                            <ul>
                                {this.props.navbarShowData.map((row, index) => (
                                    <li key={index} className={row.class} onClick={eval(row.clickEvent)}>{row.label} {row.hasOwnProperty('value') && row.value? ['=',this.props.barData.datasets[0]['data'][index]] : []}</li>
                                ))}
                            </ul>
                        </div>
                    </div>
                    {this.props.LinkButton!=null && typeof(this.props.LinkButton) =='object' && this.props.LinkButton.hasOwnProperty('show') && this.props.LinkButton.hasOwnProperty('label') && this.props.LinkButton.hasOwnProperty('link') && this.props.LinkButton.show ?
                        <Link to={this.props.LinkButton.link}>
                            <button className="btn btn1 mt-4 mb-2">{this.props.LinkButton.label}</button>
                        </Link> : <React.Fragment />}
                </div>
            </div>
        )
    };

};


CustomChart.defaultProps = {
    chartTitleShow: 'Title Name',
    barData: {
        labels: ['Total', 'Paid', 'Outstanding'],
        datasets: [{
            data: ['01', '09', '05'],
            backgroundColor: ['#a52259 ', '#006166', '#3a2467'],
        }],
    },
    navbarShowData: [
        { label: 'Total Invoice', class: '', value:true},
        { label: 'Paid', class: '', value: true },
        { label: 'Outstanding', class: '', value: true },
    ],
    scaleLabelYAxis: {
        scaleLabel: {
            display: false,
            labelString: 'y-axis'
        }
    },
    scaleLabelXAxis: {
        scaleLabel: {
            display: false,
            labelString: 'x-axis'
        }
    },
    height: 130,
    width: 130,
    LinkButton: {
        show: true,
        label: 'test',
        link: '/plan'
    }
}
export default CustomChart;