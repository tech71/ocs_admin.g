import React from 'react';
import PropTypes from 'prop-types';

const MainHeading = (props) => {
    return (
        <h1 className={"mainHeader text-left" + props.className}>{props.title}</h1>
    )
}

MainHeading.propTypes = {
    title:PropTypes.string,
    className:PropTypes.string

}

MainHeading.defaultProps = {
    title:'Main Heading',
    className:' '
}

export default MainHeading;