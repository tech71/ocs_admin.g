import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { ROUTER_PATH} from 'services/config';

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <header className="main_hdr">
              
                <div className="logo_and_nav">
                <div className="logo_s">
                   <Link className="nav_logo"to={ROUTER_PATH}>
                        <img src="/images/logo.svg" />
                 </Link>
                </div>

                {/* <div className="but_s">
                     <span className="but_l">
                        <a className="right_anB " href="">
                            Sign Up
                        </a>
                    </span>
                    <span className="but_l login_bt">
                        <a className="right_anB " href="">
                            Log in
                        </a>
                    </span> 
                    <span className="but_l toggle_s">
                        <div className="toggle_ic">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </span>
                </div> */}

                <nav className="nav_s">
                    <i className="fa fa-close close_ic"></i>
                    <ul className="navbar-navie">
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}licensees`}>Licensees</NavLink></strong>
                        </li>
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}plan`}>Plan & Pricing</NavLink></strong>
                        </li>
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}invoice`}>Invoice</NavLink></strong>
                        </li>
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}help`}>Help Desk</NavLink></strong>
                        </li>
                        <li className="nav-item ">
                            <strong><NavLink className="nav-link" to={`${ROUTER_PATH}report`}>Reporting</NavLink></strong>
                        </li>
                        {/* <li className="nav-item navie_bts ">
                            <a className="nav-link " href=""><button>Sign Up</button></a>
                        </li>
                        <li className="nav-item navie_bts ">
                            <a className="nav-link " href="">
                                <button>Log in</button>
                            </a>
                        </li> */}
                    </ul>
                </nav>
                </div>

                <div className="notification_header">
                    sdafupsdifbn
                </div>
              

              
            </header>
        )
    }
}

export default Header;