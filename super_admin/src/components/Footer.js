import React from 'react';

const footer = (props) => {

    const date = new Date();
    const year = date.getFullYear();
    
    return (
        <footer className="footer">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-3 col-sm-3 col-12 ">
                        <div className="company_ct pr-5">
                            <ul className="p-0">
                                <li><img src="/images/logo_w.svg" height="60px" className="contact_logo" /></li>
                                <li className="clr_inherit">(03) 9888 12221</li>
                                <li className="">123 Surrey Hills Road, Surrey Hills VIC 3127</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                        <div className="company_ct">
                            <ul className="p-0">
                                <li><a href="">Why HCM</a></li>
                                <li><a href="">Features</a></li>
                                <li><a href="">Packages</a></li>
                                <li><a href="">Newsletter</a></li>
                                <li><a href="">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                        <div className="company_ct">
                            <ul className="mb-0 p-0">
                                <li><a href="">Privacy Policy</a></li>
                                <li ><a href="">Terms & Conditions</a></li>
                            </ul>

                        </div>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                        <div className="social_icons">
                            <a href="" className="clr_inherit"><i className="fa fa-facebook footer_ic"></i></a>
                            <a href="" className="clr_inherit"><i className="fa fa-linkedin-square footer_ic"></i></a>
                            <a href="" className="clr_inherit"><i className="fa fa-twitter footer_ic"></i></a>
                        </div>
                    </div>
                    <div className="col-12 text-center designed_by my-2">
                        <p className="mb-1">Designed and Developed by <a className="clr_inherit" href="www.yourdevelopementteam.com.au">
                            Your Development Team</a></p>
                        <p>&#169; {year}- ALL Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default footer;