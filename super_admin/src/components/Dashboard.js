import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { ROUTER_PATH } from 'services/config';
import CustomChart from './common/CustomChart';
import { bindActionCreators } from 'C:/Users/User/AppData/Local/Microsoft/TypeScript/3.6/node_modules/redux';
import {connect} from 'react-redux';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        const LinkButtonSetInvoice={ label:'View Invoice', link:`${ROUTER_PATH}InvoiceDetails`, show:true}
        const LinkButtonSetPlan={ label:'View Plan & Pricing', link:`${ROUTER_PATH}PlanDetails`, show:true}
        const navbarShowDataPlan=[
            {label:'Intermediate', value:0, value:true, class: '',} ,
            {label:'Paid', value:0, value:true, class: '',},
            {label:'Outstanding', value:0, value:true, class: '',},
        ]


        return (
            <section className="dashboardSection cmnSec">
                <div className="transBox">
                    <Row>
                        <Col xs={12} sm={12} md={5} lg={4} xl={3}>
                            <div className="white_bx__">
                                <div className="profilePic">
                                    <div className="profInitial">SA</div>
                                    <span className={'addImg'}><i className="fa fa-plus"></i></span>
                                </div>
                                <div className={'mt-4 text-center profNam_det__'}>
                                    <h5 >Super Admin</h5>
                                    <h6>HCM Admin</h6>
                                    <Link to={`${ROUTER_PATH}profile/Details`}>
                                        <button className="btn btn1 mt-4 mb-2">Manage Account</button>
                                    </Link>
                                </div>
                            </div>

                            <div className="white_bx__">
                                <div>
                                    <h3 className="color1 f-bolder mt-2 notifyHead">
                                        Notifications<span >30</span>
                                    </h3>
                                    <div>
                                        <Link to={`${ROUTER_PATH}notification_list`}>
                                            <button className="btn btn1 mt-4 mb-2">Notifications Page</button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </Col>

                        <Col xs={12} sm={12} md={7} lg={8} xl={9}>
                            <Row>
                                <Col xs={12} md={12} lg={12} xl={6}>
                                    <div className="white_bx__">
                                        <div>
                                            <h3 className="color1 f-bolder mt-2">Licencees</h3>
                                            <div className={'mt-4 color2'}>
                                                <div className='f-13'>1,000 licencee companies connected to HCM</div>
                                                <div className='f-13'><span className="color1 f-bold"> 846</span> licencees on monthly plan</div>
                                                <div className='f-13'><span className="color1 f-bold">154</span> licencees on yearly plan</div>
                                            </div>
                                            <Link to={'#'}>
                                                <button className="btn btn1 mt-4 mb-2">Manage licencees</button>
                                            </Link>
                                        </div>
                                    </div>
                                    <CustomChart chartTitleShow={'This Month Plan'} LinkButton={LinkButtonSetPlan} navbarShowData={navbarShowDataPlan}  barData={this.props.viewPlan}/>
                                </Col>
                                <Col xs={12} md={12} lg={12} xl={6}>
                                    <CustomChart chartTitleShow={'This Month Invoices'} LinkButton={LinkButtonSetInvoice} barData={this.props.viewInvoice}/>
                                    <div className="white_bx__">
                                        <div>
                                            <h3 className="color1 f-bolder mt-2">OutStanding Invoices</h3>
                                            <div className={'mt-4 color2'}>
                                                <div className='f-13 custom-bullet due_'>1,000 licencee companies connected to HCM</div>
                                                <div className='f-13 custom-bullet overdue_'> licencees on monthly plan</div>
                                            </div>
                                            <Link to={'#'}>
                                                <button className="btn btn1 mt-4 mb-2">View OutStanding Invoices</button>
                                            </Link>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
        )
    };

};

const mapStateToProps = state =>{
    return{
        viewInvoice: state.DashboardReducer.viewInvoice,
        viewPlan:state.DashboardReducer.viewPlan
    };
}
const mapDispactchTopProps= (dispatch) => bindActionCreators({
    
}, dispatch);

export default connect(mapStateToProps, mapDispactchTopProps) (Dashboard);
