-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 19, 2019 at 10:29 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hcm_prepurchase`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_tbl`
--

CREATE TABLE `contact_tbl` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `company` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `query` text NOT NULL,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_tbl`
--

INSERT INTO `contact_tbl` (`id`, `first_name`, `last_name`, `email`, `contact`, `company`, `position`, `query`, `datetime`) VALUES
(1, 'asfd', 'asdf', 'fdsaf@gm.co', '12345890214', 'dwsf', 'afd', 'adsf dasfadsfasdf', '2019-11-11 08:03:01'),
(2, 'mohammad', 'burhani', 'mdburh@gmail.com', '1234567890', 'burhanis', 'director', 'hello world whats up', '2019-11-11 08:06:07'),
(3, 'adsf', 'd fadsf', 'fdsa@gm.co', '1235468790', 'sadfkjl', 'lskdfgkl', 'm flmasdlfmlkdasmflmldkasf', '2019-11-11 08:07:31'),
(4, 'fdasdf', 'dasfasdf', 'asdf@gn.co', '1235468790', 'fsdaf', 'sdaf', 'asdf dsafdasfasdf', '2019-11-11 08:09:04'),
(5, 'fsdaf', 'asdfdsaf', 'sadf@gm.co', '1234568790', 'dsaf', 'sadfasdfasd', 'f dsafsadfda fadsf', '2019-11-11 08:09:52'),
(6, 'fdasf', 'fdsaf', 'fdas@gm.co', '2135468790', 'asdf', 'fdas f', 'f fasdfadf afadfasdf', '2019-11-11 08:10:20'),
(7, 'asdf', 'f adsfadsf', 'asdf@gm.co', '1235468790', 'fdasf', 'adsf adf', 'asd fds sdaf asdfsdafsdaf', '2019-11-11 08:26:38'),
(8, 'atif', 'khabn', 'atifkhan@gmail.com', '1235468708', 'dfas', 'fsdaf', 'asdf dassdafdasf adsf asdf dasfasdf', '2019-11-11 09:18:49'),
(9, 'atif', 'khan', 'atifkhan@gmail.com', '12354689714', 'atifandsons', 'MD', 'i need ta new subscripition on the same email', '2019-11-11 10:18:10'),
(10, 'asdf', 'dasf', 'asdf@gm.co', '1235468790', 'gfsdg', 'sdfg', 'sdfgsdfg gsdfgfsdg', '2019-11-11 15:05:50'),
(11, 'fdasf', 'dasf', 'fdsa@gm.co', '1235468790', 'fdasf', 'dsaf', 'fdas fadsfdasfsdafdasf', '2019-11-12 13:17:27'),
(12, 'mohammad', 'burhani', 'toofani@gmail.com', '3216549870', 'Aerospace', 'pilot', 'fd dsaf asdf adsfasdf adfd dasfasdf asdfadfasdfsdaf', '2019-11-15 07:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `feature_detail_tbl`
--

CREATE TABLE `feature_detail_tbl` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `hero_img` varchar(500) NOT NULL,
  `heading_1` varchar(200) NOT NULL,
  `content_1` text NOT NULL,
  `image_1` varchar(500) NOT NULL,
  `heading_2` varchar(200) NOT NULL,
  `content_2` text NOT NULL,
  `image_2` varchar(500) NOT NULL,
  `previous_link` varchar(100) NOT NULL,
  `previous_title` varchar(100) NOT NULL,
  `next_link` varchar(100) NOT NULL,
  `next_title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature_detail_tbl`
--

INSERT INTO `feature_detail_tbl` (`id`, `slug`, `title`, `hero_img`, `heading_1`, `content_1`, `image_1`, `heading_2`, `content_2`, `image_2`, `previous_link`, `previous_title`, `next_link`, `next_title`) VALUES
(1, 'crm', 'CRM Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'plan_management_system', 'View Plean Managenent Module', 'imail', 'View Imail Module'),
(3, 'imail', 'Imail Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'crm', 'View CRM Module', 'recruitment_module', 'View Recruitment Module'),
(4, 'recruitment_module', 'Recruitment Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'imail', 'View Imail Module', 'recruitment_app', 'View Recruitment Application'),
(5, 'recruitment_app', 'Recruitment Application', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'recruitment_module', 'View Recruitment Module', 'admin_module', 'View Admin Module'),
(6, 'admin_module', 'Admin Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'recruitment_app', 'View Recruitment Application', 'fms_module', 'View FMS Module'),
(7, 'fms_module', 'FMS Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'admin_module', 'View Admin Module', 'member_module', 'View Member Module'),
(8, 'member_module', 'Member Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'fms_module', 'View FMS Module', 'member_app', 'View Member Application'),
(9, 'member_app', 'Member Application', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'member_module', 'View Member Module', 'organisation_module', 'View Organisation Module'),
(10, 'organisation_module', 'Organisation Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'member_app', 'View Member Application', 'organisation_portal', 'View Organisation Portal'),
(11, 'organisation_portal', 'Organisation Portal', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'organisation_module', 'View Organisation Module', 'participant_module', 'View Participant Module'),
(12, 'participant_module', 'Participant Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'organisation_portal', 'View Organisation Portal', 'client_portal', 'View Client Portal'),
(13, 'client_portal', 'Client Portal', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'participant_module', 'View Participant Module', 'schedules_module', 'View Schedules Module'),
(14, 'schedules_module', 'Schedules Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'client_portal', 'View Client Portal', 'finance_module', 'View Finance Module'),
(15, 'finance_module', 'Finance Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'schedules_module', 'View Schedules Module', 'marketing_module', 'View Marketing Module'),
(16, 'marketing_module', 'Marketing Module', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'finance_module', 'View Finance Module', 'plan_management_system', 'View Plan Management System'),
(17, 'plan_management_system', 'Plan Management System', 'image_icon.jpg', 'Sub Header 1', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'Sub Header 2', 'Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,ut mel dicit oratio habemus.Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam. Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit oratio habemus.', 'image.png', 'marketing_module', 'View Marketing Module', 'crm', 'View CRM Module');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_tbl`
--

CREATE TABLE `newsletter_tbl` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletter_tbl`
--

INSERT INTO `newsletter_tbl` (`id`, `name`, `email`, `datetime`) VALUES
(4, 'mohammad', 'md@gmail.com', '2019-11-11 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `plans_tbl`
--

CREATE TABLE `plans_tbl` (
  `id` int(11) NOT NULL,
  `subscription_plan` varchar(100) NOT NULL,
  `monthly` int(11) NOT NULL,
  `yearly` int(11) NOT NULL,
  `currency` int(11) NOT NULL COMMENT '1 for dollar 2 for cent',
  `status` int(11) NOT NULL COMMENT '1 for active 0 for deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans_tbl`
--

INSERT INTO `plans_tbl` (`id`, `subscription_plan`, `monthly`, `yearly`, `currency`, `status`) VALUES
(1, 'basic', 100, 1000, 1, 1),
(2, 'intermediate', 175, 1500, 1, 1),
(3, 'complete', 285, 2500, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_tbl`
--

CREATE TABLE `users_tbl` (
  `id` int(11) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_position` varchar(100) NOT NULL,
  `user_contact` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(100) NOT NULL,
  `company_postcode` varchar(20) NOT NULL,
  `state` varchar(100) NOT NULL,
  `company_contact` varchar(20) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_website` varchar(100) NOT NULL,
  `team_member` int(11) NOT NULL,
  `subscription_type` varchar(50) NOT NULL,
  `subscription_plan` varchar(50) NOT NULL,
  `license_amt` int(11) NOT NULL,
  `discount_code` varchar(100) NOT NULL,
  `payment_date` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_tbl`
--

INSERT INTO `users_tbl` (`id`, `user_email`, `user_password`, `user_name`, `user_position`, `user_contact`, `company_name`, `company_address`, `company_postcode`, `state`, `company_contact`, `company_email`, `company_website`, `team_member`, `subscription_type`, `subscription_plan`, `license_amt`, `discount_code`, `payment_date`, `datetime`) VALUES
(1, 'atifkhan@gmail.com', 'ZkdpeUd2YnZ1bGNUbXppQ3pPZHk0dz09', 'asdf', 'safd', '1235468790', 'asf', 'asdf', '1234', 'State1', '2315468790', 'asdf@gm.co', 'http://localhost.co', 12, 'monthly', 'basic', 1, '', '', '2019-11-14 06:20:39'),
(2, 'md1@gmail.com', 'ZkdpeUd2YnZ1bGNUbXppQ3pPZHk0dz09', 'asdf', 'fsdaf', '21354687901', 'fdsf', 'asdfasdf', '1234', 'State1', '123412341234', 'md1@gm.co', 'http://localhost.co', 123, 'monthly', 'complete', 96, '', '', '2019-11-14 06:22:25'),
(3, 'atifkhan1@gmail.com', 'ZkdpeUd2YnZ1bGNUbXppQ3pPZHk0dz09', 'asfd', 'sadf', '2135468970', 'fdasf', 'sdaf', '2342', 'State1', '21345697890', 'atifkhan1@gmail.com', 'http://localhost.co', 12, 'yearly', 'basic', 15, '', '', '2019-11-15 08:19:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_detail_tbl`
--
ALTER TABLE `feature_detail_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_tbl`
--
ALTER TABLE `newsletter_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans_tbl`
--
ALTER TABLE `plans_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_tbl`
--
ALTER TABLE `users_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_tbl`
--
ALTER TABLE `contact_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `feature_detail_tbl`
--
ALTER TABLE `feature_detail_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `newsletter_tbl`
--
ALTER TABLE `newsletter_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `plans_tbl`
--
ALTER TABLE `plans_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_tbl`
--
ALTER TABLE `users_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
