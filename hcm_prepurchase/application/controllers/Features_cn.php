<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Features_cn extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('FeaturesModel');
	}


	public function features(){

		$this->load->view('header');
		$data['results'] = $this->FeaturesModel->getAllFeatures();
		$this->load->view('features', $data);
		$this->load->view('footer');
	}


	public function feature_details($slugTitle){
		$this->load->view('header');

		$checkEntry = $this->CommonModel->getRowNum($slugTitle, 'feature_detail_tbl', 'slug');

		if($checkEntry < 1){
			redirect (base_url('not_found'));
		}
		else{
		
			$data['feats'] = $this->FeaturesModel->getFeatureData($slugTitle);
			$this->load->view('feature_details', $data);
			$this->load->view('footer');
		}
	
	}


}
