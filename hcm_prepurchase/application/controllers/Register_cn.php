<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_cn extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('PlansModel');
	}
	
	public function register(){

		

		$getie = $this->input->get();

		$data = array();
		$allData = $this->PlansModel->getPlansData();

	
		if(!empty($getie)){

			$subscription_type = $this->input->get('subscription_type');
			$subscription_plan = $this->input->get('subscription_plan');

			$plans = planType();
			if(in_array($subscription_type, $plans)){

				$getSelectedData = $this->PlansModel->getSelectedData_fn($subscription_plan, 'plans_tbl', $subscription_type);

				// check plan exist
				if(!empty($getSelectedData)){
					$data['selected_data'] = $getSelectedData;		
				}	
				
			}	
			
			
		}

		// plan types select options
		$planType = planType();
		$planTypeOptions = "";

		foreach ($planType as $options => $opt) {

			$capsOpt = ucfirst($opt);
			if(array_key_exists('selected_data', $data)){
				$planTypeOptions .= "<option value='".$opt."' ".($opt == $subscription_type? 'selected':'') .">Subscription Type:$capsOpt</option>";
			}
			else{
				$planTypeOptions .= "<option value='".$opt."'>Subscription Type:$capsOpt</option>";
			}	
		}

		$data['planTypeOptions'] = $planTypeOptions;


		// plan amount and name select options
		$planOptions = "";

		if(array_key_exists('selected_data', $data)){

			$subType = $subscription_type == 'monthly'? 'Month' :'Year';		
			foreach ($allData as $options => $option) {
				$planOptions .="<option value='".$option['subscription_plan']."' ".($option['subscription_plan'] == $subscription_plan? 'selected':'') .">".ucfirst($option['subscription_plan']) ."($$option[$subscription_type] per ".$subType.")</option>";
			}				
		}

		$data['planOptions'] = $planOptions;
		
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

		$this->load->view('header');
		$this->load->view('register', $data);
		$this->load->view('footer');


	}


	public function CreateOptions($result, $type){

		$subType = $type == 'monthly' ? 'Month' : 'Year';
		$res = "<option disabled selected value='select'>Select Plan</option>";

		foreach ($result as $opt) {
		$res .= "<option value='".$opt['subscription_plan']."'>". ucfirst($opt['subscription_plan']) ."($$opt[$type] per $subType)</option>";
		}

		return $res;

	}


	
	public function getPlans_Option(){

		$type = $this->input->post('type');
		
		$plans = planType();
		if(in_array($type, $plans)){
		

			$result = $this->PlansModel->getPlansOptions($type);
		
			if(!empty($result)){

				$creteOpt = $this->CreateOptions($result, $type);
				echo json_encode(array('success' => true, 'data' => $creteOpt));
	            exit();

			}
			else{
				echo json_encode(array('success' => false, 'message' => 'no data'));
	            exit();
			}
		}
		else{
			echo json_encode(array('success' => false, 'message' => 'wrong credentials'));
	        exit();
		}

	}


	public function planDets(){

		$plan = $this->input->post('plan');
		$type = $this->input->post('type');
		
		$plansType = planType();
		if(in_array($type, $plansType)){


			$result = $this->PlansModel->getPlanDets($plan, $type);

			if(!empty($result)){
				echo json_encode(array('success' => true, 'data' => $result));
	            exit();
			}
			else{
				echo json_encode(array('success' => false, 'message' => 'no data'));
	            exit();
			}
		}else{

			echo json_encode(array('success' => false, 'message' => 'wrong params'));
	        exit();
		}

	}

}