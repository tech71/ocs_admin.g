<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_cn extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();


	}


	public function index(){

		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');

	}

	public function why_hcm(){

		$this->load->view('header');
		$this->load->view('why_hcm');
		$this->load->view('footer');
	}

	public function package(){

		$this->load->view('header');
		$this->load->view('package');
		$this->load->view('footer');
	}

	public function contact(){

		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}

	public function login(){

		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function forgot_password(){

		$this->load->view('header');
		$this->load->view('forgot_password');
		$this->load->view('footer');
	}

	public function reset_password($key){


		$decodedtoken = base64_decode(urldecode($key));
		$array = array('token' => $decodedtoken, 'token_status' => '1');
		$checkExist = $this->CommonModel->checkEntryExist($array, 'users_tbl');

		$res['key'] = $key;

		if ($checkExist > 0) 
		{
			$this->load->view('header');
			$this->load->view('reset_password', $res);
			$this->load->view('footer');
		}
		else{
			echo 'wrong url or link expired';
		}

		
	}
	
}


