<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_cn extends CI_Controller {

	public function registerUser(){

	
		$this->form_validation->set_rules('admin_email', 'Email', 'trim|required|max_length[100]|valid_email|callback_field_check['. json_encode(array('key' => 'user_email')) .']');	
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', 'confrim password', 'trim|required|min_length[6]|matches[password]');
		$this->form_validation->set_rules('admin_name', 'admin name', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('admin_position', 'admin position', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('admin_contact', 'admin contact', 'trim|required|min_length[10]|callback_field_check['. json_encode(array('key' => 'user_contact')) .']');
		$this->form_validation->set_rules('company_name', 'company name', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('company_address', 'company address', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('company_postcode', 'company postcode', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('state', 'state', 'trim|required');
		$this->form_validation->set_rules('company_contact', 'company contact', 'trim|required|min_length[10]|numeric');
		$this->form_validation->set_rules('company_email', 'Email', 'trim|required|max_length[100]|valid_email|callback_field_check['. json_encode(array('key' => 'company_email')) .']');
		$this->form_validation->set_rules('company_website', 'company website', 'trim|required|callback_valid_url');
		$this->form_validation->set_rules('team_member', 'team member', 'trim|required|numeric');
		$this->form_validation->set_rules('subscription_type', 'subscription type', 'trim|required');
		$this->form_validation->set_rules('subscription_plan', 'subscription plan', 'trim|required');
		$this->form_validation->set_rules('license_amt', 'license amt', 'trim|required|numeric');
		$this->form_validation->set_rules('discount_code', 'discount code', 'trim');


		if ($this->form_validation->run() == FALSE) {
			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 
		else{

			$request_data =(object)$this->input->post();

			$passwordEnc = encrypt_dcrypt(strip_tags($request_data->password), 'encrypt');
			// echo $passwordenc;
		

			$data = array(
				'user_email' =>  strip_tags($request_data->admin_email),
				'user_password' => $passwordEnc,
				'user_name' => strip_tags($request_data->admin_name),
				'user_position' => strip_tags($request_data->admin_position),
				'user_contact' => strip_tags($request_data->admin_contact),
				'company_name' => strip_tags($request_data->company_name),
				'company_address' => strip_tags($request_data->company_address),
				'company_postcode' => strip_tags($request_data->company_postcode),
				'state' => strip_tags($request_data->state),
				'company_contact' => strip_tags($request_data->company_contact),
				'company_email' => strip_tags($request_data->company_email),
				'company_website' => strip_tags($request_data->company_website),
				'team_member' => strip_tags($request_data->team_member),
				'subscription_type' => strip_tags($request_data->subscription_type),
				'subscription_plan' => strip_tags($request_data->subscription_plan),
				'license_amt' => strip_tags($request_data->license_amt),
				'discount_code' => strip_tags($request_data->discount_code),
				'datetime' => DATE_TIME
			);

	        $insert_entry = $this->CommonModel->insertData($data, 'users_tbl');

	        if (!empty($insert_entry)) {
	            echo json_encode(array('success' => true, 'message' => 'User registerd successfully'));
	            exit();
	        } 
	        else {
	            echo json_encode(array('success' => false, 'message' => 'User registeration failed! Please try again'));
	            exit();
	        }



		}

	}


	public function field_check($str, $columnArr)
	{

		$arr=json_decode($columnArr);
		$columnName=$arr->key;


		$msg = '';

		if($columnName == 'user_email'){
			$msg = 'The given admin email already exists.';
		}
		if($columnName == 'company_email'){
			$msg = 'The given company email already exists.';
		}
		if($columnName == 'user_contact'){
			$msg = 'The given user contact No. already exists.';
		}
		
		$check = array();

		$checkExist = $this->CommonModel->getRowNum($str, 'users_tbl', $columnName);
		if ($checkExist > 0) 
		{
			$check = array('status' => false,);
			$this->form_validation->set_message('field_check', $msg);
			return FALSE;
		} 
		else {
			return TRUE;
		}
	}

	public function valid_url($url)
	{
	    $pattern = '/^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/';
	    if (!preg_match($pattern, $url))
	    {
	    	$this->form_validation->set_message('valid_url', 'Please enter valid url');
	        return FALSE;
	    }

	    return TRUE;
	}


	public function checkUser(){

		$email =$this->input->post('email');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');


		if ($this->form_validation->run() == FALSE) {

			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 

		else{

			$checkEmail = $this->CommonModel->getRowNum(strip_tags($email), 'users_tbl', 'user_email');

			if ($checkEmail > 0) {

				$loginBox = $this->load->view('loginBox', '', true);
				// print_r($loginBox);
				// die();

				echo json_encode(array('success' => true, 'message' => 'email verified', 'data' => $loginBox));
				exit();
			}else{
				echo json_encode(array('success' => false, 'message' => 'The email doesnt exist in our system. You can signup by clicking the button below'));
				exit();
			}
		}

	}


	public function loginUser(){

		$email =$this->input->post('email');
		$password =$this->input->post('password');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {

			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 

		else{

			$Encrypt_password = encrypt_dcrypt(strip_tags($password), 'encrypt');


			$array = array('user_email' => strip_tags($email), 'user_password' => strip_tags($Encrypt_password));
			$checkExist = $this->CommonModel->checkEntryExist($array, 'users_tbl');

			if ($checkExist > 0) 
			{
				$response = array('status' => true, 'message' => 'user matches ');
				echo json_encode($response);
				exit();

			}
			else{

				$response = array('status' => false, 'message' => 'Invalid User email or password');
				echo json_encode($response);
				exit();
			}

		}

	}


	public function forgotPasswordUser(){


		$email =$this->input->post('email');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');

		if ($this->form_validation->run() == FALSE) {

			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 

		else{
			$checkEmail = $this->CommonModel->getRowNum(strip_tags($email), 'users_tbl', 'user_email');

			if ($checkEmail > 0) {
				$this->load->helper('Mail_helper');

				$token = encrypt_dcrypt(microtime().$email, 'encrypt');

				$url = base_url('reset_password/'.urlencode(base64_encode($token)));
				$msg = '<h2>HCM Prepurchase reset Password</h2>';
				$msg .= '<div>Click on this link <br/><a href="'.$url.'">Reset </a></div>';

				$data = array(
						'token' => $token,
						'token_status' => '1'
					);

				$this->load->model('UsersModel');
				$ResetTokenInsert = $this->UsersModel->ResetTokenInsert($email, $data);

				

				if($ResetTokenInsert > 0){

					$mail = sendMail($email, 'Reset Password', $msg );

					if($mail){
						echo json_encode(array('success' => true, 'message' => 'reset link successfully sent on your registered email address'));
						exit();
					}
					else{
						echo json_encode(array('success' => false, 'message' => 'Please try again'));
						exit();
					}				
		
				}
				else{
					echo json_encode(array('success' => false, 'message' => 'Please try again'));
					exit();
				}
				
			}
			else{
				echo json_encode(array('success' => false, 'message' => 'The email is not registered with any user. '));
				exit();
			}
		}
		
	}

	public function passwordReset(){

		$password =$this->input->post('password');
		$confirm_password =$this->input->post('confirm_password');
		$token =$this->input->post('token');

		$decodedtoken = base64_decode(urldecode($token));
		$Encrypt_password = encrypt_dcrypt(strip_tags($password), 'encrypt');

		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]|min_length[6]');
		$this->form_validation->set_rules('confirm_password', 'confirm password', 'trim|required|matches[password]');

		if ($this->form_validation->run() == FALSE) {

			$errors = $this->form_validation->error_array();
			$response = array('success' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 

		else{

			$array = array('token' => $decodedtoken, 'token_status' => '1');
			

			$checkExist = $this->CommonModel->checkEntryExist($array, 'users_tbl');

			if ($checkExist > 0) 
			{
				$data = array(
					'user_password' => $Encrypt_password,
					'token_status' => '0',
					'token'=>''
				);
				$UpdatePassword = $this->CommonModel->UpdataData($array, $data, 'users_tbl');
				if($UpdatePassword){
					$response = array('success' => true, 'message' => 'Password Updated successfully');
					echo json_encode($response);
					exit();
				}
				else{
					$response = array('success' => false, 'message' => 'Failed!! Please try again');
					echo json_encode($response);
					exit();
				}

			}
			else{
				$response = array('success' => false, 'message' => 'Link expired please go to forgot password again');
				echo json_encode($response);
				exit();
			}
		}



	}

	// public function checkEntryExist(){

	// 		$email = 'md@gmail.com';
	// 		$Encrypt_password = encrypt_dcrypt(strip_tags('123456'), 'encrypt');
	// 		$array = array('user_email' => strip_tags($email), 'user_password' => strip_tags($Encrypt_password));

	// 		$checkExist = $this->CommonModel->checkEntryExist($array, 'users_tbl');
	// 		echo $checkExist;

	// }

	// public function checkPass(){
	// 	$passwordenc = encrypt_dcrypt('ZkdpeUd2YnZ1bGNUbXppQ3pPZHk0dz09', 'dcrypt');
	// 	echo $passwordenc;

	// 	$passworenc = encrypt_dcrypt(date("d/m/Y"), 'encrypt');
	// 	echo $passworenc;
	// 	die();


	// }



}