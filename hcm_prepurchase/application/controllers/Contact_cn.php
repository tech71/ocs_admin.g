<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_cn extends CI_Controller {

	public function contactUs(){


		// $first_name = $this->input->post('first_name');

		$this->form_validation->set_rules('firstname', 'firstname', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('lastname', 'lastname', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email');
		$this->form_validation->set_rules('contact', 'contact', 'required|min_length[10]|numeric');
		$this->form_validation->set_rules('company', 'company', 'required');
		$this->form_validation->set_rules('position', 'position', 'required');
		$this->form_validation->set_rules('query', 'query', 'required|min_length[10]');

		$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
		$userIp=$this->input->ip_address();
		$secret = $this->config->item('google_secret');
		$url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;

		$ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      
		$status= json_decode($output, true);

		if ($this->form_validation->run() == FALSE) {
			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 

		
		if (!$status['success']) {
			$response = array('success' => false, 'message' => 'Please select google recaptcha');
			echo json_encode($response);
			exit();
		}

		else {

			$request_data =(object)$this->input->post();

			$data = array(
				'first_name' =>  strip_tags($request_data->firstname),
				'last_name' => strip_tags($request_data->lastname),
				'email' => strip_tags($request_data->email),
				'contact' => strip_tags($request_data->contact),
				'company' => strip_tags($request_data->company),
				'position' => strip_tags($request_data->position),
				'query' => strip_tags($request_data->query),
				'datetime' => DATE_TIME
			);

			$this->load->model('ContactModel');
	        $insert_entry = $this->CommonModel->insertData($data, 'contact_tbl');

	        if (!empty($insert_entry)) {
	            echo json_encode(array('success' => true, 'message' => 'msg sent successfully'));
	            exit();
	        } 
	        else {
	            echo json_encode(array('success' => false, 'message' => 'msg sending failed! Please try again'));
	            exit();
	        }

		}
	}


	public function newsletterSubscribe(){

		$this->form_validation->set_rules('name', 'name', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email|callback_email_check');

		if ($this->form_validation->run() == FALSE) {
			$errors = $this->form_validation->error_array();
			$response = array('status' => false, 'message' => implode(', ', $errors));
			echo json_encode($response);
			exit();
		} 
		else{

			$request_data =(object)$this->input->post();

			$data = array(
				'name' =>  strip_tags($request_data->name),
				'email' => strip_tags($request_data->email),
				'datetime' => DATE_TIME
			);

	        $insert_entry = $this->CommonModel->insertData($data, 'newsletter_tbl');

	        if (!empty($insert_entry)) {
	            echo json_encode(array('success' => true, 'message' => 'Subscribed successfully'));
	            exit();
	        } 
	        else {
	            echo json_encode(array('success' => false, 'message' => 'subscription failed! Please try again'));
	            exit();
	        }


		}


	}


	public function email_check($str)
	{
		$check = array();

		$checkEmail = $this->CommonModel->getRowNum($str, 'newsletter_tbl', 'email');
		if ($checkEmail > 0) 
		{
			$check = array('status' => false,);
			$this->form_validation->set_message('email_check', 'The given email already exists.');
			return FALSE;
		} else {
			return TRUE;
		}
	}	



}