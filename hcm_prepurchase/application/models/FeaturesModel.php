
<?php

class FeaturesModel extends CI_Model
{
	public function getAllFeatures(){
		$this->db->from('feature_detail_tbl');
		$this->db->order_by("f_order", "asc");
		$query = $this->db->get(); 
		return $query->result_array();
	}

	public function getFeatureData($slug){

		$this->db->select("sub_fdt.slug",false);
		$this->db->from('feature_detail_tbl as sub_fdt');
		$this->db->where("sub_fdt.f_order=featTable.previous_link",NULL,FALSE);
		$preQuerySlug= $this->db->get_compiled_select();

		$this->db->select("sub_fdt.title",false);
		$this->db->from('feature_detail_tbl as sub_fdt');
		$this->db->where("sub_fdt.f_order=featTable.previous_link",NULL,FALSE);
		$preQueryTitle= $this->db->get_compiled_select();


		$this->db->select("sub_fdt.slug",false);
		$this->db->from('feature_detail_tbl as sub_fdt');
		$this->db->where("sub_fdt.f_order=featTable.next_link",NULL,FALSE);
		$nextQueryslug=$this->db->get_compiled_select();

		$this->db->select("sub_fdt.title",false);
		$this->db->from('feature_detail_tbl as sub_fdt');
		$this->db->where("sub_fdt.f_order=featTable.next_link",NULL,FALSE);
		$nextQueryTitle=$this->db->get_compiled_select();

		$this->db->select(
			array(
				"(".$preQuerySlug.") as  prev_link_s",
				"(".$preQueryTitle.") as  prev_title",
				"(".$nextQueryslug.") as  next_link_s",
				"(".$nextQueryTitle.") as  next_title",'featTable.*'),
			false);

		$this->db->where('featTable.slug', $slug);
        $query = $this->db->get('feature_detail_tbl as featTable');
        return $query->row();

	}
}
