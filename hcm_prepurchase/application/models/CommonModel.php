<?php 


class CommonModel extends CI_Model
{
	
	public function getRowNum($param, $tableName, $columnName)
	{ 

		$this->db->select('*'); 
		$this->db->where($columnName,$param);
		$this->db->from($tableName); 
		$query = $this->db->get();
		return $query->num_rows();

	}

	public function checkEntryExist($array, $tableName){
		$this->db->select('*'); 
		$this->db->where($array);
		$this->db->from($tableName);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function insertData($data, $tableName){

		$this->db->insert($tableName, $data);
        return $this->db->insert_id();
	}

	public function UpdataData($arr, $data, $tblName) {
      
        $this->db->where($arr);
        $this->db->update($tblName, $data);
        return $this->db->affected_rows();
    }




	public function getRow($param, $tableName, $columnName)
	{ 

		// $this->db->select('subscription_plan, monthly, yearly');
		$this->db->where($columnName, $param);
        $query = $this->db->get( $tableName);
        return $query->result();
	}


	public function getSelectedData($selected, $tableName, $orderBy = false, $orderType = false){

		$this->db->select($selected);

		if($orderBy != false && $orderType != false){

			$type = 'asc';
			if($orderType == 2){
				$type = 'desc';
			}
			$this->db->order_by($orderBy, $type);
		}
		
		$query = $this->db->get($tableName);
		return $query->result_array();
	}


}


?>