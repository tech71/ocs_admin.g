
<?php

class PlansModel extends CI_Model
{

	public function getPlansData(){

		$this->db->where('status', '1');
        $query = $this->db->get('plans_tbl');
        return $query->result_array();      
	}


	public function getSelectedData_fn($param, $tableName, $getType)
	{ 

		$this->db->select("subscription_plan, $getType as amount");
		$arr = array('subscription_plan' => $param, 'status' => '1');
	
		$this->db->where($arr);
        $query = $this->db->get($tableName);       
        $res = $query->row_array();

        if(!empty($res)){
        	$tot = $res['amount'];
        	$res = TotalAmountCalc($tot);
        }

        return $res;
	}


	public function getPlansOptions($type){

		$this->db->select("subscription_plan, $type");
		$this->db->where('status', '1');
        $query = $this->db->get('plans_tbl');
        return $query->result_array();      
	}


	public function getPlanDets($plan, $type){
		
		$this->db->select("subscription_plan, $type as amount");
		$arr = array('subscription_plan' => $plan, 'status' => '1');
		$this->db->where($arr);
		$query = $this->db->get('plans_tbl');
		$res = $query->row_array();
		$totalAmt = '';

		if(!empty($res)){
			$totalAmt = TotalAmountCalc($res['amount']);
			$totalAmt['subscription_plan'] = $res['subscription_plan'];
		}
		
        return $totalAmt;  
	}
}