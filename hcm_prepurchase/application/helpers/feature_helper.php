<?php 


function featureLinks()
{
    $CI = get_instance();
    $CI->load->model('CommonModel');
    $arr = array('slug', 'title');
 
    $data = $CI->CommonModel->getSelectedData($arr, 'feature_detail_tbl', 'f_order', 1);
    return $data;
}
