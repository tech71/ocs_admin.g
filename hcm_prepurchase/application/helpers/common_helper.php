<?php 

function encrypt_dcrypt($string, $keyval){



	$Token = '';

	 $key = hash( 'sha256', SECRETPASSKEY );
     $iv = substr( hash( 'sha256', SECRETPASSIV ), 0, 16 );

	if($keyval == 'encrypt'){

		$Token = base64_encode( 
			openssl_encrypt( 
				$string, 
				ENCRYPTMETHOD, 
				$key, 
				0, 
				$iv
			) 
		);
	}
	if($keyval == 'dcrypt'){

		$Token = openssl_decrypt( 
			base64_decode( $string ), 
			ENCRYPTMETHOD,  
			$key, 
			0, 
			$iv
		);
	}

    	
	return $Token;
}

function planType(){
	return array("monthly", "yearly");
}


function TotalAmountCalc($PlanAmt){

	$subtotal = $PlanAmt;
	$gst = (int)$PlanAmt * 10/100;
	$total = (int)$PlanAmt + $gst;

	$res = array();
	$res['subtotal'] = '$'.$subtotal;
    $res['gst'] = '$'. $gst;
    $res['total'] = '$'.$total;

    return $res;
}