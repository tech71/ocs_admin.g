

	

	<section class="section_one cmnSec">

        <div class="bg_triangle"></div>
        <div class="container text-center mt-5">
            <h1 class="page_head cmnHead mb-5"><strong>Packages</strong></h1>     
        </div>

         <div class="container whiteBoxCmn">


         	<div class="row justify-content-center">
         		

         		<div class="col-md-10 col-12"> 


         			<div>
	         			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			         	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			         	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			         	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			         	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			         	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		         	</div>

		         	<div class="packgeBoxes mt-3">

		         		<div class="package__ d-flex flex-wrap">
		         			<div class="p_fPart align-self-start w-100">
			         			<h3 class="p_head__">Basic</h3>
			         			<div class="descP__ mt-4">
			         				A system to onboard members and participants. 
			         				Contains imail for easier communication.
			         			</div>
		         			</div>
		         			<div class="p_sPArt align-self-end w-100">
		         				<div class="pt-5 ">
		         					<div class="planRate basicPlan">$100</div>
		         				</div>
		         				
		         			</div>
		         		</div>
		         		<div class="package__  d-flex flex-wrap">
		         			<div class="p_fPart align-self-start">
			         			<h3 class="p_head__">Intermediate</h3>
			         			<div class="descP__ mt-4">
			         				A basic package with system designed to store member, organizations and participant details, shifts
			         				and feedback. Contains additional applications and software.
			         			</div>
			         		</div>
		         			<div class="p_sPArt align-self-end w-100">
		         				<div class="pt-5">
		         					<div class="planRate IntermediatePlan">$175</div>
		         				</div>
		         			</div>

		         		</div>
		         		<div class="package__ d-flex flex-wrap">
		         			<div class="p_fPart align-self-start">
			         			<h3 class="p_head__">Complete</h3>
			         			<div class="descP__ mt-4">
			         				Everything from the intermediate package, plus finance marketing management systems
			         			</div>
			         		</div>
			         		<div class="p_sPArt align-self-end w-100">
		         				<div class="pt-5 ">
		         					<div class="planRate completePlan">$285</div>
		         				</div>
		         			</div>
		         		</div>
	                    
            		</div>



		        </div>
		    </div>

         </div>
         	


    </section>


