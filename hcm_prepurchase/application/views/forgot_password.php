


<section class="section_one loginSection">
	<div class="bg_triangle"></div>

	<div class="container text-center mt-5">
        <h1 class="heading_one mb-5"><strong>Forgot Password</strong></h1>


        <div class="row justify-content-center pb-5">
        	<div class="col-lg-8 col-md-10 col-12">
        		<div class="loginBox">

                    <form id="forgotPassword">
                        <div class="row mt-4 mb-4">
                            <div class=" col-12 mb-2">
                                <div class="form-group">
                                    <label>Enter email</label>
                                    <input type="text" class="form-control customInp1" id="email" name="email" 
                                    autocomplete="new-password">
                                    
                                </div>      
                            </div>
                            <div class="col-md-12 col-12 text-center mt-5">
                                <div class="form-group">
                                    <button class="loginSubmit btn "><strong>Submit</strong></button>
                                   
                                    <div class="text-center mt-2 mb-2">
                                        <div class="serverMsg" ></div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>
                    </form>

        		</div>	
        	</div>
        </div>

    </div>

</section>

