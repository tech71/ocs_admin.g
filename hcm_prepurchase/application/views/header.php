<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/output.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/styles/style2.css');?>">

    <link rel="icon" href="<?php echo base_url('assets/images/favi.png');?>" type="image/x-icon"/>
    <title>Healthcare Management</title>

     <script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
</head>
<?php
    $uri = $this->uri->segment(1);
    $sub_uri = $this->uri->segment(2);
    if ($uri == '') {
        $uri = 'home';
    }
    // echo $uri;
    $features = featureLinks();



?>


<body>

   <!--  <div class="main_loader">
        
        <div class="logo_t">
            <div class="trs tr1"></div>
            <div class="trs tr2"></div>
            <div class="trs tr3"></div>
        </div>

    </div> -->
    <img src="<?php echo base_url('assets/images/watermark1.png');?>" class="watermark1" />

    <header class="main_hdr">
        <div class="logo_s">
             <a class="nav_logo" href="<?php echo base_url('');?>"><img src="<?php echo base_url('assets/images/logo.svg');?>" /></a>
        </div>

        <div class="but_s">
            <span class="but_l">
                <a class="right_anB <?php if ($uri == 'register') {echo 'active';} ?>" href="<?php echo base_url('register');?>">
                    Sign Up
                </a>
            </span>
            <span class="but_l">
                <a class="right_anB <?php if ($uri == 'login') {echo 'active';} ?>" href="<?php echo base_url('login');?>">
                    Log in
                </a>
            </span>
            <span class="but_l">
                <div class="toggle_ic">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </span>

        </div>

        <nav class="nav_s">
            <i class="fa fa-close close_ic"></i>
            <ul class="navbar-navie">
                <li class="nav-item  <?php if ($uri == 'why_hcm') {echo 'active';} ?>">
                    <strong><a class="nav-link" href="<?php echo base_url('why_hcm');?>">Why</a></strong>
                </li>
                <li class="nav-item feat_drop <?php if ($uri == 'features') {echo 'active';} ?>">
                    <!-- data-toggle="dropdown" -->
                    <div class="dropdown" >
                      <a class="nav-link " href="<?php echo base_url('features');?>" >
                       <strong>Features <i class="fa fa-caret-down caret_ic"></i></strong>
                      </a>
                      <div class="dropdown-menu">
                        <div class="drp_nc">

                            
                            <div class="row">
                                <?php 
                                    $count = 1;
                                    foreach ($features as $feats => $feat) {
                                        if ($count%4 == 1){  
                                            echo "<div class='col-lg-3 col-12'>";
                                        }
                                ?>

                                        <a class="dropdown-item <?php if ($sub_uri == $feat['slug']) {echo 'active';} ?>" href="<?php echo base_url('/features/'.$feat['slug']) ?>">
                                            <?php echo $feat['title'] ?>
                                                    
                                        </a>
                                        <!-- echo $feat['title']; -->
                                <?php
                                        if ($count%4 == 0){
                                            echo "</div>";
                                        }
                                        $count++;
                                    }


                                    if ($count%4 != 1) echo "</div>";
                                ?>
                               
                            </div>
                            <!-- <div>
                                <a class="dropdown-item" href="<?php echo base_url('/features/admin_module') ?>">Admin</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/member_module') ?>">Members</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/participant_module') ?>">Participants</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/schedules_module') ?>">Schedules</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/marketing_module') ?>">Marketing</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/finance_module') ?>">Finance</a>
                            </div>
                            <div>
                                <a class="dropdown-item" href="<?php echo base_url('/features/fms_module') ?>">FMS</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/recruitment_module') ?>">Recruitment</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/crm') ?>">CRM</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/imail') ?>">Imail</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/organisation_module') ?>">Organisations</a>
                                <a class="dropdown-item" href="<?php echo base_url('/features/plan_management_system') ?>">Financial Planning</a>
                            </div> -->
                        </div>
                      </div>
                    </div>
                </li>
                <li class="nav-item <?php if ($uri == 'package') {echo 'active';} ?>">
                    <strong><a class="nav-link" href="<?php echo base_url('package');?>">Packages</a></strong>
                </li>
                <li class="nav-item <?php if ($uri == 'contact') {echo 'active';} ?>">
                    <strong><a class="nav-link" href="<?php echo base_url('contact');?>">Contact</a></strong>
                </li>
                <li class="nav-item navie_bts <?php if ($uri == 'register') {echo 'active';} ?>">
                    <a class="nav-link " href="<?php echo base_url('register');?>"><button>Sign Up</button></a>
                </li>
                <li class="nav-item navie_bts <?php if ($uri == 'login') {echo 'active';} ?>">
                    <a class="nav-link <?php if ($uri == 'login') {echo 'active';} ?>" href="<?php echo base_url('login');?>"><button>Log in</button></a>
                </li>
            </ul>
        </nav>
    </header>
    
