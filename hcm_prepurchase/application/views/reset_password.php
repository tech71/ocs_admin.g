



<section class="section_one loginSection">
	<div class="bg_triangle"></div>

	<div class="container text-center mt-5">
        <h1 class="heading_one mb-5"><strong>Reset Password</strong></h1>


        <div class="row justify-content-center pb-5">
        	<div class="col-lg-8 col-md-10 col-12">
        		<div class="loginBox">

                    <form id="resetPassword">
                        <div class="row mt-4 mb-4">
                            <div class=" col-12 mb-2">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control customInp1" id="password" name="password" 
                                    autocomplete="new-password">
                                    
                                </div>   
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control customInp1" id="confirm_password" name="confirm_password" 
                                    autocomplete="new-password">
                                    
                                </div>      
                            </div>
                            <div class="col-md-12 col-12 text-center mt-5">
                                <div class="form-group">
                                    <button class="loginSubmit btn rese_submit"><strong>Submit</strong></button>
                                    <input name="token" type="hidden" value="<?php echo $key; ?>" name="">
                                    <div class="text-center mt-2 mb-2">
                                        <div class="serverMsg" ></div>
                                    </div>
                                </div>
                                
                            </div>

                        </div>
                    </form>

        		</div>	
        	</div>
        </div>

    </div>

</section>

