

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<section class="section_one contactSections">
	<div class="bg_triangle"></div>

	<div class="container text-center mt-5">
        <h1 class="heading_one mb-5"><strong>Contact Us</strong></h1>
    </div>


    <!-- secretkey = "6LcLesEUAAAAAFI7wKPMZSa9J4q1e3jcIPYMnvIy" -->

    <div class="container whiteBox_cmn p-5 mb-5">
        	<div class="row align-items-center justify-content-center">

        		<div class="col-md-11 col-12 pb-3">
        			
        			<div class="contBox">


        				<form id="contactForm">
        					
        					<div class="row mt-4 mb-4">

        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>First Name</label>
        								<input type="text" class="form-control customInp1" name="firstname">
        							</div>		
        						</div>
        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>Surname</label>
        								<input type="text" class="form-control customInp1" name="lastname">
        							</div>
        							
        						</div>
        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>Email</label>
        								<input type="text" class="form-control customInp1" name="email">
        							</div>
        							
        						</div>
        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>Contact Number</label>
        								<input type="text" class="form-control customInp1" name="contact" >
        							</div>
        							
        						</div>
        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>Company Name</label>
        								<input type="text" class="form-control customInp1" name="company" >
        							</div>
        							
        						</div>
        						<div class="col-md-6 col-12 mb-2">
        							<div class="form-group">
                                        <label>Position</label>
        								<input type="text" class="form-control customInp1" name="position" >
        							</div>	
        						</div>

        						<div class="col-md-12 col-12 mb-2">
        							<div class="form-group">
                                        <label>Query</label>
        								<textarea class="form-control customInp1" name="query" ></textarea>
        							</div>	
        						</div>

        						<div class="col-md-12 col-12 d-flex justify-content-center">
        							<div class="form-group">
        								<div class="captcha_Dv">
		                                    <div class="g-recaptcha contactCaptcha" 
		                                    name="captcha" data-callback="recaptchaCallback" 
		                                    data-sitekey="<?php echo $this->config->item('google_sitekey') ?>"></div>
		                                </div>
		                                <div id="captcha_error" class="error_r"></div>
        							</div>
        						</div>

        						<div class="col-md-12 col-12 text-center">
        							<div class="form-group">
        								<button class="contSubmitBtn"><strong>Submit</strong></button>
                                        <div class="error_ser"></div>
        							</div>

                                    <div class="spinner-border text-muted spinForm" style="margin: 0 auto;"></div>
        						</div>

        					</div>

        				</form>

                        <div class="successBox">
                            <p>
                                Thank you for your query We will be in contact with you shortly
                            </p>
                            <div class="mt-4">
                                 <a href="<?php echo base_url(''); ?>">
                                    <button class="btn cmn_btnm1 bckHome">
                                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back Home
                                    </button>
                                </a>
                            </div>
                           
                        </div>

        			</div>


        			<div class="locationBox mt-5">
        				<div class="row">
        					<div class="col-lg-4 col-md-12 col-sm-4 col-12">
        						<div class="mb-3">
        							<h3 class="cmn-clr-m3"><strong>Find Us</strong></h3>
        							<h6 class="add_h">123, Surrey Hills Road,<br/> Surrey Hills VIC 3127</h6>
        							<h6 class="add_h "><a class="clr_inherit" href="tel:039888 1222">(03) 9888 1222</a></h6>
        						</div>
        					</div>
        					<div class="col-lg-8 col-md-12 col-sm-8 col-12">
        						<div id="map_canvas" class="mapping"></div>
        					</div>
        				</div>
        			</div>


        		</div>
        	</div>
        	
    </div>
    <div class="triangle_bot"></div>
</section>




<script src="<?php echo base_url('assets/js/map.js');?>"></script>
<script>
    function recaptchaCallback() {
        $('.error_r').text('');
    };
</script>