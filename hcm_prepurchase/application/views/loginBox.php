<form id="loginForm">
    <div class="row mt-4 mb-4">
        <div class=" col-12 mb-2">
            <div class="form-group">
                <input type="text" class="form-control customInp1" id="email" name="email" 
                                    placeholder="|Please enter email" autocomplete="new-password">
            </div>      
        </div>
        <div class=" col-12 mb-2">
            <div class="form-group">
                <input type="password" class="form-control customInp1" id="password" name="password" placeholder="|Please enter password" autocomplete="new-password">
            </div>      
        </div>

        <div class="col-md-12 col-12 text-center mt-5">
            <div class="form-group">
                <button class="loginSubmit btn " onclick="loginSubmit()"><strong>Log In</strong></button>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <a href="<?php echo base_url('forgot_password') ?>" class="cmn-clr-m3">Forgot Password</a>
                </div>
            </div>
        </div>
    </div>
</form>