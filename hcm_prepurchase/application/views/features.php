




<section class="features_section cmnSec">


    <div class="bg_triangle"></div>
        <div class="container text-center mt-5">
            <h1 class="page_head cmnHead mb-5"><strong>Features and modules</strong></h1>     
        </div>

        <div class="container p-5 content mb-5">
            
           <div class="row pckGRow">

            <?php 
              // print_r($results);
              foreach ($results as $res => $feat) {

            ?>

              <div class="col-md-6 col-12">
                   <div class="feat_box__">
                       <div class="imgBx__">
                           <img src="<?php echo base_url('assets/images/'.$feat['hero_img']); ?> ">
                       </div>
                       <div class="feat_des__">
                           <h4><?php echo $feat['title']; ?></h4>
                           <p >
                            <?php 
                              $description = $feat['content_1'];
                                if(strlen($feat['content_1'])>200) {
                                  $description = substr($feat['content_1'], 0,200).'...';
                                }
                                echo $description; 
                             
                            ?>
                              
                            </p>
                           <a href="<?php echo base_url('/features/'.$feat['slug']) ?>">
                               <button class="btn cmn_btnm1">Learn More</button>
                           </a>
                       </div>
                   </div>
               </div>


                <?php
                
              }

              ?>
        </div> 
        <!-- row ends -->

        <div class="pckGRow d-none">
            <div class="feature_grid ">
                <?php 
                  // print_r($results);
                  foreach ($results as $res => $feat) {

                ?>
                <div class="featgrid_item">
                    
                    <div class="feat_box__">
                           <div class="imgBx__">
                               <img src="<?php echo base_url('assets/images/'.$feat['hero_img']); ?> ">
                           </div>
                           <div class="feat_des__">
                               <h4><?php echo $feat['title']; ?></h4>
                               <p >
                                <?php 
                                  $description = $feat['content_1'];
                                    // if(strlen($feat['content_1'])>200) {
                                    //   $description = substr($feat['content_1'], 0,200).'...';
                                    // }
                                    echo $description; 
                                 
                                ?>
                                  
                                </p>
                               <a href="<?php echo base_url('/features/'.$feat['slug']) ?>">
                                   <button class="btn cmn_btnm1">Learn More</button>
                               </a>
                           </div>
                       </div>

                </div>
                <?php
                    
                  }

                  ?>

            </div>
        </div>


    </div>

    <div class="bg_bottom_triangle"></div>

</section>

<script type="text/javascript" src="<?php echo base_url('assets/js/masonry.js');?>"></script>
<script type="text/javascript">
    $('.feature_grid').masonry({
        itemSelector: '.featgrid_item',
        percentPosition: true
    })
</script>