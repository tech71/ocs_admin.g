
    <section class="section_one">
        <div class="bg_triangle"></div>
        <div class="container text-center mt-5 pl-0 pr-0">
            <h1 class="heading_one"><strong>Support. When you need it.</strong></h1>
            <p class="head_para2 mb-5">Individual and personalised support 24/7, 365 days a year.</p>
            <div class="image">
                <img src="<?php echo base_url('assets/images/16_9.png');?>" width="100%" height="100%" class="img-fluid">
            </div>
        </div>
        <div class="container content p-5">

            <!-- Why Choose HCM? -->

            <div class="row mt-5 justify-content-center">

                <div class="col-lg-6 col-md-6 col-sm-12 col-12 ">
                    <h2 class="heading_two">Why Choose HCM?</h2>
                   <p class="para">Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri
                            quaestio,
                            ut mel dicit oratio habemus.
                            Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                            Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                            Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel
                            dicit
                            oratio habemus.
                            Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        </p>
                        <p class="para">
                            Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                            Id unum commodo his, ad eum tantas quaerendum. 
                        </p>
                    <a href="<?php echo base_url('why_hcm');?>"><button><strong>Learn More</strong></button></a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/4_3.png');?>" alt="" width="100%"
                        height="100%" class="img-fluid">
                </div>
            </div>

            <!-- FEATURES -->

            <div class="row mt-5 justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/4_3.png');?>" alt="" width="100%" height="100%" class="img-fluid">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="feature_para">
                        <h2 class="heading_two">Features</h2>
                        <p class="para">Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri
                            quaestio,
                            ut mel dicit oratio habemus.
                            Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                            Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                            Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel
                            dicit
                            oratio habemus.
                            Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        </p>
                        <p class="para">
                            Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                            Id unum commodo his, ad eum tantas quaerendum. 
                        </p>
                        <a href="<?php echo base_url('features');?>"><button><strong>Learn More</strong></button></a>
                    </div>
                </div>
                
            </div>

            <!-- PACKAGES -->

            <div class="row mt-5 justify-content-center text-center packages">
                <h1 class="packages_head">Packages</h1>
                <div class="col-12">
                    <p class="para p_para text-center">Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri
                        quaestio, ut mel dicit oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                    </p>
                </div>
                <div class=" packages_row">

                    <div class="packgeBoxes mt-3 ">

                        <div class="package__ d-flex flex-wrap basic">
                            <div class="d-flex flex-wrap">
                                <div class="p_fPart align-self-start w-100">
                                    <h3 class="p_head__">Basic</h3>
                                    <div class="descP__ ">
                                        A system to onboard members and participants. 
                                        Contains imail for easier communication.
                                    </div>
                                    <div class="Feats_l">
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                    </div>
                                </div>
                            
                             </div>
                        </div>
                        <div class="package__  d-flex flex-wrap intermediate">
                            <div class="d-flex flex-wrap">
                                <div class="p_fPart align-self-start">
                                    <div class="best_val"><div>Best<span>Value</span></div></div>
                                    <h3 class="p_head__">Intermediate</h3>
                                    <div class="descP__ ">
                                        A basic package with system designed to store member, organizations and participant details, shifts
                                        and feedback. Contains additional applications and software.
                                    </div>
                                    <div class="Feats_l">
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                    </div>
                                </div>
                                
                            </div>
                             
                        </div>
                        <div class="package__ d-flex flex-wrap complete">
                            <div class="d-flex flex-wrap">
                                <div class="p_fPart align-self-start">
                                    <h3 class="p_head__">Complete</h3>
                                    <div class="descP__ ">
                                        Everything from the intermediate package, plus finance marketing management systems
                                    </div>
                                    <div class="Feats_l">
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                        <div>Lorem ipsum is a free text</div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="col-12 packages_row d-none">

                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 px-0">
                        <div class="packages_b">
                            <h3 class="packHd">Basic</h3>
                            <p class="text-center">
                                A system to onboard members and participants. Contains imail for easier communication.
                                Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut
                                mel
                                dicit oratio habemus.
                                Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                                A system to onboard members and participants. Contains imail for easier communication.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 px-0">
                        <div class="packages_i">
                            <h3 class="packHd">Intermediate</h3>
                            <div class="bookmark my-2">Best Value</div>
                            <p class="text-center">
                                A system to onboard members and participants. Contains imail for easier communication.
                                Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut
                                mel
                                dicit
                                oratio habemus.
                                Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                                A system to onboard members and participants. Contains imail for easier communication.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-12 px-0">
                        <div class="packages_b">
                            <h3 class="packHd">Complete</h3>
                            <p class="text-center">
                                A system to onboard members and participants. Contains imail for easier communication.
                                Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut
                                mel
                                dicit
                                oratio habemus.
                                Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                                A system to onboard members and participants. Contains imail for easier communication.
                            </p>
                        </div>
                    </div>
                </div>
                <a href="<?php echo base_url('package');?>"><button class="mt-5"><strong>Learn More</strong></button></a>
            </div>
            <!-- FORM -->
            <div class="row mt-5">
                <div class="col-12">
                    <div class="sign_up text-center">
                        <h2 class="signup_head"><strong>Sign Up To Healthcare Manager's Newsletter</strong></h2>
                        <p class="text-center signup_para">Stay up to date with all the good that we're doing, software
                            updates and more by subscribing to our newsletter!
                        </p>
                        <form id="subscribe_form">
                            <div class="form-row">
                                <div class="col mb-2">
                                    <label class="text-left w-100">Full Name</label>
                                    <input type="text" class="form-control customInp1" name="name" >
                                </div>
                                <div class=" col mb-2">
                                    <label class="text-left w-100">Email</label>
                                    <input type="text" name="email" id="email" class="form-control customInp1" 
                                        >
                                </div>
                               
                            </div>
                            <div class="form-row">
                             <div class="col">
                                    <button class="mt-4 subscribe_btn">
                                        <strong>Submit</strong>
                                    </button>
                            </div>
                            </div>
                            <div class="text-center mt-2 mb-2">
                                <div class="serverMsg"></div>
                            </div>
                            <div class="spinner-border text-muted spinForm" style="margin: 0 auto;"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="bgTriangle_bottom"></div>
    </section>


   