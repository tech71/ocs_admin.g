    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-12 ">
                    <div class="company_ct pr-5">
                        <ul class="p-0">
                            <li><img src="<?php echo base_url('assets/images/logo_w.svg');?>" height="60px" class="contact_logo"></li>
                            <li class="clr_inherit">(03) 9888 12221</li>
                            <li class="">123 Surrey Hills Road, Surrey Hills VIC 3127</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <div class="company_ct">
                        <ul class="p-0">
                            <li><a href="<?php echo base_url('why_hcm');?>">Why HCM</a></li>
                            <li><a href="<?php echo base_url('features');?>">Features</a></li>
                            <li><a href="<?php echo base_url('package');?>">Packages</a></li>
                            <li><a href="<?php echo base_url('');?>">Newsletter</a></li>
                            <li><a href="<?php echo base_url('contact');?>">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <div class="company_ct">
                        <ul class="mb-0 p-0">
                            <li><a href="<?php echo base_url('');?>">Privacy Policy</a></li>
                            <li ><a href="<?php echo base_url('');?>">Terms & Conditions</a></li>
                        </ul>
                        <!-- <p class="copyright">&#169; 2019- ALL Rights Reserved<img src="<?php echo base_url('assets/images/logo_w.svg');?>"  class="mini_logo"></p> -->
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <div class="social_icons">
                        <a href="" class="clr_inherit"><i class="fa fa-facebook footer_ic"></i></a>
                        <a href="" class="clr_inherit"><i class="fa fa-linkedin-square footer_ic"></i></a>
                        <a href="" class="clr_inherit"><i class="fa fa-twitter footer_ic"></i></a>
                    </div>
                </div>
                <div class="col-12 text-center designed_by my-2">
                    <p class="mb-1">Designed and Developed by <a class="clr_inherit" href="www.yourdevelopementteam.com.au">
                    Your Development Team</a></p>
                    <p>&#169;<?php echo date("Y") ?> - ALL Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>
    
   
    <script src="<?php echo base_url('assets/js/popper.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validate.js');?>"></script>
    <script src="<?php echo base_url('assets/js/index.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validatorMethod.js');?>"></script>
    <script src="<?php echo base_url('assets/js/main.js');?>"></script>
</body>
</html>

<script type="text/javascript">
    var baseUrl = "<?php print base_url(); ?>";
</script>