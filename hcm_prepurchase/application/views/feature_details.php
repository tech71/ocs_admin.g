
<!-- <?php 
    echo '<pre>';
    print_r($feats) ;
    echo '</pre>';
    
    
   echo $feats->prev_link_s;
      
?> 
 -->
<section class="schedule">

        <div class="bg_triangle"></div>
        <div class="container text-center mt-5">
            <h1 class="page_head cmnHead mb-5"><strong><?php echo $feats->title; ?></strong></h1>     
        </div>
        <div class="container py-5 content mb-5">
            <div class="img_container">
                <img src="<?php echo base_url('assets/images/image_icon.jpg');?>" width="100%" height="100%" alt="" class="img-fluid">
            </div>
            <div class="row mt-5 right_image">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 text-left">
                    <h2 class="sub_head_one"><?php echo $feats->heading_1; ?></h2>
                    <p class="para mt-3">
                       <?php echo $feats->content_1; ?>
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12"><img src="<?php echo base_url('assets/images/'.$feats->image_1);?>" alt="" width="100%"
                        height="100%" class="img-fluid"></div>
            </div>

            <div class="row mt-5 left_image">
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <img src="<?php echo base_url('assets/images/'.$feats->image_2);?>" alt="" width="100%"height="100%" class="img-fluid">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 text-left">
                    <h2 class="sub_head_two"><?php echo $feats->heading_2; ?></h2>
                    <p class="para mt-3">
                       <?php echo $feats->content_2; ?>
                    </p>
                </div>
            </div>
            <div class="row mt-4 btns">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 left_btn">

                    <?php 

                        if (!empty($feats->prev_link_s)) {
                           ?>
                           <a href="<?php echo base_url("features/$feats->prev_link_s"); ?>">
                                <button class="">
                                    <i class="fa fa-arrow-left left_arrow"></i>
                                    View <?php echo $feats->prev_title; ?>
                                </button>
                            </a>
                           <?php
                        }

                    ?>
                    
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 right_btn">
                    <?php 

                        if (!empty($feats->next_link_s)) {
                           ?>
                           <a href="<?php echo base_url("features/$feats->next_link_s"); ?>">
                                <button>
                                    View <?php echo $feats->next_title; ?>
                                    <i class="fa fa-arrow-right right_arrow"></i>
                                </button>
                            </a>
                           <?php
                        }

                    ?>
                    
                </div>
            </div>
        </div>

</section>

