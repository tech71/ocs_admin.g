
<?php 

	$selected = 0;

	if(isset($selected_data)){	
		$selected = 1;
	}   	
?> 


<section class="section_one registerSection">
	<div class="bg_triangle"></div>

	<div class="container text-center mt-5">
        <h1 class="heading_one mb-5"><strong>Create HCM Profile</strong></h1>

        <div class="row justify-content-center pb-5<!--  white_bx2 -->">

        	<div class="col-12">
        		<div class="stepList">
        			<div class="stepN step1 active in" data-id="step1">Admin User <span>Information</span></div>
        			<div class="stepN step2" data-id="step2">Account <span>Information</span></div>
        			<div class="stepN step3" data-id="step3">Payment <span>Information</span></div>
        			<div class="stepN step4" data-id="step4">Detail <span>Confirmation</span></div>
        		</div>
        	</div>
        	<div class="col-lg-8 col-md-10 col-12">
        		<div class="registerBox mb-3">

        			<form id="registerForm">
        					
        				<div class="formSteps step1 active" >
        					
        					<div class="row mt-4 mb-4">

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Your email</label>
	        							<input type="text" data-rule-required="true" data-rule-email_v="true" class="form-control customInp1 getRes" name="admin_email" id="admin_email" autocomplete="new-password">
	        						</div>		
	        					</div>

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Set Password</label>
	        							<input type="password" data-rule-required="true" minlength="6" class="form-control customInp1" id="password" name="password" minlength="6" id="password"  autocomplete="new-password">
	        						</div>		
	        					</div>

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Confirm Password</label>
	        							<input type="password" data-rule-required="true" data-rule-equalTo="#password" class="form-control customInp1" name="confirm_password" id="confirm_password" >
	        						</div>		
	        					</div>

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Admin name</label>
	        							<input type="text" data-rule-required="true" minlength="2"  class="form-control customInp1 getRes" name="admin_name" id="admin_name" >
	        						</div>		
	        					</div>

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Admin Position</label>
	        							<input type="text" data-rule-required="true" minlength="2" class="form-control customInp1 getRes" name="admin_position" id="admin_position" >
	        						</div>		
	        					</div>

	        					<div class=" col-12 mb-1">
	        						<div class="form-group">
	        							<label>Admin Contact number</label>
	        							<input type="text" data-rule-required="true" data-rule-number="true" minlength="10" class="form-control customInp1 getRes" name="admin_contact" id="admin_contact" >
	        						</div>		
	        					</div>


	        					<div class=" col-12 mb-1 mt-5">
	        						<div class="form-group ">
	        							<div class="row justify-content-center">
	        								<div class="col-12 text-center">
	        									<span class="btn cmn_btnm1 nextBtn" onclick="goToStep('next', 'step2')">Next</span>
	        								</div>
		        							
		        						</div>
	        						</div>		
	        					</div>


	        				</div>
	        			</div>
	        			

							<!-- step1 ends -->


	        				<div class="formSteps step2" >

	        					<div class="row mt-4 mb-4">

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Company name</label>
		        							<input type="text" data-rule-required="true" minlength="2"  class="form-control customInp1 getRes" name="company_name" id="company_name" >
		        						</div>		
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Company address</label>
		        							<input type="text" data-rule-required="true" class="form-control customInp1 getRes" name="company_address" minlength="2" id="company_address" >
		        						</div>		
		        					</div>

		        					<div class="col-md-8 col-12 mb-2">
		        						<div class="form-group">
		        							<label>Post code</label>
		        							<input type="text" data-rule-required="true" data-rule-postcodecheck="true"  class="form-control customInp1" name="company_postcode" name="company_postcode"  >
		        						</div>		
		        					</div>

		        					<div class="col-md-4 col-12 mb-2">
		        						<div class="form-group">
		        							<label>State</label>
		        							<select class="form-control custmSelect1" name="state" id="state" data-rule-required="true">
		        								<option selected disabled>[Select]</option>
		        								<option >State1</option>
		        								<option >State2</option>
		        								<option >State3</option>
		        							</select>
		        							
		        						</div>		
		        					</div>


		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Company contact number</label>
		        							<input type="text" data-rule-required="true" class="form-control customInp1 getRes" name="company_contact" id="company_contact" data-rule-number="true" minlength="10" >
		        						</div>
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Company email</label>
		        							<input type="text" data-rule-required="true" data-rule-email_v="true" class="form-control customInp1 getRes"  name="company_email" id="company_email">
		        						</div>
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Company website</label>
		        							<input type="text" data-rule-required="true" class="form-control customInp1 getRes" name="company_website" data-rule-web="true" id="company_website" >
		        						</div>

		        					</div>


		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Number of Team member</label>
		        							<input type="text" data-rule-required="true" class="form-control customInp1 getRes" name="team_member" id="team_member" data-rule-number="true" >
		        						</div>
		        					</div>

		        					<div class=" col-12 mb-2 mt-5">
		        						<div class="form-group">
		        							<div class="row">
		        								
		        								<div class="col-12  text-center">
		        									<span class="btn cmn_btnm1 nextBtn" onclick="goToStep('next', 'step3')">Next</span>
		        									<span class="btn cmn_btnm5 nextBtn" onclick="goToStep('back', 'step1')">Previous</span>
		        								</div>
		        							</div>
		        							
		        						</div>		
		        					</div>


		        				</div>
	        					
	        				</div>
	        				<!-- step2 ends -->

						
	        				<div class="formSteps step3" >

	        					<div class="row mt-4 mb-4">

	        						<div class="col-md-12 col-12 mb-2">
		        						<div class="form-group">
		        							<label>Subscription type</label>
		        							<select class="form-control custmSelect1 getRes" data-rule-required="true"  name="subscription_type" id="subscription_type">
		        								<option disabled selected value="select">Select Type</option>

		        								<?php echo $planTypeOptions; ?>

		        							</select>
		        							
		        						</div>		
		        					</div>


		        					<div class="col-md-12 col-12 mb-2">
		        						<div class="form-group">
		        							<label>Subscription Plan</label>
		        							<select <?php echo($selected == 0 ? 'disabled' : '') ?> class="form-control custmSelect1 getRes" data-rule-required="true"  name="subscription_plan" id="subscription_plan" onchange="subscriptionDets()">

		        								<option disabled selected value="select">Select Plan</option>
		        								<?php echo $planOptions ?>

		        							</select>

		        							<div class="text-left">
		        								<small class="cmn-clr-m3 plnType_c" data-toggle="modal" data-target="#planModal">Click to view plan types</small>
		        							</div>
		        							
		        						</div>		
		        					</div>


		        					<div class=" col-12 mb-2">
		        						<!-- d-flex license_amt_dv -->
		        						<div class="form-group ">
		        							<label>Amount of Licensees using HCM</label>
		        							<div>
			        							<input type="text" class="form-control customInp1" data-rule-number="true" data-rule-required="true" name="license_amt" id="license_amt"  >
			        						</div>
		        						</div>
		        					</div>

		        					<!-- <div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<input type="text" class="form-control customInp1" name="discount_code" id="discount_code" placeholder="|Discount Code">
		        						</div>
		        					</div> -->

		        					<div class="col-12 mb-2">
		        						<label>Discount</label>
		        						<div class="input-group">
										  <input type="text" class="form-control customInp1" name="discount_code" id="discount_code" >
										  <div class="input-group-append">
										    <button class="btn cmn_btnm1 checkDiscount" >Check</button>
										  </div>
										</div>
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<div class="amt_calc">
		        								<div>
		        									<span>Sub-Total:</span>
		        									<span class="subscription_plan pln_amt">
		        										<?php echo($selected ==1 ?  $selected_data['subtotal'] : 'N/A')?>
		        									</span>

		        								</div>
		        								<div>
		        									<span>Discounts:</span>
			        								<span class="discount pln_discount">
			        								N/A
			        							</span>
			        							</div>
		        								<div>
		        									<span>GST:</span>
			        								<span class="gst pln_gst">
				        								<?php echo($selected ==1 ?  $selected_data['gst'] : 'N/A')?>
				        							</span>
			        							</div>
		        								<div>
		        									<span>Total:</span>
			        								<span class="total pln_total">
				        								<?php echo($selected ==1 ?  $selected_data['total'] : 'N/A')?>
				        							</span>
			        							</div>
		        							</div>
		        						</div>
		        					</div>


		        					<div class=" col-12 mb-2">
		        						<div class="form-group">

		        							<div class="d-flex cardRadiosGr">
		        								
												<div class="customRadio">
												    <input type="radio" id="visa" value="Visa" name="card_type"  checked>
												    <label for="visa">Visa</label>
												</div>
												<div class="customRadio">
												    <input type="radio" id="master" value="Master Card" name="card_type">
												    <label for="master">Master Card</label>
												</div>
												<div class="customRadio">
												    <input type="radio" id="American" value="American Express" name="card_type">
												    <label for="American">American Express</label>
												</div>

		        							</div>
		        								
		        						</div>
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Name on card</label>
		        							<input type="text" class="form-control customInp1 getRes" data-rule-required="true" name="card_name" id="card_name">
		        						</div>
		        					</div>

		        					<div class=" col-12 mb-2">
		        						<div class="form-group">
		        							<label>Card number</label>
		        							<input type="text" class="form-control customInp1 getRes" data-rule-required="true" data-rule-number="true" name="card_number" id="card_number">
		        						</div>
		        					</div>


		        					<div class="col-md-6 col-12 mb-2">
		        						<div class="form-group">
		        							<label>Expiry (MM/YYYY)</label>
											<input type="text" class="form-control customInp1 getRes" data-rule-required="true" id="card_expiry" name="card_expiry" data-rule-expiry="true" />
										</div>
		        							
		        					</div>

		        					<div class="col-md-6 col-12 mb-2">
		        						<div class="form-group">
		        							<label>CVV</label>
		        							<input type="text" class="form-control customInp1" data-rule-required="true" data-rule-number="true" name="card_cvv"  maxlength="4" minlength="3">
		        						</div>		
		        					</div>

		        					<div class="col-md-12 col-12 mb-2">
		        						<div class="form-group text-left checkiesBx">
		        							<div>

		        								<label class="customCheckie">
			        								<input type="checkbox" id="agree1" name="agree1" data-rule-required="true">
			        								<span>I agree that HCM will change me the above amount monthly to continue my subscription to the software</span>
		        								</label>
		        								<label class="error" for="agree1"></label>

		        								<label class="customCheckie">
		        									<input type="checkbox" name="agree2" id="agree2" data-rule-required="true">
			        								<span>I have read and agree to the <a href="" class="cmn-clr-m3 text_decor">Terms & Conditions</a></span>
		        								</label>
		        								<label class="error" for="agree2"></label>
		        							</div>
		        						</div>		
		        					</div>


		        					<div class=" col-12 mb-2 mt-5">
		        						<div class="form-group">
		        							<div class="row">
		        								
		        								<div class="col-12  text-center">
		        									<span class="btn cmn_btnm1 nextBtn" onclick="goToStep('next', 'step4')">Next</span>
		        									<span class="btn cmn_btnm5 nextBtn" onclick="goToStep('back', 'step2')">Previous</span>
		        								</div>
		        							</div>
		        							
		        						</div>		
		        					</div>


	        					</div>
	        				</div>
							<!-- step3 ends -->


							<div class="formSteps step4" >

	        					<div class="row mt-4 mb-4 text-left">

	        						<div class="col-12 mb-4">
	        							
	        							<h4 class="cmn-clr-m3"><Strong>Admin User Information</Strong></h4>
	        							<div class="infoTable">
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Email Address:</Strong></div>
	        									<div class="infoTd admin_email">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Name:</Strong></div>
	        									<div class="infoTd admin_name">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Position:</Strong></div>
	        									<div class="infoTd admin_position" >N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Contact Number:</Strong></div>
	        									<div class="infoTd admin_contact" >N/A</div>
	        								</div>
	        							</div>

	        						</div>


	        						<div class="col-12 mb-4">
	        							
	        							<h4 class="cmn-clr-m3"><Strong>Account Information</Strong></h4>
	        							<div class="infoTable">
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Name:</Strong></div>
	        									<div class="infoTd company_name">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Address:</Strong></div>
	        									<div class="infoTd company_address">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Contact Number:</Strong></div>
	        									<div class="infoTd company_contact">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Email Address:</Strong></div>
	        									<div class="infoTd company_email">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Website:</Strong></div>
	        									<div class="infoTd company_website">N/A</div>
	        								</div>

	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Team Members:</Strong></div>
	        									<div class="infoTd team_member">N/A</div>
	        								</div>
	        							</div>

	        						</div>


	        						<div class="col-12 mb-4">
	        							
	        							<h4 class="cmn-clr-m3"><Strong>Payment Information</Strong></h4>
	        							<div class="infoTable">
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Subscription Type:</Strong></div>
	        									<div class="infoTd subscription_type">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Plan Type:</Strong></div>
	        									<div class="infoTd subscription_plan">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Card Type:</Strong></div>
	        									<div class="infoTd card_type">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Name on Card:</Strong></div>
	        									<div class="infoTd card_name">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Card Number:</Strong></div>
	        									<div class="infoTd card_number">N/A</div>
	        								</div>
	        								
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Card Expiry:</Strong></div>
	        									<div class="infoTd card_expiry">N/A</div>
	        								</div>
	        							</div>

	        						</div>

	        						<div class="col-12 mb-4">
	        							
	        							<h4 class="cmn-clr-m3"><Strong>Payment Summary</Strong></h4>
	        							<div class="infoTable">
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Sub Total:</Strong></div>
	        									<div class="infoTd sub_total">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Discounts:</Strong></div>
	        									<div class="infoTd discount">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>GST:</Strong></div>
	        									<div class="infoTd gst">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Total:</Strong></div>
	        									<div class="infoTd total">N/A</div>
	        								</div>
	        								<div class="info_tr">
	        									<div class="infoTd"><Strong>Date of Payment:</Strong></div>
	        									<div class="infoTd dateofpayment">N/A</div>
	        								</div>

	        								
	        							</div>

	        						</div>


	        						<div class=" col-12 mb-2 mt-5">
		        						<div class="form-group">
		        							<div class="row">
		        								
		        								<div class="col-12  text-center">
		        									<span class="btn cmn_btnm1 nextBtn registerSubmit" onclick="submitRegister()">Complete Purchase</span>
		        									<span class="btn cmn_btnm5 nextBtn" onclick="cancelRegister()">Cancel Order</span>
		        								</div>
		        								
		        							</div>
		        							
		        						</div>		
		        					</div>


	        					</div>

	        				</div>
							<!-- step4 ends -->
							<div class="text-center mt-2 mb-2">
					            <div class="serverMsg" ></div>
					        </div>
		        			<div class="spinner-border text-muted spinForm" style="margin: 0 auto;"></div>
        				

        			</form>
        			
        		</div>
        	</div>
        </div>

    <div>
    </div>
</div>

    
    <div class="triangle_bot"></div>

</section>

<!-- The Modal -->
<div class="modal" id="planModal">
  <div class="modal-dialog">
    <div class="modal-content">

    

      <!-- Modal body -->
      	<div class="modal-body">
        	<h4 class="modal-title">Packages</h4>
        	<i class="close close_modal" data-dismiss="modal">&times;</i>

        	<div class="package_scr">
	        	<div class="packgeBoxes mt-3 ">

			         		<div class="package__ d-flex flex-wrap basic">
			         			<div class="d-flex flex-wrap">
				         			<div class="p_fPart align-self-start w-100">
					         			<h3 class="p_head__">Basic</h3>
					         			<div class="descP__ ">
					         				A system to onboard members and participants. 
					         				Contains imail for easier communication.
					         			</div>
				         			</div>
				         			<div class="p_sPArt align-self-end w-100 mt-2">
										<div class="mt-3">
											<div class="checkie_grp">
											<label class="check_box">
												<input type="radio" name="basic" data-id="monthly" checked class="package_sel">
											 	<span class="monthly_btn">Monthly Plan</span>
											</label>
											<label class="check_box m_right">
												<input type="radio" name="basic" data-id="yearly" class="package_sel">
												<span class="yearly_btn">Yearly Plan</span>
											</label>
											</div>
										</div>
				         				<div class="pt-2">
											<div class="planRate basic_plan">$100</div>
											<span class="per_user basic_type">Per User, Per <span class="type_s">Month</span></span> 
										</div>
										
										<button class="select_plan mt-2 mdl_selectPlan basic" data-plan="basic" data-subscription="monthly" >Select Plan</button> 
										

										<div class="modules_list w-100 mt-4 text-left">
											<h3 class="modules_head">Modules Include:</h3>
											<div class="list ">
											<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
											<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
											<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
											<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
											<div class="list_item disabled"><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
											<div class="list_item disabled"><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
											<div class="list_item disabled"><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
											<div class="list_item disabled"><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
											<div class="list_item disabled"><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
											<div class="list_item disabled"><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
											<div class="list_item disabled"><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
											<div class="list_item disabled"><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
											<div class="list_item disabled"><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
											<div class="list_item disabled"><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
											<div class="list_item disabled"><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
										</div>
									 </div>
									 </div>
								</div>
			         		</div>
			         		<div class="package__  d-flex flex-wrap intermediate">
			         			<div class="d-flex flex-wrap">
			         				<div class="p_fPart align-self-start">
				         			<h3 class="p_head__">Intermediate</h3>
				         			<div class="descP__ ">
				         				A basic package with system designed to store member, organizations and participant details, shifts
				         				and feedback. Contains additional applications and software.
				         			</div>
				         			</div>
			         				<div class="p_sPArt align-self-end w-100 mt-2">
								 	<div class="mt-3">
								 		<div class="checkie_grp">
									 	<label class="check_box">
											<input type="radio" name="intermediate" data-id="monthly" checked class="package_sel">
										 	<span class="monthly_btn">Monthly Plan</span>
										</label>
										<label class="check_box m_right">
											<input type="radio" name="intermediate" data-id="yearly" class="package_sel">
											<span class="yearly_btn">Yearly Plan</span>
										</label>
										</div>
									</div>
			         				<div class="pt-2">
										<div class="planRate intermediate_plan">$175</div>
										<span class="per_user intermediate_type">Per User, Per <span class="type_s">Month</span></span> 									 
									 </div>
									 
									<button class="select_plan mt-2 mdl_selectPlan intermediate" data-plan="intermediate" data-subscription="monthly">Select Plan</button> 
									

									<div class="modules_list w-100 mt-4 text-left">
										<h3 class="modules_head">Modules Include:</h3>
										<div class="list ">
										<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
										<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
										<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
										<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
										<div class="list_item "><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
										<div class="list_item "><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
										<div class="list_item "><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
										<div class="list_item "><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
										<div class="list_item "><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
										<div class="list_item "><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
										<div class="list_item "><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
										<div class="list_item "><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
										<div class="list_item disabled"><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
										<div class="list_item disabled"><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
										<div class="list_item disabled"><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
									</div>
								 </div>								 
			         				</div>
								</div>
			         		</div>
			         		<div class="package__ d-flex flex-wrap complete">
			         			<div class="d-flex flex-wrap">
			         			<div class="p_fPart align-self-start">
				         			<h3 class="p_head__">Complete</h3>
				         			<div class="descP__ ">
				         				Everything from the intermediate package, plus finance marketing management systems
				         			</div>
				         		</div>
				         		<div class="p_sPArt align-self-end w-100 mt-2">
								 	<div class="mt-3">
								 		<div class="checkie_grp">
									 	<label class="check_box">
											<input type="radio" name="complete" data-id="monthly" checked class="package_sel">
										 	<span class="monthly_btn">Monthly Plan</span>
										</label>
										<label class="check_box m_right">
											<input type="radio" name="complete" data-id="yearly" class="package_sel">
											<span class="yearly_btn">Yearly Plan</span>
										</label>
									</div>
									</div>
			         				<div class="pt-2">
										<div class="planRate complete_plan">$285</div>
										<span class="per_user complete_type">Per User, Per <span class="type_s">Month</span></span> 									 
									 </div>
									 
									<button class="select_plan mt-2 mdl_selectPlan complete" data-plan="complete" data-subscription="monthly">
										Select Plan
									</button> 	
									
									<div class="modules_list w-100 mt-4 text-left">
										<h3 class="modules_head">Modules Include:</h3>
										<div class="list ">
										<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
										<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
										<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
										<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
										<div class="list_item "><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
										<div class="list_item "><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
										<div class="list_item "><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
										<div class="list_item "><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
										<div class="list_item "><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
										<div class="list_item "><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
										<div class="list_item "><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
										<div class="list_item "><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
										<div class="list_item "><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
										<div class="list_item "><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
										<div class="list_item "><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
									</div>
								 </div>						 
								 </div>
								</div>
			         		</div>
		                    
	        	</div>
        	</div>

      	</div>

      

    </div>
  </div>
</div>


<!-- 
<script type="text/javascript">

	$(document).ready(function(){
		subscriptionDets();
	})

</script>
 -->
