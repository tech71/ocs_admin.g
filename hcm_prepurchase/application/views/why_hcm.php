

<section class="why_hcm cmnSec">
   
        <div class="bg_triangle"></div>


        <div class="container text-center mt-5">
            <h1 class="page_head cmnHead mb-5"><strong>Why Choose Healthcare Manager?</strong></h1>     
        </div>

        <div class="container py-4 content mb-5">
            <div class="img_container">
                <img src="<?php echo base_url('assets/images/image_icon.jpg');?>" width="100%" height="100%" alt="" class="img-fluid">
            </div>
            <div class="row mt-5 right_image">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 text-left">
                    <h2 class="sub_head_one">Sub Header 1</h2>
                    <p class="para mt-3">Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri
                        quaestio,
                        ut mel dicit oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                        Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel
                        dicit
                        oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                        Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel
                        dicit
                        oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12"><img src="<?php echo base_url('assets/images/4_3.png');?>" alt="" width="100%" height="100%" class="img-fluid"></div>
            </div>

            <div class="row mt-5 left_image">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12"><img src="<?php echo base_url('assets/images/4_3.png');?>" alt="" width="100%"
                        height="100%" class="img-fluid"></div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 text-left">
                    <h2 class="sub_head_two ">Sub Header 2</h2>
                    <p class="para mt-3">Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio,
                        ut mel dicit oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                        Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit
                        oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                        Id unum commodo his, ad eum tantas quaerendum. Id pro assum verear eleifend
                        Lorem ipsum dolor sit amet, mei ex quis dolorum. Ad pri fuisset menandri quaestio, ut mel dicit
                        oratio habemus.
                        Per an tamquam ocurreret consetetur, cu duis accusamus consequuntur nam.
                    </p>
                </div>
                
            </div>
</div>
</section>

