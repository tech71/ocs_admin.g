

	

	<section class="section_one cmnSec package_page">

        <div class="bg_triangle"></div>
        <div class="container text-center mt-5">
            <h1 class="page_head cmnHead mb-5"><strong>Packages</strong></h1>     
        </div>

         <div class="container whiteBoxCmn">


         	<div class="row justify-content-center">
         		

         		<div class="col-md-12 col-12"> 

         			<div class="text-center p_para">
	         			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			         	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			         	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			         	consequat. cu duis accusamus consequuntur nam.
		         	</div>

		         	<div class="row">
		         	<div class="packgeBoxes mt-3 ">

		         		<div class="package__ d-flex flex-wrap basic">
		         			<div class="d-flex flex-wrap">
		         			<div class="p_fPart align-self-start w-100">
			         			<h3 class="p_head__">Basic</h3>
			         			<div class="descP__ ">
			         				A system to onboard members and participants. 
			         				Contains imail for easier communication.
			         			</div>
		         			</div>
		         			<div class="p_sPArt align-self-end w-100 mt-2">
								<div class="mt-3">
									<div class="checkie_grp">
										<label class="check_box">
											<input type="radio" name="basic" data-id="monthly" checked class="package_sel">
										 	<span class="monthly_btn">Monthly Plan</span>
										</label>
										<label class="check_box ">
											<input type="radio" name="basic" data-id="yearly" class="package_sel">
											<span class="yearly_btn">Yearly Plan</span>
										</label>
									</div>
								</div>
		         				<div class="pt-2">
									<div class="planRate basic_plan">$100</div>
									<span class="per_user basic_type">Per User, Per <span class="type_s">Month</span></span> 
								</div>
								<a href="<?php echo base_url('register?subscription_type=monthly&subscription_plan=basic') ?>" class="basic_a">
									<button class="select_plan mt-2">Select Plan</button> 
								</a>

								<div class="modules_list w-100 mt-4 text-left">
									<!-- <h3 class="modules_head">Modules Include:</h3> -->
									<div class="list ">
										<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
										<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
										<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
										<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
										<div class="list_item disabled"><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
										<div class="list_item disabled"><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
										<div class="list_item disabled"><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
										<div class="list_item disabled"><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
										<div class="list_item disabled"><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
										<div class="list_item disabled"><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
										<div class="list_item disabled"><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
										<div class="list_item disabled"><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
										<div class="list_item disabled"><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
										<div class="list_item disabled"><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
										<div class="list_item disabled"><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
									</div>
							 </div>
							 </div>
							 </div>
		         		</div>
		         		<div class="package__  d-flex flex-wrap intermediate">
		         			<div class="d-flex flex-wrap">
			         			<div class="p_fPart align-self-start">
			         				<div class="best_val"><div>Best<span>Value</span></div></div>
				         			<h3 class="p_head__">Intermediate</h3>
				         			<div class="descP__ ">
				         				A basic package with system designed to store member, organizations and participant details, shifts
				         				and feedback. Contains additional applications and software.
				         			</div>
				         		</div>
			         			<div class="p_sPArt align-self-end w-100 mt-2">
								 	<div class="mt-3">
								 		<div class="checkie_grp">
									 	<label class="check_box">
											<input type="radio" name="intermediate" data-id="monthly" checked class="package_sel">
										 	<span class="monthly_btn">Monthly Plan</span>
										</label>
										<label class="check_box m_right">
											<input type="radio" name="intermediate" data-id="yearly" class="package_sel">
											<span class="yearly_btn">Yearly Plan</span>
										</label>
										</div>
									</div>
			         				<div class="pt-2">
										<div class="planRate intermediate_plan">$175</div>
										<span class="per_user intermediate_type">Per User, Per <span class="type_s">Month</span></span> 									 
									 </div>
									 <a href="<?php echo base_url('register?subscription_type=monthly&subscription_plan=intermediate') ?>" class="intermediate_a">
										<button class="select_plan mt-2">Select Plan</button> 
									</a>

									<div class="modules_list w-100 mt-4 text-left">
										<!-- <h3 class="modules_head">Modules Include:</h3> -->
										<div class="list ">
											<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
											<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
											<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
											<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
											<div class="list_item "><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
											<div class="list_item "><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
											<div class="list_item "><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
											<div class="list_item "><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
											<div class="list_item "><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
											<div class="list_item "><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
											<div class="list_item "><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
											<div class="list_item "><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
											<div class="list_item disabled"><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
											<div class="list_item disabled"><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
											<div class="list_item disabled"><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
										</div>
								 </div>								 
			         			</div>
		         			</div>
							 
		         		</div>
		         		<div class="package__ d-flex flex-wrap complete">
		         			<div class="d-flex flex-wrap">
			         			<div class="p_fPart align-self-start">
				         			<h3 class="p_head__">Complete</h3>
				         			<div class="descP__ ">
				         				Everything from the intermediate package, plus finance marketing management systems
				         			</div>
				         		</div>
				         		<div class="p_sPArt align-self-end w-100 mt-2">
								 	<div class="mt-3">
								 		<div class="checkie_grp">
									 	<label class="check_box">
											<input type="radio" name="complete" data-id="monthly" checked class="package_sel">
										 	<span class="monthly_btn">Monthly Plan</span>
										</label>
										<label class="check_box m_right">
											<input type="radio" name="complete" data-id="yearly" class="package_sel">
											<span class="yearly_btn">Yearly Plan</span>
										</label>
									</div>
									</div>
			         				<div class="pt-2">
										<div class="planRate complete_plan">$285</div>
										<span class="per_user complete_type">Per User, Per <span class="type_s">Month</span></span> 									 
									 </div>
									 <a href="<?php echo base_url('register?subscription_type=monthly&subscription_plan=complete') ?>" class="complete_a">
									<button class="select_plan mt-2">Select Plan</button> 	
									</a>	
									<div class="modules_list w-100 mt-4 text-left">
										<!-- <h3 class="modules_head">Modules Include:</h3> -->
										<div class="list ">
											<div class="list_item"><img src="./assets/images/crm.png" width="50px" class="img-fluid">CRM Module</div>
											<div class="list_item"><img src="./assets/images/imail.png" width="50px" class="img-fluid">Imail Module</div>
											<div class="list_item"><img src="./assets/images/recruitment.png" width="50px" class="img-fluid">Recruitment Module</div>
											<div class="list_item"><img src="./assets/images/recruit_a.png" width="50px" class="img-fluid">Recruitment Application</div>
											<div class="list_item "><img src="./assets/images/admin.png" width="50px" class="img-fluid">Admin Module</div>
											<div class="list_item "><img src="./assets/images/fms.png" width="50px" class="img-fluid">FMS Module</div>
											<div class="list_item "><img src="./assets/images/member.png" width="50px" class="img-fluid">Member Module</div>
											<div class="list_item "><img src="./assets/images/organisation.png" width="50px" class="img-fluid">Organisation Module</div>
											<div class="list_item "><img src="./assets/images/organisation_a.png" width="50px" class="img-fluid">Organisation Portal</div>
											<div class="list_item "><img src="./assets/images/participant.png" width="50px" class="img-fluid">Participant Module</div>
											<div class="list_item "><img src="./assets/images/client.png" width="50px" class="img-fluid">Client Portal</div>
											<div class="list_item "><img src="./assets/images/schedule.png" width="50px" class="img-fluid">Schedules Module</div>
											<div class="list_item "><img src="./assets/images/finance.png" width="50px" class="img-fluid">Finance Module</div>
											<div class="list_item "><img src="./assets/images/marketing.png" width="50px" class="img-fluid">Marketing Module</div>
											<div class="list_item "><img src="./assets/images/plan.png" width="50px" class="img-fluid">Plan Management System</div>
										</div>
								 </div>						 
								 </div>
							</div>
		         		</div>
	                    
            		</div>
            		</div>
					<div class="last_para text-center mt-3">
						<p>If you have an enterprise of 50 or more people, please contact us so that we can organise a suitable package deal for your company</p>
						<a href="<?php echo base_url('contact') ?>"><button class="contact_btn">Contact us</button></a>
					</div>

				</div>
		    </div>

         </div>
         	


    </section>


