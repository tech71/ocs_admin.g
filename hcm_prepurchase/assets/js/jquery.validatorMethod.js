$.validator.addMethod("email_v", function (value, element) {
    		if (value != '') {
    		    return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
    		} else {
    		    return true;
    		}
    		}, "Please enter valid email address"
		);

		$.validator.addMethod("postcodecheck",
		    function (value, element) {
		        if (value != '') {
		            return /^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$/.test(value);
		        } else {
		            return true;
		        }
		    },
		    "Please enter valid 4 digit postcode number"
		); 

		$.validator.addMethod("phonenumber",
            function (value, element) {
                if (value != '') {
                    return /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{8,18}$/.test(value);
                } else {
                    return true;
                }
            },
            "Please enter valid phone number"
        );


        $.validator.addMethod("expiry",
            function (value, element) {
                if (value != '') {
                    return /^((0[1-9])|(1[0-2]))\/(\d{4})$/.test(value);
                } else {
                    return true;
                }
            },
            "Please enter valid month and year"
        );

        $.validator.addMethod("web",
            function (value, element) {
                if (value != '') {
                    return /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/.test(value);
                } else {
                    return true;
                }
            },
            "Please enter valid web url"
        );