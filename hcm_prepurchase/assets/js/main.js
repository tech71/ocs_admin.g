    // (function ($) {
    //     "use strict";
    //     var win = $(window);

    //     win.on("load", function () {

            // var subscription_type = ['monthly', 'yearly'];
            // var monthlyPlans = {basic:'100', intermediate:'175', complete:'285'};
            // var yearlyPlans = {basic:'1000', intermediate:'1500', complete:'2500'};


    //     })

    // })(jQuery);

    	


    	var $form = $("#contactForm");
        if ($form.length > 0) {

        	$form.validate({
                rules: {
                    firstname: {
                        required: true,
                        minlength: 2
                    },

                    lastname: {
                        required: true,
                        minlength: 2
                    },

                    email: {
                        required: true,
                        email_v:true
                    },
                    contact: {
                        required: true,
                        number: true,
                        minlength: 10
                    },
                    company: {
                        required: true
                    },
                    position: {
                        required: true
                    },
                    query: {
                        required: true,
                        minlength:10
                    }
                },
                messages: {
                    firstname: {
                        required: "Please Enter Name",
                        minlength: "fistName must consist of at least 2 characters"
                    },
                    lastname: {
                        required: "Please Enter Name",
                        minlength: "lastname must consist of at least 2 characters"
                    },
                    email: {
                        required: "Please provide an Email",
                        email_v: "Please enter a valid Email"
                    },
                    contact: {
                        required: "Please provide Phone Number",
                        number: "Please enter only digits",
                        minlength: "Phone Number must be atleast 10 Numbers"
                    },
                    company: {
                        required: "Please provide company name"
                    },
                    position: {
                        required: "Please provide position"
                    },
                    query: {
                    	required: "Please provide you query",
                    	minlength: "Query must be atleast 10 characters"
                    }

                },

                submitHandler: function ($form) {

                	// $("#captcha_error").html('');
                    var response = grecaptcha.getResponse();

                    if (response.length === 0) {
                        $(".error_r").html('Please check Captcha')
                    } 

                    else {
                    	var data = jQuery('#contactForm').serialize();
                        jQuery('#contactForm .spinForm').addClass('active');
                        $(".contSubmitBtn").attr("disabled", true);

                        postData('Contact_cn/contactUs', data, function(response){

                            if(response.success) {

                                $('#contactForm input[type=text]').val('');
                                $('#contactForm textarea').val('');
                                $('#contactForm').hide();
                                $('.successBox').addClass('show');
                            } 
                            else{

                                $('.error_ser').addClass('show');
                                $('.error_ser').text(response.message);
                                setTimeout(function(){ $('.error_ser').removeClass('show'); }, 5000);
                                   
                            }
                            grecaptcha.reset();
                            jQuery('#contactForm .spinForm').removeClass('active');
                            $(".contSubmitBtn").attr("disabled", false);

                        });
     
                    }
                }

            })

        }
        // contact form validation and submission ends here	

        var forgotPassForm = $('#forgotPassword');
        if (forgotPassForm.length > 0) {

            forgotPassForm.validate({
                rules: {
                    email: {
                        required: true,
                        email_v:true
                    }
                    
                },
                messages: {

                    email: {
                        required: "Please provide an Email",
                        email_v: "Please enter a valid Email"
                    }

                },
                submitHandler: function () {

                    var data = jQuery('#forgotPassword').serialize();             
                    jQuery("#forgotPassword .loginSubmit").attr("disabled", true);

                    postData('Users_cn/forgotPasswordUser', data, function(response){
                        if(response.success){
                            // $('#forgotPassForm input[type=text]').val('');
                            $('.serverMsg').addClass('success');
                            $('.serverMsg').text(response.message);
                        }
                        else{
                            $('.serverMsg').addClass('error');
                            $('.serverMsg').text(response.message);
                        }

                        setTimeout(function(){ $('.serverMsg').removeClass('error success'); }, 5000);
                        jQuery("#forgotPassword .loginSubmit").attr("disabled", false);
                    });

                }
            });
        }

        var resetPassword = $('#resetPassword');
        if (resetPassword.length > 0) {

            resetPassword.validate({
                rules: {
                    password: {
                        required: true,
                        minlength:6,
                        maxlength:100
                    },
                    confirm_password:{
                        required: true,
                        equalTo: "#password"

                    }
                    
                },
                messages: {

                    password: {
                        required: "Please enter password"
                    },
                    confirm_password:{
                        required: "Please enter password again"
                    }

                },
                submitHandler: function () {

                    var data = jQuery('#resetPassword').serialize();             
                    jQuery("#resetPassword .rese_submit").attr("disabled", true);

                    postData('Users_cn/passwordReset', data, function(response){
                        if(response.success){
                            $('.serverMsg').addClass('success');
                            $('.serverMsg').text(response.message);
                        }
                        else{
                            $('.serverMsg').addClass('error');
                            $('.serverMsg').text(response.message);
                        }

                        setTimeout(function(){ $('.serverMsg').removeClass('error success'); }, 5000);
                        jQuery("#resetPassword .rese_submit").attr("disabled", false);
                    });

                }
            });
        }


        var $c_loginFform = $("#c_loginForm");
        if ($c_loginFform.length > 0) {

            $c_loginFform.validate({
                rules: {
                    email: {
                        required: true,
                        email_v:true
                    }
                    
                },
                messages: {

                    email: {
                        required: "Please provide an Email",
                        email_v: "Please enter a valid Email"
                    }

                },
                submitHandler: function () {

                    var data = jQuery('#c_loginForm').serialize();
                    var email = jQuery('#c_loginForm input#email').val();
                    $("#c_loginForm .loginSubmit").attr("disabled", true);


                    postData('Users_cn/checkUser', data, function(response){

                        if(response.success){
                            $('#c_loginForm').remove();
                            $('.loginBox').append(response.data);
                            // $("#loginForm").show();
                            $('#loginForm #email').val(email);
                        }

                        else{
                            $('#c_loginForm .emailserverMsg').addClass('error');
                            $('#c_loginForm .emailserverMsg').text(response.message);
                            setTimeout(function(){ $('#c_loginForm .emailserverMsg').removeClass('error'); }, 5000);
                        }

                        $("#c_loginForm .loginSubmit").attr("disabled", false);
                     });
                }
            })
        }
        // login form ends



        function loginSubmit(){
            var $loginFform = $("#loginForm");
            $loginFform.validate({
                rules: {
                    email: {
                        required: true,
                        email_v:true
                    },
                    password: {
                        required: true
                    },
                    
                },
                messages: {

                    email: {
                        required: "Please provide an Email",
                        email_v: "Please enter a valid Email"
                    },
                  password: {
                        required: "Please enter password",
                        
                    },

                },
                submitHandler: function ($form) {
                var data = jQuery('#loginForm').serialize();
                    $("#loginForm .loginSubmit").attr("disabled", true);

                    postData('Users_cn/loginUser', data, function(response){

                        if(response.success){
                            console.log(response)
                        }
                        else{
                            console.log(response)
                        }
                        $("#loginForm .loginSubmit").attr("disabled", false);

                    });
                 
                }
            })
        }


        var $newsletterForm = $("#subscribe_form");
        if($newsletterForm.length > 0){
            $newsletterForm.validate({
                rules: {
                    name: {
                        required: true,
                        minlength:2
                    },
                    email: {
                        required: true,
                        email_v:true
                    },
                    
                    
                },
                messages: {

                    name: {
                        required: "Please enter name",
                        minlength: "Name must consist of at least 2 characters"
                    },

                    email: {
                        required: "Please enter Email",
                        email_v: "Please enter a valid Email"
                    }
                 
                },
                submitHandler: function ($form) {

                        $('.serverMsg').removeClass('error success');
                        var data = jQuery('#subscribe_form').serialize();
                        jQuery('#subscribe_form .spinForm').addClass('active');
                        $(".subscribe_btn").attr("disabled", true);

                        postData('Contact_cn/newsletterSubscribe', data, function(response){

                            if(response.success){
                                $('#subscribe_form input[type=text]').val('');
                                $('.serverMsg').addClass('success');
                                $('.serverMsg').text(response.message);
                            }
                            else{
                                $('.serverMsg').addClass('error');
                                $('.serverMsg').text(response.message);
                            }

                            setTimeout(function(){ $('.serverMsg').removeClass('error success'); }, 5000);
                            jQuery('#subscribe_form .spinForm').removeClass('active');
                            $(".subscribe_btn").attr("disabled", false);

                        });
                
                }
            })
        }

        // subscribe_form ends

        $('.stepN').on('click', function(){

        	if($(this).hasClass('active') && !$(this).hasClass('in')){

                var dataId = $(this).data('id');

                if(checkValid()){
     
                        if(dataId == "step4"){
                            step4Params();
                        }

                        $('.stepN').removeClass('in');
                        $('.' + dataId).addClass('in');
                        $('.formSteps').removeClass('active');
                        $('#registerForm .' + dataId).addClass('active');
                }

        		

        	}
        	
        })

        $('.checkDiscount').on('click', function(e){
            e.preventDefault();
            console.log('discount code not added yet');
        });


        $('#subscription_type').on('change', function(e){

           
            if($(this).val() !== 'select'){

                
                var data = {type : $(this).val()}

                postData('Register_cn/getPlans_Option', data, function(response){

                    if (response.success) {      
                           // console.log(res)
                        $('#registerForm select[name="subscription_plan"]').prop('disabled', false);
                        $('#subscription_plan').html(response.data);
                        subscriptionDets();
                    }

                    else {
                        console.log(response.message)
                    }
                });
             
            }

        });



function checkValid(){

    // return true;
    var $regsiterForm = $('#registerForm');
    $regsiterForm.validate({});

    if($regsiterForm.valid()){
        return true;
    }
    else{
        return false;
    }
}



function goToStep(key, step){
    
	if(key == "next"){

		if(checkValid()){

			if(step == "step4"){
        		step4Params();
        	}
			
			$('.formSteps').removeClass('active');
			$('#registerForm .' + step).addClass('active');
			$('.stepN').removeClass('in');
			$('.stepList .' + step).addClass('active');
			$('.stepList .' + step).addClass('in');

		}
	}
	else{
	
		$('.formSteps').removeClass('active');
		$('.' + step).addClass('active');
        $('.stepN').removeClass('in');
        $('.stepList .' + step).addClass('in');
		
	}	

}


function step4Params(){

        $('#registerForm .getRes').each(function() {
        	var inpId = $(this).attr('name');
        	var inpVal = $(this).val();
        	// console.log(inpId, inpVal);
        	if(inpVal !== null || inpVal !== undefined || inpVal !== '')
        	{
        		$('.infoTable .' + inpId).text(inpVal);
        	}
        	else{
				$('.infoTable .' + inpId).text('N/A');
        	}
        	
        });

        var radioValue = $("input[name='card_type']:checked").val();
        
        $('.infoTable .card_type').text(radioValue);

        $('.infoTable .sub_total').text($('.pln_amt').text());
        $('.infoTable .gst').text($('.pln_gst').text());
        $('.infoTable .total').text($('.pln_total').text());


        if($('#subscription_type').val() == 'monthly'){
            $('.infoTable .dateofpayment').text('11th day of every month');
        }else{
            $('.infoTable .dateofpayment').text('11th Jan of every year');
        }
        

}


function planCalc(plan){

    var getsubPlanAmt = plan;
	getsubPlanAmt = parseInt(getsubPlanAmt);

	var gst = getsubPlanAmt*10/100;
	var total = getsubPlanAmt + gst;

	var result = {getsubPlanAmt, gst, total};

	return result;
}



function subscriptionDets(){


    var getsubPlan = $('#registerForm select[name="subscription_plan"]')[0].value;
    var getsubPlanType = $('#registerForm select[name="subscription_type"]')[0].value;

    var data = {plan:getsubPlan, type:getsubPlanType};

    postData('Register_cn/planDets', data, function(response){

        var obj ={getsubPlanAmt:null, gst:null, total:null};    

        if(response.success){
            $('.amt_calc .subscription_plan').text(response.data['subtotal']);
            $('.amt_calc .gst').text(response.data['gst']);
            $('.amt_calc .total').text(response.data['total']);
        }else{
            $('.amt_calc .subscription_plan').text('N/A');
            $('.amt_calc .gst').text('N/A');
            $('.amt_calc .total').text('N/A');
        }
    });
	
}


function cancelRegister(){
    $('#registerForm')[0].reset();
    $('.stepN').removeClass('in active');
    goToStep('back', 'step1');

}

function submitRegister(){


    $('.serverMsg').removeClass('error success');
    var validator = $("#registerForm").validate({ ignore: [], });
    

    if($("#registerForm").valid()){

        $(".registerSubmit").attr("disabled", true);
        jQuery('#registerForm .spinForm').addClass('active');
        var data = jQuery('#registerForm').serialize();

        jQuery.ajax({
            type: "POST",
            url: baseUrl + 'Users_cn/registerUser',
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.success) {      
                    $('#registerForm input[type=text]').val('');
                    $('.serverMsg').addClass('success');
                    $('.serverMsg').text(data.message);

                    setTimeout(function(){ window.location.replace(baseUrl + '/login'); }, 3000);

                } else {

                    $('.serverMsg').addClass('error');
                    $('.serverMsg').text(data.message);
                                       
                }

                jQuery('#registerForm .spinForm').removeClass('active');
                setTimeout(function(){ 
                    $('.serverMsg').removeClass('error success'); 
                    $(".registerSubmit").attr("disabled", false); 
                }, 3000);
                

                
            },
            error: function (error) {
                console.log(error);

            }
        });        
       
    }


}

$('.package_sel').change(function(e) {

   
    var planName = $(this).attr('name');
    var planId = $(this).data('id');

     $(`.${planName}_plan`).html('<i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i>');
    

    var data = {plan:planName, type:planId};

    postData('Register_cn/planDets', data, function(response){

        if(response.success == true){
            // console.log(response);
            
            setTimeout(()=> $(`.${planName}_plan`).text(response.data['subtotal']), 100)
            $(`.${planName}_type .type_s`).text(planId == 'monthly'? 'Month': 'Year');
            $(`.${planName}_a`).attr("href", baseUrl + `register?subscription_type=${planId}&subscription_plan=${planName}`);

        }else{
            console.log(response);
        }
        
    })

    // for modal on register
    $(`.mdl_selectPlan.${planName}`).attr('data-subscription', planId);

})


$('.toggle_ic').on('click', function(){
    $('.nav_s').toggleClass('show');
})

$('.close_ic').on('click', function(){
    $('.nav_s').removeClass('show');
})

$('.feat_drop').on('mouseover', function(){
    if($(window).width() > 991){
        $(this).addClass('show');
        $(this).find('.dropdown-menu').addClass('show');
    }
})

$('.feat_drop').on('mouseleave', function(){
    if($(window).width() > 991){
        closeDropdowns()
    }
})


function closeDropdowns(){
    $('.dropdown-menu').removeClass('show');
    $('.feat_drop').removeClass('show'); 
}

$("body").on("click", function(event) {   

    if(!$(event.target).hasClass('dropdown-menu')){
        if($(window).width() > 991){
            closeDropdowns()
        }   
    }
});

$(window).on('resize', function(){
    closeDropdowns()
})

$('.caret_ic').on('click', function(e){
    e.preventDefault();
    $(this).parent().parent().parent().parent('.feat_drop').toggleClass('show');
    $(this).parent().parent().siblings('.dropdown-menu').toggleClass('show');
})


$('.mdl_selectPlan').on('click', function(e){

    var plan = $(this).attr('data-plan');
    var type = $(this).attr('data-subscription');
    // console.log(plan, type);

    $('#registerForm #subscription_type').val(type);

    var data = {type : type}

    postData('Register_cn/getPlans_Option', data, function(response){

        if (response.success) {     

            $('#registerForm select[name="subscription_plan"]').prop('disabled', false);            
            $('#subscription_plan').html(response.data);
            $('#registerForm select[name="subscription_plan"]').val(plan);
            subscriptionDets();
            $('#planModal').modal('toggle');
        }

        else {
            console.log(response.message)
        }

    })

})


function postData(url, data, callback){ 

    $.ajax({

        url: baseUrl + url,
        type: "POST",
        dataType: "JSON",
        data: data,

        success : function(response){

            if(response.status_code != undefined  && response.status_code == 555){
              console.log(response);
            }
            else{
              callback(response);
            }
        },

        error:function(response){
            console.log(response); 
        }

    }) 

}