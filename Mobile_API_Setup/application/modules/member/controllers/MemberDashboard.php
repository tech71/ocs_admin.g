<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MemberDashboard extends MX_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('Member_model');
        $this->load->model('Basic_model');
        if (!$this->session->userdata('username')) 
        {
            
        }
    }

    public function index() 
    {
        $data['member_count'] = base_url('member_count');
        $data['form_action_member_srch'] = base_url('get_members_list');
        //$this->render_view('components/admin/member/AppMember',$data);
    }

    public function member_count()
    {
        $post_data = $this->input->post();
        if(!empty($post_data))
        {
            if($post_data['view_type'] == 'year')
            {
                $where['YEAR(created)'] = date('Y');
            }
            else if($post_data['view_type'] == 'week')
            {
                $where = '';
                $where1 = 'created > DATE_SUB(NOW(), INTERVAL 1 WEEK)';
                $this->db->where($where1);
            }
            else
            {
                $where['MONTH(created)'] = date('m');
            }
            $rows = $this->Basic_model->get_result('member', $where,array('id'));
            if(!empty($rows))
            $count = count($rows);
            else
            $count = 0;
            echo json_encode(array('member_count'=>$count));exit();
        }
    }

    public function load_member_list()
    {
      $reqData = json_decode($this->input->post('request'));
      $rows = $this->Member_model->member_list($reqData);
      echo json_encode($rows);exit();
    }

    public function get_member_profile()
    {
       $row = $this->Member_model->get_member_profile();
       echo json_encode(array('status'=>true,'data'=>$row));exit();   
    }

    public function get_member_about()
    {
        $row = $this->Member_model->get_member_about();
        echo json_encode(array('status'=>true,'data'=>$row));
        exit();
    }

    public function member_app_access()
    {
        if(!empty(request_handler()))
        {
            $request = request_handler();
            $status = $request->data->status;
            $member_id = $request->data->member_id;
            $data = array('enable_app_access'=>$status);
            $where = array('id'=>$member_id);
            $this->Basic_model->update_records('member', $data, $where);
            echo json_encode(array('status'=>true));exit();
        }
    }

    public function change_status()
    {
        if(!empty(request_handler()))
        {
            $request = request_handler();
            $status = $request->data->status;
            $member_id = $request->data->member_id;
            $data = array('status'=>$status);
            $where = array('id'=>$member_id);
            $this->Basic_model->update_records('member', $data, $where);
            echo json_encode(array('status'=>true));exit();
        }   
    }

    public function member_qualification()
    {
        $row = $this->Member_model->member_qualification();
        if(!empty($row))
        {
            echo json_encode(array('status'=>true,'data'=>$row));
            exit();
        }
        else
        {
            echo json_encode(array('status'=>false));
           exit();
        }
    }

    public function get_member_preference()
    {
       $row = $this->Member_model->get_member_preference();
        if(!empty($row))
        {
            echo json_encode(array('status'=>true,'data'=>$row));
            exit();
        }
        else
        {
            echo json_encode(array('status'=>false));
           exit();
        } 
    }

    public function save_member_availability()
    {
        $insert_ary = '';
        $row = $this->Member_model->insert_records('member_availability', $data, $multiple = FALSE);
        if(!empty($row))
        {
            echo json_encode(array('status'=>true));
            exit();
        }
        else
        {
            echo json_encode(array('status'=>false));
            exit();
        } 
    }
}
