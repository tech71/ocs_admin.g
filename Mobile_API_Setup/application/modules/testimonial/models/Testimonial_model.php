<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Testimonial_model extends CI_Model 
{
	 public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
	public function getTestimonials()
	{
		$src_columns = array("tbl_testimonial.id","tbl_testimonial.title","tbl_testimonial.testimonial");		
		$this->db->select($src_columns);
        $this->db->from(TBL_PREFIX . 'testimonial');	
		//$this->db->order_by(RAND()); //	ORDER BY RAND()		
		return $this->db->get()->row();
	}	
}


