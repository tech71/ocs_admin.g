<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HCM_api_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_task_applicant_id($member_id)
    {
        $select_colown = array('rta.id');
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $select_colown)), false);
        $this->db->from('tbl_recruitment_task_applicant as rta');
        $this->db->join('tbl_recruitment_task as rt', 'rt.id = rta.taskId AND rta.archive=0', 'inner');
        $this->db->join('tbl_recruitment_task_stage as rts', "rts.key = 'cab_day' AND rts.archive=0", 'inner');
        $this->db->where(array('rta.applicant_id' => $member_id, 'rta.status' => 1, ' rta.archive' => 0));
        $query = $this->db->get();
        return $query->row();
    }
}
