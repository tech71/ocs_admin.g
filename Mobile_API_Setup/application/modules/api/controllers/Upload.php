<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->helper(array('comman', 'auth'));
    }

    /* Use to upload shift related data */

    function upload_image() {
        $shift_id = $this->input->post('shiftId');

        if (empty($shift_id)) {
            $response = array('status' => false, 'error' => 'Please provide shift id.');
            echo json_encode($response);
            exit();
        }

        if (empty($_FILES)) {
            $response = array('status' => false, 'error' => 'Please provide file to upload.');
            echo json_encode($response);
            exit();
        }

        if (!empty($_FILES) && $_FILES['file']['error'] == 0) {
            $config['upload_path'] = SHIFT_UPLOAD_PATH;
            $config['input_name'] = 'file';
            $config['directory_name'] = $shift_id;
            $config['allowed_types'] = 'jpg|jpeg|png|xlx|xls|doc|docx|pdf';

            /* do_upload IS DEFINE IN COMMON HELPER */
            $is_upload = $this->do_upload($config);

            if (isset($is_upload['error'])) {
                echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                exit();
            } else {
                echo json_encode(array('status' => true, 'filename' => $is_upload['upload_data']['file_name']));
                exit();
            }
        }
    }

    function upload_user_profile_img() {
        $member_id = $this->input->post('member_id');

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => 'Please provide member id.');
            echo json_encode($response);
            exit();
        }

        if (empty($_FILES)) {
            $response = array('status' => false, 'error' => 'Please provide file to upload.');
            echo json_encode($response);
            exit();
        }

        if (!empty($_FILES) && $_FILES['file']['error'] == 0) {
            $config['upload_path'] = MEMBER_UPLOAD_PATH;
            $config['input_name'] = 'file';
            $config['directory_name'] = $member_id;
            $config['allowed_types'] = 'jpg|jpeg|png';

            /* do_upload IS DEFINE IN COMMON HELPER */
            $is_upload = $this->do_upload($config);

            if (isset($is_upload['error'])) {
                echo json_encode(array('status' => false, 'error' => strip_tags($is_upload['error'])));
                exit();
            } else {
                $this->load->model('Basic_model');
                $record = $this->basic_model->update_records('member', array('profile_image' => $is_upload['upload_data']['file_name']), array('id' => $member_id));
                echo json_encode(array('status' => true, 'filename' => $is_upload['upload_data']['file_name']));
                exit();
            }
        }
    }

    function do_upload($config_ary) {
        $CI = & get_instance();
        $response = array();
        if (!empty($config_ary)) {
            $directory_path = $config_ary['upload_path'] . $config_ary['directory_name'];

            $config['upload_path'] = $directory_path;
            $config['allowed_types'] = isset($config_ary['allowed_types']) ? $config_ary['allowed_types'] : '';
            $config['max_size'] = isset($config_ary['max_size']) ? $config_ary['max_size'] : '';
            $config['max_width'] = isset($config_ary['max_width']) ? $config_ary['max_width'] : '';
            $config['max_height'] = isset($config_ary['max_height']) ? $config_ary['max_height'] : '';

            $this->create_directory($directory_path);

            $CI->load->library('upload', $config);

            if (!$CI->upload->do_upload($config_ary['input_name'])) {
                $response = array('error' => $CI->upload->display_errors());
            } else {
                $response = array('upload_data' => $CI->upload->data());
            }
        }
        return $response;
    }

    function create_directory($directoryName) {
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755);
        }
    }

    public function php_info() {
        phpinfo();
    }

    function create_message() {
        $response = array();

        $memberData = (object) $this->input->post();
        auth_login_status(($memberData));

        $this->load->model('Api_model');

        if (empty($memberData->memberid)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if (empty($memberData->messageId)) {
            $response = array('status' => false, 'error' => 'Please provide message id');
            echo json_encode($response);
            exit();
        }

        if (empty($memberData->text) && empty($_FILES)) {
            $response = array('status' => false, 'error' => 'Please provide message or attachment');
            echo json_encode($response);
            exit();
        }

        if (empty($memberData->lastContentId)) {
            $response = array('status' => false, 'error' => 'Please provide message lastContentId');
            echo json_encode($response);
            exit();
        } else {
            $res = $this->Api_model->check_messageId_and_contentId($memberData);
            if (empty($res)) {
                $response = array('status' => false, 'error' => 'Please provide valid messageId or lastContentId');
                echo json_encode($response);
                exit();
            }
        }

        $result = $this->Api_model->create_message($memberData);
        echo json_encode(array('status' => true));
    }

}
