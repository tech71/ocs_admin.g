<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

//class Master extends MX_Controller
class Member extends REST_Controller {


    function __construct($config = 'rest') {
        //phpinfo();
        parent::__construct($config);
        $this->load->helper(array('comman', 'auth', 'email'));
        $this->load->model('Basic_model');
        $this->load->model('Hcm_api_model');
        //check first userlogged in or not
        $memberData = api_request_handler();
        auth_login_status($memberData);
    }

    function updatememberinfo_post() {
        //$Jsonpayload = $this->post('payload');
        $objMemberdata = api_request_handler();

        //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/api/controllers/response.txt', json_encode($objMemberdata).PHP_EOL , FILE_APPEND | LOCK_EX);

        $response = array();
        //$arrPayload=json_decode($Jsonpayload);				
        $member_id = $objMemberdata->memberid;
        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }
        //$objMemberdata=$arrPayload->data;		
        if (!isset($objMemberdata->firstname) && empty($objMemberdata->firstname)) {
            is_required('First Name');
        }
        if (!isset($objMemberdata->middlename) && empty($objMemberdata->middlename)) {
            is_required('Middle Name');
        }
        if (!isset($objMemberdata->lastname) && empty($objMemberdata->lastname)) {
            is_required('Last Name');
        }
        if (!isset($objMemberdata->email)) {
            is_required('Email');
        }
        if (!isset($objMemberdata->phone)) {
            is_required('Phone');
        }
        foreach ($objMemberdata->email as $emails) {
            check_valid_email($emails->email);
        }

        $firstname = $objMemberdata->firstname;
        $middlename = $objMemberdata->middlename;
        $lastname = $objMemberdata->lastname;
        $phone_arr = $objMemberdata->phone;
        $email_arr = $objMemberdata->email;

        require_once APPPATH . 'Classes/API/member/Member.php';
        $objMember = new MemberClass\Member();
        $objMember->setMemberid($member_id);
        $objMember->setFirstname($firstname);
        $objMember->setMiddlename($middlename);
        $objMember->setLastname($lastname);
        $objMember->setMemberEmail($email_arr);
        $objMember->setMemberPhone($phone_arr);
        $response = $objMember->api_update_member_info();

        if ($response) {
            $response = array('status' => true, 'success' => system_msgs('insert_success'));
        } else {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        echo json_encode($response);
    }

    function updatememberfavorite_post() {
        $objMemberdata = api_request_handler();
        //$Jsonpayload = $this->post('payload');		
        $response = array();
        //$arrPayload=json_decode($Jsonpayload);				
        $member_id = $objMemberdata->memberid;
        //	$objMemberdata=$arrPayload->data;		
        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if (!isset($objMemberdata->places)) {
            is_required('Places');
        }
        if (!isset($objMemberdata->activities)) {
            is_required('Activities');
        }

        require_once APPPATH . 'Classes/API/member/Member.php';
        $objMember = new MemberClass\Member();
        $objMember->setMemberid($member_id);
        $objMember->setMemberActivity($objMemberdata->activities);
        $objMember->setMemberPlace($objMemberdata->places);
        $response = $objMember->api_update_member_favorite();
        if ($response) {
            $response = array('status' => true, 'success' => system_msgs('insert_success'));
        } else {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        echo json_encode($response);
    }

    function updatememberqualification_post() {
        $objMemberdata = api_request_handler();
        #pr($objMemberdata);
        $response = array();
        $member_id = $objMemberdata->memberid;
        $currunt_date = date("Y-m-d H:i:s");
        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }
        $qualficaionId = 0;
        if (!isset($objMemberdata->id) && empty($objMemberdata->id)) {
            $qualficaionId = 0;
        } else {
            $qualficaionId = $objMemberdata->id;
        }
        if (!isset($objMemberdata->title) && empty($objMemberdata->title)) {
            is_required('Title');
        }
        if (!isset($objMemberdata->expiry) && empty($objMemberdata->expiry)) {
            is_required('Expiry');
        }
        if (!isset($objMemberdata->file) && empty($objMemberdata->file)) {
            is_required('File');
        }

        require_once APPPATH . 'Classes/API/member/MemberQualification.php';
        $objMemberQual = new MemberQualificationClass\MemberQualification();

        $objMemberQual->setMemberid($member_id);
        $objMemberQual->setMemberqualificationid($qualficaionId);
        $objMemberQual->setTitle($objMemberdata->title);
        $objMemberQual->setExpiry($objMemberdata->expiry);
        $objMemberQual->setFilename($objMemberdata->file);
        $objMemberQual->setCreated($currunt_date);

        $check_Qual_exist = false;
        if ($qualficaionId > 0) {
            $response = $objMemberQual->CheckMemberQualification_exist();
            if (!$response) {
                $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
                echo json_encode($response);
                exit;
            }
        }


        $response = $objMemberQual->UpdateMemberQualification();
        if ($response) {
            $response = array('status' => true, 'success' => system_msgs('insert_success'));
        } else {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }
        echo json_encode($response);
    }

    function memberinfo_post() {
        $memberData = api_request_handler();
        $response = array();
        $member_id = $memberData->memberid;
        require_once APPPATH . 'Classes/API/member/MemberLogin.php';
        $objMemberLogin = new MemberLoginClass\MemberLogin();
        $objMemberLogin->setMemberid($member_id);

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        } else {
            $member_info = $objMemberLogin->api_get_member_info();
            $response = array('status' => true, 'data' => $member_info);
        }
        echo json_encode($response);
    }

    function createfeedback_post() {
        $response = array();
        $feedbackdata = api_request_handler();
        $member_id = $feedbackdata->memberid;
        $rating = $feedbackdata->rating;
        $feedback = $feedbackdata->feedback;
        $currunt_date = date("Y-m-d H:i:s");
        require_once APPPATH . 'Classes/API/member/MemberFeedback.php';
        $objMemberFeedback = new MemberFeedbackClass\MemberFeedback();
        $objMemberFeedback->setMemberid($member_id);
        $objMemberFeedback->setCreatedDate($currunt_date);
        $objMemberFeedback->setRating($rating);
        $objMemberFeedback->setFeedback($feedback);
        $rating = (int) $rating;

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if (empty($rating) || empty($feedback)) {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        }if ($rating < 1 || $rating > 5) {
            $response = array('status' => false, 'error' => system_msgs('feedback_rating'));
        } else {
            $feedbackMember = $objMemberFeedback->create_feedback();
            if ($feedbackMember) {
                $response = array('status' => true, 'success' => system_msgs('insert_success'));
            } else {
                $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
            }
        }
        echo json_encode($response);
    }

    function getmemberqualification_post() {
        // Get Json Data
        $Memberdata = api_request_handler();
        //$Jsonpayload = $this->post('payload');
        $response = array();
        //$arrPayload=json_decode($Jsonpayload);				
        $member_id = $Memberdata->memberid;

        // Set Member id in Qualification Class object
        $currunt_date = date("Y-m-d H:i:s");
        require_once APPPATH . 'Classes/API/member/MemberQualification.php';
        $objMemberQual = new MemberQualificationClass\MemberQualification();
        $objMemberQual->setMemberid($member_id);

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        } else {
            $member_Qual = $objMemberQual->getMemberQualification();
            $response = array('status' => true, 'data' => $member_Qual);
        }
        echo json_encode($response);
    }

    function updatememberaddress_post() {
        $response = array();
        $objMemberdata = api_request_handler();
        #pr($objMemberdata);
        $member_id = $objMemberdata->memberid;

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        require_once APPPATH . 'Classes/API/member/MemberAddress.php';
        $objMemberAddress = new MemberAddressClass\MemberAddress();
        $objMemberAddress->setMemberid($member_id);

        $addressId = 0;
        if (!isset($objMemberdata->id) && empty($objMemberdata->id)) {
            $addressId = 0;
        } else {
            $addressId = $objMemberdata->id;
        }
        if (!isset($objMemberdata->street) || empty($objMemberdata->street)) {
            is_required('Street');
        }
        if (!isset($objMemberdata->city) || empty($objMemberdata->city)) {
            is_required('City');
        }
        if (!isset($objMemberdata->postal) || empty($objMemberdata->postal)) {
            is_required('postal');
        }
        if (!isset($objMemberdata->state) || empty($objMemberdata->state)) {
            is_required('state');
        }
        #if(empty($objMemberdata->primary_address)){is_required('Primary Address Type');}

        $primary_address = isset($objMemberdata->primary_address) ? $objMemberdata->primary_address : 0;
        $objMemberAddress->setMemberAddressId($addressId);
        $objMemberAddress->setStreet($objMemberdata->street);
        $objMemberAddress->setCity($objMemberdata->city);
        $objMemberAddress->setPostal($objMemberdata->postal);
        $objMemberAddress->setPrimaryAddress($primary_address);
        $objMemberAddress->setState($objMemberdata->state);

        $response = $objMemberAddress->UpdateMemberAddress();
        /* if($response){
          $response = array('status' => true, 'success' => system_msgs('insert_success'));
          }else{
          $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
      } */
      echo json_encode($response);
  }

  function get_activity_places_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
    } else {
        require_once APPPATH . 'Classes/API/member/Member.php';
        $objMember = new MemberClass\Member();
        $objMember->setMemberid($member_id);
        $activity_info = $objMember->api_get_activity_places_info();
        $response = array('status' => true, 'data' => $activity_info);
    }
    echo json_encode($response);
}

function TrimArray($Input) {

    if (!is_array($Input))
        return trim($Input);

    return array_map('TrimArray', $Input);
}

function get_member_upcoming_shift_post() {

    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
    } else {
        require_once APPPATH . 'Classes/API/member/Shift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setShiftMember($member_id);
        $shifts = $objShift->get_member_upcoming_shift($where_ary = array());
        $response = array('status' => true, 'data' => $shifts);
    }
    echo json_encode($response);
}

function find_shift_post() {
    $response = array();
    $memberData = api_request_handler();

    if (empty($memberData->memberid)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($memberData->date_range)) {
        $response = array('status' => false, 'error' => 'Please provide Date range.');
        echo json_encode($response);
        exit();
    }

    $where_ary = array('shift_day' => isset($memberData->shift_day)?$memberData->shift_day:'',
        'date_range' => $memberData->date_range,
        'shift_start' => isset($memberData->shift_start)?$memberData->shift_start:'',
        'shift_duration_hour' => isset($memberData->shift_duration_hour)?$memberData->shift_duration_hour:'',
        'shift_duration_min' => isset($memberData->shift_duration_min)?$memberData->shift_duration_min:'',
        'travel_distance' => isset($memberData->travel_distance) ? $memberData->travel_distance : '',
    );

   #pr($where_ary);
    $member_id = $memberData->memberid;
    require_once APPPATH . 'Classes/API/member/Shift.php';
    $objShift = new ShiftClass\Shift();
    $objShift->setShiftMember($member_id);
    $shifts = $objShift->get_member_upcoming_shift($where_ary);
    $response = array('status' => true, 'data' => $shifts);
        #pr($shifts);
    echo json_encode($response);
}

function save_shift_feedback_post() {
    $response = array();
    $memberData = api_request_handler();
        #$selected_date= $memberData->selected_date;

    $member_id = $memberData->memberid;
    $shift_id = $memberData->shiftid;
    $incident_type = $memberData->incident_type;
    $what_happen = $memberData->what_happen;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($shift_id)) {
        $response = array('status' => false, 'error' => 'Please provide Shift id.');
        echo json_encode($response);
        exit();
    }

    require_once APPPATH . 'Classes/API/member/ShiftFeedback.php';
    $objShiftFeedBack = new ShiftFeedbackClass\ShiftFeedback();
    $objShiftFeedBack->setMemberId($member_id);
    $objShiftFeedBack->setShiftId($shift_id);
    $objShiftFeedBack->setIncidentType($incident_type);
    $objShiftFeedBack->setWhatHappen($what_happen);
    $response = $objShiftFeedBack->save_shift_feedback();

    echo json_encode($response);
}

function get_shift_record_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $shift_id = $memberData->shiftid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($shift_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_shift_id'));
        echo json_encode($response);
        exit();
    }

    require_once APPPATH . 'Classes/API/member/Shift.php';
    $objShift = new ShiftClass\Shift();
    $objShift->setShiftMember($member_id);
    $objShift->setShiftId($shift_id);
    $shifts = $objShift->get_shift_record();
    $response = array('status' => true, 'data' => $shifts);
    echo json_encode($response);
}

function save_shift_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $shift_id = $memberData->shiftid;
    $status = $memberData->status;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($shift_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_shift_id'));
        echo json_encode($response);
        exit();
    }

    require_once APPPATH . 'Classes/API/member/Shift.php';
    $objShift = new ShiftClass\Shift();
    $objShift->setShiftMember($member_id);
    $objShift->setShiftId($shift_id);
    $objShift->setStatus($status);
    $shifts = $objShift->save_shift();
    echo json_encode($shifts);
}

function member_shifts_to_confirm_post() {

    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
    } else {
        require_once APPPATH . 'Classes/API/member/Shift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setShiftMember($member_id);
        $shifts = $objShift->member_shifts_to_confirm();
        $response = array('status' => true, 'data' => $shifts);
    }
    echo json_encode($response);
}

function member_availibility_list_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
    } else {
        require_once APPPATH . 'Classes/API/member/MemberAvailability.php';
        $objAvail = new MemberAvailabilityClass\MemberAvailability();
        $objAvail->setMemberid($member_id);
        $avail_list = $objAvail->member_availibility_list();
        $response = array('status' => true, 'data' => $avail_list);
    }
    echo json_encode($response);
}

function member_availibility_detail_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $availibility_id = $memberData->availibilityid;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        die;
    }

    if (empty($availibility_id)) {
        $response = array('status' => false, 'error' => 'Please provide Availibility id');
        echo json_encode($response);
        die;
    }

    require_once APPPATH . 'Classes/API/member/MemberAvailability.php';
    $objAvail = new MemberAvailabilityClass\MemberAvailability();
    $objAvail->setMemberid($member_id);
    $objAvail->setMemberavailabilityid($availibility_id);
    $avail_list = $objAvail->member_availibility_detail();
    $msg = '';
    if (empty($avail_list)) {
        $msg = 'No record found';
        $avail_list = array();
    }

    $response = array('status' => true, 'data' => $avail_list, 'msg' => $msg);

    echo json_encode($response);
}

function shift_sign_off_post() {

    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $shift_id = $memberData->shiftid;
    $goal_ary = isset($memberData->goal_ary) ? $memberData->goal_ary : array();
    $signature_ary = isset($memberData->signature_ary) ? $memberData->signature_ary : array();

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($shift_id)) {
        $response = array('status' => false, 'error' => 'Please provide shift id.');
        echo json_encode($response);
        exit();
    }

   /* if (empty($goal_ary)) {
        $response = array('status' => false, 'error' => 'Please provide Goal.');
        echo json_encode($response);
        exit();
    }*/

    if (empty($signature_ary)) {
        $response = array('status' => false, 'error' => 'Please provide Signature.');
        echo json_encode($response);
        exit();
    }

    $shift_info = array('goal_ary' => isset($goal_ary) ? $goal_ary : array(),
        'shift_amendment' => $memberData->shift_amendment,
        'start_time' => isset($memberData->start_time) ? $memberData->start_time : '',
        'end_time' => isset($memberData->end_time) ? $memberData->end_time : '',
        'total_break_time' => isset($memberData->total_break_time) ? $memberData->total_break_time : '',
        'shift_km' => isset($memberData->shift_km) ? $memberData->shift_km : '',
        'additional_expenses' => isset($memberData->additional_expenses) ? $memberData->additional_expenses : 0,
        'expense_ary' => isset($memberData->expense_ary) ? $memberData->expense_ary : array(),
        'signature_ary' => isset($signature_ary) ? $signature_ary : array(),
    );

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
    } else {
        require_once APPPATH . 'Classes/API/member/Shift.php';
        $shiftOff = new ShiftClass\Shift();
        $shiftOff->setShiftMember($member_id);
        $shiftOff->setShiftId($shift_id);
        $avail_list = $shiftOff->shift_sign_off($shift_info);
        $response = $avail_list;
    }
    echo json_encode($response);
}

function get_shift_incident_type_post() {
    $memberData = api_request_handler();
    $this->load->model('Basic_model');
    $record = $this->basic_model->get_record_where('shift_incident_type', $column = array('id', 'name'), $where = '');
    echo json_encode(array('status' => true, 'data' => $record));
}

function get_all_shift_by_date_post() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $selected_date = $memberData->selected_date;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($selected_date) || $selected_date == '0000-00-00' || $selected_date == '1970-01-01') {
        $response = array('status' => false, 'error' => "Please provide date.");
        echo json_encode($response);
        exit();
    }

    require_once APPPATH . 'Classes/API/member/Shift.php';
    $objShift = new ShiftClass\Shift();
    $objShift->setShiftMember($member_id);
        #$objShift->setShiftDate($shift_date);
    $shifts = $objShift->get_all_shift_by_date($selected_date);
    $response = $shifts;
    echo json_encode($response);
}

function confirm_shift() {
    $response = array();
    $memberData = api_request_handler();
    $member_id = $memberData->memberid;
    $shift_id = $memberData->shift_id;

    if (empty($member_id)) {
        $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
        echo json_encode($response);
        exit();
    }

    if (empty($shift_id)) {
        $response = array('status' => false, 'error' => "Please provide shift id.");
        echo json_encode($response);
        exit();
    }

    require_once APPPATH . 'Classes/API/member/Shift.php';
    $objShift = new ShiftClass\Shift();
    $objShift->setShiftMember($member_id);
    $objShift->setShiftDate($shift_date);
    $shifts = $objShift->confirm_shift($selected_date);
    $response = $shifts;
    echo json_encode($response);
}

    /*
     * function : create_member_availiability_post
     * use: crate availiability of member
     */
    function create_member_availiability_post() {
        $response = array();
        // handle request and check token
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;

         //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/api/controllers/response.txt', json_encode($memberData).PHP_EOL , FILE_APPEND | LOCK_EX);die;
        // if is_default value 2 mean its special availiability 
        // In special availiability end date is required field
        $end_date = isset($memberData->is_default) && $memberData->is_default == 2 ? $memberData->end_date : '';
        $first_week = isset($memberData->first_week) ? json_encode($memberData->first_week) : '';
        $second_week = isset($memberData->second_week) ? json_encode($memberData->second_week) : '';
        $travel_km = isset($memberData->travel_km) ? $memberData->travel_km : 10;
        $memberData->flexible_availability = isset($memberData->flexible_availability) ? $memberData->flexible_availability : 0;

        // check here member
        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        // check is_default value
        if (empty($memberData->is_default)) {
            $response = array('status' => false, 'error' => 'Please select DEFAULT availability for this member.');
            echo json_encode($response);
            exit();
        }

        // check title value
        if (empty($memberData->title)) {
            $response = array('status' => false, 'error' => 'Title is required.');
            echo json_encode($response);
            exit();
        }

        // check start date
        if (empty($memberData->start_date)) {
            $response = array('status' => false, 'error' => 'Start date is required.');
            echo json_encode($response);
            exit();
        }

         // In special availiability end date is required field
        if (empty($memberData->end_date) && $memberData->is_default == 2) {
            $response = array('status' => false, 'error' => 'End date is required when you select DEFAULT availability "NO".');
            echo json_encode($response);
            exit();
        }

        // check first week and second week date is not empty
        if (empty(json_decode($first_week,true)) && empty(json_decode($second_week,true))) {
            $response = array('status' => false, 'error' => 'Need atleast one week to create availability.');
            echo json_encode($response);
            exit();
        }
        // check first week and second week date is not empty
        if (empty($memberData->flexible_availability)) {
            $response = array('status' => false, 'error' => 'Flexible availability is required.');
            echo json_encode($response);
            exit();
        }
        //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/api/controllers/response.txt', $memberData->flexible_availability.PHP_EOL , FILE_APPEND | LOCK_EX);
        
        // check first week and second week date is not empty
        if (empty($memberData->travel_km)) {
            $response = array('status' => false, 'error' => 'travel km is required.');
            echo json_encode($response);
            exit();
        }

        // Include member availability class MemberAvailability
        require_once APPPATH . 'Classes/API/member/MemberAvailability.php';
        
        // create object of member availability class
        $objAvail = new MemberAvailabilityClass\MemberAvailability();

        $objAvail->setMemberid($member_id);
        $objAvail->setTitle($memberData->title);
        $objAvail->setIsDefault($memberData->is_default);

        $objAvail->setStartDate($memberData->start_date);

      

        $objAvail->setEndDate($end_date);
        $objAvail->setFirstWeek($first_week);
        $objAvail->setSecondWeek($second_week);
        $objAvail->setFlexibleAvailability($memberData->flexible_availability);
        $objAvail->setFlexibleKm($memberData->flexible_km);
        $objAvail->setTravelKm($travel_km);

        // if its availability, check on this date another availability already exist or not, 
        // if its already show error to user Availability already exist
        if ($memberData->is_default == 2) {

            // check Availability already exist
            $record_exist = $objAvail->check_availibility_already_exist();
            if(!empty($record_exist)){
                $response = array('status' => false, 'error' => 'Availability is already exist on selected date.');
                echo json_encode($response);
                exit();
            }
        }

        // create Availability
        $response = $objAvail->create_member_availiability();
        echo json_encode($response);
    }

    function member_msg_list_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;


        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }
        $this->load->model('Api_model');
        $result = $this->Api_model->get_external_messages($memberData, $member_id);
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function member_msg_badgechcount_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;


        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        $this->load->model('Api_model');
        $result = $this->Api_model->get_external_messages_badgechcount($member_id);

        echo json_encode(array('status' => true, 'data' => ['count' => $result]));
    }

    function member_single_chat_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;
        $messageId = $memberData->messageId;
        $this->load->model('Api_model');

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if (empty($messageId)) {
            $response = array('status' => false, 'error' => 'Please provide message id');
            echo json_encode($response);
            exit();
        }

        $result = $this->Api_model->member_single_chat($messageId, $member_id);
        echo json_encode(array('status' => true, 'data' => $result));
    }

    function push_notification_setting_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = trim($memberData->memberid);
        $notification_setting = isset($memberData->notification_setting)?trim($memberData->notification_setting):'';

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if ($notification_setting == '') {
            $response = array('status' => false, 'error' => 'Please provide notification setting');
            echo json_encode($response);
            exit();
        }

        if ($notification_setting != '0' && $notification_setting != 1) {
            $response = array('status' => false, 'error' => 'Please provide notification setting either 0 or 1');
            echo json_encode($response);
            exit();
        }

        require_once APPPATH . 'Classes/API/member/Member.php';
        $objMember = new MemberClass\Member();
        $objMember->setMemberid($member_id);
        $objMember->setPushNotificationEnable($notification_setting);
        $update_info = $objMember->push_notification_setting();
        $response = array('status' => true, 'msg' => 'Setting updated successfully.');
        echo json_encode($response);
        exit();
    }

    function get_participant_goal($shift_id) {
        $response = array();
        $participantData = api_request_handler();

        require_once APPPATH . 'Classes/API/member/Shift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setShiftId($shift_id);
        $goal_info = $objShift->get_shift_goal();
        return $goal_info;
    }

    /* shift that is running */

    function get_shift_for_review_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        require_once APPPATH . 'Classes/API/member/Shift.php';
        $objShift = new ShiftClass\Shift();
        $objShift->setShiftMember($member_id);
        $shifts = $objShift->get_shift_for_review();
        $temp = array();
        #echo '<pre>';print_r($shifts['shift_data']);
        if (!empty($shifts['shift_data'])) {
            foreach ($shifts['shift_data'] as $key => $shift) {
                $temp['shift_id'] = $shift['shift_id'];
                $temp['start_time'] = $shift['start_time'];
                $goal_ary = $this->get_participant_goal($shift['shift_id']);
                $temp['shift_goal'] = !empty($goal_ary) ? $goal_ary : array();
                #$temp['goal_option'] =  $this->Basic_model->get_result('goal_rating', array('archive'=>0),array('rating,name'));
                #$main_array[] = $temp;
            }
        }
        $response = array('status' => true, 'data' => $temp);
        echo json_encode($response);
    }

    /*
    *   This API is also called when rest mPin from setting at that time is_reset_pinis set to 1
    *   and other time is_reset_pin is 0 or not sent from device
    */
    function update_mPin_after_first_login_post() {
        $response = array();
        $memberData = api_request_handler();
        $member_id = $memberData->memberid;
        $mpin = $memberData->mpin;
        $screen = isset($memberData->is_reset_pin)?$memberData->is_reset_pin:0; //


        if (empty($member_id)) {
            $response = array('status' => false, 'error' => system_msgs('blank_member_id'));
            echo json_encode($response);
            exit();
        }

        if (empty($mpin)) {
            $response = array('status' => false, 'error' => system_msgs('blank_mpin_id'));
            echo json_encode($response);
            exit();
        }

        if(!is_numeric($mpin) || strlen((string)$mpin) < 6 ){
            $response = array('status' => false, 'error' => system_msgs('valid_mpin_id'));
            echo json_encode($response);
            exit();
        }

       // @file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/api/controllers/response.txt', json_encode($memberData).PHP_EOL , FILE_APPEND | LOCK_EX);
        //die;

        $encrypted_pin = password_hash($mpin, PASSWORD_BCRYPT);

        $update_data['pin'] = $encrypted_pin;

        if(isset($screen) && $screen != 1)
            $update_data['is_new_member'] = 0;

        if($this->Basic_model->update_records('member', $update_data, ['id' => $member_id])){

            if(isset($screen) && $screen != 1){
                $row = $this->Hcm_api_model->get_task_applicant_id($member_id);
                if(!empty($row))
                {
                    $task_applicant_id = $row->id;
                    $this->Basic_model->update_records('recruitment_applicant_group_or_cab_interview_detail', ['app_login_status' => 1], ['recruitment_task_applicant_id' => $task_applicant_id]);
                }
            }
            echo json_encode(array('status'=>true));
            exit();
        }
        else{
            echo json_encode(array('status'=>false));
            exit();
        }        
    }

    public function version_post()
    {
        
    }


//    function create_availability_post() {
//        $memberData = api_request_handler();
//        $member_id = $memberData->memberid;
//
//        if (!empty($memberData)) {
//            $validation_rules = array(
//                array('field' => 'start_date', 'label' => 'Start Date', 'rules' => 'required'),
//                array('field' => 'title', 'label' => 'Title', 'rules' => 'required'),
//            );
//
//            $this->form_validation->set_data((array)$memberData);
//            $this->form_validation->set_rules($validation_rules);
//
//            if ($this->form_validation->run()) {
//                $is_default = $responseAry->data->is_default;
//                $memberId = $responseAry->memberId;
//                $insert_ary = array('title' => $responseAry->data->title,
//                    'memberId' => $memberId,
//                    'start_date' => DateFormate($responseAry->data->start_date, 'Y-m-d'),
//                    'end_date' => isset($responseAry->data->end_date) && !empty($responseAry->data->end_date) ? DateFormate($responseAry->data->end_date, 'Y-m-d') : '',
//                    'first_week' => isset($responseAry->data->first_week) ? json_encode($responseAry->data->first_week) : '',
//                    'second_week' => isset($responseAry->data->second_week) ? json_encode($responseAry->data->second_week) : '',
//                    'flexible_availability' => isset($responseAry->data->flexible_availability) ? $responseAry->data->flexible_availability : '',
//                    'flexible_km' => isset($responseAry->data->flexible_km) ? $responseAry->data->flexible_km : '',
//                    'travel_km' => isset($responseAry->data->travel_km) ? $responseAry->data->travel_km : '',
//                    'status' => 1,
//                    'is_default' => $is_default,
//                    'travel_km' => isset($responseAry->data->travel_km) ? $responseAry->data->travel_km : 10,
//                    'updated' => DATE_TIME
//                );
//
//                if ($is_default == 1) {
//                    $delete_data = array('memberId' => $responseAry->memberId);
//                    $this->delete_default_availability($delete_data);
//                }
//
//                if ($responseAry->is_update == 0) {
//                    $availability_id = $row = $this->Basic_model->insert_records('member_availability', $insert_ary, $multiple = FALSE);
//                    $last_query = $this->db->last_query();
//                    //@file_put_contents($_SERVER['DOCUMENT_ROOT'].'/application/modules/member/controllers/response.txt', $last_query.PHP_EOL , FILE_APPEND | LOCK_EX);
//                    $this->generate_member_shift($row);
//                    $msg = "Availabitity created successfully.";
//                    $log_msg = 'Added new availability : ' . $availability_id;
//                } else {
//                    $availability_id = $responseAry->data->id;
//                    $where = array('id' => $responseAry->data->id);
//                    $row = $this->Basic_model->update_records('member_availability', $insert_ary, $where);
//                    $msg = "Availabitity updated successfully.";
//                    $log_msg = 'Updated availability : ' . $responseAry->full_name;
//                }
//                #if($responseAry->avail_list)
//                {
//                    #$this->merge_avail_list($responseAry,$memberId,$availability_id);
//                }
//                
//                $return = array('status' => true, 'response_msg' => $msg);
//            } else {
//                $errors = $this->form_validation->error_array();
//                $return = array('status' => false, 'error' => implode(', ', $errors));
//            }
//            echo json_encode($return);
//            exit();
//        }
//
//        $response = array('status' => true, 'data' => '');
//        echo json_encode($response);
//    }
}
