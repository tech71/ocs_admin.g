<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

//class Master extends MX_Controller
class Auth extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->helper(array('comman', 'auth'));
    }

    function memberlogin_post() { 
        $memberData = api_request_handler();
        $response = array();
        $member_id = $memberData->memberid;
        $member_pin = $memberData->pin;
        $device_id = isset($memberData->device_id)?$memberData->device_id:'';

        if (empty($member_id) || empty($member_pin)) {
            $response = array('status' => false, 'error' => system_msgs('something_went_wrong'));
        } else {
            require_once APPPATH . 'Classes/API/member/MemberLogin.php';
            $objMemberLogin = new MemberLoginClass\MemberLogin();
            $currunt_date = date("Y-m-d H:i:s");
            $objMemberLogin->setLoginUpdate($currunt_date);
            $objMemberLogin->setLoginCreated($currunt_date);
            $objMemberLogin->setMemberid($member_id);
            $objMemberLogin->setPin($member_pin);
            $objMemberLogin->setDeviceId($device_id);
            $response = $objMemberLogin->check_auth();
            //$response = array('status' => true, 'data' => $Jsonpayload);
        }
        echo json_encode($response);
    }

    function request_send_otp_post() {
        $memberData = api_request_handler();
        $response = array();
        $member_id = $memberData->memberid;

        if (empty($member_id)) {
            $response = array('status' => false, 'error' => 'Member id is required');
        } else {
            require_once APPPATH . 'Classes/API/member/MemberLogin.php';
            $objMemberLogin = new MemberLoginClass\MemberLogin();
            $objMemberLogin->setMemberid($member_id);

            $otp = mt_rand(100000, 999999);
            $objMemberLogin->setOtp($otp);
            $objMemberLogin->setOtp_expire_time(DATE_TIME);

            $response = $objMemberLogin->check_login_attampt();
            if (!empty($response)) {

                $objMemberLogin->update_otp_details();
                $objMemberLogin->send_otp_on_email();
                $response = ['status' => true];
            } else {

                $response = ['status' => false, 'error' => 'Invalid member id'];
            }
        }
        echo json_encode($response);
    }

    function verify_sended_otp_post() {
        $memberData = api_request_handler();
        $response = array();

        if (empty($memberData->memberid) && empty($memberData->otp)) {
            $response = array('status' => false, 'error' => 'Member id is required');
        } else {
            require_once APPPATH . 'Classes/API/member/MemberLogin.php';
            $objMemberLogin = new MemberLoginClass\MemberLogin();

            $objMemberLogin->setMemberid($memberData->memberid);
            $objMemberLogin->setOtp($memberData->otp);

            $res = $objMemberLogin->check_its_valid_otp($memberData->otp);
        }
        echo json_encode($res);
    }

    function reset_pin_using_otp_post() {
        $memberData = api_request_handler();
        $response = array();


        if (empty($memberData->memberid) || empty($memberData->otp)) {
            $response = array('status' => false, 'error' => 'Member id is required and otp');
        } elseif (empty($memberData->new_pin)) {
            $response = array('status' => false, 'error' => 'Pin is required');
        } else {
            require_once APPPATH . 'Classes/API/member/MemberLogin.php';
            $objMemberLogin = new MemberLoginClass\MemberLogin();

            $objMemberLogin->setMemberid($memberData->memberid);
            $objMemberLogin->setOtp($memberData->otp);
            $encypted = password_hash($memberData->new_pin, PASSWORD_BCRYPT);

            $objMemberLogin->setPin($encypted);

            $response = $objMemberLogin->check_its_valid_otp($memberData->otp);

            if (!empty($response['status']) && $response['status'] === true) {
                $objMemberLogin->update_pin();
            }
        }
        echo json_encode($response);
    }

}
