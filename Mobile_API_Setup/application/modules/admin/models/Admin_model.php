<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function list_role_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'asc';
        }

        if (!empty($filter)) {
            $this->db->like('id', $filter);
            $this->db->or_like('name', $filter);
            $this->db->or_like('created', $filter);
        }

        $colowmn = array('id', 'name', 'status', 'created', 'archive');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'role');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where('archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());



        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['id'] = $val->id;
                $row['name'] = $val->name;
                $row['created'] = date('d-m-y', strtotime($val->created));
                $row['action'] = '';
                $row['archive'] = $val->archive;
                $row['status'] = $val->status;
                $dataResult[] = $row;
            }
        }


        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);


        return $return;
    }

    public function check_dublicate_email($email) {
        $sql = "SELECT email FROM `tbl_admin` where email = '" . $email . "' UNION SELECT email FROM tbl_admin_email WHERE email = '" . $email . "'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function list_admins_dataquery($reqData) {
        $limit = $reqData->pageSize;
        $page = $reqData->page;
        $sorted = $reqData->sorted;
        $filter = $reqData->filtered;
        $orderBy = '';
        $direction = '';

        if (!empty($sorted)) {
            if (!empty($sorted[0]->id)) {
                $orderBy = $sorted[0]->id;
                $direction = ($sorted[0]->desc == 1) ? 'Desc' : 'Asc';
            }
        } else {
            $orderBy = 'id';
            $direction = 'asc';
        }

        if (!empty($filter)) {
            $this->db->like('username', $filter);
            $this->db->or_like('firstname', $filter);
            $this->db->or_like('lastname', $filter);
            $this->db->or_like('phone', $filter);
            $this->db->or_like('email', $filter);
            $this->db->or_like('gender', $filter);
        }

        $colowmn = array('id', 'username', 'firstname', 'lastname', 'phone', 'email', 'status', 'gender');
        $dt_query = $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $colowmn)), false);
        $this->db->from(TBL_PREFIX . 'admin');

        $this->db->order_by($orderBy, $direction);
        $this->db->limit($limit, ($page * $limit));
        $this->db->where('archive', 0);
        $query = $this->db->get() or die('MySQL Error: ' . $this->db->_error_number());



        $dt_filtered_total = $this->db->query('SELECT FOUND_ROWS() as count;')->row()->count;


        if ($dt_filtered_total % $limit == 0) {
            $dt_filtered_total = ($dt_filtered_total / $limit);
        } else {
            $dt_filtered_total = ((int) ($dt_filtered_total / $limit)) + 1;
        }

        $dataResult = array();
        if (!empty($query->result())) {
            foreach ($query->result() as $val) {
                $row = array();
                $row['id'] = $val->id;
                $row['username'] = $val->username;
                $row['firstname'] = $val->firstname;
                $row['lastname'] = $val->lastname;
                $row['phone'] = $val->phone;
                $row['email'] = $val->email;
                $row['status'] = $val->status;
                $row['gender'] = $val->gender;

                $dataResult[] = $row;
            }
        }


        $return = array('count' => $dt_filtered_total, 'data' => $dataResult);


        return $return;
    }

    public function get_admin_permission($token, $pemission_key = false) {

        $tbl_admin_login = TBL_PREFIX . 'admin_login';
        $tbl_admin_role = TBL_PREFIX . 'admin_role';

        $this->db->select(array($tbl_admin_role . '.roleId', $tbl_admin_login . '.adminId'));
        $this->db->from($tbl_admin_login);
        $this->db->join($tbl_admin_role, $tbl_admin_login . '.adminId = ' . $tbl_admin_role . '.adminId', 'left');
        $this->db->where(array('token' => $token));
        $role_query = $this->db->get();
        $rols_data = $role_query->result();

        if (!empty($rols_data[0]->roleId) || $rols_data[0]->adminId == 1) {
            if ($rols_data[0]->adminId == 1 && $pemission_key)
                return true;

            $roleIds = array();
            foreach ($rols_data as $val) {
                $temp_roleIds[] = $val->roleId;
            }
            $roleIds = implode(', ', $temp_roleIds);

            $tbl_permission = TBL_PREFIX . 'permission';
            $tbl_role_permission = TBL_PREFIX . 'role_permission';

            $this->db->select(array('tbl_permission.permission'));
            $this->db->from($tbl_permission);
            $this->db->join($tbl_role_permission, $tbl_role_permission . '.permission = ' . $tbl_permission . '.id', 'inner');
           
            if ($rols_data[0]->adminId != 1) {
                $this->db->where_in($roleIds);
            }

            if ($pemission_key) {
                $this->db->where(array($tbl_permission . '.permission' => $pemission_key));
            }

            $permission_query = $this->db->get();
            return $permission_query->result();
        }
    }

}
