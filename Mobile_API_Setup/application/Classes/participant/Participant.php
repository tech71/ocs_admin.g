<?php

namespace ParticipantClass;

/*
 * Filename: Participant.php
 * Desc: Details of Participants 
 * @author YDT <yourdevelopmentteam.com.au>
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * Class: Participant
 * Desc: Participants details like participant first name, last name, email, phone etc.
 * Variables and Getter and Setter Methods for participant class
 * Created: 02-08-2018
 */

class Participant {

    public function __construct() {
        $CI = & get_instance();
    }

    /**
     * @var participantid
     * @access private
     * @vartype: int
     */
    private $participantid;

    /**
     * @var username
     * @access private
     * @vartype: varchar
     */
    private $username;

    /**
     * @var firstname
     * @access private
     * @vartype: varchar
     */
    private $firstname;
    private $middlename;

    /**
     * @var middlename
     * @access private
     * @vartype: varchar
     */
    private $lastname;

    /**
     * @var lastname
     * @access private
     * @vartype: varchar
     */
    private $gender;

    /**
     * @var gender
     * @access private
     * @vartype: tinyint
     */
    private $preferredname;

    /**
     * @var preferredname
     * @access private
     * @vartype: varchar
     */
    private $dob;

    /**
     * @var dob
     * @access private
     * @vartype: varchar
     */
    private $ndis_num;

    /**
     * @var ndis_num
     * @access private
     * @vartype: varchar
     */
    private $medicare_num;

    /**
     * @var crn_num
     * @access private
     * @vartype: varchar
     */
    private $crn_num;

    /**
     * @var referral
     * @access private
     * @vartype: tinyint
     */
    private $referral;

    /**
     * @var participantid
     * @access private
     * @vartype: varchar
     */
    private $living_situation;

    /**
     * @var participantid
     * @access private
     * @vartype: varchar
     */
    private $aboriginal_tsi;

    /**
     * @var participantid
     * @access private
     * @vartype: tinyint
     */
    private $oc_departments;

    /**
     * @var participantid
     * @access private
     * @vartype: int
     */
    private $houseid;

    /**
     * @var participantid
     * @access private
     * @vartype: varchar
     */
    private $created;

    /**
     * @var participantid
     * @access private
     * @vartype: tinyint
     */
    private $status;

    /**
     * @var participantid
     * @access private
     * @vartype: tinyint
     */
    private $portal_access;

    /**
     * @arrayname participant_email
     * @access private
     * @array int|varchar|char
     */
    private $participant_email = array();

    /**
     * @arrayname participant_phone
     * @access private
     * @array int|varchar|tinyint
     */
    private $participant_phone = array();

    /**
     * @arrayname participant_assistance
     * @access private
     * @array int|smallint
     */
    private $participant_assistance = array();

    /**
     * @arrayname participant_oc_services
     * @access private
     * @array int|smallint
     */
    private $participant_oc_services = array();

    /**
     * @arrayname participant_care_not_tobook
     * @access private
     * @array int|tinyint|varchar
     */
    private $participant_care_not_tobook = array();
    private $referralfirstname;
    private $referrallastname;
    private $referralemail;
    private $referralphone;

    /**
     * @function getParticipantid
     * @access public
     * @returns $participantid int
     * Get Participant Id
     */
    public function getParticipantid() {
        return $this->participantid;
    }

    /**
     * @function setParticipantid
     * @access public
     * @param $participantid integer 
     * Set Participant Id
     */
    public function setParticipantid($participantid) {
        $this->participantid = $participantid;
    }

    /**
     * @function getUserName
     * @access public
     * @returns $participantid int
     * Get Participant Id
     */
    public function getUserName() {
        return $this->username;
    }

    public function setUserName($username) {
        $this->username = $username;
    }

    /**
     * @function getFirstname
     * @access public
     * @returns $firstname varchar
     * Get Firstname
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * @function setFirstname
     * @access public
     * @param $firstname varchar 
     * Set Firstname
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    /**
     * @function getMiddlename
     * @access public
     * @returns $middlename varchar
     * Get Middlename
     */
    public function getMiddlename() {
        return $this->middlename;
    }

    /**
     * @function setMiddlename
     * @access public
     * @param $firstname varchar 
     * Set Middlename
     */
    public function setMiddlename($middlename) {
        $this->middlename = $middlename;
    }

    /**
     * @function getLastname
     * @access public
     * @returns $lastname varchar
     * Get Lastname
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * @function setLastname
     * @access public
     * @param $lastname varchar 
     * Set Lastname
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;
    }

    /**
     * @function getGender
     * @access public
     * @returns $gender varchar
     * Get Gender
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * @function setGender
     * @access public
     * @param $gender varchar 
     * Set Gender
     */
    public function setGender($gender) {
        $this->gender = $gender;
    }

    /**
     * @function getPreferredname
     * @access public
     * @returns $preferredname varchar
     * Get Preferredname
     */
    public function getPreferredname() {
        return $this->preferredname;
    }

    /**
     * @function setPreferredname
     * @access public
     * @param $gender varchar 
     * Set Preferredname
     */
    public function setPreferredname($preferredname) {
        $this->preferredname = $preferredname;
    }

    /**
     * @function getDob
     * @access public
     * @returns $dob varchar
     * Get Dob
     */
    public function getDob() {
        return $this->dob;
    }

    /**
     * @function setDob
     * @access public
     * @param $dob varchar 
     * Set Dob
     */
    public function setDob($dob) {
        $this->dob = $dob;
    }

    /**
     * @function getNdisNum
     * @access public
     * @returns $ndis_num varchar
     * Get NdisNum
     */
    public function getNdisNum() {
        return $this->ndis_num;
    }

    /**
     * @function setNdisNum
     * @access public
     * @param $ndisNum varchar 
     * Set NdisNum
     */
    public function setNdisNum($ndisNum) {
        $this->ndis_num = $ndisNum;
    }

    /**
     * @function getMedicareNum
     * @access public
     * @returns $medicare_num varchar
     * Get Medicare_num
     */
    public function getMedicareNum() {
        return $this->medicare_num;
    }

    /**
     * @function setMedicareNum
     * @access public
     * @param $medicareNum varchar 
     * Set MedicareNum
     */
    public function setMedicareNum($medicareNum) {
        $this->medicare_num = $medicareNum;
    }

    /**
     * @function getCrnNum
     * @access public
     * @returns $CrnNum varchar
     * Get CrnNum
     */
    public function getCrnNum() {
        return $this->crn_num;
    }

    /**
     * @function setCrnNum
     * @access public
     * @param $crnNum varchar 
     * Set CrnNum
     */
    public function setCrnNum($crnNum) {
        $this->crn_num = $crnNum;
    }

    /**
     * @function getReferral
     * @access public
     * @returns $referral tinyint
     * Get Referral
     */
    public function getReferral() {
        return $this->referral;
    }

    /**
     * @function setReferral
     * @access public
     * @param $referral tinyint 
     * Set Referral
     */
    public function setReferral($referral) {
        $this->referral = $referral;
    }

    /**
     * @function getLivingSituation
     * @access public
     * @returns $living_situation varchar
     * Get Living Situation
     */
    public function getLivingSituation() {
        return $this->living_situation;
    }

    /**
     * @function setLivingSituation
     * @access public
     * @param $livingSituation varchar 
     * Set Living Situation
     */
    public function setLivingSituation($livingSituation) {
        $this->living_situation = $livingSituation;
    }

    /**
     * @function getAboriginalTsi
     * @access public
     * @returns $aboriginal_tsi varchar
     * Get Aboriginal Tsi
     */
    public function getAboriginalTsi() {
        return $this->aboriginal_tsi;
    }

    /**
     * @function setAboriginalTsi
     * @access public
     * @param $aboriginalTsi varchar 
     * Set Aboriginal Tsi
     */
    public function setAboriginalTsi($aboriginalTsi) {
        $this->aboriginal_tsi = $aboriginalTsi;
    }

    /**
     * @function getOcDepartments
     * @access public
     * @returns $oc_departments tinyint
     * Get Oc Departments
     */
    public function getOcDepartments() {
        return $this->oc_departments;
    }

    /**
     * @function setOcDepartments
     * @access public
     * @param $ocDepartments tinyint 
     * Set Oc Departments
     */
    public function setOcDepartments($ocDepartments) {
        $this->oc_departments = $ocDepartments;
    }

    /**
     * @function getHouseid
     * @access public
     * @returns $houseid tinyint
     * Get Houseid
     */
    public function getHouseid() {
        return $this->houseid;
    }

    /**
     * @function setHouseid
     * @access public
     * @param $houseid int 
     * Set Houseid
     */
    public function setHouseid($houseid) {
        $this->houseid = $houseid;
    }

    /**
     * @function getCreated
     * @access public
     * @returns $houseid varchar
     * Get Houseid
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @function setCreated
     * @access public
     * @param $houseid varchar 
     * Set Created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @function getStatus
     * @access public
     * @returns $houseid tinyint
     * Get Status
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @function setStatus
     * @access public
     * @param $status tinyint 
     * Set Status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @function getPortalAccess
     * @access public
     * @returns $portal_access tinyint
     * Get Portal Access
     */
    public function getPortalAccess() {
        return $this->portal_access;
    }

    /**
     * @function setPortalAccess
     * @access public
     * @param $portalAccess tinyint 
     * Set Portal Access
     */
    public function setPortalAccess($portalAccess) {
        $this->portal_access = $portalAccess;
    }

    /**
     * @function getParticipantEmail
     * @access public
     * returns  $participant_email integer|char|varchar
     * Get Participant Email
     */
    public function getParticipantEmail() {
        return $this->participant_email;
    }

    /**
     * @function setParticipantEmail
     * @access public
     * @param $participantEmail integer|char|varchar
     * Set Participant Email
     */
    public function setParticipantEmail($participantEmail) {
        $this->participant_email = $participantEmail;
    }

    /**
     * @function getParticipantPhone
     * @access public
     * returns  $participant_phone integer|varchar|tinyint
     * Get Participant Phone
     */
    public function getParticipantPhone() {
        return $this->participant_phone;
    }

    /**
     * @function setParticipantPhone
     * @access public
     * @param $participantPhone integer|varchar|tinyint
     * Set Participant Phone
     */
    public function setParticipantPhone($participantPhone) {
        $this->participant_phone = $participantPhone;
    }

    /**
     * @function getParticipantAssistance
     * @access public
     * returns  $participant_assistance integer|smallint
     * Get Participant Assistance
     */
    public function getParticipantAssistance() {
        return $this->participant_assistance;
    }

    /**
     * @function setParticipantAssistance
     * @access public
     * @param $participantAssistance integer|smallint
     * Set Participant Participant Assistance
     */
    public function setParticipantAssistance($participantAssistance) {
        $this->participant_assistance = $participantAssistance;
    }

    /**
     * @function getParticipantOcServices
     * @access public
     * returns  $participant_oc_services integer|smallint
     * Get Participant Oc Services
     */
    public function getParticipantOcServices() {
        return $this->participant_oc_services;
    }

    /**
     * @function setParticipantGoalResult
     * @access public
     * @param $participantOcServices integer|smallint
     * Set Participant Oc Services
     */
    public function setParticipantOcServices($participantOcServices) {
        $this->participant_oc_services = $participantOcServices;
    }

    /**
     * @function getParticipantCareNotTobook
     * @access public
     * returns  $participant_care_not_tobook integer|tinyint|varchar
     * Get Participant Care Not Tobook Services
     */
    public function getParticipantCareNotTobook() {
        return $this->participant_care_not_tobook;
    }

    /**
     * @function setParticipantCareNotTobook
     * @access public
     * @param $participantCareNotTobook integer|tinyint|varchar
     * Set Participant Care Not To book
     */
    public function setParticipantCareNotTobook($participantCareNotTobook) {
        $this->participant_care_not_tobook = $participantCareNotTobook;
    }

    public function getReferralFirstName() {
        return $this->referralfirstname;
    }

    public function setReferralFirstName($referralfirstname) {
        $this->referralfirstname = $referralfirstname;
    }

    public function getReferralLastName() {
        return $this->referrallastname;
    }

    public function setReferralLastName($referrallastname) {
        $this->referrallastname = $referrallastname;
    }

    public function getReferralEmail() {
        return $this->referralemail;
    }

    public function setReferralEmail($referralemail) {
        $this->referralemail = $referralemail;
    }

    public function getReferralPhone() {
        return $this->referralphone;
    }

    public function setReferralPhone($referralphone) {
        $this->referralphone = $referralphone;
    }

    private $participantrelation;

    public function getParticipantRelation() {
        return $this->participantrelation;
    }

    public function setParticipantRelation($participantrelation) {
        $this->participantrelation = $participantrelation;
    }

    private $archive;

    public function getArchive() {
        return $this->archive;
    }

    public function setArchive($archive) {
        $this->archive = $archive;
    }

    public function AddParticipant() {
        // check Here server site validation		
        // Insert record method		
        $CI = & get_instance();
        $CI->load->model('Participant_model');
        return $CI->Participant_model->create_participant($this);
    }

    public function UpdateParticipant() {
        
    }

    public function DeleteParticipant() {
        
    }

    public function getParticipant() {
        
    }

    public function getParticipantList() {
        
    }

    public function participant_UserName() {
        $CI = & get_instance();
        $CI->load->model('Participant_model');
        return $CI->Participant_model->Check_UserName($this);
    }

}
